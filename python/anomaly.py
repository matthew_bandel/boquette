# import math numpy
import numpy

# import SVN from sklearn
from sklearn.svm import OneClassSVM


def detect_anomaly_svn(matrices, fraction=0.05):
    """Use One-Class SVM for detecting anomalous rows.

    Arguments:
        matrices: list of numpy arrays, first one used for training
        fraction: fraction of outlier allowed in training

    Returns:
        None
    """

    # squeeze out leading trivial dimensions of matrices
    matrices = [matrix.squeeze() for matrix in matrices]

    # extract training radiances from first matrix
    radiances = matrices[0]

    # establish number of rows
    rows = radiances.shape[-2]

    # chop off margins timewise and spectrally
    margin = 400
    marginii = 20
    radiances = radiances[margin:-margin, :, marginii:-marginii]

    # make machines for each row
    machines = []
    verifications = []
    results = []
    for row in range(rows):

        # extract training data
        matrix = radiances[:, row, :]

        # perform model fit
        machine = OneClassSVM(gamma='scale', kernel='rbf', nu=fraction)
        machine.fit(matrix)

        # add machine
        machines.append(machine)

        # get predictions from training data, and normalize by max value
        verification = machine.score_samples(matrix)
        verification = verification / verification.max()
        verifications.append(verification)

    # add verifications to results
    verifications = numpy.array(verifications)
    results.append(verifications)

    # make predictions for each remaining matrix
    for radiances in matrices[1:]:

        # extract data
        radiances = radiances[margin:-margin, :, marginii:-marginii]

        # collect predictions for each row
        predictions = []
        for row in range(rows):

            # extract training data
            matrix = radiances[:, row, :]

            # perform model fit and add predictions
            machine = machines[row]
            prediction = machine.score_samples(matrix)
            prediction = prediction / prediction.max()
            predictions.append(prediction)

        # add predictions to results
        predictions = numpy.array(predictions)
        results.append(predictions)

    return results