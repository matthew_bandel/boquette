#!/usr/bin/python3

# komodos for monitoring row anomaly and other features

# import local classes
from cores import Core
from plates import Plate
from features import Feature
from formulas import Formula
from hydras import Hydra
from squids import Squid
from sliders import Slider

# import pillow for creating icons
from PIL import Image, ImageDraw
from io import BytesIO

# import requests
import requests

# import general tools
import sys
import os
import re
import yaml

# import time and datetime
import time
import datetime
import calendar

# import collection
from collections import Counter

# import numpy
import numpy

# import xarray
import xarray


# Komodoes class
class Komodo(Core):
    """Class Komodo to monitor row anomaly

    Inherits from:
        Core
    """

    def __init__(self):
        """Initialize instance.

        Arguments:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # create squid for plots
        self.squid = Squid('../studies/komodo')

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Komodo instance >'

        return representation

    def _envision(self, matrix, destination, texts=None, spectrum=None, coordinates=None):
        """Envision a matrix as a heat map.

        Arguments:
            matrix: 2-D numpy array
            destination: file path for saving image
            texts: tuple of str (title, units, dependent, independent)
            spectrum: specturm to apply
            coordinates: dict of lists

        Returns:
            None
        """

        # unpack texts
        texts = texts or []
        texts = [text for text in texts] + [''] * 4
        title, units, dependent, independent = texts[:4]

        # set default zoom
        zoom = (0, len(matrix[0]), 0, len(matrix))

        # make default labels
        independent = independent or 'horizontal gridpoint'
        dependent = dependent or 'vertical gridpoint'

        # set default coordinates
        if not coordinates:

            # default to indices
            coordinates = {independent: list(range(matrix.shape[1]))}
            coordinates.update({dependent: list(range(matrix.shape[0]))})

        # make xarry matrix
        matrix = xarray.DataArray(matrix, dims=[dependent, independent], coords=coordinates)
        matrix.attrs.update({'route': title, 'units': units})

        # make plate object and draw graph
        spectrum = spectrum or 'classic'
        plate = Plate(matrix, ['vertical', 'horizontal'], zoom, title, spectrum=spectrum, unity=units)
        plate.glance()

        # resave glance image to destination
        image = Image.open('../plots/glance.png')
        image.save(destination)

        return None

    def gap(self, start='2022-09-01', finish='2024-05-30', track=1644):
        """Make plot of l1b data gaps.

        Arguments:
            start: str, starting date
            end: str, ending date
            track: int, standard track length

        Returns:
            None
        """

        # get log file
        log = self._know('../studies/komodo/log/Missing_L1B_Data_Report.txt')

        # parse years
        years = (int(start[:4]), int(finish[:4]) + 1)

        # generate dates
        dates = []
        for year in range(*years):

            # for each month
            for month in range(1, 13):

                # for each day
                for day in range(1, 32):

                    # create date
                    date = '{}-{}-{}'.format(year, self._pad(month), self._pad(day))
                    dates.append(date)

        # filter by start, end
        dates = [date for date in dates if date > start and date < finish]

        # create record with default scans
        record = {date: track for date in dates}

        # get all days and scans from log
        days = [line.split(':')[-1].strip() for line in log if line.startswith('date')]
        scans = [int(line.split(':')[-1].strip()) for line in log if line.startswith('collection 4 scanlines')]

        # for each pair
        for day, scan in zip(days, scans):

            # update record
            record[day] = scan

        # sort items
        pairs = list(record.items())
        pairs.sort(key=lambda pair: pair[0])

        # begin arrays
        milliseconds = []
        scans = []

        # for each pair
        for date, scan in pairs:

            # try to
            try:

                # convert date to datetime
                year, month, day = [int(component) for component in date.split('-')]
                millisecond = datetime.datetime(year, month, day).timestamp() * 1000

                # add to arrays
                milliseconds.append(millisecond)
                scans.append(scan)

            # unless ValueError
            except ValueError:

                # in which case, skip
                pass

        # create arrays
        milliseconds = numpy.array(milliseconds)
        scans = numpy.array(scans)

        # make plot
        address = 'plots/gaps/missing_l1b_gaps.h5'
        title = 'Missing L1B Data Gaps'
        self.squid.ink('scanlines', scans, 'time', milliseconds, address, title)

        return None

    def monitor(self, folder, days=31):
        """Grab images from various sites.

        Arguments:
            folder: str, folder for dump
            days: int, number of days ago

        Returns:
            None
        """

        # set up sites
        sites = {'aerosol': 'https://acd-ext.gsfc.nasa.gov/anonftp/toms/omi/images/aerosol/Y2025'}
        sites.update({'ozone': 'https://acd-ext.gsfc.nasa.gov/anonftp/toms/omi/images/global/Y2025'})
        sites.update({'north': 'https://acd-ext.gsfc.nasa.gov/anonftp/toms/omi/images/npole/Y2025'})
        sites.update({'south': 'https://acd-ext.gsfc.nasa.gov/anonftp/toms/omi/images/spole/Y2025'})
        sites.update({'cloud': 'https://acd-ext.gsfc.nasa.gov/anonftp/toms/omi/images/radiative_cloud_fraction/Y2025'})

        # for each site
        for tag, site in sites.items():

            # make directory if needed and clear the previous directory
            self._make('{}/{}'.format(folder, tag))
            self._clean('{}/{}'.format(folder, tag), force=True)

            # print
            self._stamp('{}, {}...'.format(tag, site), initial=True)

            # get the data
            request = requests.get(site)
            text = request.text

            # get all png files, up to 31 days ago
            search = re.findall('"[a-z,A-Z,0-9,_]{3,}.png"', text)
            search.sort()
            search = search[-days:]

            # print
            self._stamp('constructing images...')

            # construct website urls
            webs = ['{}/{}'.format(site, result.strip('"')) for result in search]
            for site in webs:

                # get the imagee
                response = requests.get(site)
                image = Image.open(BytesIO(response.content)).convert('RGBA')

                # save to destination
                destination = '{}/{}/{}'.format(folder, tag, site.split('/')[-1])
                image.save(destination)

            # status
            self._stamp('constructed.')

        # create slideshows
        for tag in sites.keys():

            # create name
            name = '{}_observations.pdf'.format(tag)

            # create slider
            slider = Slider('{}/{}'.format(folder, tag))
            slider.arrange(bypass=True)
            slider.stitch(name, archive=None)

            # move to desktop
            date = datetime.datetime.fromtimestamp(self.now).strftime('%Ym%m%d')
            self._move('{}/{}/{}_{}'.format(folder, tag, date, name), '../../../Desktop')

        return None

    def orbit(self, number=None, new=True):
        """Report information on missing orbit.

        Arguments:
            number: int, orbit number
            new: boolean, entering new orbit?

        Returns:
            None
        """

        # set max line length
        maximum = 80

        # if a number given
        if number:

            # make string
            number = str(number)

        # set fields
        fields = {'orbit': 'orbit number', 'date': 'date', 'bug': 'bugzilla reference'}
        fields.update({'three': 'collection 3 scanlines', 'four': 'collection 4 scanlines'})
        fields.update({'latitude': 'collection 3 latitude range', 'latitudeii': 'collection 4 latitude range'})
        fields.update({'issue': 'issue', 'comment': 'comment', 'update': 'last updated'})

        # load or create orbits file
        destination = '../studies/komodo/issues/issues.json'
        issues = self._load(destination)

        # if entering new
        if new:

            # enter orbit number
            orbit = number or str(int(input('orbit number? ')))
            issues[orbit] = issues.setdefault(orbit, {})
            issues[orbit]['orbit'] = self._pad(int(orbit), 6)

            # enter update date
            now = datetime.datetime.now()
            date = '{}-{}-{}'.format(now.year, self._pad(now.month), self._pad(now.day))
            issues[orbit]['update'] = date

            # for each field
            for field, description in fields.items():

                # ignore orbit and updated date
                if field not in ('orbit', 'update'):

                    # get current entry, setting to blank by default
                    current = issues[orbit].setdefault(field, '')
                    self._print('{}: {}'.format(description, current))
                    information = input('update {}? '.format(description)) or current

                    # add to record
                    issues[orbit][field] = information

        # dump file
        self._dump(issues, destination)

        # sort issues by orbit number
        orbits = list(issues.keys())
        orbits = [int(orbit) for orbit in orbits]
        orbits.sort(reverse=True)

        # begin report
        report = []
        report.append(self._print('Level1B Missing Data Report'))

        # for each orbit
        for orbit in orbits:

            # add spacer
            report.append(self._print(''))
            report.append(self._print(''))

            # for each field
            for field, description in fields.items():

                # add to report
                line = self._print('{}: {}'.format(description, issues[str(orbit)][field]))

                # while line has content
                while len(line) > 0:

                    # begin offset
                    offset = 0

                    # get section
                    section = (line + ' ')[:maximum - offset]
                    while section[-1] != ' ':

                        # incrase offset
                        offset += 1

                        # get new section
                        section = line[:maximum - offset]

                    # print section
                    report.append(section)

                    # reform line
                    line = line[maximum - offset:]

        # dump report
        destinationii = '../studies/komodo/log/Missing_L1B_Data_Report.txt'
        self._jot(report, destinationii)

        return None

    def scan(self, date=None, folder=None):
        """Scan through yesterday's level 1b to see array sizes.

        Arguments:
            date: date str in 'yyyy-nn-dd' format

        Returns:
            None
        """

        # set default folder
        folder = folder or '../studies/komodo/reports'
        self._make('../studies/komodo')
        self._make(folder)

        # if now date given:
        if not date:

            # get yesterday's date
            today = datetime.datetime.now()

            # set date
            delta = datetime.timedelta(days=1)
            yesterday = today - delta

            # construct string
            date = '{}-{}-{}'.format(yesterday.year, self._pad(yesterday.month), self._pad(yesterday.day))

        # unpack date
        year, month, day = date.split('-')

        # set up reservoir
        bands = ['BAND{}'.format(number) for number in (1, 2, 3)]
        reservoir = {3: {band: {} for band in bands}}
        reservoir.update({4: {band: {} for band in bands}})

        # set up second reseroivr for latitudees
        reservoirii = {3: {band: {} for band in bands}}
        reservoirii.update({4: {band: {} for band in bands}})

        # set products and bands
        products = {'OML1BRUG': ('BAND1', 'BAND2'), 'OML1BRVG': ('BAND3',)}
        archives = {3: '10003', 4: '10004'}
        radiances = {3: 'RadianceMantissa', 4: 'radiance'}
        latitudes = {3: 'Latitude', 4: 'latitude'}
        channels = {4: {band: band for band in bands}}
        channels.update({3: {'BAND1': 'UV-1', 'BAND2': 'UV-2', 'BAND3': 'VIS'}})

        # begin orbits list
        orbits = []

        # for each product
        for product, bands in products.items():

            # for each collection
            for collection in (3, 4):

                # create hydra
                formats = (archives[collection], product, year, month, day)
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}/{}'.format(*formats))

                # for each path
                paths = [path for path in hydra.paths if not path.endswith('.met')]
                for path in paths:

                    # begin processing path
                    print('path: {}...'.format(path))

                    # get the orbit number
                    names = list(radiances.values()) + list(latitudes.values())
                    hydra.ingest(path, names=names)
                    orbit = int(hydra._stage(path)['orbit'])
                    orbits.append(orbit)

                    # for each band
                    for band in bands:

                        # grab the data
                        formats = (channels[collection][band], radiances[collection])
                        radiance = hydra.grab('{}/{}'.format(*formats))

                        # add shape to reservoir
                        reservoir[collection][band][orbit] = radiance.shape

                        # grab the latitudes
                        formats = (channels[collection][band], latitudes[collection])
                        latitude = hydra.grab('{}/{}'.format(*formats))

                        # get first and last latitude positions
                        latitude = latitude.squeeze()
                        middle = int(latitude.shape[1] / 2)
                        reservoirii[collection][band][orbit] = (latitude[0, middle], latitude[-1, middle])

        # begin report
        report = ['Level 1B shapes for {}, {} orbits'.format(date, len(orbits) / 4)]

        # for each orbit
        orbits = self._skim(orbits)
        orbits.sort()

        for orbit in orbits:

            # add orbit to report
            report.append('{}:'.format(orbit))

            # for each band
            bands = ['BAND{}'.format(band) for band in (1, 2, 3)]
            for band in bands:

                # get collection 3 and collection 4 arrays, or default to 0
                three = reservoir[3][band].setdefault(orbit, (0,))
                four = reservoir[4][band].setdefault(orbit, (0,))
                threeii = reservoirii[3][band].setdefault(orbit, (0,))
                fourii = reservoirii[4][band].setdefault(orbit, (0,))

                # construct line
                formats = (band, three, four, ' ' * 20, '*' * int(three != four), '*' * int(max(four) < 1400))
                line = '{}: Col3: {}, Col4: {}, {}{}{}'.format(*formats)
                report.append(line)

                # construct latitudes line
                formats = (threeii, fourii)
                line = 'latitudes: Col3: {}, Col4: {}'.format(*formats)
                report.append(line)

        # print report to screen
        [self._print(line) for line in report]

        # if folder given
        if folder:

            # jot report
            destination = '{}/Orbit_images_{}m{}{}'.format(folder, year, month, day)
            self._jot(report, destination)

        return None


# if opening from commandline
if len(sys.argv[0]) > 1:

    # print status
    print('starting komodo...')

    # get arguments
    arguments = sys.argv[1:]

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # if verify option
    if '--scan' in options:

        # add default arguments
        arguments += [None, None]

        # run verification script on yesterday's date
        komodo = Komodo()
        komodo.scan(*arguments[:2])

    # otherwise
    else:

        # run script
        komodo = Komodo()
        komodo.monitor('../studies/komodo', days=65)
