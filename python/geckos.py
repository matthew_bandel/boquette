# geckos.py to make GES DISC spreadsheet

# import local classes
from cores import Core

# import math
import math


# class Gecko to parse hdf files
class Gecko(Core):
    """Gecko class to parse GES DISC information.

    Inherits from:
        cores.Core
    """

    def __init__(self, sink=''):
        """Initialize a Gecko instance.

        Arguments:
            sink: str, filepath for data dump
            source: str, filepath of source files
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # set directory information
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Gecko instance at: {} >'.format(self.sink)

        return representation

    def _calculate(self, records):
        """Calculate individual file sizes.

        Arguments:
            records: list of dicts

        Returns:
            list of dicts
        """

        # begin calculations
        calculations = []

        # set units
        units = {'TB': 1, 'GB': 1000, 'MB': 1000000}

        # for each record
        for record in records:

            # convert total size to TB
            record['terabytes'] = round(float(record['total']) / units[record['unit']], 4)

            # find file size in MB
            record['size'] = round(float(units['MB']) * record['terabytes'] / int(record['files']), 4)

            # add to calculations
            calculations.append(record)

        return calculations

    def _measure(self, records):
        """Input file sizes.

        Arguments:
            records: list of dict

        Returns:
            list of dicts
        """

        # load sizes json
        name = '{}/sizes/sizes.json'.format(self.sink)
        sizes = self._load(name)

        # begin measures
        measures = []

        # for each record
        for record in records:

            # get easdt
            earth = record['ESDT']

            # try to
            try:

                # get the size info
                number = sizes[earth]['files']
                total = sizes[earth]['total']
                unit = sizes[earth]['unit']

            # unless a keyerror
            except KeyError:

                # input the information
                number = input('number of files for {}? '.format(earth))
                total, unit = input('total collection size for {}? '.format(earth)).split()

                # add to sizes
                sizes[earth] = {'files': number, 'total': total, 'unit': unit}

                # dump file
                self._dump(sizes, name)

            # add to record
            record['files'] = number
            record['total'] = total
            record['unit'] = unit

            # add to set
            measures.append(record)

        return measures

    def _parse(self):
        """Parse the text file with gesc disc information.

        Arguments:
            None

        Returns:
            dict of esdts
        """

        # load in text
        text = self._know('{}/notes/disc.txt'.format(self.sink))

        # begin records
        records = []

        # make triplets of all text lines, excluding headers
        text = text[1:]
        number = math.ceil(len(text) / 3)
        triplets = [text[3 * index: 3 * index + 3] for index in range(number)]

        # for each triplet
        for triplet in triplets:

            # begin record
            record = {}

            # grab collection from second line
            fragments = triplet[2].split()
            collection = [fragment for fragment in fragments if fragment.isdigit()]
            if len(collection) > 0:

                # add collection
                record['collection'] = collection[0]

            # otherwise
            else:

                # add collection
                record['collection'] = fragments[2]

            # grab esdt from second line in parantheses
            earth = triplet[1].split()[-2].strip('(')
            record['ESDT'] = earth

            # grab processing level from third field, third line
            level = triplet[2].split()[-3]
            record['level'] = level

            # append records
            records.append(record)

        return records

    def _tabulate(self, records):
        """Create a csv from data.

        Arguments:
            records: list of dict

        Returns:
            None
        """

        # create header
        header = ['ESDT', 'collection', 'processing level', 'number of files', 'size per file ( MB )']
        header += ['total size ( TB )']

        # begin rows
        rows = [header]
        rowsii = [header]

        # sort records by total file size
        records.sort(key=lambda record: record['terabytes'], reverse=True)

        # for each record
        total = 0
        for record in records:

            # create row
            fields = ['ESDT', 'collection', 'level', 'files', 'size', 'terabytes']
            row = [record[field] for field in fields]
            rowii = [record['ESDT'], '4', record['level']]

            # check for verison and processing level
            if int(float(row[1])) == 3 and int(row[2][0]) > 1:

                # add row
                rows.append(row)
                rowsii.append(rowii)

                # add to total
                total += row[-1]

        # add row for total terabytes
        row = ['Total', 'NA', 'NA', 'NA', 'NA', round(total, 2)]
        rows.append(row)

        # make csv
        destination = '{}/tables/GES_DISC_Collection_3_Sizes.csv'.format(self.sink)
        self._table(rows, destination)

        # make csv
        destination = '{}/tables/GES_DISC_Collection_4_blank.csv'.format(self.sink)
        self._table(rowsii, destination)

        return None

    def compile(self):
        """Compile list of ges disc info.

        Arguments:
            None

        Returns:
            None
        """

        # parse the list
        records = self._parse()

        # input sizes
        records = self._measure(records)

        # calculate per file
        records = self._calculate(records)

        # make csv spreadsheet
        rows = self._tabulate(records)

        return rows

