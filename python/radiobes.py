# radiobes.py for the Radiobe class to collect radiance data for trending

# import general tools
import sys
import re
import ast

# import yaml for reading configuration file
import yaml

# import time and datetime
import datetime
import calendar

# import math functions
import math
import numpy

# import local classes
from features import Feature
from formulas import Formula
from hydras import Hydra


# try to
try:

    # import sci-kit learn for regressions
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** sci-kit learn not installed, regressions not possible **')

# try to
try:

    # import matplotlib for plots
    from matplotlib import pyplot

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** matplotlib not installed, plotting not possible **')


# class Radiobe to do OMI data reduction
class Radiobe(Hydra):
    """Radiobe class to generate Calibration records.

    Inherits from:
        list
    """

    def __init__(self, year='', month='', start='', finish='', sink=''):
        """Initialize a Radiobe instance.

        Arguments:
            sink: str, filepath for data dump
            year: str, year
            month: str, month
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # construct source
        source = '/tis/acps/OMI/10004/OML1BCAL/{}/{}'.format(year, self._pad(month))
        self.source = source

        # initialize the base Hydra instance
        Hydra.__init__(self, source, start, finish)
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # display contents
        self._tell(self.paths)

        # create representation
        representation = ' < Radiobe instance at: {} -> {} >'.format(self.source, self.sink)

        return representation

    def collect(self):
        """Collect the calibration data.

        Arguments:
            None

        Returns:
            None
        """

        # create sink folders
        self._make(self.sink)
        self._make('{}/orbits'.format(self.sink))

        # grab chronology
        chronology = self._load('../studies/chronologer/chronicles/chronology.json')

        # for each path
        for path in self.paths:

            # get orbit number
            orbit = self._pad(int(self._stage(path)['orbit']), 6)

            # try to
            try:

                # get configuration ids
                configurations = chronology[orbit]

                # check for solar dark mode
                if 8 in configurations:

                    # and collect dark mode
                    self.radiate(path)

            # unless orbit missing
            except KeyError:

                # alert
                self._print('skipping {}'.format(orbit))

        return None

    def radiate(self, path, sink='radiance'):
        """Collect wls mode data orbital file.

        Arguments:
            path: str, filepath
            sink: str, sink folder

        Returns:
            None
        """

        # make folder
        self._make('{}/orbits/{}'.format(self.sink, sink))

        # ingest the path
        self._print('collecting radiance from {}...'.format(path))
        self.ingest(path)

        # begin data
        data = {}
        attributes = {}

        # grab the orbit number and add to data
        orbit = self._stage(path)['orbit']
        data['orbit_number'] = numpy.array([[int(orbit)]])

        # grab the time
        date = self._stage(path)['date'] + '00'
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[7:9])
        hour = int(date[10:12])
        minute = int(date[12:14])
        second = int(date[14:16])
        time = datetime.datetime(year, month, day, hour, minute, second).timestamp() * 1000
        data['orbit_start_time_fr_yr'] = numpy.array([[time]])

        # for each band
        for band in range(1, 4):

            # for each mode
            for mode in range(3):

                # collect radiance
                radiance = self.dig('BAND{}/MODE_00{}/radiance_avg'.format(band, mode))[0]
                address = 'band_{}/mode_{}/radiance'.format(band, mode)
                data[address] = radiance.distil()
                attributes[address] = radiance.attributes

                # collect wavelength
                wavelength = self.dig('BAND{}/MODE_00{}/wavelength'.format(band, mode))[0]
                address = 'band_{}/mode_{}/wavelength'.format(band, mode)
                data[address] = wavelength.distil()
                attributes[address] = wavelength.attributes

                # collection spectral channel quality
                quality = self.dig('BAND{}/MODE_00{}/spectral_channel_quality'.format(band, mode))[0]
                address = 'band_{}/mode_{}/spectral_channel_quality'.format(band, mode)
                data[address] = quality.distil()
                attributes[address] = quality.attributes

        # create file
        formats = (self.sink, sink, date, orbit)
        destination = '{}/orbits/{}/OML1BCAL_Radiance_{}_o{}.h5'.format(*formats)
        self.spawn(destination, data, attributes)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad up to six
    number = 5
    arguments += [''] * number
    arguments = arguments[:number]

    # unpack arguments
    year, month, start, finish, sink = arguments

    # create radiobe
    radiobe = Radiobe(year, month, start, finish, sink)

    # collect
    radiobe.collect()