# one off script for making reflectances

# import hydra
from formulas import Formula
from hydras import Hydra

# import numpy
import numpy


# clip interpolations to fit
def clip():
    """Clip the interpolated radiances and irradiances to fit the same grids, based on specific files.

    Arguments:
        None

    Returns:
        None
    """

    # initialize hydra
    hydra = Hydra('../studies/reflectance/interpolations')

    # first irradiance needs 2 from the end of band1 and 3 from begining of band3, also squeezing
    paths = [hydra.paths[0]]
    old = 'interpolations'
    new = 'clips'
    names = ['band1_interpolated_irradiance', 'band1_wavelength_grid']
    names += ['band3_interpolated_irradiance', 'band3_wavelength_grid']
    names += ['band2_interpolated_irradiance']
    functions = [lambda tensor: tensor.squeeze()[:, :-2], lambda tensor: tensor[:-2]]
    functions += [lambda tensor: tensor.squeeze()[:, 3:], lambda tensor: tensor[3:]]
    functions += [lambda tensor: tensor.squeeze()]
    tag = 'clip'
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # second need the 2 from beginnig of band1 instead
    paths = [hydra.paths[1]]
    functions = [lambda tensor: tensor.squeeze()[:, 2:], lambda tensor: tensor[2:]]
    functions += [lambda tensor: tensor.squeeze()[:, 3:], lambda tensor: tensor[3:]]
    functions += [lambda tensor: tensor.squeeze()]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # first radiance needs 2 from the end of band1, as well as averaging
    paths = [hydra.paths[2]]
    names = ['band1_interpolated_radiance', 'band1_wavelength_grid']
    names += ['band2_interpolated_radiance']
    def finitizing(tensor): return numpy.array([matrix for matrix in tensor if numpy.isnan(matrix).sum() == 0])
    functions = [lambda tensor: finitizing(tensor.squeeze())[:, :, :-2].mean(axis=0)]
    functions += [lambda tensor: tensor[:-2]]
    functions += [lambda tensor: tensor.squeeze().mean(axis=0)]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # second radiance needs 2 from the end of band1, as well as averaging
    paths = [hydra.paths[3]]
    names = ['band1_interpolated_radiance', 'band1_wavelength_grid']
    names += ['band2_interpolated_radiance']
    functions = [lambda tensor: finitizing(tensor.squeeze())[:, :, 2:].mean(axis=0)]
    functions += [lambda tensor: tensor[2:]]
    functions += [lambda tensor: tensor.squeeze().mean(axis=0)]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # third radiance needs 2 from beginning
    paths = [hydra.paths[4]]
    names = ['band3_interpolated_radiance', 'band3_wavelength_grid']
    functions = [lambda tensor: tensor.squeeze()[:, :, 2:].mean(axis=0), lambda tensor: tensor[2:]]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # fourth radiance needs just averaging
    paths = [hydra.paths[5]]
    names = ['band3_interpolated_radiance']
    functions = [lambda tensor: tensor.squeeze().mean(axis=0)]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    return None


# compute reflectances and logarithms
def reflect():
    """Calculate reflectances on select files.

    Arguments:
        None

    Returns:
        None
    """

    # initialize hydra
    hydra = Hydra('../studies/reflectance/clips')

    # assign paths
    irradiance, irradianceii, ultraviolet, ultravioletii, visible, visibleii = hydra.paths

    # divide first radiance by first irradiance, and take logarithm
    tag = 'refl'
    old = 'clips'
    new = 'reflectances'
    names = ['band1_interpolated_radiance', 'band2_interpolated_radiance']
    namesii = ['band1_interpolated_irradiance', 'band2_interpolated_irradiance']
    function = lambda one, two: one / two
    hydra.synthesize(ultraviolet, irradiance, old, new, names, namesii, function, tag)

    # repeat for band 3
    names = ['band3_interpolated_radiance']
    namesii = ['band3_interpolated_irradiance']
    function = lambda one, two: one / two
    hydra.synthesize(visible, irradiance, old, new, names, namesii, function, tag)

    # divide first radiance by first irradiance, and take logarithm
    tag = 'refl'
    old = 'clips'
    new = 'reflectances'
    names = ['band1_interpolated_radiance', 'band2_interpolated_radiance']
    namesii = ['band1_interpolated_irradiance', 'band2_interpolated_irradiance']
    function = lambda one, two: numpy.where(numpy.isfinite(one / two), one / two, 0)
    hydra.synthesize(ultravioletii, irradianceii, old, new, names, namesii, function, tag)

    # repeat for band 3
    names = ['band3_interpolated_radiance']
    namesii = ['band3_interpolated_irradiance']
    hydra.synthesize(visibleii, irradianceii, old, new, names, namesii, function, tag)

    # ingest reflectances
    hydraii = Hydra('../studies/reflectance/reflectances')

    # take logarithms of both uv files
    paths = hydraii.paths[:2]
    old = 'reflectances'
    new = 'logarithms'
    tag = 'log'
    names = ['band1_interpolated_radiance_refl', 'band2_interpolated_radiance_refl']
    function = lambda tensor: numpy.log10(numpy.where(tensor > 0, tensor, 1))
    functions = [function, function]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    # and visible files
    paths = hydraii.paths[2:]
    names = ['band3_interpolated_radiance_refl']
    functions = [function, function]
    hydra.mimic(paths, old, new, names, functions=functions, tag=tag)

    return None


# data various pixel based ratios
def rationalize(folder):
    """Compute radiance and irradiance pixel based collection 4 vs collection 3 ratios.

    Arguments:
        folder: folder of interpolation data

    Returns
        None
    """

    # initialize hydra
    hydra = Hydra(folder)

    # group paths by orbit
    dates = hydra._group(hydra.paths, lambda path: hydra._stage(path)['date'][:9])
    for date, members in dates.items():

        # sort members
        members.sort()

        # define attributes
        rows = {'band1': 30, 'band2': 60, 'band3': 60}
        collections = ('col4', 'col3')
        bands = ('band1', 'band2', 'band3')

        # create the radiance pixel grids from Collection 4
        hydra.ingest(members[2])
        hydra.ingest(members[4], discard=False)

        # split into independents and Categories
        independents = hydra.dig('IndependentVariables')
        fours = hydra.dig('Categories')

        # begin formula
        formula = Formula()

        # for each band
        for band in bands:

            # average all tropical wavelengths
            def averaging(tensor): return tensor.mean(axis=0)
            parameter = '{}_wavelengths_tropics'.format(band)
            name = '{}_wavelengths_col4'.format(band)
            formula.formulate(parameter, averaging, name)

            # extract pixel grid for each row
            def functional(row): return lambda tensor: tensor[row]
            for row in range(rows[band]):

                # create pixel grid rows
                def gridding(row): return functional(row)
                parameter = '{}_wavelengths_col4'.format(band)
                name = '{}_pixel_grid_{}'.format(band, str(row + 1).zfill(2))
                address = 'IndependentVariables'
                formula.formulate(parameter, gridding(row), name, addresses=address)

            # remove all nans
            def excising(tensor): return numpy.array([matrix for matrix in tensor if numpy.all(numpy.isfinite(matrix))])
            parameter = '{}_radiance_tropics'.format(band)
            name = '{}_radiance_tropics_avg_col4'.format(band)
            formula.formulate(parameter, excising, name)

        # make the grid
        cascade = hydra.cascade(formula, fours)

        # get the categories from collecction 3
        hydra.ingest(members[3])
        hydra.ingest(members[5], discard=False)
        threes = hydra.dig('Categories')

        # begin new formula
        formula = Formula()

        # for bands 2 and 3
        for band in bands[1:]:

            # remove all nans
            def excising(tensor): return numpy.array([matrix for matrix in tensor if numpy.all(numpy.isfinite(matrix))])
            parameter = '{}_radiance_tropics'.format(band)
            name = '{}_radiance_tropics_avg_col3'.format(band)
            formula.formulate(parameter, excising, name)

        # flip band1
        #def excising(tensor): return numpy.array([matrix for matrix in tensor if numpy.all(numpy.isfinite(matrix))])
        def flipping(tensor): return numpy.flip(numpy.array([matrix for matrix in tensor if numpy.all(numpy.isfinite(matrix))]), axis=2)
        formula.formulate('band1_radiance_tropics', flipping, 'band1_radiance_tropics_avg_col3')

        # add collection three radiancce
        cascade += hydra.cascade(formula, threes)

        # begin new formula
        formula = Formula()

        # for each band
        for band in bands:

            # perform division
            def dividing(four, three): return four.mean(axis=0) / three.mean(axis=0)
            parameters = ['{}_radiance_tropics_avg_{}'.format(band, collection) for collection in collections]
            name = '{}_radiance_tropics_col4_div_col3'.format(band)
            formula.formulate(parameters, dividing, name)

        # add collection three radiancce
        cascade += hydra.cascade(formula, cascade)

        # stash contents
        destination = members[2].replace('interpolations', 'ratios').replace('.h5', '{}_col4_div_col3.h5'.format(date))
        hydra._stash(independents + cascade, destination, 'Data')

        # create the irradiance pixel grids from Collection 4
        hydra.ingest(members[0])

        # split into independents and Categories
        independents = hydra.dig('IndependentVariables')
        fours = hydra.dig('Categories')

        # begin formula
        formula = Formula()

        # for each band
        for band in bands:

            # average all tropical wavelengths
            def squeezing(tensor): return tensor.squeeze()
            parameter = '{}_calculated_wavelengths'.format(band)
            name = '{}_wavelengths_col4'.format(band)
            formula.formulate(parameter, squeezing, name)

            # extract pixel grid for each row
            def functional(row): return lambda tensor: tensor[row]
            for row in range(rows[band]):

                # create pixel grid rows
                def gridding(row): return functional(row)
                parameter = '{}_wavelengths_col4'.format(band)
                name = '{}_pixel_grid_{}'.format(band, str(row + 1).zfill(2))
                address = 'IndependentVariables'
                formula.formulate(parameter, gridding(row), name, addresses=address)

            # remove trivial dimensions
            def squeezing(tensor): return tensor.squeeze()
            parameter = '{}_irradiance'.format(band)
            name = '{}_irradiance_col4'.format(band)
            formula.formulate(parameter, squeezing, name)

        # make the grid
        cascade = hydra.cascade(formula, fours)

        # get the categories from collecction 3
        hydra.ingest(members[1])
        threes = hydra.dig('Categories')

        # begin new formula
        formula = Formula()

        # flip band1
        #def flipping(tensor): return numpy.flip(tensor.squeeze(), axis=1)
        def flipping(tensor): return tensor.squeeze()
        formula.formulate('band1_irradiance', flipping, 'band1_irradiance_col3')

        # for each band
        for band in bands[1:]:

            # squeeze out trivials
            def squeezing(tensor): return tensor.squeeze()
            parameter = '{}_irradiance'.format(band)
            name = '{}_irradiance_col3'.format(band)
            formula.formulate(parameter, squeezing, name)

        # add collection three radiancce
        cascade += hydra.cascade(formula, threes)

        # begin new formula
        formula = Formula()

        # for each band
        for band in bands:

            # perform division
            def dividing(four, three): return four / three
            parameters = ['{}_irradiance_{}'.format(band, collection) for collection in collections]
            name = '{}_irradiance_col4_div_col3'.format(band)
            formula.formulate(parameters, dividing, name)

        # add collection three radiancce
        cascade += hydra.cascade(formula, cascade)

        # stash contents
        destination = members[0].replace('interpolations', 'ratios').replace('.h5', '{}_col4_div_col3.h5'.format(date))
        hydra._stash(independents + cascade, destination, 'Data')

    return None


# make lists of all diffuser orbits
def orbit():
    """Make lists of all diffuser orbits.

    Arguments:
        None

    Returns:
        None
    """

    # make a hydra
    hydra = Hydra()

    # import orbit files
    descriptions = {'quartzes': 'Quartz Volume Diffuser', 'regulars': 'Regular Aluminum Diffuser'}
    descriptions.update({'backups': 'Backup Aluminum Diffuser'})
    orbits = {diffuser: hydra._load('../jason/{}.json'.format(diffuser)) for diffuser in descriptions.keys()}

    # for each diffuser
    for diffuser, description in descriptions.items():

        # begin text
        #text = ['{} Orbit Numbers'.format(description)]
        text = []

        # group keys by month
        months = hydra._group(list(orbits[diffuser].keys()), lambda date: '{}-{}'.format(*date.split('-')[:2]))
        months = list(months.items())
        months.sort(key=lambda pair: pair[0])

        # add each month to the text
        for month, members in months:

            print(month)

            # print month after spaceer
            # text.append('')
            # text.append('{}:'.format(month))

            # sort members, and for each one
            members.sort()
            for member in members:

                # add to document
                #line = '{}  {}'.format(orbits[diffuser][member], member)
                line = '{}'.format(orbits[diffuser][member], member)
                text.append(line)

        # create document
        destination = '../text/orbits_only_{}.txt'.format(description.replace(' ', '_')).lower()
        hydra._jot(text, destination)

    return None


