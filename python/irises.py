#!/usr/bin/env python3

# irises.py for the Iris class to build omi climatology

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import netcdf4
import netCDF4

# import random forest
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier

# # import netcdf4
# import netCDF4


# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    #matplotlib.use('Tkagg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Iris to do OMOCNUV analysis
class Iris(Hydra):
    """Iris class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # create squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Iris instance at {} >'.format(self.sink)

        return representation

    def angle(self, year=2005, month=3, day=21, orbit=3628, limit=200):
        """Calculate beta angle.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit
            limit: int, number of points

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots/angle'.format(self.sink))

        # grab hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10004/OML1BCAL/{}/{}/{}'.format(*formats))
        hydra.ingest(self._pad(orbit, 6))

        # begin data
        data = {}

        # get the ephemeris data
        data['abscissa'] = hydra.grab('vel_x').squeeze()
        data['ordinate'] = hydra.grab('vel_y').squeeze()
        data['applicate'] = hydra.grab('vel_z').squeeze()

        # for each field
        for field in data.keys():

            # get data
            array = data[field][:limit]
            steps = list(range(array.shape[0]))[:limit]

            # plot
            formats = (field, year, self._pad(month), self._pad(day), self._pad(orbit, 6))
            title = '{}, {}m{}{}, o{}'.format(*formats)
            address = 'plots/angle/{}_{}m{}{}_o{}.h5'.format(*formats)
            self.squid.ink(field, array, 'step', steps, address, title)

        return None

    def bridge(self, years, months, reference=2019, days=31, collect=False):
        """Collection irr records, bridging the beta angle gap.

        Arguments:
            years: tuple of ints, the years
            months: tuple of ints, the months associated with years
            reference: int, the reference year for timing
            days: int, number of days per month to grab
            collect: boolean: perform collection?

        Returns:
            None
        """

        # construct folders
        self._make('{}/beta'.format(self.sink))
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/beta'.format(self.sink))

        # construct destinations
        formats = (self.sink, years[0], self._pad(months[0]), years[-1], self._pad(months[-1]))
        destination = '{}/beta/OML1BCAL_{}m{}_{}m{}.h5'.format(*formats)
        destinationii = '{}/beta/OML1BIRR_{}m{}_{}m{}.h5'.format(*formats)

        # begin fields
        fields = {}
        fieldsii = {}

        # for each band
        bands = ['band{}'.format(channel) for channel in range(1, 4)]
        bandsii = [channel.upper() for channel in bands]
        for band, bandii in zip(bands, bandsii):

            # create search fields for oml1bcal
            fields.update({'irradiance_{}'.format(band): '{}_IRRADIANCE/irradiance_avg'.format(bandii)})
            fields.update({'wavelength_{}'.format(band): '{}_IRRADIANCE/wavelength'.format(bandii)})
            fields.update({'time_{}'.format(band): '{}_IRRADIANCE/time'.format(bandii)})
            fields.update({'azimuth_{}'.format(band): '{}_IRRADIANCE/solar_azimuth_angle'.format(bandii)})
            fields.update({'elevation_{}'.format(band): '{}_IRRADIANCE/solar_elevation_angle'.format(bandii)})
            fields.update({'excursion_{}'.format(band): '{}_IRRADIANCE/out_of_nominal_range'.format(bandii)})

            # create search fields for oml1birr
            fieldsii.update({'irradiance_{}'.format(band): '{}/irradiance'.format(bandii)})
            fieldsii.update({'wavelength_{}'.format(band): '{}/wavelength'.format(bandii)})
            fieldsii.update({'time_{}'.format(band): '{}/time'.format(bandii)})

        # if performing collection
        if collect:

            # grab the chronology
            chronology = self._load('../studies/chronologer/chronicles/chronology.json')

            # get all quartz diffuser orbits
            orbits = [orbit for orbit, configurations in chronology.items() if 8 in configurations]
            orbits.sort()

            # begin data reservoirs
            data = {field: [] for field in fields.keys()}
            dataii = {field: [] for field in fieldsii.keys()}

            # add time information
            data.update({field: [] for field in ('year', 'month', 'day', 'orbit')})
            dataii.update({field: [] for field in ('year', 'month', 'day', 'orbit')})

            # for each year, month
            for year, month in zip(years, months):

                # create hydra for cal file and subset quartz orbits
                hydra = Hydra('/tis/acps/OMI/10004/OML1BCAL/{}/{}'.format(year, self._pad(month)), 1, days)
                paths = [path for path in hydra.paths if self._stage(path)['orbit'] in orbits]
                paths.sort()

                # for each path:
                for path in paths:

                    # ingest path
                    self._print('ingesting {}...'.format(path))
                    hydra.ingest(path)

                    # append data
                    [data[field].append(hydra.grab(search).squeeze()) for field, search in fields.items()]

                    # add time information
                    data['orbit'].append(int(self._stage(path)['orbit']))
                    data['year'].append(year)
                    data['month'].append(month)
                    data['day'].append(int(self._stage(path)['day'][-2:]))

                # create hydra for irr file
                hydraii = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}'.format(year, self._pad(month)), 1, days)
                pathsii = [path for path in hydraii.paths if self._stage(path)['orbit'] in orbits]
                pathsii.sort()

                # for each path:
                for path in pathsii:

                    # ingest path
                    self._print('ingesting {}...'.format(path))
                    hydraii.ingest(path)

                    # append data
                    [dataii[field].append(hydraii.grab(search).squeeze()) for field, search in fieldsii.items()]

                    # add time information
                    dataii['orbit'].append(int(self._stage(path)['orbit']))
                    dataii['year'].append(year)
                    dataii['month'].append(month)
                    dataii['day'].append(int(self._stage(path)['day'][-2:]))

            # create arrays
            data = {field: numpy.array(array) for field, array in data.items()}
            dataii = {field: numpy.array(array) for field, array in dataii.items()}

            # stash files
            self.spawn(destination, data)
            self.spawn(destinationii, dataii)

        # otherwise
        else:

            # set destinations
            destinations = (destination, destinationii)

            # for each destination
            for destination in destinations:

                # open collection
                hydra = Hydra('{}/beta'.format(self.sink))
                hydra.ingest(destination)

                # get product etc info
                product = hydra._stage(destination)['product']
                bracket = '_'.join(self._file(destination).split('.')[0].split('_')[1:])

                # get data
                data = hydra.extract()

                # get time field and convert to milliseconds with respect to reference year
                initial = data['year'][0]
                dates = [datetime.datetime.fromtimestamp(entry) for entry in data['time_band1']]
                dates = [date.replace(year=reference + date.year - initial) for date in dates]
                time = numpy.array([date.timestamp() * 1000 for date in dates])

                # begin ordinates
                ordinates = {}

                # for each band
                for band in bands:

                    # if cal product
                    if product == 'OML1BCAL':

                        # get angle information
                        ordinates['azimuth_{}'.format(band)] = data['azimuth_{}'.format(band)][:].min(axis=1)
                        ordinates['azimuthii_{}'.format(band)] = data['azimuth_{}'.format(band)][:].max(axis=1)
                        ordinates['elevation_{}'.format(band)] = data['elevation_{}'.format(band)][:].min(axis=1)
                        ordinates['elevationii_{}'.format(band)] = data['elevation_{}'.format(band)][:].max(axis=1)

                        # get percentage of excursion
                        ordinates['excursion_{}'.format(band)] = data['excursion_{}'.format(band)]

                    # get wavelength and irradiance
                    irradiance = data['irradiance_{}'.format(band)]
                    wavelength = data['wavelength_{}'.format(band)]

                    # set band wavelength and row brackets
                    brackets = {'band1': (290, 300), 'band2': (320, 330), 'band3': (390, 400)}
                    sections = {'band1': (0, 11), 'band2': (0, 22), 'band3': (0, 22)}
                    sectionsii = {'band1': (27, 30), 'band2': (54, 60), 'band3': (54, 60)}

                    # set row and wavelength brackets
                    waves = brackets[band]
                    rows = sections[band]
                    rowsii = sectionsii[band]

                    # create mask for wavelengths 290 - 300
                    mask = (wavelength >= waves[0]) & (wavelength <= waves[1]) & numpy.isfinite(wavelength)

                    # limit to rows 12-27
                    indices = [row for row in range(irradiance.shape[1]) if (row < rows[1]) or (row > rowsii[0])]
                    irradiance = irradiance[:, indices, :]
                    mask = mask[:, indices, :]

                    # apply averaging across rows and wavelengths for each timepoint
                    panels = [panel[masque].mean() for panel, masque in zip(irradiance, mask)]
                    ordinates['irradiance_{}'.format(band)] = numpy.array(panels)

                # for each ordinate
                for name, array in ordinates.items():

                    # create mask for fill values
                    mask = (array > 0) & (abs(array) < 1e20) & (numpy.isfinite(array))

                    print('{}, {}, {}'.format(product, name, mask.sum()))

                    # construct plot
                    title = '{}, {}, {}'.format(name, product, bracket)
                    address = 'plots/beta/{}_{}_{}.h5'.format(name, product, bracket)
                    self.squid.ink(name, array[mask], 'time', time[mask], address, title)

        return None

    def wave(self, year=2023, month=11, day=11, orbit=102789, row=30):
        """Compare wavelength assignment across the two products.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit
            row: int, the row to observe

        Returns:
            None
        """

        # create folder
        self._make('{}/plots/wave'.format(self.sink))

        # construct hydra for irr product
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}/{}'.format(*formats))
        hydra.ingest(str(orbit))

        # construcet hydra for cal file
        hydraii = Hydra('/tis/acps/OMI/10004/OML1BCAL/{}/{}/{}'.format(*formats))
        hydraii.ingest(str(orbit))

        # construct date
        date = '{}m{}{}'.format(*formats)

        # for each band
        for band in range(1, 4):

            # get wavelength data
            wavelength = hydra.grab('BAND{}_IRR/wavelength'.format(band)).squeeze()
            reference = hydra.grab('BAND{}_IRR/reference_column'.format(band)).squeeze()
            wavelengthii = hydraii.grab('BAND{}_IRR/wavelength'.format(band)).squeeze()
            shiftii = hydraii.grab('BAND{}_IRR/wavelength_shift'.format(band)).squeeze()

            # set row
            rowii = row

            # if band 1
            if band == 1:

                # half row
                rowii = int(row / 2)

                # flip wavelength
                wavelength = numpy.flip(wavelength, axis=1)

            # print reference wavelength
            self._print('band {}: {}, {}'.format(band, reference, wavelength[rowii, reference]))

            # calculate difference
            difference = wavelengthii - wavelength

            # create titles
            formats = (band, rowii, orbit, date)
            titles = ['OML1BIRR wavelength, Band{}, Row {}, o{}, {}'.format(*formats)]
            titles += ['OML1BCAL wavelength, Band{}, Row {}, o{}, {}'.format(*formats)]
            titles += ['OML1BCAL wavelength - OML1BIRR wavelength, Band{}, Row {}, o{}, {}'.format(*formats)]

            # set names
            names = ['wavelength_irr', 'wavelength_cal', 'wavelength_diff']

            # set panels
            panels = [wavelength, wavelengthii, difference]

            # create rows and pixels
            pixels = numpy.array([range(wavelength.shape[1])] * wavelength.shape[0])

            # for each panel
            for panel, title, name in zip(panels, titles, names):

                # create plot
                address = 'plots/wave/{}_band{}.h5'.format(name, band)
                self.squid.ink(name, panel[rowii], 'pixel', pixels[rowii], address, title)

            # plot shift
            scans = numpy.array(range(shiftii.shape[0]))
            title = 'OML1BCAL, wavelength shift, Band{}, Row {}, o{}, {}'.format(*formats)
            address = 'plots/wave/shift_band{}.h5'.format(band)
            self.squid.ink('shift'.format(band), shiftii[:, rowii], 'scan', scans, address, title)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

