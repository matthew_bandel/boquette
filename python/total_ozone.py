#!/usr/bin/env python3

# total_ozone.py for converting ozone into total ozone, based on OMOCNUV totoz adjustment

# import numpy
import numpy


# calculate total ozone by approximating addition ozone below terrain pressure
def calculate_total_ozone(ozone, latitude, pressure, clip=False):
    """Calculate the total ozone based on measured ozone, latitude, and surface pressure.

    Arguments:
        ozone: numpy array of measured ozone in DU
        latitude: numpy array of corresponding latitude
        pressure: numpy array of surface pressure in Pa
        clip: boolean, clip pressures at 1 atm?

    Returns:
        numpy array of total ozone

    Notes:
        1) this method is based on the pmfrac.f, prfind.f, and uvf2.f functions in the OMOCNUV code.  It is
        meant to correct the OMI measured ozone values with an additional ozone amount expected to be present
        below the terrain pressure.  The original function computes two total ozone values and a profile mixing
        fraction.  These two total ozone values are evenutally interpolated in the final interpolation step in
        uvf2.f.  This code, instead, only returns one total ozone value, carrying out the profile mixing
        interpolation on the below terrain ozone amounts at the bracketing latitudinal nodes.

        2) original omocnuv code uses three (absolute) latitudinal bands.  The equatorial band only uses
        six ozone profile nodes, whereas the other two use ten.  The six equatorial nodes are equivalent to
        the middle six nodes of the other two bands.  The ozone profile nodes map 1:1 to a set of below terrain
        pressure ozone amounts.  For the sake of this code, the equatorial profiles have been padded at either
        end, and the below terrain pressure extensions have been padded as well by duplicating the end
        members.  This results in equivalent behavior but cleaner code.

        3) some surface pressure values are greater than 1 atm, in which case the additional ozone from below
        terrain pressure becomes negative.  Pressures are not clipped to 1 atm in this code by default, but
        may be clipped with the clip=True option.
    """

    # set ozone profile amounts ( original code omits two on each end for equatorial band )
    profiles = [125, 175, 225, 275, 325, 375, 425, 475, 525, 575]
    slope = (profiles[-1] - profiles[0]) / (len(profiles) - 1)
    intercept = profiles[0]

    # calculate primary profile indices, making sure they are at least zero and less than 9
    primary = numpy.floor(intercept + slope * ozone).astype(int)
    primary = numpy.where(primary < 0, 0, primary)
    primary = numpy.where(primary > 8, 8, primary)

    # calculate lower profile indices, in case the adjustment leads to a lower profile bracket
    lower = primary - 1
    lower = numpy.where(lower < 0, 0, lower)

    # calculate upper profile indices, in case the adjustment leads to a larger profile bracket
    upper = primary + 1
    upper = numpy.where(upper > 8, 8, upper)

    # define pascals per atm and convert pressure to atm
    pascals = 101325
    pressure = pressure / pascals

    # if clipping desired
    if clip:

        # clip pressure to 1 atm
        pressure = numpy.where(pressure > 1, 1, pressure)

    # calculate pressure fraction based on surface pressure ( linear approximation from original code )
    fraction = (1 / 0.6) * (1 - pressure)

    # set abs(latitude) band nodes
    bands = [15, 45, 75]

    # set ozone below terrain pressure extension amounts, padding equatorial zone on either side
    extension = [[18.5, 18.5] + [18.5, 18.5, 18.4, 18.4, 18.4, 18.4] + [18.4, 18.4]]
    extension += [[11.1, 11.2, 19.7, 20.0, 20.0, 20.0, 19.8, 20.2, 20.5, 21.8]]
    extension += [[11.1, 11.2, 12.0, 17.4, 17.9, 18.3, 18.7, 19.8, 21.2, 21.8]]
    extension = numpy.array(extension)

    # for each latitude band
    adjustments = []
    for band, _ in enumerate(bands):

        # add extensions at three indices, weighted by the fraction
        additions = [fraction * extension[band, indices] for indices in (lower, primary, upper)]

        # assume primary addition is correct
        guess = ozone + additions[1]

        # recalculate primary index based on new total
        primaryii = numpy.floor(intercept + slope * guess).astype(int)
        primaryii = numpy.where(primaryii < 0, 0, primaryii)
        primaryii = numpy.where(primaryii > 8, 8, primaryii)

        # assume actual addition is primary addition, but adjust based on new guess
        actual = additions[1]
        actual = numpy.where(primaryii < primary, additions[0], actual)
        actual = numpy.where(primaryii > primary, additions[2], actual)

        # add actual adjustment to triplet of adjustments across latitude bands
        adjustments.append(actual)

    # make into array
    adjustments = numpy.array(adjustments)

    # use absolute latitude to calculate weighting factors
    latitude = abs(latitude)

    # initialize weights for all three latitude bands
    weights = numpy.zeros((3, *latitude.shape))

    # for latitude less than 15, weight of first band is 1, otherwise linear until 45 degrees
    weights[0] = numpy.where(latitude <= 15, 1, weights[0])
    weights[0] = numpy.where(latitude > 15, 1 - ((latitude - 15) / 30), weights[0])

    # for latitude between 15 and 75, weights are 1 at 45 for middle band and linear along both sides
    weights[1] = numpy.where(latitude <= 45, (latitude - 15) / 30, weights[1])
    weights[1] = numpy.where(latitude > 45, 1 - ((latitude - 45) / 30), weights[1])

    # for latitude greater than 75, weight of last band is 1, otherwise linear until 45 degrees
    weights[2] = numpy.where(latitude <= 75, (latitude - 45) / 30, weights[2])
    weights[2] = numpy.where(latitude > 75, 1, weights[2])

    # all negative weights should be zero, and weights should max out at 1
    weights = numpy.where(weights < 0, 0, weights)
    weights = numpy.where(weights > 1, 1, weights)

    # total correction is weighted sum across latitude bands
    correction = (adjustments * weights).sum(axis=0)

    # compute total ozone
    total = ozone + correction

    return total