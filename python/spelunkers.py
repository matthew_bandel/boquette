# spelunkers.py for Spelunker class to track variable usage

# import local classes
from cores import Core
from features import Feature
from formulas import Formula

# import general tools
import os
import re
import yaml

# import time and datetime
import time
import datetime
import calendar

# import collection
from collections import Counter

# import numpy nand math
import numpy
import math


# class Spelunker to parse hdf files
class Spelunker(Core):
    """Spelunker class to track variables in a program.

    Inherits from:
        cores.Core
    """

    def __init__(self, source='', sink=''):
        """Initialize a Spelunker instance.

        Arguments:
            source: str, source folder for files
            sink: str, folder for report
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # set directory information
        self.source = source
        self.sink = sink

        # establish code works
        self.code = []
        self._establish()

        # set tab spacing
        self.tab = 4
        self.line = '#line#'

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Spelunker instance at: {} >'.format(self.source)

        return representation

    def _crawl(self, variable, lines, maximum, accumulation=None):
        """Crawl through the lines, recursively adding entries.

        Arguments:
            variable: str, variable of interest
            lines: dict of code lines
            maximum: int, maximum line number to check
            accumulation: list of str, variables searched so far

        Returns:
            dict
        """

        # default acucmulation to current variable
        accumulation = accumulation or []
        accumulation.append(variable)

        # begin history
        history = {}

        # get line numbers
        numbers = [number for number in lines.keys() if number <= maximum]
        numbers.sort(reverse=True)

        # for each number
        for number in numbers:

            # grab the tokens
            tokens = lines[number]['tokens']
            if variable in tokens:

                # add entry
                history[number] = {self.line: lines[number]['line']}

                # add entry for each token
                relations = [token for token in tokens if token not in accumulation]
                for token in relations:

                    # add entry recursively
                    history[number][token] = self._crawl(token, lines, number - 1, accumulation)

        return history

    def _establish(self):
        """Establish code words to ignore.

        Arguments:
            None

        Returns:
            None

        Sets:
            self.codes
        """

        # set code words
        words = ['write', 'read', 'goto', 'end', 'continue', 'do', 'enddo', 'if', 'elif', 'endif']
        words += ['allocate', 'deallocate', 'real', 'integer', 'implicit', 'module', 'kind', 'to', 'call']
        words += ['allocatable', 'dimension', 'intent', 'character', 'open']
        for word in words:

            # add to self
            self.code.append(word)

        return None

    def _parse(self, text):
        """Parse text into lines, combining those with &, and noting the tokens.

        Arguments:
            text: list of str

        Returns:
            list of tuples
        """

        # prune off comments
        text = self._silence(text)

        # make list of lines
        lines = {index + 1: {'line': line, 'tokens': self._tokenize(line)} for index, line in enumerate(text)}

        # prune lines of empties, and combine &s
        lines = self._prune(lines)

        return lines

    def _prune(self, lines):
        """Prune empty lines and combine extended lines.

        Arguments:
            lines: dict of lines by line number

        Returns:
            dict of lines
        """

        # begin deletions list
        deletions = []

        # get line numbers and reverse list
        numbers = list(lines.keys())
        numbers.sort(reverse=True)

        # for each number
        for number in numbers:

            # if the line is empty
            if len(lines[number]['line']) < 1:

                # add to deletions
                deletions.append(number)

            # otherwisee
            else:

                # if the line ends in &
                if lines[number]['line'][-1] == '&':

                    # add line below
                    lines[number]['line'].replace('&', lines[number + 1]['line'])

                    # add tokens from below and remove duplicates
                    lines[number]['tokens'] = self._skim(lines[number]['tokens'] + lines[number + 1]['tokens'])

                    # slate next line for deletion
                    deletions.append(number + 1)

        # remove all deletions
        prune = {index: entry for index, entry in lines.items() if index not in deletions}
        prune = {index: entry for index, entry in prune.items() if len(entry['tokens']) > 0}

        return prune

    def _report(self, tree, tab=0):
        """Attach an entry to the variable report.

        Arguments:
            tree: dict
            tab: int level of tabs

        Returns:
            list of str
        """

        # set report
        report = []

        # for each key in tree
        names = list(tree.keys())
        names = [name for name in names if name != self.line]
        names.sort()
        for name in names:

            # add line and advance tab
            line = '{}{}:'.format(tab * self.tab * ' ', name)
            report += [line]

            # for each number ( going backwards )
            numbers = list(tree[name].keys())
            numbers.sort(reverse=True)
            for number in numbers:

                # print the line
                line = '{}{}: {}'.format((tab + 1) * self.tab * ' ', number, tree[name][number][self.line])
                report += [line]

                # get variables
                report += self._report(tree[name][number], tab + 2)

        return report

    def _silence(self, text):
        """Remove comments from text.

        Arguments:
            text: list of str

        Returns:
            list of str
        """

        # begin silence
        silence = []

        # for each line in the text
        for line in text:

            # if comment
            if '!' in line:

                # find the index
                index = line.index('!')
                line = line[:index]
                silence.append(line.strip())

            # otherwise
            else:

                # just append
                silence.append(line.strip())

        return silence

    def _tokenize(self, line):
        """Break a line apart into individual variable names.

        Arguments:
            line: str, line of code

        Returns:
            list of str, the tokens
        """

        # set symbols
        symbols = (',', ' ', '=', '-', '!', '+', '(', ')', '[', ']', '*', '&', '.', ':', '/')

        # begin tokens with line
        tokens = [line]
        for symbol in symbols:

            # copy all portions
            fragments = []
            for token in tokens:

                # split at symbol and add to fragments
                fragment = token.split(symbol)
                fragments += fragment

            # recopy to portions
            tokens = fragments

        # weed out empties and code words, and duplicates
        tokens = [token for token in tokens if len(token) > 0]
        tokens = [token for token in tokens if token not in self.code]
        tokens = [token for token in tokens if not token.isdigit()]
        tokens = self._skim(tokens)

        return tokens

    def spelunk(self, variable, source):
        """Trace all usage of a variable in a fortran program.

        Arguments:
            variable: str, variable name of interest
            source: str, file name of interest

        Returns:
            None
        """

        # create destination
        destination = '{}/spelunk_{}_{}.txt'.format(self.sink, source.replace('_', '').replace('.', ''), variable)

        # grab all text from the file
        text = self._know('{}/{}'.format(self.source, source))

        # parse into lines and tokens
        lines = self._parse(text)

        # make record
        last = max(list(lines.keys()))
        crawl = {variable: self._crawl(variable, lines, last)}

        # create report from crawl
        report = []
        report += self._report(crawl)

        # jot down
        self._jot(report, destination)

        # also pretty print
        destination = destination.replace('.txt', '_prttu.txt')
        self._look(crawl, 20, destination)

        return None
