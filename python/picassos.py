# picassos.py for the Ozonator class to provide a loose class for exploring OMI data

# import general tools
import os
import sys
import json
import csv
import re

# import datetime
from datetime import datetime, timedelta
from time import time

# evaluate strings
from ast import literal_eval

# import tools
from pprint import pprint
from collections import Counter
from copy import copy, deepcopy

# import math
import numpy
from numpy.random import random
from math import sin, cos, pi, log10, exp, floor, ceil, sqrt

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import gridspec
from matplotlib import colorbar
from matplotlib import colors as Colors
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib import path as Path
# from mpl_toolkits.mplot3d import Axes3D
# from mpl_toolkits.basemap import Basemap

Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# import h5py to read h5 files
import h5py

# import regex for string matching
from re import search

# import fuzzywuzzy for fuzzy matches
from fuzzywuzzy import fuzz

# import counter
from collections import Counter

# import random forest and regression
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans, MeanShift
from sklearn.decomposition import PCA, FastICA


# pdf maker
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter

# import xarray
import xarray
xarray.set_options(keep_attrs=True)

# import modules
from cores import Core
from prisms import Prism
from plates import Plate
from hydras import Hydra


# class Picasso to do OMI data mining
class Picasso(Hydra):
    """Picasso class to do OMI data mining.

    Inherits from:
        Core
    """

    def __init__(self, source, sink):
        """Initialize an Picasso instance.

        Arguments:
            None

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, source)

        # set sink
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Picasso instance at: {} >'.format(self.source)

        return representation

    def _augment(self, path, latitude):
        """Augment the graph with a strip of map at appropriate latitude.

        Arguments:
            path: path of imagee
            latitude: float: the latitude

        Returns:
            None
        """

        # open up the world plot
        world = Image.open('world.png')

        # resize to colorbar size
        width = 1565
        world = world.resize((width, int(world.height * (world.width / width))))

        # translate to array
        world = numpy.array(world)

        # set strip thickness
        thickness = 40

        # add margins
        margin = numpy.full((thickness, width, 4), 255)
        world = numpy.concatenate([margin, world, margin], axis=0)

        # calculate the area of the particular latitude
        height = world.shape[0] - (2 * thickness)
        pixel = int((height / 180) * (90 - latitude)) + thickness
        strip = world[pixel - thickness: pixel + thickness, :, :]

        # open up the plot
        plot = Image.open(path)
        plot = numpy.array(plot)

        # add strip to plot
        top = 795
        left = 240
        plot[top: top + strip.shape[0], left: left + strip.shape[1], :] = strip
        Image.fromarray(plot).save(path)

        return None

    def _delineate(self, vectors, indices):
        """Find the first three vectors that are not collinear.

        Arguments:
            vectors: list of list of floats
            indices: list of int, ordering by distance

        Returns:
            list of list of floats
        """

        # get the first two vectors
        points = [vectors[indices[0]], vectors[indices[1]]]

        # go through remaining vectors
        for index in indices[2:]:

            # get unique latitudes and longitudes
            latitudes = [points[0][0], points[1][0], vectors[index][0]]
            longitudes = [points[0][1], points[1][1], vectors[index][1]]

            # check for unique coordinates with third
            if len(set(latitudes)) > 1 and len(set(longitudes)) > 1:

                # keep point and stop looking
                points += [vectors[index]]
                break

        return points

    def _envision(self, matrix, destination, title='', units='', dependent='', independent='', spectrum=None, coordinates=None):
        """Envision a matrix as a heat map.

        Arguments:
            matrix: 2-D numpy array
            destination: file path for saving image
            title='': str, figure title
            units='': str, figure units
            dependent='': str, figure yaxis label
            independent='': str, figure xaxis label
            coordinates: dict of lists

        Returns:
            None
        """

        # set default zoom
        zoom = (0, len(matrix[0]), 0, len(matrix))

        # make default labels
        independent = independent or 'horizontal gridpoint'
        dependent = dependent or 'vertical gridpoint'

        # set default coordinates
        if not coordinates:

            # default to indices
            coordinates = {independent: list(range(matrix.shape[1]))}
            coordinates.update({dependent: list(range(matrix.shape[0]))})

        # make xarry matrix
        matrix = xarray.DataArray(matrix, dims=[dependent, independent], coords=coordinates)
        matrix.attrs.update({'route': title, 'units': units})

        # make plate object and draw graph
        spectrum = spectrum or 'classic'
        plate = Plate(matrix, ['vertical', 'horizontal'], zoom, title, spectrum=spectrum)
        plate.glance()

        # resave glance image to destination
        image = Image.open('../plots/glance.png')
        image.save(destination)

        return None

    def _imagine(self, latitude, longitude, one, two, three):
        """Interpolate a datum based on the datum at surrounding coordinates.

        Arguments:
            latitude: float
            longitude: floatt
            one: list of float, first vector
            two: list of float, second vector
            three: list of float, third vector

        Returns:
            float
        """

        # calculate the two - one difference and three - one difference
        vector = [two[index] - one[index] for index in range(3)]
        vectorii = [three[index] - one[index] for index in range(3)]

        # calculate the cross product from the determinant i (vc - wb) + j (wa - uc) + k (uc - wa)
        cross = [(vector[1] * vectorii[2]) - (vector[2] * vectorii[1])]
        cross += [(vector[2] * vectorii[0]) - (vector[0] * vectorii[2])]
        cross += [(vector[0] * vectorii[1]) - (vector[1] * vectorii[0])]

        # determine constant from cross product coefficients and one point
        constant = sum([coefficient * coordinate for coefficient, coordinate in zip(cross, one)])

        #  calculate the datum z = (d - ax - by) / c
        datum = (constant - cross[0] * latitude - cross[1] * longitude) / cross[2]

        return datum

    def _patch(self, matrix, fill):
        """Replace fill values with the mean of all real, non fill values.

        Arguments:
            matrix: numpy aray
            fill: fill value

        Returns:
            numpy array
        """

        # replace all nonfinites with fill
        matrix = numpy.nan_to_num(matrix, posinf=fill, neginf=fill, nan=fill)

        # get a mask for fill values
        mask = matrix == fill
        mirror = matrix != fill

        # find the average of all nonfills
        mean = matrix[mirror].mean()

        # replace fill values with matrix
        matrix[mask] = mean

        return matrix

    def canvas(self):
        """Project a flat file with a cubesphere file and compare.

        Arguments:
            None

        Returns:
            None
        """

        # ingest both files
        self._stamp('ingesting files...', initial=True)
        [self.ingest(path, discard=False) for path in self.paths]

        # collect cubesphere latitudes and longitudes
        latitudes = self.dig('lats')[0].distil().squeeze()
        longitudes = self.dig('lons')[0].distil().squeeze()

        # collect plane grid
        norths = self.dig('lat')[0].distil().squeeze()
        easts = self.dig('lon')[0].distil().squeeze()

        # convert east coordinatas to 360 system
        easts = easts + 360 * (easts < 0)

        # group all features by names, excluding singleets
        features = list(self)
        pairs = self._group(features, lambda feature: feature.name)
        pairs = {name: pair for name, pair in pairs.items() if len(pair) == 2}

        # for each feature pair
        self._stamp('checking pairs...')
        for name, pair in pairs.items():

            # sort the pair by shape, assuming first is cubic and second is planar
            pair.sort(key=lambda feature: feature.shape)
            cubic, planar = pair

            # if dimensions check out
            if cubic.shape == (1, 6, 180, 180) and planar.shape == (1, 361, 576):

                # limiting to a subset of tags:
                if any([tag in pair[0].name for tag in ('TS',)]):

                    # project the planar data onto cubic coordinates
                    self._stamp('projecting {}...'.format(planar.name))
                    data = planar.distil().squeeze()
                    portrait = self.impasto(data, norths, easts, latitudes, longitudes)

                    # get original data
                    original = cubic.distil().squeeze()

                    # compute the comparisonn
                    comparison = (portrait - original)
                    percent = (100 / original.mean()) * comparison

                    # create plots
                    self._stamp('plotting {}...'.format(planar.name))
                    for face in range(6):

                        # print the original
                        label = planar.attributes['long_name'].decode('utf8')
                        title = 'original cubesphere for {}, {}, face {}'.format(planar.name, label, face)
                        dependent = 'degrees'
                        independent = 'degrees'
                        units = planar.attributes.get('Units', '_')
                        destination = '{}/canvas/{}_original_{}.png'.format(self.sink, planar.name, face)
                        self._print(destination)
                        self._envision(original[face], destination, title, units, dependent, independent, 'hot')

                        # print the projection
                        title = 'projected cubesphere for {}, {}, face {}'.format(planar.name, label, face)
                        destination = '{}/canvas/{}_projection_{}.png'.format(self.sink, planar.name, face)
                        self._print(destination)
                        self._envision(portrait[face], destination, title, units, dependent, independent, 'hot')

                        # print the comparison
                        title = 'comparison for {}, {}, face {}'.format(planar.name, label, face)
                        units = '% difference projection - original'
                        destination = '{}/canvas/{}_difference_{}.png'.format(self.sink, planar.name, face)
                        self._print(destination)
                        self._envision(comparison[face], destination, title, units, dependent, independent, 'hot')

        # finish
        self._stamp('finished.')

        return None

    def impasto(self, data, norths, easts, latitudes, longitudes):
        """Construct a cubic projection from planar data using interpolationn.

        Arugments:
            data: numpy array of data
            norths: numpy array, planar northward coordinates
            eests: numpy array, planar eastward coordinates
            latitudes: numpy array, cubic latitudes
            longitudes: numpy array, cubic longitudes

        Returns:
            numpy array
        """

        # expand norths and easts
        shape = data.shape
        easts = numpy.vstack([easts] * shape[0])
        norths = numpy.vstack([norths] * shape[1]).transpose(1, 0)

        # zip all together
        zipper = list(zip(norths.ravel(), easts.ravel(), data.ravel()))

        # construct an empty cube
        portrait = numpy.zeros((6, 180, 180))

        # for each face
        for face in range(6):

            self._print('face {}...'.format(face))

            # for each row
            for row in range(180):

                self._print('row: {}'.format(row))

                # and each column
                for column in range(180):

                    # get the target latitude and longitude
                    latitude = latitudes[face][row][column]
                    longitude = longitudes[face][row][column]

                    # compute distances from target for all grid points
                    distances = [(latitude - north) ** 2 + (longitude - east) ** 2 for north, east, _ in zipper]
                    indices = numpy.argsort(distances)

                    # get the first three points that are not collinear
                    points = self._delineate(zipper, indices)

                    # determine the interpolated datum
                    datum = self._imagine(latitude, longitude, *points)

                    # add to portrait
                    portrait[face][row][column] = datum

        return portrait

    def profile(self, field):
        """Compare two vertical temperature difference distribution.

        Arguments:
            field: str, name of field to search for

        Returns:
            None
        """

        # clear current plots
        self._clean(self.sink)

        # collect latitudes, temperatures, longitudes from all files
        latitudes = {}
        longitudes = {}
        heights = {}
        measurements = {}
        description = ''
        for path in self.paths:

            # ingest the path
            self.ingest(path)

            # gather attributes
            latitudes[path] = self.dig('lat')[0].distil().squeeze()
            longitudes[path] = self.dig('lon')[0].distil().squeeze()
            heights[path] = self.dig('lev')[0].distil().squeeze()
            measurements[path] = self.dig(field)[0].distil().squeeze()
            description = self.dig(field)[0].attributes['long_name'].decode('utf8')

        # calculate differences and plot
        paths = self.paths

        # short path names to stubs
        stubs = [path.split('/')[-1] for path in paths]

        # calculate absolute relative difference
        matrixii = measurements[paths[1]]
        matrix = measurements[paths[0]]
        differences = 200 * (matrixii - matrix) / (abs(matrixii) + abs(matrix))

        # perform log transformation
        logarithms = numpy.log10(abs(differences) + 1) * numpy.sign(differences)

        # go through several latitude gridpoints
        for index, latitude in enumerate(latitudes[paths[0]]):

            # for particular layers
            if latitude % 5 == 0:

                # print
                self._print('latitude {}...'.format(latitude))

                # get the matrix and flip layer
                matrix = logarithms[:, index, :]
                matrix = numpy.flip(matrix, axis=0)

                # create destination
                destination = '{}/{}_deltas_{}.png'.format(self.sink, field, str(361 - index).zfill(3))

                # set up graph
                title = '{}\n - {}\n {} ({}), latitude: {} degrees north'.format(stubs[1], stubs[0], description, field, latitude)
                units = 'symlog relative percent difference [ +/- log(|RPD| + 1) ]'
                dependent = 'layer'
                independent = 'longitude'

                # create coordinates
                layers = list(range(72))
                layers.reverse()
                coordinates = {dependent: layers}
                coordinates.update({independent: longitudes[paths[0]].tolist()})

                # make plot
                arguments = {'spectrum': 'rainbow', 'coordinates': coordinates}
                self._envision(matrix, destination, title, units, dependent, independent, **arguments)

                # augment the plot with a map strip
                self._augment(destination, latitude)

        # finish
        self._stamp('finished.')

        return None

    def temper(self):
        """Project a flat file with a cubesphere file and compare.

        Arguments:
            None

        Returns:
            None
        """

        # clear current plots
        self._clean(self.sink)

        # collect latitudes, temperatures, longitudes from all files
        latitudes = {}
        longitudes = {}
        heights = {}
        temperatures = {}
        for path in self.paths:

            # ingest the path
            self.ingest(path)

            # gather attributes
            latitudes[path] = self.dig('lat')[0].distil().squeeze()
            longitudes[path] = self.dig('lon')[0].distil().squeeze()
            heights[path] = self.dig('lev')[0].distil().squeeze()
            temperatures[path] = self.dig('T')[0].distil().squeeze()

        # calculate differences and plot
        paths = self.paths

        # short path names to stubs
        stubs = [path.split('/')[-1] for path in paths]

        # calculate differences
        differences = temperatures[paths[1]] - temperatures[paths[0]]

        # go through several layers and print differences
        for layer in range(72):

            # for particular layers
            if layer % 2 == 0:

                # print
                self._print('layer {}...'.format(layer))

                # get the matrix
                matrix = differences[layer]

                # create destination
                destination = '{}/temperature_deltas_{}.png'.format(self.sink, str(layer).zfill(2))

                # set up graph
                title = '{}\n - {}\n Air Temperature, layer: {}'.format(stubs[1], stubs[0], layer)
                units = 'difference (degrees K)'
                dependent = 'latitude grid point'
                independent = 'longitude grid point'

                # make plot
                self._envision(matrix, destination, title, units, dependent, independent, 'hot')

        # finish
        self._stamp('finished.')

        return None

    def splatter(self, *comparisons):
        """Compare two files on several parameters.

        Arguments:
            *comparisons: unpacked list of fields for comparisons

        Returns:
            None
        """

        # clear current plots
        self._clean(self.sink)

        # set particular parameters for comparing
        paths = self.paths
        for comparison in comparisons:

            # print comparisons
            self._print('comparing: \n{}\n{}\n{}'.format(paths[0], paths[1], comparison))

            # ingest first path
            self.ingest(paths[0])

            # get the first path data
            feature = self.dig(comparison)[0]
            fill = float(feature.attributes['_FillValue'])
            matrix = feature.distil().squeeze()
            matrix = self._patch(matrix, fill)

            # get the units and description
            units = feature.attributes['units'].decode('utf8')
            description = feature.attributes['long_name'].decode('utf8')

            # set independent and dependent
            dependent = 'latitude grid point'
            independent = 'longitude grid point'

            # create destination, title, and plot
            stub = paths[0].split('/')[-1]
            destination = '{}/{}_a.png'.format(self.sink, comparison)
            title = '{}\n {} ({})'.format(stub, comparison, description)
            self._envision(matrix, destination, title, units, dependent, independent, 'hot')

            # ingest second path
            self.ingest(paths[1])

            # get the first path data
            feature = self.dig(comparison)[0]
            fill = float(feature.attributes['_FillValue'])
            matrixii = feature.distil().squeeze()
            matrixii = self._patch(matrixii, fill)

            # create destination, title, and plot
            stubii = paths[1].split('/')[-1]
            destination = '{}/{}_b.png'.format(self.sink, comparison)
            title = '{}\n {} ({})'.format(stubii, comparison, description)
            self._envision(matrixii, destination, title, units, dependent, independent, 'hot')

            # calculate absolute relative difference
            difference = 200 * (matrixii - matrix) / (abs(matrixii) + abs(matrix))

            # plot
            units = '% difference'
            destination = '{}/{}_diff.png'.format(self.sink, comparison)
            title = '{}\n - {}\n {} difference ({})'.format(stubii, stub, comparison, description)
            self._envision(difference, destination, title, units, dependent, independent, 'hot')
        #
        #
        #
        #
        # # collect latitudes, temperatures, longitudes from all files
        # latitudes = {}
        # longitudes = {}
        # heights = {}
        # temperatures = {}
        # for path in self.paths:
        #
        #     # ingest the path
        #     self.ingest(path)
        #
        #     # gather attributes
        #     latitudes[path] = self.dig('lat')[0].distil().squeeze()
        #     longitudes[path] = self.dig('lon')[0].distil().squeeze()
        #     heights[path] = self.dig('lev')[0].distil().squeeze()
        #     temperatures[path] = self.dig('T')[0].distil().squeeze()
        #
        # # calculate differences and plot
        # paths = self.paths
        #
        # # short path names to stubs
        # stubs = [path.split('/')[-1] for path in paths]
        #
        # # calculate differences
        # differences = temperatures[paths[1]] - temperatures[paths[0]]
        #
        # # go through several layers and print differences
        # for layer in range(72):
        #
        #     # for particular layers
        #     if layer % 2 == 0:
        #
        #         # print
        #         self._print('layer {}...'.format(layer))
        #
        #         # get the matrix
        #         matrix = differences[layer]
        #
        #         # create destination
        #         destination = '{}/temperature_deltas_{}.png'.format(self.sink, str(layer).zfill(2))
        #
        #         # set up graph
        #         title = '{}\n - {}\n Air Temperature, layer: {}'.format(stubs[1], stubs[0], layer)
        #         units = 'difference (degrees K)'
        #         dependent = 'latitude grid point'
        #         independent = 'longitude grid point'
        #
        #         # make plot
        #         self._envision(matrix, destination, title, units, dependent, independent, 'hot')

        # finish
        self._stamp('finished.')

        return None
