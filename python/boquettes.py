# boquettes.py for Boquette class to view OMI hdf data using bokeh

# import local classes
from cores import Core
from hydras import Hydra
from blooms import Bloom
from plates import Plate

# import sys
import os
import sys
import subprocess

# import datetime
import datetime

# import itertools and collections
import itertools
from collections import Counter

# import math and numpy
import math
import numpy

# import sleep
from time import sleep

# import xarray
import xarray
xarray.set_options(keep_attrs=True)

# import json
import json

# import regex, string
import re
import string

# import math
import math

# import garbage collection
import gc

# import pillow for creating icons
from PIL import Image, ImageDraw

# import Html2Image for sending plots to pdf
from html2image import Html2Image

# import web service for modis backgrounds
from owslib.wmts import WebMapTileService
from io import BytesIO

# import bokeh ( version 2.3.0 )
from bokeh.events import Tap, ButtonClick, MenuItemClick
# from bokeh.io import export_png, export_svg
from bokeh.io import export_png
from bokeh.io.export import get_screenshot_as_png
from bokeh.layouts import row as Row, column as Column
from bokeh.models import HoverTool, ColumnDataSource, CheckboxGroup, DatetimeTickFormatter
from bokeh.models import Paragraph, Button, LinearAxis, Range1d, Legend, PrintfTickFormatter
from bokeh.models import CustomAction, UndoTool, ColorPicker, TextInput, PointDrawTool
from bokeh.models import ColorBar, LogColorMapper, LogTicker, FixedTicker, ColorMapper, LinearColorMapper
from bokeh.models.callbacks import CustomJS
from bokeh.plotting import figure as Figure, curdoc as Curdoc, output_file, save as Save
from bokeh.resources import CDN

# import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.svm import OneClassSVM, SVC
from sklearn.decomposition import PCA

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib import ticker
from matplotlib.colors import ListedColormap
from matplotlib.ticker import StrMethodFormatter, NullFormatter
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False

# set up chrome driver for propagation
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
# driver = webdriver.Chrome(ChromeDriverManager().install())


# class Boquette to contain the web app
class Boquette(Core):
    """Class Boquettte to contain all bokeh plots in the bokeh app.

    Inherits from:
        Core
    """

    def __init__(self, ticket, source='', dimension=''):
        """Instantiate the Boquette instance.

        Arguments:
            ticket: str, yaml file name
            source: str, file path for source
            dimension: int, number of dimensions

        Returns:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # set yaml ticket
        self.ticket = ticket
        self.yam = {}

        # allocate for source folder of hdf files
        self.source = source

        # allocate for album of scenes
        self.album = None

        # set default dimensional expansion
        self.dimension = int(dimension or 1)

        # set default graph labels
        self.title = None
        self.ordinate = None
        self.abscissa = None

        # set default line labels and colors
        self.labels = None
        self.colors = None

        # establish custom tool options
        self.outliers = None
        self.normalizations = None
        self.corners = None
        self.pen = None
        self.styles = None
        self.gauges = None
        self.spectra = None
        self.fonts = None
        self.coaxes = None

        # squeeze out palettes and hexadecimal color values
        self.palette = {}
        self.hexes = {}

        # set propagation
        self.propagation = None

        # set background images
        self.scenery = {}

        # set default datatype
        self.type = '<f4'

        # set machine learning parameters
        self.window = 11
        self.fraction = 0.8
        self.fill = -9999

        # set up geodes
        self.brackets = {}
        self.geodes = {}

        # allocate for hydra instance and its paths
        self.hydra = None
        self.paths = None

        # ingest data
        self.variables = {}
        self.manifestations = {}
        self.abbreviations = {}
        self.reference = {}

        # build the nest
        self.nest = {}
        self.diagram = {}
        self.twigs = {}
        self.nebula = {}
        self.stems = {}
        self.cloud = {}
        self.storm = {}
        self.drawers = {}

        # begin the page
        self.page = None
        self.boxes = {}

        # create the instance
        self._create()

        # default chrome to None
        self.chromium = None
        self.options = None

        return

    def __repr__(self):
        """Create string representation.

        Arguments:
            None

        Returns:
            str
        """

        # create designation
        designation = '< Boquette instance at: {} >'.format(self.source)

        return designation

    def _abbreviate(self):
        """Make abbreviations for all paths.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.abbreviations
        """

        # sort paths
        paths = self.hydra.paths
        paths.sort()

        # for each path
        abbreviations = {}
        for index, path in enumerate(paths):

            # get the file name and extension
            name = path.split('/')[-1].split('.')[0]
            extension = path.split('.')[-1]

            # split at _
            words = name.split('_')
            shells = [word[0] + word[int(len(word) / 2)] + word[-1] for word in words]
            tags = [shell if len(shell) < len(word) else word for shell, word in zip(shells, words)]
            alias = '_'.join(tags)

            # construct abbreviation
            formats = (str(index).zfill(2), alias, extension)
            abbreviation = '({}) {}.{}'.format(*formats)
            abbreviations[path] = abbreviation

        # set attribute
        self.abbreviations = abbreviations

        return None

    def _accentuate(self, graph, reservoir):
        """Add the secondary axis to the graph.

        Arguments:
            graph: bokeh figure
            reservoir: dict

        Returns:
            None
        """

        # get variables from reservoir
        variables = reservoir['variables']

        # construct information for twin secondary axis
        start = graph.y_range.start
        finish = graph.y_range.end
        label = graph.yaxis.axis_label
        axes = {'twin': {'label': label, 'start': start, 'finish': finish}}

        # construct information for duel secondary axis
        label = graph.yaxis.axis_label
        axes.update({'duel': {'label': label, 'start': start, 'finish': finish}})

        # check for second line
        if len(reservoir['data'].keys()) > 1:

            print(list(reservoir['data'].keys()))

            # get index
            index = list(reservoir['data'].keys())[-1]

            # get min and max
            minimum = min(reservoir['data'][index])
            maximum = max(reservoir['data'][index])

            # calculate extend
            extent = maximum - minimum
            margin = extent * 0.05

            # add margin
            minimum = minimum - margin
            maximum = maximum + margin

            # update secondary axis
            axes.update({'duel': {'label': label, 'start': minimum, 'finish': maximum}})

        # construct entry for percents
        middle = (graph.y_range.start + graph.y_range.end) / 2 or 1
        start = 100 * (start - middle) / middle
        finish = 100 * (finish - middle) / middle
        label = 'percent ( % )'
        axes.update({'percent': {'label': label, 'start': start, 'finish': finish}})

        # construct entry for seasons
        label = 'earth sun distance ( au )'
        axes.update({'season': {'label': label, 'start': 0.97, 'finish': 1.03}})

        # construct entry for azimuths
        label = 'solar azimuth angle ( degrees )'
        axes.update({'azimuth': {'label': label, 'start': 15, 'finish': 35}})

        # construct entry for longitude
        label = 'longitude ( degrees east )'
        axes.update({'longitude': {'label': label, 'start': -200, 'finish': 200}})

        # construct entry for track length
        label = 'track length ( images )'
        axes.update({'track': {'label': label, 'start': 800, 'finish': 1400}})

        # set default range
        coaxis = self.coaxes[0]
        start = axes[coaxis]['start']
        finish = axes[coaxis]['finish']
        label = axes[coaxis]['label']
        graph.extra_y_ranges['cycle'] = Range1d(start=start, end=finish)
        secondary = LinearAxis(y_range_name='cycle', axis_label=label)

        # add axis
        if self.yam.get('axis', True):

            # add axis
            graph.add_layout(secondary, 'right')

        # merge all ordinates
        ordinates = []
        stage = {}
        for source in variables.values():

            # note status
            self._stamp('source...', initial=True)

            # try to
            times = []
            self._stamp('getting times...')
            try:

                # get time values
                times = source['time']

            # unless not in source
            except KeyError:

                # nevermind
                pass

            # define set of secondary axes
            self._stamp('setting coaxes...')
            fields = {'twin': '_', 'duel': '_', 'percent': '_', 'season': 'orbit_earth_sun_distance'}
            fields.update({'azimuth': 'solar_azimuth_angle', 'longitude': 'orbit_longitude'})
            fields.update({'track': 'orbit_track_length'})
            swatches = []
            for coaxis in self.coaxes:

                # create faux and search in source
                faux = ['NaN'] * len(times)
                field = fields[coaxis]
                swatches.append(source.get(field, faux))

            # update ordinates
            self._stamp('updating ordinates....')
            zipper = zip(times, *swatches)
            [ordinates.append(list(member)) for member in zipper]

        # sort ordinates
        ordinates.sort(key=lambda ordinate: ordinate.count('NaN'), reverse=True)

        # remove duplicates
        singlets = {ordinate[0]: ordinate for ordinate in ordinates}
        singlets = list(singlets.values())
        singlets.sort(key=lambda ordinate: ordinate[0])

        # # extract ordinates
        # tags = ['times'] + self.coaxes
        # ordinates = {tag: [singlet[index] for singlet in singlets] for index, tag in enumerate(tags)}

        # collect data
        times = [singlet[0] for singlet in singlets]
        for index, coaxis in enumerate(self.coaxes):

            # set visible flag for first index
            visible = (index == 0)

            # create lines and add to stage
            ordinate = [singlet[index + 1] for singlet in singlets]
            line = graph.line(x=times, y=ordinate, color='gray', line_width=1.0, y_range_name='cycle', visible=visible)
            stage[coaxis] = line

        # get track start and end
        margins = self._marginalize(stage['track'].data_source.data['y'])
        axes['track']['start'] = margins[0]
        axes['track']['finish'] = margins[1]

        # update the stage
        stage.update({'secondary': secondary, 'axes': axes})

        return stage

    def _admix(self, spectrum, number, quantities=None, brackets=None):
        """Mix colors according to selected palette.

        Arguments:
            spectrum: str, name of spectra, or list of hexes
            number: int, number of lines
            quantities: list of values to use for mixing
            brackets: list of floats, the endpoints for the colors

        Returns:
            list of str
        """

        # try
        try:

            # gather all hex values from spectrum
            colors = [self.hexes[color] for color in self.palette[spectrum]]

        # unless spectrum is already a list
        except TypeError:

            # set colors to the list
            colors = spectrum

        # if not enough colors
        if len(colors) < number:

            # begin interpolations
            interpolations = []

            # determine step size
            step = (len(colors) - 1) / (number - 1)
            indices = list(range(number))

            # for each index
            for index in indices:

                # calculate ratio and bracket points
                ratio = step * index
                first = math.floor(ratio)
                fraction = ratio - first

                # get initial color of bracket
                initial = colors[first]

                # check for zero fraction
                if fraction == 0:

                    # append initial
                    interpolations.append(initial)

                # otherwise
                else:

                    # get interpolation
                    final = colors[first + 1]
                    hexadecimal = self._curse(initial, final, fraction)

                    # append interplation
                    interpolations.append(hexadecimal)

            # set colors to interpolations
            colors = interpolations

        # if quantites are given
        if quantities is not None:

            # begin interpolations
            interpolations = []

            # create new brackets from old brackets, by fitting to one less bracket
            minimum = brackets[0][0]
            penultimate = brackets[-1][0]
            ultimate = brackets[-1][1]
            growth = (ultimate - minimum) / (penultimate - minimum)
            bracketsii = []
            for bracket in brackets:

                # get enlarged bracket
                bracketii = [minimum + (bracket[0] - minimum) * growth, minimum + (bracket[1] - minimum) * growth]
                bracketsii.append(bracketii)

            # get bounds
            bounds = [bracket[0] for bracket in bracketsii]
            for quantity in quantities:

                # set default color as white
                interpolation = '#ffffff'

                # check each bound
                for index, bound in enumerate(bounds[:-1]):

                    # if included
                    if float(quantity) >= bounds[index] and float(quantity) <= bounds[index + 1]:

                        # interpolate
                        initial = spectrum[index]
                        final = spectrum[index + 1]
                        fraction = (float(quantity) - bounds[index]) / (bounds[index + 1] - bounds[index])

                        # get interpolation
                        interpolation = self._curse(initial, final, fraction)

                # append interpolation
                interpolations.append(interpolation)

            # set colors to interpolations
            colors = interpolations

        return colors

    def _anchor(self):
        """Construct javascript code for adjusting outlier setting.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # log to screen
        java += """console.log("anchoring...");"""

        # advance anchor dial
        java += self._dial('anchor')
        java += """var outliers = advance;"""

        # get normalization setting
        java += self._dial('scales', advance=False)
        java += """var normalization = status;"""

        # tighten fit
        java += self._tighten()

        return java

    def _annotate(self, graph, reservoir, sources, trends, tools=True):
        """Add annotations to graph.

        Arguments:
            graph: bokeh figure
            reservoir: dict
            sources: dict of column source objects
            trends: dicts of column source objects
            tools: keep toolbar visible?

        Returns:
            None
        """

        # get names and labels
        names = reservoir['blooms']

        # if hover tool needed
        if self.yam.get('hover', True):

            # create annotations
            for name in names:

                # add annotations
                annotations = [(label, '@{' + label + '}') for label in sources[name].data.keys()]

                # add hover tool
                hover = HoverTool(tooltips=annotations, names=[name])
                graph.add_tools(hover)

                # add trend annotations
                annotations = [(label, '@{' + label + '}') for label in trends[name].data.keys()]

                # add hover tool
                hover = HoverTool(tooltips=annotations, names=[name + '_trend'])
                graph.add_tools(hover)

        # set axis names and font sizes
        graph.xaxis.axis_label_text_font_size = self.fonts[0]
        graph.yaxis.axis_label_text_font_size = self.fonts[0]
        graph.xaxis.major_label_text_font_size = self.fonts[0]
        graph.yaxis.major_label_text_font_size = self.fonts[0]
        graph.title.text_font_size = self.fonts[0]

        # if secondary axis
        if self.yam.get('axis', True):

            # set axis names
            graph.yaxis[1].axis_label_text_font_size = self.fonts[0]
            graph.yaxis[1].major_label_text_font_size = self.fonts[0]

        # maybe remove tools
        if not tools:

            # remove tools
            graph.toolbar_location = None

        return None

    def _bracket(self, percentiles):
        """Create brackets and texts for each set of percentiles.

        Arguments:
            percentiles: list of float

        Returns:
            (list of tuples, list of str) tuple
        """

        # determine brackets
        brackets = [(left, right) for left, right in zip(percentiles[:-1], percentiles[1:])]

        # strip all exponents
        exponents = [math.floor(math.log10(abs(entry + 1e-20))) for entry in percentiles]
        mantissas = [entry * 10 ** -power for entry, power in zip(percentiles, exponents)]

        # round all mantissas
        mantissas = [round(entry, 1) for entry in mantissas]

        # change exponents to texts, padding with zeros
        signs = ['-' if exponent < 0 else '+' for exponent in exponents]
        digits = len(str(max([abs(exponent) for exponent in exponents])))
        exponents = [sign + ('0' * digits + str(abs(exponent)))[-digits:] for exponent, sign in zip(exponents, signs)]

        # change mantissas to texts
        signs = ['-' if mantissa < 0 else '+' for mantissa in mantissas]
        mantissas = [sign + str(abs(mantissa)) for mantissa, sign in zip(mantissas, signs)]

        # combine exponents and mantissas
        combinations = ['({}e{})'.format(mantissa, exponent) for mantissa, exponent in zip(mantissas, exponents)]

        # make texts
        texts = ['_{}-{}'.format(left, right) for left, right in zip(combinations[:-1], combinations[1:])]
        texts = ['{}{}'.format(letter, text) for letter, text in zip('ABCDEFGHIJKLMNOP', texts)]

        return brackets, texts

    def _branch(self, blooms, index=0):
        """Create a nesting map of features.

        Arguments:
            blooms: list of bloom instances
            index: int, current index

        Returns:
            None
        """

        # begin nest
        nest = {}

        # collect all routes that are long enough
        routes = [bloom.bunch[:-1] + [bloom.name] for bloom in blooms]
        routes = [route for route in routes if len(route) > index]

        # get all singlet routes
        singlets = [route for route in routes if len(route) == index + 1]

        # get route members members
        members = self._skim([route[index] for route in routes])

        # check for all singlets
        if len(singlets) == len(routes):

            # update all members
            nest.update({member: {} for member in members})

        # otherwise
        else:

            # update nest
            for member in members:

                # subset the features
                subset = [bloom for bloom in blooms if bloom.bunch[index] == member]
                nest.update({member: self._branch(subset, index + 1)})

        return nest

    def _build(self):
        """Build the nest.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.nest
            self.diagram
        """

        # start timing
        self._stamp('building nests...', initial=True)

        # attempt to group or split features
        self._stamp('bundling...')
        categories = self.hydra.dig('Categories')
        bundles = self._bundle(categories)

        # populate
        self._stamp('populating...')
        for bundle in bundles:

            # populate
            [self.append(bloom) for bloom in bundle]

        # update the index for each bloom in self
        [bloom.update({'index': index}) for index, bloom in enumerate(self)]

        # register stem names
        self._enroll()

        # designate feature names
        self._designate()

        # create nest
        self._stamp('branching...')
        nests = [{'Data': self._branch([bundle[0]])} for bundle in bundles]

        # entangle all nests
        nest = self._entangle(nests)
        self.nest = nest

        # make checkbox diagram
        self._stamp('diagramming...')
        diagram = self._graft(nest)

        # renew alphabetically
        files = list(diagram.keys())
        files.sort()
        diagram = {key: diagram[key] for key in files}
        self.diagram = diagram

        # strip off all forks
        self._stamp('plucking...')
        twigs = self._pluck(diagram)
        self.twigs = twigs

        # status
        self._stamp('nests built.')

        return None

    def _bundle(self, features):
        """Bundle up mean, max groups into singular categories, or create bundles from dimensional grouping

        Arguments:
            features: list of dicts

        Returns:
            None

        Populates:
            self
        """

        # begin bundles
        bundles = []

        # set standard tags
        tags = ('mean', 'max', 'min', 'median', 'std')

        # sort features by dimensions
        features.sort(key=lambda feature: len(feature.shape))

        # for each feature
        for feature in features:

            # try to
            try:

                # print feature
                print('bundling {} {}...'.format(feature.name, feature.shape))

                # collect independent variables
                independents = []

                # for every abscissa shape
                for length, variables in self.variables[feature.path].items():

                    # for all variable names:
                    for variable in variables.keys():

                        # filter out excess time info
                        if 'orbit_start' not in variable:

                            # add abscissa
                            independent = (variable, length)
                            independents.append(independent)

                # sort independents
                # independents.sort(key=lambda independent: len(independent[0]))
                # independents.sort(key=lambda independent: feature.name in independent[0], reverse=True)
                independents.sort(key=lambda independent: feature.name.split('_')[-1] in independent[0], reverse=True)
                #independents.sort(key=lambda independent: 'number' in independent[0], reverse=True)
                independents.sort(key=lambda independent: 'time' in independent[0], reverse=True)
                independents.sort(key=lambda independent: 'index' in independent[0], reverse=True)
                independents.sort(key=lambda independent: independent[1] in feature.shape, reverse=True)
                independents.sort(key=lambda independent: independent[1] == feature.shape[0], reverse=True)

                # self._tell(independents)

                # get abscissas
                size = 0
                if len(independents) > 0:

                    # get size
                    size = independents[0][1]
                    abscissas = [abscissa for abscissa, length in independents if size == length]

                # get abbreviation from path
                abbreviation = self.abbreviations[feature.path]

                # check for any of the standard statistical reductions, assuming 1-D
                def tagging(tag): return tag.capitalize() in feature.name or '_{}_'.format(tag) in feature.name
                if any([tagging(tag) for tag in tags]):

                    # create bundle following standard omps form
                    bundle = self._stack(feature, tags, abscissas, abbreviation)

                # otherwise, check for geodes
                elif 'geode' in feature.name:

                    # create bundle basd on geodes
                    bundle = self._track(feature, abbreviation)

                # otherwise, check for heatmaps
                elif 'heatmap' in feature.name:

                    # create bundle basd on geodes
                    bundle = self._wrack(feature, abbreviation)

                # otherwise, check for heatmaps
                elif 'histogram' in feature.name:

                    # create bundle basd on geodes
                    bundle = self._rack(feature, abbreviation)

                # otherwise, check for heatmaps
                elif 'network' in feature.name:

                    # create bundle basd on geodes
                    bundle = self._clack(feature, abbreviation)

                # otherwise, bundle against available abscissas
                else:

                    # pass a 1 dimensional feature as is
                    if len(feature.shape) == 1:

                        # bundle up single feature
                        bundle = self._tack(feature, abscissas, abbreviation)

                    # otherwise, if 2-d or greater
                    else:

                        # # pack up using standard tags
                        # bundle = self._pack(feature, tags, abscissas, abbreviation, size)

                        # split feature along second dimension if possible
                        if len(feature.shape) == 2 and self.dimension > 1:

                            # add splitting along secondary axis
                            bundle = self._hack(feature, abscissas, independents, abbreviation, size)

                        # split feature along both secondary dimensions, shortest first
                        if len(feature.shape) == 3 and self.dimension > 2:

                            # add splitting along both axes
                            bundle = self._crack(feature, abscissas, independents, abbreviation, size)

                # add to bundles
                bundles.append(bundle)

                # add bundle to reference
                self.reference = self._refer(bundle, self.reference)

            # otherwise
            except ValueError:

                # skip
                print('error!: {}'.format(feature.name))
                pass

        return bundles

    def _censor(self, data):
        """Censor nan numbers by converting to text NaN.

        Arguments:
            data: list of floats

        Returns:
            list of float
        """

        # replace nans
        clean = ['NaN' if self.fill in [number] else number for number in data]

        return clean

    def _chrome(self):
        """Set chrome options.

        Arguments:
            None

        Returns:
            chrome driver
        """

        # create chrome driver
        driver = '/Users/matthewbandel/.wdm/drivers/chromedriver/mac64/113.0.5672.63/chromedriver'
        options = webdriver.ChromeOptions()

        # # set timeout to 1 hr
        # webdriver.Manage().Timeouts().PageLoad = 3600;

        # # eliminate margins
        # state = {
        #     "recentDestinations": [
        #         {
        #             "id": "Save as PDF",
        #             "origin": "local",
        #             "account": ""
        #         }
        #     ],
        #     "selectedDestinationId": "Save as PDF",
        #     "version": 2,
        #     "marginsType": 1
        # }
        # profile = {'printing.print_preview_sticky_settings.appState': json.dumps(state)}
        #options.add_experimental_option('prefs', profile)
        options.headless = True
        #options.add_experimental_option('prefs', {'marginsType': 1})
        options.add_argument('--kiosk-printing')
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-gpu')

        # set chrome
        chrome = webdriver.Chrome(driver, chrome_options=options)
        self.chromium = chrome
        self.options = options

        return chrome

    def _clack(self, feature, abbreviation, bins=25):
        """Bundle up a network

        Arguments:
            feature: feature instance
            abbreviation: file abbreviation
            bins: number of boxes to break data into

        Returns:
            list of blooms
        """

        # print status
        self._print('clacking {}...'.format(feature.name))

        # begin bundle
        bundle = []

        # grab the data and create histogramn information
        data = feature.distil()

        # go through each bracket
        brackets = feature.attributes['brackets']
        labels = feature.attributes['labels']

        # create functional for wiring
        def wiring(array): return lambda tensor: numpy.array(array)

        # for each bracket
        for label, bracket in zip(labels, brackets):

            # get data block
            block = data[:, bracket[0]: bracket[1]]

            # make array of verticals
            zipper = zip(block[2], block[3])
            segments = [(vertical, verticalii, self.fill) for vertical, verticalii in zipper]
            array = [entry for segment in segments for entry in segment]

            # create abscissa from bin middles trippling each entry (endponts of bar, plus blank)
            variables = self.variables.setdefault(feature.path, {})
            abscissa = '{}_{}_abscissa'.format(feature.name, label)
            subset = variables.setdefault(len(array), {})
            variables[len(array)][abscissa] = array

            # make array of horizontals
            zipper = zip(block[0], block[1])
            segments = [(horizontal, horizontalii, self.fill) for horizontal, horizontalii in zipper]
            array = [entry for segment in segments for entry in segment]

            # create bloom
            address = [abbreviation, feature.name, '_{}'.format(label)]
            bloom = Bloom(feature, address, wiring(array), None, [abscissa])
            bundle.append(bloom)

        return bundle

    def _clump(self, data, size):
        """Clump data points together by averaging to display at low resolution.

        Arguments:
            data: list of floats
            size: int, partition size

        Returns:
            list of floats
        """

        # by default, set bins t
        averages = [float(entry) for entry in data]

        # if the resolution is smaller than the length
        if size > 1:

            # define chunk size
            chunks = math.floor(len(data) / size)
            averages = [float(sum(data[index * size:size + index * size]) / size) for index in range(chunks)]

        return averages

    def _cluster(self, data, abscissas, void, window=None, fraction=None):
        """Cluster sequences of observations into a number of clusters.

        Arguments:
            data: list of floats
            abscissas: list of ints
            void: float, fill value
            window: int, sequence length
            number: int, number of clusters

        Returns:
            list of floats
        """

        # set default number of clusters
        size = 5

        # set defaults
        window = window or self.window
        fraction = fraction or self.fraction

        # prepare abscissaal sequences
        training, verification, samples = self._prepare(abscissas, window, fraction, center=True)

        # create registry
        registry = {abscissa: datum for abscissa, datum in zip(abscissas, data) if datum != void}

        # calculate average of all registry data
        average = sum(registry.values()) / len(registry)

        # generate targets and training matrix
        matrix = []
        for sequence in training:

            # try to:
            try:

                # create vector for sample and add to matrix
                vector = [registry[number] for number in sequence]
                matrix.append(vector)

            # unless missing fill value number
            except KeyError:

                # in which case, skip
                pass

        # perform model fit
        machine = KMeans(n_clusters=size)
        machine.fit(matrix)

        # get middle index
        middle = math.floor(window / 2)

        # generate cluster
        clusters = {index: [] for index in range(size)}
        for sequence in verification + samples:

            # create vector
            vector = [registry.setdefault(number, average) for number in sequence]

            # make prediction
            prediction = machine.predict([vector])[0]

            # append to cluster, using NaN for the rest
            for index in range(size):

                # check against prediction
                if index == prediction:

                    # add to cluster
                    clusters[index].append(registry[sequence[middle]])

                # otherwise
                else:

                    # add nan
                    clusters[index].append(self.fill)

        # make clusters
        clusters = list(clusters.values())
        clusters = [numpy.array(cluster) for cluster in clusters]

        return clusters

    def _configure(self):
        """Configure based on yaml file, or initialize a yam if not created.

        Arguments:
            None

        Returns:
            None
        """

        # retrieve the yaml file
        yam = self._acquire(self.ticket)
        self.yam = yam

        # if it is empty
        if len(yam) < 1:

            # create it
            self._initialize(self.ticket)
            yam = self._acquire(self.ticket)

            # raise assertion error
            self._print('{} is empty, creating...'.format(self.ticket))

            # if no source
            if not yam['source']:

                # exit
                sys.exit(0)

        # configure data attributes
        self.source = yam['source']
        self.album = yam['album']
        self.dimension = yam['dimension']

        # configure labels
        self.title = yam['title']
        self.ordinate = yam['ordinate']['name']
        self.abscissa = yam['abscissa']['name']

        # configure style attributes
        self.outliers = yam['outliers']
        self.normalizations = yam['normalizations']
        self.styles = yam['styles']
        self.spectra = yam['spectra']
        self.fonts = yam['fonts']
        self.gauges = yam['gauges']
        self.corners = yam['corners']
        self.pen = yam['pen']
        self.coaxes = yam['coaxes']

        # add propagation
        self.propagation = yam['propagation']

        # configure line labels and colors
        labels = []
        colors = []
        for line in yam.get('lines', []):

            # append label and colors
            labels.append(line['label'])
            colors.append(line['color'])

        # set labels and colors
        self.labels = labels
        self.colors = colors

        # set yam
        self.yam = yam

        return None

    def _contextualize(self, graph, reservoir, clickers):
        """Add pickers and stickers.

        Arguments:
            graph: bokeh figure instance
            reservoir: dict of plot data
            clickers: tuple of lists (pickers, stickers)

        Returns:
            bokeh row
        """

        # unpack clickers
        pickers, stickers = clickers

        # add titling
        contexts = []

        # set units
        units = reservoir['graph']['units']
        abscissas = list(reservoir['abscissas'].values())
        abscissas.sort()
        abscissa = abscissas[0]

        # hook up title
        context = TextInput(value=graph.title.text, width=200, sizing_mode='stretch_width')
        context.js_link('value', graph.title, 'text')
        contexts.append(context)

        # add input for ordinate
        context = TextInput(value=graph.yaxis[0].axis_label, width=100, sizing_mode='stretch_width')
        context.js_link('value', graph.yaxis[0], 'axis_label')

        # if seconary acis
        if self.yam.get('axis', True):

            # add link
            context.js_link('value', graph.yaxis[1], 'axis_label')

        # add contexta
        contexts.append(context)

        # add input for abscissa
        context = TextInput(value=graph.xaxis[0].axis_label, width=100, sizing_mode='stretch_width')
        context.js_link('value', graph.xaxis[0], 'axis_label')
        contexts.append(context)

        # add input for secondary axis
        context = TextInput(value=graph.yaxis[1].axis_label, width=100, sizing_mode='stretch_width')
        context.js_link('value', graph.yaxis[1], 'axis_label')
        contexts.append(context)

        # add template checkbox
        box = CheckboxGroup(labels=['Template'], active=[])
        box.on_click(lambda _: self.template(graph, pickers, stickers, contexts))

        # make column from color pickers and text boxes
        stick = Row(stickers, width=1000, margin=(0, 50, 0, 50), width_policy='fixed')
        row = Row(pickers, width=1000, margin=(0, 50, 0, 50))
        texts = Row(contexts, width=1000, margin=(0, 50, 0, 50))
        template = Row(box, width=100)
        column = Column([graph, texts, row, stick, template])

        return column

    def _counsel(self, message, *variables):
        """Log a set of variables to the java console.

        Arguments:
            message: str
            *variables: unpacked list of str

        Returns:
            str
        """

        # begin java
        java = """"""

        # add message
        java += """console.log("{}");""".format(message)

        # for each variable
        for variable in variables:

            # add heading
            java += """console.log("{}");""".format(variable)

            # add variable
            java += """console.log({});""".format(variable)

        return java

    def _count(self, data, bins=25, margin=2):
        """Count data into histogram bins.

        Arguments:
            data: list of float
            bins: int, number of bins
            margin: int, margin in percentiles for edge bins

        Returns:
            tuple of (list of floats, list of floats, floats)
        """

        # create array
        data = numpy.array(data)

        # get 2nd and 98th percentiles
        low = numpy.percentile(data, margin)
        high = numpy.percentile(data, 100 - margin)

        # determine statisitcal properties
        mean = numpy.mean(data)
        median = numpy.percentile(data, 50)
        minimum = numpy.array(data).min()
        maximum = numpy.array(data).max()

        # determine chunk size, not counting bins on each end
        size = (high - low) / (bins - 2)

        # create brackets
        brackets = [(minimum, low)]
        brackets += [(low + size * index, low + size + size * index) for index in range(bins - 2)]
        brackets += [(high, maximum)]

        # define middles of each bin
        middles = [low - (size / 2)] + [(left + right) / 2 for left, right in brackets[1:-1]] + [high + (size / 2)]

        print('middles:')
        print(middles)

        # get the counts
        # counts = [len([datum for datum in data if left <= datum <= right]) for left, right in brackets]
        counts = [((data >= left) & (data < right)).sum() for left, right in brackets[:-1]]
        counts += [((data >= left) & (data <= right)).sum() for left, right in brackets[-1:]]

        # adjust for endponts, make sure noninclusive
        # counts[0] = len([datum for datum in data if brackets[0][0] <= datum < brackets[0][1]])
        # counts[-1] = len([datum for datum in data if brackets[-1][0] < datum <= brackets[-1][1]])

        # deternine mode
        boxes = [(middle, count) for middle, count in zip(middles, counts)]
        boxes.sort(key=lambda pair: pair[1], reverse=True)
        mode = boxes[0][0]

        # calculate standard deviation
        deviation = numpy.std(data) or 1.0

        return middles, counts, size, mean, median, mode, deviation

    def _crack(self, feature, abscissas, independents, abbreviation, size):
        """Split a feature along secondary and tertiary axes.

        Arguments:
            feature: feature instance:
            abscissas: list of str
            independents: list of (str, int) tuples
            abbreviation: str
            size: int

        Returns:
            list of bloom instances
        """

        # get path
        path = feature.path

        # begin bundle
        bundle = []

        # find the perpendicular short and long axes
        parallel = feature.shape.index(size)
        axes = [(index, length) for index, length in enumerate(feature.shape) if index != parallel]
        axes.sort(key=lambda pair: pair[1])

        # define long and short axes
        short = axes[0][0]
        long = axes[1][0]

        # define primary and secondary coordinates
        primaries = [index + 1 for index in range(feature.shape[short])]
        secondaries = [index + 1 for index in range(feature.shape[long])]

        # check for primary match
        matches = [name for name, length in independents if length == len(primaries)]
        if len(matches) > 0:

            # get coordinates
            primaries = self.variables[path][len(primaries)][matches[0]]

        # check for secondary match
        matches = [name for name, length in independents if length == len(secondaries)]
        if len(matches) > 0:

            # get coordinates
            secondaries = self.variables[path][len(secondaries)][matches[0]]

        # determine digits length
        digits = len(str(max(primaries)))
        digitsii = len(str(max(secondaries)))

        # for each short index
        for index in range(feature.shape[short]):

            # split into chunks of some size
            length = 10
            chunks = math.ceil(feature.shape[long] / length)
            for chunk in range(chunks):

                # get bracket
                bracket = [chunk * length, length + chunk * length]
                bracket[1] = min([bracket[1], feature.shape[long] - 1])
                for indexii in range(*bracket):

                    # create default tag using 1-based indexing
                    tag = '_{}'.format(self._place(secondaries[indexii], digitsii))

                    # create address
                    address = [step for step in feature.route] + [tag]
                    address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

                    # mutate name
                    formats = [self._place(primaries[index], digits), self._place(secondaries[bracket[0]], digitsii)]
                    formats += [self._place(secondaries[bracket[1] - 1], digitsii)]
                    name = feature.name + '_{}_{}-{}'.format(*formats)
                    address[-2] = name

                    # define subset list
                    subset = [None] * 3
                    subset[short] = index
                    subset[long] = indexii

                    # make bloom
                    bloom = Bloom(feature, address, None, subset, abscissas)
                    bundle.append(bloom)

        # report completion
        self._stamp('brackets made.')

        return bundle

    def _craft(self, folder):
        """Set up scenery.

        Arguments:
            folder: folder of scenes

        Returns:
            None

        Populates:
            self.scenery
        """

        # begin scenery with blank
        scenery = self._plaster('../scenes/blank/blank.json')

        # if there is a folder
        if folder:

            # load up all images
            paths = self._see(folder)
            paths = [path for path in paths if '.json' in path]

            # plaster each one
            [scenery.update(self._plaster(path)) for path in paths]

        # populate attribute
        self.scenery = scenery

        return None

    def _create(self):
        """Initialize the instance.

        Arguments:
            None

        Returns:
            None
        """

        # configure from ticket
        self._stamp('configuring...', initial=True)
        self._configure()

        # establish palettes
        self._stamp('establishing palettes...')
        self._squeeze()

        # establish backgrounds
        self._stamp('establishing backgrounds...')
        self._craft(self.album)

        # ingest the data
        self._stamp('ingesting data...')
        self._imbibe()

        # build the features
        self._stamp('populating parameters...')
        self._build()

        # post to the website
        self._stamp('posting to site...')
        self._publish()
        self._stamp('published.')

        return None

    def _curse(self, initial, final, fraction):
        """Create an interpolation between two hexadecximal colors.

        Arguments:
            initial: hex str, first color
            final: hex str, second color
            fractIio: float

        Returns:
            str, hexadecimal color
        """

        # interpolate
        hexadecimal = "#"

        # for each channel
        for channel in (1, 3, 5):

            # interpolate red
            left = int('0x' + initial[channel: channel + 2], base=16)
            right = int('0x' + final[channel: channel + 2], base=16)
            interval = right - left
            hue = math.floor(left + fraction * interval)
            hexadecimal += ('00' + hex(hue)[2:])[-2:]

        return hexadecimal

    def _customize(self, graph, reservoir, sources, glyphs, clickers, stage, scenes):
        """Add custom action tools.

        Arguments:
            graph: graph object
            reservoir: dict of attributes
            glyphs: tuple of dicts (lines, circles, regressions, dashes)
            clickers: tuple of lists (pickers, stickers)
            stage: dict of secondary axes data
            scenes: dict of background scenes

        Returns:
            None
        """

        # unpack glyphs and clickers
        lines, circles, regressions, dashes, patches, gradient = glyphs
        pickers, stickers = clickers

        # add undo tool (undo arrow)
        graph.add_tools(UndoTool())

        # make csv button (download)
        callback = CustomJS(args=dict(source=sources), code=self._export())
        custom = CustomAction(action_tooltip='download as csv', icon='../icons/download.png', callback=callback)
        graph.add_tools(custom)

        # add free point draw (dots)
        blank = ColumnDataSource({'x': [], 'y': []})
        triangles = graph.scatter(x='x', y='y', source=blank, marker='square_pin', color='black', size=4)
        tool = PointDrawTool(renderers=[triangles])
        graph.add_tools(tool)

        # add custom action button for changing text size (glasses)
        fonts = self.fonts.copy()
        glasses = self._swivel(fonts)
        graph.tags.append('glasses:' + fonts[0])
        arguments = {'glasses': glasses, 'graph': graph, 'legend': graph.legend}
        arguments.update({'xaxis': graph.xaxis[0], 'yaxis': graph.yaxis[0]})

        # if axis
        if self.yam.get('axis', True):

            # add secondary
            arguments.update({'yiiaxis': graph.yaxis[1]})

        # add callback
        callback = CustomJS(args=arguments, code=self._magnify())
        tip = 'font: {}'.format(fonts[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/glasses.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for toggling backdrop (roller)
        scenery = self.scenery
        backdrops = sorted(list(scenery.keys()))
        backdrops.sort(key=lambda scene: scene !='blank')
        roller = self._swivel(backdrops)
        graph.tags.append('roller:' + backdrops[0])
        arguments = {'scenes': scenes, 'graph': graph, 'scenery': scenery, 'roller': roller}
        callback = CustomJS(args=arguments, code=self._roll())
        tip = 'scene: {}'.format(roller[backdrops[0]])
        custom = CustomAction(action_tooltip=tip, icon='../icons/roller.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for changing legend location (puzzle)
        corners = self.corners.copy()
        puzzle = self._swivel(corners)
        graph.tags.append('puzzle:' + corners[0])
        arguments = {'puzzle': puzzle, 'graph': graph, 'legend': graph.legend}
        callback = CustomJS(args=arguments, code=self._orient())
        tip = 'legend: {}'.format(corners[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/puzzle.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for toggling lines on/ off (compass)
        widths = self.pen.copy()
        compass = self._swivel(widths)
        graph.tags.append('compass:' + str(widths[0]))
        arguments = {'compass': compass, 'graph': graph, 'lines': lines, 'circles': circles, 'legend': graph.legend}
        callback = CustomJS(args=arguments, code=self._draft())
        custom = CustomAction(action_tooltip='lines: {}'.format(widths[1]), icon='../icons/compass.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for changing markers (quill)
        styles = self.styles.copy()
        quill = self._swivel(styles)
        graph.tags.append('quill:' + styles[0])
        arguments = {'quill': quill, 'graph': graph, 'lines': lines, 'circles': circles, 'legend': graph.legend}
        arguments.update({'gauges': self.gauges})
        callback = CustomJS(args=arguments, code=self._dip())
        custom = CustomAction(action_tooltip='style: {}'.format(styles[1]), icon='../icons/feather.png', callback=callback)
        graph.add_tools(custom)

        # make sure there are two spectra available
        spectra = self.spectra.copy()
        if len(spectra) < 2:

            # add another copy
            spectra += [spectra[0]]

        # add custom action for changing palette (palette)
        palette = self._swivel(spectra)
        legend = graph.legend
        graph.tags.append('palette:' + spectra[0])
        arguments = {'palette': palette, 'graph': graph, 'lines': lines, 'circles': circles, 'legend': legend}
        arguments.update({'pickers': pickers, 'spectra': self.palette, 'hexes': self.hexes, 'items': legend.items})
        callback = CustomJS(args=arguments, code=self._splash())
        custom = CustomAction(action_tooltip='palette: {}'.format(spectra[1]), icon='../icons/palette.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for mixing colors into gradient ( pestle )
        toggle = ['off', 'on']
        pestle = self._swivel(['on', 'off'])
        legend = graph.legend
        graph.tags.append('pestle:' + toggle[0])
        arguments = {'colorbar': gradient, 'graph': graph, 'lines': lines, 'circles': circles, 'legend': legend}
        arguments.update({'pickers': pickers, 'spectra': self.palette, 'hexes': self.hexes, 'items': legend.items})
        arguments.update({'brackets': reservoir['heatmap']['brackets'], 'stem': reservoir['heatmap']['stem']})
        arguments.update({'patches': patches})
        callback = CustomJS(args=arguments, code=self._grind())
        custom = CustomAction(action_tooltip='pestle: {}'.format(toggle[1]), icon='../icons/mortar.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for normalizing (scales)
        normalizations = self.normalizations.copy()
        scales = self._swivel(normalizations)
        graph.tags.append('scales:' + normalizations[0])
        arguments = {'graph': graph, 'source': sources, 'axis': graph.yaxis[0], 'secondary': stage['secondary']}
        arguments.update({'lines': lines, 'circles': circles, 'scans': [], 'reservoir': reservoir})
        arguments.update({'units': reservoir['graph']['units'], 'variables': None, 'scales': scales})
        callback = CustomJS(args=arguments, code=self._normalize())
        tip = 'scale: {}'.format(normalizations[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/scale.png', callback=callback)
        graph.add_tools(custom)

        # toggle secondary axis (swatch)
        coaxes = self.coaxes.copy()
        swatch = self._swivel(coaxes)
        graph.tags.append('swatch:' + coaxes[0])
        arguments = {'graph': graph, 'swatch': swatch, 'stage': stage, 'axis': graph.extra_y_ranges['cycle']}
        #arguments = {'graph': graph, 'swatch': swatch, 'stage': stage, 'axis': graph.yaxis[1]}
        arguments.update({'secondary': stage['secondary'], 'yaxis': graph.yaxis[0], 'reservoir': reservoir})
        arguments.update({'lines': lines, 'order': list(lines.keys())})
        callback = CustomJS(args=arguments, code=self._switch())
        tip = 'swatch: {}'.format(coaxes[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/swatch.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for adding regression lines (ruler)
        toggles = ['on', 'off']
        ruler = self._swivel(toggles)
        graph.tags.append('ruler:' + toggles[0])
        arguments = {'graph': graph, 'regressions': regressions, 'dashes': dashes, 'circles': circles}
        arguments.update({'legend': graph.legend, 'ruler': ruler, 'stickers': stickers, 'reservoir': reservoir})
        callback = CustomJS(args=arguments, code=self._measure())
        tip = 'regression: {}'.format(toggles[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/ruler.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for removing outliers (anchor)
        outliers = self.outliers.copy()
        anchor = self._swivel(outliers)
        graph.tags.append('anchor:' + str(outliers[0]))
        arguments = {'graph': graph, 'source': sources, 'axis': graph.yaxis[0]}
        arguments.update({'lines': lines, 'circles': circles, 'scans': [], 'reservoir': reservoir})
        arguments.update({'units': reservoir['graph']['units'], 'variables': None, 'anchor': anchor})
        callback = CustomJS(args=arguments, code=self._anchor())
        tip = 'drop outliers <: {}'.format(outliers[1])
        custom = CustomAction(action_tooltip=tip, icon='../icons/anchor.png', callback=callback)
        graph.add_tools(custom)

        # add custom action for bringing up a layer (broom)
        layers = [index + 1 for index, _ in enumerate(list(lines.keys()))]
        broom = self._swivel(layers)
        graph.tags.append('broom:' + str(layers[-1]))
        arguments = {'graph': graph, 'circles': circles, 'lines': lines, 'legend': graph.legend, 'broom': broom}
        arguments.update({'items': graph.legend.items, 'pickers': pickers, 'stickers': stickers})
        arguments.update({'patches': patches, 'regressions': regressions, 'dashes': dashes})
        callback = CustomJS(args=arguments, code=self._sweep())
        tip = 'cover: {}'.format(layers[0])
        custom = CustomAction(action_tooltip=tip, icon='../icons/broom.png', callback=callback)
        graph.add_tools(custom)

        # toggle on polygons (dice)
        alphas = ['0/0.001', '0.15/2', '0.25/0.001', '0.5/0.001', '0.75/0.001', '1/0.001', '0/0.5', '0/1', '0/2']
        dice = self._swivel(alphas)
        graph.tags.append('dice:' + str(alphas[0]))
        arguments = {'graph': graph, 'regressions': regressions, 'dashes': dashes, 'circles': circles}
        arguments.update({'legend': graph.legend, 'ruler': ruler, 'stickers': stickers, 'reservoir': reservoir})
        arguments.update({'patches': patches, 'dice': dice})
        callback = CustomJS(args=arguments, code=self._polymerize())
        tip = 'polygons: {}'.format(str(alphas[1]))
        custom = CustomAction(action_tooltip=tip, icon='../icons/dice.png', callback=callback)
        graph.add_tools(custom)

        return None

    def _depend(self, path):
        """Begin data reservoir with independent variables.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # timestamp
        self._print('getting independent variables for {}...'.format(path))

        # subset independent variables
        hydra = self.hydra
        variables = hydra.apply(lambda feature: feature.path == path)
        variables = hydra.dig('IndependentVariables', variables)

        # squeeze to one dimension and cast into float
        [variable.fill() for variable in variables]
        [variable.squeeze() for variable in variables]

        # for each variable
        for variable in variables:

            # try to
            try:

                variable.cast(self.type)

            # othereiwse
            except ValueError:

                # nevermind
                variable.cast(str)

        # sort by shape
        reservoir = {}
        shapes = self._group(variables, lambda variable: variable.shape[0])
        for shape, members in shapes.items():

            # go through each member
            reservoir[shape] = {}
            for member in members:

                # grab data
                data = member.spill()

                # create entry
                name = member.name
                reservoir[shape][name] = {}

                # add to reservoir
                reservoir[shape][name] = data

                # assuming timefryear is in timestamped milliseconds
                if 'start_time' in name:

                    # make times entry in microseconds
                    reservoir[shape]['time'] = data

                    # create date string entry
                    days = [str(datetime.datetime.fromtimestamp(time / 1000)) for time in data]

                    days = [day.split()[0] for day in days]
                    reservoir[shape]['day'] = days

                # duplicate start_time settings for month ( will format later )
                if 'month' in name:

                    # make times entry in microseconds
                    reservoir[shape]['time'] = data

                    # create date string entry
                    days = [str(datetime.datetime.fromtimestamp(time / 1000)) for time in data]

                    days = [day.split()[0] for day in days]
                    reservoir[shape]['day'] = days

                # assuming timefryear is in timestamped milliseconds
                if name == 'orbit_start_time_fr_yr':

                    # make times entry in microseconds
                    reservoir[shape]['time'] = data

                    # create date string entry
                    days = [str(datetime.datetime.fromtimestamp(time / 1000)) for time in data]
                    days = [day.split()[0] for day in days]
                    reservoir[shape]['day'] = days

                # convert bracket
                if name == 'bracket_time_fr_yr':

                    # get all unique years bracketing the data, in milliseconds timestamps
                    starts = [math.floor(entry) for entry in data]
                    finishes = [math.ceil(entry) for entry in data]
                    years = self._skim(starts + finishes)

                    # convert each year to microseconds, for bokeh time formatting
                    stamps = {year: datetime.datetime(year, 1, 1).timestamp() for year in years}

                    # get partial years
                    fractions = [entry - year for entry, year in zip(data, starts)]
                    durations = [stamps[finish] - stamps[start] for start, finish in zip(starts, finishes)]
                    partials = [fraction * duration for fraction, duration in zip(fractions, durations)]

                    # make times entry in microseconds
                    times = [stamps[start] + partial for start, partial in zip(starts, partials)]

                    # multiple times by 1000 for ms
                    times = [time * 1000 for time in times]
                    reservoir[shape]['time'] = times

                # if there is an orbit numbetr
                if name == 'orbit_number':

                    # round entries to the nearest integer
                    numbers = [int(entry) for entry in data]
                    reservoir[shape]['number'] = numbers

        return reservoir

    def _designate(self):
        """Make designations for all features depending on field occurrences.

        Arguments:
            None

        Returns:
            None
        """

        # collect all bunches but last
        bunches = [bloom.bunch[:-1] for bloom in list(self)]

        self._tell(bunches)

        # find longest bunch
        bunches.sort(key=lambda bunch: len(bunch), reverse=True)
        length = len(bunches[0])

        # pad all bunches
        bunches = [bunch + [''] * length for bunch in bunches]
        bunches = [bunch[:length] for bunch in bunches]

        # zip bunches together at levels
        ingots = zip(*bunches)
        levels = [list(set(member for member in ingot if member != '')) for ingot in ingots]

        # add indices and sort by length
        levels = [(index, level) for index, level in enumerate(levels)]
        levels.sort(key=lambda pair: len(pair[1]), reverse=True)

        # get top three indices and sort
        indices = [level[0] for level in levels[:3]]
        indices.sort()

        # add designation
        for bloom in list(self):

            # make designation, padding bunch aforehand
            bloom.designation = [(bloom.bunch + (indices[-1] + 1) * ['_'])[index] for index in indices]
            bloom.designation.append(bloom.stem)

        return None

    def _dial(self, knob, advance=True):
        """Prepare java code for changing a setting position.

        Arguments:
            knob: str, setting name
            advance: boolean, advance knob to next?

        Returns:
            str, java code
        """

        # begin javascript
        java = """"""

        # get relevant status from graph tags marked by swatch
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("{}");""".format(knob)
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # if also advancing the dial
        if advance:

            # get new setting
            java += """var advance = {}[status];""".format(knob)

            # update button name
            java += """var stub = cb_obj.description.split(":")[0];"""
            java += """cb_obj.description = stub + ": " + {}[advance];""".format(knob)

            java += self._counsel('before', 'graph.tags')

            # remove old tag and add new to advance dial
            java += """graph.tags.forEach(function(tag, index) {"""
            java += """  if (tag.includes("{}"))""".format(knob) + """ {"""
            java += """    var replacement = "{}:" + advance;""".format(knob)
            java += """    graph.tags[index] = replacement;""".format(knob)
            java += """    };"""
            java += """  });"""

            java += self._counsel('after filter', 'graph.tags')


            # java += """graph.tags.push("{}:" + advance);""".format(knob)

            java += self._counsel('after reconstiution', 'graph.tags')

        return java

    def _diffract(self, color):
        """Deconstruct a single integer color into individual components.

        Arguments:
            color: int

        Returns:
            tuple of ints, the four channels
        """

        # get binary from integer, removing designator
        binary = bin(color)
        binary = binary[2:]

        # pad with zeros
        binary = ('0' * 32 + binary)[-32:]

        # create all channels
        red = int(binary[24:32], 2)
        green = int(binary[16:24], 2)
        blue = int(binary[8:16], 2)
        opacity = int(binary[0:8], 2)

        return red, green, blue, opacity

    def _digitize(self, abscissa):
        """Convert an abscissa into integers.

        Arguments:
            abscissa: list of float

        Returns:
            list of int
        """

        # get minimal step length
        steps = [second - first for first, second in zip(abscissa[:-1], abscissa[1:])]
        step = numpy.median(steps)

        # convert differences from frist to integers
        initial = abscissa[0]
        integers = [math.ceil((datum - initial) / step) for datum in abscissa]

        return integers

    def _dip(self):
        """Dip the quill to change line style.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # get new quill
        java += self._dial('quill')
        java += """var style = advance;"""

        # go through each marker
        java += """Object.keys(circles).forEach(function(name) {"""
        java += """  var circle = circles[name];"""

        # set marker style
        java += """  circle.glyph.marker = gauges[style]["marker"];"""
        java += """  circle.glyph.size = gauges[style]["size"];"""
        java += """  circle.glyph.line_alpha = gauges[style]["alpha"];"""
        java += """  });"""

        return java

    def _draft(self):
        """Toggle lines on or off.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # advance compass setting
        java += self._dial('compass')
        java += """var toggle = advance;"""

        # go through each line and change width
        java += """Object.keys(lines).forEach(function(name) {"""
        java += """  var line = lines[name];"""
        java += """  line.glyph.line_width = toggle;"""
        java += """  });"""

        return java

    def _draw(self, graph, reservoir, sources, trends, visible=False):
        """Draw the lines on the graph.

        Arguments:
            graph: bokeh figure
            names: list of str
            reservoir: dict
            sources: list of column data objects
            trends: dict of column data objects
            visible: boolean, all lines visible?

        Returns:
            tuple of dicts, lists of bokeh objects
        """

        # check for pestile entry
        pestle = self.yam.get('pestle', False)

        # set axis names and font sizes
        variables = reservoir['variables']
        labels = reservoir['labels']
        names = reservoir['blooms']

        # delete reservoir entries to save memory
        del(reservoir['data'])
        del(reservoir['variables'])

        # add default first axis colors
        colors = self._admix(self.spectra[0], len(names))
        colors = self._swap(colors, self.colors)
        colors = colors[:len(names)]

        # set width and visibility attributes
        widths = [self.pen[0]] * len(names)
        visibilities = [True] + [False] * (len(names) - 1)
        if visible or self.yam.get('visibility', False):
            
            # set all visibles
            visibilities = [True] * len(names)

        # set all levels as glyph except for last
        levels = ['glyph'] * (len(names) - 1) + ['glyph']

        # begin color bar override
        override = {}

        # if pestle is chosen
        rainbows = {name: None for name in names}
        if pestle:

            # # create list of NaNs
            # quantities = ['NaN'] * len(sources[names[0]].data['y'])
            #
            # # check for selection
            # if names[0] in reservoir['totals'].keys():
            #
            #     # create list of NaNs
            #     quantities = ['NaN'] * reservoir['totals'][names[0]]

            # get the stem
            stem = reservoir['heatmap']['stem']
            abscissa = '{}_gradient'.format(stem)
            abscissaii = '{}_data_abscissa'.format(stem)

            # for each name
            for name in names:

                # # get the data
                # for index, entry in enumerate(sources[name].data[abscissaii]):
                #
                #     # if not Nan
                #     if entry != 'NaN':
                #
                #         # add to quantities
                #         quantities[index] = entry

                # gradient gradient for values
                brackets = reservoir['heatmap']['brackets']
                quantities = sources[name].data[abscissaii]
                rainbows[name] = self._admix(colors, 0, quantities=quantities, brackets=brackets)

                self._print(' ')
                self._print('quantities', quantities[:10])
                self._print('rainbow', rainbows[name][:10])
                self._print('brackets', brackets)

                # # update override
                # for bracket in brackets:
                #
                #     self._print(bracket)
                #     self._print(bracket[0])
                #     self._print(bracket[1])
                #
                #     # update overridew
                #     override[int(bracket[0])] = 'label_{}'.format(bracket[0])
                #     override[int(bracket[1])] = 'label_{}'.format(bracket[1])

        self._print(' ')
        self._print('override:')
        self._print(override)

        # add legend outside graph
        if self.yam.get('legend', ''):

            # add legend outside
            graph.add_layout(Legend(), self.yam.get('legend'))

        # graph all lines
        lines = {}
        circles = {}
        patches = {}
        pickers = []
        stickers = []
        zipper = list(zip(labels, colors, widths, visibilities, levels, names))
        for label, color, width, visibility, level, name in zipper:

            # make arguments for tighten js call
            arguments = {'lines': lines, 'circles': circles, 'graph': graph, 'reservoir': reservoir}

            # make line and circle options
            options = {'source': sources[name], 'x': 'x', 'y': 'y', 'color': color, 'line_color': color}
            options.update({'line_width': 0.2, 'visible': visibility, 'legend_label': label, 'name': name})
            options.update({'level': level})

            print(label)
            print(sources[name].data['x'][:10])
            print(sources[name].data['x'][-10:])
            print(sources[name].data['y'][:10])
            print(sources[name].data['y'][-10:])

            # if pestle is chosen
            if pestle:

                # get the stem
                stem = reservoir['heatmap']['stem']
                abscissa = '{}_gradient'.format(stem)

                # # check for selection
                # if name in reservoir['selections'].keys():
                #
                #     # get selection and appolu to gradient
                #     selection = reservoir['selections'][name]
                #     subset = [entry for index, entry in enumerate(rainbow) if index in selection]

                # set gradianet
                sources[name].data[abscissa] = rainbows[name]

                # update options
                options.update({'color': abscissa})
                options.update({'line_color': abscissa})

            # render associated circle markers in similar fashion
            marker = self.gauges[self.styles[0]]['marker']
            size = self.gauges[self.styles[0]]['size']
            circle = graph.scatter(**options, size=size, marker=marker)
            circle.js_on_change('visible', CustomJS(args=arguments, code=self._tighten()))
            circles[name] = circle

            # reset line width to that of pen
            options['line_width'] = width
            options.update({'color': color, 'line_color': color})

            # render line, attaching tightening fit on visibility
            line = graph.line(**options)
            line.js_on_change('visible', CustomJS(args=arguments, code=self._tighten()))
            lines[name] = line

            # draw polygons
            polygons = reservoir['polygons'][name]
            horizontals = self._censor(polygons[0])
            verticals = self._censor(polygons[1])

            print('horizontals:', len(horizontals))
            print('verticals:', len(horizontals))

            # check for visible polygons
            fill = 0.0
            if self.yam.get('polygons', False):

                # reset visibility
                fill = 1.0

            # add polygons
            options = {'color': color, 'line_width': 0.0001, 'fill_alpha': fill, 'name': name, 'level': level}
            options.update({'visible': visibility, 'alpha': 1.0, 'legend_label': label})

            # if pestle is chosen
            if pestle:

                # # get the stem
                # stem = reservoir['heatmap']['stem']
                # abscissa = '{}_gradient'.format(stem)
                # subset = rainbow
                #
                # # check for selection
                # if name in reservoir['selections'].keys():
                #
                #     # get selection and appolu to gradient
                #     selection = reservoir['selections'][name]
                #     subset = [entry for index, entry in enumerate(rainbow) if index in selection]
                #     sources[name].data[abscissa] = subset
                #
                # set sourcds
                sources[name].data[abscissa] = rainbows[name]

                # update options
                options.update({'color': sources[name].data[abscissa]})
                options.update({'fill_color': sources[name].data[abscissa]})
                options.update({'line_color': sources[name].data[abscissa]})
                options.update({'line_width': 1})
                #options.update({'line_color': 'black'})

            # make pathc
            patch = graph.patches(horizontals, verticals, **options)
            patch.js_on_change('visible', CustomJS(args=arguments, code=self._tighten()))
            patches[name] = patch

            # create picker
            picker = ColorPicker(width=20, color=color, sizing_mode='stretch_width')
            picker.js_link('color', line.glyph, 'line_color')
            pickers.append(picker)

            # link picker to marker and polygon
            picker.js_link('color', circle.glyph, 'fill_color')
            picker.js_link('color', circle.glyph, 'line_color')
            picker.js_link('color', patch.glyph, 'fill_color')
            picker.js_link('color', patch.glyph, 'line_color')

            # create a sticker
            sticker = TextInput(value=label, width=5, sizing_mode='stretch_width')
            sticker.js_link('value', graph.legend.items[-1], 'label')
            stickers.append(sticker)

        # plot trend lines
        regressions = {}
        dashes = {}
        for name, color, picker in zip(names, colors, pickers):

            # create regressionn line
            options = {'source': trends[name], 'x': 'x', 'y': 'y', 'color': color}
            options.update({'line_width': 4, 'visible': False})
            options.update({'line_dash': 'solid', 'name': '{}_trend'.format(name)})
            regression = graph.line(**options)
            regressions[name] = regression

            # attach picker
            picker.js_link('color', regression.glyph, 'line_color')

            # add black dashed line on top
            options.update({'line_dash': 'dashed', 'color': 'black'})
            dash = graph.line(**options)
            dashes[name] = dash

        # set legend options
        legend = graph.legend
        legend.click_policy = 'hide'
        legend.location = self.corners[0]
        legend.label_text_font_size = self.fonts[0]

        # create gradient bar
        ticks = reservoir['heatmap']['ticks']
        rainbow = LinearColorMapper(palette=self._admix(colors, 25), low=min(ticks), high=max(ticks))
        units = reservoir['graph']['units']
        font = str(int(float(self.yam['fonts'][0].replace('pt', '')) / 1.2)) + 'pt'
        #ticks = [(first + second) / 2 for first, second in zip(ticks[:-1], ticks[1:])]
        ticks = ticks[1:-1]
        width = int(self.yam.get('width', 1000) * 0.5)
        height = int(width / 40)
        orientation = self.yam.get('orientation', 'horizontal')

        # if orientation is vertical
        if orientation == 'vertical':

            # swap widht and height
            width, height = height, width

        # make colorbar
        parameters = {'color_mapper': rainbow, 'label_standoff': 10, 'location': self.corners[0]}
        parameters.update({'width': width, 'height': height, 'major_label_text_font_size': font})
        parameters.update({'major_label_text_color': 'black', 'major_tick_line_color': 'black', 'margin': 10})
        parameters.update({'ticker': FixedTicker(ticks=ticks), 'orientation': orientation, 'title': units})
        parameters.update({'visible': True, 'title_text_font_size': font, 'scale_alpha': 1.0})
        parameters.update({'background_fill_alpha': 1.0})
        parameters.update({'major_label_overrides': override})
        gradient = ColorBar(**parameters)

        # if desired
        if self.yam.get('colorbar', 'false'):

            # set default location
            location = 'bottom'
            if orientation == 'vertical':

                # switch to right
                location = 'right'

            # add coloar bar
            graph.add_layout(gradient)
            gradient.visible = False

        # if usig pestle
        if pestle:

            # hide legend and make gradient visible
            gradient.visible = True
            legend.visible = False

        # if there are geodes
        if len(self.geodes) > 0:

            # add click option
            def poking(event): return self._poke(event, graph, circles)
            graph.on_event('tap', poking)

        # group attributes for clarity
        glyphs = (lines, circles, regressions, dashes, patches, gradient)
        clickers = (pickers, stickers)

        return glyphs, clickers, variables

    def _enroll(self):
        """Enroll all blooms with their stems.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.stems
        """

        # create stems
        stems = {}

        # for every bloom
        for index, bloom in enumerate(self):

            # make registry
            registry = stems.setdefault(bloom.stem, [])
            registry.append(index)

        # set attribute
        self.stems = stems

        return None

    def _entangle(self, nests):
        """Entangle all nests together into one diagram.

        Arguments:
            nests: list of dicts

        Returns:
            dict
        """

        # begin bower
        bower = {}

        # go through each nest
        for nest in nests:

            # create new pointer to bower
            wreath = bower

            # go through keys while they exist
            field = list(nest.keys())[0]
            while field in wreath.keys():

                # advance
                wreath = wreath[field]
                nest = nest[field]

                # get next field
                field = list(nest.keys())[0]

            # append nest
            wreath[field] = nest[field]

        return bower

    def _enterprise(self, series, factor=0.2):
        """Calculate the sample entropy of the series.

        Arguments:
            series: numpy array
            factor: filtration factor

        Returns:
            float
        """

        # set default entropy to absurdly high
        entropy = 1000000

        # normalize the series into zscores
        mean = series.mean()
        deviation = series.std() or 1.0
        scores = numpy.array([(datum - mean) / deviation for datum in series])

        # construct all triplets by rolling and stacking
        shift = numpy.roll(scores, -1)
        shiftii = numpy.roll(scores, -2)
        triplets = numpy.vstack([scores, shift, shiftii])[:, :-2]
        triplets = triplets.transpose(1, 0)

        # start matrix to hold difference between each pair of triplets, excluding sames
        length = triplets.shape[0]
        matrix = numpy.zeros((length, length, 3))
        for increment in range(length):

            # roll the triplets and subtract
            roll = numpy.roll(triplets, -increment, axis=0)
            differences = triplets - roll
            matrix[increment] = differences

        # remove first row to avoid self counting
        matrix = matrix[1:, :, :]

        # make boolean mask if the absolute value of the difference is within +/- factor
        mask = abs(matrix) <= factor

        # check for first two members being true
        doubles = mask[:, :, 0] & mask[:, :, 1]
        doubles = doubles.sum()

        # check for all three members being true
        triples = mask[:, :, 0] & mask[:, :, 1] & mask[:, :, 2]
        triples = triples.sum()

        # calculate entropy as -log(triples / doubles
        entropy = -math.log(triples / doubles)

        return entropy

    def _entitle(self, suite):
        """Parse a suite of bloom parameters into various labels.

        Arguments:
            suite: dict

        Returns:
            tuple of str and list of str
        """

        # print blooms
        [print(bloom.designation, bloom.abscissas[0]) for bloom in suite]

        # assume more than one
        if len(suite) > 1:

            # get all sets of parallel designations
            ingots = [ingot for ingot in zip(*[bloom.designation + [bloom.petal] for bloom in suite])]

            # get indices for static designations
            statics = [ingot[0] for ingot in ingots if len(set(ingot)) < 2]
            dynamics = [ingot for ingot in ingots if len(set(ingot)) > 1]

            # construct title from statics
            title = '/'.join(statics)

            # construct labels from dynamics
            labels = ['/'.join(dynamic) for dynamic in zip(*dynamics)]

        # otherwise
        else:

            # get designation
            title = '/'.join(suite[0].designation)
            labels = [suite[0].petal]

        # correct for default title
        if not title:

            # set title
            title = '_'

        # swap in self.labels
        labels = self._swap(labels, self.labels)

        # # shrink labels
        # labels = ['_'.join(label.split('_')[-2:]) for label in labels]

        # check first member of suite for title
        if suite[0].attributes.get('title', ''):

            # set title
            title = suite[0].attributes['title']
            self.title = title

        # remove traioing commas
        title = title.strip().strip(',')

        # get from yam
        title = self.title

        return title, labels

    def _envision(self, matrix, destination, texts=None, spectrum=None, coordinates=None):
        """Envision a matrix as a heat map.

        Arguments:
            matrix: 2-D numpy array
            destination: file path for saving image
            texts: tuple of str (title, units, dependent, independent)
            spectrum: specturm to apply
            coordinates: dict of lists

        Returns:
            None
        """

        # unpack texts
        texts = texts or []
        texts = [text for text in texts] + [''] * 4
        title, units, dependent, independent = texts[:4]

        # set default zoom
        zoom = (0, len(matrix[0]), 0, len(matrix))

        # make default labels
        independent = independent or 'horizontal gridpoint'
        dependent = dependent or 'vertical gridpoint'

        # set default coordinates
        if not coordinates:

            # default to indices
            coordinates = {independent: list(range(matrix.shape[1]))}
            coordinates.update({dependent: list(range(matrix.shape[0]))})

        # make xarry matrix
        matrix = xarray.DataArray(matrix, dims=[dependent, independent], coords=coordinates)
        matrix.attrs.update({'route': title, 'units': units})

        # make plate object and draw graph
        spectrum = spectrum or 'classic'
        plate = Plate(matrix, ['vertical', 'horizontal'], zoom, title, spectrum=spectrum, unity=units)
        plate.glance()

        # open with context manager
        with Image.open('../plots/glance.png') as image:

            # resave glance image to destination
            image.save(destination)

        return None

    def _exchange(self, path, destination, codex):
        """Swap out yaml attributes:

        Arguments:
            path: str, file path to source yaml
            destination: str, file path to destination yaml
            codex: dict, the update dictionary

        Returns:
            None
        """

        # open up yaml
        yam = self._acquire(path)

        # make updates
        yam.update(codex)

        # redispence
        self._dispense(yam, destination)

        return None

    def _excise(self, data):
        """Excise nan values from data.

        Arguments:
            data: list of floats

        Returns:
            list of floats
        """

        # remove NaNs
        excision = [datum for datum in data if datum != 'NaN']

        return excision

    def _export(self):
        """Create javascript code for exporting csv.

        Arguments:
            None

        Returns:
            str
        """

        javascript = """
        function table_to_csv(source) {
            const columns = Object.keys(source.data)
            const nrows = source.get_length()
            const lines = [columns.join(',')]

            for (let i = 0; i < nrows; i++) {
                let row = [];
                for (let j = 0; j < columns.length; j++) {
                    const column = columns[j]
                    row.push(source.data[column][i].toString())
                }
                lines.push(row.join(','))
            }
            return lines.join('\\n').concat('\\n')
        }


        const filename = 'data_result.csv'
        var filetext = table_to_csv(source)
        const blob = new Blob([filetext], { type: 'text/csv;charset=utf-8;' })

        //addresses IE
        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, filename)
        } else {
            const link = document.createElement('a')
            link.href = URL.createObjectURL(blob)
            link.download = filename
            link.target = '_blank'
            link.style.visibility = 'hidden'
            link.dispatchEvent(new MouseEvent('click'))
        }
        """

        return javascript

    def _forecast(self, data, abscissas, void, window=None, fraction=None):
        """Forecast the trend patterns using linear regression.

        Arguments:
            data: list of floats
            abscissas: list of floats
            void: float, fill value
            window: int, lag window
            fraction: float, training fraction

        Returns:
            list of floats
        """

        # set defaults
        window = window or self.window
        fraction = fraction or self.fraction

        # prepare abscissaal sequences
        training, verification, samples = self._prepare(abscissas, window, fraction, center=False)

        # create registry
        registry = {abscissa: datum for abscissa, datum in zip(abscissas, data) if datum != void}

        # calculate average of all registry data
        average = sum(registry.values()) / len(registry)

        # generate targets and training matrix
        targets = []
        matrix = []
        for sequence in training:

            # try to
            try:

                # create vector for sample and add to matrix
                vector = [registry[number] for number in sequence[:-1]]
                matrix.append(vector)

                # add target to targets
                target = registry[sequence[-1]]
                targets.append(target)

            # unless it is missing due to voids
            except KeyError:

                # in which case, skip
                pass

        # perform model fit
        machine = LinearRegression(fit_intercept=False)
        machine.fit(matrix, targets)

        # generate verification fit
        fit = []
        for sequence in verification:

            # create vector
            vector = [registry.setdefault(number, average) for number in sequence[:-1]]

            # make prediction
            prediction = machine.predict([vector])[0]
            fit.append(prediction)

            # if there is no data in the registry
            if sequence[-1] not in registry.keys():

                # add entry
                registry[sequence[-1]] = prediction

        # generate forecast
        forecast = []
        for sequence in samples:

            # create vector
            vector = [registry.setdefault(number, average) for number in sequence[:-1]]

            # make prediction
            prediction = machine.predict([vector])[0]
            forecast.append(prediction)

            # if there is no data in the registry
            if sequence[-1] not in registry.keys():

                # add entry
                registry[sequence[-1]] = prediction

        # add padding at mean of training data
        fit = fit + [self.fill] * len(samples)
        forecast = [self.fill] * len(verification) + forecast

        # cast as arrays
        fit = numpy.array(fit)
        forecast = numpy.array(forecast)

        return fit, forecast

    def _fork(self, column):
        """Set new fork onto nesting map.

        Arguments:
            column: bokeh column window

        Returns:
            None
        """

        # get all checkboxes by column id
        identifier = column.id
        boxes = self.twigs[identifier]

        # check for active status
        if len(column.children[0].active) > 0:

            # check for terminal node
            if len(boxes) > 0:

                # add in appropriate branches
                [column.children.append(box) for box in boxes]

            # otherwise
            else:

                # select
                self.select(column)

        # otherwise
        else:

            # remove all checkbox columns
            while len(column.children) > 1:

                # discard
                column.children.pop()

            # prune selections if at a node
            if len(boxes) < 1:

                # prune
                self._prune(column)

        return None

    def _frame(self, title, reservoir):
        """Begin the graph figure.

        Arguments:
            title: str
            reservoir: dict

        Returns:
            bokeh figure
        """

        # unpack reservoir
        abscissas = list(reservoir['abscissas'].values())
        primary = reservoir['graph']['primary']
        bracket = reservoir['graph']['bracket']

        print('\nabscissas: ', abscissas)
        print('')

        # format for datetime
        if 'time' in abscissas[0]:

            # set axis formatting
            axis = {'x_axis_type': 'datetime', 'x_axis_label': self.abscissa or 'date'}

        # format for datetime
        elif 'month' in abscissas[0]:

            # set axis formatting
            axis = {'x_axis_type': 'datetime', 'x_axis_label': self.abscissa or 'date'}

        # otherwise
        else:

            # set axis formatting
            axis = {'x_axis_label': self.abscissa or abscissas[0]}
            axis['x_axis_label'] = axis['x_axis_label'].replace('_', ' ')

        # check for log or linear scale
        if self.yam.get('logarithm', False):

            # make y axis logarithmic
            axis['y_axis_type'] = 'log'

        # otherwise
        else:

            # make it linear
            axis['y_axis_type'] = 'linear'

        # add default title
        title = self.title or title
        title = title.strip().strip(',')
        # title = self.yam['title']

        print('')
        print('title')
        print(title)
        print('')

        # begin figure
        units = reservoir['graph']['units']
        aspect = self.yam.get('aspect', 4/3)
        width = self.yam.get('width', 1000)
        options = {'title': title, 'sizing_mode': 'fixed', 'plot_width': width, 'plot_height': int(width / aspect)}
        options.update({'visible': True, 'y_range': primary, 'margin': (0, 50, 0, 50), 'x_range': bracket})
        options.update({'y_axis_label': self.ordinate or units})
        options['y_axis_label'] = options['y_axis_label'].replace('_', ' ')
        options.update(axis)
        options.update({'background_fill_color': self.yam.get('background', 'whitesmoke')})
        options.update({'output_backend': 'canvas'})
        graph = Figure(**options)
        graph.grid.grid_line_color = self.yam.get('grid', 'lightgray')

        # format with month
        if 'time' in abscissas[0]:

            print('here, month!')

            # format for month
            # graph.xaxis.formatter = DatetimeTickFormatter(months='%b')

        # check for log or linear scale
        if self.yam.get('logarithm', False):

            # make y axis logarithmic
            graph.yaxis[0].formatter = PrintfTickFormatter(format="%5f")

        # check for wallpaper
        wallpaper = self.yam.get('wallpaper', '__')

        # construct scenes
        scenes = {}
        for name, construction in self.scenery.items():

            # unpack blank scenery
            arguments = {'image': construction['image'], 'x': construction['left'], 'y': construction['bottom']}
            arguments.update({'dw': construction['width'], 'dh': construction['height']})
            arguments.update({'visible': construction['visible'], 'level': construction['level']})

            # check for wallpaper
            if wallpaper in name:

                # make visible
                arguments.update({'visible': True})

            # add background image
            scene = graph.image_rgba(**arguments)
            scenes[name] = scene

        # remove logo
        graph.toolbar.logo = None

        return graph, scenes

    def _ghost(self, path):
        """Make all white backgrounds transporent.

        Arguments:
            path: str, image filepath

        Returns:
            None
        """

        # open imagge with context manager
        with Image.open(path) as pointer:

            # open up image and convert to array
            image = numpy.array(pointer)

            # add intensities
            intensities = image[:, :, :3].sum(axis=2)

            # change grey to white
            intensities = numpy.where(intensities < 400, intensities, 700)

            # multiply by lightening factor
            image[:, :, 3] = numpy.where(intensities < 600, image[:, :, 3], 0)
            image = image.astype('uint8')

            # resave
            Image.fromarray(image).save(path)

        return None

    def _graft(self, nest, route=None, diagram=None, tab=0):
        """Set new branch onto nesting map.

        Arguments:
            nest: paramters nest
            route=None: str, data path
            diagram=None: dict of lists
            tab=0: amount to tab over

        Returns:
            None

        Populates:
            self.diagram
        """

        # set default diagram and route
        diagram = diagram or {}
        route = route or ''

        # try to
        try:

            # go through all members
            def clicking(column): return lambda _: self._fork(column)
            for name, branches in nest.items():

                # make checkbox
                margin = (2, 50 * tab + 5, 2, 50 * tab + 5)
                box = CheckboxGroup(labels=[name], active=[], visible=True, width=200, margin=margin)

                # make column object around box
                column = Column([box])

                # add to route and set to column name
                structure = '{}:{}'.format(name, route)
                column.name = structure

                # add click action
                box.on_click(clicking(column))

                # add to diagram
                diagram.update({name: {'box': column, 'children': {}}})
                diagram[name]['children'].update(self._graft(branches, structure, diagram[name]['children'], tab + 1))

        # otherwise
        except AttributeError:

            # terminate growth
            pass

        return diagram

    def _grind(self):
        """Mix current colors into a point by point gradient.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # add message to console
        java += self._counsel("grinding...", "lines", "items")

        # make legend invisible
        java += """legend[0].visible = false;"""

        # gather all hex values from spectrum
        java += """var rainbow = items.map(function(item) {"""
        java += """  var identifier = item.renderers[0].name;"""

        # change line color
        java += """  var color = lines[identifier].glyph.line_color;"""
        java += """  return color;"""
        java += """  });"""

        # determine number of legend items
        java += """var number = items.length;"""

        # create new brackets from old brackets, by fitting to one less bracket
        java += """var bracketsii = brackets.map(function(bracket) {"""
        java += """  var minimum = brackets[0][0];"""
        java += """  var enlargement = number / (number - 1);"""
        java += """  return [minimum + (bracket[0] - minimum) * enlargement, minimum + (bracket[1] - minimum) * enlargement];"""
        java += """  });"""

        # get first entry for the bounds
        java += """var bounds = bracketsii.map(function(bracket) {;"""
        java += """  return bracket[0];"""
        java += """  });"""

        # for each line
        java += """Object.values(circles).forEach(function(line, number) {;"""

        # begin the gradient
        java += """  var gradient = [];"""

        # get the measurement data
        java += """  stem = String(stem);"""
        java += """  var key = stem.concat("_data_abscissa");"""
        java += """  var measurements = line.data_source.data[key];"""

        #java += self._counsel("brackets", "brackets", "bracketsii", "bounds")


        # for each measurment
        java += """  measurements.forEach(function(measurement) {;"""
        java += """    var hex = "#ffffff";"""

        # and each bound, if the measurement is within the bound
        java += """    bounds.slice(0, -1).forEach(function(bound, index) {;"""

        #java += self._counsel("measurement", "measurement")

        java += """      if ((measurement >= bounds[index]) && (measurement < bounds[index + 1])) {;"""

        # define interpolationn parameters

        #java += self._counsel("here", "bounds[index]", "bounds[index + 1]", "hex")

        java += """        var first = index;"""
        java += """        var second = index + 1;"""
        java += """        var initial = rainbow[index];"""
        java += """        var final = rainbow[index + 1];"""
        java += """        var fraction = (measurement - bounds[index]) / (bounds[index + 1] - bounds[index]);"""

        # perfrom interpolation
        java += self._interpolate()

        # add the color to the gradient
        java += """        };"""
        java += """      });"""
        java += """    gradient.push(hex);"""
        java += """    });"""

        # add the gradient as the glyph color
        java += """  key = stem.concat("_gradient");"""
        java += """  line.data_source.data[key] = gradient;"""
        java += """  line.glyph.fill_color = {"field": key};"""
        java += """  line.glyph.line_color = {"field": key};"""
        # java += """  patches[identifier][number].glyph.fill_color = {"field": key};"""
        # java += """  patches[identifier][number].glyph.line_color = {"field": key};"""
        java += """  line.data_source.change.emit();"""

        java += self._counsel("gradient", "gradient")

        # close
        java += """  });"""

        # make the colorbar visible
        java += """colorbar.visible = true;"""

        return java

    def _hack(self, feature, abscissas, independents, abbreviation, size):
        """Split a feature along secondary axis.

        Arguments:
            feature: feature instance:
            abscissas: list of str
            independents: list of (str, int) tuples
            abbreviation: str
            size: int

        Returns:
            list of bloom instances
        """

        # begin bundle
        bundle = []

        # find the perpendicular axis
        parallel = feature.shape.index(size)
        axis = int(not parallel)

        # define primary and secondary coordinates
        primaries = [index + 1 for index in range(feature.shape[axis])]

        # check for primary match
        matches = [name for name, length in independents if length == len(primaries)]
        if len(matches) > 0:

            # get coordinates
            primaries = self.variables[feature.path][len(primaries)][matches[0]]

        # determine digits length
        digits = len(str(max(primaries)))

        # split into chunks of ten
        length = 10
        chunks = math.ceil(feature.shape[axis] / length)
        for chunk in range(chunks):

            # get bracket, truncating to last in abscissa
            bracket = [chunk * length, length + chunk * length]
            bracket[1] = min([bracket[1], feature.shape[axis]])
            for index in range(*bracket):

                # create address
                tag = '_{}'.format(self._place(primaries[index], digits))
                address = [step for step in feature.route] + [tag]
                address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

                # mutate name
                formats = [self._place(primaries[bracket[0]], digits), self._place(primaries[bracket[1] - 1], digits)]
                name = feature.name + '_{}-{}'.format(*formats)
                address[-2] = name

                # define subset list
                subset = [None] * 2
                subset[axis] = index

                # replicate abscissas list and sort based on presence of tag
                replicas = abscissas.copy()
                replicas.sort(key=lambda abscissa: tag in abscissa, reverse=True)

                # make bloom
                bloom = Bloom(feature, address, None, subset, replicas)
                bundle.append(bloom)

        return bundle

    def _hail(self, nebula):
        """Set new branch onto clouding map.

        Arguments:
            nebula: paramters cloud

        Returns:
            None

        Populates:
            self.storm
        """

        # define functional
        def clicking(column): return lambda _: self._fork(column)

        # for each entry
        for tag, members in nebula.items():

            # construct all routes
            routes = (['Parameters'] + tag.split('..') + [member] for member in members)

            # for each route
            for route in routes:

                # reset storm
                storm = self.storm
                level = 0

                # for each step in the route
                for step in route:

                    # check for entry
                    if step not in storm.keys():

                        # make checkbox
                        margin = (2, 50 * level + 5, 2, 50 * level + 5)
                        box = CheckboxGroup(labels=[step], active=[], visible=True, width=200, margin=margin)

                        # make column object around box
                        column = Column([box])

                        # add to route and set to column name
                        structure = '{}:{}'.format(step, route)
                        column.name = structure

                        # add click action
                        box.on_click(clicking(column))

                        # add to storm
                        storm.update({step: {'box': column, 'children': {}}})

                    # go down step
                    level += 1
                    storm = storm[step]['children']

        return None

    def _imbibe(self):
        """Ingest all data files into one set of features.

        Arguments:
            None

        Returns:
            None

        Populates:
            self
        """

        # create hydra instance
        self.hydra = Hydra(self.source)
        self.paths = self.hydra.paths
        self.paths.sort()

        # make abbreviations for paths
        self._abbreviate()

        # for all paths
        for path in self.paths:

            # accumulate the data
            self.hydra.ingest(path, discard=False)

            # add reservoir of independent variables
            self.variables[path] = self._depend(path)

        return None

    def _initialize(self, ticket):
        """Initialize a yaml configuration file.

        Arguments:
            ticket: str, name of file

        Returns:
            None
        """

        # begin yaml record with default values for data folder, scenery folder, title, and dimension
        yam = {'source': self.source, 'album': '', 'dimension': self.dimension}
        yam.update({'title': ''})
        entry = {'name': '', 'start': '', 'finish': ''}
        yam.update({'abscissa': entry, 'ordinate': entry.copy(), 'visibility': False})
        yam.update({'legend': '', 'grid': 'lightgray', 'base': '', 'orientation': 'horizontal', 'symlog': False})
        yam.update({'pestle': False, 'colorbar': False, 'polygons': False, 'wallpaper': '', 'hover': True})
        yam.update({'width': 1000, 'aspect': 1.33, 'axis': True, 'logarithm': False, 'background': 'whitesmoke'})
        yam.update({'extra': 0})

        # add line arguments
        line = {'label': '', 'color': ''}
        yam.update({'lines': [line, line.copy()]})

        # add propagation
        yam.update({'propagation': {'folder': '', 'stub': ''}})
        specifics = {'name': '', 'start': '', 'finish': '', 'entries': '', 'step': ''}
        yam['propagation'].update({'primary': specifics, 'secondary': specifics.copy()})
        entry = {'name': '', 'tag': '', 'file': '', 'step': 1}
        yam['propagation'].update({'information': ['']})
        yam['propagation'].update({'parameters': [entry, entry.copy()]})

        # get default attributes
        whetstone = self._whet()
        for name, info in whetstone.items():

            # add to yam
            yam[name] = info

        # dump yam
        self._dispense(yam, ticket)

        return None

    def _interpolate(self):
        """Create java code for color interpolation.

        Arguments:
            None

        Returns:
            str, javascript
        """

        # begin java
        java = """"""

        # begin hexadecimal color string
        java += """      hex = "#";"""

        #java += self._counsel("begin", "hex")

        # interpolate red
        java += """      var left = parseInt(initial.slice(1, 3), 16);"""
        java += """      var right = parseInt(final.slice(1, 3), 16);"""
        java += """      var interval = right - left;"""
        java += """      var red = Math.floor(left + fraction * interval);"""
        java += """      red = red.toString(16);"""
        java += """      red = '00'.concat("", red).slice(-2);"""
        java += """      hex = hex.concat("", red);"""

        #java += self._counsel("red", "hex")


        # interpolate green
        java += """      var left = parseInt(initial.slice(3, 5), 16);"""
        java += """      var right = parseInt(final.slice(3, 5), 16);"""
        java += """      var interval = right - left;"""
        java += """      var green = Math.floor(left + fraction * interval);"""
        java += """      green = green.toString(16);"""
        java += """      green = '00'.concat("", green).slice(-2);"""
        java += """      hex = hex.concat("", green);"""

        #java += self._counsel("green", "hex")


        # interpolate blue
        java += """      var left = parseInt(initial.slice(5, 7), 16);"""
        java += """      var right = parseInt(final.slice(5, 7), 16);"""
        java += """      var interval = right - left;"""
        java += """      var blue = Math.floor(left + fraction * interval);"""
        java += """      blue = blue.toString(16);"""
        java += """      blue = '00'.concat("", blue).slice(-2);"""
        java += """      hex = hex.concat("", blue);"""

        #java += self._counsel("blue", "hex")


        return java

    def _magnify(self):
        """Increase text size.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin javascript
        java = """"""

        # advance dial
        java += self._dial("glasses")

        # set size
        java += """var size = advance;"""

        # adjust title and legend fonts
        java += """graph.title.text_font_size = size;"""
        java += """legend[0].label_text_font_size = size;"""

        # adjust xaxis fonts
        java += """xaxis.axis_label_text_font_size = size;"""
        java += """xaxis.major_label_text_font_size = size;"""

        # adjust yaxis fonts
        java += """yaxis.axis_label_text_font_size = size;"""
        java += """yaxis.major_label_text_font_size = size;"""

        return java

    def _marginalize(self, stream, pad=0.05):
        """Get the start and end ranges for a data stream.

        Arguments:
            stream: list of float
            pad: float, amount of padding

        Returns:
            (float, float) tuple
        """

        # set margins by default
        margins = [-0.01, 0.01]

        # remove NaN
        data = [entry for entry in stream if entry != 'NaN']
        if len(data) > 0:

            # determine min and max
            minimum = min(data)
            maximum = max(data)

            # determine margins
            extent = maxium = minimum
            margins = [minimum - pad * extent, maximum + pad * extent]

        return margins

    def _measure(self):
        """Add java code for making ruler lines visible.

        Arguments:
            None

        Returns:
            str
        """

        # begin javascript
        java = """"""

        # for each line, toggle the regression line
        java += """Object.keys(circles).forEach(function(label) {"""
        java += """  if (circles[label].visible) {"""
        java += """    regressions[label].visible = !regressions[label].visible;"""
        java += """    dashes[label].visible = !dashes[label].visible;"""

        # for each sticker, if it matches the label, add the rate
        java += """    stickers.forEach(function(sticker) {"""
        java += """      if (sticker.value === label) {"""
        java += """        sticker.value = sticker.value + " ( " + reservoir["rates"][label] + " % / yr )"};"""
        java += """      });"""
        java += """    };"""
        java += """  });"""

        return java

    def _mill(self, year, month, day):
        """Translate a year value into milliseconds.

        Arguments:
            year: int
            month: int
            day: int

        Returns:
            int, milliseconds
        """

        # get year
        date = datetime.datetime(year=year, month=month, day=day)

        # translate to milliseconds
        milliseconds = date.timestamp() * 1000

        return milliseconds

    def _mix(self):
        """Mix colors according to selected palette.

        Arguments:
            None

        Returns:
            str
        """

        # begin java
        java = """"""

        # gather all hex values from spectrum
        java += """var colors = spectra[spectrum].map(function(color) {"""
        java += """  return hexes[color];"""
        java += """  });"""

        # determine number of legend items
        java += """var number = items.length;"""

        # if not enough colors, interpolate
        java += """if (colors.length < number) {"""

        # begin interpolations
        java += """  var interpolations = [];"""

        # determine step size
        java += """  var step = (colors.length - 1) / (number - 1);"""
        java += """  var indices = new Array(number);"""

        # for each index
        java += """  items.forEach(function(item, index) {"""

        # calculate ratio and bracket points
        java += """    var ratio = step * index;"""
        java += """    var first = Math.floor(ratio);"""
        java += """    var fraction = ratio - first;"""

        # get initial color of bracket
        java += """    var initial = colors[first];"""

        # check for zero fraction
        java += """    if (fraction === 0) {"""
        java += """      interpolations.push(initial);"""
        java += """      }"""

        # otherwise, interpolate
        java += """    else {"""
        java += """      var second = first + 1;"""
        java += """      var final = colors[second];"""

        # call interpolation
        java += """      var hex = "#ffffff";"""
        java += self._interpolate()

        # add to interpolations
        java += """      interpolations.push(hex);"""
        java += """      };"""
        java += """    });"""

        # convert colors
        java += """  colors = interpolations;"""
        java += """  };"""

        return java

    def _nebulize(self, pool, size=10):
        """Group several names in groups alphabetically.

        Arguments:
            pool: incoming dict
            position: int, position of breakpoint
            size: max size of list

        Returns:
            dict
        """

        # get maximum length
        maximum = max([len(member) for member in pool])
        reservoir = {'_': [(member + '_' * maximum)[:maximum] for member in pool]}

        # assign to default nebula
        nebula = reservoir

        # for each position
        for position in range(maximum):

            # begin nebula
            nebula = {}

            # for each group in the reservoir
            for tag, members in reservoir.items():

                # if there are more members than the size
                if len(members) > size:

                    # group members by letter at current position
                    letters = self._group(members, lambda member: member[position])

                    # if only one group was found
                    if len(letters) == 1:

                        # add back to nebula
                        nebula[tag] = members

                    # otherwise
                    else:

                        # for each group
                        for letter, membersii in letters.items():

                            # add to the nebula
                            nebula['{}..{}{}'.format(tag, members[0][:position], letter)] = membersii

                # other wise
                else:

                    # add back members
                    nebula[tag] = members

                # reassign to reservoir
                reservoir = nebula

        # strip underscore and excess ellipsis
        def stripping(word): return word.strip('_').strip('.')
        nebula = {stripping(tag): [stripping(member) for member in members] for tag, members in nebula.items()}

        return nebula

    def _normalize(self):
        """Alter the scaling of the vertical axis in javascript.

        Arguments:
            None

        Returns:
            str, javascript
        """

        # begin java
        java = """"""

        # print status
        java += """console.log("normalizing...");"""

        # advance dial
        java += self._dial('scales')
        java += """var prenormalization = status;"""
        java += """var normalization = advance;"""

        # get outler status
        java += self._dial('anchor', advance=False)
        java += """var outliers = status;"""

        # go through each line
        java += """Object.keys(lines).forEach(function(label) {"""

        # get original data stream
        java += """  var source = lines[label].data_source;"""
        java += """  var stream = source.data["y"];"""

        # calculate the buffer in case of zero or negative logarithms
        java += """  var tiny = 10e-10;"""
        java += """  var buffer = Math.abs(Math.min(Math.min(...stream), tiny));"""
        java += """  var mean = reservoir["averages"][label];"""
        java += """  var deviation = reservoir["deviations"][label];"""
        java += """  var middle = (graph.y_range.start + graph.y_range.end) / 2;"""
        java += """  if (deviation === 0) {deviation = 1};"""

        # undo absolute normalization
        java += """  if (prenormalization === "absolute") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return datum;"""
        java += """      });"""
        java += """    stream = data;"""
        java += """    };"""

        # undo percent normalization
        java += """  if (prenormalization === "percent") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return middle + (datum * middle) / 100;"""
        java += """      });"""
        java += """    stream = data;"""
        java += """    };"""

        # undo absolute symlog normalization
        java += """  if (prenormalization === "logarithm") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return Math.sign(datum) * ((10 ** Math.abs(datum)) - 1);"""
        java += """      });"""
        java += """    stream = data;"""
        java += """    };"""

        # undo absolute sigma normalization
        java += """  if (prenormalization === "sigma") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return (datum * deviation) + mean;"""
        java += """      });"""
        java += """    middle = (middle * deviation) + mean;"""
        java += """    stream = data;"""
        java += """    };"""

        # convert from sigma if normalization is normal
        java += """  if (normalization === "absolute") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return datum;"""
        java += """      });"""
        java += """    };"""

        # convert to percent
        java += """  if (normalization === "percent") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return 100 * (datum - middle) / middle;"""
        java += """      });"""
        java += """    };"""

        # convert to log scale if normalization is logarithm
        java += """  if (normalization === "logarithm") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return Math.sign(datum) * Math.log10(Math.abs(datum) + 1);"""
        java += """      });"""
        java += """    };"""

        # convert from log to sigma scale if normalization is sigmas
        java += """  if (normalization === "sigma") {"""
        java += """    var data = stream.map(function(datum, index) {"""
        java += """      return (datum - mean) / deviation;"""
        java += """      });"""
        java += """    };"""

        # update data source
        java += """  source.data["y"] = data;"""
        java += """  source.change.emit();"""
        java += """  console.log("emitted.");"""
        java += """  });"""

        # adjust units depending on scale
        java += """var designation = reservoir["graph"]["units"];"""
        java += """if (normalization === "percent") {designation = "percent median ( % ) " + designation};"""
        java += """if (normalization === "logarithm") {designation = "symlog " + designation};"""
        java += """if (normalization === "sigma") {designation = "stdevs from mean " + designation};"""
        java += """axis.axis_label = designation;"""

        # adjust scaling bounds
        java += """var start = reservoir["graph"]["primary"][0];"""
        java += """var finish = reservoir["graph"]["primary"][1];"""

        # adjust scale if absolute
        java += """if (normalization === "absolute") {"""
        java += """  var bottom = start;"""
        java += """  var top = finish;"""
        java += """  };"""

        # adjust scale for percent
        java += """if (normalization === "percent") {"""
        java += """  var center = (start + finish) / 2;"""
        java += """  var bottom = 100 * (start - center) / center;"""
        java += """  var top = 100 * (finish - center) / center;"""
        java += """  };"""

        # adjust scale for symlog
        java += """if (normalization === "logarithm") {"""
        java += """  var bottom = Math.sign(start) * Math.log10(Math.abs(start) + 1);"""
        java += """  var top = Math.sign(finish) * Math.log10(Math.abs(finish) + 1);"""
        java += """  };"""

        # adjust scale for sigma
        java += """if (normalization === "sigma") {"""
        java += """  var bottom = -outliers;"""
        java += """  var top = outliers;"""
        java += """  };"""

        # change range
        java += """graph.y_range.start = bottom;"""
        java += """graph.y_range.end = top;"""

        # print status to terminal
        print('change emit!')

        return java

    def _orient(self):
        """Locate the legend.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # advance dial
        java += self._dial('puzzle')
        java += """var corner = advance;"""

        # change legend location
        java += """legend[0].location = corner;"""

        return java

    def _pack(self, feature, tags, abscissas, abbreviation, size):
        """Bundle up a multidimensinoal feature by taking standard reductions.

        Arguments:
            feature: feature instance
            tags: list of str
            abscissas: list of str
            abbreviation: str
            size: int, size of abscissa

        Returns:
            list of bloom instances
        """

        # begin bundle
        bundle = []

        # find the perpendicular axes
        parallel = feature.shape.index(size)
        axes = tuple([axis for axis, _ in enumerate(feature.shape) if axis != parallel])

        # construct new address, replace categories and leave a space
        address = [step for step in feature.route] + ['_']
        address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

        # create functionals
        functionals = {}
        functionals.update({'mean': lambda axes: lambda tensor: tensor.mean(axis=axes)})
        functionals.update({'min': lambda axes: lambda tensor: tensor.min(axis=axes)})
        functionals.update({'max': lambda axes: lambda tensor: tensor.max(axis=axes)})
        functionals.update({'std': lambda axes: lambda tensor: tensor.std(axis=axes)})
        functionals.update({'median': lambda axes: lambda tensor: numpy.percentile(tensor, 50, axis=axes)})

        # go through each standard tag
        for tag in tags:
            # add tag to address
            address[-1] = tag

            # craete function
            function = functionals[tag](axes)

            # create bloom and add list
            bloom = Bloom(feature, address, function, None, abscissas)
            bundle.append(bloom)

        return bundle

    def _pad(self, array, length):
        """Pad an array to a length with NaN.

        Arguments:
            array: list of numbers
            length: int, desired length

        Returns:
            list of numbers
        """

        # pad array with NaN
        array = array + ['NaN'] * length
        array = array[:length]

        return array

    def _partition(self, length, resolution, base):
        """Clump data points together by averaging to display at low resolution.

        Arguments:
            length: int, length of dataset
            resolution: int, max number of points
            base: int, partition using powers of this base

        Returns:
            list of floats
        """

        # calculate bin size
        power = math.ceil(math.log10(length / resolution) / math.log10(base))
        size = max([base ** power, 1])

        return size

    def _place(self, number, digits):
        """Extend the textual representation of a number to some digits with zeros.

        Arguments:
            number: int, float, or digist str
            digits: int, number of digits

        Returns:
            str
        """

        # Add zeros to a number and truncate
        zeros = '0' * digits + str(number)
        truncation = zeros[-digits:]

        return truncation

    def _plaster(self, path):
        """Establish the scene for the background image.

        Arguments:
            path: str, path for json file

        Returns:
            image
        """

        # load json
        backdrop = self._load(path)

        # open image with context manager
        with Image.open(backdrop['image']) as pointer:

            # get image
            array = numpy.array(pointer)
            image = array.view(dtype=numpy.uint32).reshape((array.shape[0], array.shape[1]))
            image = numpy.flip(image, axis=0)

            # subset pixels and coordinates
            coordinates = backdrop['coordinates']
            pixels = backdrop['pixels']

            # calculate left edge coordinate: mc = lc - (rc - lc) * lp / (ip - rp - lp)
            section = image.shape[1] - (pixels['left'] + pixels['right'])
            left = coordinates['left'] - (coordinates['right'] - coordinates['left']) * pixels['left'] / section

            # calculate coordinate width
            width = image.shape[1] * (coordinates['right'] - coordinates['left']) / section

            # calculate bottom edge coordinate: mc = lc - (rc - lc) * lp / (ip - rp - lp)
            section = image.shape[0] - (pixels['top'] + pixels['bottom'])
            bottom = coordinates['bottom'] - (coordinates['top'] - coordinates['bottom']) * pixels['bottom'] / section

            # calculate coordinate height
            height = image.shape[0] * (coordinates['top'] - coordinates['bottom']) / section

            # figure out overlay
            overlay = backdrop.get('overlay', 'image')

            # construct scene
            name = path.split('/')[-1].split('.')[0]
            scene = {'name': name, 'image': [image], 'left': left, 'bottom': bottom, 'width': width, 'height': height}
            scene.update({'visible': False, 'level': overlay})

            # add name as key
            scene = {name: scene}

        return scene

    def _plat(self, date, destination, latitude=0, longitude=0):
        """Create a map from tile service.

        Arguments:
            date: str, date in yyyy-mm-dd format
            destination: str, file path
            latitude: float, the latitude of the tile
            longitude: float, the longitude of the tile

        Returns:
            None
        """

        # connect to service
        url = 'http://gibs.earthdata.nasa.gov/wmts/epsg4326/best/wmts.cgi'
        service = WebMapTileService(url)

        # determine row and column based on zoom level 5 ( 9 deg x 9 deg tiles )
        zoom = 1
        size = 144
        vertical = math.floor((90 - latitude) / size)
        horizontal = math.floor((180 + longitude) / size)

        # determine bounds of 3x3 tile region
        west = (-180 + (horizontal * size)) - size
        east = west + 3 * size
        north = (90 - (vertical * size))
        south = north - (size * 2)

        # for each row
        rows = []
        for row in (0, 1):

            # for each column
            columns = []
            for column in (horizontal - 1, horizontal, horizontal + 1):

                # set up search parameters
                layer = 'MODIS_Aqua_CorrectedReflectance_TrueColor'
                parameters = {'layer': layer, 'tilematrixset': 'EPSG4326_250m'}
                parameters.update({'tilematrix': str(zoom), 'row': str(row), 'column': str(column)})
                parameters.update({'format': "image/jpeg", 'time': date})

                # get the map
                response = service.gettile(**parameters)

                # convert to an image and save to destination
                image = Image.open(BytesIO(response.read()))
                image = image.convert('RGBA')
                columns.append(image)

            # combine columns into strips
            arrays = [numpy.array(image) for image in columns]
            strip = numpy.concatenate(arrays, axis=1)
            rows.append(strip)

        # combine into full composite
        composite = numpy.concatenate(rows, axis=0)
        composite = Image.fromarray(composite)

        # save
        composite.save(destination)

        # also create json file
        coordinates = [str(coordinate) for coordinate in (west, east, north, south)]
        pixels = [0, 0, 0, 0]
        self._situate(destination, coordinates=coordinates, pixels=pixels)

        return None

    def _pluck(self, diagram, twigs=None):
        """Pluck off all twigs from the checkbox nesting diagram.

        Arguments:
            diagram: dict of dicts
            twigs: dict

        Returns:
            dict
        """

        # set default twigs
        if twigs is None:

            # set to empty
            twigs = {}

        # go through each branch
        for name, branch in diagram.items():

            # find the column id
            identifier = branch['box'].id

            # collect the children
            children = [branch['children'][child]['box'] for child in branch['children'].keys()]

            # add entry to fork
            twigs[identifier] = children

            # check next branch
            twigs = self._pluck(branch['children'], twigs)

        return twigs

    def _poke(self, event, graph, circles):
        """Examine more detailed available data.

        Arguments:
            event: bokeh event

        Returns:
            None
        """

        print('poking!')

        # set name to default name for now
        name = 'radiance_geode'

        # check for key
        if name in self.geodes.keys():

            # determine click points
            east = event.x
            north = event.y

            # sort by closeness to longitude and lattiude
            coordinates = list(self.geodes[name].keys())
            distances = [(pair[0] - north) ** 2 + (pair[1] - east) ** 2 for pair in coordinates]
            zipper = list(zip(coordinates, distances))
            zipper.sort(key=lambda pair: pair[1])

            # check the first distance
            tolerance = 1.0
            if zipper[0][1] < tolerance:

                # grab some data
                data = self.geodes[name][zipper[0][0]]

                # grab the brackets
                brackets = self.brackets[name]

                # begin insert
                insert = numpy.zeros((*data.shape, 4))

                # for each legend item
                for index, member in enumerate(graph.legend.items):

                    # get the color
                    hex = circles[member.label['value']].glyph.fill_color

                    # decompose into ints
                    colors = [int('0x' + hex[number: number + 2], 16) for number in (1, 3, 5)]
                    channels = numpy.array(colors + [255])

                    # get mask
                    mask = (data >= brackets[index][0]) & (data <= brackets[index][1])

                    # apply to insert
                    insert[mask, :] = channels

                # get insert
                insert = insert.astype('uint8')
                insert = Image.fromarray(insert)
                insert = insert.resize((500, 300))
                quartet = numpy.array(insert)
                quartet = numpy.flip(quartet, axis=0)
                quartet = quartet.transpose(1, 0, 2)

                # convert to singlet floats
                array = [[self._refract(*channels) for channels in row] for row in quartet]
                insert = numpy.array(array)

                # get image
                image = graph.renderers[0].data_source.data['image']
                image = numpy.array(image)
                image[0, :500, :300] = insert[:, :]
                image = image.tolist()

                # reset
                graph.renderers[0].data_source.data['image'] = image

        return None

    def _polymerize(self):
        """Make javascript code for making polygons appear.

        Arguments:
            None

        Returns:
            str, java
        """

        # begin java
        java = """"""

        # print
        java += """console.log("polymerizing...");"""

        # advance dial
        java += self._dial("dice")

        # get alpha and line width
        java += """var alpha = advance.split('/')[0];"""
        java += """var width = advance.split('/')[1];"""

        # set all old renderer levels to glyph
        java += """Object.values(patches).forEach(function(patch) {"""
        java += """  patch.glyph.fill_alpha = Number(alpha);"""
        java += """  patch.glyph.line_alpha = 1.0;"""
        java += """  patch.glyph.line_width = Number(width);"""
        java += """  });"""

        return java

    def _prepare(self, abscissas, window, fraction, center=False):
        """Prepare training and test samples for machine learning analysis.

        Arguments:
            abscissas: list of ints
            window: int, lag window
            fraction: float, training fraction
            center: boolean, center lags on abscissa?

        Returns:
            tuple of lists of ints (abscissa numbers)
        """

        # get first and last abscissas
        first = abscissas[0]
        last = abscissas[-1]

        # determine pivot point at training / test split
        pivot = math.ceil(first + fraction * (last - first))

        # generate training matrix of abscissa numbers
        training = []
        for abscissa in range(first, pivot):

            # if the entire window is present
            if all([number in abscissas for number in range(abscissa + 1 - window, abscissa + 1)]):

                # create row of abscissa numbers
                row = [number for number in range(abscissa + 1 - window, abscissa + 1)]
                training.append(row)

        # determine half of window, defaulting to zero
        half = 0
        if center:

            # calculate half from window size
            half = math.floor(window / 2)

        # create verification samples
        verification = []
        samples = []
        for abscissa in abscissas:

            # create row from all abscissa numbers around particular abscissa (potentially centered)
            row = [number for number in range(abscissa + 1 + half - window, abscissa + 1 + half)]

            # depending on relation to pivot
            if abscissa < pivot:

                # add to verifications
                verification.append(row)

            # otherwise
            else:

                # add to samples
                samples.append(row)

        return training, verification, samples

    def _prune(self, column):
        """Prune selections from data structures that turn inactive.

        Arguments:
            column: str, column id

        Returns:
            None
        """

        # extract words from column name
        parameter = column.name.split(':')[0]
        words = self._purge(parameter).split('_')

        # deactive all word boxes
        for word in words:

            # check for presence
            boxes = [member for member in self.boxes['parameters'] if word in member.labels]
            if len(boxes) > 0:

                # and deactivate
                box = boxes[0]
                index = box.labels.index(word)
                box.active = [entry for entry in box.active if entry != index]

        return None

    def _publish(self):
        """Publish the page with the data map and choice checkboxes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.page
        """

        # print status
        self._stamp('creating page...', initial=True)

        # begin the page
        page = []

        # add title as paragraph
        text = """Boquette: an OMI Trends Monitoring Prototype using Bokeh"""
        paragraph = Paragraph(text=text, width=700, style={'font-size': '15pt'})
        page.append(paragraph)

        # add map heading
        text = """View_Data_Structure"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add View checkbox
        column = self.diagram['Data']['box']
        page.append(column)

        # go through categories
        rows = {}

        # add paragraph
        text = """Propagate_Ticket"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add empty row for checkboxes later
        row = Row([])
        rows['propagate'] = row
        page.append(row)

        # add paragraph
        text = """Choose_Parameters"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # add empty row for checkboxes later
        row = Row([])
        rows['parameters'] = row
        page.append(row)

        # add dividing line
        text = """"""
        text += '_' * (130 - len(text))
        paragraph = Paragraph(text=text, width=700)
        page.append(paragraph)

        # make into bokeh column
        heading = Column(page)
        graphs = Column([])
        self.page = Column(heading, graphs)

        # get all parameter names and initial letters
        self._stamp('nebulizing parameters...')
        names = self._skim([bloom.stem.lower() for bloom in list(self)])
        nebula = self._nebulize(names, 20)
        self.nebula = nebula

        # create drawers
        self._stamp('creating hailstorm...')
        self._hail(nebula)
        drawers = self.storm
        self.drawers = drawers

        # create twigs
        self._stamp('plucking twigs...')
        twigs = self._pluck(drawers)
        self.twigs.update(twigs)

        # add to row
        rows['parameters'].children.append(drawers['Parameters']['box'])

        # add propagation checkbox
        box = CheckboxGroup(labels=['Propagate'], active=[])
        box.on_click(lambda _: self.propagate(screen=True))
        rows['propagate'].children.append(box)

        # print status
        self._stamp('page created.')

        return None

    def _purge(self, word, criterion=30):
        """Purge digits from a word.

        Arguments:
            word: str

        Return:
            str
        """

        # replace all digits
        for digit in range(10):

            # replace each digit or digit underscore combo with a blank
            word = word.replace('_' + str(digit), '').replace(str(digit) + '_', '').replace(str(digit), '')

        # add ellipses if word is longer than criterion
        if len(word) > criterion:

            # add ellipses instead
            front = int(criterion / 2)
            back = criterion - (front + 3)
            word = '{}...{}'.format(word[:front], word[-back:])

        return word

    def _rack(self, feature, abbreviation, bins=100, margin=0, normalize=True):
        """Bundle up a histogram.

        Arguments:
            feature: feature instance
            abbreviation: file abbreviation
            bins: number of boxes to break data into
            margin: margin in percentile
            normalize: boolean, divide by total counts?

        Returns:
            list of blooms
        """

        # print status
        self._print('racking {}...'.format(feature.name))

        # begin bundle
        bundle = []

        # grab the data and create histogramn information
        data = feature.distil()
        middles, counts, width, mean, median, mode, deviation = self._count(data, bins=bins, margin=margin)

        # normalizer counts
        if normalize:

            # divide by total counts
            counts = counts / numpy.array(counts).sum()

        # determine height
        height = max(counts)

        # create abscissa from bin middles trippling each entry (endponts of bar, plus blank)
        triplicate = [entry for entry in middles for _ in range(3)]
        variables = self.variables.setdefault(feature.path, {})
        label = '{}_bars'.format(feature.name)
        subset = variables.setdefault(len(triplicate), {})
        variables[len(triplicate)].update({label: triplicate})

        # create bars as polygons
        polygons = [[], []]
        for middle, count in zip(middles, counts):

            # create left and right bounds
            left = middle - (width * 0.45)
            right = middle + (width * 0.45)

            # add polygon
            horizontals = [left, left, right, right]
            verticals = [0, count, count, 0]

            # add to list
            polygons[0].append(horizontals)
            polygons[1].append(verticals)

        # create bars
        bars = [entry for count in counts for entry in (0, count, self.fill)]
        def barring(tensor): return numpy.array(bars)
        address = [abbreviation, feature.name, '_bars']
        bloom = Bloom(feature, address, barring, None, [label])
        bloom.attributes = bloom.attributes.copy()
        bloom.attributes['polygons'] = polygons
        bundle.append(bloom)

        # create absicssa for gaussian approximation
        label = '{}_gaussian'.format(feature.name)
        subset = variables.setdefault(len(middles), {})
        variables[len(middles)].update({label: middles})

        # calculate area and height of equivalent normal curve
        area = sum(counts) * width
        normalization = 1 / math.sqrt(2 * math.pi * deviation ** 2)

        # calculate normal curve
        curve = [area * normalization * math.exp(-0.5 * ((middle - mean) / deviation) ** 2) for middle in middles]

        # create bloom
        def curving(tensor): return numpy.array(curve)
        address = [abbreviation, feature.name, '_gaussian']
        bloom = Bloom(feature, address, curving, None, [label])
        bundle.append(bloom)

        # for each statitistic
        tags = ['mean', 'median', 'mode']
        statistics = [mean, median, mode]
        subset = variables.setdefault(2, {})
        for tag, statistic in zip(tags, statistics):

            # create abscissa and bloom for mean
            label = '{}_{}'.format(feature.name, tag)
            variables[2].update({label: [statistic, statistic]})
            def tagging(tensor): return numpy.array([0, height])
            address = [abbreviation, feature.name, '_{}'.format(tag)]
            bloom = Bloom(feature, address, tagging, None, [label])
            bundle.append(bloom)

        return bundle

    def _reckon(self, data, limit=5):
        """Calculate average and stdev after removing outliers.

        Arguments:
            data: list of floats
            limit: float, number of stdevs to keep

        Returns:
            (float, float) tuple, the average and stdev
        """

        # take initial mean and deviation
        mean = numpy.mean(data)
        deviation = numpy.std(data) or 1.0

        # convert to zee scores
        zees = [(datum - mean) / deviation for datum in data]

        # filter out zscores greater than limit
        stash = [datum for datum, zee in zip(data, zees) if abs(zee) <= limit]

        # set default mean and deviation in case of empty list
        mean = 0.0
        deviation = 1.0
        if len(stash) > 0:

            # get new mean and deviation
            mean = numpy.mean(stash)
            deviation = numpy.std(stash) or 1.0

        return mean, deviation

    def _refer(self, blooms, reference=None):
        """Create a reference for a set of features for quicker lookup.

        Arguments:
            blooms: list of bloom instances

        Returns
            dict
        """

        # begin reference
        reference = reference or {}

        # for each feature
        for bloom in blooms:

            # add an entry for full slash
            sprigs = reference.setdefault(bloom.sprig, [])
            sprigs.append(bloom)

            # add entry for single name
            names = reference.setdefault(bloom.stem, [])
            names.append(bloom)

        return reference

    def _refract(self, red, green, blue, opacity):
        """Reconstruct a single integer color from individual components.

        Arguments:
            red: int, 0-255
            green: int, 0-255
            blue: int, 0-255
            opacity: int, 0-255

        Returns:
            int
        """

        # combine binary strings, excluding binary string designator, and pad
        binaries = [bin(channel) for channel in (opacity, blue, green, red)]
        binaries = [binary[2:] for binary in binaries]
        binaries = [(8 * '0' + binary)[-8:] for binary in binaries]
        singlet = ''.join(binaries)

        # create integer
        integer = int(singlet, 2)

        return integer

    def _relate(self, independents, dependents, limit=5):
        """Determine the correlation between two series.

        Arguments:
            independents: list of floats
            dependents: list of floats
            limit: std limit for calculating correlations

        Returns:
            (list of floats, float) tuple
        """

        # store independents for later
        stash = numpy.array(independents)
        stashii = numpy.array(dependents)

        # create numpy arrays
        independents = numpy.array(independents)
        dependents = numpy.array(dependents)

        # create masks for eleminating infinites
        finites = numpy.isfinite(independents)
        finitesii = numpy.isfinite(dependents)

        # element all entries that are not finite for either set
        mask = finites * finitesii
        independents = independents[mask]
        dependents = dependents[mask]

        # weed out high std values from independents
        average = independents.mean()
        deviation = independents.std()
        scores = (independents - average) / deviation
        screen = abs(scores) <= limit

        # weed out high std values from independents
        average = dependents.mean()
        deviation = dependents.std()
        scores = (dependents - average) / deviation
        screenii = abs(scores) <= limit

        # recreate lists
        mask = screen * screenii
        independents = independents[mask].tolist()
        dependents = dependents[mask].tolist()

        # normalize independents
        mean = numpy.mean(independents)
        deviation = numpy.std(independents) or 1.0
        independents = [(datum - mean) / deviation for datum in independents]
        stash = numpy.array([(datum - mean) / deviation for datum in stash])

        # reshape into one row per entry
        independents = [[entry] for entry in independents]

        # normalize dependents
        mean = numpy.mean(dependents)
        deviation = numpy.std(dependents) or 1.0
        dependents = [(datum - mean) / deviation for datum in dependents]
        stashii = numpy.array([(datum - mean) / deviation for datum in stashii])

        # perform regression
        machine = LinearRegression(fit_intercept=False)
        machine.fit(independents, dependents)

        # get the coefficient of determination
        score = machine.score(independents, dependents)

        # predict using model
        predictions = machine.predict(stash.reshape(-1, 1))

        # calculate errors between dependents and predictions
        errors = [prediction - dependent for prediction, dependent in zip(predictions, stashii)]

        return errors, score

    def _reserve(self, suite, labels, visible):
        """Fill data reservoir.

        Arguments:
            suite: list of bloom instances
            labels: list of str
            visible: boolean: all lines visible?

        Returns:
            dict
        """

        # create names from bloom indices
        names = [str(bloom.index) for bloom in suite]

        # begin reservoir
        reservoir = {'data': {}, 'variables': {}, 'averages': {}, 'deviations': {}, 'graph': {}, 'paths': {}}
        reservoir.update({'polygons': {}})
        reservoir.update({'abscissas': {}, 'minimums': {}, 'maximums': {}, 'blooms': [], 'labels': labels})
        reservoir.update({'slopes': {}, 'intercepts': {}, 'scores': {}, 'rates': {}, 'initials': {}})
        reservoir.update({'totals': {}, 'selections': {}})

        # extract data and add to reservoir
        zipper = zip(names, suite)
        #[bloom.fill(bloom.function, bloom.subset, refill=True) for bloom in suite if bloom.data is None]
        [bloom.fill(bloom.function, bloom.subset, refill=True) for bloom in suite]
        [bloom.plug() for bloom in suite]
        [bloom.squeeze() for bloom in suite]
        [bloom.cast(self.type) for bloom in suite]
        reservoir['data'].update({name: self._censor(bloom.spill().tolist()) for name, bloom in zipper})

        # add heatmap bracketrs
        brackets = suite[0].attributes.get('brackets', [(0, 0)])
        ticks = [bracket[0] for bracket in brackets] + [brackets[-1][1]]
        reservoir['heatmap'] = {'brackets': brackets, 'ticks': ticks, 'stem': suite[0].stem}

        # for each feature
        variables = {bloom.path: {} for bloom in suite}
        for bloom, name in zip(suite, names):

            # get all variables
            path = bloom.path
            size = bloom.shape[0]

            # update indices
            reservoir['blooms'].append(str(bloom.index))

            # check for selection and total size
            if 'total' in bloom.attributes.keys():

                # get total size and selection
                size = bloom.attributes['total']
                selection = bloom.attributes['selection']
                reservoir['totals'][name] = size
                reservoir['selections'][name] = selection

            # add path
            variables[path].update(self.variables[path][size])

            # add path to reservoir
            reservoir['paths'][name] = path

            # add abscissa
            abscissa = bloom.abscissas[0]
            reservoir['abscissas'][name] = abscissa

            # perform linear regression
            slope, intercept, score = self._trend(bloom)
            reservoir['slopes'][name] = slope
            reservoir['intercepts'][name] = intercept
            reservoir['scores'][name] = score

            # calculate yearly percent change
            variable = self.variables[bloom.path][size][bloom.abscissas[0]]
            initial = (slope * variable[0]) + intercept

            # calculate the percent change based on projected initial in milliseconds
            rate = (100 * 1000 * 60 * 60 * 24 * 365.2 * slope) / (initial or 1.0)
            reservoir['initials'][name] = initial
            reservoir['rates'][name] = rate

            # add polygons
            reservoir['polygons'][name] = bloom.attributes.get('polygons', [[self.fill], [self.fill]])

        # add to reservoir
        reservoir['variables'] = variables

        # update means and stdevs
        for name in names:

            # calculate average and deviation
            average, deviation = self._reckon(self._excise(reservoir['data'][name]))
            reservoir['averages'].update({name: average})
            reservoir['deviations'].update({name: deviation})
            # reservoir['minimums'].update({name: numpy.min(self._excise(reservoir['data'][name]))})
            # reservoir['maximums'].update({name: numpy.max(self._excise(reservoir['data'][name]))})

        # load up other entries
        entries = []
        for name, bloom in zip(names, suite):

            # get all abscissa sntries at the path
            path = bloom.path
            entries += [entry for entry in variables[path][bloom.abscissas[0]]]

        # determine xrange
        entries = [entry for entry in entries if entry != 'NaN']
        margin = 0.05 * (max(entries) - min(entries))
        bracket = (min(entries) - margin, max(entries) + margin)
        reservoir['graph']['bracket'] = bracket

        # check for default bracket
        start, finish = self.yam['abscissa']['start'], self.yam['abscissa']['finish']
        if start:

            # check for percent
            if 'ce' in str(start):

                # calculate new primary as percent from middle of old
                left = self._mill(int(start.strip('ce')))
                right = self._mill(int(finish.strip('ce')))
                bracket = (left, right)

            # otherwise
            else:

                # set bracket assume direct float values
                bracket = (start, finish)

        # set primary to reservoir
        reservoir['graph']['bracket'] = bracket

        # extend bracket
        extension = bracket[1] - bracket[0]
        margin = 0.1 * extension
        bracket = (bracket[0] - margin, bracket[1] + margin)

        # set primary y range according to visible names
        span = numpy.array(variables[suite[0].path][suite[0].abscissas[0]])

        # check for selection
        if 'selection' in suite[0].attributes.keys():

            # correct span
            selection = suite[0].attributes['selection']
            span = span[selection]

        # create subset
        mask = (bracket[0] <= span) & (span <= bracket[1])
        subset = numpy.array(reservoir['data'][names[0]])[mask]
        first = [float(entry) for entry in self._excise(subset.tolist())]

        # if visible
        if visible:

            # add each additional name
            for bloom in suite[1:]:

                # add all non NaN data
                span = numpy.array(variables[bloom.path][bloom.abscissas[0]])

                # check for selection
                if 'selection' in bloom.attributes.keys():

                    # correct span
                    selection = bloom.attributes['selection']
                    span = span[selection]

                # create mask
                mask = (bracket[0] - margin <= span) & (span <= bracket[1] + margin)
                subset = numpy.array(reservoir['data'][str(bloom.index)])[mask]
                first += [float(entry) for entry in self._excise(subset.tolist())]

        # calculate range
        difference = max(first) - min(first)
        margin = max([0.05 * difference, 1e-10])
        primary = (min(first) - margin, max(first) + margin)
        median = numpy.median(first)

        # check for default primary
        start, finish = self.yam['ordinate']['start'], self.yam['ordinate']['finish']
        if start or finish:

            # check for percent
            if '%' in str(start):

                # calculate new primary as percent from middle of old
                bottom = median + (float(start.strip('%')) / 100) * median
                top = median + (float(finish.strip('%')) / 100) * median
                primary = (bottom, top)

            # otherwise
            else:

                # set primary assuming float value
                primary = (start, finish)

        # set primary to reservoir, and add bottom and top
        reservoir['graph']['primary'] = primary

        # determine units
        units = ','.join(set([bloom.units for bloom in suite]))
        reservoir['graph']['units'] = units

        # get paths
        reservoir['graph']['paths'] = [bloom.path for bloom in suite]

        return reservoir

    def _roll(self):
        """Roll on or off the backdrop.

        Arguments:
            None

        Returns:
            None
        """

        # begin java
        java = """"""

        # advance roller dial
        java += self._dial('roller')

        # change scene attributes
        java += """scenes[status].visible = false;"""
        java += """scenes[advance].visible = true;"""

        return java

    def _scale(self, graph, reservoir, sources):
        """Scale the graph according to normalization setting.

        Arguments:
            graph: bokeh figure instance
            reservoir: dict of graph attributes
            sources: bokeh column data objects

        Returns:
            None
        """

        # # print
        # self._print(self.normalizations)
        # self._tell(list(sources.items()))

        # begin java
        java = """"""

        # print status
        java += """console.log("normalizing...");"""

        # advance dial
        java += self._dial('scales')
        java += """var prenormalization = status;"""
        java += """var normalization = advance;"""

        # get outler status
        java += self._dial('anchor', advance=False)
        java += """var outliers = status;"""

        # for each source
        for label, source in sources.items():

            # get data stream
            stream = source.data['y']

            # calculate the buffer in case of zero or negative logarithms
            #tiny = 10e-10
            #buffer = abs(min([min(stream), tiny]))
            mean = reservoir['averages'][label]
            deviation = reservoir['deviations'][label]
            middle = (graph.y_range.start + graph.y_range.end) / 2
            deviation = deviation or 1.0
            designation = graph.yaxis[0].axis_label
            start = reservoir['graph']['primary'][0]
            finish = reservoir['graph']['primary'][1]

            # if sbsoute is first
            if self.normalizations[0] == 'absolute':

                # simpy copy the data
                data = [datum for datum in stream]

                # set up new bounds
                bottom = start
                top = finish

            # if percent is first
            if self.normalizations[0] == 'percent':

                # convert to percents
                data = [100 * (datum - middle) / middle for datum in stream]

                # adjust label
                designation = 'percent median ( % ) {}'.format(designation)

                # set up bounds
                center = (start + finish) / 2
                bottom = 100 * (start - center) / center
                top = 100 * (finish - center) / center

            # if normalization is logarithm
            if self.normalizations[0] == 'logarithm':

                # convert to logarithms
                data = [math.copysign(datum) * math.log10(abs(datum) + 1) for datum in stream]

                # adjust label
                designation = 'symlog {}'.format(designation)

                # set new bounds
                bottom = math.copysign(start) * math.log10(abs(start) + 1)
                top = math.copysign(finish) * math.log10(abs(finish) + 1)

            # if normalization is sigma
            if self.normalizations[0] == 'sigma':

                # convert to sigma
                data = [(datum - mean) / deviation for datum in stream]

                # adjust label
                designation = 'stdevs from mean {}'.format(designation)

                # set new bounds
                bottom = -self.outliers[0]
                top = self.outliers[0]

            # update data source
            source.data['y'] = data
            graph.yaxis[0].axis_label = designation

        # update graph bounds
        graph.y_range.start = bottom
        graph.y_range.end = top

        return None

    def _sculpt(self, tensor, folder, stub, layers, tag='layer', texts=None, spectrum=None, coordinates=None):
        """Generate a stack of two dimensional plots from layers of a tensor.

        Arguments:
            tensor: numpy.array (3-d)
            folder: str, folder to save stack
            stub: str, name for each slide
            layers: list of ints, the layers to choose
            tag: name for tag
            texts: tuple of str, (title, units, dependent label, independent label)
            spectrum: str, name of spectrum
            coordinates: dict, additional coordinate labels

        Returns:
            None
        """

        # create folder
        self._make(folder)

        # unpack texts
        texts = texts or []
        texts = [text for text in texts] + [''] * 4
        title, units, dependent, independent = texts[:4]

        # get coordinate from tag
        panels = [''] * len(layers)
        if tag in coordinates.keys():

            # grab the coordinates
            panels = coordinates[tag]

            # restructure coordinates
            coordinates = {key: value for key, value in coordinates.items() if key != tag}

        # for each layer
        for layer, panel in zip(layers, panels):

            # print status
            self._print('layer {}...'.format(layer))

            # extract the matrix
            matrix = tensor[layer]

            # construct the destination
            places = len(str(max(layers)))
            label = str(layer).zfill(places)
            destination = '{}/{}_{}.png'.format(folder, stub, label)

            # add label to title, and repack
            texts = (title + ',{} {}'.format(tag, panel or label), units, dependent, independent)

            # trigger envision
            self._envision(matrix, destination, texts=texts, spectrum=spectrum, coordinates=coordinates)

        return None

    def _situate(self, scene, coordinates=None, pixels=None):
        """Create a scene file.

        Arguments:
            scene: str, file path for image
            coordinates: tuple of data coordinates (left, right, top, bottom)
            pixels: tuple of pixels from the edge (left, right, top, bottom)

        Returns:
            None
        """

        # begin record
        situation = {'image': scene, 'coordinates': {}, 'pixels': {}, 'overlay': 'image'}

        # if coordinates are given
        if coordinates:

            # input coordinates
            situation['coordinates']['left'] = coordinates[0]
            situation['coordinates']['right'] = coordinates[1]
            situation['coordinates']['top'] = coordinates[2]
            situation['coordinates']['bottom'] = coordinates[3]

        # otherwise
        else:

            # input them
            situation['coordinates']['left'] = input('left coordinate? ')
            situation['coordinates']['right'] = input('right coordinate? ')
            situation['coordinates']['top'] = input('top coordinate? ')
            situation['coordinates']['bottom'] = input('bottom coordinate? ')

        # if pixel coordinates are given
        if pixels:

            # set them
            situation['pixels']['left'] = pixels[0]
            situation['pixels']['right'] = pixels[1]
            situation['pixels']['top'] = pixels[2]
            situation['pixels']['bottom'] = pixels[3]

        # otherwise
        else:

            # input them
            situation['pixels']['left'] = float(input('pixels from left edge? '))
            situation['pixels']['right'] = float(input('pixels from right edge? '))
            situation['pixels']['top'] = float(input('pixels from top edge? '))
            situation['pixels']['bottom'] = float(input('pixels from bottom edge? '))

        # find years
        for direction in ('left', 'right', 'top', 'bottom'):

            # get choice
            choice = situation['coordinates'][direction]
            if 'ce' in choice:

                # reconstruct date
                date = choice.strip('ce').split('-')

                # construct timestamp
                year = int(date[0])
                month = int(date[1])
                day = int(date[2])
                situation['coordinates'][direction] = self._mill(year, month, day)

            # otherwise
            else:

                # make it a float
                situation['coordinates'][direction] = float(choice)

        # ask about overlay
        overlay = bool(input('overlay? '))
        if overlay:

            # set overlay to overlay
            situation['overlay'] = 'overlay'

        # set destination
        destination = scene.replace('png', 'json')
        self._dump(situation, destination)

        return None

    def _splash(self):
        """Splash a new palette onto colors.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # advance palette
        java += self._dial('palette')
        java += """var spectrum = advance;"""

        # mix colors based on number of items
        java += """console.log("mixing...");"""
        java += self._mix()

        # for each item
        java += """items.forEach(function(item, index) {"""

        # get color and label
        java += """  var label = item.label.value;"""
        java += """  var color = colors[index];"""

        # get the line name from the label name
        java += """  var identifier = item.renderers[0].name;"""

        # change line color
        java += """  lines[identifier].glyph.line_color = color;"""

        # change circle color
        java += """  circles[identifier].glyph.line_color = color;"""
        java += """  circles[identifier].glyph.fill_color = color;"""

        # change picker color
        java += """  pickers[index].color = color;"""
        java += """  });"""

        return java

    def _squeeze(self):
        """Squeeze out the color palettes.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.palette
        """

        # define earth spectrum
        self.palette['earth'] = ['blue', 'skyblue', 'green', 'purple', 'violet', 'orange']
        self.palette['earth'] += ['lightgreen', 'red', 'indigo', 'magenta', 'forestgreen', 'orchid']

        # define classic spectrum
        self.palette['classic'] = ['darkblue', 'blue', 'green', 'yellow']

        # define hot spectrum
        self.palette['hot'] = ['black', 'blue', 'magenta']

        # define cool spectrum
        self.palette['cool'] = ['black', 'blue', 'cyan']

        # define rainbow spectrum
        self.palette['rainbow'] = ['red', 'yellow', 'green', 'cyan', 'blue', 'violet']

        # define rainbow spectrum
        self.palette['bowrain'] = ['blue', 'cyan', 'green', 'yellow', 'red']

        # define acid spectrum
        self.palette['acid'] = ['yellow', 'green', 'forestgreen', 'blue', 'violet', 'magenta']

        # define cloud spectrum
        self.palette['clouds'] = ['yellow', 'greenyellow', 'springgreen', 'deepskyblue', 'royalblue']
        self.palette['clouds'] += ['purple', 'indigo', 'black']

        # define error spectrum
        self.palette['errors'] = ['green', 'blue', 'paleblue', 'white', 'palered', 'red', 'orange']
        self.palette['percent'] = ['blue', 'paleblue', 'lightgray', 'palered', 'red']
        self.palette['percent'] = ['purple', 'blue', 'paleblue', 'lightgreen', 'palered', 'red', 'orange']

        # define tripple spectrum
        self.palette['triple'] = ['blue', 'red', 'green']
        self.palette['double'] = ['red', 'blue']
        self.palette['quartet'] = ['red', 'blue', 'orange', 'green']
        self.palette['quintuplet'] = ['darkblue', 'blue', 'lightgreen', 'red', 'darkred']

        # define octopus for plotting an eight color rainbow
        self.palette['squid'] = ['black', 'blue', 'dodgerblue', 'green', 'limegreen', 'gold', 'orange', 'red']
        self.palette['octopus'] = ['black', 'royalblue', 'paleturquoise', 'lightgreen']
        self.palette['octopus'] += ['palegoldenrod', 'gold', 'indianred', 'mediumvioletred']

        # define other color schemes
        self.palette['light'] = ['black', 'violet', 'lightblue', 'turquoise', 'lightgreen', 'greenyellow', 'yellow', 'orange']
        self.palette['green'] = ['black', 'forestgreen', 'green', 'lightgreen', 'greenyellow', 'yellow']
        self.palette['blue'] = ['black', 'blueviolet', 'blue', 'dodgerblue', 'turquoise']

        # add hex values for colors
        self.hexes['black'] = '#000000'
        self.hexes['blue'] = '#0000ff'
        self.hexes['blueviolet'] = '#8a2be2'
        self.hexes['cyan'] = '#00ffff'
        self.hexes['darkblue'] = '#00008b'
        self.hexes['darkred'] = '#8b0000'
        self.hexes['deepskyblue'] = '#00bfff'
        self.hexes['dodgerblue'] = '#1e90ff'
        self.hexes['forestgreen'] = '#228b22'
        self.hexes['gold'] = '#ffd700'
        self.hexes['green'] = '#008000'
        self.hexes['greenyellow'] = '#adff2f'
        self.hexes['indianred'] = '#cd5c5c'
        self.hexes['indigo'] = '#4b0082'
        self.hexes['lightgray'] = '#e6e6e6'
        self.hexes['lightgreen'] = '#90ee90'
        self.hexes['lightblue'] = '#add8e6'
        self.hexes['lime'] = '#00ff00'
        self.hexes['limegreen'] = '#32cd32'
        self.hexes['magenta'] = '#ff00ff'
        self.hexes['mediumslateblue'] ='#7b68ee'
        self.hexes['mediumvioletred'] ='#c71585'
        self.hexes['midnightblue'] = '#191970'
        self.hexes['navy'] = '#000080'
        self.hexes['orange'] = '#ffa500'
        self.hexes['orchid'] = '#da70d6'
        self.hexes['palegoldenrod'] = '#eee8aa'
        self.hexes['palered'] = '#ffa0a0'
        self.hexes['paleblue'] = '#a0a0ff'
        self.hexes['paleturquoise'] = '#afeeee'
        self.hexes['pink'] = '#ffccff'
        self.hexes['purple'] = '#800080'
        self.hexes['red'] = '#ff0000'
        self.hexes['royalblue'] = '#4169e1'
        self.hexes['skyblue'] = '#87ceeb'
        self.hexes['springgreen'] = '#00ff7f'
        self.hexes['turquoise'] = '#40e0d0'
        self.hexes['violet'] = '#ee82ee'
        self.hexes['white'] = '#ffffff'
        self.hexes['yellow'] = '#ffff00'

        # define ozone scheme
        self.palette['ozone'] = ['ozone_{}'.format(index) for index in range(8)]
        hexes = ['#000000', '#a62bf7', '#4236f2', '#a7e6fb', '#a5f570', '#f8fb51', '#f8355c', '#f792e1']
        self.hexes.update({'ozone_{}'.format(index): hexadecimal for index, hexadecimal in enumerate(hexes)})

        # define chlorophyll scheme
        self.palette['chlorophyll'] = ['chlorophyll_{}'.format(index) for index in range(8)]
        hexes = ['#000000', '#ac1dc9', '#4d41fb', '#8be1fd', '#95ff3c', '#fdfa3a', '#fb1e0e', '#9d0606']
        self.hexes.update({'chlorophyll_{}'.format(index): hexadecimal for index, hexadecimal in enumerate(hexes)})

        # define attenuation constant scheme
        self.palette['constant'] = ['constant_{}'.format(index) for index in range(8)]
        hexes = ['#000000', '#3021fd', '#7035f8', '#f83cfb', '#ff7589', '#ffb747', '#ffef10', '#ffffff']
        self.hexes.update({'constant_{}'.format(index): hexadecimal for index, hexadecimal in enumerate(hexes)})

        # define penetration depth scheme
        self.palette['depth'] = ['depth_{}'.format(index) for index in range(8)]
        hexes = ['#000000', '#2c74e8', '#27ecdf', '#2eff93', '#7de67f', '#eefc94', '#ffbb5c', '#fd3030']
        self.hexes.update({'depth_{}'.format(index): hexadecimal for index, hexadecimal in enumerate(hexes)})

        return None

    def _stack(self, feature, tags, abscissas, abbreviation):
        """Stack related features together.

        Arguments:
            feature: feature instance
            tags: list of str
            abscissas: list of str
            abbreviation: str

        Returns:
            list of bloom instances
        """

        print('stacking...')

        # for each tag
        bundle = []
        name = feature.name
        for tag in tags:

            # default tagged to False
            tagged = False

            # if there is a tag therein
            capital = 'Orbit' + tag.capitalize()
            if capital in name:

                # remove from name
                name = name.split(capital)[-1]
                tagged = True

            # if there is a tag therein
            underscore = 'orbit_{}_'.format(tag)
            if underscore in name:

                # use condensed name for branch, removing secondary addresses
                name = name.split(underscore)[-1]
                tagged = True

            # if tagged
            if tagged:

                # remove other route designations
                for step in feature.route:

                    # remove from name
                    name = name.replace(step, '')

                # strip underscores
                name = name.strip('_')

                # construct new address
                address = [step for step in feature.route[:-1]] + [name] + [tag]

                # replace Categories with abbreviation
                address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

                # create bloom and add list
                bloom = Bloom(feature, address, None, None, abscissas)
                bundle.append(bloom)

        return bundle

    def _support(self, data, abscissas, void, window=None, fraction=None):
        """Use a one class support vector machine to detect anamolies.

        Arguments:
            data: list of floats
            abscissas: list of ints
            void: the void value
            window: int, number of past events
            fraction: float, training, test split

        Returns:
            tuple of lists of floats
        """

        # set defaults
        window = window or self.window
        fraction = fraction or self.fraction

        # prepare abscissaal sequences
        training, verification, samples = self._prepare(abscissas, window, fraction, center=True)

        # create registry
        registry = {abscissa: datum for abscissa, datum in zip(abscissas, data) if datum != void}

        # calculate average of all registry data
        average = sum(registry.values()) / len(registry)

        # generate targets and training matrix
        matrix = []
        for sequence in training:

            # try to
            try:

                # create vector for sample and add to matrix
                vector = [registry[number] for number in sequence]
                matrix.append(vector)

            # unless it is missing due to voids
            except KeyError:

                # in which case, skip
                pass

        # perform model fit
        machine = OneClassSVM(gamma='scale', kernel='rbf', nu=0.05)
        machine.fit(matrix)

        # get middle index
        middle = math.floor(window / 2)

        # generate interpolated data to test
        normals = []
        anomalies = []
        for sequence in verification + samples:

            # create vector
            vector = [registry.setdefault(number, average) for number in sequence]

            # make prediction
            prediction = machine.predict([vector])[0]

            # if the normal class is predicted
            if prediction > 0:

                # add to normals and anomalies
                normals.append(registry[sequence[middle]])
                anomalies.append(self.fill)

            # otherwise
            else:

                # add to normals and anomalies
                anomalies.append(registry[sequence[middle]])
                normals.append(self.fill)

        # make arrays
        normals = numpy.array(normals)
        anomalies = numpy.array(anomalies)

        return normals, anomalies

    def _sweep(self):
        """Bring the bottommost element to the top.

        Arguments:
            None

        Returns:
            str, javascript code
        """

        # begin java
        java = """"""

        # print
        java += """console.log("excavating...");"""

        # advance dial
        java += self._dial("broom")

        # set all old renderer levels to glyph
        java += """items[status - 1].renderers.forEach(function(renderer) {"""
        java += """  renderer.level = "glyph";"""
        java += """  });"""

        # set all new levels to overlay
        java += """items[advance - 1].renderers.forEach(function(renderer) {"""
        java += """  renderer.level = "underlay";"""
        java += """  });"""

        return java

    def _switch(self):
        """Define javascript code for altering secondary axis.

        Arguments:
            None

        Returns:
            None
        """

        # begin javascript
        java = """"""

        # print status
        java += """console.log("switching...");"""

        # advance the dial and get statuses
        java += self._dial('swatch')

        # make all secondaries invisible
        java += """stage["twin"].visible = false;"""
        java += """stage["percent"].visible = false;"""
        java += """stage["season"].visible = false;"""
        java += """stage["azimuth"].visible = false;"""
        java += """stage["longitude"].visible = false;"""
        java += """stage["track"].visible = false;"""

        # switch axis to twin
        java += """if (advance === "twin") {"""
        java += """  stage["twin"].visible = true;"""
        java += """  axis.start = graph.y_range.start;"""
        java += """  axis.end = graph.y_range.end;"""
        java += """  secondary.axis_label = yaxis.axis_label;"""
        java += """  };"""

        # switch axis to duel
        java += """if (advance === "duel") {"""

        java += self._counsel('lines', 'lines')

        # get index of second line
        java += """  var keys = Object.keys(lines);"""
        java += """  var index = order[order.length-1];"""

        java += self._counsel('index', 'keys', 'index', 'order')

        # turn on axis
        java += """  stage["duel"].visible = true;"""
        java += """  axis.start = stage["axes"][advance]["start"];"""
        java += """  axis.end = stage["axes"][advance]["finish"];"""

        # get original data stream
        java += """  var source = lines[index].data_source;"""
        java += """  var stream = source.data["y"];"""

        # calculate the buffer in case of zero or negative logarithms
        java += """  var ratio = (graph.y_range.end - graph.y_range.start) / (axis.end - axis.start);"""
        java += """  var offset = graph.y_range.start - axis.start * ratio ;"""
        java += """  var novel = stream.map(function(datum, index) {"""
        java += """    return (datum * ratio) + offset;"""
        java += """    });"""
        java += """  source.data["y"] = novel;"""
        java += """  source.change.emit();"""
        java += """  };"""

        java += self._counsel('y-range:', 'graph.y_range.start', 'graph.y_range.end', 'axis.start', 'axis.end')

        # switch axis to percent
        java += """if (advance === "percent") {"""
        java += """  stage["percent"].visible = true;"""
        java += """  var start = graph.y_range.start;"""
        java += """  var finish = graph.y_range.end;"""
        java += """  var middle = (start + finish) / 2;"""
        java += """  axis.start = 100 * (start - middle) / middle;"""
        java += """  axis.end = 100 * (finish - middle) / middle;"""
        java += """  secondary.axis_label = stage["axes"]["percent"]["label"];"""
        java += """  };"""

        java += self._counsel('secondary', 'secondary')
        java += self._counsel('axis', 'axis')

        java += self._counsel('y-range after:', 'graph.y_range.start', 'graph.y_range.end', 'axis.start', 'axis.end')

        # # switch axis to others
        # java += """if !(["twin", "percent"].includes(advance)) {"""
        # java += """  stage[advance].visible = true;"""
        # java += """  axis.start = stage["axes"][advance]["start"];"""
        # java += """  axis.end = stage["axes"][advance]["finish"];"""
        # java += """  secondary.axis_label = stage["axes"][advance]["label"];"""
        # java += """  };"""

        # # switch axis
        # java += """if (advance === "azimuth") {"""
        # java += """  stage["azimuth"].visible = true;"""
        # java += """  axis.start = 15;"""
        # java += """  axis.end = 35;"""
        # java += """  secondary.axis_label = "solar azimuth angle ( degrees )";"""
        # java += """  };"""
        #
        # # switch axis
        # java += """if (advance === "longitude") {"""
        # java += """  stage["longitude"].visible = true;"""
        # java += """  axis.start = -200;"""
        # java += """  axis.end = 200;"""
        # java += """  secondary.axis_label = "longitude ( degrees east )";"""
        # java += """  };"""
        #
        # # switch axis
        # java += """if (advance === "track") {"""
        # java += """  stage["track"].visible = true;"""
        # java += """  axis.start = stage["margins"][0];"""
        # java += """  axis.end = stage["margins"][1];"""
        # java += """  secondary.axis_label = "track length ( images / orbit )";"""
        # java += """  };"""

        return java

    def _swivel(self, settings):
        """Turn a list of settings into a dial.

        Arguments:
            settings: list of str

        Returns:
            dict, a settings dial
        """

        # make copy
        settings = settings.copy()

        # add first setting to end
        initial = settings[0]
        settings.append(initial)

        # create dial
        dial = {first: second for first, second in zip(settings[:-1], settings[1:])}

        return dial

    def _tabularize(self, suite, reservoir):
        """Create table source objects:

        Arguments:
            suite: list of bloom instances
            reservoir: dict

        Returns:
            list of bokeh column data sources
        """

        # use the bloom index as the name
        names = [str(bloom.index) for bloom in suite]

        # get variables
        variables = reservoir['variables']

        # load up other entries
        tables = []
        entries = []
        tablets = []
        for name, bloom in zip(names, suite):

            # performing clumping and padding, and add to table
            table = {'y': reservoir['data'][name]}

            # calculate  number of extra abscissa
            extra = self.yam.get('extra', 1)

            # begin table with independent variables
            path = bloom.path
            table.update({abscissa: variables[path][abscissa] for abscissa in bloom.abscissas[:extra]})
            table.update({'x': variables[path][bloom.abscissas[0]]})

            # if selection present
            if name in reservoir['selections'].keys():

                # apply selection
                selection = reservoir['selections'][name]
                #subset = [entry for index, entry in enumerate(table['x']) if index in selection]
                subset = numpy.array(table['x'])[selection].tolist()
                table.update({'x': subset})

                # applu to remaining abscissas
                for abscissa in bloom.abscissas[:extra]:

                    self._print('abscissas:')
                    self._print(abscissa)

                    # applyt to abscissa
                    #subset = [entry for index, entry in enumerate(table[abscissa]) if index in selection]
                    subset = numpy.array(table[abscissa])[selection].tolist()
                    table.update({abscissa: subset})

            # add to tables
            tables.append(table)

            # keep track of all entries
            entries += [entry for entry in table['x']]

            # make trendline table for trends
            slope = reservoir['slopes'][name]
            intercept = reservoir['intercepts'][name]
            score = reservoir['scores'][name]
            rate = reservoir['rates'][name]
            initial = reservoir['initials'][name]

            # try to
            try:

                # make trend from bracket
                bracket = [min(table['x']), max(table['x'])]

            # unless index error
            except IndexError:

                # set bracket to first and last
                bracket = [1, 1]

            # make trends
            trend = [(slope * point) + intercept for point in bracket]
            tablet = {'x': bracket, 'y': trend, 'slope': [slope, slope], 'intercept': [intercept, intercept]}
            tablet.update({'r2': [score, score], 'rate': [rate, rate]})
            tablet.update({'initial': [initial, initial]})
            tablets.append(tablet)

        # create source table object
        sources = {name: ColumnDataSource(table) for name, table in zip(names, tables)}
        trends = {name: ColumnDataSource(tablet) for name, tablet in zip(names, tablets)}

        # print status
        self._print('tabularized.')

        return sources, trends

    def _tack(self, feature, abscissas, abbreviation):
        """Create a bundle from a single one-dimensional feature.

        Arguments:
            feature: feature instance
            abscissas: list of str
            abbreviation: str

        Returns:
            list of bloom instances
        """

        # begin bundle
        bundle = []

        # construct new address, using name for both name and sub category
        address = [step for step in feature.route] + [feature.route[-1]]

        # replace Categories with abbreviation
        address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

        # create bloom and add list
        bloom = Bloom(feature, address, abscissas=abscissas)
        bundle.append(bloom)

        return bundle

    def _tighten(self):
        """Tighten the scaling to fit all visible lines.

        Arguments:
            None

        Returns:
            str, javascript
        """

        # begin java
        java = """"""

        # add status
        java += """console.log("tightening...");"""

        # get normalization status
        java += self._dial('scales', advance=False)
        java += """var normalization = status;"""

        # get outlier status
        java += self._dial('anchor', advance=False)
        java += """var outliers = status;"""

        # gather all visible glyphs
        java += """var visibles = Object.values(lines).filter(function(line) {return line.visible});"""
        java += """var visiblesii = Object.values(circles).filter(function(circle) {return circle.visible});"""
        java += """visibles = visibles.concat(visiblesii);"""

        # collect mins and maxes of all data
        java += """var collection = [];"""
        java += """visibles.forEach(function(line) {"""
        java += """  var label = line.name;"""
        java += """  var source = line.data_source;"""
        java += """  var points = source.data["y"].filter(function(entry) {return entry != "NaN"});"""

        # get mean and std of data
        java += """  var mean = reservoir["averages"][label];"""
        java += """  var std = reservoir["deviations"][label];"""

        # convert to percent
        java += """  if (normalization === "percent") {"""
        java += """    var middle = (graph.y_range.start + graph.y_range.end) / 2;"""
        java += """    mean = 100 * (mean - middle) / middle;"""
        java += """    std = 100 * (std - middle) / middle;"""
        java += """    };"""

        # convert to log scale if normalization is logarithm
        java += """  if (normalization === "logarithm") {"""
        java += """    mean = Math.sign(mean) * Math.log10(Math.abs(mean) + 1);"""
        java += """    std = Math.sign(std) * Math.log10(Math.abs(std) + 1);"""
        java += """    };"""

        # convert from log to sigma scale if normalization is sigmas
        java += """  if (normalization === "sigma") {"""
        java += """    mean = 0.0;"""
        java += """    std = 1.0;"""
        java += """    };"""

        # java += """var datamax = Math.max(...points);"""
        # java += """var datamin = Math.min(...points);"""

        # filter out points with z-scores greater than outlier setting
        java += """  if (Math.abs(std) > 0) {"""
        java += """    var score = Number(outliers);"""
        java += """    var points = points.filter(function(entry) {"""
        java += """      return Math.abs((entry - mean) / std) < score});"""
        java += """    };"""

        # java += """var datamax = Math.max(...points);"""
        # java += """var datamin = Math.min(...points);"""

        # append min and max
        java += """  collection.push(Math.max(...points));"""
        java += """  collection.push(Math.min(...points));"""
        java += """  });"""

        # get minimum and maximum
        java += """var maximum = Math.max(...collection);"""
        java += """var minimum = Math.min(...collection);"""

        # calculate the margin based on the difference between the two, with a limit
        java += """var margin = Math.max(0.1 * (maximum - minimum), 1e-10);"""

        # make new bounds
        java += """graph.y_range.start = minimum - margin;"""
        java += """graph.y_range.end = maximum + margin;"""

        # make new bounds for secondary axis
        java += """graph.extra_y_ranges["cycle"].start = minimum - margin;"""
        java += """graph.extra_y_ranges["cycle"].end = maximum + margin;"""

        # # adjust percent axis
        # java += """var middle = (graph.y_range.start + graph.y_range.end) / 2;"""
        # java += """var top = (100 * (graph.y_range.end / middle) - 100);"""
        # java += """var bottom = (100 * (graph.y_range.start / middle) - 100);"""

        # # get secondary axis bounds
        # java += """graph.extra_y_ranges["cycle"].start=bottom;"""
        # java += """graph.extra_y_ranges["cycle"].end=top;"""
        # java += """graph.extra_y_ranges["cycle"].axis_label='%';"""

        # get relevant font size status from graph tag
        java += """var tags = graph.tags.filter(function(tag) {"""
        java += """  return tag.includes("glasses");"""
        java += """  });"""
        java += """var status = tags[0].split(":")[1];"""

        # set axis labels
        java += """graph.extra_y_ranges["cycle"].axis_label_text_font_size = status;"""
        java += """graph.extra_y_ranges["cycle"].major_label_text_font_size = status;"""

        return java

    def _tile(self, date, destination, latitude=0, longitude=0, zoom=5):
        """Create a map from tile service.

        Arguments:
            date: str, date in yyyy-mm-dd format
            destination: str, file path
            latitude: float, the latitude of the tile
            longitude: float, the longitude of the tile

        Returns:
            None
        """

        # connect to service
        url = 'http://gibs.earthdata.nasa.gov/wmts/epsg4326/best/wmts.cgi'
        service = WebMapTileService(url)

        # create sizes dictionary
        sizes = {5: 9, 4: 18, 3: 36, 2: 72, 1: 144}

        # determine row and column based on zoom level 5 ( 9 deg x 9 deg tiles )
        size = sizes[zoom]
        vertical = math.floor((90 - latitude) / size)
        horizontal = math.floor((180 + longitude) / size)

        # determine bounds of 3x3 tile region
        west = (-180 + (horizontal * size)) - size
        east = west + 3 * size
        north = (90 - (vertical * size)) + size
        south = north - 3 * size

        # for each row
        rows = []
        for row in (vertical - 1, vertical, vertical + 1):

            # for each column
            columns = []
            for column in (horizontal - 1, horizontal, horizontal + 1):

                # set up search parameters
                layer = 'MODIS_Aqua_CorrectedReflectance_TrueColor'
                parameters = {'layer': layer, 'tilematrixset': 'EPSG4326_250m'}
                parameters.update({'tilematrix': str(zoom), 'row': str(row), 'column': str(column)})
                parameters.update({'format': "image/jpeg", 'time': date})

                # get the map
                response = service.gettile(**parameters)

                # convert to an image and save to destination
                image = Image.open(BytesIO(response.read()))
                image = image.convert('RGBA')
                columns.append(image)

            # combine columns into strips
            arrays = [numpy.array(image) for image in columns]
            strip = numpy.concatenate(arrays, axis=1)
            rows.append(strip)

        # combine into full composite
        composite = numpy.concatenate(rows, axis=0)
        composite = Image.fromarray(composite)

        # save
        composite.save(destination)

        # also create json file
        coordinates = [str(coordinate) for coordinate in (west, east, north, south)]
        pixels = [0, 0, 0, 0]
        self._situate(destination, coordinates=coordinates, pixels=pixels)

        return None

    def _track(self, feature, abbreviation):
        """Create a bundle from a single one-dimensional feature.

        Arguments:
            feature: feature instance
            abbreviation: str

        Returns:
            list of bloom instances
        """

        # begin bundle
        bundle = []

        # get geode data
        geode = feature.distil()

        # subset
        data = geode[:, :, :-2]

        # get latitudes and longitudes
        latitudes = geode[:, :, -2]
        longitudes = geode[:, :, -1]

        # record geode
        geodes = {}
        for index, row in enumerate(data):

            # record entry
            for indexii, entry in enumerate(row):

                # make lat, lon pair
                pair = (latitudes[index][indexii], longitudes[index][indexii])

                # get panel
                panel = data[index]

                # add to geode
                geodes[pair] = panel

        # assign geodes
        self.geodes[feature.name] = geodes

        # take spectral mean of data
        data = data.mean(axis=2)

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # define brackets and labels
        brackets, texts = self._bracket(percentiles)
        self.brackets[feature.name] = brackets

        # create bundle
        bundle = []

        # for each bracket
        for (first, second), text in zip(brackets, texts):

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            # order along latitude, then longitude
            norths = latitude
            easts = longitude
            norths = norths[numpy.argsort(latitude)]
            easts = easts[numpy.argsort(latitude)]
            norths = norths[numpy.argsort(longitude)]
            easts = easts[numpy.argsort(longitude)]
            latitude = norths
            longitude = easts

            # get path
            path = feature.path

            # create independent variable from longitude
            name = feature.name.replace('geode', 'longitude') + text

            # update variables
            self.variables[path][name] = data

            # construct new address
            address = [step for step in feature.route] + [text]

            # replace Categories with abbreviation
            address = ('/'.join(address).replace('Categories', abbreviation)).split('/')

            # create bloom and add list
            bloom = Bloom(feature, address, abscissas=[name])
            bloom.instil(latitude)
            bundle.append(bloom)
            #
            # # condense measurements to two-D
            # data = measurements.squeeze().mean(axis=2)
            # latitudes = latitudes.squeeze()
            # longitudes = longitudes.squeeze()
            #
            # # define percentile markers
            # percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
            # percentiles = [numpy.percentile(data, percent) for percent in percents]
            #
            # # begin geodes
            # geodes = []
            #
            # # for each bracket
            # brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
            # for first, second in brackets:
            #
            #     # create mask for particular range
            #     mask = (data >= first) & (data <= second)
            #
            #     # get all data in the bin
            #     datum = data[mask].flatten()
            #     latitude = latitudes[mask].flatten()
            #     longitude = longitudes[mask].flatten()
            #
            #     print(' ')
            #     print('datum: {}'.format(datum.shape))
            #     print('latitude: {}, {} - {}'.format(latitude.shape, latitude.min(), latitude.max()))
            #     print('longitude: {}, {} - {}'.format(longitude.shape, longitude.min(), longitude.max()))
            #
            #     # concatenate
            #     geode = numpy.vstack([datum, latitude, longitude])
            #
            #     print('geode: {}'.format(geode.shape))
            #
            #     # order along latitude, then longitude
            #     geode = geode[:, numpy.argsort(latitude)]
            #     geode = geode[:, numpy.argsort(longitude)]
            #
            #     print('geode: {}'.format(geode.shape))
            #
            #     # append
            #     geodes.append(geode[1, :])
            #     geodes.append(geode[2, :])
            #
            #     print(geode[1, :20])
            #     print(geode[2, :20])

        return bundle

    def _trend(self, bloom, limit=5):
        """Perform linear regression on a feature.

        Arguments:
            bloom: Bloom instance
            limit: float, number of stdevs to be within

        Returns:
            (float, float) tuple, the slope, intercept
        """

        # try
        try:

            # get data
            data = bloom.spill()

            # print(bloom)
            # self._tell(bloom.abscissas)

            # get variable
            variable = self.variables[bloom.path][bloom.shape[0]][bloom.abscissas[0]]
            variable = numpy.array(variable)

            # weed out nans and voids
            mask = (data != bloom.void) * (numpy.isfinite(data))
            ordinate = data[mask]

            # weed out high std values
            average = ordinate.mean()
            deviation = ordinate.std() or 1.0
            scores = (ordinate - average) / deviation
            maskii = abs(scores) <= limit
            ordinate = ordinate[maskii]

            # apply masks to abscissa
            abscissa = variable[mask]
            abscissa = abscissa[maskii].reshape(-1, 1)

            # try
            try:

                # create the regression machine
                machine = LinearRegression()
                regression = machine.fit(abscissa, ordinate)

                # get the attributes
                slope = float(regression.coef_)
                intercept = float(regression.intercept_)
                score = regression.score(abscissa, ordinate)

            # unless there are problems
            except (ValueError, TypeError):

                # in which set defaults
                slope = 0.0
                intercept = 0.0
                score = 0.0

        # unless a key error
        except KeyError:

            # in which set defaults
            slope = 0.0
            intercept = 0.0
            score = 0.0

        return slope, intercept, score

    def _trim(self, data, outlier):
        """Trim data according to an outlier limit.

        Arguments:
            data: list of float
            outlier: float, stdev limit

        Returns:
            list of float
        """

        # calculate mean and std of data
        mean = numpy.mean(data)
        deviation = numpy.std(data) or 1.0

        # calcualte alll z-scores
        zees = [(datum - mean) / deviation for datum in data]

        # remove outliers
        trimmed = [datum for datum, zee in zip(data, zees) if abs(zee) <= outlier]

        return trimmed

    def _wash(self, icon, degree, destination):
        """Wash out a solid icon and redestination.

        Arguments:
            icon: str, filepath to icon
            degree: float, lightening factor
            destination: str, destination filepath

        Returns:
            None
        """

        # open imagge with context manager
        with Image.open(icon) as pointer:

            # open up image and convert to array
            image = numpy.array(pointer)

            # multiply by lightening factor
            image = image * degree
            image = image.astype('uint8')

            # redestination
            Image.fromarray(image).save(destination)

        return None

    def _whet(self):
        """Define default custom tool attributes.

        Arguments:
            None

        Returns:
            dict of lists
        """

        # set outliers in z-score values
        outliers = [5, 3, 2, 1, 1000, 100, 10]

        # set scaling options
        normalizations = ['absolute', 'percent', 'logarithm', 'sigma']

        # set legend corners
        corners = ['bottom_left', 'top_left', 'top_right', 'bottom_right']

        # set palettes
        spectra = ['triple', 'double', 'octopus', 'earth', 'hot', 'cool', 'classic', 'rainbow', 'acid', 'clouds']

        # set font sizes
        fonts = ['12pt', '13pt', '14pt', '15pt', '16pt', '17pt', '9pt', '10pt', '11pt']

        # set maker styles and gauges
        styles = ['dot', 'small', 'square', 'large', 'star', 'triangle', 'asterisk', 'cross', 'blank']

        # set list of secondary axis possibilities
        coaxes = ['twin', 'duel', 'percent', 'season', 'azimuth', 'longitude', 'track']

        # set pen toggle
        pen = [0.0001, 0.2, 0.5, 1, 2, 5]

        # set marker shapes, sizes, alphas, line widths
        gauges = {'dot': ('dot', 10, 1.0, 0.5), 'small': ('circle', 3, 1.0, 0.5), 'large': ('circle', 4, 1.0, 0.5)}
        gauges.update({'star': ('star', 5, 1.0, 0.5), 'triangle': ('triangle_pin', 5, 1.0, 0.5)})
        gauges.update({'square': ('square_pin', 5, 1.0, 0.5), 'asterisk': ('asterisk', 12, 1.0, 2.0)})
        gauges.update({'cross': ('x', 10, 1.0, 2.0), 'blank': ('dot', 0.0, 0.0, 0.0)})

        # rewrite gauges as dictionary
        for name, traits in gauges.items():

            # construct traits as dict
            marker, size, alpha, width = traits
            record = {'marker': marker, 'size': size, 'alpha': alpha, 'width': width}
            gauges[name] = record

        # create record
        whetstone = {'outliers': outliers, 'normalizations': normalizations, 'corners': corners, 'pen': pen}
        whetstone.update({'fonts': fonts, 'spectra': spectra, 'styles': styles, 'coaxes': coaxes, 'gauges': gauges})

        return whetstone

    def _wrack(self, feature, abbreviation):
        """Bundle up a heat map.

        Arguments:
            feature: feature instance
            abbreviation: file abbreviation

        Returns:
            list of blooms
        """

        # begin bundle
        bundle = []

        # grab the top layer of data
        data = feature.distil()[0]

        # create abscissa from longitude (adding padding in case of emtpy bins
        longitude = feature.spill()[2].tolist()
        length = len(longitude)
        abscissa = '{}_abscissa'.format(feature.name)
        self.variables[feature.path] = self.variables.get(feature.path, {})
        self.variables[feature.path][length] = self.variables[feature.path].get(length, {})
        self.variables[feature.path][length].update({abscissa: longitude})

        # make endpoints
        brackets = feature.attributes.get('brackets', [])
        data = numpy.where(data < brackets[0][0], brackets[0][0], data)
        data = numpy.where(data > brackets[-1][1], brackets[-1][1], data)

        # add the datum as independent variable
        abscissaii = '{}_data_abscissa'.format(feature.name)
        self.variables[feature.path][length].update({abscissaii: data.tolist()})

        # add the datum as independent variable
        gradient = '{}_gradient'.format(feature.name)
        self.variables[feature.path][length].update({gradient: ['#0' for _ in data.tolist()]})

        # check for brackets
        brackets = feature.attributes.get('brackets', [])
        defaults = ['{}-{}'.format(first, second) for first, second in brackets]
        labels = feature.attributes.get('labels', defaults)
        if len(brackets) < 1:

            # define percentile markers
            percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
            percentiles = [numpy.percentile(data, percent) for percent in percents]

            # for each bracket
            brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
            labels = ['{}-{}'.format(round(first, 2), round(second, 2)) for first, second in brackets]

        # calculate lenghs of each label based on major and minor characters
        symbols = [' ', '.', '-']
        majors = []
        minors = []
        lengths = []
        for label in labels:

            # get major and minor characters
            major = len([character for character in label if character not in symbols])
            minor = len([character for character in label if character in symbols])
            length = minor + 2 * major

            # append
            majors.append(major)
            minors.append(minor)
            lengths.append(length)

        # right justify labels
        longest = max(lengths)
        justifications = []
        for label, major, minor in zip(labels, majors, minors):

            # calculate pad
            pad = longest - (minor + 2 * major)
            justification = (pad * ' ') + label
            justifications.append(justification)

        # reinstate labels
        labels = justifications

        # create masks
        def functional(selection): return lambda tensor: tensor[1][selection].astype('f4')
        for index, (first, second) in enumerate(brackets):

            # construct address
            formats = (string.ascii_uppercase[index], int((first + second) / 2))
            tag = ''.join((['  '] * 4 + [digit for digit in str(int(second))])[-4:])
            tag = labels[index]

            # get total lenght of data
            total = len(data)

            # complete address
            address = [abbreviation, feature.name, '{}'.format(tag)]

            # create mask for particular range
            mask = (data >= first) & (data <= second)
            selection = [indexii for indexii, datum in enumerate(data) if (datum >= first) and (datum <= second)]

            # create function from latitude
            function = functional(selection)

            # check for polygons
            polygons = [[], []]
            spill = feature.spill()
            if len(spill) > 3:

                # go through the data
                for indexii, value in enumerate(data):

                    # check for value within bracket
                    if (value >= first) & (value <= second):

                        # add polygon
                        horizontals = [coordinate for coordinate in spill[7:11, indexii]]
                        verticals = [coordinate for coordinate in spill[3:7, indexii]]

                        # add to list
                        polygons[0].append(horizontals)
                        polygons[1].append(verticals)

                # add fake polygons if for empy list
                if len(polygons[0]) < 1:

                    # add fake polygons
                    polygons[0].append([self.fill, self.fill, self.fill])
                    polygons[1].append([self.fill, self.fill, self.fill])

            # otherwise
            else:

                # add fake polygons
                polygons = [[self.fill], [self.fill]]

            # add to bundle
            bloom = Bloom(feature, address, function, None, [abscissa, abscissaii, gradient])
            bloom.attributes = bloom.attributes.copy()
            bloom.attributes['polygons'] = polygons
            bloom.attributes['order'] = index
            bloom.attributes['selection'] = selection
            bloom.attributes['total'] = total
            bundle.append(bloom)

        return bundle

    def _xerox(self, graph, destination):
        """Export the graph as a png.

        Arguments:
            graph: bokeh Figure instance
            destination: str, filepath

        Returns:
            None
        """

        # export as png
        export_png(graph, filename=destination)

        return None

    def apply(self, filter, features=None, discard=False):
        """Apply a filter to a list of records.

        Arguments:
            filter: function object
            features=None: list of dicts
            discard: boolean, discard the rest

        Returns:
            list
        """

        # default features to entire collection
        features = features or list(self)

        # apply filter
        survivors = [feature for feature in features if filter(feature)]

        # sort survivors first by product of dimensions, then by number of dimensions
        survivors.sort(key=lambda feature: feature.name)
        survivors.sort(key=lambda feature: numpy.prod(feature.shape), reverse=True)
        survivors.sort(key=lambda feature: len(feature.shape), reverse=True)

        # if it is desired to discard the features that don't meet the condition
        if discard:

            # repopulate
            self._populate(survivors, discard=discard)

        return survivors

    def categorize(self, graph):
        """Perform time series analysis and draw a graph.

        Arguments:
            graph: bokeh graph object
            fraction=0.8: amount of training data

        Returns:
            None
        """

        # find right cell in the page
        cell = None
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].children[0].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[6].children) < 1:

            # get all visible lines from all selected graphs
            plot = cell.children[1].children[0]
            line = [line for line in plot.renderers if line.visible][0]
            label = '{}/{}'.format(plot.title.text, line.name)

            # get blooms
            def dubbing(bloom): return '/'.join(bloom.designation + [bloom.petal])
            bloom = self.apply(lambda member: dubbing(member) == label)[0]

            # fill bloom
            bloom.fill(bloom.function, bloom.subset)

            # begin suite
            suite = [bloom]

            # get abscissa and convert to integers
            abscissa = self.variables[bloom.path][bloom.shape[-1]][bloom.abscissas[0]]

            # create integers from abscissa
            integers = self._digitize(abscissa)

            # create clusters
            clusters = self._cluster(bloom.spill(), integers, bloom.void)

            # for each cluster
            for index, cluster in enumerate(clusters):

                # create clone
                clone = bloom.clone()
                clone.instil(cluster)
                clone.petal += '_cluster{}'.format(index + 1)
                clone.designation[-1] += '_cluster{}'.format(index + 1)
                suite.append(clone)

            # make comparison graph
            analysis = self.paint(suite, visible=True)

            # add graph to cell
            cell.children[6].children.append(analysis)
            print('clustered.')

        # otherwise
        else:

            # pop out member
            cell.children[6].children.pop()

        return None

    def compare(self, graph):
        """Draw a graph of all selected lines for comparison.

        Arguments:
            graph: bokeh graph object

        Returns:
            None
        """

        # find right cell in the page
        cell = None
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].children[0].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[3].children) < 1:

            # find all selected graphs
            cells = [cell for cell in self.page.children[1].children if len(cell.children) > 1]
            selections = [cell.children[1].children[0] for cell in cells if cell.children[1].visible]

            # get all visible lines from all selected graphs
            lines = [line for plot in selections for line in plot.renderers if line.visible]
            indices = [int(line.name) for line in lines if line.name]
            indices = self._skim(indices, maintain=True)
            suite = [self[index] for index in indices]

            # make comparison graph
            comparison = self.paint(suite, visible=True)

            # add graph to cell
            cell.children[3].children.append(comparison)
            print('compared.')

        # otherwise
        else:

            # pop out member
            cell.children[3].children.pop()

        return None

    def correlate(self, graph):
        """Draw a graph correlating first graph with others.

        Arguments:
            graph: bokeh graph object

        Returns:
            None
        """

        # find right cell in the page
        cell = None
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].children[0].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[4].children) < 1:

            # find all selected graphs
            cells = [cell for cell in self.page.children[1].children if len(cell.children) > 1]
            selections = [cell.children[1].children[0] for cell in cells if cell.children[1].visible]

            # get all visible lines from all selected graphs
            lines = [line for plot in selections for line in plot.renderers if line.visible]
            indices = [int(line.name) for line in lines if line.name]
            indices = self._skim(indices, maintain=True)
            blooms = [self[index] for index in indices]

            # fill all bouqeuets
            [bloom.fill(bloom.function, bloom.subset) for bloom in blooms]

            # only accept those with the length matching the first
            size = blooms[0].shape[-1]
            blooms = [bloom for bloom in blooms if bloom.shape[-1] == size]

            # make all 2 member paris, and only keep first 12
            pairs = itertools.combinations(list(range(len(blooms))), 2)

            # begin ranks of all correlations
            ranks = []
            for first, second in pairs:

                # compute the error matrix and correlation coefficient
                matrix, score = self._relate(blooms[first].spill(), blooms[second].spill())
                matrix = numpy.array(matrix)

                # clone the first
                clone = blooms[first].clone()

                # add new matrix
                clone.instil(matrix)

                # add to label
                formats = [blooms[first].bunch[0][:5], blooms[first].petal]
                formats += [blooms[second].bunch[0][:5], blooms[second].petal]
                formats += [round(float(score), 2)]
                clone.petal = '{}/{}_vs_{}/{} ({})'.format(*formats)
                clone.attributes['units'] = 'error ( stdevs )'
                clone.units = 'error ( stdevs )'

                # add to suite
                ranks.append((clone, score))

            # sort and take top 12
            ranks.sort(key=lambda rank: rank[1], reverse=True)
            suite = [clone for clone, _ in ranks[:12]]

            # default to first entry if empty
            if len(suite) < 1:

                # add first entry
                suite = [blooms[0]]

            # make comparison graph
            correlation = self.paint(suite, visible=True)

            # add graph to cell
            cell.children[4].children.append(correlation)
            print('correlated')

        # otherwise
        else:

            # pop out member
            cell.children[4].children.pop()

        return None

    def cultivate(self, variants):
        """Proceed through multiple propagions.

        Arguments:
            variants: list of dicts, the yam key paths and their new values.

        Returns:
            None
        """

        # set up chrome driver
        self._chrome()

        # for each variant
        for variant in variants:

            # update the yam
            for slash, datum in variant.items():

                # split slash
                fields = slash.split('/')

                # begin with yam
                tree = self.yam
                for field in fields[:-1]:

                    # walk down triee
                    tree = tree[field]

                # deposit value
                tree[fields[-1]] = datum

            # propgate
            self.propagate()

        return None

    def detect(self, graph):
        """Perform time series analysis and draw a graph.

        Arguments:
            graph: bokeh graph object
            fraction=0.8: amount of training data

        Returns:
            None
        """

        # find right cell in the page
        cell = None
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].children[0].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[7].children) < 1:

            # get all visible lines from all selected graphs
            plot = cell.children[1].children[0]
            line = [line for line in plot.renderers if line.visible][0]
            label = '{}/{}'.format(plot.title.text, line.name)

            # get blooms
            def dubbing(bloom): return '/'.join(bloom.designation + [bloom.petal])
            bloom = self.apply(lambda member: dubbing(member) == label)[0]

            # fill bloom
            bloom.fill(bloom.function, bloom.subset)
            bloom.plug()

            # begin suite
            suite = [bloom]

            # get abscissa and convert to integers
            abscissa = self.variables[bloom.path][bloom.shape[-1]][bloom.abscissas[0]]

            # create integers from abscissa
            integers = self._digitize(abscissa)

            # use regression to forecast
            normals, anomalies = self._support(bloom.spill(), integers, bloom.void)

            # clone the first
            clone = bloom.clone()
            clone.instil(normals)
            clone.petal += '_normal'
            clone.designation[-1] += '_normal'
            suite.append(clone)

            # clone the second
            clone = bloom.clone()
            clone.instil(anomalies)
            clone.petal += '_anomalous'
            clone.designation[-1] += '_anomalous'
            suite.append(clone)

            # make comparison graph
            analysis = self.paint(suite, visible=True)

            # add graph to cell
            cell.children[7].children.append(analysis)
            print('detected.')

        # otherwise
        else:

            # pop out member
            cell.children[7].children.pop()

        return None

    def garden(self, *words, exclusions=None, inclusions=None):
        """Create plots from first entry in every file.

        Arguments:
            *words: unpacked list of keywords
            exclusions: list of str, file keywords to exclude
            inclusions: list of str, file keywords to include anyway

        Returns:
            None
        """

        # create hydra
        hydra = Hydra(self.source)

        # set defaults
        exclusions = exclusions or []
        inclusions = inclusions or []

        # get source paths
        paths = [path for path in hydra.paths]

        # if words are given
        if len(words) > 0:

            # only include paths with those words
            paths = [path for path in paths if any([word in path for word in words])]

        # create list of excluded paths
        excluded = [path for path in paths if any([exclusion in path for exclusion in exclusions])]

        # create list of included paths
        included = [path for path in paths if any([inclusion in path for inclusion in inclusions])]

        # apply exlusions and inclusiuons and remove duplicates
        paths = [path for path in paths if path not in excluded] + included
        paths = self._skim(paths)
        paths.sort()

        # create seeds
        seeds = []
        for path in paths:

            # ingest
            hydra.ingest(path)

            # make stub
            stub = path.split('/')[-1].split('.')[0]

            # make seed from first feature
            seed = ([''], hydra[0].name, stub, [path])
            seeds.append(seed)

        # create variants
        variants = []
        for seed in seeds:

            # add variants
            variants += self.varigate(*seed)

        # cultivate
        self.cultivate(variants)

        return None

    def imagine(self, graph, destination):
        """Render graph with matplotlib

        Arguments:
            graph: boke graph object
            destination: str, pathname for destination

        Returns:
            None
        """

        # print status
        print('rendering in matplotlib...')

        # create matplotlib figure
        figure = pyplot.figure(figsize=(8, 3))
        axis = figure.add_subplot(111)

        # set up figure attributes
        axis.set_xlabel(graph.xaxis[0].axis_label)
        axis.set_ylabel(graph.yaxis[0].axis_label)
        axis.set_xlim((graph.x_range.start, graph.x_range.end))
        axis.set_ylim((graph.y_range.start, graph.y_range.end))

        # set background color
        background = self.yam.get('background', '#ffffff')
        axis.set_facecolor(background)

        # set fonts
        pyplot.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Arial'], 'size': 16})
        for axisii in ['xaxis', 'yaxis']:

            # center label
            pyplot.rc(axisii, labellocation='center')

        # get graph glyphs
        for entry in graph.legend.items:

            # get renderer
            for renderer in entry.renderers:

                # check for visibile scatter plot
                if renderer.visible and 'Patches' in str(renderer.glyph.__class__):

                    # get attibutes
                    horizontals = renderer.data_source.data['xs']
                    verticals = renderer.data_source.data['ys']
                    color = renderer.glyph.fill_color

                    # make patches
                    patches = []
                    for horizontal, vertical in zip(horizontals, verticals):

                        # make patch
                        polygon = numpy.array([horizontal, vertical]).transpose(1, 0)
                        patch = matplotlib.patches.Polygon(xy=polygon)
                        patches.append(patch)

                    # make patch collection
                    collection = matplotlib.collections.PatchCollection(patches, color=color, linewidth=0)
                    axis.add_collection(collection)

                    # # check for length
                    # if len(horizontal) > 5:
                    #
                    #     # plot
                    #     size = self.yam['gauges']['dot']['size']
                    #     parameters = {'marker': '.', 'linewidth': 0, 'label': entry.label['value']}
                    #     parameters.update({'markersize': size})
                    #     axis.plot(horizontal, vertical, color, **parameters)


        # # get graph glyphs
        # for entry in graph.legend.items[:1]:
        #
        #     # get renderer
        #     for renderer in entry.renderers[:1]:
        #
        #         # get attibutes
        #         horizontal = renderer.data_source.data['x']
        #         vertical = renderer.data_source.data['y']
        #
        #         # add to grid
        #         for point in zip(horizontal, vertical):
        #
        #             # add to grid
        #             grid[int(point[1]), int(point[0])] = 1


        # # get color map
        # colormap = ListedColormap(["#ffffef","black"])
        # figure, axis = pyplot.subplots(1,1, figsize=(8, 5))
        #
        # # map image
        # image=axis.imshow(grid, aspect=4, origin="lower", interpolation="nearest", cmap=colormap)

        # set axis properties
        axis.set_xlim(-1,752)
        axis.set_ylim(-1,60)
        # axis.set_ylabel("Pixel Spatial Index")
        # axis.set_xlabel("Pixel Spectral Index")
        axis.tick_params(axis='x', which='minor', bottom=False)
        axis.tick_params(axis='y', which='minor', left=False)
        axis.tick_params(axis='x', which='minor', top=False)
        axis.tick_params(axis='y', which='minor', right=False)
        axis.tick_params(axis="both",length=0)

        # for every format
        for extension in ['.eps','.png']:

            # save to destination
            pyplot.savefig(destination.replace('.png', extension), bbox_inches='tight')

        # clear figure
        pyplot.clf()

        return None

    def interpret(self, filter, limit=5500):
        """Rank the data according to sample entropy scores.

        Arguments:
            filter: function object, filter to subset data
            limit: int, last number of points to consider

        Returns:
            None
        """

        # interpretting
        self._stamp('interpretting...', initial=True)

        # fill all members
        self._stamp('filling subset...')
        blooms = [bloom for bloom in self if filter(bloom)]
        [bloom.fill(bloom.function, bloom.subset) for bloom in blooms]

        # plug with void values
        self._stamp('plugging...')
        [bloom.plug() for bloom in blooms]

        # for each seriese
        self._stamp('calculating entropies...')
        entropies = []
        for index, bloom in enumerate(blooms):

            # add counter
            print('{}, {} of {}...'.format(bloom.sprig, index, len(blooms)))

            # try to
            try:

                # calculate sample entropy based on latest year's worth, after removing voids
                series = bloom.spill()
                series = series[series != bloom.void][-limit:]
                entropy = self._enterprise(series)

                # if entropy is not nan
                if abs(entropy) >= 0:

                    # append
                    entropies.append((entropy, bloom.sprig))
                    #print('adding: {}, {} ({} of {})'.format(bloom.sprig, entropy, index, len(blooms)))

            # unless a feature with flexible type
            except (TypeError, ValueError):

                # in which case, skip
                print('skipped {}.'.format(bloom.sprig))
                pass

        # sort entropies
        self._stamp('sorting...')
        entropies.sort(key=lambda pair: pair[0], reverse=True)

        # make report
        report = ['{}: {}'.format(round(abs(pair[0]), 6), pair[1]) for pair in entropies]
        self._jot(report, 'entropy.txt')

        return None

    def kill(self, name, _):
        """Kill a sparkline.

        Arguments:
            name: str
            _:  bokeh event dict

        Returns:
            str
        """

        # print status
        print('killing {}...'.format(name))

        # go through active graphs
        kills = []
        for index, entry in enumerate(self.page.children[1].children):

            # check for name in labels
            if name in entry.children[0].children[0].labels:

                # add index to the list
                kills.append(index)

        # for each index
        kills.sort(reverse=True)
        for index in kills:

            # reset button
            self.page.children[1].children[index].children[0].children[0].active = []

            # pop off spark
            self.page.children[1].children.pop(index)

        return None

    def manifest(self, suite):
        """Draw the plot based on a feature.

        Arguments:
            suite: list of feature instances

        Returns:
            list of bokeh objects.
        """

        # paint graph
        panel = self.paint(suite)
        children = [panel]
        graph = panel.children[0]

        # define checkbox margin
        margin = (10, 10, 5, 5)
        width = 190

        # append an empty row and several empty columns
        children.append(Row([], width_policy='fit'))
        [children.append(Column([])) for _ in range(5)]

        # add default arguments
        arguments = {'width': width, 'visible': True, 'margin': margin, 'width_policy': 'fit'}

        # add comparison checkboxes
        box = CheckboxGroup(labels=['Compare selected parameters'], active=[], **arguments)
        box.on_click(lambda _: self.compare(graph))
        children[1].children.append(box)

        # add correlation checkboxes
        box = CheckboxGroup(labels=['Correlate with selected parameters'], active=[], **arguments)
        box.on_click(lambda _: self.correlate(graph))
        children[1].children.append(box)

        # add autoregression forecast
        box = CheckboxGroup(labels=['Forecast with autoregression'], active=[], **arguments)
        box.on_click(lambda _: self.regress(graph))
        children[1].children.append(box)

        # add clustering
        box = CheckboxGroup(labels=['Categorize with Kmeans'], active=[], **arguments)
        box.on_click(lambda _: self.categorize(graph))
        children[1].children.append(box)

        # add clustering
        box = CheckboxGroup(labels=['Detect anomalies with SVM'], active=[], **arguments)
        box.on_click(lambda _: self.detect(graph))
        children[1].children.append(box)

        # timestamp
        self._stamp('added to page.')

        return children

    def paint(self, suite, visible=False, tools=True):
        """Draw the plot based on a suite of blooms.

        Arguments:
            suite: list of Bloom instances
            labels: list of alternative labels
            visible: boolean, make all lines visible?
            tools: boolean, make toolbar visible?

        Returns:
            bokeh column.
        """

        # set visible flag
        visible = visible or self.yam.get('visibility', False)

        # order blooms in suite
        if all(['order' in bloom.attributes.keys() for bloom in suite]):

            # order by this ordering
            suite.sort(key=lambda bloom: bloom.attributes['order'])

        # timestamp
        self._stamp('painting...', initial=True)

        # parse suite names into title words and legend names, and overwrite if supplied
        title, labels = self._entitle(suite)

        # begin the data reservoir
        self._stamp('extracting data...')
        reservoir = self._reserve(suite, labels, visible)

        # create data table sources
        self._stamp('making tables...')
        sources, trends = self._tabularize(suite, reservoir)

        # prepare figure
        self._stamp('preparing figure...')
        graph, scenes = self._frame(title, reservoir)

        # add secondary axis
        self._stamp('adding secondary axis...')
        stage = self._accentuate(graph, reservoir)

        # scale graph according to normalization
        self._scale(graph, reservoir, sources)

        # draw the lines
        self._stamp('graphing lines...')
        glyphs, clickers, variables = self._draw(graph, reservoir, sources, trends, visible)

        # add custom tools
        self._stamp('adding custom tools...')
        self._customize(graph, reservoir, sources, glyphs, clickers, stage, scenes)

        # add hover tools for annotations
        self._stamp('adding annotations...')
        self._annotate(graph, reservoir, sources, trends, tools=tools)

        # add customization boxes
        self._stamp('adding customization...')
        column = self._contextualize(graph, reservoir, clickers)

        # timestamp
        self._stamp('painted.')

        return column

    def pick(self, search, reference=None):
        """Pick for blooms with specific members in their route.

        Arguments:
            search: slashed search string
            reference: dict of feature lists, or feature list

        Returns:
            list of dicts
        """

        # set reference to all features
        reference = reference or self.reference

        # if the reference is not a dictionary
        if reference == list(reference):

            # assume list of features and convert
            reference = self._refer(reference)

        # if the search term is in the reference
        treasure = []
        if search in reference.keys():

            # get features
            treasure += reference[search]

        # otherwise
        else:

            # check for all terms in the keys, excluding single keys
            keys = [key for key in reference.keys() if '/' in key]
            keys = [key for key in keys if all([field in key for field in search.split('/')])]
            for key in keys:

                # add references
                treasure += reference[key]

        return treasure

    def probe(self, path, band, row, wavelength, bracket):
        """Probe the anomaly structure at a particular row and wavelength bin.

        Arguments:
            path: str, filepath
            row: int, row number
            wavelength: int, wavelength
            bracket: pair of ints, orbit numbers for annotation

        Returns:
            None
        """

        # collect all blooms with the path
        blooms = [bloom for bloom in self if bloom.path == path]

        # subset radiances
        blooms = self.dig('band{}'.format(band), blooms)
        blooms = [bloom for bloom in blooms if str(wavelength) in bloom.stem and str(row) in bloom.petal]
        bloom = blooms[0]

        # fill bloom
        bloom.fill(bloom.function, bloom.subset)
        bloom.plug()

        # get abscissa and convert to integers
        orbits = self.variables[bloom.path][bloom.shape[-1]]['orbit_number']
        integers = self._digitize(orbits)
        radiances = bloom.spill()

        # prepare abscissaal sequences
        training, verification, samples = self._prepare(integers, self.window, self.fraction, center=True)

        # create registry
        registry = {abscissa: datum for abscissa, datum in zip(integers, radiances) if datum != bloom.void}

        # calculate average of all registry data
        average = sum(registry.values()) / len(registry)

        # generate targets and training matrix
        matrix = []
        for sequence in verification + samples:

            # create vector for sample and add to matrix
            vector = [registry.get(number, average) for number in sequence]
            matrix.append(vector)

        # use svm to detect anomalies
        _, anomalies = self._support(radiances, integers, bloom.void)

        # compress matrix to 2 dimensions
        matrix = numpy.array(matrix)
        compressor = PCA(n_components=3)
        decomposition = compressor.fit_transform(matrix)

        # try all thee permutations
        events = {number: number - 5 for number in range(11)}
        permutations = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9), (9, 10)]
        for first, second in permutations:

            # begin plot
            pyplot.clf()
            pyplot.title('SVN Anomalies, Band{}, Row {}, {}nm'.format(band, row, wavelength), fontsize=30)

            # get axis labels
            def signing(number): return str(number) if number < 0 else '+{}'.format(number)
            one = 'radiance, t{}'.format(signing(events[first]))
            two = 'radiance, t{}'.format(signing(events[second]))

            # add to axis labels
            pyplot.xlabel(str(one), fontsize=30)
            pyplot.ylabel(str(two), fontsize=30)
            pyplot.xticks(fontsize=30)
            pyplot.yticks(fontsize=30)
            pyplot.gca().xaxis.get_offset_text().set_fontsize(30)
            pyplot.gca().yaxis.get_offset_text().set_fontsize(30)

            # plot normal points
            horizontals = [point[first] for point, radiance in zip(matrix, anomalies) if radiance == self.fill]
            verticals = [point[second] for point, radiance in zip(matrix, anomalies) if radiance == self.fill]
            pyplot.plot(horizontals, verticals, 'cx', markersize=5)

            # plot anomalies points
            horizontals = [point[first] for point, radiance in zip(matrix, anomalies) if radiance != self.fill]
            verticals = [point[second] for point, radiance in zip(matrix, anomalies) if radiance != self.fill]
            pyplot.plot(horizontals, verticals, 'mx', markersize=15)

            # annotate points
            for orbit, point in zip(orbits, matrix):

                # check for bracket
                if bracket[0] - 170 <= orbit <= bracket[1] + 170:

                    # set font size
                    size = 30 if orbit in bracket else 20

                    # annotate
                    pyplot.annotate(str(int(orbit)), xy=(point[first], point[second]), fontsize=size)

            # save figure
            formats = (self.source.replace('data', 'plots'), bracket[0], band, row, wavelength, str(first).zfill(2))
            pyplot.savefig('{}/svn_{}_band_{}_row_{}_{}_{}.png'.format(*formats))

        return None

    def propagate(self, screen=False, static=False):
        """Churn out plots.

        Arguments:
            None

        Returns:
            None
        """

        # get propagation attributes
        propagation = self.propagation
        self.spectra = self.yam['spectra']

        # set default primary and secondary
        primaries = ['']
        secondaries = ['']
        self._print('propagating...')

        # try to
        first = propagation['primary']
        length = 2
        try:

            # if there are specific entries
            if first['entries']:

                # get entries from string
                entries = [entry.strip() for entry in first['entries'].split(',')]
                length = max([len(entry) for entry in entries])
                primaries = [str(entry).zfill(length) for entry in entries]

            # otherwise
            else:

                # determine rows
                number = int((first['finish'] - first['start']) / first['step']) + 1
                primaries = [str(first['start'] + index * first['step']).zfill(length) for index in range(number)]

        # unless information
        except (ValueError, TypeError):

            # in which case, pass
            pass

        # try to
        second = propagation['secondary']
        lengthii = 5
        try:

            # if there are specific entries
            if second['entries']:

                # get entries from string
                entries = [entry.strip() for entry in second['entries'].split(',')]
                lengthii = max([len(entry) for entry in entries])
                secondaries = [str(entry).zfill(lengthii) for entry in entries]

            # otherwise
            else:

                # determine wavelengths
                number = int((second['finish'] - second['start']) / second['step']) + 1
                waves = [str(second['start'] + index * second['step']) for index in range(number)]

        # unless information
        except (ValueError, TypeError):

            # in which case, pass
            pass

        # get all parameters
        parameters = propagation['parameters']

        # print
        self._look(parameters, 3)

        # grab propagation folder
        folder = propagation['folder']
        stub = propagation['stub']

        # create folder
        self._make(folder)

        # get title base
        self.title = self.yam['title'] or self.title
        base = self.title

        # set labels and colors
        self.labels = [entry['label'] for entry in self.yam['lines']]
        self.colors = [entry['color'] for entry in self.yam['lines']]

        # collection all steps
        steps = list(set([parameter['step'] for parameter in parameters]))
        steps.sort()

        # grab information
        informations = propagation['information']
        count = 0

        # for each row
        for primary in primaries:

            # and each wavelength
            for secondary in secondaries:

                # get information and increment count
                information = informations[count % len(informations)]
                count += 1

                # try
                try:

                    # collect all row numbers
                    numbers = ' / '.join([str(step * int(primary)).zfill(length) for step in steps])

                # unless valuerror
                except ValueError:

                    # blankify string
                    numbers = ''

                # reset title
                self.title = '{}, {} {} {}'.format(base, first['name'], numbers, information)
                self.title = self.title.strip(',')

                # if there is a wavelength
                if secondary:

                    # add the wavelength
                    formats = (base, first['name'], numbers, second['name'], secondary, information)
                    self.title = '{}, {}: {}, {}: {} {}'.format(*formats)
                    self.title = self.title.strip(',')

                # begin suite
                suite = []

                # go through each member
                for member in parameters:

                    # get file number
                    number = member['file']

                    # if file is a string
                    if number == str(number):

                        # get the file numbers
                        paths = [path for path in self.paths if all([segment in path for segment in number.split('/')])]

                        # get number
                        number = [entry for entry, path in enumerate(self.paths) if path == paths[0]][0]

                    # get file subset
                    candidates = self.apply(lambda bloom: bloom.path == self.paths[number], self)

                    # unpack member
                    name, step, tag = member['name'], member['step'], member.get('tag', '')

                    # if given a wavelength
                    if secondary:

                        # get particular bloom
                        search = '{}_{}'.format(name, str(int(primary) * step).zfill(length))
                        blooms = self.apply(lambda entry: search in entry.sprig, candidates)
                        petals = ['_' + str(secondary), '_' + str(round(float(secondary), 1)).zfill(lengthii + 2)]
                        blooms = [bloom for bloom in blooms if any([petal == bloom.petal for petal in petals])]
                        suite.append(blooms[0])

                    # otherwise
                    else:

                        # if a given primary
                        if primary:

                            # get particular bloom using only row
                            search = '{}'.format(name)
                            blooms = self.apply(lambda entry: search in entry.sprig, candidates)
                            petal = '_' + str(int(primary) * step).zfill(2)
                            blooms = [bloom for bloom in blooms if petal == bloom.petal]
                            suite.append(blooms[0])

                        # otherwise
                        else:

                            # check for tag
                            if tag:

                                # get blooms using tag for petal
                                search = '{}/{}'.format(name, tag)
                                blooms = self.apply(lambda entry: search in entry.sprig, candidates)
                                # petal = '_' + str(int(primary) * step).zfill(2)
                                # blooms = [bloom for bloom in blooms if petal == bloom.petal]
                                [suite.append(bloom) for bloom in blooms]

                            # otherwise, handle heatamp case
                            else:

                                # get blooms using tag for petal
                                search = '{}'.format(name)
                                blooms = self.apply(lambda entry: search in entry.sprig, candidates)
                                # petal = '_' + str(int(primary) * step).zfill(2)
                                # blooms = [bloom for bloom in blooms if petal == bloom.petal]
                                [suite.append(bloom) for bloom in blooms]
                                self.title = self.title.strip(',')

                # make the painting
                column = self.paint(suite, visible=True, tools=False)

                # if screen
                if screen:

                    # add to page
                    self.page.children[-1].children.append(column)

                # otherwise
                else:

                    # print status
                    self._stamp('sending image to html...')

                    # send figure to html
                    suffix = self.ticket.split('/')[-1].split('.')[0]
                    figure = column.children[0]

                    # set destination name
                    secondary = secondary or '00'
                    #destination = '{}/{}_{}_{}.png'.format(folder, stub, str(primary).zfill(2), secondary)
                    destination = '{}/{}.png'.format(folder, stub)

                    # check for matplot lib
                    if static:

                        # check for heatmap
                        if 'heatmap' in suite[0].name:

                            # render image with matplotlib
                            self.imagine(figure, destination)

                        # otherwise
                        else:

                            # render with matplotlib
                            self.render(figure, destination)

                    # otherwise
                    else:

                        # set destination
                        destination = '{}/{}.png'.format(folder, stub)
                        storage = 'html_{}.png'.format(suffix)

                        # construct size
                        width = self.yam.get('width', 1000)
                        height = int(width / self.yam.get('aspect', 1.33))
                        # width = int(width * 1.2)

                        # # scrreenshot
                        # image = get_screenshot_as_png(figure, height=height, width=width, driver=chrome)
                        # image.save(storage)
                        # image.save(destination)

                        # set border margins, chrome cuts off border
                        figure.min_border_right = 150
                        figure.min_border_left = 0

                        # export to png
                        self._stamp('exporting {} to png...'.format(destination), initial=True)
                        export_png(figure, filename=destination, webdriver=self.chromium, timeout=3600)
                        #export_svg(figure, filename='test.svg')
                        self._stamp('exported.')

                        # # continue with html
                        # test = 'temp/html_{}.html'.format(suffix)
                        # output_file(filename=test)
                        # Save(figure)
                        #
                        # # assume blank page by default
                        # blank = True
                        #
                        # # while the page is blank
                        # while blank:
                        #
                        #     # print status
                        #     self._stamp('rendering with html2image...')
                        #
                        #     # set destination
                        #     destination = '{}/{}.png'.format(folder, stub)
                        #
                        #     # construct size
                        #     width = self.yam.get('width', 1000)
                        #     height = int(width / self.yam.get('aspect', 1.33))
                        #     size = (width * 10, height * 10)
                        #     size = (8000, 6000)
                        #     size = (1, 1)
                        #
                        #     print('size: ', size)
                        #
                        #     # make html2image instance
                        #     flags = ['--default-background-color=00000000', '--hide-scrollbars']
                        #     #renderer = Html2Image(custom_flags=flags, size=size)
                        #     renderer = Html2Image(custom_flags=flags)
                        #     storage = 'html_{}.png'.format(suffix)
                        #     with open(test) as pointer:
                        #
                        #         # take screenshot
                        #         specifications = ['body {height: 2000px;}', 'body {width: 4000px;}']
                        #         #renderer.screenshot(pointer.read(), save_as=storage, size=size, css_str=specifications)
                        #         renderer.screenshot(pointer.read(), save_as=storage)
                        #
                        #     # # also transfer html to png using webkit
                        #     # destination = '{}/{}_{}_{}.png'.format(folder, stub, str(primary).zfill(2), secondary)
                        #     # replacement = destination.replace('.png', '')
                        #     # command = ["webkit2png", "--delay=1", "-Fs" "2", test, "-o", replacement]
                        #     # print(command)
                        #     # sleep(5)
                        #     # subprocess.call(command)
                        #
                        #     # open up the file in context manager
                        #     with Image.open(storage) as pointer:
                        #
                        #         # crate array
                        #         image = numpy.array(pointer)
                        #
                        #         # find all blank rows and columns
                        #         rows = [index for index in range(image.shape[0]) if image[index, :, 3].sum() > 0]
                        #         columns = [index for index in range(image.shape[1]) if image[:, index, 3].sum() > 0]
                        #
                        #         # take subset
                        #         image = image[min(rows): max(rows), min(columns): max(columns)]
                        #
                        #         # convert back to image and save
                        #         image = Image.fromarray(image)
                        #         image.save(destination)
                        #
                        #         # set blank to false
                        #         blank = False
                                #
                                # # check mean at all white
                                # mean = image.mean()
                                # print('mean: {}'.format(mean))
                                # if mean < 253:
                                #
                                #     # carry one to next
                                #     blank = False

        # reset title
        self.title = base

        # collect garbage ( troubleshooting crashing after several screenshots )
        gc.collect()

        return None

    def regress(self, graph):
        """Perform time series analysis and draw a graph.

        Arguments:
            graph: bokeh graph object
            fraction=0.8: amount of training data

        Returns:
            None
        """

        # find right cell in the page
        cell = None
        for child in self.page.children[1].children:

            # check for length
            if len(child.children) > 1:

                # check for graph id
                if child.children[1].children[0].id == graph.id:

                    # set cell
                    cell = child

        # check for missing graph based on the cell length
        if len(cell.children[5].children) < 1:

            # get all visible lines from all selected graphs
            plot = cell.children[1].children[0]
            line = [line for line in plot.renderers if line.visible][0]
            label = '{}/{}'.format(plot.title.text, line.name)

            # get blooms
            def dubbing(bloom): return '/'.join(bloom.designation + [bloom.petal])
            bloom = self.apply(lambda member: dubbing(member) == label)[0]

            # fill bloom
            bloom.fill(bloom.function, bloom.subset)

            # begin suite
            suite = [bloom]

            # get abscissa and convert to integers
            abscissa = self.variables[bloom.path][bloom.shape[-1]][bloom.abscissas[0]]

            # create integers from abscissa
            integers = self._digitize(abscissa)

            # use regression to forecast
            fit, prediction = self._forecast(bloom.spill(), integers, bloom.void)

            # clone the first
            clone = bloom.clone()
            clone.instil(fit)
            clone.petal += '_fit'
            clone.designation[-1] += '_fit'
            suite.append(clone)

            # clone the second
            clone = bloom.clone()
            clone.instil(prediction)
            clone.petal += '_forecast'
            clone.designation[-1] += '_forecast'
            suite.append(clone)

            # make comparison graph
            analysis = self.paint(suite, visible=True)

            # add graph to cell
            cell.children[5].children.append(analysis)
            print('autoregressed.')

        # otherwise
        else:

            # pop out member
            cell.children[5].children.pop()

        return None

    def render(self, graph, destination):
        """Render graph with matplotlib

        Arguments:
            graph: boke graph object
            destination: str, pathname for destination

        Returns:
            None
        """

        # print status
        print('rendering in matplotlib...')

        # create matplotlib figure
        figure = pyplot.figure(figsize=(8, 5))
        axis = figure.add_subplot(111)

        # set fonts
        pyplot.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Arial'], 'size': 16})
        for axisii in ['xaxis', 'yaxis']:

            # center label
            pyplot.rc(axisii, labellocation='center')

        # set up figure attributes
        axis.set_xlabel(graph.xaxis[0].axis_label)
        axis.set_ylabel(graph.yaxis[0].axis_label)
        axis.set_xlim((graph.x_range.start, graph.x_range.end))
        axis.set_ylim((graph.y_range.start, graph.y_range.end))

        # set x ticks assuming years
        # axis.set_xlim((2004,2024))
        axis.tick_params('both', length=4, width=1, which='minor')
        axis.tick_params('both', length=8, width=2, which='major')
        axis.xaxis.set_major_locator(ticker.MultipleLocator(base=4))

        # if logarithm
        if self.yam.get('logarithm', False):

            # make axis logarithmic
            axis.set_yscale('log')
            axis.set_yticks([0.01, 0.1, 1.0, 10, 100])
            axis.yaxis.set_major_formatter(StrMethodFormatter('{x:.2f}'))

        # otherwise
        else:

            # for many trials
            span = graph.y_range.end - graph.y_range.start
            trials = [5, 10, 20, 25, 40, 50, 100]
            bases = [10 ** int(math.log10(span)) / trial for trial in trials]
            chunks = [span / base for base in bases]
            scores = [(chunk - 8) ** 2 for chunk in chunks]
            best = numpy.argsort(scores)[0]

            # set base
            base = self.yam.get('base', False) or bases[best]

            # set ticks using best base
            axis.yaxis.set_major_locator(ticker.MultipleLocator(base=base))

        # get graph glyphs
        for entry in graph.legend.items:

            # get renderer
            for renderer in entry.renderers:

                # check for visibile scatter plot
                if renderer.visible and 'Scatter' in str(renderer.glyph.__class__):

                    # get attibutes
                    horizontal = renderer.data_source.data['x']
                    vertical = renderer.data_source.data['y']
                    color = renderer.glyph.fill_color

                    # check for length
                    if len(horizontal) > 5:

                        # plot
                        size = self.yam['gauges']['dot']['size']
                        parameters = {'marker': '.', 'linewidth': 0, 'label': entry.label['value']}
                        parameters.update({'markersize': size})
                        axis.plot(horizontal, vertical, color, **parameters)

        # set up legend
        corners = {'top_left': 'upper left', 'bottom_left': 'lower left'}
        corners.update({'top_right': 'upper right', 'bottom_right': 'lower right'})
        corner = corners[self.yam['corners'][0]]
        scale = 10 / self.yam['gauges']['dot']['size']
        axis.legend(fontsize=12, loc=corner, markerscale=scale, labelcolor='linecolor')

        # for every format
        for extension in ['.eps', '.png']:

            # save to destination
            pyplot.savefig(destination.replace('.png', extension), bbox_inches='tight')

        # clear figure
        pyplot.clf()

        return None

    def select(self, column):
        """Select blooms from checkboxes.

        Arguments:
            column: column object

        Returns:
            str
        """

        # go through parameter labels, choosing those covered by enough selections
        labels = column.children[0].labels
        blooms = [self[index] for label in labels for index in self.stems[label]]

        # check for entries
        if len(blooms) > 0:

            # sort blooms by name
            blooms.sort(key=lambda bloom: bloom.stem)

            # group blooms by bunch
            suites = self._group(blooms, lambda bloom: '/'.join(bloom.designation))
            suites = list(suites.items())
            # suites.sort(key=lambda item: item[0].split(')')[1].strip())
            suites.sort(key=lambda item: item[1][0].path)

            # draw new sparkline plots
            news = [self.spark(name, suite) for name, suite in suites]
            pairs = [(name, suite) for name, suite in suites]

            self._print('')
            self._print('news:')
            self._tell(pairs)

            # insert new column at bottom
            [self.page.children[1].children.append(spark) for spark in news]

        return None

    def spark(self, name, suite):
        """Add the sparkline miniature plot.

        Arguments:
            name: str, slashed name of bloom branch
            suite: list of Feature instances

        Returns:
            bokeh object.
        """

        # check for current manifestation
        if self.manifestations.setdefault(name, None):

            return self.manifestations[name]

        # sort blooms by leaf, putting main on top
        suite.sort(key=lambda bloom: bloom.petal)
        suite.sort(key=lambda bloom: bloom.petal == 'mean', reverse=True)

        # make label from band and parameter names
        label = '/'.join(suite[0].designation)

        # make checkbox
        checkbox = CheckboxGroup(labels=[label], active=[], width=750, visible=True)
        checkbox.on_click(lambda _: self.toggle(name, suite))

        # make miniature figure
        miniature = Figure(sizing_mode="fixed", plot_width=200, plot_height=60, visible=True)
        miniature.xaxis.axis_label = suite[0].description[:30]

        # get the data from first bloom
        data = suite[0].distil(suite[0].function, suite[0].subset, refill=True).squeeze().astype('f8')

        # check for empty data
        if data.shape == ():

            # create data as zeros
            data = numpy.array([0, 0])

        # plot the line
        abscissa = [index for index, _ in enumerate(data)]
        miniature.line(x=abscissa, y=data, color='blue')

        # remove ticks
        miniature.xaxis.major_label_text_font_size = '0pt'
        miniature.yaxis.major_label_text_font_size = '0pt'

        # remove log
        miniature.toolbar.logo = None
        miniature.toolbar_location = None

        # add kill switch
        killer = CheckboxGroup(labels=['kill'], active=[], width=650, visible=True)
        killer.on_click(lambda event: self.kill(name, event))

        # add to figures
        figures = [checkbox, miniature, killer]

        # make row of graphs and add to reservoir
        row = Row(figures)

        # make cell
        cell = Column([row])
        self.manifestations[name] = cell

        return cell

    def speculate(self, path):
        """Speculate as to the most interesting orbits.

        Arguments:
            path: str, filepath

        Returns:
            None
        """

        # collect all blooms with the path
        blooms = [bloom for bloom in self if bloom.path == path]

        # subset radiances
        blooms = self.dig('radiance', blooms)
        blooms = [bloom for bloom in blooms if '_' in bloom.petal]

        # sort by band
        anomalous = []
        bands = self._group(blooms, lambda bloom: re.search('band[1-3]', bloom.stem).group())
        for band, members in bands.items():

            # print band
            print('{}...'.format(band))

            # group members by wavelength
            waves = self._group(members, lambda bloom: re.search('[0-9]{2}0.0', bloom.stem).group())
            matrix = []
            for wave, entries in waves.items():

                # print wavelength
                print('{}...'.format(wave))

                # sort by row
                entries.sort(key=lambda entry: entry.petal)

                # for each entry
                rows = []
                for index, bloom in enumerate(entries):

                    # print the name
                    print('analyzing {}, {} of {}...'.format(bloom.sprig, index, len(entries)))

                    # fill bloom
                    bloom.fill(bloom.function, bloom.subset)
                    bloom.plug()

                    # get abscissa and convert to integers
                    orbits = self.variables[bloom.path][bloom.shape[-1]]['orbit_number']
                    integers = self._digitize(orbits)

                    # use svm to detect anomalies
                    _, anomalies = self._support(bloom.spill(), integers, bloom.void)

                    # add to list of anomalous orbits
                    anomalous += [orbit for orbit, anomaly in zip(orbits, anomalies) if anomaly != self.fill]

                    # create mask
                    mask = anomalies != self.fill
                    rows.append(mask)

                # add rows to matrix
                matrix.append(rows)

                # envison all rows
                rows = numpy.array(rows)
                destination = self.source.replace('data', 'plots') + '/svn_{}_{}.png'.format(band, wave)
                title = 'Support Vector Machine Anomaly Detection, {}, {}nm'.format(band, wave)
                units = '95th %ile'
                dependent = 'row'
                independent = 'orbit'
                coordinates = {dependent: [index + 1 for index, _ in enumerate(rows)]}
                coordinates.update({independent: orbits})
                self._envision(rows, destination, title, units, dependent, independent, coordinates=coordinates)

            # envison average
            matrix = numpy.array(matrix).mean(axis=0)
            destination = self.source.replace('data', 'plots') + '/svn_{}_avg.png'.format(band)
            title = 'Support Vector Machine Anomaly Detection, {}'.format(band)
            units = '95th %ile'
            dependent = 'row'
            independent = 'orbit'
            coordinates = {dependent: [index + 1 for index, _ in enumerate(rows)]}
            coordinates.update({independent: orbits})
            self._envision(matrix, destination, title, units, dependent, independent, coordinates=coordinates)

        # count orbits
        counter = Counter(anomalous)

        # sort by counts
        counter = list(counter.items())
        counter.sort(key=lambda item: item[1], reverse=True)

        # jot down report
        report = ['Anomaly Counts:'] + ['{}: {}'.format(orbit, count) for orbit, count in counter]
        self._jot(report, '{}/anomalies.txt'.format(self.source.replace('data', 'notes')))

        return None

    def template(self, graph, pickers, stickers, contexts):
        """Update the yaml file by the characteristics of the graph at hand.

        Arguments:
            graph: bokeh figure object
            stickers: list of text label boxes
            pickers: list of color picker boxes
            contexts: list of graph labels

        Returns:
            None
        """

        # update yam
        self._print('updating {}...'.format(self.ticket))

        # grap contexts
        title = contexts[0].value
        ordinate = contexts[1].value
        abscissa = contexts[2].value

        # update yam
        self.yam['title'] = title
        self.yam['ordinate']['name'] = ordinate
        self.yam['abscissa']['name'] = abscissa

        # gather lines
        lines = []
        for picker, sticker in zip(pickers, stickers):

            # print info
            lines.append({'label': sticker.value, 'color': picker.color})

        # update lines
        self.yam['lines'] = lines

        # gather propagation parameters
        identifiers = [renderer.name for renderer in graph.renderers if renderer.name and renderer.name.isdigit()]
        identifiers = self._skim(identifiers, maintain=True)

        # construct parameters
        parameters = []
        for identifier in identifiers:

            # get the bloom name
            bloom = self[int(identifier)]
            name = bloom.name
            index = self.paths.index(bloom.path)

            # add parameters
            parameters.append({'file': index, 'name': name, 'step': 1})

        # update yam
        self.yam['propagation']['parameters'] = parameters

        # set up settings queue
        settings = [('fonts', 'glasses', str)]
        settings.append(('corners', 'puzzle', str))
        settings.append(('styles', 'quill', str))
        settings.append(('spectra', 'palette', str))
        settings.append(('pen', 'compass', float))

        # for each member
        for setting, symbol, function in settings:

            # update attributes
            members = self.yam[setting]
            selection = function([tag for tag in graph.tags if symbol in tag][0].split(':')[-1])
            index = members.index(selection)
            members = members[index:] + members[:index]
            self.yam[setting] = members

        # send to yaml file
        self._dispense(self.yam, self.ticket)

        return None

    def toggle(self, name, suite):
        """Toggle the visible states of a feature, and draw if not yet drawn:

        Arguments:
            name: name of feature in manifestations
            suite: list of feature instances

        Returns:
            None
        """

        # grab the feature's manifestation
        manifestation = self.manifestations[name]

        # check for less than two children
        if len(manifestation.children) < 2:

            # in which case draw the feature and its checkbox and add
            manifestation.children += self.manifest(suite)

        # otherwise
        else:

            # toggle graph
            graph = manifestation.children[1]
            graph.visible = not graph.visible

            # toggle other options
            for box in manifestation.children[2:]:

                # set box visibility
                box.visible = not box.visible

            return None

    def varigate(self, titles, name, tag, indices=None, options=None, folder=None):
        """Create cultivation variants from a list of titles, the image name, and the parameter name.

        Arguments:
            titles: list of str, the titles per file
            name: str, paramter name to graph
            tag: str, name of each slide
            indices: list of file indices
            options: dict of further options
            folder: str, name of propagation folder

        Returns:
            list of dicts
        """

        # set default options
        options = options or {}

        # create stubs
        stubs = ['{}_{}'.format(tag, str(index).zfill(2)) for index, _ in enumerate(titles)]

        # construct default indices
        if not indices:

            # construct
            indices = [index for index, _ in enumerate(titles)]

        # create variants
        variants = []
        for index, title, stub in zip(indices, titles, stubs):

            # begin variant
            variant = {'title': title, 'propagation/stub': stub}

            # add parameters
            variant.update({'propagation/parameters': [{'name': name, 'file': index, 'step': 1}]})

            # update options
            variant.update(options)

            # add folder
            if folder:

                # add it
                variant.update({'propagation/folder': folder})

            # append
            variants.append(variant)

        return variants


# get arguments from commandline
arguments = [argument for argument in sys.argv[1:]]

# try to
try:

    # create boquette instance and send to site
    boquette = Boquette(*arguments)
    Curdoc().add_root(boquette.page)

# otherwise
except TypeError:

    # nevermind
    pass