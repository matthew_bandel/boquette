# magrittes.py for the Ozonator class to provide a loose class for exploring OMI data

# import local modules
from cores import Core
from features import Feature
from formulas import Formula
#from plates import Plate
from hydras import Hydra

# import general tools
import os
import sys
import json
import csv
import re

# import math
import math

# import datetime
from datetime import datetime, timedelta
from time import time

# evaluate strings
from ast import literal_eval

# import tools
from pprint import pprint
from collections import Counter
from copy import copy, deepcopy

# import math
import numpy
from numpy.random import random
from math import sin, cos, pi, log10, exp, floor, ceil, sqrt

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import gridspec
from matplotlib import colorbar
from matplotlib import colors as Colors
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib import path as Path
# from mpl_toolkits.mplot3d import Axes3D
# from mpl_toolkits.basemap import Basemap

Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# import h5py to read h5 files
import h5py

# import regex for string matching
from re import search

# import fuzzywuzzy for fuzzy matches
#from fuzzywuzzy import fuzz

# import counter
from collections import Counter

# import random forest and regression
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans, MeanShift
from sklearn.decomposition import PCA, FastICA

# pdf maker
from PIL import Image
#from PyPDF2 import PdfFileReader, PdfFileWriter

# # import xarray
# import xarray
# xarray.set_options(keep_attrs=True)


# class Magritte to do OMI data mining
class Magritte(Hydra):
    """Magritte class to do OMI data compariwons.

    Inherits from:
        Core
    """

    def __init__(self, source, sink):
        """Initialize an Magritte instance.

        Arguments:
            None

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, source)

        # add sink directory
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # display contents
        self._tell(self.paths)

        # create representation
        representation = ' < Magritte instance at: {} >'.format(self.source)

        return representation

    def _aggregate(self, radiances, times, days, months, years):
        """Aggregate radiances over every five days.

        Arguments:
            radiances: numpy array
            times: numpy array
            days: numpy array, days of the month
            months: numpy array
            years: numpy array

        Returns:
            (numpy, array, numpy array) tuple
        """

        # define chunks, with the last chunk being of variable length
        chunks = {0: (1, 5), 1: (6, 10), 2: (11, 15), 3: (16, 20), 4: (21, 25), 5: (26, 31)}

        # get all months and years
        dates = (numpy.unique(years).tolist(), numpy.unique(months).tolist())
        dates[0].sort()
        dates[1].sort()

        # begin empty matrix and time start list
        tensor = []
        starts = []
        seasons = []
        monthlies = []
        yearlies = []

        # for evey year
        for year in dates[0]:

            # and evey month
            for month in dates[1]:

                # and every chunk
                for chunk in range(6):

                    # create masks
                    first = chunks[chunk][0]
                    last = chunks[chunk][1]
                    mask = (years == year)
                    mask = mask * (months == month)
                    mask = mask * ((days >= first) & (days <= last))

                    # if nonempty
                    if mask.sum() > 0:

                        # average radiances along mask
                        matrix = radiances[mask, :, :].mean(axis=0)
                        start = times[mask][0]

                        # add to growing list
                        tensor.append(matrix)
                        starts.append(start)

                        # add seasonality numbers
                        season = ((month - 1) * 6) + chunk
                        seasons.append(season)

                        # add year and month trackers
                        monthlies.append(month)
                        yearlies.append(year)

        # convert to arrays
        tensor = numpy.array(tensor)
        starts = numpy.array(starts)
        seasons = numpy.array(seasons)
        monthlies = numpy.array(monthlies)
        yearlies = numpy.array(yearlies)

        return tensor, starts, seasons, monthlies, yearlies

    def _calculate(self, coefficients, references, radiance):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            radiance: numpy array, last member of shape is number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        print('incoming...')
        print('coefficients: {}'.format(coefficients.shape))
        print('radiance: {}'.format(radiance.shape))
        print('reference: {}'.format(references.shape))

        # set pixels
        pixels = radiance.shape[-1]

        # remove first dimensions
        coefficients = coefficients[0]
        radiance = radiance[0]
        references = numpy.atleast_1d(references.squeeze())

        # adjust coefficients
        if radiance.shape[0] < coefficients.shape[0]:

            # set coefficients to mid point of cal
            coefficients = coefficients[40:41, :, :]

        # get relevant dimensions
        orbits, rows, degree = coefficients.shape
        print('scans: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))
        self._stamp('calculating wavelengths...', initial=True)

        print('coefficients: {}'.format(coefficients.shape))
        print('radiance: {}'.format(radiance.shape))
        print('reference: {}'.format(references.shape))

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns] * rows)
        indices = numpy.array([image] * orbits)

        # expand tensor of column references
        references = numpy.array([references] * rows)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(degree)])
        powers = powers.transpose(1, 2, 3, 0)

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)
        coefficients = coefficients.transpose(1, 2, 0, 3)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=3)

        print('waves: {}'.format(wavelengths.shape))

        # timestamp
        self._stamp('calculated.')

        return wavelengths

    def _compose(self, mantissas, exponents, distances):
        """Compose radiances from mantissas and exponenets.

        Arguments:
            mantissas: numpy array
            exponents: numpy array
            distances: numpy array

        Returns:
            numpy array
        """

        # remove first dimension
        mantissas = mantissas[0]
        exponents = exponents[0]

        # get conversion factor
        factor = 10000 / 6.02214076e23
        meters = 1.49597870e11

        # raise exponents
        tens = numpy.ones(exponents.shape) * 10
        powers = numpy.power(tens, exponents)

        # construct radiances
        radiances = factor * mantissas * powers

        # adjust for earth-sun distance
        radiances = radiances * (distances / meters) ** 2

        return radiances

    def _delineate(self, vectors, indices):
        """Find the first three vectors that are not collinear.

        Arguments:
            vectors: list of list of floats
            indices: list of int, ordering by distance

        Returns:
            list of list of floats
        """

        # get the first two vectors
        points = [vectors[indices[0]], vectors[indices[1]]]

        # go through remaining vectors
        for index in indices[2:]:

            # get unique latitudes and longitudes
            latitudes = [points[0][0], points[1][0], vectors[index][0]]
            longitudes = [points[0][1], points[1][1], vectors[index][1]]

            # check for unique coordinates with third
            if len(set(latitudes)) > 1 and len(set(longitudes)) > 1:

                # keep point and stop looking
                points += [vectors[index]]
                break

        return points

    def _envision(self, matrix, destination, texts=None, spectrum=None, coordinates=None):
        """Envision a matrix as a heat map.

        Arguments:
            matrix: 2-D numpy array
            destination: file path for saving image
            texts: tuple of str (title, units, dependent, independent)
            spectrum: specturm to apply
            coordinates: dict of lists

        Returns:
            None
        """

        # unpack texts
        texts = texts or []
        texts = [text for text in texts] + [''] * 4
        title, units, dependent, independent = texts[:4]

        # set default zoom
        zoom = (0, len(matrix[0]), 0, len(matrix))

        # make default labels
        independent = independent or 'horizontal gridpoint'
        dependent = dependent or 'vertical gridpoint'

        # set default coordinates
        if not coordinates:

            # default to indices
            coordinates = {independent: list(range(matrix.shape[1]))}
            coordinates.update({dependent: list(range(matrix.shape[0]))})

        # make xarry matrix
        matrix = xarray.DataArray(matrix, dims=[dependent, independent], coords=coordinates)
        matrix.attrs.update({'route': title, 'units': units})

        # make plate object and draw graph
        spectrum = spectrum or 'classic'
        plate = Plate(matrix, ['vertical', 'horizontal'], zoom, title, spectrum=spectrum, unity=units)
        plate.glance()

        # resave glance image to destination
        image = Image.open('../plots/glance.png')
        image.save(destination)

        return None

    def _imagine(self, latitude, longitude, one, two, three):
        """Interpolate a datum based on the datum at surrounding coordinates.

        Arguments:
            latitude: float
            longitude: floatt
            one: list of float, first vector
            two: list of float, second vector
            three: list of float, third vector

        Returns:
            float
        """

        # calculate the two - one difference and three - one difference
        vector = [two[index] - one[index] for index in range(3)]
        vectorii = [three[index] - one[index] for index in range(3)]

        # calculate the cross product from the determinant i (vc - wb) + j (wa - uc) + k (uc - wa)
        cross = [(vector[1] * vectorii[2]) - (vector[2] * vectorii[1])]
        cross += [(vector[2] * vectorii[0]) - (vector[0] * vectorii[2])]
        cross += [(vector[0] * vectorii[1]) - (vector[1] * vectorii[0])]

        # determine constant from cross product coefficients and one point
        constant = sum([coefficient * coordinate for coefficient, coordinate in zip(cross, one)])

        #  calculate the datum z = (d - ax - by) / c
        datum = (constant - cross[0] * latitude - cross[1] * longitude) / cross[2]

        return datum

    def _interpolate(self, irradiances, wavelengths):
        """Interpolate irradiances across a wavelength grid.

        Arguments:
            irradiances: numpy array of irradiances
            wavelengths: numpy array of floats, calculated wavelengths

        Returns:
            None
        """

        # if wavelengths are backwards:
        if wavelengths[:, :, 0].mean() > wavelengths[:, :, -1].mean():

            # flip wavelengths and irradiances
            wavelengths = numpy.flip(wavelengths, axis=2)
            irradiances = numpy.flip(irradiances, axis=2)

        # # clip 20 spectral pixels from each end
        # wavelengths = wavelengths[:, :, 20:-20]
        # irradiances = irradiances[:, :, 20:-20]

        print('wavelengths: {}'.format(wavelengths.shape))
        print('radiances: {}'.format(irradiances.shape))

        # for each image
        for index, matrix in enumerate(irradiances):

            # transpose to get columns
            columns = matrix.transpose(1, 0)
            for indexii, column in enumerate(columns):

                # create masks for less than zero (bad pixel data)
                mask = column <= 0
                maskii = column > 0
                average = column[maskii].mean()
                column[mask] = average

                # replace column in matrix
                columns[indexii] = column

            # re transpose matrix
            matrix = columns.transpose(1, 0)
            irradiances[index] = matrix

        # transpose matrices
        wavelengths = wavelengths.transpose(2, 1, 0)
        irradiances = irradiances.transpose(2, 1, 0)

        # get the start and finish wavelengths based on the shortest and longest wavelengths amongst all rows
        start = wavelengths.min(axis=0).max()
        finish = wavelengths.max(axis=0).min()

        # convert nanometers to angstroms for a grid based on tenths of nanometers
        start = math.ceil(10 * start)
        finish = math.floor(10 * finish)

        # create wavelength grid for entire range at a resolution of tenth of wavelength
        grid = [number / 10 for number in range(start, finish + 1)]
        grid = numpy.array(grid)

        # for each wavelength assignment in the grid
        self._stamp('interpolating...', initial=True)
        interpolations = []
        for assignment in grid:

            # for particular wavelengths
            if assignment == int(assignment) and int(assignment) % 10 == 0:

                # print status
                print('{} nm...'.format(assignment))

            # make lesser and greater masks
            lesser = wavelengths <= assignment
            greater = wavelengths > assignment

            # roll the greater mask towards the lesser so trues overlap only left of the assignment
            left = lesser * numpy.roll(greater, -1, axis=0)

            # roll the lesser towards the greater so trues overlap only right of the assignment
            right = numpy.roll(lesser, 1, axis=0) * greater

            # extract initial and final irradiances by multiplyiing with masks
            initial = (irradiances * left)
            final = (irradiances * right)

            # collapse dimension by summing (just one entry should be nonzero per row)
            initial = initial.sum(axis=0)
            final = final.sum(axis=0)

            # extract short and long wavelengths
            short = (wavelengths * left).sum(axis=0)
            long = (wavelengths * right).sum(axis=0)

            # calculate rise and run
            rise = final - initial
            run = long - short

            # apply linear interpolation formula: y = y0 + (x - x0) * (y1 - y0) / (x1 - x0)
            interpolation = initial + (assignment - short) * rise / run

            # traspose and append
            interpolation = interpolation.transpose(1, 0)
            interpolations.append([interpolation])

        # concatenate and transpose
        self._stamp('concatenating...')
        interpolations = numpy.concatenate(interpolations, axis=0)
        interpolations = interpolations.transpose(1, 2, 0)
        self._stamp('concatenated.')

        return interpolations, grid

    def _quantize(self, radiances, times, wavelengths, row, rowii,  wavelength):
        """Split average radiances by quantile.

        Arguments:
            radiances: numpy array
            times: numpy array
            wavelengths: numpy array
            row: int, row index
            rowii: int, ending row index
            wavelength: int, wavelength in nm

        Returns:
            tuple of numpy arrays
        """

        # subtract 1 from row to get 0-based index
        row = row - 1
        rowii = rowii - 1

        # get spectral index by closest index
        wave = ((wavelengths - wavelength) ** 2).argmin()

        # normalize based on first
        radiances = (radiances - radiances[0]) / radiances[0]

        # average radiances along row
        radiances = radiances.squeeze()[:, row:rowii + 1, wave].mean(axis=1)

        # set quantiles
        quantiles = [numpy.quantile(radiances, number / 10) for number in range(11)]

        # create masks for each quantile
        subsets = []
        abscissas = []
        for index in range(10):

            # create mask
            #mask = (radiances >= quantiles[index]) & (radiances <= quantiles[index + 1])
            mask = (radiances >= quantiles[index])

            # make subsets
            subset = radiances[mask]
            subsets.append(subset)

            # make abscissa
            abscissa = times[mask]
            abscissas.append(abscissa)

        # return tensors
        tensors = subsets + abscissas

        return tensors

    def _reaggregate(self, radiances, times, seasons, months, years):
        """Aggregate radiances over every five days.

        Arguments:
            radiances: numpy array
            times: numpy array
            seasons: numpy array, days of the month
            months: numpy array
            years: numpy array

        Returns:
            (numpy, array, numpy array) tuple
        """

        # define chunks based on seasonal index
        chunks = {number: (number * 6, number * 6 + 5) for number in range(12)}

        # get all months and years
        dates = (numpy.unique(years).tolist(), numpy.unique(months).tolist())
        dates[0].sort()
        dates[1].sort()

        # begin empty matrix and time start list
        tensor = []
        starts = []

        # for evey year
        for year in dates[0]:

            # and evey month
            for month in dates[1]:

                # and every chunk
                for chunk in range(12):

                    # create masks
                    first = chunks[chunk][0]
                    last = chunks[chunk][1]
                    mask = (years == year)
                    mask = mask * (months == month)
                    mask = mask * ((seasons >= first) & (seasons <= last))

                    # if nonempty
                    if mask.sum() > 0:

                        # average radiances along mask
                        matrix = radiances[mask, :, :].mean(axis=0)
                        start = times[mask][0]

                        # add to growing list
                        tensor.append(matrix)
                        starts.append(start)

        # convert to arrays
        tensor = numpy.array(tensor)
        starts = numpy.array(starts)

        return tensor, starts

    def _seasonalize(self, radiances, seasons):
        """Subtract seasonal trends from the radiances.

        Arguments:
            radiances: numpy array
            seasons: numpy array

        Returns:
            numpy array
        """

        # get all unique seasonal indices
        indices = sorted(numpy.unique(seasons).tolist())

        # get the average of the radiances
        average = radiances.mean(axis=0)

        # begin seasonalization
        boxes = {}

        # for each seasonal index
        for index in indices:

            # average all entries for each seasonal box
            mask = seasons == index
            box = (radiances - average)[mask].mean(axis=0)
            boxes[index] = box

        # subtract seasonalizations
        seasonalizations = []
        for radiance, season in zip(radiances, seasons):

            # subtract the box
            seasonalization = radiance - boxes[season]
            seasonalizations.append(seasonalization)

        # create array
        seasonalizations = numpy.array(seasonalizations)

        return seasonalizations

    def _trope(self, radiances, wavelengths, latitudes, degrees=20):
        """Subset the radiances according tropical latitudes.

        Arguments:
            radiances: numpy array
            wavelengths: numpy array
            latitudes: numpy array
            degrees: int, absolute degrees north and south of equator

        Returns:
            tuple of numpy arrays
        """

        self._print('troping...')
        self._print('radiances: {}'.format(radiances.shape))
        self._print('wavelengths: {}'.format(wavelengths.shape))
        self._print('latitudes: {}'.format(latitudes.shape))

        # radiances should be 3-d
        if len(radiances.shape) > 3:

            # squeeze
            radiances = radiances.squeeze()

        # set defaults
        subset = radiances
        waves = wavelengths
        latitudes = latitudes[0]
        tropics = latitudes

        # if radiances are extensive (and not just irradiances)
        if radiances.shape[0] > 1:

            # make a mask based on latitude
            mask = numpy.abs(latitudes) <= degrees

            # get the tropics from the mask
            tropics = latitudes[mask]

            # subset the radiances and wavelengths
            subset = radiances[mask, :, :]
            waves = wavelengths[mask, :, :]

        self._print('after troping...')
        self._print('subset: {}'.format(subset.shape))
        self._print('waves: {}'.format(waves.shape))
        self._print('latitudes: {}'.format(latitudes.shape))
        self._print('tropics: {}'.format(tropics.shape))

        return subset, waves, latitudes, tropics

    def _weed(self, radiances, times, days, months, years, quantile):
        """Weed out the lower radiance measurements.

        Arguments:
            radiances: numpy.array
            quantile: float, quantile above which to keep

        Returns:
            None
        """

        # take averages
        average = radiances.mean(axis=1).mean(axis=1)

        # get the quantile
        marker = numpy.quantile(average, quantile)

        # make a mask
        mask = average >= marker

        # return mask
        weeds = radiances[mask, :, :]

        # mask all independents
        times = times[mask]
        days = days[mask]
        months = months[mask]
        years = years[mask]

        return weeds, times, days, months, years

    def calibrate(self, paths, destinations):
        """Calculate the wavelengths from collection 3 files.

        Arguments:
            paths: list of str, filenames

        Returns:
            None
        """

        # set bands for each collection
        bands = {'3': {'band1': 'UV-1', 'band2': 'UV-2', 'band3': 'VIS'}}
        bands.update({'4': {'band1': 'BAND1', 'band2': 'BAND2', 'band3': 'BAND3'}})

        # set radiation names for each collection
        radiations = {'3': {'OML1BCAL': 'Irradiance', 'OML1BRUG': 'Radiance', 'OML1BRVG': 'Radiance'}}
        radiations.update({'4': {'OML1BCAL': 'irradiance_after_relirr_avg'}})
        radiations['4'].update({'OML1BRUG': 'radiance', 'OML1BRVG': 'radiance'})

        # set prefixes
        prefixes = {'OML1BCAL': 'ir', 'OML1BRUG': '', 'OML1BRVG': ''}

        # set wavelength coefficient names
        waves = {'3': ['WavelengthCoefficient', 'ReferenceColumn']}
        waves.update({'4': ['wavelength_coefficient', 'reference_column']})

        # set latitude coordinate field names
        coordinates = {'3': 'SpacecraftLatitude', '4': 'satellite_latitude'}

        # for each path
        for path, destination in zip(paths, destinations):

            # ingest path
            self.ingest(path)

            # grab product and collection
            stage = self._stage(path)
            product, collection = stage['product'], stage['collection']

            # begin formulation
            formula = Formula()

            # narrow down feature set depending on Collection 3 or Collection 4
            fields = ['Avg Sun', 'Earth', 'OBSERVATIONS', 'INSTRUMENT', 'GEODATA']
            categories = [feature for feature in self if any([field in feature.slash for field in fields])]

            # grab the independent variables from file path
            independents = self._parse(path)

            # create features from all independent variables
            independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
            independents = [Feature(['IndependentVariables', name], array) for name, array in independents.items()]

            # set radiation and prefix
            radiation = radiations[collection][product]
            prefix = prefixes[product]

            # for each band
            for band, alias in bands[collection].items():

                # check for presence of band
                if len(self.dig(alias)) > 0:

                    # if collection 3
                    if collection == '3':

                        # compose the data from mantissas and exponent
                        def composing(mantissa, exponent, distance): return self._compose(mantissa, exponent, distance)
                        parameters = ['{}Mantissa'.format(radiation), '{}Exponent'.format(radiation), 'EarthSunDistance']
                        parameters = ['{}/{}'.format(alias, parameter) for parameter in parameters]
                        name = '{}_{}radiance'.format(band, prefix)
                        formula.formulate(parameters, composing, name, 'Categories/{}'.format(band))

                    # otherwise, assume collection 4
                    else:

                        # and rename the data for convenience
                        def squeezing(tensor): return numpy.atleast_3d([tensor.squeeze()])
                        parameter = '{}/{}'.format(alias, radiation)
                        name = '{}_{}radiance'.format(band, prefix)
                        formula.formulate(parameter, squeezing, name, 'Categories/{}'.format(band))

                    # and make wavelength calculations
                    def calculating(wavelength, reference, size): return self._calculate(wavelength, reference, size)
                    parameters = ['{}/{}'.format(alias, wave) for wave in waves[collection]]
                    parameters += ['{}_{}radiance'.format(band, prefix)]
                    name = '{}_calculated_wavelengths'.format(band)
                    formula.formulate(parameters, calculating, name, 'Categories/{}'.format(band))

                    # subset based on tropics
                    def troping(radiance, wavelengths, latitudes): return self._trope(radiance, wavelengths, latitudes)
                    parameters = ['{}_{}radiance'.format(band, prefix)]
                    parameters += ['{}_calculated_wavelengths'.format(band)]
                    parameters += [coordinates[collection]]
                    names = ['{}_{}radiance_tropics'.format(band, prefix)]
                    stubs = ('{}_wavelengths_tropics', '{}_latitude', '{}_latitude_tropics')
                    names += [stub.format(band) for stub in stubs]
                    formula.formulate(parameters, troping, names)

                    # construct the wavelength grid and interpolate the solar irradiances
                    def interpolating(radiances, wavelengths): return self._interpolate(radiances, wavelengths)
                    parameters = ['{}_{}radiance_tropics'.format(band, prefix)]
                    parameters += ['{}_wavelengths_tropics'.format(band)]
                    names = ['{}_interpolated_{}radiance'.format(band, prefix), '{}_wavelength_grid'.format(band)]
                    addresses = [None, 'IndependentVariables']
                    description = 'linearly interpolated irradiances'
                    attributes = [{'units': '{}radiance'.format(prefix), 'description': description}]
                    attributes += [{'units': 'nm'}]
                    formula.formulate(parameters, interpolating, names, addresses, attributes)

            # add variables
            for variable in independents:

                # add to formula
                formula.formulate([variable.name])

            # make cascasde and stash
            cascade = self.cascade(formula, categories + independents)
            self._stash(cascade, destination, 'Data')

        return None

    def fresco(self, left=20, right=120, row=19):
        """Examine band1 winter spikes .

        Arguments:
            left: int, left spectral index
            right: int, right spectral index
            row: int, row number

        Returns:
            None
        """

        # clear old data
        self._clean(self.sink)

        # go through each file
        for path in sorted(self.paths):

            # grab the orbit number
            orbit = self._stage(path)['orbit']

            # stamp
            self._stamp('analyzing orbit {}...'.format(orbit), initial=True)

            # ingest the path
            self.ingest(path)

            # grab the band1 radiance data
            radiances = self.dig('BAND1_RADIANCE/radiance')[0].distil().squeeze()

            # grab latitudes
            latitudes = self.dig('satellite_latitude')[0].distil().squeeze()

            # find the latitude closest to 20 S and 20 N
            south = ((latitudes + 20) ** 2).argsort()[0]
            equator = ((latitudes) ** 2).argsort()[0]
            north = ((latitudes - 20) ** 2).argsort()[0]
            #
            # # create dict
            # views = {north: 'North, {} deg lat'.format(round(float(latitudes[north]), 2))}
            # views.update({south: 'South, {} deg lat'.format(round(float(latitudes[south]), 2))})
            # views.update({equator: 'Equator, {} deg lat'.format(round(float(latitudes[equator]), 2))})

            # go through all indices
            for index in range(north - 60, len(latitudes)):

                # only every tenth
                if index % 5 == 0:

                    # make description
                    description = round(float(latitudes[index]), 2)

                    # print status
                    self._print('{}, {}...'.format(index, description))

                    # get matrix
                    matrix = radiances[index, :, left:right]

                    # envision image differences
                    date = self._stage(path)['date'].split('t')[0]
                    destination = '{}/{}_{}.png'.format(self.sink, orbit, str(index).zfill(4))
                    title = 'Orbit: {} / {}, scan {}: {} degrees latitude'.format(orbit, date, index, description)
                    unit = 'mol photons / m^2 nm sr s'
                    dependent = 'spatial pixel'
                    independent = 'spectral pixel'
                    self._envision(matrix, destination, title, unit, dependent, independent)

            # finish
            self._stamp('analyzed.')

        return None

    def glint(self, path, band, wavelength, destination):
        """Take a slice at one wavelength, across time.

        Arguments:
            path: str, filepath
            band: int, band number
            wavelength: float, in nm
            destination: destination of plot
            texts: list of str (title, units, dependent, independent)

        Returns:
            None
        """

        # stage path for orbit, collection, and date information
        stage = self._stage(path)
        orbit, collection, date = stage['orbit'], stage['collection'], stage['date']

        # ingest the path
        self.ingest(path)

        # get the radiances, latitudes, and grid
        tensor = self.dig('band{}_interpolated_radiance'.format(band))[0].distil()
        grid = self.dig('band{}_wavelength_grid'.format(band))[0].distil()
        latitudes = self.dig('band{}_latitude_tropics'.format(band))[0].distil()

        # find the index from the grid
        index = ((grid - wavelength) ** 2).argmin()

        # get the matrix at the position
        matrix = tensor[:, :, index]
        matrix = matrix.transpose(1, 0)

        # unpack texts
        formats = (collection, band, orbit, date, wavelength)
        title = 'Radiance, Collection {}, Band {}, +/- 40 degrees latitude, Orbit {}, {}, {} nm'.format(*formats)
        units = 'radiance ( mol photons / s m^2 nm sr )'
        dependent = 'rows'
        independent = 'latitude'
        texts = (title, units, dependent, independent)

        # create coordinates
        coordinates = {independent: latitudes}

        # envision
        self._envision(matrix, destination, texts=texts, coordinates=coordinates)

        return None

    def illustrate(self):
        """Compare radiances of several collection 3 / 4 pairs.

        Arguments:
            None

        Returns:
            None
        """

        # clear old data
        self._clean(self.sink)

        # group paths by orbit number
        orbits = self._group(self.paths, lambda path: self._stage(path)['orbit'])

        # go through each orbit
        for orbit, paths in orbits.items():

            # stamp
            self._stamp('analyzing orbit {}...'.format(orbit), initial=True)

            # sort by collection
            collections = self._group(paths, lambda path: self._stage(path)['collection'])

            # get the paths
            three = collections['3'][0]
            four = collections['4'][0]

            # ingest the collection 3 path
            self.ingest(three)

            # grab the radiance and mantissa data
            mantissas = self.dig('Earth UV-1 Swath/Mantissa')[0].distil()
            exponents = self.dig('Earth UV-1 Swath/Exponent')[0].distil()

            # recompose the radiance data
            radiances = self._compose(mantissas, exponents)

            # flip radiances due to band1 reversal
            radiances = numpy.flip(radiances, axis=2)

            # adjust for the earth sun distance
            earth = 1.49597870e11
            distance = self.dig('EarthSunDistance')[0].distil()
            radiances = radiances * (distance ** 2) * (1 / earth ** 2)

            # grab collection 4 radiances
            self.ingest(four)
            radiancesii = self.dig('BAND1_RADIANCE/radiance')[0].distil().squeeze()

            # subset radiances by row  and wavelength
            radiances = radiances[:, :, 20:120]
            radiancesii = radiancesii[:, :, 20:120]

            # calculate absolute relative difference
            differences = 200 * (radiancesii - radiances) / (abs(radiancesii) + abs(radiances))

            # average across all pixels and rows
            orbits = radiances.mean(axis=2).mean(axis=1)
            orbitsii = radiancesii.mean(axis=2).mean(axis=1)
            averages = differences.mean(axis=2).mean(axis=1)

            # # envision orbital differences
            # date = self._stage(four)['date'].split('t')[0]
            # destination = '{}/{}_orbit_diff.png'.format(self.sink, orbit)
            # title = 'Orbit: {} / {}, Orbital Average Band1 Radiance Differences, Pixels 20 - 120'.format(orbit, date)
            # unit = '% difference'
            # dependent = '_'
            # independent = 'track'
            # self._envision(numpy.array([averages]), destination, title, unit, dependent, independent)

            # grab latitudes
            latitudes = self.dig('satellite_latitude')[0].distil().squeeze()

            # find the latitude closest to 20 S and 20 N
            south = ((latitudes + 20) ** 2).argsort()[0]
            equator = ((latitudes) ** 2).argsort()[0]
            north = ((latitudes - 20) ** 2).argsort()[0]

            # find largest and smallest difference within the range
            largest = (averages[south: north + 1] ** 2).argmax() + south
            smallest = (averages[south: north + 1] ** 2).argmin() + south

            # correlate with bokeh
            print('collection 3: {}'.format(radiances[south: north + 1].mean()))
            print('collection 4: {}'.format(radiancesii[south: north + 1].mean()))

            # create dict
            views = {north: 'North, {} deg lat'.format(round(float(latitudes[north]), 2))}
            views.update({south: 'South, {} deg lat'.format(round(float(latitudes[south]), 2))})
            views.update({equator: 'Equator, {} deg lat'.format(round(float(latitudes[equator]), 2))})
            views.update({largest: 'Largest Abs Difference, {} deg lat'.format(round(float(latitudes[largest]), 2))})
            views.update({smallest: 'Smallest Abs Difference, {} deg lat'.format(round(float(latitudes[smallest]), 2))})

            # go through all views
            for index in range(len(differences)):

                # check every 50th
                if index % 100 == 0:

                    # get description
                    latitude = latitudes[index]
                    latitude = round(float(latitude), 2)
                    description = 'Scan: {}, Latitude: {}'.format(index, latitude)

                    # print status
                    self._print('{}, {}...'.format(index, description))

                    # go through each mode
                    modes = ['Collection 4 - Collection 3']
                    tags = ['diff']
                    data = [differences]
                    #units = ['mol photons / m^2 nm sr s', 'mol photons / m^2 nm sr s', '% difference']
                    units = ['% difference']
                    for datum, mode, unit, tag in zip(data, modes, units, tags):

                        # envision image differences
                        date = self._stage(four)['date'].split('t')[0]
                        destination = '{}/{}_{}_{}.png'.format(self.sink, orbit, str(index).zfill(4), tag)
                        title = 'Orbit: {} / {}, {} Band1 Radiances, {}, Pixels 20 - 120'.format(orbit, date, mode, description)
                        dependent = 'spatial pixel'
                        independent = 'spectral pixel'
                        self._envision(numpy.array(datum[index]), destination, title, unit, dependent, independent)

            # finish
            self._stamp('analyzed.')

        return None

    def impasto(self, data, norths, easts, latitudes, longitudes):
        """Construct a cubic projection from planar data using interpolationn.

        Arugments:
            data: numpy array of data
            norths: numpy array, planar northward coordinates
            eests: numpy array, planar eastward coordinates
            latitudes: numpy array, cubic latitudes
            longitudes: numpy array, cubic longitudes

        Returns:
            numpy array
        """

        # expand norths and easts
        shape = data.shape
        easts = numpy.vstack([easts] * shape[0])
        norths = numpy.vstack([norths] * shape[1]).transpose(1, 0)

        # zip all together
        zipper = list(zip(norths.ravel(), easts.ravel(), data.ravel()))

        # construct an empty cube
        portrait = numpy.zeros((6, 180, 180))

        # for each face
        for face in range(6):

            self._print('face {}...'.format(face))

            # for each row
            for row in range(180):

                self._print('row: {}'.format(row))

                # and each column
                for column in range(180):

                    # get the target latitude and longitude
                    latitude = latitudes[face][row][column]
                    longitude = longitudes[face][row][column]

                    # compute distances from target for all grid points
                    distances = [(latitude - north) ** 2 + (longitude - east) ** 2 for north, east, _ in zipper]
                    indices = numpy.argsort(distances)

                    # get the first three points that are not collinear
                    points = self._delineate(zipper, indices)

                    # determine the interpolated datum
                    datum = self._imagine(latitude, longitude, *points)

                    # add to portrait
                    portrait[face][row][column] = datum

        return portrait

    def juxtapose(self):
        """Compare radiances of several collection 3 / 4 pairs.

        Arguments:
            None

        Returns:
            None
        """

        # clear old data
        self._clean(self.sink)

        # group paths by orbit number
        orbits = self._group(self.paths, lambda path: self._stage(path)['orbit'])

        # go through each orbit
        for orbit, paths in orbits.items():

            # stamp
            self._stamp('analyzing orbit {}...'.format(orbit), initial=True)

            # sort by collection
            collections = self._group(paths, lambda path: self._stage(path)['collection'])

            # get the paths
            three = collections['3'][0]
            four = collections['4'][0]

            # ingest the collection 3 path
            self.ingest(three)

            print('survey:')
            self.survey()

            # grab the radiance and mantissa data
            mantissas = self.dig('Mantissa')[0].distil()
            exponents = self.dig('Exponent')[0].distil()

            # recompose the radiance data
            radiances = self._compose(mantissas, exponents)

            # adjust for the earth sun distance
            earth = 1.49597870e11
            distance = self.dig('EarthSunDistance')[0].distil()
            radiances = radiances * (distance ** 2) * (1 / earth ** 2)

            # grab collection 4 radiances
            self.ingest(four)
            radiancesii = self.dig('radiance')[0].distil().squeeze()

            # # subset radiances by row  and wavelength
            # radiances = radiances[:, 4:15, 420:465]
            # radiancesii = radiancesii[:, 4:15, 420:465]

            # calculate differences
            differences = radiancesii - radiances
            differences = differences * (100 / radiancesii)

            # average across all pixels and rows
            orbits = radiances.mean(axis=2).mean(axis=1)
            orbitsii = radiancesii.mean(axis=2).mean(axis=1)
            averages = differences.mean(axis=2).mean(axis=1)

            # envision orbital differences
            date = self._stage(four)['date'].split('t')[0]
            destination = '{}/{}_orbit_diff.png'.format(self.sink, orbit)
            title = 'Orbit: {} / {}, Orbital Average Radiance Differences, Rows 5-15, 435 - 465 nm'.format(orbit, date)
            unit = '% difference'
            dependent = '_'
            independent = 'track'
            self._envision(numpy.array([averages]), destination, title, unit, dependent, independent)

            # grab latitudes
            latitudes = self.dig('satellite_latitude')[0].distil().squeeze()

            # find the latitude closest to 20 S and 20 N
            south = ((latitudes + 20) ** 2).argsort()[0]
            equator = ((latitudes) ** 2).argsort()[0]
            north = ((latitudes - 20) ** 2).argsort()[0]

            # find largest and smallest difference within the range
            largest = (averages[south: north + 1] ** 2).argmax() + south
            smallest = (averages[south: north + 1] ** 2).argmin() + south

            # correlate with bokeh
            print('collection 3: {}'.format(radiances[south: north + 1, 9, 465: 514].mean()))
            print('collection 4: {}'.format(radiancesii[south: north + 1, 9, 465: 514].mean()))

            # create dict
            views = {north: 'North, {} deg lat'.format(round(float(latitudes[north]), 2))}
            views.update({south: 'South, {} deg lat'.format(round(float(latitudes[south]), 2))})
            views.update({equator: 'Equator, {} deg lat'.format(round(float(latitudes[equator]), 2))})
            views.update({largest: 'Largest Abs Difference, {} deg lat'.format(round(float(latitudes[largest]), 2))})
            views.update({smallest: 'Smallest Abs Difference, {} deg lat'.format(round(float(latitudes[smallest]), 2))})

            # go through all views
            for index, description in views.items():

                # print status
                self._print('{}, {}...'.format(index, description))

                # go through each mode
                modes = ['Collection 3', 'Collection 4', 'Collection 4 - Collection 3']
                tags = ['col3', 'col4', 'diff']
                data = [radiances, radiancesii, differences]
                units = ['mol photons / m^2 nm sr s', 'mol photons / m^2 nm sr s', '% difference']
                for datum, mode, unit, tag in zip(data, modes, units, tags):

                    # envision image differences
                    date = self._stage(four)['date'].split('t')[0]
                    destination = '{}/{}_{}_{}.png'.format(self.sink, orbit, index, tag)
                    title = 'Orbit: {} / {}, {} Radiances, {}, Rows 5-15, 435 - 465 nm'.format(orbit, date, mode, description)
                    dependent = 'spatial pixel'
                    independent = 'spectral pixel'
                    self._envision(numpy.array(datum[index]), destination, title, unit, dependent, independent)

            # finish
            self._stamp('analyzed.')

        return None

    def quantify(self, paths, band, row, rowii, wavelength):
        """Break up radiance data by quantile.

         Arguments:
             band: int, band number
             paths: list of str, paths to deseasonalize
             row: int, row to check
             rowii: int, second row
            wavelength: int, neaerest wavelength in nm

        Returns:
            None
        """

        # for each path
        for path in paths:

            # ingest and grab features, other than 'Data' duplicates
            self.ingest(path)
            features = self.dig('Categories') + self.dig('IndependentVariables')

            # create destination
            destination = path.replace('.h5', '_quantiled_band{}_row{}_{}nm.h5'.format(band, row, wavelength))

            # begin formulation
            formula = Formula()

            # for each band
            band = 'band{}'.format(band)

            # weed out lower radiance scenes
            quantiles = ['00', '10', '20', '30', '40', '50', '60', '70', '80', '90']
            def quantizing(radiances, times, waves): return self._quantize(radiances, times, waves, row, rowii, wavelength)
            parameters = ['{}_binned_tropical_radiances'.format(band)]
            parameters += ['orbit_start_time_fr_yr', '{}_wavelength_bins'.format(band)]
            formats = [(band, row, rowii, wavelength, quantile) for quantile in quantiles]
            names = ['{}_binned_tropical_radiances_rows{}-{}_{}nm_{}th'.format(*details) for details in formats]
            names += ['start_time_fr_year_{}th'.format(quantile) for quantile in quantiles]
            addresses = ['Categories'] * 10 + ['IndependentVariables'] * 10
            formula.formulate(parameters, quantizing, names, addresses=addresses)

            # fill all unfilled features
            [feature.fill() for feature in features]

            # create new features, linking to Data
            cascade = self.cascade(formula, features)
            [feature.update({'link': True}) for feature in cascade + features if 'Categories' in feature.slash]

            # remove intermediates and stash
            tags = ['weed', 'five']
            features = [feature for feature in features + cascade if not any([tag in feature.name for tag in tags])]
            self._stash(features, destination, 'Data', scan=True)

        return None

    def reflect(self, path, pathii, wavelength=354.0, degrees=20, boundary=7e-7):
        """Split radiances into high reflectance and low reflectance averages.

        Arguments:
            path: str, uv radiance pathway
            pathii: str, vis radiances pathway
            wavelength: float, vis wavelength
            degrees: float, latitude bounds
            boundary: radiance value at reflectance split

        Returns:
            None
        """

        # ingest vis file
        self.ingest(pathii)

        # extract latitude, grid, and radiances
        radiances = self.dig('band3_interpolated_radiance')[0].distil()
        latitudes = self.dig('band3_latitude_tropics')[0].distil()
        grid = self.dig('band3_wavelength_grid')[0].distil()

        # extract latitude bracket
        bracket = numpy.abs(latitudes) <= degrees
        radiances = radiances[bracket]

        # determine index at grid point
        index = ((grid - wavelength) ** 2).argmin()

        # create low and high reflectance masks from particular wavelengths
        low = radiances[:, :, index] <= boundary
        high = radiances[:, :, index] >= boundary

        # create mimic
        tag = 'refl'
        names = ['band3_interpolated_radiance'] * 2
        aliases = ['band3_{}_reflectance_{}_avg'.format(depth, degrees) for depth in ('low', 'high')]
        functions = [lambda tensor: (tensor[bracket] * numpy.stack([low] * tensor.shape[2], axis=2)).mean(axis=0)]
        functions += [lambda tensor: (tensor[bracket] * numpy.stack([high] * tensor.shape[2], axis=2)).mean(axis=0)]
        self.mimic([pathii], '_', '_', names, aliases, functions, tag=tag)

        # begin mimic for band 2
        names = ['band2_interpolated_radiance'] * 2
        aliases = ['band2_{}_reflectance_{}_avg'.format(depth, degrees) for depth in ('low', 'high')]
        functions = [lambda tensor: (tensor[bracket] * numpy.stack([low] * tensor.shape[2], axis=2)).mean(axis=0)]
        functions += [lambda tensor: (tensor[bracket] * numpy.stack([high] * tensor.shape[2], axis=2)).mean(axis=0)]

        # extract even rows
        indices = [index for index in range(low.shape[1]) if index % 2 == 0]
        lowii = low[:, indices]
        highii = high[:, indices]

        # add parameters for band1
        names += ['band1_interpolated_radiance'] * 2
        aliases += ['band1_{}_reflectance_{}_avg'.format(depth, degrees) for depth in ('low', 'high')]
        functions += [lambda tensor: (tensor[bracket] * numpy.stack([lowii] * tensor.shape[2], axis=2)).mean(axis=0)]
        functions += [lambda tensor: (tensor[bracket] * numpy.stack([highii] * tensor.shape[2], axis=2)).mean(axis=0)]
        self.mimic([path], '_', '_', names, aliases, functions, tag=tag)

        return None

    def sculpt(self, tensor, folder, stub, layers, tag='layer', texts=None, spectrum=None, coordinates=None):
        """Generate a stack of two dimensional plots from layers of a tensor.

        Arguments:
            tensor: numpy.array (3-d)
            folder: str, folder to save stack
            stub: str, name for each slide
            layers: list of ints, the layers to choose
            tag: name for tag
            texts: tuple of str, (title, units, dependent label, independent label)
            spectrum: str, name of spectrum
            coordinates: dict, additional coordinate labels

        Returns:
            None
        """

        # create folder
        self._make(folder)

        # unpack texts
        texts = texts or []
        texts = [text for text in texts] + [''] * 4
        title, units, dependent, independent = texts[:4]

        # for each layer
        for layer in layers:

            # print status
            self._print('layer {}...'.format(layer))

            # extract the matrix
            matrix = tensor[layer]

            # construct the destination
            places = len(str(max(layers)))
            label = str(layer).zfill(places)
            destination = '{}/{}_{}.png'.format(folder, stub, label)

            # add label to title, and repack
            texts = (title + ',{} {}'.format(tag, label), units, dependent, independent)

            # trigger envision
            self._envision(matrix, destination, texts=texts, spectrum=spectrum, coordinates=coordinates)

        return None

    def season(self, paths, quantile=0.0):
        """Time aggregate and deseasonalize the radiance data.

         Arguments:
             paths: list of str, paths to deseasonalize

        Returns:
            None
        """

        # for each path
        for path in paths:

            # ingest and grab features, other than 'Data' duplicates
            self.ingest(path)
            features = self.dig('Categories') + self.dig('IndependentVariables')

            # create destination
            destination = path.replace('.h5', '_seasonalized_{}.h5'.format(str(int(100 * quantile)).zfill(2)))

            # begin formulation
            formula = Formula()

            # for each band
            for band in ('band1', 'band2', 'band3'):

                # weed out lower radiance scenes
                def weeding(radiances, times, days, months, years): return self._weed(radiances, times, days, months, years, quantile)
                parameters = ['{}_binned_tropical_radiances'.format(band)]
                parameters += ['orbit_start_time_fr_yr', 'orbit_start_d_o_m', 'orbit_start_mon', 'orbit_start_yr']
                names = ['{}_binned_tropical_radiances_weeded'.format(band)]
                names += ['orbit_start_time_fr_yr_weed', 'orbit_start_d_o_m_weed', 'orbit_start_mon_weed', 'orbit_start_yr_weed']
                addresses = ['Categories'] + ['IndependentVariables'] * 4
                formula.formulate(parameters, weeding, names, addresses=addresses)

                # aggregate and average over each 5 day period
                def aggregating(weeds, times, days, months, years): return self._aggregate(weeds, times, days, months, years)
                parameters = ['{}_binned_tropical_radiances_weeded'.format(band)]
                parameters += ['orbit_start_time_fr_yr_weed', 'orbit_start_d_o_m_weed', 'orbit_start_mon_weed', 'orbit_start_yr_weed']
                names = ['{}_binned_tropical_radiances_five'.format(band), 'orbit_start_time_fr_yr_five']
                names += ['orbit_season_index', 'orbit_start_mon_five', 'orbit_start_yr_five']
                addresses = ['Categories'] + ['IndependentVariables'] * 4
                formula.formulate(parameters, aggregating, names, addresses=addresses)

                # take the ratio
                def rationalizing(tensor): return (tensor - tensor[0]) / tensor[0]
                parameter = '{}_binned_tropical_radiances_five'.format(band)
                formula.formulate(parameter, rationalizing, '{}_ratio'.format(parameter))

                # remove seasonality
                def seasonalizing(tensor, seasons): return self._seasonalize(tensor, seasons)
                parameters = ['{}_binned_tropical_radiances_five'.format(band), 'orbit_season_index']
                names = ['{}_seasonalized'.format(parameters[0])]
                formula.formulate(parameters, seasonalizing, names)

                # take the ratio
                def rationalizing(tensor): return (tensor - tensor[0]) / tensor[0]
                parameter = '{}_binned_tropical_radiances_five_seasonalized'.format(band)
                formula.formulate(parameter, rationalizing, '{}_ratio'.format(parameter))

                # reaggregate by 30 day chunks
                def reaggregating(radiances, times, seasons, months, years): return self._reaggregate(radiances, times, seasons, months, years)
                parameters = ['{}_binned_tropical_radiances_five_seasonalized'.format(band)]
                parameters += ['orbit_start_time_fr_yr_five', 'orbit_season_index']
                parameters += ['orbit_start_mon_five', 'orbit_start_yr_five']
                names = [parameters[0].replace('five', 'thirty'), 'orbit_start_time_fr_yr_thirty_{}'.format(band)]
                addresses = ['Categories', 'IndependentVariables']
                formula.formulate(parameters, reaggregating, names, addresses=addresses)

                # take the ratio
                def rationalizing(tensor): return (tensor - tensor[0]) / tensor[0]
                parameter = '{}_binned_tropical_radiances_thirty_seasonalized'.format(band)
                formula.formulate(parameter, rationalizing, '{}_ratio'.format(parameter))

            # fill all unfilled features
            [feature.fill() for feature in features]

            # create new features, linking to Data
            cascade = self.cascade(formula, features)
            [feature.update({'link': True}) for feature in cascade + features if 'Categories' in feature.slash]

            # remove intermediates and stash
            tags = ['weed', 'five']
            features = [feature for feature in features + cascade if not any([tag in feature.name for tag in tags])]
            self._stash(features, destination, 'Data', scan=True)

        return None





