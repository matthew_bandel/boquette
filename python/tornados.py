# tornados to interface with tomrad fortran files

# import scipy
from scipy.io import FortranFile


# digest tables
def digest(name):
    """Digest tomrad files.

    Arguments:
        name:str

    Returns:
        dict
    """

    # begin table
    table = dict()

    # read file
    with FortranFile(name) as pointer:

        # read in contents
        nvza, nsza, nwav = pointer.read_ints()
        table['vza'] = pointer.read_reals('d')
        table['sza'] = pointer.read_reals('d')
        table['wav'] = pointer.read_reals('d')
        table['i0'] = pointer.read_reals('d').reshape(nvza, nsza, nwav)
        table['z1'] = pointer.read_reals('d').reshape(nvza, nsza, nwav)
        table['z2'] = pointer.read_reals('d').reshape(nvza, nsza, nwav)
        table['t'] = pointer.read_reals('d').reshape(nvza, nsza, nwav)
        table['sb'] = pointer.read_reals('d')

    return table