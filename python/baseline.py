import numpy as np

def base(x, y, H):

    # Constants
    Re = 6378.137  # Earth's equatorial radius in km
    Rp = 6356.752  # Earth's polar radius in km
    # H = 42164.0    # Satellite's height above Earth's center in km
    lambda_0 = np.radians(-75.2)  # Sub-satellite point longitude (for GOES-16)
    H = H + Re

    print('H: {}'.format(H))
    # # Load x, y from data (example in radians)
    # x = np.array([/* your x data */])
    # y = np.array([/* your y data */])

    # Precompute some values
    cosx = np.cos(x)
    cosy = np.cos(y)
    r_x = Re / Rp

    # Calculate the intermediate r1 value
    r1 = np.sqrt(H**2 - Re**2 * cosx**2 * cosy**2)

    # Calculate latitude and longitude
    lat = np.arctan(((H * cosx * cosy) - r1) / (r_x**2 * Re * np.sin(y)))
    lon = lambda_0 + np.arctan(np.sin(x) / (cosx * np.cos(y)))

    # Convert from radians to degrees
    lat_deg = np.degrees(lat)
    lon_deg = np.degrees(lon)

    return lat_deg, lon_deg