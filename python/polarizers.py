#!/usr/bin/env python3

# polarizers for the Polarizer class to make polar OMI plots

# import numpy functions
import numpy

# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    #matplotlib.use('Tkagg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Polarizer to do OMOCNUV analysis
class Polarizer(list):
    """Polarizer class to plot OMI orbits in polar coordinates.

    Inherits from:
        list
    """

    def __init__(self):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Polarizer instance >'

        return representation

    def _compress(self, corners, trackwise, rowwise):
        """Compress the latitude or longitude bounds based on compression factors.

        Arguments:
            corners: dict of corner arrays
            trackwise: north-south compression factor
            rowwise: east-west compression factor

        Returns:
            numpy array
        """

        # set directions
        compasses = ['southwest', 'southeast', 'northeast', 'northwest']

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = corners[compass][:, :, 1]
        #     corners[compass][:, :, 1] = numpy.where(corner < 0, corner + 360, corner)

        # get shape
        shape = corners['southwest'].shape

        # get selected trackwise indices and rowwise indices
        selection = numpy.array([index for index in range(shape[0]) if index % trackwise == 0])
        selectionii = numpy.array([index for index in range(shape[1]) if index % rowwise == 0])

        # adjust based on factors
        adjustment = numpy.array([min([index + trackwise - 1, shape[0] - 1]) for index in selection])
        adjustmentii = numpy.array([min([index + rowwise - 1, shape[1] - 1]) for index in selectionii])

        # begin compression
        compression = {}

        # adjust southwest
        compression['southwest'] = corners['southwest'][selection]
        compression['southwest'] = compression['southwest'][:, selectionii]
        compression['southeast'] = corners['southeast'][selection]
        compression['southeast'] = compression['southeast'][:, adjustmentii]
        compression['northeast'] = corners['northeast'][adjustment]
        compression['northeast'] = compression['northeast'][:, adjustmentii]
        compression['northwest'] = corners['northwest'][adjustment]
        compression['northwest'] = compression['northwest'][:, selectionii]

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = compression[compass][:, :, 1]
        #     compression[compass][:, :, 1] = numpy.where(corner >= 180, corner - 360, corner)

        return compression

    def _cross(self, bounds):
        """Adjust for longitude bounds that cross the dateline.

        Arguments:
            bounds: numpy array of bounds

        Returns:
            numpy array
        """

        # for each image
        for image in range(bounds.shape[0]):

            # and each row
            for row in range(bounds.shape[1]):

                # get the bounds
                polygon = bounds[image][row]

                # check for discrepancy
                if any([(entry < -170) for entry in polygon]) and any([entry > 170 for entry in polygon]):

                    # adjust bounds
                    bounds[image][row] = numpy.where(polygon > 170, polygon, polygon + 360)

        return bounds

    def _excise(self, polygons, tracer):
        """Excise bad polygons from the dataset.

        Arguments:
            polygons: numpy array
            tracer: numpy array

        Returns:
            tuple of numpy arrays
        """

        # extract longitudes from polygons
        longitudes = polygons[:, :, 4:]

        # calculation standard deviations
        deviation = longitudes.std(axis=2)
        mask = (deviation < 20)

        # apply mask
        polygonsii = polygons[mask]
        tracerii = tracer[mask]

        return polygonsii, tracerii

    def _frame(self, latitude, longitude):
        """Construct the corners of polygons from latitude and longitude coordinates.

        Arguments:
            latitude: numpy array
            longitude: numpy array

        Returns:
            dict of numpy arrays, the corner points
        """

        # get main shape
        shape = latitude.shape

        # # adjust longitude for negative values
        # longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # initialize all four corners, with one layer for latitude and one for longitude
        northwest = numpy.zeros((*shape, 2))
        northeast = numpy.zeros((*shape, 2))
        southwest = numpy.zeros((*shape, 2))
        southeast = numpy.zeros((*shape, 2))

        # create latitude frame, with one more row and image on each side
        frame = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frame[1: shape[0] + 1, 1: shape[1] + 1] = latitude

        # extend sides of frame by extrapolation
        frame[1: -1, 0] = 2 * latitude[:, 0] - latitude[:, 1]
        frame[1: -1, -1] = 2 * latitude[:, -1] - latitude[:, -2]
        frame[0, 1:-1] = 2 * latitude[0, :] - latitude[1, :]
        frame[-1, 1:-1] = 2 * latitude[-1, :] - latitude[-2, :]

        # extend corners
        frame[0, 0] = 2 * frame[1, 1] - frame[2, 2]
        frame[0, -1] = 2 * frame[1, -2] - frame[2, -3]
        frame[-1, 0] = 2 * frame[-2, 1] - frame[-3, 2]
        frame[-1, -1] = 2 * frame[-2, -2] - frame[-3, -3]

        # create longitude frame, with one more row and image on each side
        frameii = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frameii[1: shape[0] + 1, 1: shape[1] + 1] = longitude

        # extend sides of frame by extrapolation
        frameii[1: -1, 0] = 2 * longitude[:, 0] - longitude[:, 1]
        frameii[1: -1, -1] = 2 * longitude[:, -1] - longitude[:, -2]
        frameii[0, 1:-1] = 2 * longitude[0, :] - longitude[1, :]
        frameii[-1, 1:-1] = 2 * longitude[-1, :] - longitude[-2, :]

        # extend corners
        frameii[0, 0] = 2 * frameii[1, 1] - frameii[2, 2]
        frameii[0, -1] = 2 * frameii[1, -2] - frameii[2, -3]
        frameii[-1, 0] = 2 * frameii[-2, 1] - frameii[-3, 2]
        frameii[-1, -1] = 2 * frameii[-2, -2] - frameii[-3, -3]

        # populate interior polygon corners image by image
        for image in range(0, shape[0]):

            # and row by row
            for row in range(0, shape[1]):

                # frame indices are off by 1
                imageii = image + 1
                rowii = row + 1

                # by averaging latitude frame at diagonals
                northwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii + 1]) / 2

                # and by averaging longitude longitudes at diagonals
                northwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii + 1]) / 2

        # fix longitude edge cases, by looking for crisscrossing, and truncating
        northwest[:, :, 1] = numpy.where(northwest[:, :, 1] < longitude, northwest[:, :, 1], longitude)
        northeast[:, :, 1] = numpy.where(northeast[:, :, 1] > longitude, northeast[:, :, 1], longitude)
        southwest[:, :, 1] = numpy.where(southwest[:, :, 1] < longitude, southwest[:, :, 1], longitude)
        southeast[:, :, 1] = numpy.where(southeast[:, :, 1] > longitude, southeast[:, :, 1], longitude)

        # average all overlapping corners
        overlap = southeast[1:, :-1, :] + southwest[1:, 1:, :] + northwest[:-1, 1:, :] + northeast[:-1, :-1, :]
        average = overlap / 4

        # transfer average
        southeast[1:, :-1, :] = average
        southwest[1:, 1:, :] = average
        northwest[:-1, 1:, :] = average
        northeast[:-1, :-1, :] = average

        # average western edge
        average = (southwest[1:, 0, :] + northwest[:-1, 0, :]) / 2
        southwest[1:, 0, :] = average
        northwest[:-1, 0, :] = average

        # average eastern edge
        average = (southeast[1:, -1, :] + northeast[:-1, -1, :]) / 2
        southeast[1:, -1, :] = average
        northeast[:-1, -1, :] = average

        # average northern edge
        average = (northwest[-1, 1:, :] + northeast[-1, :-1, :]) / 2
        northwest[-1, 1:, :] = average
        northeast[-1, :-1, :] = average

        # average southern edge
        average = (southwest[0, 1:, :] + southeast[0, :-1, :]) / 2
        southwest[0, 1:, :] = average
        southeast[0, :-1, :] = average

        # # subtact 360 from any longitudes over 180
        # northwest[:, :, 1] = numpy.where(northwest[:, :, 1] >= 180, northwest[:, :, 1] - 360, northwest[:, :, 1])
        # northeast[:, :, 1] = numpy.where(northeast[:, :, 1] >= 180, northeast[:, :, 1] - 360, northeast[:, :, 1])
        # southeast[:, :, 1] = numpy.where(southeast[:, :, 1] >= 180, southeast[:, :, 1] - 360, southeast[:, :, 1])
        # southwest[:, :, 1] = numpy.where(southwest[:, :, 1] >= 180, southwest[:, :, 1] - 360, southwest[:, :, 1])

        # collect corners
        corners = {'northwest': northwest, 'northeast': northeast}
        corners.update({'southwest': southwest, 'southeast': southeast})

        return corners

    def _orientate(self, latitude, longitude):
        """Create corners by orienting latitude bounds and longitude bounds.

        Arguments:
            latitude: numpy array, latitude bounds
            longitude: numpy array, longitude bounds

        Returns:
            dict of numpy arrays
        """

        # add 360 to all negative longitude bounnds
        longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # get all cardinal directions
        cardinals = {'south': latitude.min(axis=2), 'north': latitude.max(axis=2)}
        cardinals.update({'west': longitude.min(axis=2), 'east': longitude.max(axis=2)})

        # begin corners with zeros
        compasses = ('northwest', 'northeast', 'southeast', 'southwest')
        compassesii = ('northeast', 'southeast', 'southwest', 'northwest')
        corners = {compass: numpy.zeros((latitude.shape[0], latitude.shape[1], 2)) for compass in compasses}

        # double up bounds
        latitude = latitude.transpose(2, 1, 0)
        longitude = longitude.transpose(2, 1, 0)
        latitude = numpy.vstack([latitude, latitude, latitude]).transpose(2, 1, 0)
        longitude = numpy.vstack([longitude, longitude, longitude]).transpose(2, 1, 0)

        # go through each position
        for index in range(4):

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] < longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compasses):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] > longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compassesii):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

        # subtract 360 from longitudes
        for compass in compasses:

            # subtract form longitude
            corner = corners[compass][:, :, 1]
            corners[compass][:, :, 1] = numpy.where(corner > 180, corner - 360, corner)

        return corners

    def _polymerize(self, latitude, longitude, resolution):
        """Construct polygons from latitude and longitude bounds.

        Arguments:
            latitude: numpy array of latitude bounds
            longitude: numpy array of longitude bounds
            resolution: int, vertical compression factor

        Returns:
            numpy.array
        """

        # squeeze arrays
        latitude = latitude.squeeze()
        longitude = longitude.squeeze()

        # if bounds are given
        if len(latitude.shape) > 2:

            # orient the bounds
            corners = self._orientate(latitude, longitude)

        # otherwise
        else:

            # get corners from latitude and longitude points
            corners = self._frame(latitude, longitude)

        # compress corners vertically
        corners = self._compress(corners, resolution, 1)

        # construct bounds
        compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        arrays = [corners[compass][:, :, 0] for compass in compasses]
        bounds = numpy.stack(arrays, axis=2)
        arrays = [corners[compass][:, :, 1] for compass in compasses]
        boundsii = numpy.stack(arrays, axis=2)

        # adjust polygon  boundaries to avoid crossing dateline
        boundsii = self._cross(boundsii)

        # make polygons
        polygons = numpy.dstack([bounds, boundsii])

        return polygons

    def _resolve(self, array, resolution):
        """Pare down an array to only mod zero entries based on resolution.

        Arguments:
            array: numpy array
            resolution: int, number of pixels to combine

        Returns:
            numpy array
        """

        # get indices at mod 0
        indices = numpy.array([index for index in range(array.shape[0]) if index % resolution == 0])

        # apply to array
        resolution = array[indices]

        return resolution

    def polarize(self, arrays, path, texts, scale, spectrum, limits, grid=1, **options):
        """Make matplotlib polygon figure, using a polar projection.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            path: str, filepath to destination
            texts: list of str, title and units
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                south: boolean, use south projection?
                extension: 'both', 'min', 'max', 'neither' for colarbar arrows

        Returns:
            None
        """

        # set default options
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        log = options.get('log', False)
        bar = options.get('bar', False)
        south = options.get('south', False)
        extension = options.get('extension', 'both')

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set native and projection coordinates
        native = cartopy.crs.PlateCarree()
        projection = cartopy.crs.NorthPolarStereo()
        if south:

            # set to northern projection
            projection = cartopy.crs.SouthPolarStereo()

        # set cartopy axis with coastlines
        # axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
        axis = pyplot.axes(projection=projection)
        # Set the extent for the polar plot
        # axis.set_extent([-180, 180, 60, 90], crs=native)
        # feature = cartopy.feature.NaturalEarthFeature
        # coast = feature(category='physical', name='coastline', facecolor='none', scale='10m')
        # axis.add_feature(coast, edgecolor='gray')
        # axis.add_feature(cartopy.feature.COASTLINE, linewidth=1.0, edgecolor='black')
        # _ = axis.gridlines(draw_labels=True, crs=cartopy.crs.PlateCarree())
        # _ = axis.gridlines(draw_labels=True)
        axis.coastlines(linewidth=1, zorder=5)
        # axis.add_feature(cartopy.feature.BORDERS, linestyle='-', alpha=1)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left_margin = 0.1
        right_margin = 0.1
        top_margin = 0.19
        bottom_margin = 0.18
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_extent(limits, crs=native)

        # axis.set_xlim(limits[2], limits[3])
        # axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=10)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]

                # begin polar coordinates
                polar = []
                for point in patch:

                    # convert to polar
                    radius = numpy.sqrt(point[0] ** 2 + point[1] ** 2)
                    theta = numpy.arctan2(point[1], point[1])
                    # polar.append((radius, theta))

                    # add to polar
                    polar.append(projection.transform_point(*point, native))

                # convert to polar coordinates
                # patch = numpy.array(polar) / 111320
                # patch = numpy.array(polar) / 100000
                patch = numpy.array(polar)

                # create polygon
                # patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01, transform=native)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # set up color scheme, removing first two
        colors = matplotlib.cm.get_cmap(gradient)
        # colors = colors[2:]

        # remove first two colors (magenta and light purple) from front of colormap
        # start = 40
        # selection = numpy.linspace(start, colors.N, colors.N - start + 1).astype(int)
        selection = numpy.array(selection)
        colors = matplotlib.colors.ListedColormap(colors(selection))

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        # collection.set_transform(native)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.08, position.width, 0.02])
            #colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend=extension)
            colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None


# fake data
ozone = numpy.array([[250, 260, 270], [280, 290, 300], [250, 260, 270]])
latitude = numpy.array([[-50, -50, -50], [-60, -60, -60], [-70, -70, -70]])
longitude = numpy.array([[30, 40, 50], [30, 40, 50], [30, 40, 50]])
arrays = [ozone, latitude, longitude]

# set destination path for plot
path = 'test_plot.png'

# set title and units
texts = ['fake ozone data, south pole', 'DU']

# set colorbar data range
scale = (200, 350)

# set color gradient from matplotlib, assuming using all 256 colors
spectrum = ['gist_rainbow_r', list(range(256))]

# set polar plot geographical limits, longitude then latitude
limits = (-180, 180, -50, -90)

# set grid size, 1 means full resolution, higher numbers combine pixels trackwise for quicker "rough drafts"
grid = 1

# set figure size
size = (10, 5)

# set background color
back = 'white'

# use logarithmic scale?
log = False

# add colorbar?
bar = True

# plotting south pole?
south = True

# add extension arrows to color bar ( 'both', 'neither', 'min', 'max' )
extension = 'both'

# plot
print('plotting...')
options = {'size': size, 'back': back, 'log': log, 'bar': bar, 'south': south, 'extension': extension}
Polarizer().polarize(arrays, path, texts, scale, spectrum, limits, grid, **options)




