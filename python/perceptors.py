#!/usr/bin/env python3

# perceptors.py as a sample sci-kit neural network

# import numpy functions
import numpy

# import datetime
import datetime

# import sci-kit learn multi layer perceptron, regressor ( sklearn )
from sklearn.neural_network import MLPRegressor


def perceive(matrix, truth, rate=0.00001, architecture=(10, 10), options=None, fraction=0.9, shuffle=True):
    """Train a multilayer perceptron.

    Arguments:
        matrix:1D or 2D numpy array or equivalent lists of training inputs
        truth: 1D or 2D numpy array or equivalent lists of training outputs
        rate: initial learning rate
        architecture: tuple of ints, the hidden layer sizes
        options: dict of perceptron parameters, override defaults
        fraction=0.9: training / test split
        shuffle=True: boolean, shuffle dataset?

    Returns:
        None
    """

    # convert inputs to numpy array, and if only 1-D
    matrix = numpy.array(matrix)
    if len(matrix.shape) < 2:

        # extend to 2-D
        matrix = matrix.reshape(-1, 1)

    # convert outputs to array
    truth = numpy.array(truth).squeeze()

    # generate train, test split
    partition = int(fraction * matrix.shape[0])
    indices = list(range(matrix.shape[0]))

    # if shuffling
    if shuffle:

        # sort randomly
        indices.sort(key=lambda index: numpy.random.rand())

    # generate splits
    train = indices[:partition]
    test = indices[partition:]

    # set default perceptron parameters
    parameters = {'max_iter': 200, 'hidden_layer_sizes': architecture, 'activation': 'relu', 'tol': 1e-16}
    parameters.update({'verbose': True, 'n_iter_no_change': 10, 'solver': 'adam', 'epsilon': 1e-4})
    parameters.update({'learning_rate': 'adaptive', 'learning_rate_init': rate})
    parameters.update({'momentum': 0.9, 'nesterovs_momentum': True, 'warm_start': True})
    parameters.update({'alpha': 1e-6, 'shuffle': True, 'beta_1': 0.9, 'beta_2': 0.999})
    parameters.update({'batch_size': 32})

    # update parameters with options
    options = options or {}
    parameters.update(options)

    # set up meural network
    perceptor = MLPRegressor(**parameters)

    # note time
    start = datetime.datetime.now()
    print('training perceptron {} on matrix {} and truth {}...'.format(architecture, matrix.shape, truth.shape))

    # train on training set, noteing the times
    perceptor.fit(matrix[train], truth[train])

    # note time and calculate duration
    finish = datetime.datetime.now()
    duration = finish - start
    print('finished, took {} seconds...'.format(duration.seconds))

    # grab loss curve
    curve = perceptor.loss_curve_

    # verify training by confirmation with training matrix
    confirmation = perceptor.predict(matrix[train])
    verification = 100 * ((confirmation / truth[train]) - 1)
    median = numpy.percentile(abs(verification), 50)
    maximum = abs(verification).max()
    print('median absolute training error: {} %'.format(median))
    print('maximum absolute training error: {} %'.format(maximum))

    # verify testing by prediction with testing matrix
    prediction = perceptor.predict(matrix[test])
    validation = 100 * ((prediction / truth[test]) - 1)
    median = numpy.percentile(abs(validation), 50)
    maximum = abs(validation).max()
    print('median absolute testing error: {} %'.format(median))
    print('maximum absolute testing error: {} %'.format(maximum))

    # create model object
    model = {'perceptor': perceptor, 'matrix': matrix, 'truth': truth, 'train': train, 'test': test}
    model.update({'parameters': parameters, 'curve': curve, 'duration': duration})
    model.update({'verification': verification, 'validation': validation})

    return model
