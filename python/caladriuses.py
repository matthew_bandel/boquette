# caladriuses.py for the Caladrius class to make OMI Rawflux hdf5 files

# import general tools
import sys
import re
import ast

# import yaml for reading configuration file
import yaml

# import time and datetime
import datetime
import calendar

# import math functions
import math
import numpy

# import local classes
from features import Feature
from formulas import Formula
from hydras import Hydra


# try to
try:

    # import sci-kit learn for regressions
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** sci-kit learn not installed, regressions not possible **')

# try to
try:

    # import matplotlib for plots
    from matplotlib import pyplot

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** matplotlib not installed, plotting not possible **')


# class Caladrius to do OMI data reduction
class Caladrius(Hydra):
    """Caladrius class to generate Calibration records.

    Inherits from:
        list
    """

    def __init__(self, year='', month='', start='', finish='', sink=''):
        """Initialize a Caladrius instance.

        Arguments:
            sink: str, filepath for data dump
            year: str, year
            month: str, month
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # construct source
        source = '/tis/acps/OMI/10004/OML1BCAL/{}/{}'.format(year, self._pad(month))
        self.source = source

        # initialize the base Hydra instance
        Hydra.__init__(self, source, start, finish)
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # display contents
        self._tell(self.paths)

        # create representation
        representation = ' < Caladrius instance at: {} -> {} >'.format(self.source, self.sink)

        return representation

    def darken(self, path, sink='dark'):
        """Collect dark mode data orbital file.

        Arguments:
            path: str, filepath
            sink: str, sink folder

        Returns:
            None
        """

        # make folder
        self._make('{}/orbits/{}'.format(self.sink, sink))

        # ingest the path
        self._print('collecting dark current from {}...'.format(path))
        self.ingest(path)

        # begin data
        data = {}

        # grab the orbit number and add to data
        orbit = self._stage(path)['orbit']
        data['orbit_number'] = numpy.array([[int(orbit)]])

        # grab the time
        date = self._stage(path)['date'] + '00'
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[7:9])
        hour = int(date[10:12])
        minute = int(date[12:14])
        second = int(date[14:16])
        time = datetime.datetime(year, month, day, hour, minute, second).timestamp() * 1000
        data['orbit_start_time_fr_yr'] = numpy.array([[time]])

        # for each detector
        for detector in (1, 2):

            # grab the dark current signal
            dark = self.grab('DETECTOR{}/BACKGROUND_IRRADIANCE_MODE_009/signal'.format(detector)).squeeze()

            # begin array
            array = []

            # add mean
            mean = dark.mean()
            array.append(mean)

            # add median
            median = numpy.quantile(dark, 0.5)
            array.append(median)

            # add min
            minimum = dark.min()
            array.append(minimum)

            # add max
            maximum = dark.max()
            array.append(maximum)

            # add deviation
            deviation = dark.std()
            array.append(deviation)

            # add range
            array.append(maximum - minimum)

            # add to data
            data['detector_{}/dark_current'.format(detector)] = numpy.array([array])

        # create file
        formats = (self.sink, sink, date, orbit)
        destination = '{}/orbits/{}/OML1BCAL_Dark_Current_{}_o{}.h5'.format(*formats)
        self.spawn(destination, data)

        return None

    def lighten(self, path, sink='light'):
        """Collect led mode data orbital file.

        Arguments:
            path: str, filepath
            sink: str, sink folder

        Returns:
            None
        """

        # make folder
        self._make('{}/orbits/{}'.format(self.sink, sink))

        # ingest the path
        self._print('collecting led from {}...'.format(path))
        self.ingest(path)

        # begin data
        data = {}

        # grab the orbit number and add to data
        orbit = self._stage(path)['orbit']
        data['orbit_number'] = numpy.array([[int(orbit)]])

        # grab the time
        date = self._stage(path)['date'] + '00'
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[7:9])
        hour = int(date[10:12])
        minute = int(date[12:14])
        second = int(date[14:16])
        time = datetime.datetime(year, month, day, hour, minute, second).timestamp() * 1000
        data['orbit_start_time_fr_yr'] = numpy.array([[time]])

        # for each detector
        for detector in (1, 2):

            # grab the dark current signal
            light = self.grab('DETECTOR{}/LED_MODE_011/signal'.format(detector)).squeeze()

            # begin array
            array = []

            # add mean
            mean = light.mean()
            array.append(mean)

            # add median
            median = numpy.quantile(light, 0.5)
            array.append(median)

            # add min
            minimum = light.min()
            array.append(minimum)

            # add max
            maximum = light.max()
            array.append(maximum)

            # add deviation
            deviation = light.std()
            array.append(deviation)

            # add range
            array.append(maximum - minimum)

            # add to data
            data['detector_{}/led_signal'.format(detector)] = numpy.array([array])

        # create file
        formats = (self.sink, sink, date, orbit)
        destination = '{}/orbits/{}/OML1BCAL_LED_{}_o{}.h5'.format(*formats)
        self.spawn(destination, data)

        return None

    def collect(self):
        """Collect the calibration data.

        Arguments:
            None

        Returns:
            None
        """

        # create sink folders
        self._make(self.sink)
        self._make('{}/orbits'.format(self.sink))

        # grab chronology
        chronology = self._load('../studies/chronologer/chronicles/chronology.json')

        # for each path
        for path in self.paths:

            # get orbit number
            orbit = self._pad(int(self._stage(path)['orbit']), 6)

            # try to
            try:

                # get configuration ids
                configurations = chronology[orbit]

                # check for solar dark mode
                if 9 in configurations:

                    # and collect dark mode
                    self.darken(path)

                # check for led mode
                if 11 in configurations:

                    # and collect led data
                    self.lighten(path)

                # check for wls mode
                if 29 in configurations:

                    # and collect wls data
                    self.whiten(path)

            # unless orbit missing
            except KeyError:

                # alert
                self._print('skipping {}'.format(orbit))

        return None

    def whiten(self, path, sink='white'):
        """Collect wls mode data orbital file.

        Arguments:
            path: str, filepath
            sink: str, sink folder

        Returns:
            None
        """

        # make folder
        self._make('{}/orbits/{}'.format(self.sink, sink))

        # ingest the path
        self._print('collecting wls from {}...'.format(path))
        self.ingest(path)

        # begin data
        data = {}

        # grab the orbit number and add to data
        orbit = self._stage(path)['orbit']
        data['orbit_number'] = numpy.array([[int(orbit)]])

        # grab the time
        date = self._stage(path)['date'] + '00'
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[7:9])
        hour = int(date[10:12])
        minute = int(date[12:14])
        second = int(date[14:16])
        time = datetime.datetime(year, month, day, hour, minute, second).timestamp() * 1000
        data['orbit_start_time_fr_yr'] = numpy.array([[time]])

        # for each detector
        for detector in (1, 2):

            # grab the dark current signal
            white = self.grab('DETECTOR{}/WLS_MODE_029/signal'.format(detector)).squeeze()

            # begin array
            array = []

            # add mean
            mean = white.mean()
            array.append(mean)

            # add median
            median = numpy.quantile(white, 0.5)
            array.append(median)

            # add min
            minimum = white.min()
            array.append(minimum)

            # add max
            maximum = white.max()
            array.append(maximum)

            # add deviation
            deviation = white.std()
            array.append(deviation)

            # add range
            array.append(maximum - minimum)

            # add to data
            data['detector_{}/wls_signal'.format(detector)] = numpy.array([array])

        # create file
        formats = (self.sink, sink, date, orbit)
        destination = '{}/orbits/{}/OML1BCAL_WLS_{}_o{}.h5'.format(*formats)
        self.spawn(destination, data)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad up to six
    number = 5
    arguments += [''] * number
    arguments = arguments[:number]

    # unpack arguments
    year, month, start, finish, sink = arguments

    # create caladrius
    caladrius = Caladrius(year, month, start, finish, sink)

    # collect
    caladrius.collect()