#!/usr/bin/env python3

# millipedes.py for the Millipede class to reduce OMI trending data

# import local classes
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import regex
import re

# import numpy functions
import numpy

# import datetime
import datetime

# import sleep
from time import sleep


# class Millipede to do OMI data reduction
class Millipede(Hydra):
    """Millipede class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, controller='', code='', sink='', source='', start='', finish='', clock=True):
        """Initialize a Millipede instance.

        Arguments:
            controller: str, control file name
            code: str, letter name of concatenation ('d', 'm', 'y', 's')
            sink: str, self.sink
            start: str, begining day'
            finish: str, ending day'
            archive: str, archive number
            clock: boolean, allow timing output?

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, '')

        # codify app name
        self.code = code
        self.singlet = ''
        self.plural = ''
        self._codify()

        # set control file name
        self.sink = sink
        self.source = source
        self.controller = controller
        self._assume(sink)

        # get start and end dates
        self.start = start
        self.finish = finish

        # set timing switch
        self.clock = clock

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Millipede instance at: {} >'.format(self.sink)

        return representation

    def _assume(self, sink):
        """Assume control from a default control file

        Arguments:
            sink: str, sink directory

        Returns:
            None

        Populates:
            self.controller
        """

        # if there is no control file given
        if not self.controller:

            # create control file name from sink
            self.controller = '{}/control-file.txt'.format(sink)

        # print controller
        self._print('assuming controller: {}'.format(self.controller))

        return None

    def _codify(self):
        """Create esdt names based on code.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.singlet
            self.plural
        """

        # update code, defaulting to day
        self.code = ('m' + self.code)[-1] or 'm'

        # create codes dictionary mapping plural code to singlet code
        codes = {'m': '', 'y': 'm', 's': 'y'}

        # create esdt names
        self.singlet = 'OML1BRADHIS{}'.format(codes[self.code])
        self.plural = 'OML1BRADHIS{}'.format(self.code)

        return None

    def _generate(self):
        """Generate input paths.

        Arguments:
            None

        Returns:
            list of str
        """

        # if there is a source given
        paths = []
        if self.source:

            # if months also given
            if self.start and self.finish:

                # create hydra for all days
                hydra = Hydra('{}/{}'.format(self.source, self.start, self.finish))

                # add to paths
                paths += hydra.paths

            # otherwise
            else:

                # get just the source
                hydra = Hydra(self.source)
                paths += hydra.paths

        # sort paths
        paths.sort()

        return paths

    def _plant(self, path):
        """Build the data tree from file paths.

        Arguments:
            path: str, orbital file path

        Returns:
            dict
        """

        # begin tree
        tree = {}

        # get the data at the path
        data = self._fetch(path)

        # split into orbit, filename
        pieces = path.split('/')[-1].split('.')[0].split('_')
        product = pieces[1]
        orbit = pieces[2]

        # check for orbit in tree
        if orbit not in tree.keys():

            # add orbit
            tree[orbit] = {}

        # insert data into tree
        tree[orbit][product] = data

        return tree

    def _process(self, job):
        """Extract the processing version information from a job description string.

        Arguments:
            job: str, job description

        Returns:
            dict of strings
        """

        # begin processor
        versions = {'processor': '', 'core': ''}

        # search for processor and core
        processor = re.search('OML1BPDS-[0-9].[0-9].[0-9].[0-9]', job.decode('utf8'))
        core = re.search('core_processor_version = [0-9].[0-9].[0-9].[0-9]{5}', job.decode('utf8'))

        # if processor was found
        if processor:

            # add to versions
            versions['processor'] = processor.group()

        # if core was found
        if core:

            # add to versions
            versions['core'] = core.group()

        return versions

    def aggregate(self, members, destination):
        """Aggregate using merge with two extra attempts to dodge intermittant IO errors.

        Arguments:
            members: list of str, the paths for merging
            destination: str, filename for merge

        Returns:
            None
        """

        # try to
        try:

            # merge members
            self.merge(members, destination)

        # unless IO issues
        except OSError:

            # in which case
            try:

                # print error
                self._print('OSError for {}, trying second time...'.format(destination))

                # pause for 1 second
                sleep(1)

                # merge members
                self.merge(members, destination)

            # unless IO issues again
            except OSError:

                # in which case, print error
                self._print('OSError for {}, trying third time...'.format(destination))

                # pause for 1 second
                sleep(1)

                # try again to merge members
                self.merge(members, destination)

        return None

    def control(self, trim=False, now=''):
        """Generate a control file.

        Arguments:
            trim: boolean, trim source and sink folders form inputs and outputs?
            now: overrides current now for given now, for keeping production time constant for updates

        Returns:
            None
        """

        # # get previous aggregate
        # previous = self.previous
        #
        # # trim if possible
        # if trim:
        #
        #     # remove previous folders
        #     previous = previous.split('/')[-1]

        # read in from description
        now = now or self._note()
        description = self._acquire('../doc/Description.txt')

        # copy into control
        exclusions = ['Dynamic Input Files', 'Output Files']
        control = {key: value for key, value in description.items() if key not in exclusions}

        # get app name and singlet name
        # name = description['APP Name']
        # singlet = description['Dynamic Input Files'][0]['ESDT']
        name = self.plural
        singlet = self.singlet

        # generate all input paths
        self._print('generating paths...')
        paths = self._generate()
        self._print('generated.')

        # get version of first path for use in output version
        version = self._stage(paths[-1])['version']

        # find appropriate output template for use as destination
        destination = description['Output Files'][0]['Filename']

        # create destination name and add to outputs
        destination = destination.replace('<EndTime!%Ym%m%dt%H%M%S>', self._stage(paths[-1])['date'])
        destination = destination.replace('<EndOrbit>', self._stage(paths[-1])['orbit'])
        destination = destination.replace('<ProductionTime>', now)
        destination = destination.replace('<ECSCollection>', version)

        # if not trimming
        if not trim:

            # add sink
            destination = '{}/{}'.format(self.sink, destination)

        # if trim option selected
        if trim:

            # remove source folder information
            paths = [path.split('/')[-1] for path in paths]

        # begin specifics
        specifics = {'Input Files': {singlet: paths}, 'Output Files': {name: [destination]}}

        # update with specifics
        control.update(specifics)

        # dump into control
        self._dispense(control, self.controller)

        # improve spacing
        self._disperse(self.controller)

        return None

    def crawl(self, temporary='../tmp'):
        """Feed all files through stack.

        Arguments:
            temporary: str, folder name for temporary files

        Returns:
            None
        """

        # open up the control file and retrieve input and output files
        control = self._acquire(self.controller)

        # get input paths and output paths
        paths = [path for members in control['Input Files'].values() for path in members]
        paths.sort(key=lambda path: self._stage(path)['date'])
        destination = [path for members in control['Output Files'].values() for path in members][0]

        # # set start and finish based on paths
        # self.start = self._stage(paths[0])['date']
        # self.finish = self._stage(paths[-1])['date']

        # stack singlets together
        self.devour(paths, destination, temporary)

        return None

    def define(self):
        """Define the earth science data type.

        Arguments:
            None

        Returns:
            None
        """

        # begin definition
        definition = {}

        # set time periods
        periods = {'d': 'Days', 'm': 'Months', 'y': 'Years'}

        # # get singlet esdt name
        # singlets = [singlet.strip() for singlet in input('singlet ESDTs? ').split(',')]
        # plurals = [plural.strip() for plural in input('plural ESDTs for series? ').split(',')]

        # get esdts
        singlets = [self.singlet]
        plurals = [self.plural]

        # for each singlet, plural pair
        for singlet, plural in zip(singlets, plurals):

            # set sizes
            size = 'Orbit'
            sizes = {'d': 'Day', 'm': 'Month', 'y': 'Year'}
            for letter, chunk in sizes.items():

                # if singlet ends in letter
                if singlet.endswith(letter):

                    # set size
                    size = chunk

            # add esdt name and processing level
            definition['ESDT Name'] = plural
            definition['Processing Level'] ='L3'

            # add Long Name
            name = 'OMI/Aura Timewise Concatenation of Single {} {} Files'.format(size, singlet)
            definition['Long Name'] = name

            # add description
            formats = (size.lower(), singlet)
            text = 'Timewise concatenations of single {} {} files derived from OMI L1B Data '.format(*formats)
            text += 'into time series.'
            definition['Description'] = text

            # add conntacts
            definition['Science Team Contacts'] = ['Matthew Bandel (matthew.bandel@ssaihq.com)']
            definition['Science Team Contacts'] += ['David Haffner (david.haffner@ssaihq.com)']
            definition['Support Contact'] = 'Phillip Durbin (pdurbin@sesda.com)'

            # set storage dictionary for aggregate based on singlet size
            storage = {'Orbit': ('15 MB', '75 MB'), 'Day': ('450 MB', '2 GB'), 'Month': ('5 GB', '25 GB')}

            # define storage size
            definition['Minimum Size'] = storage[size][0]
            definition['Maximum Size'] = storage[size][1]

            # define file name pattern
            pattern = '<Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>'
            pattern += '_v<Collection>-<ProductionTime>.<File Format>'
            definition['Filename Pattern'] = pattern

            # add filename elements
            definition['Platform'] = 'Aura'
            definition['Instrument'] = 'OMI'
            definition['File Format'] = 'h5'

            # add other processing elements
            definition['Period'] = '{}=1'.format(periods[plural[-1]])
            definition['Archive Method'] = 'Compress'
            definition['File SubTable Type'] = 'L3'
            definition['Extractor Type'] = 'Filename_L3'
            definition['Metadata Type'] = 'timerange'
            definition['Parser'] = 'TimeAveragedFile'
            definition['keypattern'] = '<StartTime>_<EndTime>'

            # dump into yaml
            destination = '../doc/{}.txt'.format(plural)
            self._dispense(definition, destination)

            # clean up spacing
            self._disperse(destination)

        return None

    def describe(self, version=None):
        """Generate the descriptions.txt file.

        Arguments:
            version: str, version number

        Returns:
            None
        """

        # get singlet esdt name and version numbers
        # singlets = [singlet.strip() for singlet in input('single orbit ESDTs? ').split(',')]
        # plurals = [plural.strip() for plural in input('plural ESDTs for series? ').split(',')]
        version = version or input('version? ')

        # set singlet and plural esdt names
        singlet = self.singlet
        plurals = [self.plural]

        # load in the esdt info
        definitions = [self._acquire('../doc/{}.txt'.format(plural)) for plural in plurals]
        definition = definitions[0]

        # begin description
        description = {}

        # add APP Name
        name = definition['ESDT Name']
        description['APP Name'] = name

        # add APP Type
        description['APP Type'] = 'OMI'

        # add APP Version, from input if not specified
        description['APP Version'] = version

        # add Long Name
        description['Long Name'] = definition['Long Name']

        # add description, apply block form
        description['Description'] = '>\n' + definition['Description']

        # add algorithm lead
        description['Lead Algorithm Scientist'] = definition['Science Team Contacts'][0]

        # add other scientists
        description['Other Algorithm Scientists'] = definition['Science Team Contacts'][1]

        # add software developer
        description['Software Developer'] = definition['Science Team Contacts'][0]

        # add software developer
        description['Support Contact'] = definition['Support Contact']

        # add structure field
        structure = '>\nRun {}.py as a python program, using the control.txt file path as its argument.'.format(name)
        description['Structure'] = structure

        # add operational scenario
        scenario = '>\nRun over all orbits within given start and end times.'
        description['Operational Scenario'] = scenario

        # add the period
        description['Period'] = definition['Period']

        # add the execution
        description['EXE'] = {'Name': '{}.py'.format(name), 'Program': '{}.py'.format(name)}

        # begin description of dynamic input files
        dynamic = 'Dynamic Input Files'

        # set sizes
        size = 'Orbit'
        sizes = {'d': 'Day', 'm': 'Month', 'y': 'Year'}
        for letter, chunk in sizes.items():

            # if singlet ends in letter
            if singlet.endswith(letter):

                # set size
                size = chunk

        # make entry for singlets
        contents = 'OMI Level 1B Single {} {} File'.format(size, singlet)
        description[dynamic] = [{'ESDT': singlet, 'Rule': 'Required', 'Desc': contents}]

        # # make entry for previous
        # contents = 'OMI L1B Time Series'.format(singlet)
        # description[dynamic] += [{'ESDT': plural, 'Rule': 'Optional', 'Desc': contents, 'ActionIfFailed': 'Continue'}]

        # start outputs
        output = 'Output Files'

        # begin entry for Filename
        pattern = definition['Filename Pattern']

        # for each filename parameterr
        for parameter in ('Instrument', 'Platform', 'ESDT Name', 'Processing Level', 'File Format'):

            # replace with esdt definition
            pattern = pattern.replace('<{}>'.format(parameter), definition[parameter])

        # add start time format and collection for now
        pattern = pattern.replace('<DataDate>', '<EndTime!%Ym%m%dt%H%M%S>')
        pattern = pattern.replace('<OrbitNumber>', '<EndOrbit>')
        pattern = pattern.replace('<Collection>', '<ECSCollection>')

        # add output description
        contents = 'Concatenation of single {} files'.format(size.lower())
        description[output] = [{'Filename': pattern, 'ESDT': name, 'Rule': 'Required', 'Desc': contents}]

        # make entry for Runtime Parameters
        runtime = 'Runtime Parameters'
        description[runtime] = [{'Param': 'AppShortName', 'Value': name}]
        description[runtime] += [{'Param': 'Instrument', 'Value': definition['Instrument']}]
        description[runtime] += [{'Param': 'Source', 'Value': definition['Instrument']}]
        description[runtime] += [{'Param': 'Platform', 'Value': definition['Platform']}]
        description[runtime] += [{'Param': 'OrbitNumber', 'Value': '"<OrbitNumber>"'}]
        description[runtime] += [{'Param': 'StartTime', 'Value': '"<StartTime>"'}]
        description[runtime] += [{'Param': 'EndTime', 'Value': '"<EndTime>"'}]
        description[runtime] += [{'Param': 'StartOrbit', 'Value': '"<StartOrbit>"'}]
        description[runtime] += [{'Param': 'EndOrbit', 'Value': '"<EndOrbit>"'}]
        description[runtime] += [{'Param': 'ProductionTime', 'Value': '"<ProductionTime>"'}]
        description[runtime] += [{'Param': 'ECSCollection', 'Value': '"<ECSCollection>"'}]
        description[runtime] += [{'Param': 'Maximum Level of Detail', 'Value': '2'}]

        # set production rule variants based on singlet esdts
        keys = {'OML1BRADHIS': 'PGEKeyMonthlyParams', 'OML1BRADHISm': 'PGEKeyYearlyParams'}
        times = {'OML1BRADHIS': 'OrbitTimeRange', 'OML1BRADHISm': 'L3MatchTimeRange'}

        # make entry for Production Rules
        rules = 'Production Rules'
        description[rules] = [{'Rule': keys[singlet]}]
        description[rules] += [{'Rule': times[singlet], 'ESDT': singlet, 'Min_Files': 2}]

        # add compiler
        description['Compilers'] = [{'CI': 'python', 'Version': '3.6.8'}]

        # add environment
        description['Environment'] = [{'CI': 'env_setup', 'Version': '0.0.9'}]
        description['Environment'] += [{'CI': 'python', 'Version': '3.6.8'}]

        # add operating system
        description['Operating System'] = [{'CI': 'Linux', 'Version': '2.6.9'}]

        # dump into yaml
        destination = '../doc/Description.txt'.format(self.sink)
        self._dispense(description, destination)

        # improve spacing
        self._disperse(destination)

        return None

    def devour(self, paths, destination, temporary='../tmp'):
        """Fuse all individual trending files into larger aggregates.

        Arguments:
            paths: list of single orbit paths to stack
            previous: str, file path of previous stack
            destination: str, file path of stacked file
            temporary: str, temporary directory

        Returns:
            None
        """

        # sort paths
        paths.sort()

        # default leading dimension to false, but check for daily files
        lead = False
        if 'OML1BRADHISm' in destination:

            # use leading dimension
            lead = True

        # merge files
        self.merge(paths, destination, lead=lead)

        # # sort all paths and defaul them to befores segment
        # paths.sort()
        # befores = paths
        # afters = []
        #
        # # if a valid previous filepath is given
        # if previous:
        #
        #     # collect start and finish dates (production is second date)
        #     start = self._stage(previous)['date']
        #     finish = self._stage(previous.replace(start, '_'))['date']
        #
        #     # check for inclusion
        #     if self.start <= start and self.finish >= finish:
        #
        #         # sort paths
        #         befores = [path for path in paths if self._stage(path)['date'] < start]
        #         afters = [path for path in paths if self._stage(path)['date'] > finish]
        #
        #     # otherwise ignore previous
        #     else:
        #
        #         # nullify previous
        #         previous = ''
        #
        # # set segments, and eliminate any empties
        # segments = {'before': befores, 'after': afters}
        # segments = {segment: paths for segment, paths in segments.items() if len(paths) > 0}
        #
        # # create tmepory foldeer
        # self._make(temporary)
        # self._make('{}/segments'.format(temporary))
        #
        # # for each segment
        # for segment, paths in segments.items():
        #
        #     # print status
        #     self._stamp('stacking {} orbits...'.format(len(paths)), initial=True, clock=self.clock)
        #
        #     # create temp folders
        #     folder = '{}/{}'.format(temporary, segment)
        #     self._make(folder)
        #     self._make('{}/days'.format(folder))
        #     self._make('{}/months'.format(folder))
        #     self._make('{}/years'.format(folder))
        #
        #     # group paths by days
        #     days = self._group(paths, lambda path: self._stage(path)['day'])
        #     for day, members in days.items():
        #
        #         # print status
        #         self._stamp('stacking {}...'.format(day), clock=self.clock)
        #
        #         # sort paths, construct destination, and merge
        #         members.sort()
        #         daily = '{}/days/{}.h5'.format(folder, day)
        #         self.aggregate(members, daily)
        #
        #     # status
        #     self._stamp('stacking all files in {}/days...'.format(folder), clock=self.clock)
        #
        #     # group paths by days
        #     months = self._group(self._see('{}/days'.format(folder)), lambda path: path.split('/')[-1][:7])
        #     for month, members in months.items():
        #
        #         # print status
        #         self._stamp('stacking {}...'.format(month), clock=self.clock)
        #
        #         # sort paths, construct destination, and merge
        #         members.sort()
        #         monthly = '{}/months/{}.h5'.format(folder, month)
        #         self.aggregate(members, monthly)
        #
        #     # status
        #     self._stamp('stacking all files in {}/months...'.format(folder), clock=self.clock)
        #
        #     # group paths by days
        #     years = self._group(self._see('{}/months'.format(folder)), lambda path: path.split('/')[-1][:4])
        #     for year, members in years.items():
        #
        #         # print status
        #         self._stamp('stacking {}...'.format(year), clock=self.clock)
        #
        #         # sort paths, construct destination, and merge
        #         members.sort()
        #         yearly = '{}/years/{}.h5'.format(folder, year)
        #         self.aggregate(members, yearly)
        #
        #     # status
        #     self._stamp('stacking all files in {}/years...'.format(folder), clock=self.clock)
        #
        #     # merge into final segment
        #     yearlies = self._see('{}/years'.format(folder))
        #     yearlies.sort()
        #     self.aggregate(yearlies, '{}/segments/{}.h5'.format(temporary, segment))
        #
        #     # print status
        #     self._stamp('stacked.', clock=self.clock)
        #
        #     # empty temporary folders
        #     self._clean('{}/days'.format(folder), force=True)
        #     self._clean('{}/months'.format(folder), force=True)
        #     self._clean('{}/years'.format(folder), force=True)
        #
        # # get all newly made segment files
        # segments = self._see('{}/segments'.format(temporary))
        # front = ([path for path in segments if 'before' in path] + [''])[0]
        # back = ([path for path in segments if 'after' in path] + [''])[0]
        #
        # # merge all valid segments
        # segments = [path for path in (front, previous, back) if path]
        # self.aggregate(segments, destination)
        #
        # # clean up segments
        # self._clean('{}/segments'.format(temporary), force=True)

        return None

    def imitate(self):
        """Imitate the /run folder setup of the APP, assuming currently in /tbl"

        Arguments:
            None

        Returns:
            None
        """

        # create run directory
        self._make('../run')

        # copy all /bin files into /run
        paths = self._see('../bin')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /tbl files into /run
        paths = self._see('../tbl')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /utd files into /run
        paths = self._see('../utd')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        return None

    def mock(self, orbit):
        """Prepare a mock configure file to test in RunApp.

        Arguments:
            date: str, datestring
            destination: file name

        Returns:
            None
        """

        # open up the description file
        description = self._acquire('../doc/Description.txt')
        version = description['APP Version']

        # prepare mock attributes
        mock = {'archiveset': 70004, 'ECSCollection': 4, 'OrbitNumber': orbit}
        mock.update({'PGE': 'OMSTACK', 'PGEVersion': version})
        mock.update({'InstrumentConfigAS': 70004, 'source': 'OMI'})

        # set inside runtimte
        mock = {'Runtime Parameters': mock}

        # dump into file
        destination = '../bin/OMSTACK.input'
        self._dispense(mock, destination)

        return None


# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 6
    arguments = arguments[:6]

    # check for clock option to time processes
    clock = any([option in options for option in ['-clock', '--clock', '-c', '--c']])

    # check for renew option to renew the control file
    refresh = any([option in options for option in ['-refresh', '--refresh', '-r', '--r']])

    # unpack control file name, output directory, input directory, and subdirectory range
    controller, code, sink, source, start, finish = arguments

    # create millipede instance
    millipede = Millipede(controller, code, sink, source, start, finish, clock)

    # if control file is to be refreshed
    if refresh:

        # create Description.txt and control.txt file from given directories
        millipede.control()

    # compress files
    millipede.crawl()
