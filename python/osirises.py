#!/usr/bin/env python3

# osirises.py for the Osiris class to process OMTO3 data

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys
import os

# import subprocess
import subprocess

# import collections
from collections import Counter

# import regex
import re

# import itertools
import itertools

# import numpy functions
import numpy
import math

# import random forest
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier

# import datetime
import datetime

# import PIL for images
import PIL

# import requests
import requests

# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    #matplotlib.use('Tkagg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Osiris to do OMI data reduction
class Osiris(Hydra):
    """Osiris class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # add squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Osiris instance at {} >'.format(self.sink)

        return representation

    def _calculate(self, coefficients, references, pixels):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            pixels: int, number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        # get relevant dimensions
        orbits, rows, degree = coefficients.shape
        print('orbits: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))
        self._stamp('calculating wavelengths...', initial=True)

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns] * rows)
        indices = numpy.array([image] * orbits)

        # expand tensor of column references
        references = numpy.array([references] * rows)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(degree)])
        powers = powers.transpose(1, 2, 3, 0)

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)
        coefficients = coefficients.transpose(1, 2, 0, 3)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=3)

        # timestamp
        self._stamp('calculated.')

        return wavelengths

    def _compress(self, corners, trackwise, rowwise):
        """Compress the latitude or longitude bounds based on compression factors.

        Arguments:
            corners: dict of corner arrays
            trackwise: north-south compression factor
            rowwise: east-west compression factor

        Returns:
            numpy array
        """

        # set directions
        compasses = ['southwest', 'southeast', 'northeast', 'northwest']

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = corners[compass][:, :, 1]
        #     corners[compass][:, :, 1] = numpy.where(corner < 0, corner + 360, corner)

        # get shape
        shape = corners['southwest'].shape

        # get selected trackwise indices and rowwise indices
        selection = numpy.array([index for index in range(shape[0]) if index % trackwise == 0])
        selectionii = numpy.array([index for index in range(shape[1]) if index % rowwise == 0])

        # adjust based on factors
        adjustment = numpy.array([min([index + trackwise - 1, shape[0] - 1]) for index in selection])
        adjustmentii = numpy.array([min([index + rowwise - 1, shape[1] - 1]) for index in selectionii])

        # begin compression
        compression = {}

        # adjust southwest
        compression['southwest'] = corners['southwest'][selection]
        compression['southwest'] = compression['southwest'][:, selectionii]
        compression['southeast'] = corners['southeast'][selection]
        compression['southeast'] = compression['southeast'][:, adjustmentii]
        compression['northeast'] = corners['northeast'][adjustment]
        compression['northeast'] = compression['northeast'][:, adjustmentii]
        compression['northwest'] = corners['northwest'][adjustment]
        compression['northwest'] = compression['northwest'][:, selectionii]

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = compression[compass][:, :, 1]
        #     compression[compass][:, :, 1] = numpy.where(corner >= 180, corner - 360, corner)

        return compression

    def _cross(self, bounds):
        """Adjust for longitude bounds that cross the dateline.

        Arguments:
            bounds: numpy array of bounds

        Returns:
            numpy array
        """

        # for each image
        for image in range(bounds.shape[0]):

            # and each row
            for row in range(bounds.shape[1]):

                # get the bounds
                polygon = bounds[image][row]

                # check for discrepancy
                if any([(entry < -170) for entry in polygon]) and any([entry > 170 for entry in polygon]):

                    # adjust bounds
                    bounds[image][row] = numpy.where(polygon > 170, polygon, polygon + 360)

        return bounds

    def _decompose(self, irradiance, distance):
        """Decompose the irradiance into mantissa and exponent.

        Arguments:
            irradiance: numpy array
            distance: earth sun distance

        Returns:
            numpy arrays, mantissa, exponent
        """

        # define conversion factor
        factor = 6.02214076e23 / 10000

        # adjust for sun earth distance and convert to photons
        photons = irradiance * factor / float(distance) ** 2

        # calculate exponent
        exponent = (numpy.round(numpy.log10(photons)) -4).astype('int8')

        # calculate mantissa
        mantissa = (photons / 10.0 ** exponent).astype('int16')

        return mantissa, exponent

    def _frame(self, latitude, longitude):
        """Construct the corners of polygons from latitude and longitude coordinates.

        Arguments:
            latitude: numpy array
            longitude: numpy array

        Returns:
            dict of numpy arrays, the corner points
        """

        # get main shape
        shape = latitude.shape

        # # adjust longitude for negative values
        # longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # initialize all four corners, with one layer for latitude and one for longitude
        northwest = numpy.zeros((*shape, 2))
        northeast = numpy.zeros((*shape, 2))
        southwest = numpy.zeros((*shape, 2))
        southeast = numpy.zeros((*shape, 2))

        # create latitude frame, with one more row and image on each side
        frame = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frame[1: shape[0] + 1, 1: shape[1] + 1] = latitude

        # extend sides of frame by extrapolation
        frame[1: -1, 0] = 2 * latitude[:, 0] - latitude[:, 1]
        frame[1: -1, -1] = 2 * latitude[:, -1] - latitude[:, -2]
        frame[0, 1:-1] = 2 * latitude[0, :] - latitude[1, :]
        frame[-1, 1:-1] = 2 * latitude[-1, :] - latitude[-2, :]

        # extend corners
        frame[0, 0] = 2 * frame[1, 1] - frame[2, 2]
        frame[0, -1] = 2 * frame[1, -2] - frame[2, -3]
        frame[-1, 0] = 2 * frame[-2, 1] - frame[-3, 2]
        frame[-1, -1] = 2 * frame[-2, -2] - frame[-3, -3]

        # create longitude frame, with one more row and image on each side
        frameii = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frameii[1: shape[0] + 1, 1: shape[1] + 1] = longitude

        # extend sides of frame by extrapolation
        frameii[1: -1, 0] = 2 * longitude[:, 0] - longitude[:, 1]
        frameii[1: -1, -1] = 2 * longitude[:, -1] - longitude[:, -2]
        frameii[0, 1:-1] = 2 * longitude[0, :] - longitude[1, :]
        frameii[-1, 1:-1] = 2 * longitude[-1, :] - longitude[-2, :]

        # extend corners
        frameii[0, 0] = 2 * frameii[1, 1] - frameii[2, 2]
        frameii[0, -1] = 2 * frameii[1, -2] - frameii[2, -3]
        frameii[-1, 0] = 2 * frameii[-2, 1] - frameii[-3, 2]
        frameii[-1, -1] = 2 * frameii[-2, -2] - frameii[-3, -3]

        # populate interior polygon corners image by image
        for image in range(0, shape[0]):

            # and row by row
            for row in range(0, shape[1]):

                # frame indices are off by 1
                imageii = image + 1
                rowii = row + 1

                # by averaging latitude frame at diagonals
                northwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii + 1]) / 2

                # and by averaging longitude longitudes at diagonals
                northwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii + 1]) / 2

        # fix longitude edge cases, by looking for crisscrossing, and truncating
        northwest[:, :, 1] = numpy.where(northwest[:, :, 1] < longitude, northwest[:, :, 1], longitude)
        northeast[:, :, 1] = numpy.where(northeast[:, :, 1] > longitude, northeast[:, :, 1], longitude)
        southwest[:, :, 1] = numpy.where(southwest[:, :, 1] < longitude, southwest[:, :, 1], longitude)
        southeast[:, :, 1] = numpy.where(southeast[:, :, 1] > longitude, southeast[:, :, 1], longitude)

        # average all overlapping corners
        overlap = southeast[1:, :-1, :] + southwest[1:, 1:, :] + northwest[:-1, 1:, :] + northeast[:-1, :-1, :]
        average = overlap / 4

        # transfer average
        southeast[1:, :-1, :] = average
        southwest[1:, 1:, :] = average
        northwest[:-1, 1:, :] = average
        northeast[:-1, :-1, :] = average

        # average western edge
        average = (southwest[1:, 0, :] + northwest[:-1, 0, :]) / 2
        southwest[1:, 0, :] = average
        northwest[:-1, 0, :] = average

        # average eastern edge
        average = (southeast[1:, -1, :] + northeast[:-1, -1, :]) / 2
        southeast[1:, -1, :] = average
        northeast[:-1, -1, :] = average

        # average northern edge
        average = (northwest[-1, 1:, :] + northeast[-1, :-1, :]) / 2
        northwest[-1, 1:, :] = average
        northeast[-1, :-1, :] = average

        # average southern edge
        average = (southwest[0, 1:, :] + southeast[0, :-1, :]) / 2
        southwest[0, 1:, :] = average
        southeast[0, :-1, :] = average

        # # subtact 360 from any longitudes over 180
        # northwest[:, :, 1] = numpy.where(northwest[:, :, 1] >= 180, northwest[:, :, 1] - 360, northwest[:, :, 1])
        # northeast[:, :, 1] = numpy.where(northeast[:, :, 1] >= 180, northeast[:, :, 1] - 360, northeast[:, :, 1])
        # southeast[:, :, 1] = numpy.where(southeast[:, :, 1] >= 180, southeast[:, :, 1] - 360, southeast[:, :, 1])
        # southwest[:, :, 1] = numpy.where(southwest[:, :, 1] >= 180, southwest[:, :, 1] - 360, southwest[:, :, 1])

        # collect corners
        corners = {'northwest': northwest, 'northeast': northeast}
        corners.update({'southwest': southwest, 'southeast': southeast})

        return corners

    def _imbibe(self, bar):
        """Convert a colorbar image into colorbar colors.

        Arguments:
            bar: str, filename to colorbar

        Returns:
            None
        """

        # get colorbar image
        image = PIL.Image.open(bar)
        array = numpy.array(image)

        # get mid point
        half = int(array.shape[0] / 2)

        # get all colors
        colors = array[half, :, :3]

        # write to file
        text = [' '.join([str(channel / 255) for channel in line]) for line in colors]
        destination = bar.replace('.png', '.txt')
        self._jot(text, destination)

        return None

    def _orientate(self, latitude, longitude):
        """Create corners by orienting latitude bounds and longitude bounds.

        Arguments:
            latitude: numpy array, latitude bounds
            longitude: numpy array, longitude bounds

        Returns:
            dict of numpy arrays
        """

        # add 360 to all negative longitude bounnds
        longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # get all cardinal directions
        cardinals = {'south': latitude.min(axis=2), 'north': latitude.max(axis=2)}
        cardinals.update({'west': longitude.min(axis=2), 'east': longitude.max(axis=2)})

        # begin corners with zeros
        compasses = ('northwest', 'northeast', 'southeast', 'southwest')
        compassesii = ('northeast', 'southeast', 'southwest', 'northwest')
        corners = {compass: numpy.zeros((latitude.shape[0], latitude.shape[1], 2)) for compass in compasses}

        # double up bounds
        latitude = latitude.transpose(2, 1, 0)
        longitude = longitude.transpose(2, 1, 0)
        latitude = numpy.vstack([latitude, latitude, latitude]).transpose(2, 1, 0)
        longitude = numpy.vstack([longitude, longitude, longitude]).transpose(2, 1, 0)

        # go through each position
        for index in range(4):

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] < longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compasses):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] > longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compassesii):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

        # subtract 360 from longitudes
        for compass in compasses:

            # subtract form longitude
            corner = corners[compass][:, :, 1]
            corners[compass][:, :, 1] = numpy.where(corner > 180, corner - 360, corner)

        return corners

    def _paint(self, arrays, path, texts, scale, spectrum, limits, grid=1, **options):
        """Make matplotlib polygon figure.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            texts: list of str, title and units
            path: str, filepath to destination
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                north: boolean, use north projection?
                projection: name of projection

        Returns:
            None
        """

        # set defaults
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        bar = options.get('bar', True)
        log = options.get('log', False)
        north = options.get('north', True)
        extension = options.get('extension', 'both')

        # set projections
        native = cartopy.crs.PlateCarree()
        projection = options.get('projection', cartopy.crs.PlateCarree())

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set cartopy axis with coastlines
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=3)
        _ = axis.gridlines(draw_labels=True)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left_margin = 0.1
        right_margin = 0.1
        top_margin = 0.19
        bottom_margin = 0.18
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_xlim(limits[2], limits[3])
        axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=14)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]
                patch = numpy.array(patch)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # try
        try:

            # set up color scheme, removing first two
            colors = matplotlib.cm.get_cmap(gradient)
            selection = numpy.array(selection)

            # set up color map
            colors = matplotlib.colors.ListedColormap(colors(selection))

        # except
        except ValueError:

            # set up color scheme, removing first two
            text = self._know(gradient)
            tuples = [tuple([float(channel) for channel in line.split()]) for line in text]

            # set up color map
            colors = matplotlib.colors.ListedColormap(tuples)

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        collection = axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.08, position.width, 0.02])
            #colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend=extension)
            colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None

    def _pant(self, arrays, path, texts, scale, spectrum, limits, grid=1, size=None, log=False, bar=False, back=None):
        """Make matplotlib polygon figure.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            texts: list of str, title and units
            path: str, filepath to destination
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            size: tuple of ints, figure size
            log: boolean, use log scale?
            bar: boolean, add colorbar?
            back: str, background color

        Returns:
            None
        """

        # set defaults
        size = size or (10, 5)
        back = back or 'white'

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set cartopy axis with coastlines
        # axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
        # axis.coastlines(linewidth=3)
        # _ = axis.gridlines(draw_labels=True)

        # aet up axis
        axis = pyplot.gca()
        # axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # # if not colorbar
        # if not bar:
        #
        #     # set aspects
        #     axis.set_aspect('auto')
        #     axis.set_global()

        # set axis limits and title
        axis.set_xlim(limits[2], limits[3])
        axis.set_ylim(limits[0], limits[1])
        axis.set_title(title)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]
                patch = numpy.array(patch)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # set up color scheme, removing first two
        colors = matplotlib.cm.get_cmap(gradient)
        # colors = colors[2:]

        # remove first two colors (magenta and light purple) from front of colormap
        # start = 40
        # selection = numpy.linspace(start, colors.N, colors.N - start + 1).astype(int)
        selection = numpy.array(selection)
        colors = matplotlib.colors.ListedColormap(colors(selection))

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.06, position.width, 0.02])
            colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend='both')
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None

    def _plant(self, data, target, destination, header=None, classify=True):
        """Use random forest to predict target value from data

        Arguments:
            data: dict of str, numpy array
            target: str, name of target variable
            destination: str, pathname for destination
            header: header for report
            classify: boolean, use classifier?

        Returns:
            None
        """

        # assemble matrix
        names = [name for name in data.keys() if name != target] + [target]
        train = numpy.array([data[name] for name in names]).transpose(1, 0)

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # begin report
        report = header or ['Random forest for {}'.format(target)]
        report.append('')

        # if classifier
        if classify:

            # run random forest
            self._stamp('running random forest for {}...'.format(target), initial=True)
            self._print('matrix: {}'.format(matrix.shape))
            forest = RandomForestClassifier(n_estimators=100, max_depth=5)
            forest.fit(matrix, truth)

        # otherwise
        else:

            # run random forest
            self._stamp('running random forest for {}...'.format(target), initial=True)
            self._print('matrix: {}'.format(matrix.shape))
            forest = RandomForestRegressor(n_estimators=100, max_depth=5)
            forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(names, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        self._jot(report, destination)

        # print status
        self._stamp('planted.')

        return None

    def _polymerize(self, latitude, longitude, resolution):
        """Construct polygons from latitude and longitude bounds.

        Arguments:
            latitude: numpy array of latitude bounds
            longitude: numpy array of longitude bounds
            resolution: int, vertical compression factor

        Returns:
            numpy.array
        """

        # squeeze arrays
        latitude = latitude.squeeze()
        longitude = longitude.squeeze()

        # if bounds are given
        if len(latitude.shape) > 2:

            # orient the bounds
            corners = self._orientate(latitude, longitude)

        # otherwise
        else:

            # get corners from latitude and longitude points
            corners = self._frame(latitude, longitude)

        # compress corners vertically
        corners = self._compress(corners, resolution, 1)

        # construct bounds
        compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        arrays = [corners[compass][:, :, 0] for compass in compasses]
        bounds = numpy.stack(arrays, axis=2)
        arrays = [corners[compass][:, :, 1] for compass in compasses]
        boundsii = numpy.stack(arrays, axis=2)

        # adjust polygon  boundaries to avoid crossing dateline
        boundsii = self._cross(boundsii)

        # make polygons
        polygons = numpy.dstack([bounds, boundsii])

        return polygons

    def _seek(self, group, names=None):
        """Seek nested metadata attributes in a netcdf group.

        Arguments:
            group: netCDF group
            names: list of previous groups

        Returns:
            None
        """

        # set default names
        names = names or []

        # print name
        self._print('')
        self._print('{}: '.format('/'.join(names)))

        # get all attributes
        attributes = group.ncattrs()

        # for each attribute
        [self._print(attribute, group.getncattr(attribute)) for attribute in attributes]

        # get all nested gruops
        groups = group.groups
        for key in groups.keys():

            # check next level
            self._seek(group[key], names + [key])

        return None

    def acclimatize(self, year=2006, month=11, day=28, archive=95042):
        """Examine Cloud pressure differences between OMCLDRR and OMTO3.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            archive: int, firt archive set
            archiveii: int, second archive set

        Returns:
            None
        """

        # make plots folder
        folder = 'plots/climatology'
        folderii = 'plots/process'
        self._make('{}/{}'.format(self.sink, folder))
        self._make('{}/{}'.format(self.sink, folderii))

        # create hydras
        date = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(archive, *date))
        hydraii = Hydra('/tis/acps/OMI/{}/OMCLDRR/{}/{}/{}'.format(archive, *date))

        # set minnimum and maximum pressures in hPa
        minimum = 250
        maximum = 1013.25

        # begin report
        report = ['Cloud Pressure Differences, OMTO3 vs OMCLDRR, {}m{}{}'.format(*date)]

        # begin stubs
        stubs = {'lower': 'Lower than {} hPa'.format(minimum)}
        stubs.update({'greater': 'Greater than {} hPa'.format(maximum)})
        stubs.update({'terrain': 'Greater than Terrain Pressure'})
        stubs.update({'climatology': 'Using Climatology, non Fill Values'})
        stubs.update({'fill': 'Using Climatology, Fill Values'})

        # begin data
        latitudes = {name: [] for name in stubs.keys()}
        longitudes = {name: [] for name in stubs.keys()}

        # begin totals
        totals = {name: 0 for name in stubs.keys()}
        totals.update({'total': 0})

        # begin omcldrr processing quality collection
        collection = {'latitude': [], 'longitude': [], 'quality': []}

        # for each pair
        for path, pathii in zip(hydra.paths, hydraii.paths):

            # get the orbit number
            orbit = self._stage(path)['orbit']

            # gather archive set data
            hydra.ingest(path)
            cloud = hydra.grab('CloudPressure')
            quality = hydra.grab('QualityFlags')
            terrain = hydra.grab('TerrainPressure')
            latitude = hydra.grab('Latitude')
            longitude = hydra.grab('Longitude')

            # gather archiveii set data
            hydraii.ingest(pathii)
            cloudii = hydraii.grab('CloudPressureforO3')
            qualityii = hydraii.grab('ProcessingQualityFlagsforO3')
            latitudeii = hydraii.grab('Latitude')
            longitudeii = hydraii.grab('Longitude')

            # add omcldrr data to collection
            collection['latitude'].append(latitudeii)
            collection['longitude'].append(longitudeii)
            collection['quality'].append(qualityii)

            # add orbit to report
            report.append(self._print('\norbit: {}'.format(orbit)))

            # begin masks
            masks = {}

            # create mask of cloud pressure differences
            mask = (cloudii != cloud)
            total = mask.shape[0] * mask.shape[1]
            formats = (mask.sum(), self._round(100 * mask.sum() / total, 2))
            report.append(self._print('{} differences ( {} % )'.format(*formats)))

            # increment total
            totals['total'] += total

            # find amount due to climatology, non fill
            masks['climatology'] = ((quality[mask] & 128) == 128) & (cloudii[mask] >= 0)
            mean = cloudii[mask][masks['climatology']].mean()
            formats = (masks['climatology'].sum(), self._round(100 * masks['climatology'].sum() / total, 2))
            report.append(self._print('{} using climatology, nonfill ( {} % )'.format(*formats)))
            report.append(self._print('mean: {}'.format(mean)))
            totals['climatology'] += masks['climatology'].sum()

            # find amount due to climatology, fills
            masks['fill'] = ((quality[mask] & 128) == 128) & (cloudii[mask] < 0)
            mean = cloudii[mask][masks['fill']].mean()
            formats = (masks['fill'].sum(), self._round(100 * masks['fill'].sum() / total, 2))
            report.append(self._print('{} using climatology, fill ( {} % )'.format(*formats)))
            report.append(self._print('mean: {}'.format(mean)))
            totals['fill'] += masks['fill'].sum()

            # find amount due to lower than minimum
            masks['lower'] = (cloudii[mask] < minimum) & ((quality[mask] & 128) == 0)
            mean = cloudii[mask][masks['lower']].mean()
            formats = (masks['lower'].sum(), minimum, self._round(100 * masks['lower'].sum() / total, 2))
            report.append(self._print('{} lower than {} hPa minimum ( {} % )'.format(*formats)))
            report.append(self._print('mean: {}'.format(mean)))
            totals['lower'] += masks['lower'].sum()

            # find amount due to greater than minimum
            masks['greater'] = (cloudii[mask] > maximum) & ((quality[mask] & 128) == 0)
            mean = cloudii[mask][masks['greater']].mean()
            formats = (masks['greater'].sum(), maximum, self._round(100 * masks['greater'].sum() / total, 2))
            report.append(self._print('{} greater than {} hPa minimum ( {} % )'.format(*formats)))
            report.append(self._print('mean: {}'.format(mean)))
            totals['greater'] += masks['greater'].sum()

            # find amount due to below than terrain pressure
            masks['terrain'] = (cloudii[mask] > terrain[mask]) & ((quality[mask] & 128) == 0) & (cloudii[mask] < maximum)
            mean = cloudii[mask][masks['terrain']].mean()
            formats = (masks['terrain'].sum(), self._round(100 * masks['terrain'].sum() / total, 2))
            report.append(self._print('{} greater than terrain pressure ( {} % )'.format(*formats)))
            report.append(self._print('mean: {}'.format(mean)))
            totals['terrain'] += masks['terrain'].sum()

            # add to latitudes and longitudes
            [latitudes[name].append(latitude[mask][masks[name]].reshape(-1, 1)) for name in stubs.keys()]
            [longitudes[name].append(longitude[mask][masks[name]].reshape(-1, 1)) for name in stubs.keys()]

        # create arrays
        latitudes = {name: numpy.vstack(arrays) for name, arrays in latitudes.items()}
        longitudes = {name: numpy.vstack(arrays) for name, arrays in longitudes.items()}

        # for each name
        for name, stub in stubs.items():

            # make plot
            percent = self._round(100 * totals[name] / totals['total'], 2)
            title = 'OMCLDRR cloud pressure {}, ( {} % ), {}m{}{}'.format(stub, percent, *date)
            address = '{}/{}_{}m{}{}.h5'.format(folder, name, *date)
            self.squid.ink('latitude', latitudes[name], 'longitude', longitudes[name], address, title)

        # create arrays from omcldrr collection
        collection = {name: numpy.vstack(arrays) for name, arrays in collection.items()}

        # for each processing flag
        for flag in range(16):

            # create mask, and coordinates
            mask = ((collection['quality'] & 2 ** flag) == 2 ** flag)
            ordinate = collection['latitude'][mask]
            abscissa = collection['longitude'][mask]

            # make plot
            title = 'OMCLDRR processing quality flag: {}, {}m{}{}'.format(flag, *date)
            address = '{}/OMCLDRR_Flag_{}_{}m{}{}.h5'.format(folderii, self._pad(flag), *date)
            self.squid.ink('latitude', ordinate, 'longitude', abscissa, address, title)

        return None

    def aerate(self, year=2005, month=3, day=21, number=0):
        """Determine aerosol nans in omto3 data.

        Arguments:
            year: int, year
            month: int, month
            day: int, day

        Returns:
            None
        """

        # make hydra and ingest top orbit
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/95042/OMTO3/{}/{}/{}'.format(*formats))
        hydra.ingest(number)

        # set up searches
        searches = {'reflectivity': 'Reflectivity331', 'ozone': 'ColumnAmountO3'}
        searches.update({'quality': 'QualityFlags', 'algorithm': 'AlgorithmFlags'})
        searches.update({'sulfur': 'SO2index', 'aerosol':'UVAerosolIndex'})
        searches.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
        searches.update({'zenith': 'SolarZenithAngle', 'zenithii': 'ViewingZenithAngle'})
        searches.update({'residual': 'ResidualStep1', 'residualii': 'ResidualStep2'})
        searches.update({'residue': 'Residual'})

        # get data
        data = {name: hydra.grab(search) for name, search in searches.items()}

        # construct reflectivity mask for all cases > 0, < 100
        mask = (data['reflectivity'] > 0) & (data['reflectivity'] < 100)

        # construct algorithm flag mask for all samples not skipped ( those with snow / ice are allowed )
        maskii = (data['algorithm'] % 10 > 0)

        # set quality mask for ascending and good retrievals (QualityFlags & 15) == 0 | 1
        maskiii = (data['quality'] & 15 < 2)

        # set quality mask for no anomaly ( not bit 6 )
        maskiv = (data['quality'] & 64 == 0)

        # combine all masks
        masque = mask & maskii & maskiii & maskiv

        # apply masque
        data = {name: array[masque] for name, array in data.items()}

        # begin report
        report = ['{} and {} fill values for'.format(searches['sulfur'], searches['aerosol'])]
        report.append('{}'.format(hydra.current))
        report.append('')
        report.append('{} fill values:'.format(searches['sulfur']))

        # get occurence of sulfar fiils
        fill = data['sulfur'].min()
        report.append('{} occurrences out of {}'.format((data['sulfur'] == fill).sum(), data['sulfur'].shape[0]))

        # get pixels
        pixels = self._pin(fill, data['sulfur'], number=10)

        # for each pixel
        for pixel in pixels:

            # print status
            report.append(self._print(''))
            [report.append(self._print('{}: {}'.format(searches[name], array[pixel]))) for name, array in data.items()]

            # calculate pathlength
            length = 0
            length += (1 / numpy.cos(data['zenith'][pixel] * numpy.pi / 180))
            length += (1 / numpy.cos(data['zenithii'][pixel] * numpy.pi / 180))
            report.append(self._print('path_length: {}'.format(length)))

        # report for Aerosol
        report.append('')
        report.append('{} fill values:'.format(searches['aerosol']))

        # get occurence of sulfar fiils
        fill = data['aerosol'].min()
        report.append('{} occurrences out of {}'.format((data['aerosol'] == fill).sum(), data['aerosol'].shape[0]))

        # get pixels
        pixels = self._pin(fill, data['aerosol'], number=10)

        # for each pixel
        for pixel in pixels:

            # print status
            report.append(self._print(''))
            [report.append(self._print('{}: {}'.format(searches[name], array[pixel]))) for name, array in data.items()]

            # calculate pathlength
            length = 0
            length += (1 / numpy.cos(data['zenith'][pixel] * numpy.pi / 180))
            length += (1 / numpy.cos(data['zenithii'][pixel] * numpy.pi / 180))
            report.append(self._print('path_length: {}'.format(length)))

        # jot down report
        self._make('{}/reports'.format(self.sink))
        destination = '{}/reports/Aerosol_Sulfur_Fill_Values.txt'.format(self.sink)
        self._jot(report, destination)

        return None

    def algorize(self, year=2006, months=(1, 13), erase=False):
        """Check for algorithm flag values greeater than 19, a coding bug.

        Arguments:
            year: int, the year
            months: tuple of ints, the months to check
            erase: boolean, erase previous report?

        Returns:
            None
        """

        # make reports folder
        self._make('{}/reports'.format(self.sink))

        # set report destination
        destination = '{}/reports/algorithm_flag_report.txt'.format(self.sink)

        # load report
        report = self._know(destination)

        # if erase
        if erase:

            # erase report
            report = []

        # if no report yet
        if len(report) < 1:

            # begin report
            report = ['Algorithm Flag exeedences, year {}'.format(year)]

        # for each month
        for month in range(*months):

            # print month
            self._print('month {}...'.format(month))

            # create the hydra for the month
            hydra = Hydra('/tis/acps/OMI/95044/OMTO3/{}/{}'.format(year, self._pad(month)), 1, 31, show=False)

            # for each path
            for path in hydra.paths:

                # ingest
                hydra.ingest(path)

                # get info
                date = hydra._stage(path)['day']
                orbit = hydra._stage(path)['orbit']

                # grab the data
                algorithm = hydra.grab('AlgorithmFlag')
                ozone = hydra.grab('ColumnAmountO3')
                quality = hydra.grab('QualityFlags')

                # get all pixels with algflg > 19
                pixels = hydra._mask(algorithm > 19)

                # if 1 pixel or more
                if len(pixels) > 0:

                    # add orbit and date
                    report.append(self._print('algorithm > 19:'))
                    report.append(self._print('orbit {}, {}: '.format(orbit, date)))

                    # get rows and ozone values
                    rows = Counter([pixel[1] for pixel in pixels])
                    ozones = Counter([ozone[pixel] for pixel in pixels])

                    # make string from row entries
                    string = ', '.join(['{}: {}'.format(quantity, count) for quantity, count in rows.items()])
                    report.append(self._print(string))

                    # make string from row entries
                    string = ', '.join(['{}: {}'.format(quantity, count) for quantity, count in ozones.items()])
                    report.append(self._print(string))
                    report.append(self._print(' '))

                    # jot report
                    self._jot(report, destination)

                # get all pixels with algflg = 1, but ozone is a fill value
                pixels = hydra._mask((algorithm == 1) & (abs(ozone) > 1e20) & (quality < 2))

                # if 1 pixel or more
                if len(pixels) > 0:

                    # add orbit and date
                    report.append(self._print('algorithm = 1, ozone fill:'))
                    report.append(self._print('orbit {}, {}: '.format(orbit, date)))

                    # get rows and ozone values
                    rows = Counter([pixel[1] for pixel in pixels])
                    ozones = Counter([ozone[pixel] for pixel in pixels])

                    # make string from row entries
                    string = ', '.join(['{}: {}'.format(quantity, count) for quantity, count in rows.items()])
                    report.append(self._print(string))

                    # make string from row entries
                    string = ', '.join(['{}: {}'.format(quantity, count) for quantity, count in ozones.items()])
                    report.append(self._print(string))
                    report.append(self._print(' '))

                    # jot report
                    self._jot(report, destination)

        return None

    def analyze(self):
        """Compare the new reader version with the old.

        Arguments:
            None

        Returns:
            None
        """

        # get first hydra from tis
        hydra = Hydra('/tis/acps/OMI/95042/OMTO3/2005/05/16'); hydra.ingest(0)

        # get second hydra from run
        # hydraii = Hydra('../apps/OMTO3v85/OMTO3/run'); hydraii.ingest(15)
        hydraii = Hydra('../studies/omto3v8/reader'); hydraii.ingest(0)

        # collect data
        latitude = hydra.grab('Latitude')
        wavelength = hydra.grab('Wavelength')
        value = hydra.grab('NValue')
        valueii = hydraii.grab('NValue')
        ozone = hydra.grab('ColumnAmountO3')
        ozoneii = hydraii.grab('ColumnAmountO3')
        one = hydra.grab('StepOneO3')
        oneii = hydraii.grab('StepOneO3')
        two = hydra.grab('StepTwoO3')
        twoii = hydraii.grab('StepTwoO3')
        residue = hydra.grab('Residual')
        residueii = hydraii.grab('Residual')
        first = hydra.grab('ResidualStep1')
        firstii = hydra.grab('ResidualStep1')
        second = hydra.grab('ResidualStep2')
        secondii = hydra.grab('ResidualStep2')

        # calculate difference
        absolute = abs(self._relate(ozone, ozoneii))

        # create mask for all absolute differences greater than 0.2
        mask = (absolute > 0.2)

        # for each wavelength
        for index, wave in enumerate(wavelength):

            # round wvavelenvth
            formats = (self._round(wave, 0),)

            # plot nvalue difference versus latitude
            difference = self._relate(value[:, :, index], valueii[:, :, index])
            title = 'N Value differences, OMI Reader - AS95042, 2005-05-16, {} nm'.format(*formats)
            address = 'plots/difference/NValue_diffs_{}.h5'.format(*formats)
            self.squid.ink('n_value_difference', difference[mask], 'latitude', latitude[mask], address, title)

            # plot residual difference versus latitude
            difference = self._relate(residue[:, :, index], residueii[:, :, index])
            title = 'Residual differences, OMI Reader - AS95042, 2005-05-16, {} nm'.format(*formats)
            address = 'plots/difference/Residual_diffs_{}.h5'.format(*formats)
            self.squid.ink('residual_difference', difference[mask], 'latitude', latitude[mask], address, title)

            # plot difference versus latitude
            difference = self._relate(first[:, :, index], firstii[:, :, index])
            title = 'Step One Residual differences, OMI Reader - AS95042, 2005-05-16, {} nm'.format(*formats)
            address = 'plots/difference/Residual_one_diffs_{}.h5'.format(*formats)
            self.squid.ink('residual_step_one_difference', difference[mask], 'latitude', latitude[mask], address, title)

            # plot difference versus latitude
            difference = self._relate(second[:, :, index], secondii[:, :, index])
            title = 'Step Two Residual differences, OMI Reader - AS95042, 2005-05-16, {} nm'.format(*formats)
            address = 'plots/difference/Residual_two_diffs_{}.h5'.format(*formats)
            self.squid.ink('residual_step_two_difference', difference[mask], 'latitude', latitude[mask], address, title)

        # plot ozone difference
        difference = self._relate(ozone, ozoneii)
        title = 'Total Column Ozone differences, OMI Reader - AS95042, 2005-05-16'
        address = 'plots/difference/Ozone_diffs.h5'
        self.squid.ink('ozone_difference', difference[:, :][mask], 'latitude', latitude[mask], address, title)

        # plot step1 difference
        difference = self._relate(one, oneii)
        title = 'Step One O3 differences, OMI Reader - AS95042, 2005-05-16'
        address = 'plots/difference/Step_one_diffs.h5'
        self.squid.ink('step_one_ozone_difference', difference[:, :][mask], 'latitude', latitude[mask], address, title)

        # plot step2 difference
        difference = self._relate(two, twoii)
        title = 'Step Two O3 differences, OMI Reader - AS95042, 2005-05-16'
        address = 'plots/difference/Step_two_diffs.h5'
        self.squid.ink('step_two_ozone_difference', difference[:, :][mask], 'latitude', latitude[mask], address, title)

        return None

    def animate(self, year=2021, months=(6, 13), rows=(50, 51, 52, 53, 54)):
        """Scan through OMTO3 records to look for misflagged row anomaly.

        Arguments:
            year: int, the yeaer
            months: tuple of ints, the months
            rows: tuple of ints, the rows

        Returns:
            None
        """

        # begin report
        report = ['row anomlay flags, rows {}'.format(rows)]

        # for each month
        for month in range(*months):

            # print status
            self._print('year {}, month {}...'.format(year, month))

            # create hydra
            formats = (year, self._pad(month))
            hydra = Hydra('/tis/acps/OMI/95044/OMTO3/{}/{}'.format(*formats), 1, 31, show=False)

            # for each path
            for path in hydra.paths:

                # get quality data
                hydra.ingest(path)
                quality = hydra.grab('QualityFlags')

                # get anomaly flag
                anomaly = ((quality & 2 ** 6) == 2 ** 6).astype(int)

                # multiple through rows
                product = numpy.prod(anomaly, axis=0)

                # for each row
                for row in rows:

                    # if row is not flagged
                    flag = product[row]
                    if flag < 1:

                        # add alert
                        report.append(self._print('{}: row {}: flag: {}'.format(path, row, flag)))

        # jot report
        self._jot(report, '{}/reports/anomaly_flags.txt'.format(self.sink))

        return None

    def animize(self, year=2023, month=10, day=10, archive=10003, orbits=16, plots=True):
        """Create a row anomaly plot from omto3 file and OMUANC.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            archive: int, archive set
            orbits: int, number of orbits
            plots: boolean, make plots?

        Returns:
            None
        """

        # grab omto3 files from
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))
        paths = [path for path in hydra.paths if path.endswith('.he5')]

        # make secondary hydra for ancillary
        archiveii = 10004
        hydraii = Hydra('/tis/acps/OMI/{}/OMUANC/{}/{}/{}'.format(archiveii, *formats[1:]))

        # for each path
        for path in paths[:orbits]:

            # ingest path
            self._print('ingesting {}...'.format(path))
            hydra.ingest(path)

            # get orbit nhmber
            orbit = self._stage(path)['orbit']
            date = self._stage(path)['date']

            # grab quality
            quality = hydra.grab('QualityFlags')
            latitude = hydra.grab('Latitude')
            aerosol = hydra.grab('UVAerosolIndex')
            reflectivity = hydra.grab('Reflectivity331')
            reflectivityii = hydra.grab('Reflectivity360')
            difference = reflectivity - reflectivityii
            difference = difference / 100

            # make row matrix
            row = numpy.array([numpy.linspace(0, 59, 60).astype(int)] * latitude.shape[0])

            # get bit 6 from quality
            anomaly = (quality & 64) == 64
            anomaly = anomaly.astype(float)

            # get row anomaly from anc
            hydraii.ingest('o{}'.format(orbit))
            anomalyii = hydraii.grab('row_anomaly')[0]

            # check for equality
            self._print('orbit: {}'.format(orbit))
            self._print((anomaly == anomalyii).sum(), numpy.prod(anomaly.shape))

            # if plottitng
            if plots:

                # specify color gradient and particular indices
                gradient = 'gist_rainbow'
                gradient = 'jet'
                selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]
                selection = [index for index in range(0, 256)]

                # create destioation
                destination = '{}/plots/quality/Row_Flag_OMTO3_{}_o{}.png'.format(self.sink, date, orbit)
                title = 'Row anomaly, OMTO3 AS {}, {}, o{}'.format(archive, date, orbit)
                arrays = [anomaly, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destination, texts, (0, 1), [gradient, selection], (-90, 90, 0, 60), bar=True)

                print(anomaly.shape, anomalyii.shape)

                # create destioation
                destinationii = '{}/plots/quality/Row_Flag_OMUANC_{}_o{}.png'.format(self.sink, date, orbit)
                title = 'Row anomaly flag, OMUANC AS {}, {}, o{}'.format(archiveii, date, orbit)
                arrays = [anomalyii, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destinationii, texts, (0, 1), [gradient, selection], (-90, 90, 0, 60), bar=True)

                # create destioation
                destinationii = destination.replace('Row_Flag', 'Flag_Diff')
                title = 'Row anomaly flag OMUAC - OMTO3, {}, o{}'.format(date, orbit)
                arrays = [anomalyii - anomaly, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destinationii, texts, (-1, 1), [gradient, selection], (-90, 90, 0, 60), bar=True)

                # plot reflectivity heeatmap
                gradient = 'jet_r'
                destinationii = destination.replace('Row_Flag', 'Difference')
                title = 'R331 - R360, OMTO3 AS {}, {}, o{}'.format(archive, date, orbit)
                scale = (-0.1, 0)
                difference = numpy.where(difference <= scale[0], scale[0] * 0.99, difference)
                difference = numpy.where(difference >= scale[1], scale[1], difference)
                arrays = [difference, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destinationii, texts, scale, [gradient, selection], (-90, 90, 0, 60), bar=True)

                # plot reflectivity heeatmap
                gradient = 'jet'
                destinationii = destination.replace('Row_Flag', 'Aerosol')
                title = 'Aerosol index, OMTO3 AS {}, {}, o{}'.format(archive, date, orbit)
                scale = (1.0, 4.5)
                aerosol = numpy.where(aerosol <= scale[0], scale[0], aerosol)
                aerosol = numpy.where(aerosol >= scale[1], scale[1], aerosol)
                arrays = [aerosol, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destinationii, texts, scale, [gradient, selection], (-90, 90, 0, 60), bar=True)

                # plot reflectivity heeatmap
                gradient = 'jet'
                destinationii = destination.replace('Row_Flag', 'Unflagged')
                title = 'Unflagged Aerosol, OMTO3 AS {}, {}, o{}'.format(archive, date, orbit)
                scale = (1.0, 4.5)
                aerosol = numpy.where(aerosol <= scale[0], scale[0], aerosol)
                aerosol = numpy.where(aerosol >= scale[1], scale[1], aerosol)
                unflagged = (-(anomaly - 1) * aerosol)
                arrays = [unflagged, latitude, row]
                texts = [title, '_']
                self._paint(arrays, destinationii, texts, scale, [gradient, selection], (-90, 90, 0, 60), bar=True)

        return None

    def anomalize(self, year=2024, month=9, day=29, parallel=60, meridian=-140):
        """Check aerosol data and row anomaly data in OMI aerosol index.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            parallel: float, the latitude to check
            meridian: longitude to check

        Returns:
            None
        """

        # get hydra for collect 3 and 4
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(*formats))
        hydraii = Hydra('/tis/acps/OMI/10004/OMTO3/{}/{}/{}'.format(*formats))

        # if not present
        if len(hydraii.paths) < 1:

            # get hydra for collection 4 from local directory
            hydraii = Hydra('{}/anomaly'.format(self.sink))

        # collect distances to point
        distances = []

        # for each path
        for path in hydra.paths:

            # ingest
            self._print('ingeesting {}...'.format(path))
            hydra.ingest(path)

            # get orbit, latitude, longitude
            orbit = self._stage(path)['orbit']
            latitude = hydra.grab('Latitude')
            longitude = hydra.grab('Longitude')

            # find closest point
            pixel = self._pin([parallel, meridian], [latitude, longitude])[0]

            # calucate distance
            distance = (latitude[pixel] - parallel) ** 2 + (longitude[pixel] - meridian) ** 2
            distances.append((orbit, pixel, distance))

        # sort distances
        distances.sort(key=lambda triplet: triplet[2])

        # get closest orbit and scanline
        orbit, pixel, _ = distances[0]
        track = pixel[0]

        # ingest
        hydra.ingest(orbit)
        hydraii.ingest(orbit)

        # get uvai index and quality
        aerosol = hydra.grab('UVAerosolIndex')[track]
        quality = hydra.grab('QualityFlags')[track]
        anomaly = ((quality & 2 ** 6) == 2 ** 6).astype(int)
        longitude = hydra.grab('Longitude')[track]
        latitude = hydra.grab('Latitude')[track]

        # get col4 attributes
        aerosolii = hydraii.grab('UVAerosolIndex')[track]
        qualityii = hydraii.grab('QualityFlags')[track]
        anomalyii = ((qualityii & 2 ** 6) == 2 ** 6).astype(int)

        # mask aerosols
        mask = (numpy.isfinite(aerosol)) & (abs(aerosol) < 1e20)
        aerosol = numpy.where(mask, aerosol, 100)
        mask = (numpy.isfinite(aerosolii)) & (abs(aerosolii) < 1e20)
        aerosolii = numpy.where(mask, aerosolii, 100)

        # add rows
        rows = numpy.array(range(60))

        # print data
        zipper = zip(rows, aerosol, aerosolii, anomaly, anomalyii, longitude, latitude)
        for row, entry, entryii, flag, flagii, degree, degreeii in zipper:

            # print row by row
            self._print(row, flag, flagii, entry, entryii, degree, degreeii)

        # create figure with primary axis
        pyplot.clf()
        figure, axis = pyplot.subplots(figsize=(8, 6))

        # create title
        date = '{}m{}{}'.format(*formats)
        formatsii = (date, orbit, track, self._orient(parallel), self._orient(meridian, east=True, clip=True))
        title = 'UVAI, Row Anomaly Flag, Col3, Col4 \nOMTO3 {}, o{}, scanline {} (~{}, {})'.format(*formatsii)
        pyplot.title(title)

        # plot aerosol index
        axis.plot(rows, aerosol, 'ro-', label='col3 uvai')
        axis.plot(rows, aerosolii, 'go-', label='col4 uvai')
        axis.set_xlabel('row')
        axis.set_ylabel('uvai')
        axis.set_ylim(-1, 10)

        # create secondary axis
        axisii = axis.twinx()

        # plot row anomaly on secondary
        axisii.plot(rows, anomaly, 'co-', label='col3 flag')
        axisii.plot(rows, anomalyii, 'bo-', label='col4 flag')
        axisii.set_ylabel('flag')
        axisii.set_ylim(-0.5, 1.5)
        axisii.set_yticks([0, 1])

        # create legend
        axis.legend(loc='best')
        axisii.legend(loc='best')

        # save the plot
        destination = '{}/plots/anomaly/col3_col4_row_anomaly_{}_{}.png'.format(self.sink, date, orbit)
        pyplot.savefig(destination)
        pyplot.clf()

        # print
        self._print('plotted {}'.format(destination))

        return None

    def banana(self, year=2024, month=8, day=18):
        """Plot nan ozone values.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day

        Returns:
            None
        """

        # make folder
        folder = '{}/plots/banana'.format(self.sink)
        self._make(folder)

        # get hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/95044/OMTO3/{}/{}/{}'.format(*formats))

        # begin collection
        ozone = []
        latitude = []
        longitude = []

        # for each path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # collect data
            ozone.append(hydra.grab('ColumnAmountO3'))
            latitude.append(hydra.grab('Latitude'))
            longitude.append(hydra.grab('Longitude'))

        # create arrays
        ozone = numpy.vstack(ozone)
        latitude = numpy.vstack(latitude)
        longitude = numpy.vstack(longitude)

        # create nan mask
        mask = ~numpy.isfinite(ozone)

        # create data
        intensity = numpy.where(mask, 1, -1)

        # specify color gradient and particular indices
        gradient = 'rainbow_r'
        selection = [index for index in range(0, 256)]

        # plot orbits
        destination = '{}/Ozone_nans_{}m{}{}.png'.format(folder, *formats)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'Column Amount O3 NaNs, {}m{}{}'.format(*formats)
        arrays = [intensity, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 2)}
        parameters.update({'spectrum': [gradient, selection], 'limits': (-90, 90, -180, 180), 'grid': 1})
        parameters.update({'log': False, 'bar': False, 'back': 'white', 'points': True})
        self._paint(**parameters)
        self._stamp('plotted.')

        return None

    def cloud(self, year=2006, month=11, day=28, archive=95042, archiveii=10004):
        """Examine OMDCLDRR difference between two archive sets.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            archive: int, firt archive set
            archiveii: int, second archive set

        Returns:
            None
        """

        # make plots folder
        folder = 'plots/cloud'
        self._make('{}/{}'.format(self.sink, folder))

        # set fields
        fields = {'cloud': 'CloudPressureforO3_uncorrected', 'correction': 'CloudPressureforO3'}
        fields.update({'latitude': 'Latitude', 'longitude': 'Longitude', 'process': 'ProcessingQualityFlagsforO3'})
        fields.update({'reflectivity': 'Reflectivity', 'bias': 'Residual_bias', 'shift': 'WavelengthShift'})

        # set stubs for titles
        stubs = {field: name for field, name in fields.items()}
        stubs.update({'row': 'Row', 'scan': 'Scanline'})
        stubs.update({'process': 'Largest Missing Flag'})

        # create hydras
        date = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMCLDRR/{}/{}/{}'.format(archive, *date))
        hydraii = Hydra('/tis/acps/OMI/{}/OMCLDRR/{}/{}/{}'.format(archiveii, *date))

        # for each pair
        for path, pathii in zip(hydra.paths, hydraii.paths):

            # get the orbit number
            orbit = self._stage(path)['orbit']

            # gather archive set data
            hydra.ingest(path)
            data = {field: hydra.grab(search) for field, search in fields.items()}

            # gather archiveii set data
            hydraii.ingest(pathii)
            dataii = {field: hydraii.grab(search) for field, search in fields.items()}

            # get masks for differences
            modes = ['cloud', 'correction', 'reflectivity', 'shift', 'bias']
            masks = {mode: (data[mode] != dataii[mode]) for mode in modes}

            # for each mask
            for mode, mask in masks.items():

                # if non empty
                if mask.sum() > 0:

                    # get difference as ordinate
                    ordinate = dataii[mode][mask] - data[mode][mask]

                    # begin abscissas
                    abscissas = {}

                    # create latitude and longitude abscissas
                    abscissas['latitude'] = data['latitude'][mask]
                    abscissas['longitude'] = data['longitude'][mask]

                    # create row abscissa
                    shape = data[mode].shape
                    rows = numpy.array([range(shape[1])] * shape[0])
                    abscissas['row'] = rows[mask]

                    # create scan abscissa
                    scans = numpy.array([range(shape[0])] * shape[1]).transpose(1, 0)
                    abscissas['scan'] = scans[mask]

                    # get processing quality flags
                    process = data['process'][mask]
                    processii = dataii['process'][mask]

                    # pair up the bit flags
                    pairs = [(self._bite(one), self._bite(two)) for one, two in zip(process, processii)]

                    # create maximizing function to get the maximum of a list or 0 if empty
                    def maximizing(flags): return 0 if len(flags) < 1 else max(flags)

                    # get maximum bit flags missing from first path and second path
                    first = [maximizing([flag for flag in pair[0] if flag not in pair[1]]) for pair in pairs]
                    second = [maximizing([flag for flag in pair[1] if flag not in pair[0]]) for pair in pairs]

                    # compute difference
                    abscissas['process'] = numpy.array(second) - numpy.array(first)

                    # for each abscissa
                    for name, abscissa in abscissas.items():

                        # duplicate ordinate and abscissa
                        ordinateii = ordinate[:]
                        abscissaii = abscissa[:]

                        # if length of ordinate is only 1
                        if mask.sum() == 1:

                            # double up to avoid plotting error
                            ordinateii = [ordinate[:], ordinate[:]]
                            abscissaii = [abscissa[:], abscissa[:]]

                        # plot cloud pressure difference
                        formats = (stubs[mode], stubs[name], orbit, *date, archiveii, archive)
                        title = '{} difference vs {}, o{}, {}m{}{}, OMCLDRR AS{} - AS{}'.format(*formats)
                        address = '{}/{}_{}_{}_{}m{}{}.h5'.format(folder, mode, name, orbit, *date)
                        self.squid.ink(mode, ordinateii, name, abscissaii, address, title)

        return None

    def collapse(self, events=None, year=2005, archives=(10003, 95044), product='OMTO3G', tag='collapse'):
        """Produce a zonal means of a omto3g comparison.

        Arguments:
            events: list of str, the dates in YYYYmMMDD format
            year: int, the year, if events not given
            archives: tuple of ints, the archive sets
            product: str, name of specific product
            tag: str, name of plot folder

        Returns:
            None
        """

        # make directory
        folder = '{}/{}'.format(self.sink, tag)
        self._make(folder)

        # if events not given
        if events is None:

            # create datetime instances
            instances = []

            # create initial datetime
            start = datetime.datetime(year, 1, 1)

            # for each days
            for days in range(366):

                # create new instance
                instance = start + datetime.timedelta(days=days)
                instances.append((year, self._pad(instance.month), self._pad(instance.day)))

            # create events
            events = ['{}m{}{}'.format(*instance) for instance in instances]

        # sort events
        events.sort()

        # collect zones and julian days
        julians = []
        differences = []
        parallels = []
        zones = []
        ordinate = None

        # for each instance
        for event in events:

            # parse event to override random selections
            year = int(event[:4])
            month = int(event[5:7])
            day = int(event[7:9])

            # get julian day
            julian = datetime.datetime(year, month, day).timetuple().tm_yday
            julians.append(julian)

            # begin ozones
            ozones = []

            # construct date
            date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))

            # for each archive set
            for archive in archives:

                # create hydra for first archive set
                formats = (archive, product, year, self._pad(month))
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}'.format(*formats))
                hydra.ingest('_{}_'.format(date))

                # if product is OMTO3G
                if product == 'OMTO3G':

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    quality = hydra.grab('QualityFlags')

                    # construct ozone grid from all positive ozone values
                    mask = (ozone >= 0) & (numpy.isfinite(ozone)) & (quality < 1)
                    summation = numpy.where(mask, ozone, 0).sum(axis=0)
                    samples = mask.astype(int).sum(axis=0)
                    grid = summation / samples

                    # reassign ozone
                    ozone = grid
                    ozones.append(ozone)

                # otherwise
                else:

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    ozones.append(ozone)

            # append differences
            difference = ozones[1] - ozones[0]

            # construct latitude and longitude
            shape = difference.shape
            latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
            longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]

            # craete grid
            grid, gridii = numpy.meshgrid(longitudes, latitudes)

            # construct zonal means from difference
            fill = 1e20
            mask = (numpy.isfinite(difference)) & (abs(difference) < fill)
            tracer = numpy.where(mask, difference, 0)
            number = numpy.ones(tracer.shape)
            number = numpy.where(mask, number, 0)
            zone = tracer.sum(axis=1) / number.sum(axis=1)
            zones.append(zone)
            # tracer = numpy.where((tracer > scale[1]) & (abs(tracer) < fill), scale[1], tracer)
            # tracer = numpy.where((tracer < scale[0]) & (abs(tracer) < fill), scale[0], tracer)

            # set ordinate
            # abscissa = grid
            ordinate = gridii[:, 0]

        # set scale
        scale = (-5, 5)
        zones = numpy.array(zones).transpose(1, 0)
        zones = numpy.flip(zones, axis=0)
        print('zones: {}'.format(zones.shape))

        # clip to scale
        zones = numpy.where(zones < scale[0], scale[0], zones)
        zones = numpy.where(zones > scale[1], scale[1], zones)

        # set up color scheme, removing first two
        text = self._know('{}/colorbars/dobson_difference_colorbar.txt'.format(self.sink))
        tuples = [tuple([float(channel) for channel in line.split()]) for line in text]

        # set up color map
        colors = matplotlib.colors.ListedColormap(tuples)

        # create the heatmap
        pyplot.clf()
        # pyplot.imshow(zones, cmap='seismic', interpolation='nearest', vmin=-5, vmax=5)
        pyplot.imshow(zones, cmap=colors, interpolation='nearest', vmin=-5, vmax=5)
        pyplot.colorbar()  # Show color scale

        # Customize ticks
        ordinate = numpy.flip(ordinate, axis=0)
        labels = [-90, -60, -30, 0, 30, 60, 90]
        indices = [self._pin(label, ordinate, 1)[0][0] for label in labels]

        # set latitude ticks
        pyplot.yticks(ticks=indices, labels=labels)
        # pyplot.xticks(rotation=45)  # Rotate x-axis labels by 45 degrees if needed
        # pyplot.yticks(rotation=0)  # Keep y-axis labels horizontal

        # Add labels and title
        pyplot.xlabel('Julian Day')
        pyplot.ylabel('Latitude')
        pyplot.title('{} AS95044 - AS10003 Zonal Means, 2005'.format(product))

        # set destination
        destination = '{}/{}_zonal_diffs_2005.png'.format(folder, product)

        # save plot
        pyplot.savefig(destination)
        pyplot.clf()

        return None

    def compare(self, year=2005, number=5, archives=(10003, 95044), product='OMTO3G', event=None, tag='comparison'):
        """Sketch a random set of gridded ozone files.

        Arguments:
            year: int, the year
            number: number of instances
            archives: tuple of ints, the archive sets
            product: str, name of specific product
            event: str, specific date in YYYYmMMDD format

        Returns:
            None
        """

        # make directory
        folder = '{}/{}'.format(self.sink, tag)
        self._make(folder)

        # create random days
        days = numpy.random.randint(1, 29, size=number)
        months = numpy.random.randint(1, 12, size=number)

        # if event given
        if event:

            # parse event to override random selections
            year = int(event[:4])
            months = [int(event[5:7])]
            days = [int(event[7:9])]

        # for each instance
        for month, day in zip(months, days):

            # begin ozones
            ozones = []

            # construct date
            date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))

            # for each archive set
            for archive in archives:

                # create hydra for first archive set
                formats = (archive, product, year, self._pad(month))
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}'.format(*formats))
                hydra.ingest('_{}_'.format(date))

                # if gridded
                if product == 'OMTO3G':

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    quality = hydra.grab('QualityFlags')

                    # construct ozone grid from all positive ozone values
                    mask = (ozone >= 0) & (numpy.isfinite(ozone)) & (quality < 1)
                    summation = numpy.where(mask, ozone, 0).sum(axis=0)
                    samples = mask.astype(int).sum(axis=0)
                    grid = summation / samples

                    # reassign ozone
                    ozone = grid
                    ozones.append(ozone)

                # otherwise
                else:

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    ozones.append(ozone)

            # append differences
            difference = ozones[1] - ozones[0]

            # set up plot arrays and attributes
            arrays = ozones + [difference]
            scales = [(100, 500), (100, 500), (-5, 5)]
            text = '../studies/osiris/eight/colorbars/ozone_color_bar.txt'
            gradients = [text, text, 'seismic']

            # set up stubs and modes
            modes = ['AS{}'.format(archives[0]), 'AS{}'.format(archives[1])]
            modes += ['AS{} - AS{}'.format(archives[1], archives[0])]
            stubs = [mode.lower() for mode in modes[:2]] + ['diff']

            # for each array
            for array, mode, stub, gradient, scale in zip(arrays, modes, stubs, gradients, scales):

                # construct latitude and longitude
                shape = array.shape
                latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
                longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]

                # craete grid
                grid, gridii = numpy.meshgrid(longitudes, latitudes)

                # create title destination
                title = 'Ozone, {}, {}, {}'.format(product, mode, date)
                formatsii = (folder, product.lower(), date, stub)
                destination = '{}/Ozone_{}_{}_{}.png'.format(*formatsii)

                # set parameters
                limits = (-90, 90, -180, 180)
                size = (10, 7)
                logarithm = False
                back = 'white'
                unit = 'DU'
                selection = list(range(256))
                bar = True

                # create mask and apply, clipping to scale
                fill = 1e20
                mask = (numpy.isfinite(array)) & (abs(array) < fill)
                tracer = numpy.where(mask, array, numpy.nan)
                tracer = numpy.where((tracer > scale[1]) & (abs(tracer) < fill), scale[1], tracer)
                tracer = numpy.where((tracer < scale[0]) & (abs(tracer) < fill), scale[0], tracer)
                abscissa = grid
                ordinate = gridii

                # make plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer, ordinate, abscissa], destination, [title, unit]]
                parameters += [scale, [gradient, selection], limits, 1]
                options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                self._paint(*parameters, **options)

        return None

    def contrast(self, event, archives=(10003, 95044), tag='contrast', resolution=20, rows=(49, 60), qualify=True):
        """Sketch differences betwee OMTO3 files.

        Arguments:
            event: str, date in YYYYmMMDD format
            archives: tuple of ints, the archive sets
            product: str, name of specific product
            event: str, specific date in YYYYmMMDD format
            rows: tuple of int, the bracket of rows
            qualify: boolean, filter based on quality flags?

        Returns:
            None
        """

        # set product
        product = 'OMTO3'

        # make directory
        folder = '{}/plots/{}'.format(self.sink, tag)
        self._make(folder)

        # parse event to override random selections
        year = int(event[:4])
        month = int(event[5:7])
        day = int(event[7:9])

        # begin ozones
        ozones = []
        qualities = []
        latitudes = []
        longitudes = []

        # construct date
        date = event

        # for each archive set
        for archive in archives:

            # create hydra for first archive set
            formats = (archive, product, year, self._pad(month), self._pad(day))
            hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}/{}'.format(*formats))
            # hydra.ingest('_{}_'.format(date))

            # set up accumulation
            ozone = []
            quality = []
            latitude = []
            longitude = []

            # for each path
            for path in hydra.paths:

                # ingest the path
                hydra.ingest(path)

                # append arrays
                ozone.append(hydra.grab('ColumnAmountO3'))
                quality.append(hydra.grab('QualityFlags'))
                latitude.append(hydra.grab('Latitude'))
                longitude.append(hydra.grab('Longitude'))

            # create arrays
            ozone = numpy.vstack(ozone)
            quality = numpy.vstack(quality)
            latitude = numpy.vstack(latitude)
            longitude = numpy.vstack(longitude)

            # if qualifying
            if qualify:

                # apply quality
                mask = (quality < 2)
                ozone = numpy.where(mask, ozone, numpy.nan)

            # append, subsetting by rows
            ozones.append(ozone[:, rows[0]: rows[1]])
            qualities.append(quality[:, rows[0]: rows[1]])
            latitudes.append(latitude[:, rows[0]: rows[1]])
            longitudes.append(longitude[:, rows[0]: rows[1]])

        # append differences
        difference = ozones[1] - ozones[0]

        # set up plot arrays and attributes
        scales = [(100, 500), (100, 500), (-5, 5)]
        text = '../studies/osiris/eight/colorbars/ozone_color_bar.txt'
        gradients = [text, text, 'seismic']

        # if not qualifying
        if not qualify:

            # adjust difference scale
            scales = [(100, 500), (100, 500), (-60, 60)]

        # replace with nans
        arrays = []
        for array in ozones + [difference]:

            # apply mask
            mask = (numpy.isfinite(array)) & (abs(array) < 1e30)
            array = numpy.where(mask, array, numpy.nan)
            arrays.append(array)

        # set up stubs and modes
        modes = ['AS{}'.format(archives[0]), 'AS{}'.format(archives[1])]
        modes += ['AS{} - AS{}'.format(archives[1], archives[0])]
        stubs = [mode.lower() for mode in modes[:2]] + ['diff']

        # for each array
        for array, mode, stub, gradient, scale in zip(arrays, modes, stubs, gradients, scales):

            # # construct latitude and longitude
            # shape = array.shape
            # latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
            # longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]
            #
            # # craete grid
            # grid, gridii = numpy.meshgrid(longitudes, latitudes)

            # create title destination
            title = 'Ozone, {}, {}, {}, rows {} to {}'.format(product, mode, date, rows[0], rows[1] - 1)
            formatsii = (folder, product.lower(), date, stub)
            destination = '{}/Ozone_{}_{}_{}.png'.format(*formatsii)

            # set parameters
            limits = (-90, 90, -180, 180)
            size = (10, 7)
            logarithm = False
            back = 'white'
            unit = 'DU'
            selection = list(range(256))
            bar = True

            # create mask and apply, clipping to scale
            fill = 1e20
            mask = (numpy.isfinite(array)) & (abs(array) < fill)
            tracer = numpy.where(mask, array, numpy.nan)
            tracer = numpy.where((tracer > scale[1]) & (abs(tracer) < fill), scale[1], tracer)
            tracer = numpy.where((tracer < scale[0]) & (abs(tracer) < fill), scale[0], tracer)
            abscissa = longitudes[0]
            ordinate = latitudes[0]

            # make plot
            self._stamp('plotting {}...'.format(destination), initial=True)
            parameters = [[tracer, ordinate, abscissa], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
            self._paint(*parameters, **options)

        return None

    def coordinate(self):
        """Determine coordinates of omto3G fill values missing from as 95042.

        Arguments:
            None

        Returns:
            None
        """

        # get omto3g data
        hydra = Hydra('{}/day'.format(self.sink))
        hydra.ingest(0)

        # grab latitudes, longitude, and orbit numbers
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')
        orbit = hydra.grab('OrbitNumber')
        time = hydra.grab('Time')

        # get second hydra
        hydraii = Hydra('{}/day'.format(self.sink))
        hydraii.ingest(2)

        # grab latitudes, longitude, and orbit numbers
        latitudeii = hydraii.grab('Latitude')
        longitudeii = hydraii.grab('Longitude')
        orbitii = hydraii.grab('OrbitNumber')

        # get all pixels with differeing latitudes
        pixels = hydra._mask(latitude != latitudeii)

        # get all spatial coordinates, removing duplicates
        spaces = self._skim([pixel[1:] for pixel in pixels])

        # for each space
        coordinates = []
        for parallel, meridian in spaces:

            # get cell contents
            cell = latitude[:, parallel, meridian]
            cellii = latitudeii[:, parallel, meridian]

            # get indices of any missing latitude
            indices = [index for index, latitude in enumerate(cell) if latitude not in cellii]

            # for each index
            for index in indices:

                # create coordinates
                coordinate = [orbit[index, parallel, meridian], cell[index], longitude[index, parallel, meridian]]
                coordinate += [time[index, parallel, meridian]]
                coordinates.append(tuple(coordinate))

        # sort coordinates
        coordinates.sort()

        return coordinates

    def differ(self, path, pathii, sink='dynamic'):
        """Compare ozone across two paths.

        Arguments:
            path: first ozone file path
            pathii: second ozone file path

        Returns:
            None
        """

        # create hydras
        hydra = Hydra(path)
        hydraii = Hydra(pathii)

        # ingest
        hydra.ingest()
        hydraii.ingest()

        # get ozones
        ozone = hydra.grab('ColumnAmountO3')
        ozoneii = hydraii.grab('ColumnAmountO3')

        # calculate difference
        difference = ozoneii - ozone

        # grab latitude
        latitude = hydraii.grab('Latitude')

        # grab algorithm and quality flags
        algorithm = hydra.grab('AlgorithmFlags')
        algorithmii = hydraii.grab('AlgorithmFlags')
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')

        # crate masks for nans and fills
        mask = numpy.isfinite(difference)
        maskii = abs(difference) < 1e10

        # create masks for algorithm conditions
        maskiii = algorithm % 10 > 0
        maskiv = algorithmii % 10 > 0

        # create masks for quality
        maskv = quality & 15 < 2
        maskvi = qualityii & 15 < 2
        maskvii = quality & 64 == 0
        maskviii = qualityii & 64 == 0

        # combine masks
        masque = mask & maskii & maskiii & maskiv & maskv & maskvi & maskvii & maskviii

        # plot differences
        date = self._stage(path)['date']
        orbit = self._stage(path)['orbit']
        address = 'plots/{}/Ozone_Differences_dynamic_{}_{}.h5'.format(sink, date, orbit)
        title = 'Difference in Ozone, {}, orbit {}, dynamic - static irradiance'.format(date[:-5], orbit)
        self.squid.ink('ozone difference', difference[masque], 'latitude', latitude[masque], address, title)

        return None

    def examine(self, year, month, day, number=0, sink='../run'):
        """Prepare the experiment for examining ozone difference between dynamic and static irradiances

        Arguments:
            year: int, test year
            month: int, test month
            day: int, test day
            number: int, index of test orbit on day
            sink: str, directory for file dump
        """

        # set default sinkii
        sinkii = '/user/1001/home/mbandel/studies/osiris/dynamic'

        # get pcf file
        details = self._know('OMTO3.pcf')

        # find the appropriate radiance file and copy to run directory
        formats = (year, self._pad(month), self._pad(day))
        radiance = Hydra('/tis/acps/OMI/19999/OML1BRUG/{}/{}/{}'.format(*formats))
        paths = [path for path in radiance.paths if path.endswith('.he4')]
        name = paths[number]
        self._print('copying {} into {}...'.format(name, sink))
        self._copy(name, sink)

        # update pcf file
        line = '1100|{}||||{}|1'.format(self._file(name), self._file(name))
        details[5] = line

        # get orbit number
        orbit = int(self._stage(name)['orbit'])

        # find the appropriate cloud file, and copy to ru
        formats = (year, self._pad(month), self._pad(day))
        cloud = Hydra('/tis/acps/OMI/10004/OMCLDRR/{}/{}/{}'.format(*formats))
        paths = [path for path in cloud.paths if path.endswith('.he5')]
        name = paths[number]
        self._print('copying {} into {}...'.format(name, sink))
        self._copy(name, sink)

        # update pcf file
        line = '500001|{}||||{}|1'.format(self._file(name), self._file(name))
        details[7] = line

        # get row anomaly file
        anomaly = Hydra('/tis/acps/OMI/10003/OMTO3yrFlag/{}/01'.format(year))

        # try to
        try:

            # get the closest anomaly fie
            name = anomaly._search(anomaly.paths, '{}m{}'.format(year, self._pad(month)))[0]

        # unless not available
        except IndexError:

            # in which case, get first in year
            name = anomaly.paths[0]

        # copy into run
        self._print('copying {} into {}...'.format(name, sink))
        self._copy(name, sink)

        # swap out pcf reference
        line = '420300|{}|{}||||1'.format(self._file(name), sink)
        details[19] = line

        # get name of static file
        test = Hydra(sink)
        static = [path for path in test.paths if '-overwrite.he4' in path][0]

        # swap out irradiance file in irr file section
        line = '1104|{}||||{}|1'.format(self._file(static), self._file(static))
        details[6] = line

        # swap out irradiance file in backup section
        line = '400100|{}|../run|||{}|1'.format(self._file(static), self._file(static))
        details[9] = line

        # create product name
        formats = (year, self._pad(month), self._pad(day), self._pad(orbit, 6))
        product = 'OMI-Aura_L2-OMTO3_{}m{}{}t0000-o{}_static.he5'.format(*formats)

        # swap into pcf
        line = '449010|{}|||||1'.format(product)
        details[25] = line

        # copy pcf into runn directory
        self._jot(details, '{}/OMTO3.pcf'.format(sink))
        self._jot(details, '{}/OMTO3_static.pcf'.format(sink))

        # run algorithm
        self._print('running test...')
        call = ['make', 'run']
        subprocess.call(call)

        # form dynamic irradiance file name and copy
        dynamic = static.replace('-overwrite.he4', '-o{}_dynamic.he4'.format(self._pad(orbit, 6)))
        self._print('copying {} into {}...'.format(static, dynamic))
        self._copy(static, dynamic)

        # find appropriate irradiance file from collection 4
        original = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}/{}'.format(*formats))
        original.ingest()

        # for each band
        bands = ['BAND{}'.format(index + 1) for index in range(3)]
        for band in bands:

            # get the irradiance data from the irradiance file
            irradiance = original.grab('{}/irradiance'.format(band))
            distance = original.grab('earth_sun_distance')

            # decompose and overwrite into new path
            self.overwrite(dynamic, irradiance, distance)

        # swap out irradiance file for dynamic irradiance in first place
        line = '1104|{}||||{}|1'.format(self._file(dynamic), self._file(dynamic))
        details[6] = line

        # swap out irradiance file in backup
        line = '400100|{}|{}|||{}|1'.format(self._file(dynamic), sink, self._file(dynamic))
        details[9] = line

        # swap out product name
        productii = product.replace('static', 'dynamic')
        line = '449010|{}|||||1'.format(productii)
        details[25] = line

        # copy pcf into runn directory
        self._jot(details, '{}/OMTO3.pcf'.format(sink))
        self._jot(details, '{}/OMTO3_dynamic.pcf'.format(sink))

        # run algorithm
        self._print('running test...')
        call = ['make', 'run']
        subprocess.call(call)

        # transfer products to studies
        self._copy('{}/{}'.format(sink, product), sinkii)
        self._copy('{}/{}'.format(sink, productii), sinkii)

        # calculate difference
        path = '{}/{}'.format(sinkii, product)
        pathii = '{}/{}'.format(sinkii, productii)
        self.differ(path, pathii, 'dynamic')

        return None

    def forest(self, event='2006m0801', orbit=10886, parallels=(-40, 40), rows=(10, 50), exclusions=None):
        """Examine ozone differences through random forest.

        Arguments:
            event: str, date in YYYYmMMDD format
            orbits: int, the orbit number of interest
            archives: tuple of ints, the archive sets
            parallels: tuple of floats, latitudes for zonal means
            rows: tuple of int, the row of interest

        Returns:
            None
        """

        # set exclusions
        exclusions = exclusions or []

        # unpack parallels
        south, north = parallels

        # parse event
        year = int(event[:4])
        month = int(event[5:7])
        day = int(event[7:9])

        # set archives
        archives = (10003, 95044)

        # set reservoirs
        reservoirs = []

        # for each archive set
        for archive in archives:

            # create hydra for first archive set
            formats = (archive, year, self._pad(month), self._pad(day))
            hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats), show=False)

            # ingest data
            hydra.ingest(str(orbit))
            data = hydra.extract('ColumnAmountO3')
            reservoirs.append(data)

        # create differences
        difference = {name: reservoirs[1][name] - reservoirs[0][name] for name in reservoirs[0].keys()}
        reservoirs.append(difference)

        # set stubs
        stubs = ['col3', 'col4', 'diff']

        # for each reservoir
        for index, stub in enumerate(stubs):

            # update names with stubs
            reservoirs[index] = {'{}_{}'.format(stub, name): array for name, array in reservoirs[index].items()}

        # combine into training data
        train = {}
        [train.update(reservoir) for reservoir in reservoirs]

        # get track bouondaries from latitude
        latitude = reservoirs[0]['col3_Latitude']
        start = hydra._pin(south, latitude[:, 30])[0][0]
        finish = hydra._pin(north, latitude[:, 30])[0][0]

        # subset all arrays
        train = {name: array[start: finish, rows[0]: rows[1]] for name, array in train.items()}

        # remove exclusions
        train = {name: array for name, array in train.items() if not any([word in name for word in exclusions])}

        # set target
        target = 'diff_ColumnAmountO3'
        train.update({target: reservoirs[2][target][start: finish, rows[0]: rows[1]]})

        # create destination
        degrees = (self._orient(south), self._orient(north), self._pad(rows[0]), self._pad(rows[1]))
        formats = (self.sink, target, event, orbit, *degrees)
        destination = '{}/reports/random_forest_{}_{}_{}_{}_{}_{}_{}.txt'.format(*formats)

        # plant random forest
        print('running random forest for {}...'.format(destination))
        self.plant(train, target, destination)

        return None

    def fractionate(self, year=2006, month=11, day=28, archive=95042):
        """Examine OMTO3 radiative cloud fraction difference between two reprocessings.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            archive: int, firt archive set
            archiveii: int, second archive set

        Returns:
            None
        """

        # make plots folder
        folder = 'plots/fraction'
        self._make('{}/{}'.format(self.sink, folder))

        # set fields
        fields = {'fraction': 'RadiativeCloudFraction', 'zenith': 'SolarZenithAngle'}
        fields.update({'latitude': 'Latitude', 'longitude': 'Longitude', 'algorithm': 'AlgorithmFlag'})

        # set title stubs
        stubs = fields.copy()
        stubs.update({'snow': 'OMUANC AS 95000 Snow / Ice Flag'})
        stubs.update({'snowii': 'OMUANC AS 10004 Snow / Ice Flag'})
        stubs.update({'snowiii': 'OMUANC Snow / Ice Flag Difference'})

        # create hydras
        date = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(archive, *date))
        hydraii = Hydra('/data/1004/to3/data/OMTO3/20240425/2006m1128')

        # ingest first orbit 12608 for random forest
        hydra.ingest('12608')
        hydraii.ingest('12608')

        # extract data
        data = hydra.extract('RadiativeCloudFraction')
        dataii = hydraii.extract('RadiativeCloudFraction')
        differences = {name: dataii[name] - data[name] for name in data.keys()}

        # add cloud fraction difference
        difference = dataii['RadiativeCloudFraction'] - data['RadiativeCloudFraction']
        data['difference'] = difference

        # apply mask of differences
        mask = (abs(difference) > 0)
        data = {name: array[mask] for name, array in data.items()}
        differences = {name: array[mask] for name, array in differences.items()}

        # run random forest on initial data
        destination = '{}/reports/random_forest_12608_rcf_diffs.txt'.format(self.sink)
        self.plant(data, 'difference', destination)

        # run random forest on differences
        destination = '{}/reports/random_forest_12608_rcf_diffs_diff.txt'.format(self.sink)
        self.plant(differences, 'RadiativeCloudFraction', destination)

        # get snow ice flags from omuanc in two archive sets
        hydraiii = Hydra('/tis/acps/OMI/95000/OMUANC/{}/{}/{}'.format(*date))
        hydraiv = Hydra('/tis/acps/OMI/10004/OMUANC/{}/{}/{}'.format(*date))

        # for each pair
        for path, pathii, pathiii, pathiv in zip(hydra.paths, hydraii.paths, hydraiii.paths, hydraiv.paths):

            # get the orbit number
            orbit = self._stage(path)['orbit']

            # gather archive set data
            hydra.ingest(path)
            data = {field: hydra.grab(search) for field, search in fields.items()}

            # gather archiveii set data
            hydraii.ingest(pathii)
            dataii = {field: hydraii.grab(search) for field, search in fields.items()}

            # get mask for cloud fraction difference
            mask = (data['fraction'] != dataii['fraction'])

            # construct differences as ordiante
            ordinate = (dataii['fraction'] - data['fraction'])[mask]

            # construct abscissas
            abscissas = {'fraction': data['fraction'][mask], 'zenith': data['zenith'][mask]}
            abscissas.update({'latitude': data['latitude'][mask], 'longitude': data['longitude'][mask]})
            abscissas.update({'algorithm': (dataii['algorithm'] - data['algorithm'])[mask]})

            # ingest omuanc
            hydraiii.ingest(pathiii)
            hydraiv.ingest(pathiv)

            # get snow ice flag data from omuanc and add to abscissas
            snow = hydraiii.grab('snow_ice').squeeze()
            snowii = hydraiv.grab('snow_ice').squeeze()
            abscissas.update({'snow': snow[mask]})
            abscissas.update({'snowii': snowii[mask]})
            abscissas.update({'snowiii': (snowii - snow)[mask]})

            # if non empty
            if mask.sum() > 0:

                # for each abscissa
                for name, abscissa in abscissas.items():

                    # duplicate ordinate and abscissa
                    ordinateii = ordinate[:]
                    abscissaii = abscissa[:]

                    # if length of ordinate is only 1
                    if mask.sum() == 1:

                        # double up to avoid plotting error
                        ordinateii = [ordinate[:], ordinate[:]]
                        abscissaii = [abscissa[:], abscissa[:]]

                    # plot cloud pressure difference
                    formats = (fields['fraction'], stubs[name], orbit, *date, archive)
                    title = '{} difference vs {}, o{}, {}m{}{}, OMCLDRR 20240425 - AS{}'.format(*formats)
                    address = '{}/fraction_{}_{}_{}m{}{}.h5'.format(folder, name, orbit, *date)
                    self.squid.ink('fraction/{}'.format(name), ordinateii, name, abscissaii, address, title)

        return None

    def gape(self, year=2024, month=1, day=5, orbit=103585, resolution=1):
        """Examine data gaps in a block of OMTO3 data.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, orbit number
            block: tuple of ints, beginning and ending scanline
            rows: tuple of ints, beginning ending rows
            resolution: int, resolution

        Returns:
            None
        """

        # set block
        block = (1400, 1500)
        scale = (100, 600)
        rows = (20, 30)

        # set zones
        centers = [-150, -130, -110, -90, -70, -50, -30, -10]
        centers += [10, 30, 50, 70, 90, 110, 130, 150]
        zones = [(center - 30, center + 30) for center in centers]

        # make folders
        self._make('{}/gaps'.format(self.sink))
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/gaps'.format(self.sink))

        # get hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(*formats))

        # get hydra for uv
        hydraii = Hydra('/tis/acps/OMI/10003/OML1BRUG/{}/{}/{}'.format(*formats))
        hydraii.ingest(str(orbit))

        # ingest orbit
        hydra.ingest(str(orbit))

        # create searches
        searches = {'ozone': 'ColumnAmountO3', 'one': 'StepOneO3', 'two': 'StepTwoO3'}
        searches.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
        searches.update({'algorithm': 'AlgorithmFlags', 'quality': 'QualityFlags'})

        # grab data
        data = {name: hydra.grab(search) for name, search in searches.items()}

        # create searches for L1B
        searchesii = {'mantissa': 'UV-2/RadianceMantissa', 'exponent': 'UV-2/RadianceExponent'}
        searchesii.update({'coefficient': 'UV-2/WavelengthCoefficient'})
        searchesii.update({'reference': 'UV-2/WavelengthReferenceColumn'})

        # grab data
        dataii = {name: hydraii.grab(search).squeeze() for name, search in searchesii.items()}

        # construct wavelengths and radiances
        conversion = 10000 / 6.02214076e23
        wavelength = self._calculate(dataii['coefficient'], dataii['reference'], 557)
        radiance = conversion * dataii['mantissa'] * 10.0 ** dataii['exponent']

        # find average pixel
        wave = 331
        average = wavelength.mean(axis=0).mean(axis=0)
        pixel = numpy.argmin((average - wave) ** 2)

        print(radiance[:, :, pixel].min(), radiance[:, :, pixel].max(), average[pixel])

        # create array of rows and scanlines
        ozone = data['ozone']
        data['row'] = numpy.array([list(range(ozone.shape[1]))] * ozone.shape[0])
        data['scan'] = numpy.array([list(range(ozone.shape[0]))] * ozone.shape[1]).transpose(1, 0)

        # subset
        subset = {name: array[block[0]: block[1], rows[0]: rows[1]] for name, array in data.items()}

        # create mask for fill conditions
        ozone = subset['ozone']
        quality = subset['quality']
        mask = (ozone < 100) & (ozone > -100) & (numpy.isfinite(ozone))

        # set header
        header = ['scan', 'row', 'latitude', 'longitude', 'one', 'two', 'ozone', 'algorithm', 'quality']

        # create spreadsheet
        sheet = [[searches.get(name, name) for name in header]]
        sheet += list(zip(*[subset[name][mask].flatten() for name in header]))
        sheet = [list(line) for line in sheet]

        # replace quality flags with bits
        for line in sheet[1:]:

            # reassign bit breakdown
            line[-1] = str(self._bite(line[-1]))

        # # print rows
        # [self._print(line) for line in sheet]

        # save to csv
        destination = '{}/gaps/OMTO3_{}m{}{}_{}_{}_{}_{}_{}.csv'.format(self.sink, *formats, orbit, *block, *rows)
        self._table(sheet, destination)

        # specify color gradient and particular indices
        gradient = 'gist_rainbow'
        gradient = 'jet'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]
        selection = [index for index in range(0, 256)]

        # set fill
        fill = -999

        # make data
        ozone = data['ozone']
        latitude = data['latitude']
        longitude = data['longitude']

        # find closest center
        distances = numpy.array([(longitude[1400, 30] - center) ** 2 for center in centers])
        minimum = numpy.argmin(distances)
        zone = zones[minimum]
        limits = (40, 70, *zone)

        # set fill based on mask
        mask = (abs(ozone) < 1e20) & (ozone > 0) & (numpy.isfinite(ozone))
        ozone = numpy.where(mask, ozone, fill)

        # create destioation
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        destination = '{}/plots/gaps/Ozone_{}_o{}_{}.png'.format(self.sink, date, orbit, scale[1])
        title = 'ColumnAmountOzone, {}, o{}'.format(date, orbit)
        arrays = [ozone, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
        parameters.update({'spectrum': [gradient, selection], 'limits': limits, 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        # self._paint(**parameters)

        # make data
        ozone = data['ozone']
        latitude = data['latitude']
        longitude = data['longitude']

        # get masks
        filled = (abs(ozone) > 1e20)
        undefined = numpy.logical_not(numpy.isfinite(ozone))
        negative = (ozone < 0) & (ozone > -999)
        zero = (ozone > 0) & (ozone < 100)
        one = (ozone > 100) & (ozone < 300)
        two = (ozone > 300)
        masks = [filled, undefined, negative, zero, one, two]

        # make legend
        legend = ['fill_values', 'nan', '< 0 DU', '0 to 100 DU', '100 - 300 DU', '> 300 DU']

        # for each mask
        for index, mask in enumerate(masks):

            # change ozone data
            ozone = numpy.where(mask, index, ozone)

        # create destioation
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        destination = '{}/plots/gaps/Ozone_fills_{}_o{}_{}.png'.format(self.sink, date, orbit, scale[1])
        title = 'ColumnAmountOzone, {}, o{}'.format(date, orbit)
        arrays = [ozone, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 5)}
        parameters.update({'spectrum': [gradient, selection], 'limits': limits, 'grid': resolution})
        parameters.update({'log': False, 'bar': False, 'back': 'white'})
        parameters.update({'legend': legend})
        # parameters.update({'projection': cartopy.crs.Mollweide()})
        self._paint(**parameters)

        # make data
        radiance = radiance[:, :, pixel]
        latitude = data['latitude']
        longitude = data['longitude']

        # create destioation
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        destination = '{}/plots/gaps/OML1BRUG_{}_o{}_{}.png'.format(self.sink, date, orbit, scale[1])
        title = 'Radiance 331nm, {}, o{}'.format(date, orbit)
        arrays = [radiance, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 5e-7)}
        parameters.update({'spectrum': [gradient, selection], 'limits': limits, 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        # parameters.update({'legend': legend})
        # parameters.update({'projection': cartopy.crs.Mollweide()})
        self._paint(**parameters)

        return None

    def glob(self, year=2024, month=1, day=5, resolution=1):
        """Examine data gaps in gridded product.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            resolution: int, resolution

        Returns:
            None
        """

        # make folders
        self._make('{}/gaps'.format(self.sink))
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/gaps'.format(self.sink))

        # get hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3G/{}/{}'.format(*formats[:2]))

        # ingest orbit
        date = '{}m{}{}'.format(*formats)
        hydra.ingest('_{}_'.format(date))

        # create searches
        searches = {'ozone': 'ColumnAmountO3', 'two': 'StepTwoO3'}
        searches.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
        searches.update({'algorithm': 'AlgorithmFlags', 'quality': 'QualityFlags'})

        # grab data
        data = {name: hydra.grab(search) for name, search in searches.items()}

        # get orbits
        orbits = hydra.grab('OrbitNumber').max(axis=1).max(axis=1)

        # specify color gradient and particular indices
        gradient = 'jet_r'
        selection = [index for index in range(0, 256)]

        # set fill
        fill = -999

        # make data
        ozone = data['ozone']
        quality = data['quality']
        latitude = data['latitude']
        longitude = data['longitude']
        anomaly = ((quality & 64) == 0).astype(int)

        # for each orbit
        for index, orbit in enumerate(orbits):

            # alert
            self._stamp('plotting {}, {} of {}...'.format(orbit, index, len(orbits)), initial=True)

            # create destioation
            date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
            destination = '{}/plots/gaps/Grid_{}_o{}.png'.format(self.sink, date, index)
            title = 'Row Anomaly, OMTO3G, {}'.format(date)
            arrays = [anomaly[index], latitude[index], longitude[index]]
            texts = [title, '_']
            legend = ['row anomaly', 'no anomaly']
            parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 1)}
            parameters.update({'spectrum': [gradient, selection], 'limits': (-90, 90, -180, 180), 'grid': resolution})
            parameters.update({'log': False, 'bar': False, 'back': 'white', 'legend': legend, 'points': True})

            # paint
            self._paint(**parameters)
            self._stamp('plotted')

        return None

    def grid(self, year=2024, month=1, day=5, resolution=1, scale=(100, 500)):
        """Examine data gaps in gridded product.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            resolution: int, resolution
            scale: tuple of int, data range

        Returns:
            None
        """

        # make folders
        self._make('{}/gaps'.format(self.sink))
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/gaps'.format(self.sink))

        # get hydra for gridded product
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3d/{}/{}'.format(*formats[:2]))

        # ingest orbit
        date = '{}m{}{}'.format(*formats)
        hydra.ingest('_{}_'.format(date))

        # grab the ozone data
        ozone = hydra.grab('ColumnAmountO3')

        # add fill everywhere ozone
        mask = (ozone > scale[0]) & (numpy.isfinite(ozone))

        # add fill values for unmasked pixels
        fill = -9999
        ozone = numpy.where(mask, ozone, fill)

        # construct latitudes and longitudes
        latitude = numpy.array([[index - 90 for index in range(180)]] * 360).transpose(1, 0)
        longitude = numpy.array([[index - 180 for index in range(360)]] * 180)

        # get second hydra for orbital files
        hydraii = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(*formats))
        paths = [path for path in hydraii.paths if path.endswith('.he5')]

        # begin reservoirs
        qualities = []
        ozones = []
        latitudes = []
        longitudes = []
        zeniths = []

        # for each path
        for path in paths:

            # ingest
            hydraii.ingest(path)

            # get data
            qualities.append(hydraii.grab('QualityFlags'))
            ozones.append(hydraii.grab('ColumnAmountO3'))
            latitudes.append(hydraii.grab('Latitude'))
            longitudes.append(hydraii.grab('Longitude'))
            zeniths.append(hydraii.grab('SolarZenithAngle'))

        # create stacks
        qualityii = numpy.vstack(qualities)
        ozoneii = numpy.vstack(ozones)
        latitudeii = numpy.vstack(latitudes)
        longitudeii = numpy.vstack(longitudes)
        zenithii = numpy.vstack(zeniths)

        # make mask for threshold 360 ( bits 1 and 2, but not 0 )
        mask = ((qualityii & 1) == 1) & ((qualityii & 2) == 2) & ((qualityii & 4) == 0)
        thresholdii = numpy.where(mask, ozoneii, fill)

        # create mask for ozone
        mask = (ozoneii > scale[0]) & (numpy.isfinite(ozoneii)) & ((qualityii & 64) == 0)
        maskii = ((qualityii & 1) == 1) & ((qualityii & 2) == 2) & ((qualityii & 4) == 0)
        masque = mask & numpy.logical_not(maskii)
        ozoneii = numpy.where(masque, ozoneii, fill)

        # create mask for zenith
        # mask = ((qualityii & 64) == 0) & (numpy.isfinite(ozoneii))
        mask = ((qualityii & 64) == 0)
        zenithii = numpy.where(mask, zenithii, fill)

        # specify color gradient and particular indices
        gradient = 'colorbars/ozone_colorbar.txt'
        selection = [index for index in range(0, 256)]

        # extend ranges
        epsilon = 1e-4
        # ozone = numpy.where((ozone <= scale[0] + epsilon) & (ozone > fill + epsilon), scale[0] + epsilon, ozone)
        ozone = numpy.where((ozone >= scale[1] - epsilon), scale[1] - epsilon, ozone)
        # ozoneii = numpy.where((ozoneii <= scale[0] + epsilon) & (ozoneii > fill + epsilon), scale[0] + epsilon, ozoneii)
        ozoneii = numpy.where((ozoneii >= scale[1] - epsilon), scale[1] - epsilon, ozoneii)

        # plot grid
        destination = '{}/plots/gaps/Gridded_{}_total.png'.format(self.sink, date)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'ColumnAmountO3, OMTO3d, {}'.format(date)
        arrays = [ozone, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
        parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        # parameters.update({'points': True})
        self._paint(**parameters)
        self._stamp('plotted.')

        # plot orbits
        destination = '{}/plots/gaps/Orbits_{}_total.png'.format(self.sink, date)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'ColumnAmountO3, OMTO3, {}'.format(date)
        arrays = [ozoneii, latitudeii, longitudeii]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
        parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        self._paint(**parameters)
        self._stamp('plotted.')

        # clip zeniths at 100
        zenithii = numpy.where(zenithii > 100, 100, zenithii)

        # plot zenith angle
        destination = '{}/plots/gaps/Zenith_{}_total.png'.format(self.sink, date)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'SolarZenithAngle, {}'.format(date)
        arrays = [zenithii, latitudeii, longitudeii]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 100)}
        parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        self._paint(**parameters)
        self._stamp('plotted.')

        # plot threshold 360
        destination = '{}/plots/gaps/Threshold360_{}_total.png'.format(self.sink, date)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'Residual 360 > Threshold, OMTO3, {}'.format(date)
        arrays = [thresholdii, latitudeii, longitudeii]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
        parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
        parameters.update({'log': False, 'bar': True, 'back': 'white'})
        self._paint(**parameters)
        self._stamp('plotted.')

        # for each path
        for index, path in enumerate(paths):

            # get the orbit
            orbit = self._stage(path)['orbit']

            # construct mask
            mask = (ozones[index] > 0) & (numpy.isfinite(ozones[index])) & ((qualities[index] & 64) == 0)
            ozones[index] = numpy.where(mask, ozones[index], fill)

            # adjust scale
            condition = (ozones[index] <= scale[0] + epsilon) & (ozones[index] > fill + epsilon)
            # ozones[index] = numpy.where(condition, scale[0] + epsilon, ozones[index])
            ozones[index] = numpy.where((ozones[index] >= scale[1] - epsilon), scale[1] - epsilon, ozones[index])

            # plot orbits
            destination = '{}/plots/gaps/Orbit_{}_o{}.png'.format(self.sink, date, orbit)
            self._stamp('plotting {}...'.format(destination), initial=True)
            title = 'ColumnAmountO3, OMTO3, {}, o{}'.format(date, orbit)
            arrays = [ozones[index], latitudes[index], longitudes[index]]
            texts = [title, '_']
            parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
            parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
            parameters.update({'log': False, 'bar': True, 'back': 'white'})
            self._paint(**parameters)
            self._stamp('plotted.')

            # cap zenith at 100
            zeniths[index] = numpy.where(zeniths[index] > 100, 100, zeniths[index])

            # plot orbits
            destination = '{}/plots/gaps/Zenith_{}_o{}.png'.format(self.sink, date, orbit)
            self._stamp('plotting {}...'.format(destination), initial=True)
            title = 'SolarZenithAngle, {}, o{}'.format(date, orbit)
            arrays = [zeniths[index], latitudes[index], longitudes[index]]
            texts = [title, '_']
            parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (0, 100)}
            parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
            parameters.update({'log': False, 'bar': True, 'back': 'white'})
            self._paint(**parameters)
            self._stamp('plotted.')

            # make mask for threshold 360 ( bits 1 and 2, but not 0 )
            mask = ((qualities[index] & 1) == 1) & ((qualities[index] & 2) == 2) & ((qualities[index] & 4) == 0)
            threshold = numpy.where(mask, ozones[index], fill)

            # plot orbits
            destination = '{}/plots/gaps/Threshold_{}_o{}.png'.format(self.sink, date, orbit)
            self._stamp('plotting {}...'.format(destination), initial=True)
            title = 'Residual 360 > Threshold, OMTO3, {}, o{}'.format(date, orbit)
            arrays = [threshold, latitudes[index], longitudes[index]]
            texts = [title, '_']
            parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': scale}
            parameters.update({'spectrum': [gradient, selection], 'limits': (45, 90, -180, 180), 'grid': resolution})
            parameters.update({'log': False, 'bar': True, 'back': 'white'})
            self._paint(**parameters)
            self._stamp('plotted.')

        return None

    def grow(self, year=2005, month=3, day=21, number=0, target='aerosol', exclusions=['sulfur'], position=0):
        """Use random forest to predict fill values in omto3 data.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            number: int, number of orbit on day
            target: str, name of target
            exclusions: list of str, parameters to be excluded
            position: int, slice for three-d arrays

        Returns:
            None
        """

        # make hydra and ingest top orbit
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/95042/OMTO3/{}/{}/{}'.format(*formats))
        hydra.ingest(number)

        # set up searches
        searches = {'reflectivity': 'Reflectivity331', 'ozone': 'ColumnAmountO3'}
        searches.update({'quality': 'QualityFlags', 'algorithm': 'AlgorithmFlags'})
        searches.update({'sulfur': 'SO2index', 'aerosol': 'UVAerosolIndex'})
        searches.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
        searches.update({'zenith': 'SolarZenithAngle', 'zenithii': 'ViewingZenithAngle'})
        searches.update({'step': 'ResidualStep1', 'stepii': 'ResidualStep2'})
        searches.update({'residual': 'Residual'})

        # get data
        data = {name: hydra.grab(search).squeeze() for name, search in searches.items()}

        # pare down 3-D arrays to first
        for name, array in data.items():

            # if 3-d
            if len(array.shape) > 2:

                # take a slice
                data[name] = array[:, :, position]

        # construct reflectivity mask for all cases > 0, < 100
        mask = (data['reflectivity'] > 0) & (data['reflectivity'] < 100)

        # construct algorithm flag mask for all samples not skipped ( those with snow / ice are allowed )
        maskii = (data['algorithm'] % 10 > 0)

        # set quality mask for ascending and good retrievals (QualityFlags & 15) == 0 | 1
        maskiii = (data['quality'] & 15 < 2)

        # set quality mask for no anomaly ( not bit 6 )
        maskiv = (data['quality'] & 64 == 0)

        # combine all masks
        masque = mask & maskii & maskiii & maskiv

        # apply masque
        data = {name: array[masque] for name, array in data.items()}

        # replace target with mask for presence of fill
        data[target] = (abs(data[target]) < 1e10).astype(int)

        # assemble matrix
        names = [name for name in data.keys() if name not in exclusions + [target]] + [target]
        train = numpy.array([data[name] for name in names]).transpose(1, 0)

        # screen out fill values
        screen = abs(train.sum(axis=1)) < 1e10
        train = train[screen]

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # begin report
        report = ['Random Forest predicting fill values for {}'.format(searches[target])]
        report.append('in file: {}/'.format(self._fold(hydra.current)))
        report.append('{}'.format(self._file(hydra.current)))
        report.append('')

        # run random forest
        self._stamp('running random forest for fills in {}...'.format(searches[target]), initial=True)
        self._print('matrix: {}'.format(matrix.shape))
        forest = RandomForestClassifier(n_estimators=100, max_depth=5)
        forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(names, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(searches[field], importance)))

        # make report
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, searches[target])
        self._jot(report, destination)

        # print status
        self._stamp('planted.')

        return None

    def heat(self, orbit='100172', resolution=20):
        """Make heatmap of cloud pressure differences.

        Arguments:
            orbit: str, orbit number
            resolution: int, number of pixels to coagulate

        Returns:
            None
        """

        # set defaults
        source = 'three'
        sourceii = 'four'
        sourceiii = 'radiance'
        sink = 'plots/heatmap'

        # set names
        names = {'three': 'Col3', 'four': 'Col4', 'hybrid': 'Col4L1B+Col3 OMCLDRR'}

        # create hydras
        hydra = Hydra('{}/{}'.format(self.sink, source))
        hydraii = Hydra('{}/{}'.format(self.sink, sourceii))
        hydraiii = Hydra('{}/{}'.format(self.sink, sourceiii))

        # ingest paths
        hydra.ingest('o' + orbit)
        hydraii.ingest('o' + orbit)
        hydraiii.ingest('o' + orbit)

        # grab geolocation from first
        latitude = hydraii.grab('Latitude')
        longitude = hydraii.grab('Longitude')

        # grab latitude and longitude bounds
        bounds = hydraiii.grab('latitude_bounds').squeeze()
        boundsii = hydraiii.grab('longitude_bounds').squeeze()

        # grab quality flags and clouds
        cloud = hydra.grab('CloudPressure')
        cloudii = hydraii.grab('CloudPressure')
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')
        reflectivity = hydraii.grab('Reflectivity331')
        fraction = hydra.grab('CloudFraction')
        fractionii = hydraii.grab('CloudFraction')
        terrain = hydra.grab('Residual')[:, :, 5]
        terrainii = hydraii.grab('Residual')[:, :, 5]

        # calculate cloud difference
        difference = cloudii - cloud

        # unpack bits and get bit 7 for cloud pressure source
        bits = numpy.unpackbits(quality.astype('uint8')).reshape(*quality.shape, -1)[:, :, 0]
        bitsii = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 0]
        climatology = bitsii - bits

        # unpack bits and get bit 6 for row anomaly
        anomaly = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 1]

        # unpack bits for descending pixels
        quartet = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)
        quartet[:, :, :4] = 0
        descension = numpy.packbits(quartet.flatten()).reshape(qualityii.shape)

        # construct polygons from points
        polygons = self._polymerize(latitude, longitude, resolution)

        # construct polygons from bounds
        polygons = self._polymerize(bounds, boundsii, resolution)

        # # get corners from latitude and longitude bounds
        # corners = self._orientate(bounds, boundsii)
        # corners = self._compress(corners, resolution, 1)
        #
        # # construct bounds
        # data = {}
        # compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        # arrays = [corners[compass][:, :, 0] for compass in compasses]
        # data['latitude_bounds'] = numpy.stack(arrays, axis=2)
        # arrays = [corners[compass][:, :, 1] for compass in compasses]
        # data['longitude_bounds'] = numpy.stack(arrays, axis=2)
        #
        # # adjust polygon  boundaries to avoid crossing dateline
        # data['longitude_bounds'] = self._cross(data['longitude_bounds'])
        #
        # # make polygons
        # bounds = data['latitude_bounds']
        # boundsii = data['longitude_bounds']
        # polygons = numpy.dstack([bounds, boundsii])

        # apply resolution to other fields
        evens = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])
        difference = difference[evens]
        climatology = climatology[evens]
        anomaly = anomaly[evens]
        latitude = latitude[evens]
        longitude = longitude[evens]
        descension = descension[evens]
        reflectivity = reflectivity[evens]
        cloud = cloud[evens]
        cloudii = cloudii[evens]
        fraction = fraction[evens]
        fractionii = fractionii[evens]
        terrain = terrain[evens]
        terrainii = terrainii[evens]

        # create mask for no anomaly, no source difference, and no descending orbit
        mask = (climatology == 0) & (anomaly == 0) & (descension < 10)

        # apply mask
        difference = difference[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]
        polygons = polygons[mask]
        reflectivity = reflectivity[mask]
        cloud = cloud[mask]
        cloudii = cloudii[mask]
        fraction = fraction[mask]
        fractionii = fractionii[mask]
        terrain = terrain[mask]
        terrainii = terrainii[mask]

        # print highest difference pixel
        maximum = difference.max()
        pixels = self._pin(maximum, difference, number=20)
        for pixel in pixels:

            # print
            formats = [array[pixel] for array in (latitude, longitude, difference, cloud, cloudii)]
            formats += [array[pixel] for array in (terrain, terrainii)]
            self._print('lat: {} lon: {} diff: {} col3: {} col4: {} ter3: {} ter4: {}'.format(*formats))

        # create color bracket
        bracket = [-1000, -600, -200, 200, 600, 1000]

        # get date
        date = self._stage(hydra.current)['day']

        # heat map differences
        tracer = difference
        folder = '{}/cloud_difference_heatmap.h5'.format(sink)
        title = 'Cloud Pressure differences, same source, Col4 - Col3, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure_difference'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # create color bracket
        bracket = [0, 50, 100, 150, 200]

        # heat map reflectivity
        tracer = reflectivity
        folder = '{}/reflectivity_heatmap.h5'.format(sink)
        title = 'Reflectivity331, same source, Col4 - Col3, orbit {}, {}'.format(orbit, date)
        name = 'reflectivity'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # create color bracket
        bracket = [200, 400, 600, 800, 1000, 1200]

        # heat map reflectivity
        tracer = cloudii
        folder = '{}/cloud_pressure_col4_heatmap.h5'.format(sink)
        title = 'Cloud Pressure, Col4, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # heat map reflectivity
        tracer = cloud
        folder = '{}/cloud_pressure_col3_heatmap.h5'.format(sink)
        title = 'Cloud Pressure, Col3, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # scatter plot cloud pressure diff vs cloud fraction diff
        differenceii = fractionii - fraction
        maskii = (abs(difference) < 10000) & (abs(differenceii) < 10000)
        folder = 'plots/scatters/cloud_pressure_diff_fraction_diff.h5'
        title = 'Cloud Pressure differences vs Cloud Fraction differences'
        name = 'cloud_fraction_pressure'
        self.squid.ink(name, difference[maskii], 'fraction', differenceii[maskii], folder, title)

        return None

    def hex(self):
        """Plot the location of quality flag six instances between two versions.

        Arguments:
            None

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/hex'.format(self.sink))

        # create hydra for acps run
        hydra = Hydra('../studies/osiris/eight/output')
        hydra.ingest('acps')

        # get data
        quality = hydra.grab('QualityFlags')
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')

        # ingest the tlcf run
        hydra.ingest('original')
        qualityii = hydra.grab('QualityFlags')

        # create masks
        mask = (quality - qualityii == 64)
        maskii = (qualityii - quality == 64)

        print(mask.sum())
        print(maskii.sum())

        # set up map plot
        size = (10, 5)
        back = 'white'
        native = cartopy.crs.PlateCarree()
        pyplot.clf()
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=3)
        _ = axis.gridlines(draw_labels=True)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left = 0.1
        right = 0.1
        top = 0.19
        bottom = 0.18
        pyplot.subplots_adjust(left=left, right=1-right, top=1-top, bottom=bottom)

        # set axis ratios
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        title = 'Quality Flag 6 differences'
        axis.set_xlim(-180, 180)
        axis.set_ylim(-90, 90)
        axis.set_title(title, fontsize=14)

        # for each mask
        labels = ('tlcf', 'acps')
        for masque, color, label in zip([maskii, mask], ('green', 'red'), labels):

            # unpack point
            ordinate = latitude[masque]
            abscissa = longitude[masque]

            # plot
            axis.plot(abscissa, ordinate, marker='.', color=color, markersize=4, label=label, linestyle='None')

        # add legend
        pyplot.legend()

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        destination = '{}/plots/hex/Quality_Six.png'.format(self.sink)
        pyplot.savefig(destination)
        pyplot.clf()

        return None

    def hiss(self, tag='acpsii.he5', tagii='epififteen.he5', resolution=50):
        """Plot histograms of ozone and N-Value differences.

        Arguments:
            tag: str, tag of orginal acps data
            tagii: str, tag of comparison data
            resolution: int, grid polygon size

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/differences'.format(self.sink))
        self._make('{}/plots/hiss'.format(self.sink))
        self._make('{}/plots/scatter'.format(self.sink))

        # create hydra for acps run
        hydra = Hydra('../studies/osiris/eight/output')
        hydra.ingest(tag)

        # get data
        value = hydra.grab('NValue')
        ozone = hydra.grab('ColumnAmountO3')
        wavelength = hydra.grab('Wavelength')
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')
        reflectivity = hydra.grab('Reflectivity331')
        one = hydra.grab('StepOneO3')
        two = hydra.grab('StepTwoO3')

        # ingest the tlcf run
        hydra.ingest(tagii)
        valueii = hydra.grab('NValue')
        ozoneii = hydra.grab('ColumnAmountO3')
        reflectivityii = hydra.grab('Reflectivity331')
        oneii = hydra.grab('StepOneO3')
        twoii = hydra.grab('StepTwoO3')

        # construct differences
        differences = {'ozone': ozoneii - ozone, 'reflectivity': reflectivityii - reflectivity}
        differences.update({'one': oneii - one, 'two': twoii - two})
        for index, wave in enumerate(wavelength):

            # update with nvalue
            differences.update({'nvalue_{}'.format(int(wave)): valueii[:, :, index] - value[:, :, index]})

        # set scales
        scales = {'large': (-1000, 1000), 'small': (-10, 10), 'tiny': (-0.1, 0.1), 'smaller': (-0.5, 0.5)}
        limits = {'full': (-90, 90, -180, 180), 'zoom': (-90, 90, -180, -120)}

        # for each difference
        for name, array in differences.items():

            # for each scale
            for stub, scale in scales.items():

                # create mask for fill values and scale
                mask = (abs(array) < 1e20) & numpy.isfinite(array) & (abs(array) <= scale[1])

                # create histogram
                title = 'Histogram of {} differences ( Col 4 Reader - 2.4.4 )'.format(name)
                address = 'plots/hiss/{}_histogram_{}.h5'.format(name, stub)
                self.squid.ripple(name, array[mask], address, title)

                # create scatter plot
                title = 'Scatter Plot of {} differences ( Col4 Reader - 2.4.4. )'.format(name)
                address = 'plots/scatter/{}_scatter_{}.h5'.format(name, stub)
                self.squid.ink(name, array[mask], 'ozone diff', differences['ozone'][mask], address, title)

        # for each scale
        for stub, scale in scales.items():

            # for each limit
            for zoom, limit in limits.items():

                # plot ozone differences on a map
                title = 'Ozone differences, Col 4 Reader - 2.4.4'
                destination = '{}/plots/differences/ozone_differences_{}_{}.png'.format(self.sink, stub, zoom)
                texts = [title, 'DU']
                arrays = [differences['ozone'], latitude, longitude]
                gradient = 'seismic'
                selection = list(range(256))
                options = {'bar': True, 'back': 'black'}
                parameters = [arrays, destination, texts, scale, [gradient, selection], limit, resolution]
                self._paint(*parameters, **options)

        return None

    def histolyze(self):
        """Generate histograms looking at omto3G differences.

        Arguments:
            None

        Returns:
            None
        """

        # create plots folder
        self._make('{}/plots/histolysis'.format(self.sink))

        # create hydra
        hydra = Hydra('{}/day'.format(self.sink))
        hydraii = Hydra('{}/day'.format(self.sink))

        # ingest files
        hydra.ingest(0)
        hydraii.ingest(1)

        # get date
        date = hydraii._stage(hydraii.current)['day']

        # set fields of interest
        fields = {'ozone': 'ColumnAmountO3', 'latitude': 'Latitude', 'longitude': 'Longitude'}
        fields.update({'configuration': 'InstrumentConfigurationId', 'two': 'StepTwoO3'})
        fields.update({'terrain': 'TerrainPressure', 'height': 'TerrainHeight'})
        fields.update({'zenith': 'SolarZenithAngle', 'cloud': 'CloudPressure'})
        fields.update({'reflectivity': 'Reflectivity331', 'reflectivityii': 'Reflectivity360'})
        fields.update({'seconds': 'SecondsInDay', 'ground': 'GroundPixelQuality'})

        # collect all data
        data = {field: hydra.grab(search) for field, search in fields.items()}
        dataii = {field: hydraii.grab(search) for field, search in fields.items()}

        # set fills
        fill = -999
        fillii = 1e30

        # get mask to avoid fill value differences ( all configuration diffs are fills )
        mask = (data['configuration'] != dataii['configuration'])
        mask = numpy.logical_not(mask)

        # for each field
        for field, search in fields.items():

            # generate differences
            differences = dataii[field] - data[field]

            # get total number of pixels
            total = numpy.prod(differences.shape)

            # create mask for non zeros, combine with non fills
            maskii = (abs(differences) > 0)
            masque = mask & maskii

            # get total number of masked, and calucualte percent
            masked = masque.sum()
            percent = round(100 * (masked / total), 4)

            # if masked is greater than o
            if masked > 0:

                # generate histogram
                formats = (search, masked, percent)
                title = '{} differences ( {} pixels, {} % ), OMTO3G AS 95042 - V8.5, 2005m0408'.format(*formats)
                address = 'plots/histolysis/{}_histogram.h5'.format(field)
                self.squid.ripple(field, differences[masque], address, title)

        # also gather ozone difference vs ground pixel quality
        ozone = dataii['ozone'] - data['ozone']
        ground = numpy.log2(dataii['ground'])
        mask = (abs(ozone) > 0) & (numpy.isfinite(ground)) & (numpy.isfinite(ozone)) & (abs(ozone) < 1e20)

        # scatter plot
        title = 'Ozone differences vs ground pixel quality, 2005m0408'
        address = 'plots/histolysis/ozone_ground_scatter.h5'
        self.squid.ink('ozone', ozone[mask], 'ground', ground[mask], address, title)

        return None

    def historicize(self, tag='cloudii.he5', tagii='anomalyii.he5', tagiii='climatologyii.he5', resolution=50):
        """Plot histograms of ozone and N-Value differences.

        Arguments:
            tag: str, tag of orginal acps data
            tagii: str, tag of comparison data
            tagiii: str, tag of comparison data
            resolution: int, grid polygon size

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/history'.format(self.sink))

        # get hydras
        hydra = Hydra('../studies/osiris/eight/quality')
        hydraii = Hydra('../studies/osiris/eight/quality')
        hydraiii = Hydra('../studies/osiris/eight/quality')

        # ingest tags
        hydra.ingest(tag)
        hydraii.ingest(tagii)
        hydraiii.ingest(tagiii)

        # get calibration adjustments
        adjustment = hydra.grab('CalibrationAdjustment')
        adjustmentii = hydraii.grab('CalibrationAdjustment')
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')

        # compute difference
        difference = adjustmentii - adjustment
        mask = (abs(difference) > 0)

        # get orbit and date
        orbit = self._stage(hydra.current)['orbit']
        date = self._stage(hydra.current)['day']

        # plot histogram
        title ='Histogram of CalAdjustment Diffs due to OMUANC row anomaly, o{}, {}'.format(orbit, date)
        address = 'plots/history/Cal_adj_histogram.h5'
        self.squid.ripple("calibration adjustment differennce", difference[mask], address, title)

        # construct scanline and row matrix
        scans = numpy.array([range(quality.shape[0])] * quality.shape[1]).transpose(1, 0)
        rows = numpy.array([range(quality.shape[1])] * quality.shape[0])

        # construct difference mask
        mask = (quality != qualityii)
        title = 'QualityFlag differences with switch to OMUANC row anomaly, o{}, {}'.format(orbit, date)
        address = 'plots/history/Row_anomaly_diff.h5'
        self.squid.ink('scan', scans[mask], 'row', rows[mask], address, title)

        # construct difference mask
        mask = ((quality & 64) == 64)
        title = 'OMUANC row anomaly, o{}, {}'.format(orbit, date)
        address = 'plots/history/Row_anomaly_omuanc.h5'
        self.squid.ink('scan', scans[mask], 'row', rows[mask], address, title)

        # construct difference mask
        mask = ((qualityii & 64) == 64)
        title = 'Prior Row Anomaly, o{}, {}'.format(orbit, date)
        address = 'plots/history/Row_anomaly_prior.h5'
        self.squid.ink('scan', scans[mask], 'row', rows[mask], address, title)

        # set climatology difference fields
        fields = {'ozone': 'ColumnAmountO3', 'cloud': 'CloudPressure', 'reflectivity': 'Reflectivity360'}

        # construct data differences
        data = {field: hydraiii.grab(search) - hydra.grab(search) for field, search in fields.items()}

        self._view(data)

        # for each difference
        for field, array in data.items():

            # set mask
            mask = numpy.isfinite(array)

            # plot histogram of difference
            title = '{} difference, Climatology - OMCLDRR, o{}, {}'.format(fields[field], orbit, date)
            address = 'plots/history/histogram_{}.h5'.format(field)
            self.squid.ripple(field, array[mask], address, title)

        return None

    def imbibe(self, path, destination=None):
        """Create a color map file from a png image.

        Arguments:
            path: str, filename of colorbar image
            destination: str, filename of colorbar.txt file

        Returns:
            None
        """

        # get the image and convert to array
        image = PIL.Image.open('{}/{}'.format(self.sink, path))
        image = numpy.array(image)

        # find the vertical middle
        middle = int(math.floor(image.shape[0]) / 2)
        tape = image[middle]

        # find first index not white
        first = (tape.sum(axis=1) < 1000).tolist().index(True)
        last = (tape[first:].sum(axis=1) > 1000).tolist().index(True) + first

        # subset
        tape = tape[first: last]

        # begin color bar
        bar = []
        for pixel in tape:

            # divide each channel by 255, including alpha
            spectrum = [channel / 255 for channel in pixel]

            # add to bar
            bar.append('{} {} {} {}'.format(*spectrum))

        # jot down bar
        destination = destination or path.replace('.png', '.txt')
        self._jot(bar, '{}/{}'.format(self.sink, destination))

        return None

    def investigate(self, year=2020, month=6, row=5, bounds=(-20, 20), archives=(95042, 95031)):
        """Investigate sample number discrepancies amongst two archive sets.

        Arguments:
            year: int, the year
            month: int, the month
            row: int, row of interest
            bounds: tuple of floats, latitude bounds
            archives: tuple of ints, the archive sets

        Returns:
            None
        """

        # begin report
        report = []

        # for each day
        for day in range(1, 32):

            # create hydras
            formats = (archives[0], year, self._pad(month), self._pad(day))
            formatsii = (archives[1], year, self._pad(month), self._pad(day))
            hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))
            hydraii = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formatsii))

            # subset paths
            paths = [path for path in hydra.paths if path.endswith('.he5')]
            pathsii = [path for path in hydraii.paths if path.endswith('.he5')]

            # print date
            report.append(self._print(' '))
            report.append(self._print('{}m{}{}, Row: {}'.format(year, self._pad(month), self._pad(day), row)))

            # for each pair of paths
            for path, pathii in zip(paths, pathsii):

                # ingest paths
                hydra.ingest(path)
                hydraii.ingest(pathii)

                # grab data
                ozone = hydra.grab('ColumnAmountO3')
                ozoneii = hydraii.grab('ColumnAmountO3')
                quality = hydra.grab('QualityFlags')
                qualityii = hydraii.grab('QualityFlags')

                # find latitude indices
                latitude = hydra.grab('Latitude')
                south = hydra._pin(bounds[0], latitude[:, row])[0][0]
                north = hydra._pin(bounds[1], latitude[:, row])[0][0]

                # subset data by latitude and row
                ozone = ozone[south: north, row]
                ozoneii = ozoneii[south: north, row]
                quality = quality[south: north, row]
                qualityii = qualityii[south: north, row]

                # create finite masks
                mask = (numpy.isfinite(ozone)) & (abs(ozone) < 1e20)
                maskii = (numpy.isfinite(ozoneii)) & (abs(ozoneii) < 1e20)

                # print orbit
                report.append(self._print(''))
                report.append(self._print('orbit: {}'.format(self._stage(path)['orbit'])))

                # print ozone means
                formats = (archives[0], ozone[mask].mean(), archives[1], ozoneii[maskii].mean())
                report.append(self._print('ozone mean: {}: {}, {}: {}'.format(*formats)))

                # print sample counts
                formats = (archives[0], mask.sum(), archives[1], maskii.sum())
                report.append(self._print('ozone counts: {}: {}, {}: {}'.format(*formats)))

                # extract flags
                flags = [self._bite(flag) for flag in quality[mask]]
                flagsii = [self._bite(flag) for flag in qualityii[maskii]]

                # extract decimal flags
                decimals = [self._unite(*[bit for bit in flag if bit < 3]) for flag in flags]
                decimalsii = [self._unite(*[bit for bit in flag if bit < 3]) for flag in flagsii]

                # extract bit flags
                bits = [[bit for bit in flag if bit > 2] for flag in flags]
                bitsii = [[bit for bit in flag if bit > 2] for flag in flagsii]
                bits = [bit for flag in bits for bit in flag]
                bitsii = [bit for flag in bitsii for bit in flag]

                # count decimal flags
                counter = Counter(decimals)
                counterii = Counter(decimalsii)
                keys = self._skim(list(counter.keys()) + list(counterii.keys()))
                for key in keys:

                    # report counts
                    count = counter.get(key, 0)
                    countii = counterii.get(key, 0)
                    difference = (count != countii)
                    formats = (key, archives[0], count, archives[1], countii, '***' * int(difference))
                    report.append(self._print('decimal {}: {}: {}, {}: {} {}'.format(*formats)))

                # count bit flags
                counter = Counter(bits)
                counterii = Counter(bitsii)
                keys = self._skim(list(counter.keys()) + list(counterii.keys()))
                for key in keys:

                    # report counts
                    count = counter.get(key, 0)
                    countii = counterii.get(key, 0)
                    difference = (count != countii)
                    formats = (key, archives[0], count, archives[1], countii, '***' * int(difference))
                    report.append(self._print('bit {}: {}: {}, {}: {} {}'.format(*formats)))

        # print report
        [self._print(line) for line in report]

        # jot report
        self._jot(report, '{}/reports/OMTO3_ozone_comparison_row_{}.txt'.format(self.sink, row))

        return None

    def irradiate(self, *names, first='prior', second='irradiance', positions=None, condition=None):
        """Examine placement of differences between one pass irradiance and loop irradiance interplation.

        Arguments:
            *names: unpacked list of feature names
            first: str, tag for first file
            second: str, tag for second file
            positions: list of ints, the indices to check
            condition: function object, condition for plotting data

        Returns:
            None
        """

        # create folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/irradiance'.format(self.sink))

        # create hydras
        hydra = Hydra('{}/output'.format(self.sink))
        hydraii = Hydra('{}/output'.format(self.sink))

        # ingest irradiance file and prior file
        hydra.ingest(first)
        hydraii.ingest(second)

        # for each name
        for name in names:

            # grab arrays
            array = hydra.grab(name)
            arrayii = hydraii.grab(name)

            # get shape
            shape = array.shape

            # if shape is 2-D
            if len(shape) == 2:

                # add third trivial dimension
                array = array.reshape(*shape, 1)
                arrayii = arrayii.reshape(*shape, 1)

            # for each position
            positions = positions or range(array.shape[2])
            for position in positions:

                # print status
                self._print('examining {}, position {}...'.format(name, position))

                # calculate difference
                difference = arrayii[:, :, position] - array[:, :, position]

                # get mask for non zero differences
                mask = (difference != 0).astype(int)

                # create row and scanline arrays
                row = numpy.array([list(range(shape[1]))] * shape[0])
                scanline = numpy.array([list(range(shape[0]))] * shape[1]).transpose(1, 0)

                # specify color gradient and particular indices
                gradient = 'jet'
                selection = [index for index in range(0, 256)]

                # create destioation and plot nonzeros
                destination = '{}/plots/irradiance/{}_{}_Nonzero_differences.png'.format(self.sink, name, position)
                title = '{} index {}, Non Zero Differences'.format(name, position)
                arrays = [mask, scanline, row]
                texts = [title, '_']
                bounds = (0, shape[0], 0, shape[1])
                self._paint(arrays, destination, texts, (0, 1), [gradient, selection], bounds, bar=True)

                # create destination and plot differences
                destination = '{}/plots/irradiance/{}_{}_differences.png'.format(self.sink, name, position)
                title = '{} index {}, Differences'.format(name, position)
                arrays = [difference, scanline, row]
                texts = [title, '_']
                bounds = (0, shape[0], 0, shape[1])
                limits = (difference.min(), difference.max())
                self._paint(arrays, destination, texts, limits, [gradient, selection], bounds, bar=True)

                # if not given
                if not condition:

                    # set dummy condition
                    condition = lambda array: (array > -9999)

                # create destination and plot differences
                destination = '{}/plots/irradiance/{}_{}_subset.png'.format(self.sink, name, position)
                title = '{} index {}, Differences, Subset'.format(name, position)
                limits = (difference.min(), difference.max())
                subset = condition(difference)
                difference = numpy.where(subset, difference, -9999)
                arrays = [difference, scanline, row]
                texts = [title, '_']
                bounds = (0, shape[0], 0, shape[1])
                self._paint(arrays, destination, texts, limits, [gradient, selection], bounds, bar=True)

        return None

    def locate(self, year, month, day, orbit):
        """Check geolocatin information for Col3 and Col4 L1b.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit

        Returns:
            None
        """

        # create reports folder
        folder = '{}/reports'.format(self.sink)
        self._make(folder)

        # create col3 and col4 hydras
        stub = '{}/{}/{}'.format(year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10004/OML1BRUG/{}'.format(stub), show=False)
        hydraii = Hydra('/tis/acps/OMI/10003/OML1BRUG/{}'.format(stub), show=False)

        # ingest orbits
        hydra.ingest(str(orbit))
        hydraii.ingest(str(orbit))

        # set up col4 searches
        searches = ['BAND2/latitude', 'BAND2/longitude', 'BAND2/solar_zenith_angle']
        searches += ['BAND2/viewing_zenith_angle']

        # set up col3 searches
        searchesii = ['UV-2/Latitude', 'UV-2/Longitude', 'UV-2/SolarZenithAngle']
        searchesii += ['UV-2/ViewingZenithAngle']

        # begin report
        report = ['geolocation for {}, {}'.format(stub, orbit)]

        # for each col 4search
        for search in searches:

            # get data and add min, max to report
            array = hydra.grab(search)
            minimum = array.min()
            maximum = array.max()
            report.append(self._print('Col4: {}: {} to {}'.format(search, minimum, maximum)))

        # add spacer
        report.append('')

        # for each col3 search
        for search in searchesii:

            # get data and add min, max to report
            array = hydraii.grab(search)
            minimum = array.min()
            maximum = array.max()
            report.append(self._print('Col3: {}: {} to {}'.format(search, minimum, maximum)))

        # add to reports
        destination = '{}/geolocation_{}.txt'.format(folder, orbit)
        self._jot(report, destination)

        return None

    def mean(self, events=None, year=2005, archives=(10003, 95044), product='OMTO3d', tag='collapse'):
        """Produce a zonal monthly means of a omto3d comparison.

        Arguments:
            events: list of str, the dates in YYYYmMMDD format
            year: int, the year, if events not given
            archives: tuple of ints, the archive sets
            product: str, name of specific product
            tag: str, name of plot folder

        Returns:
            None
        """

        # make directory
        folder = '{}/{}'.format(self.sink, tag)
        self._make(folder)

        # if events not given
        if events is None:

            # create datetime instances
            instances = []

            # create initial datetime
            start = datetime.datetime(year, 1, 1)

            # for each days
            for days in range(366):

                # create new instance
                instance = start + datetime.timedelta(days=days)
                instances.append((year, self._pad(instance.month), self._pad(instance.day)))

            # create events
            events = ['{}m{}{}'.format(*instance) for instance in instances]

        # sort events
        events.sort()

        # collect zones and julian days
        julians = []
        differences = []
        parallels = []
        zones = []
        ordinate = None

        # set up ozones
        ozones = {archive: [] for archive in archives}

        # for each instance
        for event in events:

            # parse event to override random selections
            year = int(event[:4])
            month = int(event[5:7])
            day = int(event[7:9])

            # get julian day
            julian = datetime.datetime(year, month, day).timetuple().tm_yday
            julians.append(julian)

            # construct date
            date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))

            # for each archive set
            for archive in archives:

                # create hydra for first archive set
                formats = (archive, product, year, self._pad(month))
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}'.format(*formats, show=False))
                hydra.ingest('_{}_'.format(date))

                # if product is OMTO3G
                if product == 'OMTO3G':

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    quality = hydra.grab('QualityFlags')

                    # construct ozone grid from all positive ozone values
                    mask = (ozone >= 0) & (numpy.isfinite(ozone)) & (quality < 1)
                    summation = numpy.where(mask, ozone, 0).sum(axis=0)
                    samples = mask.astype(int).sum(axis=0)
                    grid = summation / samples

                    # reassign ozone
                    ozone = grid
                    ozones.append(ozone)

                # otherwise
                else:

                    # get the ozone
                    ozone = hydra.grab('ColumnAmountO3')
                    ozones[archive].append(ozone)

            # # append differences
            # difference = ozones[1] - ozones[0]

            # construct latitude and longitude
            shape = ozone.shape
            latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
            longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]

            # craete grid
            grid, gridii = numpy.meshgrid(longitudes, latitudes)

            # # construct zonal means from difference
            # fill = 1e20
            # mask = (numpy.isfinite(difference)) & (abs(difference) < fill)
            # tracer = numpy.where(mask, difference, 0)
            # number = numpy.ones(tracer.shape)
            # number = numpy.where(mask, number, 0)
            # zone = tracer.sum(axis=1) / number.sum(axis=1)
            # zones.append(zone)
            # tracer = numpy.where((tracer > scale[1]) & (abs(tracer) < fill), scale[1], tracer)
            # tracer = numpy.where((tracer < scale[0]) & (abs(tracer) < fill), scale[0], tracer)

            # set ordinate
            # abscissa = grid
            ordinate = gridii[:, 0]

        # create arrays
        ozones = {archive: numpy.array(ozones[archive]) for archive in archives}

        # create means
        means = {}

        # for each archive
        for archive in archives:

            # get stack of ozone data
            ozone = ozones[archive]

            # get index pairs, and clip to endponts
            pairs = [(index - 15, index + 15) for index in range(len(ozone))]
            pairs = [(max([0, pair[0]]), min([len(ozone), pair[1]])) for pair in pairs]

            # begin means
            mean = []

            # for each pair
            for pair in pairs:

                # create block
                block = ozone[pair[0]: pair[1]]
                mask = (block > 0)
                block = numpy.where(mask, block, 0)
                number = mask.astype(int).sum(axis=0)

                # create aveerage
                average = block.sum(axis=0) / number
                mean.append(average)

                # # create naive average
                # average = block.mean(axis=0)
                # mean.append(average)

            # create monthly means
            # mean = [ozone[pair[0]: pair[1]].mean(axis=0) for pair in pairs]
            means[archive] = numpy.array(mean).mean(axis=2)

        # create difference
        zones = means[archives[1]] - means[archives[0]]

        # set scale
        scale = (-5, 5)
        zones = numpy.array(zones).transpose(1, 0)
        zones = numpy.flip(zones, axis=0)
        print('zones: {}'.format(zones.shape))

        # clip to scale
        zones = numpy.where(zones < scale[0], scale[0], zones)
        zones = numpy.where(zones > scale[1], scale[1], zones)

        # set up color scheme, removing first two
        text = self._know('{}/colorbars/dobson_difference_colorbar.txt'.format(self.sink))
        tuples = [tuple([float(channel) for channel in line.split()]) for line in text]

        # set up color map
        colors = matplotlib.colors.ListedColormap(tuples)

        # create the heatmap
        pyplot.clf()
        # pyplot.imshow(zones, cmap='seismic', interpolation='nearest', vmin=-5, vmax=5)
        pyplot.imshow(zones, cmap=colors, interpolation='nearest', vmin=-5, vmax=5)
        pyplot.colorbar()  # Show color scale

        # Customize ticks
        ordinate = numpy.flip(ordinate, axis=0)
        labels = [-90, -60, -30, 0, 30, 60, 90]
        indices = [self._pin(label, ordinate, 1)[0][0] for label in labels]

        # set latitude ticks
        pyplot.yticks(ticks=indices, labels=labels)
        # pyplot.xticks(rotation=45)  # Rotate x-axis labels by 45 degrees if needed
        # pyplot.yticks(rotation=0)  # Keep y-axis labels horizontal

        # Add labels and title
        pyplot.xlabel('Julian Day')
        pyplot.ylabel('Latitude')
        pyplot.title('{} AS95044 - AS10003 Monthly Means, 2005'.format(product))

        # set destination
        destination = '{}/{}_zonal_monthly_diffs_2005.png'.format(folder, product)

        # save plot
        pyplot.savefig(destination)
        pyplot.clf()

        return None

    def miss(self, year=2024, month=8, day=1):
        """Map the values missing in AS95044 versus AS10004 for OMTO3G.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day

        Returns:
            None
        """

        # make the folder
        folder = '{}/plots/miss'.format(self.sink)
        self._make(folder)

        # set archives
        archives = (95044, 10004)

        # begin tuples reservoir
        reservoir = {}

        # for each archive set
        for archive in archives:

            # connect to the directories
            hydra = Hydra('/tis/acps/OMI/{}/OMTO3G/{}/{}'.format(archive, year, self._pad(month)))

            # construct date string and ingest
            date = '_{}m{}{}'.format(year, self._pad(month), self._pad(day))
            hydra.ingest(date)

            # gather up arrays and flatten
            latitudes = hydra.grab('Latitude').flatten()
            longitudes = hydra.grab('Longitude').flatten()
            orbits = hydra.grab('OrbitNumber').flatten()
            ozones = hydra.grab('ColumnAmountO3').flatten()

            # create tuples
            zipper = zip(latitudes, longitudes, orbits, ozones)
            tuples = [tuple((latitude, longitude, orbit, ozone)) for latitude, longitude, orbit, ozone in zipper]

            # add to reservoir
            reservoir[archive] = tuples

        # set pairs
        pairs = [(archives[0], archives[1]), (archives[1], archives[0])]

        # for each pair
        for archive, archiveii in pairs:

            # get missing sets
            misses = set(reservoir[archive]) - set(reservoir[archiveii])

            # make title and destination
            title = 'missing from AS{}'.format(archiveii)
            destination = '{}/omto3g_points_missing_from_{}.png'.format(folder, archiveii)

            # get points
            ordinate = [miss[0] for miss in misses]
            abscissa = [miss[1] for miss in misses]

            # print ozone
            ozone = [miss[3] for miss in misses]
            self._print('ozones: {}'.format(set(ozone)))

            # plot
            pyplot.clf()
            pyplot.title(title)
            pyplot.plot(abscissa, ordinate, 'g.')

            # save
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def nuance(self, year=2024, month=8, day=18, position=8):
        """Determine chain of nan values in OMTO3.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            position: index of three-d arrays

        Returns:
            None
        """

        # get hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/95044/OMTO3/{}/{}/{}'.format(*formats))

        # begin collection
        collection = {}

        # for each path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # get all data
            data = hydra.extract()

            # filter out strings and 1-d arrays
            data = {name: array for name, array in data.items() if 'S' not in str(array.dtype)}
            data = {name: array for name, array in data.items() if len(array.shape) > 1}

            # sum three dimensional arrays across third axis
            # threes = {name: array.sum(axis=2) for name, array in data.items() if len(array.shape) > 2}
            threes = {name: array[:, :, position] for name, array in data.items() if len(array.shape) > 2}
            data.update(threes)

            # append to collection
            [collection.setdefault(name, []).append(array) for name, array in data.items()]

        # vstack all arrays
        collection = {name: numpy.vstack(arrays) for name, arrays in collection.items()}

        # only keep those with nans
        def nuancing(array): return numpy.logical_not(numpy.isfinite(array))
        def filling(array): return (numpy.isfinite(array)) & (abs(array) > 1e20)
        collection = {name: array for name, array in collection.items() if nuancing(array).sum() > 0}

        # begin report
        report = [self._print('nans in OMTO3, {}m{}{}, index: {}'.format(*formats, position))]

        # sort names by number of nans
        names = list(collection.keys())
        names.sort(key=lambda name: nuancing(collection[name]).sum(), reverse=True)
        collection = {name: collection[name] for name in names}

        # for each array with nans
        for name, array in collection.items():

            # get all nan pixels
            mask = nuancing(array)

            # add report entry
            report.append(self._print(''))
            report.append(self._print('{} nans in {}:'.format(mask.sum(), name)))

            # for each array in collection
            for nameii, arrayii in collection.items():

                # determine percent nans amongst pixels
                percent = 100 * nuancing(arrayii[mask]).sum() / mask.sum()
                percent = self._round(percent, 2)
                report.append(self._print('   {} % also nans in {}'.format(percent, nameii)))
                #
                # # determie percent fills amongst pixels
                # percent = 100 * filling(arrayii[mask]).sum() / mask.sum()
                # percent = self._round(percent, 2)
                # report.append(self._print('       {} % fills in {}'.format(percent, nameii)))

        # jot report
        self._jot(report, '{}/reports/Nan_report_OMTO3_{}m{}{}_{}.txt'.format(self.sink, *formats, position))

        return None

    def overwrite(self, path, irradiance, distance):
        """Overwrite hdf4 irradiance data with external mantissa and exponent.

        Arguments:
            path: str, file path
            irradiance: numpy array, irradiance
            distance: float or numpy array, earth sun distance

        Returns:
            None
        """

        # decompose irradiance into manitissa, exponent
        mantissa, exponent = self._decompose(irradiance, distance)

        # identify groups by shape
        groups = {159: ['Sun Volume UV-1 Swath', 'Data Fields']}
        groups.update({557: ['Sun Volume UV-2 Swath', 'Data Fields']})
        groups.update({751: ['Sun Volume VIS Swath', 'Data Fields']})

        # grap appropriate group
        group = groups[mantissa.shape[-1]]

        # insert mantissa
        name = 'IrradianceMantissa'
        self._print('inserting {} into {}...'.format(name, group))
        self._insert(mantissa, path, name, group)

        # insert exopnent
        name = 'IrradianceExponent'
        self._print('inserting {} into {}...'.format(name, group))
        self._insert(exponent, path, name, group)

        return None

    def predict(self, target='ColumnAmountO3', exclusions=None):
        """Use random forest to predict omto3 errors.

        Arguments:
            target: str, name of target
            exclusions: list of str, parameters to be excluded

        Returns:
            None
        """

        # default exclusions
        exclusions = exclusions or []

        # get originat dataset
        hydra = Hydra('../studies/omto3v8/reader')
        hydra.ingest(1)

        # get secondary dataset
        hydraii = Hydra('../studies/omto3v8/reader')
        hydraii.ingest(0)

        # split features into groups by dimension
        ones = [feature for feature in hydra if len(feature.shape) == 1]
        onesii = [feature for feature in hydraii if len(feature.shape) == 1]
        twos = [feature for feature in hydra if len(feature.shape) == 2]
        twosii = [feature for feature in hydraii if len(feature.shape) == 2]
        threes = [feature for feature in hydra if len(feature.shape) == 3]
        threesii = [feature for feature in hydraii if len(feature.shape) == 3]

        # determine average 2-d shape
        shapes = [feature.shape for feature in twos]
        counter = Counter(shapes)
        pairs = list(counter.items())
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        shape = pairs[0][0]

        # restrict features to matching shapes
        ones = [feature for feature in ones if feature.shape == shape[:1]]
        onesii = [feature for feature in onesii if feature.shape == shape[:1]]
        twos = [feature for feature in twos if feature.shape == shape]
        twosii = [feature for feature in twosii if feature.shape == shape]
        threes = [feature for feature in threes if feature.shape[:2] == shape]
        threesii = [feature for feature in threesii if feature.shape[:2] == shape]

        # create data from 2-D data
        data = {feature.name: feature.distil() for feature in twos}
        dataii = {feature.name: feature.distil() for feature in twosii}

        # add 1-D features
        def blocking(array): return numpy.array([array] * shape[1]).transpose(1, 0)
        data.update({feature.name: blocking(feature.distil()) for feature in ones})
        dataii.update({feature.name: blocking(feature.distil()) for feature in onesii})

        # for each 3d feature
        for feature, featureii in zip(threes, threesii):

            # get array
            array = feature.distil()
            arrayii = featureii.distil()

            # for each position
            for position in range(array.shape[2]):

                # add feature
                formats = (feature.name, self._pad(position))
                data.update({'{}_{}'.format(*formats): array[:, :, position]})
                dataii.update({'{}_{}'.format(*formats): arrayii[:, :, position]})

        # compute differences
        differences = {}
        for name in data.keys():

            # calculate difference
            differences[name] = self._relate(data[name], dataii[name])

        # get all masks where difference is finite
        masks = {name: numpy.isfinite(array) for name, array in differences.items()}

        # combine all masks
        masque = numpy.ones(shape).astype(bool)
        for mask in masks.values():

            # combine mask
            masque = numpy.logical_and(masque, mask)

        # apply masks
        finites = {name: difference[masque] for name, difference in differences.items()}

        # assemble matrix
        names = [name for name in finites.keys() if name not in exclusions + [target]] + [target]
        train = numpy.array([finites[name].flatten() for name in names]).transpose(1, 0)

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # begin report
        report = ['Random Forest predicting difference values for  {}'.format(target)]
        report.append('in file: {}/'.format(self._fold(hydra.current)))
        report.append('{}'.format(self._file(hydra.current)))
        report.append('')
        report.append('in file: {}/'.format(self._fold(hydraii.current)))
        report.append('{}'.format(self._file(hydraii.current)))
        report.append('')

        # run random forest
        self._stamp('running random forest for differences in {}...'.format(target), initial=True)
        self._print('matrix: {}'.format(matrix.shape))
        forest = RandomForestRegressor(n_estimators=100, max_depth=5)
        forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(names, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, target)
        self._jot(report, destination)

        # print status
        self._stamp('planted.')

        return None

    def qualify(self, year=2024, month=1, day=5, orbit=103585, point=(60, 50), extent=5):
        """Find the quality flags for all points within a certain box.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit number
            point: tuple of float, the latitude and longitude
            extent: float, number of degrees from center

        Returns:
            None
        """

        # make folder
        self._make('{}/reports'.format(self.sink))

        # grab codes
        codes = self._acquire('{}/codes/codes_V8.yaml'.format(self.sink))

        # create hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(*formats))

        # ingest orbit
        date = '{}m{}{}'.format(*formats)
        hydra.ingest(str(orbit))

        # grab data
        latitudes = hydra.grab('Latitude')
        longitudes = hydra.grab('Longitude')
        ozones = hydra.grab('ColumnAmountO3')
        qualities = hydra.grab('QualityFlags')
        algorithms = hydra.grab('AlgorithmFlags')
        grounds = hydra.grab('GroundPixelQuality')

        # create mask around point
        mask = (latitudes >= point[0] - extent) & (latitudes <= point[0] + extent)
        maskii = (longitudes >= point[1] - extent) & (longitudes <= point[1] + extent)
        masque = mask & maskii

        # begin report
        report = ['OMTO3 Quality Report', 'orbit: {}'.format(orbit), 'date: {}'.format(date)]
        report += ['lat/lon: {} +/- {}'.format(point, extent), '']

        # make zipper
        arrays = [latitudes, longitudes, ozones, qualities, algorithms, grounds]
        zipper = zip(*[array[masque] for array in arrays])

        # get length of report so far for later insertion
        insertion = len(report)

        # begin total bits
        combinations = []
        total = []

        # for each entry
        for latitude, longitude, ozone, quality, algorithm, ground in zipper:

            # exclude row anomay
            if (quality & 64) == 0:

                # add to report lat and lon
                report.append('lat: {}, lon: {}:'.format(latitude, longitude))
                report.append('ozone: {}'.format(ozone))

                # get bits
                bits = hydra._bite(quality)
                combination = hydra._unite(*[bit for bit in bits if bit < 3])
                bits = [bit for bit in bits if bit >= 3]

                # add to total
                combinations.append(combination)
                total += bits

                # report combo bits
                report.append('combo: {}, {}'.format(combination, codes['quality']['combo'][combination]))

                # report quality bits
                report.append('quality: {}, {}'.format(bits, [codes['quality']['bit'][bit] for bit in bits]))

                # report algorithm flag
                report.append('algorithm: {}, {}'.format(algorithm, codes['algorithm'][algorithm]))

                # report ground quality
                bits = hydra._bite(ground)
                report.append('ground: {}, {}'.format(bits, [codes['ground'][bit] for bit in bits]))

                # add spacer
                report.append('')

        # begin totals block
        block = []

        # count combos
        counter = list(Counter(combinations).items())
        counter.sort(key=lambda pair: pair[1], reverse=True)
        for bit, count in counter:

            # append
            block.append('{} ({}): {}'.format(bit, count, codes['quality']['combo'][bit]))

        # add spacer
        block += ['']

        # count combos
        counter = list(Counter(total).items())
        counter.sort(key=lambda pair: pair[1], reverse=True)
        for bit, count in counter:

            # append
            block.append('{} ({}): {}'.format(bit, count, codes['quality']['bit'][bit]))

        # insert block
        block += ['']
        report = report[:insertion] + block + report[insertion:]

        # create destination
        formats = (self.sink, date, orbit, self._orient(point[0]), self._orient(point[1], east=True))
        destination = '{}/reports/Quality_{}_{}_{}_{}.txt'.format(*formats)
        self._jot(report, destination)

        return None

    def radiate(self, event='2006m0801', orbit=10886, tag='radiance', parallels=(30, 35), row=30, wave=360, give=False):
        """Check radiance differences between col3 and col4.

        Arguments:
            event: str, date in YYYYmMMDD format
            orbit: int, the orbit number
            tag: str, tag for plots folder
            parallels: tuple of floats, latitudes for zonal means
            row: int, the row of interest
            wave: float, the wavelength of interest
            give: boolean, output data

        Returns:
            None
        """

        # make directory
        folder = '{}/plots/{}'.format(self.sink, tag)
        self._make(folder)

        # set archive sets
        archives = [10003, 95044]

        # parse event to override random selections
        year = int(event[:4])
        month = int(event[5:7])
        day = int(event[7:9])

        # create col3 l1b hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OML1BRUG/{}/{}/{}'.format(archives[0], *formats))
        hydra.ingest(str(orbit))

        # grab data
        exponent = hydra.grab('UV-2/RadianceExponent').squeeze()
        mantissa = hydra.grab('UV-2/RadianceMantissa').squeeze()
        distance = hydra.grab('UV-2/EarthSunDistance').squeeze()
        coefficient = hydra.grab('UV-2/WavelengthCoefficient').squeeze()
        reference = hydra.grab('UV-2/WavelengthReferenceColumn').squeeze()

        # calculate wavelengths
        wavelength = self._calculate(coefficient, reference, 557)
        wavelength = wavelength.mean(axis=0)

        # set conversion factors for photons and earth sun distance
        factor = 6.02214076e23 / 10000
        unit = 149597870700

        # calculate radiance
        radiance = mantissa * 10.0 ** exponent
        radiance = radiance / factor
        radiance = radiance / ((distance / unit) ** 2)

        # create col4 l1b hydra
        formats = (year, self._pad(month), self._pad(day))
        hydraii = Hydra('/tis/acps/OMI/{}/OML1BRUG/{}/{}/{}'.format(archives[1], *formats))
        hydraii.ingest(str(orbit))

        # grab data
        radianceii = hydraii.grab('BAND2/radiance').squeeze()
        wavelengthii = hydraii.grab('BAND2/wavelength').squeeze()
        latitude = hydraii.grab('BAND2/latitude').squeeze()
        longitude = hydraii.grab('BAND2/longitude').squeeze()

        # get start and ending scanlines
        start = hydra._pin(parallels[0], latitude[:, 30])[0][0]
        finish = hydra._pin(parallels[1], latitude[:, 30])[0][0]

        # get pixel
        pixels = [hydra._pin(wave, array[row])[0][0] for array in (wavelength, wavelengthii)]

        # for each scan
        for scan in range(start, finish):

            # print latitude
            print('')
            print('latitude: {}'.format(latitude[scan, 30]))

            # print radiances
            print('col3: {}'.format(radiance[scan, row - 2: row + 3, pixels[0]]))
            print('col4: {}'.format(radianceii[scan, row - 2: row + 3, pixels[1]]))

        # create means
        mean = radiance[start: finish, :, pixels[0]].mean(axis=0)
        meanii = radianceii[start: finish, :, pixels[1]].mean(axis=0)
        means = [mean, meanii]

        # set rows
        rows = numpy.array(range(60))

        # for each mean
        for archive, mean in zip(archives, means):

            # create plot
            degrees = (self._orient(parallels[0]), self._orient(parallels[1]))
            formats = (archive, event, orbit, *degrees)
            title = 'Ozone, AS{}, {}, o{}, {} to {}'.format(*formats)
            address = 'plots/{}/omto3_{}_{}_{}_as{}.h5'.format(tag, event, *degrees, archive)
            self.squid.ink('radiance', mean, 'row', rows, address, title)

        # if giving output
        package = None
        if give:

            # set up package
            package = {'radiance': radiance, 'radianceii': radianceii}
            package.update({'wavelength': wavelength, 'wavelengthii': wavelengthii})

        return package

    def rationalize(self, years=(2020, 2005), month=4, day=4, rows=(10, 12, 14, 16, 50, 58), sink='ratios'):
        """Plot irradiance ratios on a particular day.

        Arguments:
            years: tuple of ints, the years
            month: int, month
            day: int, day
            orbit: int, orbit number
            rows: tuple of ints, the band2 rows to plot

        Returns:
            None
        """

        # create plot folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # get first solar
        formats = (years[0], self._pad(month), self._pad(day))
        solar = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}/{}'.format(*formats))
        solar.ingest()

        # get second solar
        formats = (years[1], self._pad(month), self._pad(day))
        solarii = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}/{}'.format(*formats))
        solarii.ingest()

        # set up band specifics
        factors = {'BAND1': 2, 'BAND2': 1, 'BAND3': 1}

        # for each band
        for band, factor in factors.items():

            # get band 1 reflectance at halved rows
            rowsii = [int(row / factor) for row in rows]
            irradiance = solar.grab('{}/irradiance'.format(band)).squeeze()[rowsii, :]
            irradianceii = solarii.grab('{}/irradiance'.format(band)).squeeze()[rowsii, :]
            wavelength = solar.grab('{}/wavelength'.format(band)).squeeze()[rowsii, :]

            # calculate ratio
            ratio = irradiance / irradianceii

            # restrict to latitude band
            mask = numpy.isfinite(ratio)
            ratio = ratio[mask]
            wavelength = wavelength[mask]

            # plot band1 vs wavelength
            formats = (sink, band, *years, self._pad(month), self._pad(day))
            address = 'plots/{}/{}_{}_{}_irradiance_ratio_m{}{}.h5'.format(*formats)
            formatsii = (band, *years, self._pad(month), self._pad(day), rowsii)
            title = '{} {} / {} irradiance ratio, {}-{}, rows: {}'.format(*formatsii)
            self.squid.ink('ratio', ratio, 'wavelength', wavelength, address, title)

        return None

    def reflect(self, year=2005, month=4, day=4, orbit=3827, rows=(10, 12), latitudes=(-5, 5), sink='ratios'):
        """Plot reflectances across all bands for particular rows on a particular orbit.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: int, orbit number
            rows: tuple of ints, the band2 rows to plot

        Returns:
            None
        """

        # create plot folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # get hydra for radiance file, UV
        formats = (year, self._pad(month), self._pad(day))
        ultraviolet = Hydra('/tis/acps/OMI/10004/OML1BRUG/{}/{}/{}'.format(*formats))
        ultraviolet.ingest('o{}'.format(self._pad(orbit, 6)))

        # get hydra for radiance file, visible
        visible = Hydra('/tis/acps/OMI/10004/OML1BRVG/{}/{}/{}'.format(*formats))
        visible.ingest('o{}'.format(self._pad(orbit, 6)))

        # make hydra for irradiance
        solar = Hydra('/tis/acps/OMI/10004/OML1BIRR/{}/{}/{}'.format(*formats))
        solar.ingest()

        # set up band specifics
        hydras = {'BAND1': ultraviolet, 'BAND2': ultraviolet, 'BAND3': visible}
        factors = {'BAND1': 2, 'BAND2': 1, 'BAND3': 1}

        # for each band
        for band, hydra in hydras.items():

            # get band 1 reflectance at halved rows
            rowsii = [int(row / factors[band]) for row in rows]
            radiance = hydra.grab('{}/radiance'.format(band)).squeeze()[:, rowsii, :]
            wavelength = hydra.grab('{}/wavelength'.format(band)).squeeze()[rowsii, :]
            irradiance = solar.grab('{}/irradiance'.format(band)).squeeze()[rowsii, :]
            irradiance = numpy.array([irradiance] * radiance.shape[0])
            latitude = hydra.grab('latitude').squeeze()[:, rowsii]
            reflectance = radiance / irradiance
            wavelength = numpy.array([wavelength] * radiance.shape[0])

            # restrict to latitude band
            mask = (latitude > latitudes[0]) & (latitude < latitudes[1])
            reflectance = reflectance[mask]
            wavelength = wavelength[mask]

            # average actoss scanlines
            reflectance = reflectance.mean(axis=0)
            wavelength = wavelength.mean(axis=0)

            # remove infinities
            maskii = numpy.isfinite(reflectance)
            reflectance = reflectance[maskii]
            wavelength = wavelength[maskii]

            # plot band1 vs wavelength
            address = 'plots/{}/{}_reflectance_{}m{}{}_o{}.h5'.format(sink, band, *formats, self._pad(orbit, 6))
            formatsii = (band, *formats, orbit, *rowsii, self._orient(latitudes[0]), self._orient(latitudes[1]))
            title = '{} radiance / irradiance, {}-{}-{}, orbit {}, rows {}, {}, latitude {} to {}'.format(*formatsii)
            self.squid.ink('ratio', reflectance, 'wavelength', wavelength, address, title)

        return None

    def row(self, event='2006m0801', archives=(10003, 95044), tag='glint', parallels=(30, 35), meridian=-100, row=30):
        """Sketch row differences between OMTO3 files.

        Arguments:
            event: str, date in YYYYmMMDD format
            archives: tuple of ints, the archive sets
            tag: str, tag for plots folder
            parallels: tuple of floats, latitudes for zonal means
            meridian: float, longitude for zonal means
            row: int, the row of interest along track

        Returns:
            None
        """

        # set product
        product = 'OMTO3'

        # unpack parallels
        south, north = parallels

        # make directory
        folder = '{}/plots/{}'.format(self.sink, tag)
        self._make(folder)

        # parse event to override random selections
        year = int(event[:4])
        month = int(event[5:7])
        day = int(event[7:9])

        # set fields
        fields = {'ozone': 'ColumnAmountO3', 'value': 'NValue', 'one': 'StepOneO3', 'two': 'StepTwoO3'}
        fields.update({'quality': 'QualityFlags', 'latitude': 'Latitude', 'longitude': 'Longitude'})
        fields.update({'cloud': 'CloudPressure', 'below': 'O3BelowCloud'})

        # set tranforms to extract 360nm
        transforms = {'value': lambda array: array[:, :, 10]}

        # set mean reservoir and tracks reservoir
        means = {field: [] for field in fields.keys()}
        tracks = {field: [] for field in fields.keys()}

        # set orbit
        orbit = ''

        # try to
        try:

            # for each archive set
            for archive in archives:

                # create hydra for first archive set
                formats = (archive, product, year, self._pad(month), self._pad(day))
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}/{}'.format(*formats))

                # set up accumulation
                accumulations = {field: [] for field in fields.keys()}

                # for each path
                for path in hydra.paths:

                    # ingest the path
                    hydra.ingest(path)

                    # grab data
                    data = {field: hydra.grab(search) for field, search in fields.items()}
                    data.update({field: transform(data[field]) for field, transform in transforms.items()})

                    # grab longitudes
                    longitude = data['longitude']
                    latitude = data['latitude']
                    position = hydra._pin(north, latitude[:, 59])[0][0]
                    bracket = (longitude[position, 0], longitude[position, 59])
                    if (bracket[0] < meridian) and (bracket[1] > meridian):

                        # print path
                        self._print('using orbit {}...'.format(path))
                        self._print('{} to {}'.format(*bracket))

                        # set orbit
                        orbit = hydra._stage(path)['orbit']

                        # append arrays
                        [accumulation.append(data[field]) for field, accumulation in accumulations.items()]

                # create stacks from last member
                stacks = {field: numpy.vstack(accumulation[-1:]) for field, accumulation in accumulations.items()}

                # for each stack
                for field, array in stacks.items():

                    # zero nonvalid entries
                    mask = (stacks['latitude'] >= south) & (stacks['latitude'] <= north)
                    mask = mask & (stacks['quality'] < 2) & numpy.isfinite(stacks[field])
                    mask = mask & (abs(stacks[field] < 1e20))
                    valid = numpy.where(mask, array, 0).sum(axis=0)

                    # create mean
                    number = mask.astype(int).sum(axis=0)
                    mean = valid / number
                    means[field].append(mean)

                    # add tracks at given row
                    tracks[field].append(array[:, row])

            # set up differences
            differences = {field: mean[1] - mean[0] for field, mean in means.items()}
            differencesii = {field: track[1] - track[0] for field, track in tracks.items()}

            # set up arrays
            reservoir = {field: means[field] + [differences[field]] for field in fields.keys()}
            reservoirii = {field: tracks[field] + [differencesii[field]] for field in fields.keys()}

            # set up stubs and modes
            modes = ['AS{} vs AS{}'.format(*archives)] * 2
            modes += ['AS{} - AS{}'.format(archives[1], archives[0])]
            stubs = ['col3', 'col4', 'diff']

            # for each field
            for field, arrays in reservoir.items():

                # get secondary arrays
                arraysii = reservoirii[field]

                # for each array
                for array, arrayii, mode, stub in zip(arrays, arraysii, modes, stubs):

                    # create mask
                    mask = numpy.isfinite(array)

                    # create plot
                    degrees = (self._orient(meridian, east=True), self._orient(south), self._orient(north))
                    formats = (fields[field], product, mode, event, orbit, *degrees)
                    title = '{}, {}, {}, {}, o{}, {}, {} to {}'.format(*formats)
                    address = 'plots/{}/{}_{}_{}_{}_{}_{}.h5'.format(tag, field, event, *degrees, stub)
                    self.squid.ink(field, array[mask], 'row', numpy.array(range(60))[mask], address, title)

                    # create mask
                    mask = numpy.isfinite(arrayii) & (abs(arrayii) < 1e20)
                    latitude = reservoirii['latitude'][0]

                    # create plot
                    degrees = (self._orient(meridian, east=True), self._orient(south), self._orient(north))
                    formats = (fields[field], product, mode, event, orbit, *degrees, row)
                    title = '{}, {}, {}, {}, o{}, {}, {} to {}, Row {}'.format(*formats)
                    address = 'plots/{}/{}_{}_{}_{}_{}_{}_{}.h5'.format(tag, field, event, *degrees, stub, row)
                    self.squid.ink(field, arrayii[mask], 'latitude', latitude[mask], address, title)

            # set
            names = ['scatter_cloud', 'scatter_diff']
            ordinates = [reservoirii['ozone'][2], reservoirii['ozone'][2]]
            abscissas = [reservoirii['below'][0], reservoirii['below'][2]]

            # for each
            for name, abscissa, ordinate in zip(names, abscissas, ordinates):

                # create mask
                mask = numpy.isfinite(abscissa) & numpy.isfinite(ordinate)
                mask = mask & (abs(abscissa) < 1e20) & (abs(ordinate) < 1e20)

                # create scatter plots of col3 cloud pressure vs diff
                degrees = (self._orient(meridian, east=True), self._orient(south), self._orient(north))
                formats = (name, product, event, orbit, *degrees, row)
                title = '{}, {}, {}, o{}, {}, {} to {}, Row {}'.format(*formats)
                address = 'plots/{}/{}_{}_{}_{}_{}_{}_{}.h5'.format(tag, name, event, *degrees, stub, row)
                self.squid.ink('ozone_diff', ordinate[mask], name, abscissa[mask], address, title)

        # unless not found
        except ValueError:

            # print alert
            self._print('no latitude found')

        return None

    def scatter(self, orbit='100172', wave=317, limit=15, limitii=84, rows=(0, 20), sourceii='hybrid'):
        """Compare two OMTO3 files, making scatter plots.

        Arguments:
            orbit: str, orbit number
            rows: tuple of ints, the rows to use
            sink: str, folder for dumping plots
            wave: int, wavelength
            limit: float, reflectivity331 limit
            limitii: float, solar zenith angle limit

        Returns:
            None
        """

        # set defaults
        source = 'three'
        sink = 'plots/scatters'

        # set names
        names = {'three': 'Col3', 'four': 'Col4', 'hybrid': 'Col4L1B+Col3 OMCLDRR'}

        # set reflectivity limits
        limits = {}
        if limit < 100:

            # set limit
            limits[limit] = 'R331 < {}'.format(round(limit / 100, 2))

        # otherwise
        else:

            # set all limits
            limits[limit] = 'All R331'

        # set zenith limits
        limitsii = {}
        if limitii < 90:

            # set limit
            limitsii[limitii] = ', SZA < {}'.format(limitii)

        # otherwise
        else:

            # set all limits
            limitsii[limitii] = ''

        # set fields
        fields = {}
        fields.update({'value': 'NValue', 'reflectivity': 'Reflectivity331'})
        fields.update({'step': 'StepOneO3', 'ozone': 'ColumnAmountO3'})
        fields.update({'cloud': 'CloudPressure', 'correction': 'CalibrationAdjustment'})
        fields.update({'fraction': 'CloudFraction'})

        # create hydras
        hydra = Hydra('{}/{}'.format(self.sink, source))
        hydraii = Hydra('{}/{}'.format(self.sink, sourceii))

        # ingest paths
        hydra.ingest('o' + orbit)
        hydraii.ingest('o' + orbit)

        # grab latitudes
        latitude = hydra.grab('Latitude')
        wavelength = hydra.grab('Wavelength')
        reflectivity = hydra.grab('Reflectivity331')
        zenith = hydra.grab('SolarZenithAngle')

        # grab quality flags and unpack bits
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')

        # unpack bits and get bit 7 for cloud pressure source
        bits = numpy.unpackbits(quality.astype('uint8')).reshape(*quality.shape, -1)[:, :, 0]
        bitsii = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 0]

        # get indices for waves
        position = self._pin(wave, wavelength)[0][0]

        # unpack rows
        first, second = rows

        # for each field
        for field, search in fields.items():

            # get mask for reflectivity limit
            mask = (reflectivity[:, first:second] < limit) & (zenith[:, first:second] < limitii)

            # get data
            datum = hydra.grab(search)
            datumii = hydraii.grab(search)

            # get date
            date = self._stage(hydra.current)['day']

            # get difference
            difference = datumii - datum

            # create absiccsas from latitude and zenith
            abscissa = latitude
            dependent = 'latitude'
            abscissaii = zenith
            dependentii = 'solar zenith angle'

            # adjust for 3-D
            if len(difference.shape) > 2:

                # extend abscissa by 12
                #abscissa = numpy.array([latitude] * 12).transpose(1, 2, 0)
                difference = difference[:, first:second, position]
                difference = difference[mask]
                abscissa = abscissa[:, first:second]
                abscissa = abscissa[mask]
                abscissaii = abscissaii[:, first:second]
                abscissaii = abscissaii[mask]
                search = search + '_{}'.format(wave)
                field = field + '_{}'.format(wave)

            # adjust for calibration
            elif difference.shape[0] < 1000:

                # extend abscissa by 12
                difference = difference[first:second, :]
                abscissa = numpy.array([list(range(difference.shape[0]))] * 12).transpose(1, 0)
                abscissaii = numpy.array([list(range(difference.shape[0]))] * 12).transpose(1, 0)
                dependent = 'row'
                dependentii = 'row'

            # otherwise, adjust for rows 1-20
            else:

                # adjust
                difference = difference[:, first:second]
                difference = difference[mask]
                abscissa = abscissa[:, first:second]
                abscissa = abscissa[mask]
                abscissaii = abscissaii[:, first:second]
                abscissaii = abscissaii[mask]

            # only keep finites
            maskii = (numpy.isfinite(difference)) & (abs(difference) < 1e10)
            difference = difference[maskii]
            abscissa = abscissa[maskii]
            abscissaii = abscissaii[maskii]

            # make plot
            formats = [search, names[sourceii], names[source], orbit, date, first + 1, second]
            formats += [limits[limit], limitsii[limitii]]
            address = '{}/{}_OMTO3v8_differences_latitude.h5'.format(sink, field)
            title = '{} Differences, OMTO3 v8 {} - {}, Orbit {}, {}, Rows {}-{}, {} {}'
            title = title.format(*formats)
            self.squid.ink('{}_difference'.format(search.lower()), difference, dependent, abscissa, address, title)

            # if cloud pressure
            if field == 'cloud':

                # get cloud flag difference
                differenceii = bitsii.astype(int) - bits.astype(int)
                differenceii = differenceii[:, first:second][mask][maskii]

                # plot
                formats = [search, names[sourceii], names[source], orbit, date, first + 1, second]
                formats += [limits[limit], limitsii[limitii]]
                address = '{}/cloud_source_OMTO3v8_differences_latitude.h5'.format(sink, field)
                title = '{} Differences, OMTO3 v8 {} - {}, vs Cloud Source, Orbit {}, {}, Rows {}-{}, {} {}'
                title = title.format(*formats)
                self.squid.ink('cloud_source', differenceii, field, difference, address, title)

            # # make plot for zenith
            # formats = (search, orbit, date, first + 1, second, limit)
            # address = '{}/{}_OMTO3v8_differences_zenith.h5'.format(sink, field)
            # title = '{} Differences by SZA, OMTO3 v8 Col4 - Col3, Orbit {}, {}, Rows {}-{}, R331 < 0.{}'
            # title = title.format(*formats)
            # self.squid.ink('{}_difference'.format(search.lower()), difference, dependentii, abscissaii, address, title)

        return None

    def scour(self, year=2005, month=3, day=22, archive=96100):
        """Scour omto3 orbits for quality bit 6, fatal error.  Also run random forest.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            archive: int, archive set

        Returns:
            None
        """

        # make reports directory
        self._make('{}/reports'.format(self.sink))
        self._make('{}/plots/fatal'.format(self.sink))

        # get hydra
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))

        # begin report
        report = ['Instances of quality flag 6, fatal error, in OMTO3', hydra.source, '']

        # set points
        points = []

        # for each path
        for path in hydra.paths:

            # ingest
            self._print('checking {}...'.format(path))
            hydra.ingest(path)

            # get orbit
            orbit = self._stage(path)['orbit']
            date = self._stage(path)['day']

            # get data
            quality = hydra.grab('QualityFlags')
            algorithm = hydra.grab('AlgorithmFlags')
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')
            ozone = hydra.grab('ColumnAmountO3')
            one = hydra.grab('StepOneO3')
            reflectivity = hydra.grab('Reflectivity331')
            reflectivityii = hydra.grab('Reflectivity354')
            residual = hydra.grab('ResidualStep1')
            residualii = hydra.grab('ResidualStep2')
            reflectance = hydra.grab('NValue')

            # create counter for quality flags
            counter = Counter(quality.flatten().tolist())
            number = counter.get(6, 0)

            # get all pixels
            pixels = self._pin(6, quality, number=number)

            # for each pixel
            for pixel in pixels:

                # add spacer
                report.append('')

                # create line and add to report
                formats = [orbit, pixel] + [array[pixel] for array in [quality, latitude, longitude, algorithm, ozone]]
                line = 'orbit: {}, {}, qual: {}, lat: {}, lon: {}, alg: {}, oz: {}'.format(*formats)
                report.append(line)

                # add secondary data
                formats = [one[pixel], reflectivity[pixel], reflectivityii[pixel]]
                line = '     step1: {}, refl331: {}, refl354: {}'.format(*formats)
                report.append(line)

                # add nvalues
                formats = [reflectance[pixel]]
                line = '    nvalues: {}'.format(*formats)
                report.append(line)

                # add residual 1
                formats = [residual[pixel]]
                line = '    residual step 1: {}'.format(*formats)
                report.append(line)

                # add residual 2
                formats = [residualii[pixel]]
                line = '    residual step 2: {}'.format(*formats)
                report.append(line)

                # add to points
                point = (latitude[pixel], longitude[pixel])
                points.append(point)

        # jot report
        destination = '{}/reports/Quality_6_report_{}.txt'.format(self.sink, date)
        self._jot(report, destination)

        # set up points map
        latitude = numpy.array([point[0] for point in points])
        longitude = numpy.array([point[1] for point in points])
        quality = numpy.array([6 for point in points])

        # specify color gradient and particular indices
        gradient = 'rainbow_r'
        selection = [index for index in range(0, 256)]

        # plot orbits
        destination = '{}/plots/fatal/Fatals_{}.png'.format(self.sink, date)
        self._stamp('plotting {}...'.format(destination), initial=True)
        title = 'Quality Value 6, Fatal, OMTO3, AS {}, {}'.format(archive, date)
        arrays = [quality, latitude, longitude]
        texts = [title, '_']
        parameters = {'arrays': arrays, 'path': destination, 'texts': texts, 'scale': (5, 7)}
        parameters.update({'spectrum': [gradient, selection], 'limits': (-90, 90, -180, 180), 'grid': 1})
        parameters.update({'log': False, 'bar': False, 'back': 'white', 'points': True})
        self._paint(**parameters)
        self._stamp('plotted.')

        # assemble data
        data = self.assemble(hydra.paths)

        # make target
        target = 'quality_flag_six'
        six = ((data['QualityFlags'] & 15) == 6).astype(int)
        data[target] = six
        self._print('number of quaity 6 pixels: {}'.format(six.sum()))

        # exclude quality flags
        exclusions = ['QualityFlags']
        data = {name: array for name, array in data.items() if not any([exclusion in name for exclusion in exclusions])}

        # create masks for finites
        masks = [numpy.isfinite(array) for array in data.values()]
        masque = numpy.ones(masks[0].shape)
        for mask in masks:

            # apply mask
            masque = numpy.logical_and(masque, mask)

        # apply masque
        data.update({name: numpy.where(masque, array, 1e20) for name, array in data.items() if name != target})

        # set destination
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        destination = '{}/reports/random_forest_{}_{}.txt'.format(self.sink, target, date)

        # run forest
        self.plant(data, target, destination, classify=True)

        return None

    def scrounge(self, path):
        """Dig up the nested metadata fields and attributes.

        Arguments:
            path: str, filepath

        Returns:
            None
        """

        # create hydra
        hydra = Hydra(path)

        # create netcdf4 interface
        with hydra.nether(path) as net:

            # scrounge for all metadata
            self._seek(net)

        return None

    def scrutinize(self, north=60):
        """Scrutinize aerosol data for row anomaly effects.

        Arguments:
            None

        Returns:
            None
        """

        # grab OMTO3 data
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3/2023/09/24')

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths:

            # ingest
            hydra.ingest(path)

            # grab lattitude and aerosol
            aerosol = hydra.grab('UVAerosolIndex')
            latitude = hydra.grab('Latitude')
            longitude = hydra.grab('Longitude')
            quality = hydra.grab('QualityFlags')

            # replace nans and fills
            aerosol = numpy.where(numpy.isfinite(aerosol), aerosol, -9999)
            aerosol = numpy.where(abs(aerosol) < 1e20, aerosol, -9999)
            aerosol = numpy.where((quality == 64) == 64, -9999, aerosol)

            # find track closest to north
            track = hydra._pin(north, latitude[:, 30])[0][0]

            # find closest longitude
            east = longitude[track, 30]

            # make rows
            rows = numpy.array(range(60))

            # get orbit and date
            orbit = self._stage(path)['orbit']
            date = self._stage(path)['day']

            # plot aerosol index
            formats = (orbit, date, self._orient(north), self._orient(east, east=True))
            title = 'OMTO3 UVAerosol Index, orbit {}, {}, latitude: {}, longitude: {}'.format(*formats)
            address = 'plots/anomaly/Aerosol_Anomaly_{}.h5'.format(orbit)
            self.squid.ink('uv_aerosol_index', aerosol[track], 'row', rows, address, title)

        return None

    def seed(self, first='prior.nc', second='reader.nc', target='ResidualStep1', position=11):
        """Use random forest to predict residual differences between reader versions.

        Arguments:
            first: str, tag for first file
            second: str, tag for second file
            target: str, name of target
            position: int, slice for three-d arrays

        Returns:
            None
        """

        # create hydras
        hydra = Hydra('{}/output'.format(self.sink))
        hydraii = Hydra('{}/output'.format(self.sink))

        # ingest data
        hydra.ingest(first)
        hydraii.ingest(second)

        # get all data
        data = hydra.extract()
        dataii = hydraii.extract()

        # get target differences
        difference = dataii[target] - data[target]
        difference = difference[:, :, position]

        # update target to difference
        targetii = '{}_difference'.format(target)

        # create training dataset from original data
        train = {name: array for name, array in data.items() if array.shape == difference.shape}
        train.update({targetii : difference})

        # add 3-d arrays
        threes = [name for name in data.keys() if data[name].shape[:2] == data[target].shape[:2]]
        threes = [name for name in threes if len(data[name].shape) == 3]
        for name in threes:

            # update for all positions
            train.update({'{}_{}'.format(name, index): data[name][:, :, index] for index in range(data[name].shape[2])})

        # construct masks for finite and non files
        masks = {name: (abs(array) < 1e20) & (numpy.isfinite(array)) for name, array in train.items()}

        # get list of masks greater than 80%
        valids = [name for name, array in masks.items() if array.sum() / numpy.prod(array.shape) > 0.8]

        # import on data
        train = {name: array for name, array in train.items() if name in valids}
        masks = {name: array for name, array in masks.items() if name in valids}

        # add masques
        masque = numpy.ones(difference.shape).astype(bool)
        for mask in masks.values():

            # add to masque
            masque = numpy.logical_and(mask, masque)

        # apply masque
        train = {name: array[masque] for name, array in train.items()}
        names = [name for name in train.keys() if name != targetii] + [targetii]

        # create training block and slip off matrix and target
        block = numpy.array([train[name] for name in names]).transpose(1, 0)
        matrix = block[:, :-1]
        truth = block[:, -1]

        # begin report
        report = ['Random Forest predicting {}'.format(targetii)]
        report.append('in file: {}/'.format(self._fold(hydra.current)))
        report.append('{}'.format(self._file(hydra.current)))
        report.append('')

        # run random forest
        self._stamp('running random forest for {}...'.format(targetii), initial=True)
        self._print('matrix: {}'.format(matrix.shape))
        forest = RandomForestRegressor(n_estimators=100, max_depth=5)
        forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(names, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        self._make('{}/reports'.format(self.sink))
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, targetii)
        self._jot(report, destination)

        # print status
        self._stamp('planted.')

        return None

    def sketch(self, year, number=5, archive=95044):
        """Sketch a random set of gridded ozone files.

        Arguments:
            year: int, the year
            number: number of instances
            archive: int, the archive set

        Returns:
            None
        """

        # make directory
        folder = '{}/sketches'.format(self.sink)
        self._make(folder)

        # create random days
        days = numpy.random.randint(1, 29, size=number)
        months = numpy.random.randint(1, 12, size=number)

        # for each instance
        for month, day in zip(months, days):

            # for each product
            for product in ('OMTO3G', 'OMTO3d', 'OMTO3e'):

                # create hydra
                formats = (archive, product, year, self._pad(month))
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}'.format(*formats))

                # ingest based date
                date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
                hydra.ingest(date)

                # get the ozone
                ozone = hydra.grab('ColumnAmountO3')

                # if using gridded product
                if product == 'OMTO3G':

                    # also grab quality flags
                    quality = hydra.grab('QualityFlags')

                    # construct ozone grid from all positive ozone values
                    mask = (ozone >= 0) & (numpy.isfinite(ozone)) & (quality < 1)
                    summation = numpy.where(mask, ozone, 0).sum(axis=0)
                    samples = mask.astype(int).sum(axis=0)
                    grid = summation / samples

                    # reassign ozone
                    ozone = grid

                # construct latitude and longitude
                shape = ozone.shape
                latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
                longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]

                # craete grid
                grid, gridii = numpy.meshgrid(longitudes, latitudes)

                # create title destination
                title = 'Ozone, AS{}, {}, {}m{}{}'.format(*formats, self._pad(day))
                formatsii = (folder, archive, year, self._pad(month), self._pad(day), product)
                destination = '{}/Ozone_as{}_{}m{}{}_{}.png'.format(*formatsii)

                # set parameters
                limits = (-90, 90, -180, 180)
                size = (10, 7)
                logarithm = False
                back = 'white'
                unit = 'DU'
                gradient = 'viridis'
                selection = list(range(256))
                scale = (200, 500)
                bar = True

                # create mask and apply, clipping to scale
                mask = (numpy.isfinite(ozone)) & (ozone > 0)
                tracer = numpy.where(mask, ozone, -1)
                tracer = numpy.where(tracer > scale[1], scale[1], tracer)
                tracer = numpy.where((tracer > 0) & (tracer < scale[0]), scale[0], tracer)
                abscissa = grid
                ordinate = gridii

                # make plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer, ordinate, abscissa], destination, [title, unit]]
                parameters += [scale, [gradient, selection], limits, 1]
                options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'max'}
                self._paint(*parameters, **options)

        return None

    def spike(self, event='2006m0801', orbit=10886, scan=1544, archives=(10003, 95044), tag='spike'):
        """Sketch row differences between OMTO3 files at a particular scanline.

        Arguments:
            event: str, date in YYYYmMMDD format
            orbit: int, orbit number
            scan: int, scanline
            archives: tuple of ints, the archive sets
            tag: str, tag for plots folder

        Returns:
            None
        """

        # set product
        product = 'OMTO3'

        # make directory
        folder = '{}/plots/{}'.format(self.sink, tag)
        self._make(folder)

        # parse event to override random selections
        year = int(event[:4])
        month = int(event[5:7])
        day = int(event[7:9])

        # set fields
        fields = {'ozone': 'ColumnAmountO3', 'value': 'NValue', 'one': 'StepOneO3', 'two': 'StepTwoO3'}
        fields.update({'quality': 'QualityFlags', 'latitude': 'Latitude', 'longitude': 'Longitude'})
        fields.update({'cloud': 'CloudPressure', 'below': 'O3BelowCloud'})

        # set tranforms to extract 360nm
        transforms = {'value': lambda array: array[:, :, 10]}

        # set mean reservoir and tracks reservoir
        means = {field: [] for field in fields.keys()}

        # for each archive set
        for archive in archives:

            # create hydra for first archive set
            formats = (archive, product, year, self._pad(month), self._pad(day))
            hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}/{}'.format(*formats))

            # set up accumulation
            accumulations = {field: [] for field in fields.keys()}

            # ingest the path
            hydra.ingest(str(orbit))

            # grab data
            data = {field: hydra.grab(search) for field, search in fields.items()}
            data.update({field: transform(data[field]) for field, transform in transforms.items()})

            # append arrays
            [accumulation.append(data[field]) for field, accumulation in accumulations.items()]

            # create stacks from last member
            stacks = {field: numpy.vstack(accumulation[-1:]) for field, accumulation in accumulations.items()}

            # for each stack
            for field, array in stacks.items():

                # append array
                means[field].append(array)

        # set up differences
        differences = {field: mean[1] - mean[0] for field, mean in means.items()}

        # set up arrays
        reservoir = {field: means[field] + [differences[field]] for field in fields.keys()}

        # set up stubs and modes
        modes = ['AS{} vs AS{}'.format(*archives)] * 2
        modes += ['AS{} - AS{}'.format(archives[1], archives[0])]
        stubs = ['col3', 'col4', 'diff']

        # for each field
        for field, arrays in reservoir.items():

            # for each array
            for array, mode, stub in zip(arrays, modes, stubs):

                # create mask
                mask = numpy.isfinite(array[scan]) & (abs(array[scan]) <1e20)

                # create plot
                latitude = round(reservoir['latitude'][0][scan, 30], 0)
                longitude = round(reservoir['longitude'][0][scan, 30], 0)
                degrees = (self._orient(latitude), self._orient(longitude, east=True))
                formats = (fields[field], product, mode, event, orbit, scan, *degrees)
                title = '{}, {}, {}, {}, o{}, scanline {}, {}, {}'.format(*formats)
                address = 'plots/{}/{}_{}_{}_{}_{}_{}.h5'.format(tag, field, event, scan, *degrees, stub)
                self.squid.ink(field, array[scan][mask], 'row', numpy.array(range(60))[mask], address, title)

        return None

    def swap(self, name, year, month, day, orbit=0, archive=95044, grid=False, paths=None, earth='OMTO3'):
        """Swap out entries in an OMTO3 or OMTO3G PCF file.

        Arguments:
            name: str, filename of PCF file
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit
            grid: boolean, using grid files?
            paths: list of str, the pathnames for replacement
            earth: str, output product esdt

        Returns:
            None
        """

        # open up PCF
        text = self._know(name)

        # set input esdts
        earths = {'OMTO3G': 'OMTO3', 'OMTO3d': 'OMTO3G', 'OMTO3e': 'OMTO3G'}

        # create prefixes
        prefixes = {'OMTO3G': 'L2G', 'OMTO3d': 'L3', 'OMTO3e': 'L3'}

        # if OMTO3G file
        if grid:

            # get input esdt
            earthii = earths[earth]
            stub = '{}_'.format(earth)
            stubii = '{}_'.format(earthii)

            # get lines with OMTO3 files
            lines = [(index, line) for index, line in enumerate(text) if stubii in line]

            # get indices
            start = lines[0][0]
            finish = lines[-1][0]

            # create block
            lines = [pair[1] for pair in lines][-len(paths):]
            entries = [[entry for entry in line.split('|') if stubii in entry][0] for line in lines][-len(paths):]

            # replace block
            block = [line.replace(entry, self._file(path)) for line, entry, path in zip(lines, entries, paths)]

            # reconstrct text
            text = text[:start] + block + text[finish + 1:]

            # create OMTO3G output product destination name
            date = datetime.datetime(year, month, day)
            julian = date.timetuple().tm_yday
            formats = (prefixes[earth], earth, year, self._pad(julian, 3))
            product = 'OMI-Aura_{}-{}_{}d{}.he5'.format(*formats)

            # find the line with this input
            lines = [(index, line) for index, line in enumerate(text) if stub in line]
            index, line = lines[0]

            # replace with new product
            path = [entry for entry in line.split('|') if stub in entry][0]
            line = line.replace(path, product)
            text[index] = line

            # find the line with day of year
            prefix = prefixes[earth]
            lines = [(index, line) for index, line in enumerate(text) if '{}dyofyr'.format(prefix) in line]
            index, line = lines[0]

            # replace with new day of year
            fragments = line.split('|')[:-1] + [str(julian).strip('0')]
            line = '|'.join(fragments)
            text[index] = line

        # if not a grid file
        else:

            # set inputs
            inputs = ['OML1BRUG', 'OMUANC', 'OMCLDRR', 'OMUFPITMET', 'OML1BRVG']

            # for each input
            for file in inputs:

                # find the line with this input
                lines = [(index, line) for index, line in enumerate(text) if file in line]

                # if line is found
                if len(lines) > 0:

                    index, line = lines[0]

                    # find path
                    path = [entry for entry in line.split('|') if file in entry][0]

                    # construct hydra
                    formats = (file, year, self._pad(month), self._pad(day))
                    hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}/{}'.format(archive, *formats))
                    hydra.ingest(str(orbit))

                    # replace the pcf line with the new path
                    line = line.replace(path, self._file(hydra.current))
                    text[index] = line

            # create OMTO3 output product destination name
            formats = (year, self._pad(month), self._pad(day), self._pad(orbit, 6))
            product = 'OMI-Aura_L2-OMTO3_{}m{}{}t0000-o{}.he5'.format(*formats)

            # find the line with this input
            lines = [(index, line) for index, line in enumerate(text) if 'OMTO3_' in line]
            index, line = lines[0]

            # replace with new product
            path = [entry for entry in line.split('|') if 'OMTO3_' in entry][0]
            line = line.replace(path, product)
            text[index] = line

            # find the line with orbitnumber
            lines = [(index, line) for index, line in enumerate(text) if 'OrbitNumber' in line]
            index, line = lines[0]

            # replace with new day of year
            fragments = line.split('|')[:-1] + [str(orbit).lstrip('0')]
            line = '|'.join(fragments)
            text[index] = line

        # inspect
        self._tell(text)

        # save changes
        self._jot(text, name)

        return None

    def text(self, year=2005, number=5, archives=(10003, '10003text'), product='OMTO3d', event=None, tag='text'):
        """Sketch a random set of gridded ozone files.

        Arguments:
            year: int, the year
            number: number of instances
            archives: tuple of ints, the archive sets
            product: str, name of specific product
            event: str, specific date in YYYYmMMDD format

        Returns:
            None
        """

        # make directory
        folder = '{}/plots/{}'.format(self.sink, tag)
        self._make(folder)

        # create random days
        days = numpy.random.randint(1, 29, size=number)
        months = numpy.random.randint(1, 12, size=number)

        # if event given
        if event:

            # parse event to override random selections
            year = int(event[:4])
            months = [int(event[5:7])]
            days = [int(event[7:9])]

        # for each instance
        for month, day in zip(months, days):

            # begin ozones
            ozones = []

            # construct date
            date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))

            # for each archive set
            for archive in archives:

                # if text
                if 'text' in str(archive):

                    # get ozone from text file
                    ozone = self.textualize(year, month, day, int(archive.replace('text', '')))
                    ozones.append(ozone)

                # otherwise
                else:

                    # create hydra for first archive set
                    formats = (archive, product, year, self._pad(month))
                    hydra = Hydra('/tis/acps/OMI/{}/{}/{}/{}'.format(*formats))
                    hydra.ingest('_{}_'.format(date))

                    # if gridded
                    if product == 'OMTO3G':

                        # get the ozone
                        ozone = hydra.grab('ColumnAmountO3')
                        quality = hydra.grab('QualityFlags')

                        # construct ozone grid from all positive ozone values
                        mask = (ozone >= 0) & (numpy.isfinite(ozone)) & (quality < 1)
                        summation = numpy.where(mask, ozone, 0).sum(axis=0)
                        samples = mask.astype(int).sum(axis=0)
                        grid = summation / samples

                        # reassign ozone
                        ozone = grid
                        ozones.append(ozone)

                    # otherwise
                    else:

                        # get the ozone
                        ozone = hydra.grab('ColumnAmountO3')
                        ozones.append(ozone)

            # append differences
            difference = ozones[1] - ozones[0]

            # set up plot arrays and attributes
            arrays = ozones + [difference]
            scales = [(100, 500), (100, 500), (-5, 5)]
            text = '../studies/osiris/eight/colorbars/ozone_color_bar.txt'
            gradients = [text, text, 'seismic']

            # set up stubs and modes
            modes = ['AS{}'.format(archives[0]), 'AS{}'.format(archives[1])]
            modes += ['AS{} - AS{}'.format(archives[1], archives[0])]
            stubs = [mode.lower() for mode in modes[:2]] + ['diff']

            # for each array
            for array, mode, stub, gradient, scale in zip(arrays, modes, stubs, gradients, scales):

                # construct latitude and longitude
                shape = array.shape
                latitudes = [-90 + (90 / shape[0]) + index * (180 / shape[0]) for index in range(shape[0])]
                longitudes = [-180 + (180 / shape[1]) + index * (360 / shape[1]) for index in range(shape[1])]

                # craete grid
                grid, gridii = numpy.meshgrid(longitudes, latitudes)

                # create title destination
                title = 'Ozone, {}, {}, {}'.format(product, mode, date)
                formatsii = (folder, product.lower(), date, stub)
                destination = '{}/Ozone_{}_{}_{}.png'.format(*formatsii)

                # set parameters
                limits = (-90, 90, -180, 180)
                size = (10, 7)
                logarithm = False
                back = 'white'
                unit = 'DU'
                selection = list(range(256))
                bar = True

                # create mask and apply, clipping to scale
                fill = 1e20
                mask = (numpy.isfinite(array)) & (abs(array) < fill)
                tracer = numpy.where(mask, array, numpy.nan)
                tracer = numpy.where((tracer > scale[1]) & (abs(tracer) < fill), scale[1], tracer)
                tracer = numpy.where((tracer < scale[0]) & (abs(tracer) < fill), scale[0], tracer)
                abscissa = grid
                ordinate = gridii

                # make plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer, ordinate, abscissa], destination, [title, unit]]
                parameters += [scale, [gradient, selection], limits, 1]
                options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                self._paint(*parameters, **options)

        return None

    def textualize(self, year=2005, month=3, day=21, archive=10003):
        """Create an ozone array from a text file.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            archive: int, the archive set

        Returns:
            numpy array
        """

        # # construct site
        # date = '{}{}{}'.format(year, self._pad(month), self._pad(day))
        # site = 'https://ozonewatch.gsfc.nasa.gov/data/omi/Y{}/L3_ozone_omi_{}.txt'.format(year, date)
        #
        # # get contents
        # content = str(requests.get(site).content)
        # text = content.split('\\n')
        # text = [line for line in text if len(line) > 1]

        # grab text file
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3dtoz/{}/{}'.format(archive, year, self._pad(month)), extensions=('.txt',))
        name = [path for path in hydra.paths if '_{}'.format(date) in path][0]
        text = self._know(name)

        # begin parallels
        parallels = [[]]
        for line in text[3:]:

            # if latitde in line
            if 'lat' in line:

                # get ozone data
                line = line.strip()
                block = 10
                data = [int(str(line[index * 3: 3 + index * 3])) for index in range(block)]

                # add to current parallel
                parallels[-1] += data

                # create new parallel
                parallels.append([])

            # otherwise
            else:

                # get ozone data
                line = line.strip()
                block = 25
                data = [int(str(line[index * 3: 3 + index * 3])) for index in range(block)]

                # add to current parallel
                parallels[-1] += data

        # construct array
        ozone = numpy.array(parallels[:-1])

        return ozone

    def thresh(self, year=2024, month=1, day=5, exclusions=None, inclusions=None, position=0):
        """Use random forest to predict occurences of residual 360 threshold exceedences.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            exclusions: list of fields not to be included
            position: index for three-d features

        Returns:
            None
        """

        # make output folders
        self._make('{}/reports'.format(self.sink))
        self._make('{}/plots/threshold'.format(self.sink))

        # set default exclusions
        exclusions = exclusions or ['QualityFlags']

        # get hydra for gridded product
        formats = (year, self._pad(month), self._pad(day))
        date = '{}m{}{}'.format(*formats)
        hydra = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(*formats))
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        paths.sort()

        # set fill values
        fill = -999
        fillii = 1e20

        # begin data
        data = {}

        # define target
        target = 'residual_360_threshold'

        # define three dimensional fields
        threes = []

        # for each path
        for path in paths:

            # get the orbit number from the path
            orbit = self._stage(path)['orbit'].strip('0')

            # ingest the data
            hydra.ingest(orbit)

            # get a counter for the shapes
            counter = list(Counter([feature.shape for feature in hydra if len(feature.shape) == 2]).items())
            counter.sort(key=lambda pair: pair[1], reverse=True)
            shape = counter[0][0]

            # get all 2-D feature names
            names = [feature.name for feature in hydra if feature.shape == shape]

            # set all data names to empty list
            data.update({name: data.setdefault(name, []) for name in names})

            # append all new data
            [data[name].append(hydra.grab(name)) for name in names]

            # set all 3-D names to empty list
            data.update({name: data.setdefault(name, []) for name in threes})

            # append all new data
            [data[name].append(hydra.grab(name)[:, :, position]) for name in threes]

            # add target
            data[target] = data.setdefault(target, [])

            # make target
            quality = hydra.grab('QualityFlags')
            threshold = ((quality & 1) == 1) & ((quality & 2) == 2) & ((quality & 4) == 0)
            data[target].append(threshold.astype(int))

            # add rows and scanlines
            data.update({name: data.setdefault(name, []) for name in ('row', 'scan')})

            # create row and scan matrices
            row = numpy.array([list(range(quality.shape[1]))] * quality.shape[0])
            scan = numpy.array([list(range(quality.shape[0]))] * quality.shape[1]).transpose(1, 0)
            data['row'].append(row)
            data['scan'].append(scan)

        # concatenate all arrays
        data = {name: numpy.vstack(arrays) for name, arrays in data.items()}

        # grab quality data
        quality = data['QualityFlags']

        # prune exclusions
        data = {name: arrays for name, arrays in data.items() if name not in exclusions}

        # if inclusions
        if inclusions:

            # keep only inclusions
            data = {name: array for name, array in data.items() if name in inclusions + [target]}

        # get valid data mask
        masks = [(array > fill) & (abs(array) < fillii) & numpy.isfinite(array) for array in data.values()]

        # begin masque without row anomaly
        masque = ((quality & 64) == 0)

        # multiply all masks
        for mask in masks:

            # use logical and
            masque = numpy.logical_and(masque, mask)

        # check masked quantity
        masked = masque.sum()
        total = numpy.prod(masque.shape)
        self._print('masque: {} of {}, {} %'.format(masked, total, 100 * masked / total))

        # apply masque
        data = {name: array[masque] for name, array in data.items()}

        # view data limits
        self._view(data)

        # set destiation
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, target)

        # run forest
        self._plant(data, target, destination, classify=True)

        # set scatter plot pairs
        pairs = [('ViewingZenithAngle', 'UVAerosolIndex')]
        pairs += [('SolarZenithAngle', 'UVAerosolIndex')]
        pairs += [('Reflectivity331', 'UVAerosolIndex')]
        pairs += [('Reflectivity360', 'UVAerosolIndex')]
        pairs += [('Latitude', 'UVAerosolIndex')]
        pairs += [('Longitude', 'UVAerosolIndex')]
        pairs += [('row', 'UVAerosolIndex')]

        # for each pair
        for dependent, independent in pairs:

            # plot threshold on
            mask = (data[target] == 1)
            title = 'Residual 360 > threshold, {} vs {}, {}'.format(dependent, independent, date)
            address = 'plots/threshold/On_{}_{}_{}.h5'.format(dependent.lower(), independent.lower(), date)
            parameters = [self._serpentize(dependent), data[dependent][mask]]
            parameters += [self._serpentize(independent), data[independent][mask]]
            parameters += [address, title]
            self.squid.ink(*parameters)

            # plot threshold off
            mask = (data[target] == 0)
            title = 'Residual 360 > threshold, {} vs {}, {}'.format(dependent, independent, date)
            address = 'plots/threshold/Off_{}_{}_{}.h5'.format(dependent.lower(), independent.lower(), date)
            parameters = [self._serpentize(dependent), data[dependent][mask]]
            parameters += [self._serpentize(independent), data[independent][mask]]
            parameters += [address, title]
            self.squid.ink(*parameters)

        return None

    def track(self, year, month, day, orbit=0, archive=10003, archiveii=10004):
        """Track the row anomaly by identifing which rows are flagged in OMTO3 and OMUAC.

        Arguments:
            year: int, year
            montth: in, month
            day: int, day
            orbit: int, orbit index on day

        Returns:
            None
        """

        # grab omto3 files from
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        path = paths[orbit]

        # make secondary hydra for ancillary
        hydraii = Hydra('/tis/acps/OMI/{}/OMUANC/{}/{}/{}'.format(archiveii, *formats[1:]))

        # ingest path
        self._print('ingesting {}...'.format(path))
        hydra.ingest(path)

        # get orbit nhmber
        orbit = self._stage(path)['orbit']
        date = self._stage(path)['date']

        # grab quality
        quality = hydra.grab('QualityFlags')
        # get bit 6 from quality
        anomaly = (quality & 64) == 64
        anomaly = anomaly.astype(float)

        # get row anomaly from anc
        hydraii.ingest('o{}'.format(orbit))
        anomalyii = hydraii.grab('row_anomaly')[0]

        # begin report
        report = []
        formats = (year, self._pad(month), self._pad(day), orbit)
        report.append(self._print('Row Anomaly flagged row, {}m{}{}, o{}'.format(*formats)))

        # add header
        report.append(self._print(''))
        report.append(self._print('OMUANC / AS{}, OMTO3 / AS{}'.format(archiveii, archive)))
        report.append(self._print('N        S        N        S'))
        report.append(self._print(''))

        # for each row
        zipper = zip(anomalyii[-1, :], anomalyii[0, :], anomaly[-1, :], anomaly[0, :])
        for row, quartet in enumerate(zipper):

            # defining indicator function
            def indicating(flag) : return self._pad(str(row)) if flag else '  '

            # add to report
            formats = [indicating(flag) for flag in quartet]

            # if any flags are markeed
            if any([flag.isdigit() for flag in formats]):

                # add flags
                report.append(self._print('{}       {}       {}       {}'.format(*formats)))

        # make report destination
        self._make('{}/reports'.format(self.sink))
        formats = (self.sink, orbit, year, self._pad(month), self._pad(day))
        destination = '{}/reports/Row_anomaly_o{}_{}m{}{}.txt'.format(*formats)

        # jot report
        self._jot(report, destination)

        return None

    def warn(self, year=2024, month=6, days=(1, 16), archives=(10004, 95044)):
        """Create histogram of ozone differences due to quality warning flags.

        Arguments:
            year: int, the year
            month: int, the month
            days: tuple of ints, the day range
            archives: tuple of ints, the archive sets

        Returns:
            None
        """

        # create folder
        folder = '{}/plots/warnings'.format(self.sink)
        self._make(folder)

        # set fields
        fields = {'measure': 'MeasurementQualityFlags', 'ozone': 'ColumnAmountO3'}
        fields.update({'cloud': 'CloudPressure', 'fraction': 'RadiativeCloudFraction', 'latitude': 'Latitude'})
        fields.update({'zenith': 'SolarZenithAngle', 'quality': 'QualityFlags'})

        # begin data reservoirs
        data = {archive: {field: [] for field in fields.keys()} for archive in archives}

        # for each day
        for day in range(*days):

            # construct stub
            stub = '{}/{}/{}'.format(year, self._pad(month), self._pad(day))

            # for each archive set
            for archive in archives:

                # create hydra
                hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}'.format(archive, stub), show=False)

                # for each path
                for path in hydra.paths:

                    # ingest
                    hydra.ingest(path)

                    # collect data
                    [data[archive][field].append(hydra.grab(search).squeeze()) for field, search in fields.items()]

        # create masks for quality warning
        measures = data[archives[0]]['measure']
        masks = [((measure & 2) == 2) for measure in measures]

        print([mask.sum() for mask in masks])

        # for each archive
        for archive in archives:

            # for each field
            for field, arrays in data[archive].items():

                # apply masks
                data[archive][field] = [array[mask] for array, mask in zip(arrays, masks)]

            # delete measurement warnings
            data[archive] = {field :arrays for field, arrays in data[archive].items() if field != 'measure'}

        # create masks for pixel quality
        qualities = data[archives[1]]['quality']
        masks = [(quality < 2) for quality in qualities]

        print([mask.sum() for mask in masks])

        # for each archive
        for archive in archives:

            # for each field
            for field, arrays in data[archive].items():

                # apply masks
                data[archive][field] = [array[mask] for array, mask in zip(arrays, masks)]

        # add ozone differences to first archive set
        zipper = zip(data[archives[0]]['ozone'], data[archives[1]]['ozone'])
        data[archives[0]]['difference'] = [first - second for first, second in zipper]
        fields['difference'] = 'ColumnAmountO3 difference'

        # add pixel count
        data[archives[0]]['pixels'] = [mask.sum() for mask in masks]
        fields['pixels'] = 'Affected Pixels per Orbit'

        # for each field
        for field in ['difference', 'fraction', 'latitude', 'zenith', 'cloud', 'pixels']:

            # create title
            formats = (fields[field], year, self._pad(month), self._pad(days[0]), self._pad(days[1] - 1))
            title = 'OMTO3 quality warning {}, {}, {}, {} to {}'.format(*formats)

            # create histogram information
            array = numpy.hstack(data[archives[0]][field])

            # crate histogram
            destination = 'plots/warnings/{}_histogram.h5'.format(field)
            self.squid.ripple(field, array, destination, title)

        return None

    def zone(self, year=2005, month=6, archive=95042, limit=500, sink='collection', sinkii='zones'):
        """Collect 5 degree zonal means from omto3 files.

        Arguments:
            year: int, year
            month: int, month
            archive: int, archive set
            limit: number orbits
            sink: str, folder for collection files
            sinkii: str, folder for zonal means files

        Returns:
            None
        """

        # create destination folders and logs folder
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))
        self._make('{}/logs'.format(self.sink))

        # create destination file
        formats = (self.sink, sink, year, self._pad(month), archive)
        destination = '{}/{}/OMTO3_Zonal_Collection_{}m{}_{}.h5'.format(*formats)

        # set up latitude brackets, reversing for northmost at first index
        brackets = [(-90 + index * 5, -90 + 5 + index * 5) for index in range(36)]
        brackets.reverse()

        # create hydra
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}'.format(archive, year, self._pad(month)), 1, 31)

        # get paths
        paths = [path for path in hydra.paths if not path.endswith('.met')]

        # set up field names for grabbing data
        names = ['QualityFlags', 'AlgorithmFlags', 'TerrainPressure', 'CloudPressure', 'fc']
        names += ['Reflectivity331', 'Reflectivity360', 'StepOneO3', 'StepTwoO3', 'ColumnAmountO3']
        names += ['SO2index', 'UVAerosolIndex', 'Latitude', 'Longitude', 'SolarZenithAngle']
        names += ['ViewingZenithAngle', 'RelativeAzimuthAngle', 'NValue', 'Residual']
        names += ['ResidualStep1', 'ResidualStep2', 'dN_dR', 'Sensitivity']

        # initialize data collection dictionaries
        data = {}
        sums = {}
        squares = {}
        counts = {}
        attributes = {}

        # begin timer
        formats = (year, self._pad(month), archive, self.sink, sinkii)
        self._stamp('beginning zonal means for {}m{}, AS{}, into {}/{}...'.format(*formats), initial=True)

        # for each path ( within limited number of paths )
        paths = paths[:limit]
        for number, path in enumerate(paths):

            # print
            self._print('ingesting {}, {} of {}...'.format(path, number, len(paths)))

            # ingest path
            hydra.ingest(path)

            # collect data
            collection = {name: hydra.grab(name) for name in names}

            # collect attributes
            attributes.update({name: hydra.dig(name)[0].attributes for name in names})

            # additionally get wavelengths
            wavelength = hydra.grab('Wavelength')
            attributes['Wavelength'] = hydra.dig('Wavelength')[0].attributes
            waves = wavelength.shape[0]

            # additionally get time field
            time = hydra.grab('Time')
            attributes['Time'] = hydra.dig('Time')[0].attributes

            # construct reflectivity mask for all cases > 0, < 100
            mask = (collection['Reflectivity331'] > 0) & (collection['Reflectivity331'] < 100)

            # construct algorithm flag mask for all samples not skipped ( those with snow / ice are allowed )
            maskii = (collection['AlgorithmFlags'] % 10 > 0)

            # set quality mask for ascending and good retrievals (QualityFlags & 15) == 0 | 1
            maskiii = (collection['QualityFlags'] & 15 < 2)

            # set quality mask for no anomaly ( not bit 6 )
            maskiv = (collection['QualityFlags'] & 64 == 0)

            # combine all masks
            masque = mask & maskii & maskiii & maskiv

            # set transformations to be applied
            transforms = {'Reflectivity331': lambda tensor: tensor / 100.0}
            transforms.update({'Reflectivity360': lambda tensor: tensor / 100.0})
            transforms.update({'AlgorithmFlags': lambda tensor: (tensor % 10).astype(float)})
            transforms.update({'QualityFlags': lambda tensor: (tensor >> 7 & 1).astype(float)})

            # apply transformations
            collection.update(({name: transform(collection[name]) for name, transform in transforms.items()}))

            # apply mask to all data
            collection.update({name: collection[name][masque] for name in collection.keys()})

            # begin data arrays
            sums.update({name: sums.setdefault(name, []) for name in names})
            squares.update({name: squares.setdefault(name, []) for name in names})
            counts.update({name: counts.setdefault(name, []) for name in names})

            # allocate 2-D zero arrays
            [sums[name].append(numpy.zeros((36,))) for name in names if len(collection[name].shape) == 1]
            [squares[name].append(numpy.zeros((36,))) for name in names if len(collection[name].shape) == 1]
            [counts[name].append(numpy.zeros((36,))) for name in names if len(collection[name].shape) == 1]

            # allocate 3-D zero arrays
            [sums[name].append(numpy.zeros((36, waves))) for name in names if len(collection[name].shape) == 2]
            [squares[name].append(numpy.zeros((36, waves))) for name in names if len(collection[name].shape) == 2]
            [counts[name].append(numpy.zeros((36, waves))) for name in names if len(collection[name].shape) == 2]

            # allocate for time field
            sums['Time'] = sums.setdefault('Time', [])
            squares['Time'] = squares.setdefault('Time', [])
            counts['Time'] = counts.setdefault('Time', [])

            # append time
            sums['Time'].append(time.sum())
            squares['Time'].append((time ** 2).sum())
            counts['Time'].append(len(time))

            # add orbit number
            orbit = int(self._stage(path)['orbit'])
            data['Orbit'] = data.setdefault('Orbit', [])
            data['Orbit'].append(orbit)

            # add date and time
            date = self._stage(path)['date']
            partitions = {'Year': (0, 4), 'Month': (5, 7), 'Day': (7, 9), 'Hour': (10, 12), 'Minute': (12, 14)}
            for partition, (first, last) in partitions.items():

                # append data
                data[partition] = data.setdefault(partition, [])
                data[partition].append(int(date[first: last]))

            # add wavelength
            data['Wavelength'] = wavelength

            # add latitude brackets
            data['LatitudeBracket'] = numpy.array(brackets)
            data['LatitudeMidpoint'] = numpy.array(brackets).mean(axis=1)

            # for each latitude bracket
            for index, (south, north) in enumerate(brackets):

                # construct mask from latitude limits
                mask = (collection['Latitude'] >= south) & (collection['Latitude'] < north)

                # for each name in the collection
                for name in names:

                    # also limit to non fill values ( abs < 1e10 )
                    maskii = (abs(collection[name]) < 1e10)

                    # try to
                    try:

                        # combine masks
                        masqueii = mask & maskii

                    # unless maskii is 2-D
                    except ValueError:

                        # in which case, expand first mask
                        mask = numpy.array([mask] * waves).transpose(1, 0)
                        masqueii = mask & maskii

                    # add sum and sum of squares to data
                    sums[name][-1][index] += collection[name][masqueii].sum(axis=0)
                    squares[name][-1][index] += (collection[name][masqueii] ** 2).sum(axis=0)
                    counts[name][-1][index] += masqueii.sum(axis=0)

        # add sums and squares
        data.update({'{}Sums'.format(name): arrays for name, arrays in sums.items()})
        data.update({'{}Squares'.format(name): arrays for name, arrays in squares.items()})
        data.update({'{}Counts'.format(name): arrays for name, arrays in counts.items()})

        # update attributes
        attributes.update({'{}Sums'.format(name): attributes[name] for name in sums.keys()})
        attributes.update({'{}Squares'.format(name): attributes[name] for name in squares.keys()})
        attributes.update({'{}Counts'.format(name): attributes[name] for name in counts.keys()})

        # form into arrays
        data.update({name: numpy.array(arrays) for name, arrays in data.items()})

        # create file
        hydra.spawn(destination, data, attributes)

        # create destination name for means file
        destinationii = destination.replace(sink, sinkii).replace('Collection', 'Means')

        # for each field, including time
        dataii = {}
        for name in names + ['Time']:

            # add counts, summing across orbits
            count = data['{}Counts'.format(name)].sum(axis=0)
            dataii.update({'{}Counts'.format(name): count})

            # add mean, summing sums across orbits
            mean = data['{}Sums'.format(name)].sum(axis=0) / count
            dataii.update({'{}Mean'.format(name): mean})

            # add std dev as sqrt ( avg (squares) - avg ** 2 )
            variance = data['{}Squares'.format(name)].sum(axis=0) / count - mean ** 2
            deviation = numpy.sqrt(variance)
            dataii.update({'{}Stdev'.format(name): deviation})

            # update attributes
            attributes['{}Mean'.format(name)] = attributes[name]
            attributes['{}Stdev'.format(name)] = attributes[name]

        # copy over wavelength and latitude brackets
        dataii.update({name: data[name] for name in ('Wavelength', 'LatitudeBracket', 'LatitudeMidpoint')})

        # create means file
        hydra.spawn(destinationii, dataii, attributes)

        # finish
        self._stamp('finished.')

        return None






