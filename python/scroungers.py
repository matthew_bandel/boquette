# scroungers.py for Scrounger class to track disk usage

# import local classes
from cores import Core

# import general tools
import os
import re
import yaml
import subprocess

# import time and datetime
import time
import datetime
import calendar

# import collection
from collections import Counter

# import numpy nand math
import numpy
import math


# class Scrounger to parse hdf files
class Scrounger(Core):
    """Scrounger class to track variables in a program.

    Inherits from:
        cores.Core
    """

    def __init__(self, source='.', sink='.', usage='usage/usage.txt'):
        """Initialize a Scrounger instance.

        Arguments:
            source: str, source folder for files
            sink: str, folder for report
            start: str, date-based subdirectory
            finish: str, date-based subdirectory

        Returns:
            None
        """

        # initialize the base Core instance
        Core.__init__(self)

        # set directory information
        self.source = source
        self.sink = sink

        # set usage file
        self.usage = usage

        # set suffixes
        self.suffixes = {}
        self._establish()

        # # parse file
        # self._parse()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Scrounger instance at: {} using {} >'.format(self.sink, self.usage)

        return representation

    def _create(self):
        """Create the usage file.

        Arguments:
            None

        Returns:
            None
        """

        # create destination
        destination = '{}/{}'.format(self.sink, self.usage)

        # try to
        try:

            # delete old file
            self._clean(destination, force=True)

        # unless error
        except OSError:

            # nevermind
            pass

        # create call
        call = ['du', '-h', self.source]

        # make call
        self._print(' '.join(call))
        subprocess.call(call, stdout=open(destination, "w"))

        return None

    def _establish(self):
        """Establish suffix dictionary.

        Arguments:
            None

        Returns:
            None
        """

        # set suffix dictionary
        suffixes = {'B': 1, 'K': 1e3, 'M': 1e6, 'G': 1e9, 'T': 1e12}
        self.suffixes = suffixes

        return None

    def _parse(self):
        """Parse usage file and populate entries.

        Arguments:
            None

        Returns:
            None
        """

        # ingest usage report
        usage = self._know('{}/{}'.format(self.sink, self.usage))

        # begin entries
        entries = []
        for line in usage:

            # split line
            text, path = line.split()

            # if there is no suffix
            if text[-1].isdigit():

                # add byte suffix
                text = text + 'B'

            # get size from text
            size = float(text[:-1]) * self.suffixes[text[-1]]

            # create entry and append
            entry = (text, path, size)
            entries.append(entry)

        # sort entries according to size
        entries.sort(key=lambda entry: entry[-1], reverse=True)

        # populate
        for entry in entries:

            # append
            self.append(entry)

        return None

    def scrounge(self, folder=None, new=False):
        """Get the file sizes from a particular folder.

        Arguments:
            folder:str, folder name
            new: boolean, create new usage file?

        Returns:
            None
        """

        # set folder
        folder = folder or ''
        folder = '{}/{}'.format(self.source, folder)

        # if a new file
        if new:

            # create usage file
            self._create()

        # if length is zero
        if len(self) < 1:

            # parse
            self._parse()

        # get subset of entries
        subset = [entry for entry in self if folder in entry[1]]

        # get length of folder
        length = len(folder.split('/'))
        subset = [entry for entry in subset if len(entry[1].split('/')) == length + 1]

        # get total size
        suffixii = 'B'
        sizeii = 1
        total = sum([entry[2] for entry in subset])
        for suffix, size in self.suffixes.items():

            # if the size is greater
            if total > size:

                # set suffix
                suffixii = suffix
                sizeii = size

        # print to screen
        subset = [str(entry) for entry in subset]
        subset += ['Total size: {}{}'.format(round(total / sizeii, 1), suffixii)]
        [self._print(entry) for entry in subset]

        # make report
        formats = (self.sink, self._file(self.usage).split('.')[-2], folder.replace('/', '_').replace('.', '_'))
        destination = '{}/reports/usage_{}_{}.txt'.format(*formats)
        self._jot(subset, destination)

        return None
