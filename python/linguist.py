# linguist.py, an example svn application for distinguishing languages based on letter frequencies

# import requests and beautifulSoup for parsing websites
import requests
from bs4 import BeautifulSoup

# import collections for counting
from collections import Counter

# import numpy
import numpy

# import sklearn
from sklearn.svm import SVC
from sklearn.decomposition import PCA

# import matplotlib for plots
from matplotlib import pyplot

# set spanish and italian wikipedia references for climate change
sites = {'spanish': 'https://es.wikipedia.org/wiki/Calentamiento_global'}
sites.update({'italian': 'https://it.wikipedia.org/wiki/Riscaldamento_globale'})

# for each site
print('collecting data...')
data = {language: [] for language in sites.keys()}
for language, site in sites.items():

    # extract the text
    text = requests.get(site).text
    soup = BeautifulSoup(text, 'html.parser')
    excerpts = soup.find_all('p')
    paragraphs = [paragraph.get_text() for paragraph in excerpts]

    # get all individual sentences by splitting at periods
    sentences = [sentence for paragraph in paragraphs for sentence in paragraph.split('.')]

    # strip white space and convert to lowercase, and only keep those longer than 50 characters
    criterion = 50
    sentences = [sentence.strip().lower() for sentence in sentences if len(sentence) > criterion]

    # for each sentence
    for sentence in sentences:

        # count all characters
        counter = Counter(sentence)

        # only keep characters that are letters
        characters = {character: count for character, count in counter.items() if character.isalpha()}

        # convert counts to frequencies by dividing by total letters
        total = sum(characters.values())
        frequencies = {character: count / total for character, count in characters.items()}

        # add to data
        data[language].append(frequencies)

# get all letters used across both sets
letters = [letter for language in data.values() for counter in language for letter in counter.keys()]
letters = list(set(letters))
letters.sort()

# create mirror to map letter to its index in the list
mirror = {letter: index for index, letter in enumerate(letters)}

# create sample, using 0 and 1 as targets
print('creating samples...')
samples = []
for target, language in enumerate(sites.keys()):

    # go through each frequency set
    for frequencies in data[language]:

        # create zero vector and for each letter
        vector = [0] * len(letters)
        for letter, frequency in frequencies.items():

            # update vector with its frequency
            position = mirror[letter]
            vector[position] = frequency

        # add vector and target to samples
        samples.append((vector, target))

# shuffle samples and find the halfway point
samples.sort(key=lambda sample: numpy.random.rand())
half = int(len(samples) / 2)

# create training matrix with targets from first half
train = numpy.array([vector for vector, _ in samples[:half]])
targets = numpy.array([target for _, target in samples[:half]])

# create test matrix with ground truths from second half
test = numpy.array([vector for vector, _ in samples[half:]])
truths = numpy.array([target for _, target in samples[half:]])

# create support vector machine instance and train
print('training support vector machine...')
machine = SVC(C=1.0, kernel='poly', degree=1)
machine.fit(train, targets)

# verify training by predicting from training set
print('verifying training...')
verifications = machine.predict(train)
hits = [target for target, verification in zip(targets, verifications) if target == verification]
percentage = round(100 * len(hits) / len(targets), 3)
print('{} % training accuracy. ( {} samples )'.format(percentage, len(verifications)))

# make predictions on test set
print('predicting unknowns...')
predictions = machine.predict(test)
hits = [truth for truth, prediction in zip(truths, predictions) if truth == prediction]
percentage = round(100 * len(hits) / len(truths), 3)
print('{} % testing accuracy. ( {} samples )'.format(percentage, len(predictions)))

# begin plot with title
print('plotting 2-D graph...')
pyplot.clf()
pyplot.title('Classification by SVM of Wikipedia Sentences\n based on Climate Change Entry')

# create PCA decomposition for plotting in two dimensions
decomposer = PCA(n_components=2)
decomposer.fit(train)

# create axis labels based on a number of strongest entries
labels = []
strongest = 5
for component in range(2):

    # extract most important components to horizontal axis
    axis = decomposer.components_[component]
    pairs = list(zip(letters, axis))
    pairs.sort(key=lambda pair: abs(pair[1]), reverse=True)
    formats = []
    for pair in pairs[:strongest]:

        # add each pair to formats
        formats += [pair[0], round(pair[1], 3)]

    # create label
    label = ('{} ({}), ' * strongest).format(*formats).strip(', ')
    labels.append(label)

# add to labels
pyplot.xlabel(labels[0])
pyplot.ylabel(labels[1])

# plot training samples
matrix = decomposer.transform(train)
colors = ['r+', 'g+']
for index, color in enumerate(colors):

    # get vector subset
    vectors = [vector for vector, target in zip(matrix, targets) if target == index]
    horizontals, verticals = list(zip(*vectors))

    # plot
    label = 'training, {}'.format(list(sites.keys())[index])
    pyplot.plot(horizontals, verticals, color, label=label)

# plot test samples
matrixii = decomposer.transform(test)
colors = ['mx', 'bx']
for index, color in enumerate(colors):

    # get vector subset
    vectors = [vector for vector, truth in zip(matrixii, truths) if truth == index]
    horizontals, verticals = list(zip(*vectors))

    # plot
    label = 'testing, {}'.format(list(sites.keys())[index])
    pyplot.plot(horizontals, verticals, color, label=label)

# save plot
pyplot.legend()
pyplot.savefig('svn.png')
pyplot.clf()

# begin decision boundary plot
print('plotting 2-D decision boundary...')
pyplot.clf()
pyplot.title('Decision Boundary')
pyplot.xlabel(labels[0])
pyplot.ylabel(labels[1])

# determine abscissa data range
concatenation = numpy.concatenate([matrix[:, 0], matrixii[:, 0]], axis=0)
abscissa = (min(concatenation), max(concatenation))

# determine ordinate data range
concatenation = numpy.concatenate([matrix[:, 1], matrixii[:, 1]], axis=0)
ordinate = (min(concatenation), max(concatenation))

# create horizontal coordinates
horizontals = numpy.array(numpy.stack([numpy.linspace(0, 99, 100)] * 100, axis=1))
horizontals = horizontals.flatten()
horizontals = horizontals * ((abscissa[1] - abscissa[0]) / 100) + abscissa[0]

# create vertical coordinates
verticals = numpy.array(numpy.stack([numpy.linspace(0, 99, 100)] * 100, axis=1))
verticals = numpy.transpose(verticals, axes=(1, 0)).flatten()
verticals = verticals * ((ordinate[1] - ordinate[0]) / 100) + ordinate[0]

# concatenate for 2-D background coordinates
background = numpy.stack([horizontals, verticals], axis=1)

# use pca to expand into all dimensions
expansion = decomposer.inverse_transform(background)
decisions = machine.predict(expansion)

# plot decision boundary
colors = ['r+', 'g+']
for index, color in enumerate(colors):

    # get vector subset
    vectors = [vector for vector, decision in zip(background, decisions) if decision == index]
    horizontals, verticals = list(zip(*vectors))

    # plot
    label = '{}'.format(list(sites.keys())[index])
    pyplot.plot(horizontals, verticals, color, label=label)

# save plot
pyplot.legend()
pyplot.savefig('decisions.png')
pyplot.clf()
