#!/usr/bin/env python3

# buckets.py for manipulating AWS S3 buckets

# import boto
import boto3


# upload into a bucket
def upload(path=None, bucket=None, destination=None):
    """Upload a file into an S3 bucket.

    Arguments:
        path: str, local file path
        bucket: str, name of bucket
        destination: str, S3 file path

    Returns:
        None
    """

    # set defaults
    bucket = bucket or 'mbandeltestbucket'
    path = '../studies/tempest/bucket/test.txt'
    destination = 's3://mbandeltestbucket/test/test.txt'

    # create client
    client = boto3.client('s3')

    # upload file
    client.upload_file(path, bucket, destination)

    return None