#!/usr/bin/env python3

# diatoms.py for the Diatom class to build omi climatology

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import netcdf4
import netCDF4

# import random forest
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier

# # import netcdf4
# import netCDF4

# import PIL for images
import PIL

# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    #matplotlib.use('Tkagg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Diatom to do OMOCNUV analysis
class Diatom(Hydra):
    """Diatom class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # create squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Diatom instance at {} >'.format(self.sink)

        return representation

    def _calculate(self, coefficients, references, pixels):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            pixels: int, number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        # get relevant dimensions
        orbits, rows, degree = coefficients.shape
        print('orbits: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))
        self._stamp('calculating wavelengths...', initial=True)

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns] * rows)
        indices = numpy.array([image] * orbits)

        # expand tensor of column references
        references = numpy.array([references] * rows)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(degree)])
        powers = powers.transpose(1, 2, 3, 0)

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)
        coefficients = coefficients.transpose(1, 2, 0, 3)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=3)

        # timestamp
        self._stamp('calculated.')

        return wavelengths

    def _compose(self, grid, cells, fill, condition=None):
        """Fill the empty spaces in a grid by weighted average of neighbors.

        Arguments:
            grid: numpy array, 2-D data grid with gaps.
            cells: length of cells
            fill: float, fill value for remaining gaps
            condition: function object, true for valid data

        Returns:
            numpy array
        """

        # construct condition as valid numbers
        def validating(array): return numpy.isfinite(array)
        condition = condition or validating

        # construct shifts
        shifts = list(range(-cells, cells + 1))
        shiftsii = list(range(-cells, cells + 1))

        # collect weights and rolls
        rolls = []
        weights = []

        # for each vertical shift
        for shift in shifts:

            # and each horizontal shift
            for shiftii in shiftsii:

                # roll array
                roll = numpy.roll(numpy.roll(grid, shift=shift, axis=0), shift=shiftii, axis=1)
                rolls.append(roll)

                # calculate weight
                weight = 1 / numpy.sqrt(shift ** 2 + shiftii ** 2)

                # if both are 0, weight as 2
                if (shift == 0) and (shiftii == 0):

                    # weigh as 2
                    weight = 2

                # append weight
                weights.append(weight)

        # # get all grids 1 pixel away
        # ones = [numpy.roll(grid, shift=1, axis=0), numpy.roll(grid, shift=-1, axis=0)]
        # ones += [numpy.roll(grid, shift=1, axis=1), numpy.roll(grid, shift=-1, axis=1)]
        #
        # # get all diagonals
        # diagonals = [numpy.roll(numpy.roll(grid, shift=1, axis=0), shift=1, axis=1)]
        # diagonals += [numpy.roll(numpy.roll(grid, shift=1, axis=0), shift=-1, axis=1)]
        # diagonals += [numpy.roll(numpy.roll(grid, shift=-1, axis=0), shift=-1, axis=1)]
        # diagonals += [numpy.roll(numpy.roll(grid, shift=-1, axis=0), shift=1, axis=1)]
        #
        # # get all grids 2 pixels away
        # twos = [numpy.roll(grid, shift=2, axis=0), numpy.roll(grid, shift=-2, axis=0)]
        # twos += [numpy.roll(grid, shift=2, axis=1), numpy.roll(grid, shift=-2, axis=1)]
        #
        # # get all two step diagonals
        # diagonalsii = [numpy.roll(numpy.roll(grid, shift=2, axis=0), shift=2, axis=1)]
        # diagonalsii += [numpy.roll(numpy.roll(grid, shift=2, axis=0), shift=-2, axis=1)]
        # diagonalsii += [numpy.roll(numpy.roll(grid, shift=-2, axis=0), shift=-2, axis=1)]
        # diagonalsii += [numpy.roll(numpy.roll(grid, shift=-2, axis=0), shift=2, axis=1)]
        #
        # # get all grids 3 pixels away
        # threes = [numpy.roll(grid, shift=3, axis=0), numpy.roll(grid, shift=-3, axis=0)]
        # threes += [numpy.roll(grid, shift=3, axis=1), numpy.roll(grid, shift=-3, axis=1)]
        #
        # # get all three step diagonals
        # diagonalsiii = [numpy.roll(numpy.roll(grid, shift=3, axis=0), shift=3, axis=1)]
        # diagonalsiii += [numpy.roll(numpy.roll(grid, shift=3, axis=0), shift=-3, axis=1)]
        # diagonalsiii += [numpy.roll(numpy.roll(grid, shift=-3, axis=0), shift=-3, axis=1)]
        # diagonalsiii += [numpy.roll(numpy.roll(grid, shift=-3, axis=0), shift=3, axis=1)]
        #
        # # get all grids 4 pixels away
        # fours = [numpy.roll(grid, shift=4, axis=0), numpy.roll(grid, shift=-4, axis=0)]
        # fours += [numpy.roll(grid, shift=4, axis=1), numpy.roll(grid, shift=-4, axis=1)]
        #
        # # create weights
        # scheme = (1, 1 / numpy.sqrt(2), 1 / 2, 1 / (2 * numpy.sqrt(2)), 1 / 3, 1 / (3 * numpy.sqrt(2)), 1 / 4)
        # weights = []
        # for weight in scheme:
        #
        #     # append 4 of scheme
        #     weights += [weight] * 4

        # create array
        weights = numpy.array(weights).reshape(-1, 1, 1)

        # construct stack
        stack = numpy.array(rolls)

        # take weighted average
        mask = condition(stack)
        summation = numpy.where(mask, stack, 0).sum(axis=0)
        weights = (mask.astype(int) * weights).sum(axis=0)
        average = summation / weights

        # create the composite
        composite = numpy.where(condition(grid), grid, average)

        # fill in remaining gaps
        composite = numpy.where(condition(composite), composite, fill)

        return composite

    def _compress(self, corners, trackwise, rowwise):
        """Compress the latitude or longitude bounds based on compression factors.

        Arguments:
            corners: dict of corner arrays
            trackwise: north-south compression factor
            rowwise: east-west compression factor

        Returns:
            numpy array
        """

        # set directions
        compasses = ['southwest', 'southeast', 'northeast', 'northwest']

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = corners[compass][:, :, 1]
        #     corners[compass][:, :, 1] = numpy.where(corner < 0, corner + 360, corner)

        # get shape
        shape = corners['southwest'].shape

        # get selected trackwise indices and rowwise indices
        selection = numpy.array([index for index in range(shape[0]) if index % trackwise == 0])
        selectionii = numpy.array([index for index in range(shape[1]) if index % rowwise == 0])

        # adjust based on factors
        adjustment = numpy.array([min([index + trackwise - 1, shape[0] - 1]) for index in selection])
        adjustmentii = numpy.array([min([index + rowwise - 1, shape[1] - 1]) for index in selectionii])

        # begin compression
        compression = {}

        # adjust southwest
        compression['southwest'] = corners['southwest'][selection]
        compression['southwest'] = compression['southwest'][:, selectionii]
        compression['southeast'] = corners['southeast'][selection]
        compression['southeast'] = compression['southeast'][:, adjustmentii]
        compression['northeast'] = corners['northeast'][adjustment]
        compression['northeast'] = compression['northeast'][:, adjustmentii]
        compression['northwest'] = corners['northwest'][adjustment]
        compression['northwest'] = compression['northwest'][:, selectionii]

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = compression[compass][:, :, 1]
        #     compression[compass][:, :, 1] = numpy.where(corner >= 180, corner - 360, corner)

        return compression

    def _convolve(self, flux, spectrum, wavelength, wavelengthii):
        """Produce the convolution.

        Arguments:
            flux: numpy array, the flux
            spectrum: numpy array, weighted spectrum
            wavelength: numpy array, the wavelengths
            wavelengthii: numpy array, spectral wavelengths

        Returns:
            numpy array
        """

        # prepare convolution
        convolution = numpy.zeros(wavelength.shape)

        # reset second wavelengths
        spectrum = numpy.array([intensity for intensity, wave in zip(spectrum, wavelengthii) if wave in wavelength])
        wavelengthii = numpy.array([wave for wave in wavelengthii if wave in wavelength])

        print(wavelength.shape)
        print(wavelengthii.shape)
        print(spectrum.shape)
        print(flux.shape)

        # go through each wavelength
        for index, wave in enumerate(wavelength):

            # begin integration
            integral = 0
            for indexii, waveii in enumerate(wavelengthii):

                # try to
                try:

                    # add component
                    delta = 1
                    element = flux[indexii] * spectrum[index - indexii] * delta
                    integral += element

                # unless out of bounds
                except IndexError:

                    # in which case, pass
                    pass

            # set convolution to integral
            convolution[index] = integral

        print('convolution:', convolution.shape)

        return convolution

    def _double(self, array):
        """Use linear interpolation to double the points in an array.

        Arguments:
            array

        Returns:
            numpy.array
        """

        # create averages from middle points
        middles = [(one + two) / 2 for one, two in zip(array[:-1], array[1:])]

        # combine with original
        pairs = [(end, middle) for end, middle in zip(array, middles)]

        # create zippered list
        double = [member for pair in pairs for member in pair] + [array[-1]]
        double = numpy.array(double)

        return double

    def _imbibe(self, bar):
        """Convert a colorbar image into colorbar colors.

        Arguments:
            bar: str, filename to colorbar

        Returns:
            None
        """

        # get colorbar image
        image = PIL.Image.open(bar)
        array = numpy.array(image)

        # get mid point
        half = int(array.shape[0] / 2)

        # get all colors
        colors = array[half, :, :3]

        # write to file
        text = [' '.join([str(channel / 255) for channel in line]) for line in colors]
        destination = bar.replace('.png', '.txt')
        self._jot(text, destination)

        return None

    def _paint(self, arrays, path, texts, scale, spectrum, limits, grid=1, lines=None, **options):
        """Make matplotlib polygon figure.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            texts: list of str, title and units
            path: str, filepath to destination
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            lines: list of (array, array, str) tuples (x, y, style)
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                north: boolean, use north projection?
                projection: name of projection
                extension: str, the colorbar ends, 'neither', 'min', 'max', 'both'
                coast: int, thickness of coastline
                tight: boolean, use tight layout?

        Returns:
            None
        """

        # set defaults
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        bar = options.get('bar', True)
        log = options.get('log', False)
        north = options.get('north', True)
        extension = options.get('extension', 'both')
        coast = options.get('coast', 2)
        font = options.get('font', 14)
        fontii = options.get('fontii', 10)
        tight = options.get('tight', False)

        # set projections
        native = cartopy.crs.PlateCarree()
        projection = options.get('projection', cartopy.crs.PlateCarree())

        # set default lines
        lines = lines or []

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        gradient, selection = spectrum

        # unpack texts, adding default labels
        texts = texts + ['', '']
        title, units, dependent, independent = texts[:4]

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set cartopy axis with coastlines
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=coast)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # set gridlines
        # grid = axis.gridlines(draw_labels=True, dms=False, x_inline=False, y_inline=False)
        grid = axis.gridlines(draw_labels=True)
        grid.top_labels = False
        grid.right_labels = False
        grid.xlabel_style = {'size': fontii}
        grid.ylabel_style = {'size': fontii}

        # Adjust margins around the figure
        left_margin = 0.18
        right_margin = 0.05
        top_margin = 0.15
        bottom_margin = 0.25

        # if tight layout
        if tight:

            # make layout tight
            left_margin = 0.10

        # set margines
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_xlim(limits[2], limits[3])
        axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=font)

        # if a dependent label is given
        if dependent:

            # add a manual axis for figure text
            pyplot.gcf().text(0.04, 0.5, dependent, va='center', ha='center', rotation='vertical', fontsize=18)

        # construct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]
                patch = numpy.array(patch)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # try
        try:

            # set up color scheme, removing first two
            colors = matplotlib.cm.get_cmap(gradient)
            selection = numpy.array(selection)

            # set up color map
            colors = matplotlib.colors.ListedColormap(colors(selection))

        # except
        except ValueError:

            # set up color scheme, removing first two
            text = self._know(gradient)
            tuples = [tuple([float(channel) for channel in line.split()]) for line in text]

            # set up color map
            colors = matplotlib.colors.ListedColormap(tuples)

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)
        optionsii = {'cmap': colors, 'alpha': 1.0}

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(scale[0], scale[1])
            optionsii = {'cmap': colors, 'alpha': 1.0, 'norm': normal}

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, **optionsii)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        collection = axis.add_collection(collection)

        # for each line
        for line in lines:

            # plot line
            pyplot.gca().plot(line[0], line[1], 'k--')

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.1, position.width, 0.02])

            # if logarithm
            if log:

                # define formatter
                def formatting(value, index): return '{:.2g}'.format(value) if value < 1 else '{}'.format(int(value))

                # use logarithmic color bar
                options = {'cax': barii, 'orientation': 'horizontal', 'extend': extension}
                options.update({'format': matplotlib.ticker.FuncFormatter(formatting)})
                colorbar = pyplot.gcf().colorbar(scalar, **options)
                colorbar.update_ticks()

            # otherwise
            else:

                # use regular
                colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)

            # set label
            colorbar.set_label(units, labelpad=5, size=fontii)
            colorbar.ax.tick_params(labelsize=fontii)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path, dpi=300)
        pyplot.clf()

        return None

    def _plant(self, data, target, destination, header=None, classify=True):
        """Use random forest to predict target value from data

        Arguments:
            data: dict of str, numpy array
            target: str, name of target variable
            destination: str, pathname for destination
            header: header for report
            classify: boolean, use classifier?

        Returns:
            None
        """

        # assemble matrix
        names = [name for name in data.keys() if name != target] + [target]
        train = numpy.array([data[name] for name in names]).transpose(1, 0)

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # begin report
        report = header or ['Random forest for {}'.format(target)]
        report.append('')

        # if classifier
        if classify:

            # run random forest
            self._stamp('running random forest for {}...'.format(target), initial=True)
            self._print('matrix: {}'.format(matrix.shape))
            forest = RandomForestClassifier(n_estimators=100, max_depth=5)
            forest.fit(matrix, truth)

        # otherwise
        else:

            # run random forest
            self._stamp('running random forest for {}...'.format(target), initial=True)
            self._print('matrix: {}'.format(matrix.shape))
            forest = RandomForestRegressor(n_estimators=100, max_depth=5)
            forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(names, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        self._jot(report, destination)

        # print status
        self._stamp('planted.')

        return None

    def _polarize(self, arrays, path, texts, scale, spectrum, limits, grid=1, **options):
        """Make matplotlib polygon figure, using a polar projection.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            path: str, filepath to destination
            texts: list of str, title and units
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                south: boolean, use south projection?

        Returns:
            None
        """

        # set default options
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        log = options.get('log', False)
        bar = options.get('bar', False)
        south = options.get('south', False)
        extension = options.get('extension', 'both')

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set native and projection coordinates
        native = cartopy.crs.PlateCarree()
        projection = cartopy.crs.NorthPolarStereo()
        if south:

            # set to northern projection
            projection = cartopy.crs.SouthPolarStereo()

        # set cartopy axis with coastlines
        # axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
        axis = pyplot.axes(projection=projection)
        # Set the extent for the polar plot
        # axis.set_extent([-180, 180, 60, 90], crs=native)
        # feature = cartopy.feature.NaturalEarthFeature
        # coast = feature(category='physical', name='coastline', facecolor='none', scale='10m')
        # axis.add_feature(coast, edgecolor='gray')
        # axis.add_feature(cartopy.feature.COASTLINE, linewidth=1.0, edgecolor='black')
        # _ = axis.gridlines(draw_labels=True, crs=cartopy.crs.PlateCarree())
        _ = axis.gridlines(draw_labels=True)
        axis.coastlines(linewidth=1, zorder=5)
        # axis.add_feature(cartopy.feature.BORDERS, linestyle='-', alpha=1)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left_margin = 0.1
        right_margin = 0.1
        top_margin = 0.19
        bottom_margin = 0.18
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_extent(limits, crs=native)

        # axis.set_xlim(limits[2], limits[3])
        # axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=10)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]

                # begin polar coordinates
                polar = []
                for point in patch:

                    # convert to polar
                    radius = numpy.sqrt(point[0] ** 2 + point[1] ** 2)
                    theta = numpy.arctan2(point[1], point[1])
                    # polar.append((radius, theta))

                    # add to polar
                    polar.append(projection.transform_point(*point, native))

                # convert to polar coordinates
                # patch = numpy.array(polar) / 111320
                # patch = numpy.array(polar) / 100000
                patch = numpy.array(polar)

                # create polygon
                # patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01, transform=native)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # set up color scheme, removing first two
        colors = matplotlib.cm.get_cmap(gradient)
        # colors = colors[2:]

        # remove first two colors (magenta and light purple) from front of colormap
        # start = 40
        # selection = numpy.linspace(start, colors.N, colors.N - start + 1).astype(int)
        selection = numpy.array(selection)
        colors = matplotlib.colors.ListedColormap(colors(selection))

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        # collection.set_transform(native)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.08, position.width, 0.02])
            #colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend=extension)
            colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None

    def _pull(self, tag=None, grid=False, duel=False, replicant=False):
        """Retrieve grid file

        Arguments:
            tag: str, tag for lattice
            grid: boolean, grid file?
            duel: boolean, duel file?

        Returns:
            str, pathname
        """

        # if pulling grid file:
        if grid:

            # create hydra for grid file with tag in path
            hydra = Hydra('{}/data/grid'.format(self.sink))
            paths = [path for path in hydra.paths if (tag or '') in path]
            paths.sort()
            hydra.ingest(paths[0])

        # if pulling duel file
        elif duel:

            # create hydra for grid file with tag in path
            hydra = Hydra('{}/data/duel'.format(self.sink))
            paths = [path for path in hydra.paths if (tag or '') in path]
            paths.sort()
            hydra.ingest(paths[0])

        # if pulling replicant file
        elif replicant:

            # create hydra for grid file with tag in path
            hydra = Hydra('{}/data/replication'.format(self.sink))
            paths = [path for path in hydra.paths if (tag or '') in path]
            paths.sort()
            hydra.ingest(paths[0])

        # otherwise
        else:

            # create hydra for erythema file with tag in path
            hydra = Hydra('../studies/diatom/erythema')
            paths = [path for path in hydra.paths if (tag or '') in path]
            paths.sort()
            hydra.ingest(paths[0])

        return hydra

    def _smooth(self, wave, wavelength, radiance, half=1.0):
        """Identify radiance at particular wavelength by using trigular smoothg.

        Arguments:
            wave: wavelength in question
            wavelength: numpy array of wavelengths
            rediance: numpy array of radiances
            half: float, size of window, 1-sided

        Returns:
            numpy array
        """

        # create weights based on distance from wavelength
        difference = wavelength - wave
        absolute = abs(difference)
        mask = (absolute < half).astype(int)
        weight = mask * (1 - absolute / half)

        # multiple by radiance and sum
        triangle = (weight * radiance).sum(axis=2) / weight.sum(axis=2)

        return triangle, weight

    def absorb(self, year=2005, month=3, day=21, orbit=3630, number=20):
        """Check that interpolating aerosol values across heights leads to final values.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: int, orbit number

        Returns:
            None
        """

        # create hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/70004/OMAERUV/{}/{}/{}'.format(*formats))
        hydra.ingest(str(orbit))

        # gather data
        wavelength = hydra.grab('Wavelengths')
        layer = hydra.grab('layer')
        heights = hydra.grab('FinalAerosolLayerHeight')
        finals = hydra.grab('FinalAerosolAbsOpticalDepth')[:, :, 2]
        aerosols = hydra.grab('AerosolAbsOpticalDepthVsHeight')[:, :, :, 2]

        # create mask for non zero aerosol
        mask = (finals > 0) & (heights > 0)
        mask = (finals > 0) & (abs(finals) < 1e20) & (aerosols.sum(axis=2) > 0)

        # for each pixel
        differences = []
        zipper = list(zip(finals[mask], heights[mask], aerosols[mask]))
        for final, height, aerosol in zipper[:number]:

            # make interpolator
            interpolator = scipy.interpolate.interp1d(layer, aerosol, kind='linear')

            # try to
            try:

                # interpolate height
                interpolation = interpolator(height)

            # unless out of range
            except ValueError:

                # get 3km aerorosl
                interpolation = aerosol[2]

            # calculate difference
            difference = interpolation - final
            differences.append(difference)

            # print
            self._print(difference, interpolation, final, height, aerosol)

        # print biggest difference
        self._print(abs(numpy.array(differences)).max())

        return None

    def adorn(self, source=None):
        """Augment the ocean color files with land sea mask.

        Arguments:
            source: str, file path to source directory

        Returns:
            None
        """

        # set source default and create hydra
        source = source or '../studies/diatom/cardinals'
        hydra = Hydra(source)

        # for each path
        for path in hydra.paths:

            # get the orbit number and day
            orbit = str(int(str(self._stage(path)['orbit'])))
            date = self._stage(path)['day']
            year = date[:4]
            month = date[5:7]
            day = date[7:9]

            # begin features
            features = []

            # get the vis radiance file
            hydraii = Hydra('/tis/acps/OMI/70004/OML1BRVG/{}/{}/{}'.format(year, month, day))
            pathii = [entry for entry in hydraii.paths if orbit in entry][0]

            # get the groundpixel quality data
            hydraii.ingest(pathii)
            water = hydraii.grab('BAND3/water_fraction').squeeze()
            feature = Feature(['Geolocation', 'water_fraction'], water)
            features.append(feature)

            # add the latitude bounds
            hydraii.ingest(pathii)
            bounds = hydraii.grab('BAND3/latitude_bounds').squeeze()
            feature = Feature(['Geolocation', 'latitude_bounds'], bounds)
            features.append(feature)

            # add the longitude bounds
            hydraii.ingest(pathii)
            boundsii = hydraii.grab('BAND3/longitude_bounds').squeeze()
            feature = Feature(['Geolocation', 'longitude_bounds'], boundsii)
            features.append(feature)

            # get the snow / ice radiance file
            hydraii = Hydra('/tis/acps/OMI/70004/OMVSNOICE/{}/{}/{}'.format(year, month, day))
            pathii = [entry for entry in hydraii.paths if orbit in entry][0]

            # get the groundpixel quality data
            hydraii.ingest(pathii)
            snow = hydraii.grab('snow_fraction').squeeze()
            ice = hydraii.grab('ice_fraction').squeeze()
            feature = Feature(['Geolocation', 'snow_ice_fraction'], snow + ice)
            features.append(feature)

            # get the omto3 file for reflectivity
            hydraii = Hydra('/tis/acps/OMI/92005/OMTO3/{}/{}/{}'.format(year, month, day))
            pathii = [entry for entry in hydraii.paths if orbit in entry][0]

            # get the groundpixel quality data
            hydraii.ingest(pathii)
            reflectivity = hydraii.grab('Reflectivity380').squeeze()
            feature = Feature(['Geolocation', 'reflectivity_380'], reflectivity)
            features.append(feature)

            # augmenet the path
            hydra.augment(path, features, path)

        return None

    def aerate(self, point=(-4, -81, 0.1)):
        """Create aerosol interpolation plots.

        Arguments:
            point: tuple of float, latitude and longitude and aerosol index

        Returns:
            None
        """

        # make aerosol plot folder
        self._make('{}/plots/aeration'.format(self.sink))

        # get omi data
        hydra = Hydra('/tis/acps/OMI/70004/OMAERUV/2021/03/21')
        hydra.ingest('2021m0321t1901-o088737')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        aerosol = hydra.grab('FinalAerosolAbsOpticalDepth')[:, :, :2]
        wavelength = hydra.grab('Wavelengths')[:2]

        # get pace data
        hydraii = Hydra('../studies/diatom/simulation/ten')
        hydraii.ingest('20220321T182542.MODEL_INPUTS')
        latitudeii = hydraii.grab('latitude')
        longitudeii = hydraii.grab('longitude')
        aerosolii = numpy.array([hydraii.grab('aot_550'), hydraii.grab('aot_865')]).transpose(1, 2, 0)
        wavelengthii = numpy.array([550, 865])

        # find closest omi pixel
        pixel = self._pin(point, [latitude, longitude, aerosol[:, :, 0]])[0]

        # find closest pace pixel
        pointii = (latitude[pixel], longitude[pixel])
        pixelii = self._pin(pointii, [latitudeii, longitudeii])[0]

        # make wavelengths
        waves = numpy.linspace(290, 399, 110)

        # interpolate using omi
        exponent = numpy.array([(wave - wavelength[0]) / (wavelength[1] - wavelength[0]) for wave in waves])
        tau = aerosol[pixel][0] * (aerosol[pixel][1] / aerosol[pixel][0]) ** exponent
        logarithm = numpy.log10(tau)

        # interpolate using pace
        exponentii = numpy.array([(wave - wavelengthii[0]) / (wavelengthii[1] - wavelengthii[0]) for wave in waves])
        tauii = aerosolii[pixelii][0] * (aerosolii[pixelii][1] / aerosolii[pixelii][0]) ** exponentii
        logarithmii = numpy.log10(tauii)

        # plot omi tau
        title = 'OMI Tau'
        address = 'plots/aeration/OMI_Tau.h5'
        self.squid.ink('tau', tau, 'wavelength', waves, address, title)

        # plot omi tau log
        title = 'OMI Tau Log'
        address = 'plots/aeration/OMI_Tau_Log.h5'
        self.squid.ink('tau_log', logarithm, 'wavelength', waves, address, title)

        # plot omi tau
        title = 'PACE Tau'
        address = 'plots/aeration/PACE_Tau.h5'
        self.squid.ink('tau', tauii, 'wavelength', waves, address, title)

        # plot omi tau log
        title = 'PACE Tau Log'
        address = 'plots/aeration/PACE_Tau_Log.h5'
        self.squid.ink('tau_log', logarithmii, 'wavelength', waves, address, title)

        # plot omi tau
        pair = aerosol[pixel]
        title = 'OMI Tau points'
        address = 'plots/aeration/OMI_Tau_points.h5'
        self.squid.ink('tau', pair, 'wavelength', wavelength, address, title)

        # plot omi tau log
        pairii = numpy.log10(pair)
        title = 'OMI Tau Log points'
        address = 'plots/aeration/OMI_Tau_Log_points.h5'
        self.squid.ink('tau_log', pairii, 'wavelength', wavelength, address, title)

        # plot omi tau
        pair = aerosolii[pixelii]
        title = 'PACE Tau points'
        address = 'plots/aeration/PACE_Tau_points.h5'
        self.squid.ink('tau', pair, 'wavelength', wavelengthii, address, title)

        # plot omi tau log
        pairii = numpy.log10(pair)
        title = 'PACE Tau Log points'
        address = 'plots/aeration/PACE_Tau_Log_points.h5'
        self.squid.ink('tau_log', pairii, 'wavelength', wavelengthii, address, title)

        return None

    def aerify(self, tags=('free', 'cloud'), month=3, wave=305, resolution=20, give=False, bar=True, suffix=''):
        """Create aerosol and aerosol free difference plots.

        Arguments:
            *words: unpacked tuple of field ames
            tags: tuple of str, name of tags to plot
            month: int, the month
            wave: int, wavelength
            resolution: int, number of vertical pixels to coagulate
            give: boolean, return data collection?
            bar: boolean, add colorbar?
            suffix: str, suffix for reservoirs

        Returns:
            None
        """
        # set fill
        fill = -999

        # set default latiude zone
        zone = (-60, 60)

        # set default words
        words = ['ultraviolet', 'damage', 'aerosol']

        # set dates
        dates = {3: 'March 21, 2005', 6: 'June 21, 2005'}
        dates.update({9: 'September 21, 2005', 12: 'December 21, 2005'})
        # dates.update({'colorbars': '2005-06-21'})

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity_360', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure', 'bits': 'bit_flag'})
        fields.update({'aerosol': 'aerosol_absorption_optical_depth_354'})
        fields.update({'aerosolii': 'aerosol_absorption_optical_depth_388'})
        fields.update({'absorption': 'aerosol_absorption_factor'})
        fields.update({'algorithm': 'algorithm_flag', 'quality': 'quality_flag_surface'})
        fields.update({'qualityii': 'quality_flag_underwater'})

        # create stubs
        stubs = {'sky': 'Clear Sky Irradiance, {}nm'.format(wave), 'cloud': 'Cloud Optical Depth, 360nm'}
        stubs.update({'residue': 'Residue 331', 'ozone': 'Total Column Ozone'})
        stubs.update({'coefficient': 'Planar Diffuse Coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'Chlorophyll'})
        stubs.update({'surface': 'Planar Surface Irradiance, {}nm'.format(wave), 'reflectance': 'N Value 360'})
        stubs.update({'reflectivity': 'Reflectivity, 360nm', 'anomaly': 'Row Anomaly'})
        stubs.update({'ultraviolet': 'Ultraviolet Index'})
        stubs.update({'underwater': 'Underwater Irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': '10 % Penetration Depth, zp, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA Damage Dose Rate at 5m Depth'})
        stubs.update({'pressure': 'Surface Pressure'})
        stubs.update({'aerosol': 'Aerosol Absorption Optical Depth 354nm', })
        stubs.update({'aerosolii': 'Aerosol Absorption Optical Depth 388nm', })
        stubs.update({'absorption': 'Aerosol Absorption Factor, {}nm'.format(wave)})
        stubs.update({'climatology': 'Aerosol Climatology Used'})
        stubs.update({'exponent': 'Aerosol Absorption Exponent'})

        # create uits
        units = {'sky': '$mW / m^2 s$', 'cloud': ' ', 'residue': ' ', 'ozone': 'DU'}
        units.update({'coefficient': '1 / m', 'chlorophyll': '$mg / m^3$'})
        units.update({'surface': '$mW / m^2 s$', 'reflectance': ' ', 'reflectivity': ' '})
        units.update({'ultraviolet': 'UVI', 'underwater': '$mW / m^2 s$'})
        units.update({'depth': '$z_10%$ 305nm ( m )', 'damage': '$mW / m^2 s$', 'pressure': 'atm'})
        units.update({'aerosol': '$AAOD$ 354nm', 'aerosolii': ' ', 'absorption': ' '})
        units.update({'climatology': ' ', 'exponent': ' '})

        # declare color scale ranges
        scales = {'sky': (0, 120), 'cloud': (0, 40)}
        scales.update({'residue': (-10, 10), 'ozone': (230, 330)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0, 1)})
        scales.update({'surface': (0, 120), 'reflectance': (50, 150)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 15)})
        scales.update({'underwater': (0, 120)})
        scales.update({'depth': (0, 35)})
        scales.update({'damage': (0, 200)})
        scales.update({'pressure': (0.96, 1.04)})
        scales.update({'aerosol': (0, 0.15)})
        scales.update({'aerosolii': (0, 0.15)})
        scales.update({'absorption': (0.5, 1.0)})
        scales.update({'climatology': (0, 1)})
        scales.update({'exponent': (0, 5)})

        # set quality flag criteria
        qualities = {'surface': 'quality', 'sky': 'quality', 'aerosol': 'quality', 'aerosolii': 'quality'}
        qualities.update({'absorption': 'quality', 'cloud': 'quality', 'reflectivity': 'quality', 'ozone': 'quality'})
        qualities.update({'climatology': 'quality', 'exponent': 'quality', 'damage': 'qualityii'})
        qualities.update({'ultraviolet': 'quality'})

        # # set background colors
        # background = {'aerosol': 'white', 'aerosolii': 'white', 'exponent': 'white'}

        # set fields for logarithmic scale
        # logarithms = {'coefficient': True, 'chlorophyll': True}
        # logarithms = {'chlorophyll': True}
        # logarithms = {}

        # # set up transforms to convert from logs and replace fill values
        # def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        # transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        # transforms.update({'coefficient': powering})

        # begin data reservoir
        reservoir = {}

        # for each tag
        for tag in tags:

            # make folder based on tags
            self._make('{}/paper/plots{}'.format(self.sink, suffix))
            self._make('{}/paper/plots{}/{}'.format(self.sink, suffix, tag))

            # create omi hydra
            hydra = Hydra('{}/paper/granules{}/{}'.format(self.sink, suffix, tag))

            # grab spectral wavelength
            hydra.ingest(0)
            wavelength = hydra.grab('spectral_wavelength')
            position = hydra._pin(wave, wavelength)[0][0]

            # create positions for third index of 3-d data
            positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2}
            positions.update({'underwater': position, 'depth': position, 'absorption': position})

            # get the DNA Spectrum
            spectra = Hydra('{}/spectra'.format(self.sink))
            spectra.ingest(1)
            spectrum = spectra.grab('spectrum')
            spectrum = spectrum.reshape(1, 1, -1)

            # get subset of paths based on month
            paths = [path for path in hydra.paths if '_2005m{}'.format(self._pad(month)) in path]
            paths.sort()

            # begin data reservoir
            data = {field: [] for field in fields.keys()}

            # for each orbit
            for path in paths:

                # print
                self._print('collecting {}...'.format(path))

                # ingest
                hydra.ingest(path)

                # add data
                [data[field].append(hydra.grab(search)) for field, search in fields.items()]

            # stack all arrays
            data = {field: numpy.vstack(data[field]) for field in data.keys()}

            # calculate dna damage at 5m
            underwater = data['underwater']
            coefficient = data['coefficient']
            irradiance = underwater * numpy.exp(-coefficient * 5)
            damage = (irradiance * spectrum).sum(axis=2)
            data['damage'] = damage

            # calculate penetration depths
            data['depth'] = -1 * numpy.log(0.1) / data['coefficient']

            # calculate underwater at 5 meters
            data['underwater'] = data['underwater'] * numpy.exp(-5 * data['coefficient'])

            # add aerosol climatology flag
            data['climatology'] = data['bits'] & 1

            # calculate aerosol absorption exponent
            data['exponent'] = numpy.log(data['aerosol'] / data['aerosolii']) / numpy.log(388 / 354)

            # mask data by aerosol > 0
            mask = data['aerosol'] > 0
            data['exponent'] = numpy.where(mask, data['exponent'], fill)

            # subset positions
            data.update({field: data[field][:, :, position] for field, position in positions.items()})

            # add data to reservoir
            reservoir[tag] = data

        # create percent differences
        data = reservoir[tags[0]]
        dataii = reservoir[tags[1]]
        names = list(data.keys())
        reservoir['percent'] = {name: hydra._relate(data[name], dataii[name], percent=True) for name in names}

        # mask out percents where aersol is less than 0
        mask = (data['aerosol'] > 0) & (dataii['aerosol'] > 0)
        reservoir['percent']['ultraviolet'] = numpy.where(mask, reservoir['percent']['ultraviolet'], fill)
        reservoir['percent']['damage'] = numpy.where(mask, reservoir['percent']['damage'], fill)

        # set up color gradients
        spectrum = '{}/colorbars/aerosol_colorbar.txt'.format(self.sink)
        gradients = {'ozone': 'viridis', 'surface': 'plasma', 'chlorophyll': 'viridis', 'damage': 'plasma'}
        gradients.update({'aerosol': spectrum, 'absorption': 'plasma_r', 'exponent': 'viridis'})
        gradients.update({'depth': 'viridis', 'coefficient': 'viridis_r', 'ultraviolet': 'plasma'})

        # set up color selections
        # selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]
        selections = {}

        # set extensions
        # extensions = {'absorption': 'neither'}

        # set title suffixes
        suffixes = {tags[0]: 'without Aerosol', tags[1]: 'Daily Aerosol'}
        suffixes.update({'percent': '% Difference with Aerosol'})

        # for each field
        # words = words or list(stubs.keys())
        # words = ['aerosol']
        for field in words:

            # specify color gradient and particular indices
            gradient = gradients.get(field, 'gist_rainbow_r')
            selection = selections.get(field, range(256))

            # for each mode
            modes = [mode for mode in tags] + ['percent']
            # modes = [tags[1]]
            for mode in modes:

                # except for exclusions
                exclusions = ['latitude', 'longitude', 'anomaly', 'quality']
                if field not in exclusions:

                    # set omi components
                    tracer = reservoir[mode][field]
                    latitude = reservoir[tags[0]]['latitude']
                    longitude = reservoir[tags[0]]['longitude']
                    anomaly = reservoir[tags[0]]['anomaly']
                    bits = reservoir[tags[0]]['bits']

                    # create masks for finite data
                    ascending = ((bits & 256) == 0) & (data[qualities.get(field, 'qualityii')] < 3)
                    mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (anomaly < 1) & (tracer > -999) & ascending

                    # set plot scale
                    scale = scales[field]

                    # set units
                    unit = units[field]

                    # if percent diff
                    if mode == 'percent':

                        # reset scale and unit and gradient
                        scale = (-40, 0)
                        unit = '%'
                        extension = 'min'
                        gradient = 'seismic'
                        gradient = 'BuPu_r'
                        gradient = 'CMRmap'
                        selection = [index for index in range(192)]

                    # clip at upper bounds
                    epsilon = 1e-8
                    tracer = numpy.where((tracer < scale[0] + epsilon), scale[0] + epsilon, tracer)
                    tracer = numpy.where((tracer > scale[1] - epsilon), scale[1] - epsilon, tracer)

                    # print(scale)
                    # print(tracer.max())
                    # print(tracer.min())

                    # replace masked values with fills
                    # latitude = numpy.where(mask, latitude, fill)
                    # longitude = numpy.where(mask, longitude, fill)
                    tracer = numpy.where(mask, tracer, fill)

                    # pixels = self._pin([-10, -60], [latitude, longitude], 20)
                    # for pixel in pixels:
                    #     print(pixel, latitude[pixel], longitude[pixel], tracer[pixel])

                    # setup pace plot, full scale
                    title = '{}\n'.format(stubs[field])
                    formats = (self.sink, suffix, tags[1], field, mode, self._pad(month))
                    destination = '{}/paper/plots{}/{}/OMI_OMOCNUV_{}_{}_{}.png'.format(*formats)
                    limits = (*zone, -180, 180)

                    # limits = (-20, 10, -90, -30)

                    size = (14, 5)
                    logarithm = False
                    back = 'white'
                    # title = ''
                    extension = 'max'

                    # if percent
                    if mode == 'percent':

                        # extension is for both
                        extension = 'min'
                        title = '% Difference with Aerosol Correction\n'

                    # # troubleshooting
                    # destination = destination.replace('plots', 'trouble')

                    # plot
                    self._stamp('plotting {}...'.format(destination), initial=True)
                    parameters = [[tracer, latitude, longitude], destination, [title, unit, dates[month]]]
                    parameters += [scale, [gradient, selection], limits, resolution]
                    options = {'size': size, 'log': logarithm, 'back': back, 'north': False}
                    options.update({'bar': bar, 'extension': extension, 'coast': 1.5, 'font': 18, 'fontii': 14})
                    self._paint(*parameters, **options)

                    # print status
                    self._stamp('plotted.'.format(field))

        # default package to None
        package = None

        # if output requested
        if give:

            # set package to data
            package = data

        return package

    def air(self, resolution=50):
        """Compare aersol plots wth omaeruv vs merra.

        Arguments:
            resolution: int, size of pixels

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots/air'.format(self.sink))

        # get hydra
        hydra = Hydra('{}/air'.format(self.sink))

        # ingerst omaeruv and get surface irradiance
        hydra.ingest('omaeruv')
        surface = hydra.grab('surface_irradiance_planar')[:, :, 15]
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')

        # get merra-2
        hydra.ingest('merra')
        surfaceii = hydra.grab('surface_irradiance_planar')[:, :, 15]

        # get difference
        difference = surfaceii - surface

        print(surface.min(), surface.max())
        print(surfaceii.min(), surfaceii.max())

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set scale and limts
        scale = (0, 200)
        scaleii = (-200, 200)
        limits = (-60, 60, 0, 90)
        size = (10, 5)
        logarithm = False
        bar = True

        # plot OMAERUV
        # title = '{}, MinLER Climatology, \n{}, o{}'.format(*formats)
        title = 'Surface Irradiance, 305nm, 2005m0210, OMAERUV'
        destination = '{}/plots/air/OMERUV_irradiance.png'.format(self.sink)
        unit = 'mW / m2 nm'
        parameters = [[surface, latitude, longitude], destination, [title, unit]]
        parameters += [scale, [gradient, selection], limits, resolution]
        options = {'size': size, 'log': logarithm, 'bar': bar, 'back': 'white', 'north': False}
        self._stamp('plotting {}...'.format(destination))
        self._paint(*parameters, **options)

        # plot Merra
        # title = '{}, MinLER Climatology, \n{}, o{}'.format(*formats)
        title = 'Surface Irradiance, 305nm, 2005m0210, Merra'
        destination = '{}/plots/air/Merra_irradiance.png'.format(self.sink)
        unit = 'mW / m2 nm'
        parameters = [[surfaceii, latitude, longitude], destination, [title, unit]]
        parameters += [scale, [gradient, selection], limits, resolution]
        options = {'size': size, 'log': logarithm, 'bar': bar, 'back': 'white', 'north': False}
        self._stamp('plotting {}...'.format(destination))
        self._paint(*parameters, **options)

        # plot Difference
        # title = '{}, MinLER Climatology, \n{}, o{}'.format(*formats)
        title = 'Surface Irradiance Difference, 305nm, 2005m0210, Merra - OMAERUV'
        destination = '{}/plots/air/Differece_irradiance.png'.format(self.sink)
        unit = 'mW / m2 nm'
        parameters = [[difference, latitude, longitude], destination, [title, unit]]
        parameters += [scaleii, [gradient, selection], limits, resolution]
        options = {'size': size, 'log': logarithm, 'bar': bar, 'back': 'white', 'north': False}
        self._stamp('plotting {}...'.format(destination))
        self._paint(*parameters, **options)

        return None

    def angle(self, resolution=10, degrees=(-72, 58)):
        """Map angles from different input files.

        Arguments:
            resolution: number of points to meld
            degrees: float, latitude degree bracket

        Returns:
            None
        """

        # make plot directory
        sink = 'plots/angles'
        self._make('{}/{}'.format(self.sink, sink))

        # set bounding brackets
        brackets = {}
        brackets['sza'] = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
        brackets['vza'] = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
        brackets['saa'] = [-180, -140, -100, -60, -20, 20, 60, 100, 140, 180]
        brackets['vaa'] = [-180, -140, -100, -60, -20, 20, 60, 100, 140, 180]
        brackets['raa'] = [-180, -140, -100, -60, -20, 20, 60, 100, 140, 180]
        brackets['percent'] = [-1.0, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1.0]

        # make reserovir to hold datasets
        reservoir = {}

        # make hydra
        hydra = Hydra('{}/data/angles'.format(self.sink))

        # ingest omto3
        hydra.ingest('OMTO3')

        # grab the latitude and longitude and bounds
        latitude = hydra.grab('Latitude')
        nadir = latitude[:, 30]
        mask = (nadir > -9999)

        # make mask for ascending indices
        minimum = self._pin(float(nadir[mask].min()), latitude)[0][0]
        maximum = self._pin(float(nadir[mask].max()), latitude)[0][0]

        # subset minimum to maximum
        latitude = latitude[minimum: maximum]
        longitude = hydra.grab('Longitude')[minimum: maximum]

        # create polygons from bounds
        polygons = self._polymerize(latitude, longitude, resolution)

        # get indices based on resolution
        selection = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])

        # apply selection and mask to latitude and longitude
        latitude = latitude[selection]
        longitude = longitude[selection]

        # mask the latitude at degrees
        mask = (latitude > degrees[0]) & (latitude < degrees[1])

        # apply mask to polygons and coordinates
        polygons = polygons[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]

        # set products
        products = ['OMTO3', 'OMGLERPRE', 'OML1BRUG', 'OML1BRVG']

        # set angles per product
        zeniths = ['SolarZenithAngle', 'SZA', 'BAND2/solar_zenith_angle', 'solar_zenith_angle']
        zenithsii = ['ViewingZenithAngle', 'VZA', 'BAND2/viewing_zenith_angle', 'viewing_zenith_angle']
        azimuths = ['SolarAzimuthAngle', 'SAA', 'BAND2/solar_azimuth_angle', 'solar_azimuth_angle']
        azimuthsii = ['ViewingAzimuthAngle', 'VAA', 'BAND2/viewing_azimuth_angle', 'viewing_azimuth_angle']
        relatives = ['RelativeAzimuthAngle', 'RAA', 'BAND2/viewing_azimuth_angle', 'viewing_azimuth_angle']

        # for each product
        for index, product in enumerate(products):

            # set reservoir
            reservoir[product] = {}

            # ingest
            hydra.ingest(product)

            # get data
            reservoir[product]['sza'] = hydra.grab(zeniths[index]).squeeze()[minimum: maximum, :]
            reservoir[product]['vza'] = hydra.grab(zenithsii[index]).squeeze()[minimum: maximum, :]
            reservoir[product]['saa'] = hydra.grab(azimuths[index]).squeeze()[minimum: maximum, :]
            reservoir[product]['vaa'] = hydra.grab(azimuthsii[index]).squeeze()[minimum: maximum, :]
            reservoir[product]['raa'] = hydra.grab(relatives[index]).squeeze()[minimum: maximum, :]

            # for each array
            for field in reservoir[product].keys():

                # apply selection and mask
                reservoir[product][field] = reservoir[product][field][selection]
                reservoir[product][field] = reservoir[product][field][mask]

                # make plot
                title = '{}, {}, orbit 2573, 2005-01-07'.format(field.upper(), product)
                address = '{}/{}_{}.h5'.format(sink, field, product)
                bracket = brackets[field]
                unit = 'degrees'

                # make polygon plot
                array = reservoir[product][field]
                texts = [field, 'latitude', 'longitude']
                data = [array, latitude, longitude]
                self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

        # make difference plots for omto3 vs omglerpre
        fields = ['sza', 'saa', 'vza', 'vaa', 'raa']
        for field in fields:

            # make plot
            title = '{}, OMTO3 vs OMGLERPRE, orbit 2573, 2005-01-07'.format(field.upper())
            address = '{}/{}_difference.h5'.format(sink, field)
            bracket = brackets['percent']
            unit = '%'

            # make polygon plot
            array = 100 * ((reservoir['OMTO3'][field] / reservoir['OMGLERPRE'][field]) - 1)
            texts = ['difference_{}'.format(field), 'latitude', 'longitude']
            data = [array, latitude, longitude]
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

        return None

    def bite(self, tag='picture', date='2018m0321', resolution=20, give=False):
        """Plot a map of a day, for each bit flag.

        Arguments:
            tag: str, name of subfolder with output
            date: str, the date of interest in YYYYmMMDD format
            resolution: int, resolution of plot
            give: boolean, return data?

        Returns:
            None
        """

        # create hydra
        self._stamp('beginning', initial=True)
        folder = '{}/{}'.format(self.sink, tag)
        hydra = Hydra('{}/data'.format(folder))

        # subset paths
        paths = [path for path in hydra.paths if date in path]
        paths.sort()

        # begin data collection
        fields = ['bit_flag', 'latitude', 'longitude']
        data = {}

        # for each path
        for path in paths:

            # ingest path
            self._stamp('collecting {}...'.format(path))
            hydra.ingest(path)

            # grab all data
            datum = {field: hydra.grab(field) for field in fields}

            # get the latitude
            latitude = datum['latitude']
            shape = latitude.shape

            # find ascending data
            strip = latitude[:, 30]
            difference = strip[1:] - strip[:-1]
            indices = numpy.where(difference > 0)
            track = indices[0].min()
            trackii = indices[0].max()

            # for each field
            for field in datum.keys():

                # if shape matches
                if datum[field].shape[:2] == shape:

                    # add ascending data to collection
                    data.setdefault(field, []).append(datum[field][track:trackii])

        # create arrays
        self._stamp('stacking arrays...')
        data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

        # set fill value
        fill = -9999

        # for each variable
        for bit in range(13):

            # set omi components
            tracer = data['bit_flag']
            latitude = data['latitude']
            longitude = data['longitude']

            # extract bit
            tracer = ((tracer & 2 ** bit) == 2 ** bit).astype(int)

            # create masks for finite data
            mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (tracer > fill)

            # set plot scale
            scale = (0, 1)

            # clip at upper bounds
            epsilon = 1e-8
            maskii = (tracer < scale[0] + epsilon) & (tracer > fill + epsilon)
            tracer = numpy.where(maskii, scale[0] + epsilon, tracer)
            tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)

            # replace masked values with fills
            tracer = numpy.where(mask, tracer, fill)

            # setup pace plot, full scale
            destination = '{}/plots/OMI_OMOCNUV_{}_bit_flag_{}.png'.format(folder, date, self._pad(bit))
            limits = (-90, 90, -180, 180)
            size = (14, 5)

            # set title and units
            title = 'OMI OMOCNUV {}, bit flag {}'.format(date, bit)
            unit = ''
            gradient = 'plasma'
            selection = list(range(256))

            # plot
            self._stamp('plotting {}...'.format(destination))
            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': size, 'log': False, 'back': 'white', 'font': 10, 'fontii': 10}
            options.update({'bar': True, 'extension': 'both', 'coast': 2})
            self._paint(*parameters, **options)

            # print status
            self._stamp('plotted.')

        # default package to none
        package = None

        # if giving data
        if give:

            # set package to data
            package = data

        return package

    def burn(self, months=(3, 6, 9, 12), waves=(305, 310, 380), folder='dispersion'):
        """Create parameters list for generating plot specifics through boquette.

        Arguments:
            months: list of int, months for study
            waves: list of int, wavelengths for study

        Returns:
            None
        """

        # get file list
        paths = self._see('{}/plots/dispersion'.format(self.sink))
        paths.sort()
        pairs = list(enumerate(paths))

        # make stubs
        stubs = {'depth': 'penetration_depth', 'planar': 'downwelling_irradiance', 'scalar': 'scalar_uv'}
        stubs.update({'five': 'downwelling_uv_5m', 'reflectivity': 'reflectivity'})
        stubs.update({'constant': 'attenuation_constant_kd'})

        # begin seeds
        seeds = []

        # for each month
        for month in months:

            # pad month
            month = self._pad(month)

            # and each wavelength:
            for wave in waves:

                # and each stub
                for stub in stubs.keys():

                    print(stub)

                    # get file index
                    subset = [pair for pair in pairs if '_{}.'.format(month) in pair[1]]
                    subset = [pair for pair in subset if str(wave) in pair[1]]
                    subset = [pair for pair in subset if stubs[stub] in pair[1]]
                    index = subset[0][0]

                    # generate a set of varieties
                    tag = '{}_{}_{}'.format(stub, wave, month)
                    seed = ([''], 'heatmap_{}_{}'.format(stubs[stub], wave), tag, [index])
                    seeds.append(seed)

        return seeds

    def chart(self):
        """Plot maps with cartography.

        Arguments:
            None

        Returns:
            None
        """

        # set histogram binning schemees
        schemes = []
        schemes.append([index * 0.01 for index in range(11)])
        schemes.append([-0.03 + index * 0.005 for index in range(11)])

        # set functions
        functions = [lambda eighty, forty: eighty]
        functions += [lambda eighty, forty: eighty - forty]

        # set titles and stubs
        titles = ['March 20/21, 2005, OMTO3 R354, R354 <= 0.12, SZA <= 60']
        titles += ['March 20/21, 2005, OMTO3 R354 - R331, R354 <= 0.12, SZA <= 60']
        stubs = ('r354', 'r354_r331')

        # get omto3 paths
        hydra = Hydra('{}/march'.format(self.sink))
        paths = [path for path in hydra.paths if path.endswith('.nc')]

        # set swatch position ( row )
        swath = 9

        # for each map
        for function, title, stub, scheme in zip(functions, titles, stubs, schemes):

            # set up figure
            pyplot.clf()
            # figure, axis = pyplot.subplots(figsize=(10,5))
            # figure.subplots_adjust(bottom=0.5)
            pyplot.figure(figsize=(10, 5))
            pyplot.margins(0.5, 1.0)

            # set cartopy axis with coastlines
            axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
            axis.coastlines()

            # set axis limits and title
            axis.set_xlim(-180, 180)
            axis.set_ylim(-90, 90)
            axis.set_title(title)

            # set reserviors
            polygons = []
            quantities = []
            distribution = []

            # for each path
            for path in paths:

                # ingest path
                hydra.ingest(path)

                # grab data ( R380 is really 354 ad R340 is really 331 )
                latitude = hydra.grab('latitude')
                longitude = hydra.grab('longitude')
                forty = hydra.grab('Reflectivity340')
                eighty = hydra.grab('Reflectivity380')
                zenith = hydra.grab('solar_zenith_angle')

                # construct polygon corners
                corners = self._polymerize(latitude, longitude)

                # for each image
                images = latitude.shape[0]
                for image in range(images):

                    # status check
                    if image % 100 == 0:

                        # print
                        self._print('image {}...'.format(image))

                    # check latitude for lattidue bounds
                    if numpy.all(abs(zenith[image, :]) <= 60):

                        # for each row
                        rows = latitude.shape[1]
                        for row in range(rows):

                            # get quantity
                            quantity = function(eighty[image, row], forty[image, row])

                            # if lattiude bounds
                            #if row == swath and abs(latitude[image, row]) <= 20:
                            if abs(latitude[image, row]) <= 20:

                                # add to histogram
                                distribution.append(quantity)

                            # only plot if quantity is valid ( 1 or less )
                            if abs(quantity) <= 1 and functions[0](eighty[image, row], forty[image, row]) <= 0.12:

                                # constuct polygon
                                def eastward(compass): return corners[compass][image, row, 1]
                                def northward(compass): return corners[compass][image, row, 0]
                                order = ['northwest', 'southwest', 'southeast', 'northeast']
                                polygon = [[eastward(compass), northward(compass)] for compass in order]
                                polygon = numpy.array(polygon)
                                polygon = matplotlib.patches.Polygon(xy=polygon)
                                polygons.append(polygon)

                                # add to quantities
                                quantities.append(function(eighty[image, row], forty[image, row]))

            # if first plot
            if stub == stubs[0]:

                # set up sequectial ( reflectivity )
                limits = (0, 0.12)
                colors = matplotlib.cm.gist_rainbow_r
                # greys = matplotlib.cm.get_cmap('Greys_r')
                # colors = matplotlib.colors.ListedColormap(greys(numpy.linspace(0.0, 0.8, 10)))
                bounds = numpy.linspace(*limits, 13)
                normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

            # otherwise
            else:

                # set up discrete color map ( refelctivity difference )
                limits = (-0.03, 0.02)
                colors = matplotlib.cm.gist_rainbow_r
                #colors = matplotlib.cm.prism_r
                # colors = ["crimson", "purple", "royalblue", "deepskyblue", "lightskyblue"]
                # colors += ["thistle", "thistle", "khaki", "darkorange", "deeppink"]
                # colors = matplotlib.colors.ListedColormap(colors)
                bounds = numpy.linspace(*limits, 11)
                normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

            # plot polygons with colormap
            quantities = numpy.array(quantities)
            collection = matplotlib.collections.PatchCollection(polygons, cmap=colors, alpha=1.0)
            collection.set_clim(limits)
            collection.set_array(quantities)
            axis.add_collection(collection)

            # add colorbar
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.01, position.width, 0.02])
            pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal')

            # Save the plot by calling plt.savefig() BEFORE plt.show()
            destination = '{}/plots/charts/cartopy_20050321_{}.jpg'.format(self.sink, stub)
            self._print('saving {}...'.format(destination))
            pyplot.savefig(destination)
            pyplot.clf()

            # create histogram from distribution
            title = title.replace('SZA <= 60', 'latitude <= 20, All Rows')
            pyplot.hist(distribution, scheme)
            pyplot.title(title)

            # save and clear
            destination = '{}/plots/charts/histogram_20050321_{}.jpg'.format(self.sink, stub)
            self._print('saving {}...'.format(destination))
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def chloridize(self, year=2005, months=(3, 4), days=(31, 1), orbits=(3781, 3795), resolution=10):
        """Map the new and old chlorophyll differences across a month boundary.

        Arguments:
            year: int, the year
            months: tuple of ints, the months
            days: tuple of ints, the days
            orbits: tuple of ints, the orbit numbers

        Returns:
            None
        """

        # create folder
        folder = 'plots/chlorination'
        self._make('{}/{}'.format(self.sink, folder))

        # begin collection
        prior = {'latitude': [], 'longitude': [], 'chlorophyll': []}
        new = {'latitude': [], 'longitude': [], 'chlorophyll': [], 'flag': []}

        # for each month, day, orbit combination
        for month, day, orbit in zip(months, days, orbits):

            # get hydra for OMOCNUV
            formats = (year, self._pad(month), self._pad(day))
            # hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/{}/{}/{}'.format(*formats))
            hydra = Hydra('/tis/acps/OMI/70004/OMGLER/{}/{}/{}'.format(*formats))
            hydra.ingest(str(orbit))

            # get hydra for new OMGLERPRE
            hydraii = Hydra('{}/chlorophyll'.format(self.sink))
            hydraii.ingest(str(orbit))

            # get OMGLER data
            prior['latitude'].append(hydra.grab('Latitude'))
            prior['longitude'].append(hydra.grab('Longitude'))
            prior['chlorophyll'].append(hydra.grab('ChlorophyllConcentration'))

            # get new OMGLERPRE data
            new['latitude'].append(hydraii.grab('Latitude'))
            new['longitude'].append(hydraii.grab('Longitude'))
            new['chlorophyll'].append(hydraii.grab('chlorophyll_concentration'))
            new['flag'].append(hydraii.grab('chlorophyll_flag'))

        # create a KDTree from the first lat / lon
        self._print('making kdtree...')
        latitude = prior['latitude'][0].flatten()
        longitude = prior['longitude'][0].flatten()
        tree = scipy.spatial.cKDTree(numpy.column_stack((latitude, longitude)))

        # query the tree for the second points
        shape = prior['latitude'][1].shape
        latitudeii = prior['latitude'][1].flatten()
        longitudeii = prior['longitude'][1].flatten()
        distances, indices = tree.query(numpy.column_stack((latitudeii, longitudeii)), k=1)

        # create prior chlorophyll arrays
        chlorophyll = prior['chlorophyll'][0].flatten()[indices.flatten()]
        chlorophyll[abs(distances.flatten()) > 1] = 1e30
        chlorophyll = chlorophyll.reshape(shape)
        chlorophyllii = prior['chlorophyll'][1]
        difference = chlorophyllii - chlorophyll

        # create new chlorophyll arrays
        chlorophyll = new['chlorophyll'][0].flatten()[indices.flatten()]
        chlorophyll[abs(distances.flatten()) > 1] = 1e30
        chlorophyll = chlorophyll.reshape(shape)
        chlorophyllii = new['chlorophyll'][1]
        differenceii = chlorophyllii - chlorophyll

        # create difference of differences
        differenceiii = abs(differenceii) - abs(difference)
        differenceiii = numpy.where(chlorophyll == 1e30, 1e30, differenceiii)

        # reshape latitutde and longitude
        latitudeii = latitudeii.reshape(shape)
        longitudeii = longitudeii.reshape(shape)

        # set tracers
        tracers = [difference, differenceii, differenceiii]
        tracers += [prior['chlorophyll'][0], prior['chlorophyll'][1]]
        tracers += [new['chlorophyll'][0], new['chlorophyll'][1]]
        tracers += [new['flag'][0], new['flag'][1]]

        # set latitudes
        latitudes = [latitudeii] * 3
        latitudes += [prior['latitude'][0], prior['latitude'][1]]
        latitudes += [new['latitude'][0], new['latitude'][1]]
        latitudes += [new['latitude'][0], new['latitude'][1]]

        # set longitudes
        longitudes = [longitudeii] * 3
        longitudes += [prior['longitude'][0], prior['longitude'][1]]
        longitudes += [new['longitude'][0], new['longitude'][1]]
        longitudes += [new['longitude'][0], new['longitude'][1]]

        # set titles
        date = '{}m{}{} ( o{} )'.format(year, self._pad(months[0]), self._pad(days[0]), orbits[0])
        dateii = '{}m{}{} ( o{} )'.format(year, self._pad(months[1]), self._pad(days[1]), orbits[1])
        titles = ['OMGLER monthly averaged chlorophyll difference\n {} - {}'.format(dateii, date)]
        titles += ['OMGLERPRE daily averaged chlorophyll difference\n {} - {}'.format(dateii, date)]
        titles += ['OMGLERPRE abs difference - OMGLER abs difference\n {} - {}'.format(dateii, date)]
        titles += ['OMGLER chlorophyll {}'.format(entry) for entry in (date, dateii)]
        titles += ['OMGLERPRE chlorophyll {}'.format(entry) for entry in (date, dateii)]
        titles += ['OMGLERPRE chlorophyll flag {}'.format(entry) for entry in (date, dateii)]

        # set stubs
        stubs = ['prior_diff', 'new_dff', 'meta_diff']
        stubs += ['prior_chloro_{}'.format(orbit) for orbit in orbits]
        stubs += ['new_chloro_{}'.format(orbit) for orbit in orbits]
        stubs += ['flag_{}'.format(orbit) for orbit in orbits]

        # set gradients
        gradients = ['coolwarm'] * 3
        gradients += ['viridis'] * 4
        gradients += ['rainbow'] * 2

        # set scales
        scales = [(-0.05, 0.05)] * 3
        scales += [(0, 0.5)] * 4
        scales += [(0, 5)] * 2

        # set units
        units = ['mg / m^3'] * 7
        units += ['_'] * 2

        # make zipper
        zipper = zip(tracers, latitudes, longitudes, titles, stubs, gradients, scales, units)

        # for each tracer
        for tracer, latitude, longitude, title, stub, gradient, scale, unit in list(zipper):

            # create masks for finite data
            mask = (numpy.isfinite(tracer)) & (abs(tracer) < 99)

            # set figure size and limits
            limits = (-60, 60, -150, -90)
            size = (5, 10)
            logarithm = False
            bar = True
            back = 'white'
            selection = list(range(256))
            lines = None

            # print tracer min, max
            self._print(tracer[mask].min(), tracer[mask].max())

            # clip at upper and lower bounds
            fill = -999
            epsilon = 1e-6
            tracer = numpy.where(tracer < scale[0] + epsilon, scale[0] + epsilon, tracer)
            tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)
            tracer = numpy.where(mask, tracer, fill)

            # plot
            destination = '{}/{}/chlorophyll_interpolation_{}.png'.format(self.sink, folder, stub)
            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, lines]
            options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters, **options)

            # print status
            self._stamp('plotted.')

        return tracers, latitudes, longitudes

    def chlorinate(self, tag='', wave=305, orbits=None, zones=None, output=False, bar=False):
        """Scan for chlorophyll values.

        Arguments:
            *words: unpacked tuple of field ames
            tag: str, name of month to plot
            wave: int, wavelength
            orbit: list of int, orbit indexes, all orbits if None
            resolution: int, number of vertical pixels to coagulate
            zone: tuple of floats, the latitude limits
            output: boolean, return data collection?
            bar: boolean, add colorbar?

        Returns:
            None
        """
        # set fill
        fill = -999

        # set default zones to none
        zones = zones or []

        # make folder based on tag
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/{}'.format(self.sink, tag))

        # create omi hydra
        hydra = Hydra('{}/{}'.format(self.sink, tag))

        # grab spectral wavelength
        hydra.ingest(0)
        wavelength = hydra.grab('spectral_wavelength')
        position = hydra._pin(wave, wavelength)[0][0]

        # get the DNA Spectrum
        spectra = Hydra('../studies/diatom/simulation/spectra')
        spectra.ingest(1)
        spectrum = spectra.grab('spectrum')
        spectrum = spectrum.reshape(1, 1, -1)

        # set dates
        dates = {'march': '2005-03-21', 'june': '2005-06-21'}
        dates.update({'september': '2005-09-21', 'december': '2005-12-21'})
        dates.update({'colorbars': '2005-03-21'})

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure', 'quality': 'bit_flag'})
        fields.update({'zenith': 'solar_zenith_angle'})

        # create stubs
        stubs = {'sky': 'clear sky irradiance, {}nm'.format(wave), 'cloud': 'cloud optical depth, 360nm'}
        stubs.update({'residue': 'residue 331', 'ozone': 'total column ozone'})
        stubs.update({'coefficient': 'planar diffuse coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'chlorophyll'})
        stubs.update({'surface': 'planar surface irradiance, {}nm'.format(wave), 'reflectance': 'n value 360'})
        stubs.update({'reflectivity': 'reflectivity, 360nm', 'anomaly': 'row_anomaly'})
        stubs.update({'ultraviolet': 'ultraviolet index'})
        stubs.update({'underwater': 'underwater irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': '10 % penetration depth, zp, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA damage dose rate at 5m depth'})
        stubs.update({'pressure': 'Surface pressure'})

        # create uits
        units = {'sky': 'mW / m2 s', 'cloud': '-', 'residue': '-', 'ozone': 'DU'}
        units.update({'coefficient': '1 / m', 'chlorophyll': 'mg / m3'})
        units.update({'surface': 'mW / m2 s', 'reflectance': '-', 'reflectivity': '-'})
        units.update({'ultraviolet': 'UVI', 'underwater': 'mW / m2 s'})
        units.update({'depth': 'm', 'damage': 'mW / m2 s', 'pressure': 'atm'})

        # create positions for third index of 3-d data
        positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2, 'reflectivity': 1}
        positions.update({'underwater': position, 'depth': position})

        # declare color scale ranges
        scales = {'sky': (0, 120), 'cloud': (0, 40)}
        scales.update({'residue': (-10, 10), 'ozone': (230, 330)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0, 1)})
        scales.update({'surface': (0, 120), 'reflectance': (50, 150)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 15)})
        scales.update({'underwater': (0, 120)})
        scales.update({'depth': (0, 35)})
        scales.update({'damage': (0, 200)})
        scales.update({'pressure': (0.96, 1.04)})

        # set fields for logarithmic scale
        logarithms = {'coefficient': True, 'chlorophyll': True}
        logarithms = {}

        # set up transforms to convert from logs and replace fill values
        def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        transforms.update({'coefficient': powering})

        # set paths
        orbits = orbits or [index for index, path in enumerate(hydra.paths)]

        # begin data reservoir
        data = {field: [] for field in fields.keys()}

        # for each orbit
        for orbit in orbits:

            # print
            self._print('collecting orbit {}'.format(orbit))

            # ingest
            hydra.ingest(orbit)

            # add data
            [data[field].append(hydra.grab(search)) for field, search in fields.items()]

        # stack all arrays
        data = {field: numpy.vstack(data[field]) for field in data.keys()}

        # get chlorphyll data
        latitude = data['latitude']
        longitude = data['longitude']
        chlorophyll = data['chlorophyll']
        zenith = data['zenith']

        # for each zone
        for north, east in zones:

            # print space
            self._print('')
            self._print('{}, {}:'.format(north, east))

            # get pixels
            collection = []
            collectionii = []
            pixels = self._pin([north, east], [latitude, longitude])
            for pixel in pixels:

                # print
                formats = (chlorophyll[pixel], latitude[pixel], longitude[pixel], zenith[pixel])
                self._print('     {}, {}, {}, {}'.format(*formats))

                # add to collection
                collection.append(chlorophyll[pixel])
                collectionii.append(zenith[pixel])

            # print average
            self._print('')
            self._print('Avg chloro: {}'.format(numpy.array(collection).mean()))
            self._print('Avg zenith: {}'.format(numpy.array(collectionii).mean()))

        # default package to None
        package = None

        # if output requested
        if output:

            # set package to data
            package = data

        return package

    def churn(self, mode='Errors', *features):
        """Churn out information for making difference plots.

        Arguments:
            mode: str: beginning of each file
            *features: unpacked list of features

        Returns:
            dict of tuples of variant attributes
        """

        # set default features
        fields = ['erythema', 'erythemaii', 'dose', 'doseii', 'constant', 'constantii']
        fields += ['ozone', 'chlorophyll', 'ultraviolet']
        features = features or fields

        # set source
        source = '{}/plots/comparisons'.format(self.sink)
        hydra = Hydra(source)

        # set up sink
        sink = '{}/slides/errors'.format(self.sink)

        # collect variants
        variants = []
        for feature in features:

            # gather paths
            paths = list(enumerate(hydra.paths))
            paths = [pair for pair in paths if mode in pair[1]]
            index, path = [pair for pair in paths if feature in pair[1]][0]

            # ingest path
            hydra.ingest(path)

            # make seed depending on mode
            stub = '{}_{}'.format(feature, mode.lower())

            # create default name
            name = 'heatmap_{}'.format(feature)

            # create name
            if mode == 'Errors':

                # create name
                name = 'heatmap_{}_error'.format(feature)

            # create name
            if mode == 'Histogram':

                # create name
                name = 'histogram_{}_error'.format(feature)

            # create name
            if mode == 'UVBUVP':

                # create name
                name = 'histogram_{}'.format(feature)

            # create name
            if mode == 'OMOCNUV':

                # create name
                name = 'histogram_{}'.format(feature)

            # create variant
            variant = {'title': hydra[0].attributes['title']}
            variant.update({'propagation/stub': stub, 'propagation/folder': sink})
            variant.update({'propagation/parameters': [{'file': index, 'name': name, 'step': 1}]})

            # add to variants
            variants.append(variant)

        return variants

    def clip(self, target='flux', sink='clip'):
        """compare clipped pressure differences.

        Arguments:
            target: str, target variable

        Returns:
            None
        """

        # set degrees
        degrees = (-65, 40)

        # set tags
        tags = ['noon', 'clip']

        # link feature names to searchs
        searches = {'flux': 'surface/planar', 'zenith': '/solar_zenith_angle', 'cloud': 'cloud'}
        searches.update({'reflectivity': 'ler_value', 'value': 'n_value', 'residue': 'residue'})
        searches.update({'azimuth': 'relative_azimuth', 'latitude': 'latitude', 'longitude': 'longitude'})
        searches.update({'azimuth': 'relative_azimuth', 'sky': 'clear_sky', 'pressure': 'pressure'})
        #searches.update({'azimuth': 'relative_azimuth', 'pressure': 'pressure'})

        # set third indices
        thirds = {'flux': 20, 'reflectivity': 1, 'value': 2, 'sky': 20}
        #thirds = {'flux': 20, 'reflectivity': 0, 'value': 1}

        # set search list positions
        positions = {'pressure': -1}

        # set reservoir
        reservoir = {tag: {} for tag in tags}

        # collect first tage and find latitude indices
        ocean = self._pull(tags[0])
        latitude = ocean.grab('latitude')

        # find the low and high latitude indices
        low = max([index for index, degree in enumerate(latitude[:, 30]) if degree <= degrees[0]])
        high = min([index for index, degree in enumerate(latitude[:, 30]) if degree >= degrees[1]])

        # for each tag
        for tag in tags:

            # grab the data
            ocean = self._pull(tag)

            # for each search feature
            for name, search in searches.items():

                # add the data to the reservoir, subsetting by degree bounds
                position = positions.get(name, 0)
                array = ocean.grab(search, position)[low: high]
                reservoir[tag][name] = array

            # for each third dimension
            for name, third in thirds.items():

                # apply subset
                array = reservoir[tag][name]
                reservoir[tag][name] = array[:, :, third]

        # create percents
        data = {}
        for name in searches.keys():

            # get data
            array = reservoir[tags[0]][name]
            arrayii = reservoir[tags[1]][name]

            # add arrays
            data['{}_{}'.format(name, tags[0])] = array
            data['{}_{}'.format(name, tags[1])] = arrayii

            # calculate percent
            percent = 100 * ((arrayii / array) - 1)
            data[name] = percent

        # create latitudinal and longitudinal crosssections
        maximum = data[target].max()
        minimum = data[target].min()
        pixel = self._pin(maximum, data[target])[0]
        pixelii = self._pin(minimum, data[target])[0]
        modes = {'high': pixel, 'low': pixelii}

        # set crosssection names
        names = ['flux', 'residue', 'reflectivity', 'cloud', 'pressure', 'sky', 'value']

        # grab latitude and longitude
        latitude = ocean.grab('latitude')[low: high]
        longitude = ocean.grab('longitude')[low: high]

        # for each mode
        for mode in modes.keys():

            # and each mame
            for name in names:

                # plot the latitudinal crosssection
                pixel = modes[mode]
                abscissa = latitude[pixel[0] - 30: pixel[0] + 29, pixel[1]]
                ordinate = data['{}_{}'.format(name, tags[0])][pixel[0] - 30: pixel[0] + 29, pixel[1]]
                ordinateii = data['{}_{}'.format(name, tags[1])][pixel[0] - 30: pixel[0] + 29, pixel[1]]
                title = 'Latitudinal crosssection {}, {}'.format(name, mode)
                address = 'plots/section/Cross_{}_latitudinal_{}.h5'.format(mode, name)
                self.squid.splatter(name, [ordinate, ordinateii], 'latitude', abscissa, address, title)

                # plot the longitudinal crosssection
                pixel = modes[mode]
                abscissa = longitude[pixel[0], :60]
                ordinate = data['{}_{}'.format(name, tags[0])][pixel[0], :60]
                ordinateii = data['{}_{}'.format(name, tags[1])][pixel[0], :60]
                title = 'Longitudinal crosssection {}, {}'.format(name, mode)
                address = 'plots/section/Cross_{}_longitudinal_{}.h5'.format(mode, name)
                self.squid.splatter(name, [ordinate, ordinateii], 'longitude', abscissa, address, title)

        # for each of the first errors in the first 6
        report = ['Random Forests']

        # print status
        self._stamp('planting random forest...', initial=True)
        report.append(self._print('\nrunning random forest on percent differences in {}...'.format(target)))

        # create matrix
        features = [name for name in data.keys() if name != target] + [target]
        train = numpy.array([data[feature].flatten() for feature in features]).transpose(1, 0)

        # remove rows with NaNs
        train = numpy.array([row for row in train if numpy.isfinite(row).sum() == row.shape[0]])

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # run random forest
        forest = RandomForestRegressor(n_estimators=100, max_depth=5)
        forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(features, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, target)
        self._jot(report, destination)

        # make forest object
        ensemble = {'forest': forest, 'matrix': matrix, 'truth': truth}
        ensemble.update({'features': features, 'target': target, 'prediction': prediction})

        # make sink folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # also plot all relations
        for index, feature in enumerate(features[:-1]):

            # create plot
            abscissa = matrix[:, index]
            ordinate = truth
            title = 'Percent difference in {} vs {}'.format(target, feature)
            address = 'plots/{}/{}_{}_forest.h5'.format(sink, target, feature)
            self.squid.ink(target, ordinate, feature, abscissa, address, title)

            # create plot
            abscissa = matrix[:, index]
            ordinate = prediction
            title = 'Percent difference in predicted {} vs {}'.format(target, feature)
            address = 'plots/{}/{}_{}_forest_prediction.h5'.format(sink, target, feature)
            self.squid.ink(target, ordinate, feature, abscissa, address, title)

        # get highest and lowest percents
        high = numpy.argsort(truth)[-1]
        low = numpy.argsort(truth)[0]
        samples = {'high': high, 'low': low}

        # # create list of factors
        # self._stamp('testing sensitivity...')
        # factors = [round(-0.5 + index * 0.01, 2) for index in range(101)]
        #
        # # for each feature
        # for column, feature in enumerate(features[:-1]):
        #
        #     # for each point
        #     for label, sample in samples.items():
        #
        #         # get nominal values
        #         row = matrix[sample].copy()
        #         nominal = row[column]
        #
        #         # create abscissa
        #         abscissa = [(factor + 1) * nominal for factor in factors]
        #
        #         # for each point
        #         ordinate = []
        #         for point in abscissa:
        #
        #             # insert into row and make prediction
        #             row[column] = point
        #             prediction = forest.predict(row.reshape(1, -1))[0]
        #             ordinate.append(prediction)
        #
        #         # create plot
        #         title = 'Percent difference in {} vs {}, forest sensitivity, {}'.format(target, feature, label)
        #         address = 'plots/sensitivity/{}_{}_{}_{}_sensitivity.h5'.format(sink, target, label, feature)
        #         self.squid.ink(target, ordinate, feature, abscissa, address, title)

        # print status
        self._stamp('planted.')

        return ensemble

    def clop(self, year=2005, months=(3, 4), days=(31, 1), orbits=(3781, 3795), coordinates=(-20, -120)):
        """Check chlorophyll results across month boundary.

        Arguments:
            year: int, the year
            months: tuple of ints, the months
            days: tuple of ints, the days
            orbits: tuple of ints, the orbit numbers
            coordinates: tuple of floats, the lat and lon coordinates

        Returns:
            None
        """

        # create folder
        folder = 'plots/chlorophyll'
        self._make('{}/{}'.format(self.sink, folder))

        # begin collection
        data = {'time': [], 'prior': [], 'new': [], 'pixel': []}

        # for each month, day, orbit combination
        for month, day, orbit in zip(months, days, orbits):

            # get hydra for OMOCNUV
            formats = (year, self._pad(month), self._pad(day))
            # hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/{}/{}/{}'.format(*formats))
            hydra = Hydra('/tis/acps/OMI/70004/OMGLER/{}/{}/{}'.format(*formats))
            hydra.ingest(str(orbit))

            # get hydra for new OMGLERPRE
            hydraii = Hydra('{}/chlorophyll'.format(self.sink))
            hydraii.ingest(str(orbit))

            # # get OMOCNUV data
            # latitude = hydra.grab('latitude')
            # longitude = hydra.grab('longitude')
            # chlorophyll = hydra.grab('chlorophyll')
            # time = hydra.grab('time')

            # get OMGLER data
            latitude = hydra.grab('Latitude')
            longitude = hydra.grab('Longitude')
            chlorophyll = hydra.grab('ChlorophyllConcentration')
            time = hydra.grab('Time')
            delta = hydra.grab('Delta_time')

            # adjust time, as it is since 1993 in omgler but sine 2010 for OMOCNUV
            seconds = (datetime.datetime(2010, 1, 1) - datetime.datetime(1993, 1, 1)).total_seconds()
            time = numpy.array([((time + seconds) * 1000)] * delta.shape[0])

            # get OMGLERPRE
            chlorophyllii = hydraii.grab('chlorophyll_concentration')

            # find the pixel closest to the coordinates
            pixel = hydra._pin(coordinates, [latitude, longitude])[0]

            # append data
            data['time'].append(time[pixel[0]])
            data['prior'].append(chlorophyll[pixel])
            data['new'].append(chlorophyllii[pixel])
            data['pixel'].append(pixel)

            # print pixel
            self._print(month, day, orbit)
            self._print(pixel)
            self._print(latitude[pixel])
            self._print(longitude[pixel])

        # create arrays
        data = {name: numpy.array(array) for name, array in data.items()}

        # create plots
        for mode in ('prior', 'new'):

            # make plot
            pixels = data['pixel']
            orientation = (self._orient(coordinates[0]), self._orient(coordinates[1], east=True))
            formats = (*orientation, orbits[0], pixels[0], orbits[1], pixels[1])
            title = 'New OMGLERPRE chlorophyll vs prior, {}, {}, orbits {} ( pixel: {} ), {} ( pixel: {} )'
            title = title.format(*formats)
            address = '{}/{}_chlorophyll_{}_{}.h5'.format(folder, mode, *formats)
            self.squid.ink('chlorophyll', data[mode], 'month', data['time'], address, title)

        return None

    def colonize(self, tag=None, data=None, bounds=(-60, 40)):
        """Colonize a random forest tree to examine errors.

        Arguments:
            tag: str, tag for grid
            data: errors data
            bounds: latitude bounds

        Returns:
            None
        """

        # set fill value
        fill = -9999

        # create ascii, omocned data pairs
        comparisons = {'n331': 'n_values_0', 'n340': 'n_values_1'}
        comparisons.update({'n360': 'n_values_2', 'n380': 'n_values_3'})
        comparisons.update({'erythema': 'erythema_planar_Ed'})
        comparisons.update({'erythemaii': 'erythema_scalar_Eo'})
        comparisons.update({'ozone': 'ozone'})
        comparisons.update({'pteran': 'terrain_pressure'})
        comparisons.update({'chlorophyll': 'chlorophyll'})

        # define training matrix and target
        features = ['n331', 'n340', 'n360', 'n380', 'ozone', 'pteran', 'chlorophyll']
        targets = ['erythemaii']

        # grab the grid file
        grid = Hydra('{}/data/grid'.format(self.sink))
        paths = [path for path in grid.paths if (tag or '') in path]
        grid.ingest(paths[0])

        # get latitude
        latitude = grid.grab('latitude')

        # for eadh comparison
        differences = {}
        shape = grid[0].shape
        #mask = numpy.ones(shape).astype(bool)
        mask = (latitude >= bounds[0]) & (latitude <= bounds[1])
        for first, second in comparisons.items():

            # get the datasets
            array = grid.grab('ascii_{}'.format(first))
            arrayii = grid.grab('omocned_{}'.format(second))

            # add masks for first array
            maskii = (numpy.isfinite(array)) & (array != fill) & (abs(array) < 1e10)
            mask = numpy.logical_and(mask, maskii)

            # and second array
            maskii = (numpy.isfinite(arrayii)) & (arrayii != fill) & (abs(arrayii) < 1e10)
            mask = numpy.logical_and(mask, maskii)

            # take the difference
            difference = arrayii - array
            differences[first] = difference

        # construct stack
        stack = numpy.hstack([differences[name].reshape(-1, 1) for name in features + targets])
        stack = stack[mask.flatten()]

        # # normalize
        # stack = (stack - stack.mean(axis=0)) / stack.std(axis=0)

        # and apply mask
        print('stack:', stack.shape, stack.min(), stack.max())

        # for each of the first errors in the first 6
        report = ['Random Forests']

        # print status
        report.append(self._print('\nrunning random forest on difference in {}...'.format(targets[0])))

        # construct the block, with predictors as last three
        matrix = stack[:, :-1]
        target = stack[:, -1]

        # run random forest
        forest = RandomForestRegressor(n_estimators=100, max_depth=5)
        forest.fit(matrix, target)

        # calculate score
        score = forest.score(matrix, target)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(features, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, targets[0])
        self._jot(report, destination)

        return None

    def compare(self, orbits):
        """Compare data files for largest differeneces.

        Arguments:
            orbits: list of orbit / file tags

        Returns:
            None
        """

        # set maps
        maps = ['constant', 'constantii', 'erythemal', 'erythemalii', 'dose', 'doseii', 'chlorophyll', 'ultraviolet']

        # begin report
        report = ['Comparison of E_K output files',  '\n']

        # get all datasets
        reservoir = [self.synthesize(orbit)['data'] for orbit in orbits]
        indices = list(range(len(orbits)))

        # get latitude and longitude
        latitudes = reservoir[0]['latitude']
        longitudes = reservoir[0]['longitude']

        # get all data keys
        fields = list(reservoir[0].keys())
        for field in fields:

            # print field, add spacer
            report.append('')
            self._print('{}...'.format(field))

            # for each dataset
            for index, orbit, data in zip(indices, orbits, reservoir):

                # and each second dataset
                for indexii, orbitii,  dataii in zip(indices, orbits, reservoir):

                    # only if index is different
                    if indexii > index:

                        # get data
                        datum = data[field]
                        datumii = dataii[field]

                        print(datum.shape)
                        print(datumii.shape)

                        # find minimum length
                        length = min([len(datum), len(datumii)])

                        # make mask for nans
                        mask = numpy.isfinite(datum[:length])
                        maskii = numpy.isfinite(datumii[:length])
                        masque = numpy.logical_and(mask, maskii)

                        # compute percent differences
                        percent = 100 * ((datumii[:length][masque] / datum[:length][masque]) - 1)

                        # add to report
                        formats = (orbit, orbitii, field, round(percent.min(), 5), round(percent.max(), 5))
                        report.append('{} vs {}, {}: {} % to {} %'.format(*formats))

                        # find worst instance
                        worst = abs(percent.flatten()).argsort()[-1]
                        report.append('worst: {} vs {}'.format(datum.flatten()[worst], datumii.flatten()[worst]))

                        # create histogram
                        folder = 'plots/histograms/Histogram_error_{}_{}_{}.h5'.format(field, orbit, orbitii)
                        title = 'Histogram of Errors for {}, orbit 2573, ascii intermediate vs array'.format(field)
                        self.squid.ripple('error_{}'.format(field), percent, folder, title)

                        # check for map
                        if field in maps:

                            # create heatmap
                            latitude = latitudes[:length]
                            longitude = longitudes[:length]

                            # make absolute percents
                            percent = 100 * ((datumii[:length] / datum[:length]) - 1)
                            absolute = abs(percent)

                            print(field)
                            print(absolute.shape)
                            print(percent.shape)

                            # create errors
                            error = []
                            for row, rowii in zip(absolute, percent):

                                # get index of max
                                largest = row.argsort()[-1]
                                error.append(rowii[largest])

                            # flatten all members
                            error = numpy.array(error).flatten()
                            latitude = latitude.flatten()
                            longitude = longitude.flatten()

                            # make maks
                            mask = numpy.isfinite(error)
                            error = error[mask]
                            latitude = latitude[mask]
                            longitude = longitude[mask]

                            # make heatmap of errors
                            bounds = [-0.5, -0.05, -0.005, 0.005, 0.05, 0.5]

                            # reset bounds for does
                            if 'chlorophyll' in field:

                                # make heatmap of errors
                                bounds = [-0.5, -0.05, -0.005, 0.005, 0.05, 0.5]

                            # reset bounds for does
                            if 'dose' in field:

                                # make heatmap of errors
                                bounds = [-1.5, -0.5, -0.05, 0.05, 0.5, 1.5]

                            # reset bounds for does
                            if 'constant' in field:

                                # make heatmap of errors
                                bounds = [-0.5, -0.05, -0.005, 0.005, 0.05, 0.5]

                            # reset bounds for does
                            if 'erythemal' in field:

                                # make heatmap of errors
                                bounds = [-0.005, -0.0005, -0.00005, 0.00005, 0.0005, 0.005]

                            # reset bounds for does
                            if 'ultraviolet' in field:

                                # make heatmap of errors
                                bounds = [-0.005, -0.0005, -0.00005, 0.00005, 0.0005, 0.005]

                            # make map
                            title = 'Map of Errors for {}, orbit 2573, ascii intermediate vs array'.format(field)
                            folder = 'plots/differences/Heatmap_error_{}_{}_{}.h5'.format(field, orbit, orbitii)
                            arguments = [field, error, 'percent error', latitude, 'longitude', longitude, folder, bounds]
                            arguments += [title]
                            self.squid.shimmer(*arguments)

        # jot report
        self._jot(report, '{}/reports/Comparison_report.txt'.format(self.sink))

        return None

    def contour(self, source='glint', sink='contour', resolution=20):
        """Plot a contour of sun glint angles.

        Arguments:
            source: str, source folder
            sink: str, sink folder for plots
            resolution: int, number of pixels to combine for map

        Returns:
            None
        """

        # craete sink folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # get the data
        hydra = Hydra('{}/{}'.format(self.sink, source))
        path = hydra.paths[0]

        # ingest top path
        hydra.ingest(path)

        # grab angles
        zenith = hydra.grab('solar_zenith')
        zenithii = hydra.grab('viewing_zenith')
        azimuth = hydra.grab('relative_azimuth')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        bits = hydra.grab('bit_flag')

        # isolate sunglint warning
        bits = ((bits & 8) == 8).astype(int)

        # convert to radiancs
        radians = (numpy.pi / 180)
        zenith = zenith * radians
        zenithii = zenithii * radians
        azimuth = (180 - azimuth) * radians

        # calculate glint angle arccos[ cos cosii - sin sinii cosiii ]
        cosines = numpy.cos(zenith) * numpy.cos(zenithii)
        sines = numpy.sin(zenith) * numpy.sin(zenithii) * numpy.cos(azimuth)
        glint = numpy.arccos(cosines - sines)

        # convert back to degrees
        glint = glint / radians

        # get corners from latitude and longitude bounds
        polygons = self._polymerize(latitude, longitude, resolution)
        glint = self._resolve(glint, resolution)
        latitude = self._resolve(latitude, resolution)
        longitude = self._resolve(longitude, resolution)
        bits = self._resolve(bits, resolution)

        # apply edge mask
        mask = (polygons[:, :, 4:].std(axis=2) < 20)
        polygons = polygons[mask]
        glint = glint[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]
        bits = bits[mask]

        # set up bracket
        bracket = [0, 5, 10, 15, 20, 25, 30, 50, 70, 90]

        # plot orbit
        address = 'plots/contour/Sun_glint_contours.h5'
        title = 'Sun glint angle contours, {}, {}'.format(self._stage(path)['day'], self._stage(path)['orbit'])
        name = 'sun glint angle'
        units = 'degrees'
        texts = [name, 'latitude', 'longitude']
        arrays = [glint, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, address, bracket, title, units)

        # set up bracket
        bracket = [-0.5, 0.5, 1.5]

        # plot orbit
        address = 'plots/contour/Sun_glint_possibility_contours.h5'
        title = 'Sun glint possibility, {}, {}'.format(self._stage(path)['day'], self._stage(path)['orbit'])
        name = 'sun glint possibility'
        units = 'flag'
        texts = [name, 'latitude', 'longitude']
        arrays = [bits, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, address, bracket, title, units)

        return None

    def contrast(self, *words, tags=('OCI_v1', 'OCI_v2'), resolution=100, wave=305, sigmas=3):
        """Create percent difference plots from pace results.

        Argumnents:
            words: upnpacked list of str, the plots to make
            tags: tuple of str, keywords of pace result
            resolution: int, number of vertical polygons to meld
            wave: int, wavelength
            sigmas: number of stdevs for clipping plotting range

        Returns:
            None
        """

        # set defaults
        bar = True
        zoom = None

        # set epsilon
        epsilon = 1e-5

        # set fill
        fill = -999

        # make folder
        folder = 'plots/contrast'
        self._make('{}/plots'.format(self.sink))
        self._make('{}/{}'.format(self.sink, folder))

        # create pace data hydra
        granule = Hydra('{}/output'.format(self.sink))

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity_360', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure'})
        fields.update({'aerosol': 'aerosol_optical_depth_354'})
        fields.update({'aerosolii': 'aerosol_optical_depth_388'})

        # create stubs
        stubs = {'sky': 'clear sky irradiance, {}nm'.format(wave), 'cloud': 'cloud optical depth, 360nm'}
        stubs.update({'residue': 'residue 331', 'ozone': 'total column ozone'})
        stubs.update({'coefficient': 'planar diffuse coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'chlorophyll'})
        stubs.update({'surface': 'planar surface irradiance, {}nm'.format(wave), 'reflectance': 'n value 360'})
        stubs.update({'reflectivity': 'reflectivity, 360nm', 'anomaly': 'row_anomaly'})
        stubs.update({'ultraviolet': 'ultraviolet index'})
        stubs.update({'underwater': 'underwater irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': 'penetration depth, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA damage dose rate at 5m depth'})
        stubs.update({'pressure': 'Surface pressure'})
        stubs.update({'aerosol': 'AAOD, 354nm', 'aerosolii': 'AAOD, 388nm'})

        # add title inserts
        inserts = {'omi': {field: 'OMI' for field in stubs.keys()}, 'pace': {field: 'PACE' for field in stubs.keys()}}
        inserts['omi']['chlorophyll'] = 'OMI ( MODIS composite )'
        inserts['pace']['ozone'] = 'PACE ( OMTO3 )'
        inserts['pace']['pressure'] = 'PACE ( from height )'

        # create uits
        units = {'sky': '$mW / m^2 s$', 'cloud': '-', 'residue': '-', 'ozone': 'DU'}
        units.update({'coefficient': '1 / m', 'chlorophyll': '$mg / m^3$'})
        units.update({'surface': '$mW / m^2 s$', 'reflectance': '-', 'reflectivity': '-'})
        units.update({'ultraviolet': 'UVI', 'underwater': '$mW / m^2 s$'})
        units.update({'depth': 'm', 'damage': '$mW / m^2 s$', 'pressure': 'atm'})
        units.update({'aerosol': '-', 'aerosolii': '-'})

        # declare color scale ranges
        scales = {'sky': (10, 90), 'cloud': (0, 50)}
        scales.update({'residue': (-5, 5), 'ozone': (280, 420)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0, 2)})
        scales.update({'surface': (10, 90), 'reflectance': (70, 130)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 10)})
        scales.update({'underwater': (0, 50)})
        scales.update({'depth': (0, 30)})
        scales.update({'damage': (0, 120)})
        scales.update({'pressure': (0.85, 1.05)})
        scales.update({'aerosol': (0, 0.1), 'aerosolii': (0, 0.1)})

        # begin data
        data = {tag: {} for tag in tags}

        # for each tag
        for tag in tags:

            # ingest file
            granule.ingest(tag)
            self._print('pace file: {}'.format(granule.current))

            # grab spectral wavelength
            wavelength = granule.grab('spectral_wavelength')
            position = granule._pin(wave, wavelength)[0][0]

            # get the DNA Spectrum
            spectra = Hydra('{}/spectra'.format(self.sink))
            spectra.ingest(1)
            wavelength = spectra.grab('wavelength')
            spectrum = spectra.grab('spectrum')
            spectrum = spectrum.reshape(1, 1, -1)

            # create positions for third index of 3-d data
            positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2}
            positions.update({'underwater': position, 'depth': position})

            # set fields for logarithmic scale
            # logarithms = {'coefficient': True, 'chlorophyll': True}
            logarithms = {}

            # # set up transforms to convert from logs and replace fill values
            # def powering(array): return numpy.where(array != fill, 10 ** array, fill)
            # transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
            # transforms.update({'coefficient': powering})

            # collect originnal data and apply transformts
            data[tag] = {field: granule.grab(search) for field, search in fields.items()}

            # calculate dna damage at 5m
            underwater = data[tag]['underwater']
            coefficient = data[tag]['coefficient']
            irradiance = underwater * numpy.exp(-coefficient * 5)
            damage = (irradiance * spectrum).sum(axis=2)
            data[tag]['damage'] = damage

            # calculate penetration depths
            data[tag]['depth'] = -1 * numpy.log(0.1) / data[tag]['coefficient']

            # calculate underwater at 5 meters
            data[tag]['underwater'] = data[tag]['underwater'] * numpy.exp(-5 * data[tag]['coefficient'])

            # subset positions
            data[tag].update({field: data[tag][field][:, :, position] for field, position in positions.items()})

        # calculate percent differences
        differences = {field: self._relate(*[data[tag][field] for tag in tags]) for field in data[tags[0]].keys()}

        # create mask based on simulation bounds, with a margin
        coordinates = data[tags[0]]
        latitude = coordinates['latitude']
        longitude = coordinates['longitude']
        bounds = (latitude.min(), latitude.max())
        boundsii = (longitude.min(), longitude.max())
        margin = 2
        marginii = 2

        # create mask for original data
        means = latitude.mean(axis=1)
        meansii = longitude.mean(axis=0)
        south = (means < bounds[0]).tolist().index(False)
        north = ((means > bounds[1]).tolist() + [True]).index(True)
        west = (meansii < boundsii[0]).tolist().index(False)
        east = ((meansii > boundsii[1]).tolist() + [True]).index(True)

        # construct trapezoid
        pairs = [(0, 0), (0, -1), (-1, -1), (-1, 0), (0, 0)]
        trapezoid = numpy.array([latitude[pair] for pair in pairs])
        trapezoidii = numpy.array([longitude[pair] for pair in pairs])
        style = 'dashed'

        # make lines
        lines = [(trapezoidii, trapezoid, style)]

        # for each field
        words = words or list(stubs.keys())
        for field in words:

            # set gradient
            gradient = 'coolwarm'
            selection = [index for index in range(256)]

            # except for exclusions
            exclusions = ['latitude', 'longitude', 'anomaly']
            if field not in exclusions:

                # set pace components
                tracer = differences[field]

                print('tracer: ')
                print(tracer.min(), tracer.max())

                # find pace middle latitude min and max
                half = int(latitude.shape[1] / 2)
                lower = latitude[0, half]
                upper = latitude[-1, half]

                # create masks for finite data
                mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (tracer > -999)

                print('mask: ')
                print(mask.sum())

                # get percentile bounds
                total = tracer[mask]
                lower = numpy.percentile(total, 5)
                upper = numpy.percentile(total, 95)
                total = total[(total > lower) & (total < upper)]

                # get mean and stdev of pace data
                mean = total.mean()
                deviation = max([total.std(), 0.00001])
                minimum = -abs(mean) -sigmas * deviation
                maximum = abs(mean) + sigmas * deviation

                print('mean, std: ')
                print(mean, deviation, minimum, maximum)

                # set plot scale
                scale = (minimum, maximum)

                # set units
                unit = units[field]

                # clip at upper and lower bounds
                tracer = numpy.where((tracer < scale[0] + epsilon) & (tracer > fill), scale[0] + epsilon, tracer)
                tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)

                print('tracer min, max, after clipping:')
                print(tracer.min(), tracer.max())

                # replace masked values with fills
                latitude = numpy.where(mask, latitude, fill)
                longitude = numpy.where(mask, longitude, fill)
                tracer = numpy.where(mask, tracer, fill)

                # setup pace plot, zoom
                title = '{} {}, v2 - v1, 2024-04-11'.format(inserts['pace'][field], stubs[field])
                destination = '{}/{}/PACE_Prox_diff_{}_{}.png'.format(self.sink, folder, tag, field, tag)
                limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                size = (5, 7)
                logarithm = logarithms.get(field, False)
                back = 'white'
                limits = zoom or limits

                # plot
                self._stamp('plotting {}...'.format(destination))
                parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                parameters += [scale, [gradient, selection], limits, resolution, lines]
                options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                self._paint(*parameters, **options)

                # print status
                self._stamp('plotted.'.format(field))

        return None

    def control(self, controller=None, source=None, replacements=None, trim=False, now=''):
        """Generate a control file.

        Arguments:
            controller: str, control file path
            source: str, file path to Description.txt
            replacements: dict, control file overrides
            trim: boolean, trim source and sink folders form inputs and outputs?
            now: overrides current now for given now, for keeping production time constant for updates

        Returns:
            None
        """

        # set defaults
        controller = controller or '../utd/control.txt'.format(self.sink)
        source = source or '../doc'.format(self.sink)
        replacements = replacements or {'inputs': {}, 'outputs': {}, 'runtime': {}}

        # grab description file
        description = self._acquire('{}/Description.txt'.format(source))

        # begin control file
        control = {'Input Files': {}, 'Output Files': {}}
        inputs = control['Input Files']

        # transfer static inputs from description file
        for entry in description['Static Input Files']:

            # add entry to inputs
            inputs.update({entry['ESDT']: [entry['Filename']]})

        # # add static OMTO3 inputs
        # inputs.update({400100: ['OMI-Aura_L1-OML1BIRR_2004m1231t1248-o99002_v003-2007m0511t172858.he4']})
        # inputs.update({400122: ['CLIM/TEMP_CLIM.txt']})
        # inputs.update({400123: ['umk_lyr.txt']})
        # inputs.update({400125: ['slope_res340.txt']})
        # inputs.update({400150: ['V8ANC/terrainPressure0303.he4']})
        # inputs.update({400151: ['V8ANC/omcldrr_pressure.he4']})
        # inputs.update({400152: ['V8ANC/v8snowice.he4']})
        # inputs.update({400400: ['LUT/NVAL_LUT_OMI_007_MRG_200_newcoe.h5']})
        # inputs.update({400401: ['LUT/DNDX_LUT_OMI_007_042_200.h5']})
        # inputs.update({410100: ['OMTO3.MCF']})
        # inputs.update({420001: ['NVADJ_2019m1016.he4']})
        # inputs.update({'LEAPSECT': ['leapsec.dat.2020073154986']})

        # add dynamic inputs
        # cloud = '/tis/acps/OMI/70004/OMCLDRR/'
        # cloud += '2005/01/07/OMI-Aura_L2-OMCLDRR_2005m0107t2248-o002573_v134-2022m0727t125141.he5'
        # inputs.update({'OMCLDRR': [cloud]})
        irradiance = 'OMI-Aura_L1-OML1BIRR_2005m0107t2248-o002573_v0401-2022m0615t1959_alt.nc'
        inputs.update({'OML1BIRR': [irradiance]})
        ultraviolet = 'OMI-Aura_L1-OML1BRUG_2005m0107t2248-o002573_v0401-2022m0615t1959_alt.nc'
        inputs.update({'OML1BRUG': [ultraviolet]})
        visible = 'OMI-Aura_L1-OML1BRVG_2005m0107t2248-o002573_v0401-2022m0615t1959_alt.nc'
        inputs.update({'OML1BRVG': [visible]})
        # anomaly = 'OMI-Aura_L4-OMTO3RAFLG_2004m0809-2019m0422_v003-2019m0422t143322.h5'
        # inputs.update({'OMTO3RAFLG': [anomaly]})
        # wind = '/tis/acps/OMI/70004/OMUFPMET/'
        # wind += '2005/01/07/OMI-Aura_ANC-OMUFPMET_2005m0107t224808-o002573_v004-2022m0730t130045.nc'
        # inputs.update({'OMUFPMET': [wind]})
        ancillary = '/tis/acps/OMI/70004/OMUANC/'
        ancillary += '2005/01/07/OMI-Aura_ANC-OMUANC_2005m0107t224808-o002573_v004-2022m1110t013808.nc'
        inputs.update({'OMUANC': [ancillary]})

        # add additional dynamic inputs
        # chlorophyll = '/tis/acps/OMI/70004/OMGLER/'
        # chlorophyll += '2005/01/07/OMI-Aura_L2-OMGLER_2005m0107t224808-o02573_v134-2023m0103t142426.he5'
        # inputs.update({'OMGLER': [chlorophyll]})
        ozone = '/tis/acps/OMI/92005/OMTO3/'
        ozone += '2005/01/07/OMI-Aura_L2-OMTO3_2005m0107t2248-o02573_v003-2020m0730t205034.he5'
        inputs.update({'OMTO3': [ozone]})

        # add undefined inputs
        undefined = ['BACKUP_IRR/OMI-Aura_L1-OML1BIRR_2004m1231t1248-o99002_v003-2007m0511t172858.he4']
        undefined += ['CLIM/TEMP_CLIM.txt', 'V8ANC/terrainPressure0303.he4', 'V8ANC/omcldrr_pressure.he4']
        undefined += ['V8ANC/v8snowice.he4', 'OMTO3.MCF', 'CLIM/umk_lyr.txt', 'slope_res340.txt']
        undefined += ['LUT/NVAL_LUT_OMI_007_MRG_200_newcoe.h5', 'LUT/DNDX_LUT_OMI_007_042_200.h5']
        undefined += ['NVADJ_2019m1016.he4']
        inputs.update({'UNDEF': undefined})

        # add output file
        outputs = control['Output Files']
        erythema = 'OMOCNUV_Erythema_02573.h5'
        outputs.update({'OMOCNUV': [erythema]})

        # copy most fileds into control
        exclusions = ['Dynamic Input Files', 'Output Files', 'Runtime Parameters', 'Static Input Files']
        control.update({key: value for key, value in description.items() if key not in exclusions})

        # copu runtime parameters without headings
        runtime = 'Runtime Parameters'
        control[runtime] = {entry['Param']: entry['Value'] for entry in description[runtime]}

        # if trimming
        if trim:

            # remove directories from inputs
            replacements['inputs'] = {earth: self._file(entry) for earth, entry in replacements['inputs'].items()}
            replacements['outputs'] = {earth: self._file(entry) for earth, entry in replacements['outputs'].items()}

        # update inputs and outputs with replacements
        inputs.update(replacements['inputs'])
        outputs.update(replacements['outputs'])

        # update runtime parameters with replacements
        control[runtime].update(replacements['runtime'])

        # dump into control
        controller = controller or '{}/utd/control.txt'.format(self.sink)
        self._dispense(control, controller)

        return None

    def correct(self):
        """Correct the north Africa aerosol plot after correcting aerosol interpolation.

        Arguments:
            None

        Returns:
            None
        """

        # make plots folder
        self._make('{}/plots/correction'.format(self.sink))

        # get the trending file
        hydra = Hydra('{}/trends'.format(self.sink))
        hydra.ingest('2005_00N_000E_single')

        # grab data
        tau = hydra.grab('tau_305')
        aerosol = hydra.grab('aerosol_305')
        orbit = hydra.grab('orbit')
        time = hydra.grab('time')

        # get all pixels with aerosol factor less than 0.2
        pixels = hydra._mask(aerosol < 0.2)

        # get hydra for corrections
        hydraii = Hydra('{}/correction'.format(self.sink))

        # for each pixel
        for pixel in pixels:

            print(orbit[pixel])

            # ingest based on orbit
            hydraii.ingest(str(int(orbit[pixel])))

            # get latitude, longitude, and aersol optical depths, and aerosol factor, index 15 for 305nm
            latitude = hydraii.grab('latitude')
            longitude = hydraii.grab('longitude')
            factor = hydraii.grab('aerosol_absorption_factor')[:, :, 15]
            optical = hydraii.grab('aerosol_optical_depth_354')
            opticalii = hydraii.grab('aerosol_optical_depth_388')

            # find pixel closet to 0,0
            pixelii = hydraii._pin([0, 0], [latitude, longitude])[0]

            # calculate tau 388
            ratio = numpy.log(optical[pixelii] / opticalii[pixelii])
            exponent = ratio / numpy.log(388 / 354)
            depth = opticalii[pixelii] * (305 / 388) ** -exponent

            # insert factor and depth into trends
            index = pixel[0]
            tau[index] = depth
            aerosol[index] = factor[pixelii]

        # plot new data
        reference = 2005

        # create datetime objects form timestamp
        dates = [datetime.datetime.fromtimestamp(int(stamp)) for stamp in time]

        # reference all dates to reference year and convert to timestamp
        datesii = [datetime.datetime(reference, date.month, date.day, date.hour) for date in dates]
        time = numpy.array([date.timestamp() for date in datesii])

        # set fields
        fields = ['factor_305', 'tau_305']
        arrays = [aerosol, tau]

        # set fills
        fill = -999
        fillii = 1e20

        # for each field
        for field, array in zip(fields, arrays):

            # create fill masque
            mask = (array > fill) & (abs(array) < fillii) & (numpy.isfinite(array))
            masque = mask

            # create title
            title = 'Aerosol, {}, N.Africa ( 0N, 0E )'.format(field)

            # create new destination and plot
            address = 'plots/correction/NAfrica_{}.h5'.format(field)
            self.squid.ink(field, array[masque], 'month', time[masque] * 1000, address, title)

        return None

    def cruise(self, latitudes=None, longitude=None, comparisons=None, wave=305):
        """Compare Kd values calculated for Tedetti table.

        Arguments:
            latitudes: list of float
            longitudes: list of float
            wave: wavelength
            comparisons: list of float

        Returns
            list of Kd values
        """

        # set latitudes
        latitudes = [-8, -9, -12, -16, -17, -19, -22, -26, -29, -32, -32, -33, -34]
        longitudes = [-142, -137, -134, -130, -128, -126, -120, -114, -104, -91, -87, -84, -73]
        comparisons = [0.239, 0.228, 0.192, 0.190, 0.139, 0.121, 0.083, 0.108, 0.122, 0.194, 0.234, 0.259, 0.756]
        greens = [0.320, 0.152, 0.172, 0.061, 0.060, 0.049, 0.027, 0.029, 0.029, 0.084, 0.132, 0.139, 1.442]

        # get list of december
        hydra = Hydra('../studies/diatom/cardinals')
        paths = [path for path in hydra.paths if '2005m12' in hydra._stage(path)['date']]

        # for each pair
        pairs = list(zip(latitudes, longitudes))

        # collect data
        collection = {tuple(pair): {'pairs': [], 'constants': [], 'chlorophylls': []} for pair in pairs}

        # for each path
        for path in paths:

            # ingest the path
            hydra.ingest(path)

            # grab contents
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')
            wavelength = hydra.grab('wavelength')
            constant = hydra.grab('constant_planar')
            chlorophyll = hydra.grab('chlorophyll')

            # find the wavelength index
            index = hydra._pin(305, wavelength)[0][0]

            # for each pair
            for pair in pairs:

                # find the naerest pixel
                pixel = hydra._pin(pair, [latitude, longitude])[0]

                # add the cooridinates
                collection[tuple(pair)]['pairs'].append((latitude[pixel], longitude[pixel]))
                collection[tuple(pair)]['constants'].append(constant[pixel][index])
                collection[tuple(pair)]['chlorophylls'].append(chlorophyll[pixel])

        # find closests
        matches = {}
        for pair, comparison in zip(pairs, comparisons):

            # get pairsii
            pairsii = collection[tuple(pair)]['pairs']
            constants = collection[tuple(pair)]['constants']
            chlorophylls = collection[tuple(pair)]['chlorophylls']

            # make differernces
            differences = [(pair[0] - pairii[0]) ** 2 + (pair[1] - pairii[1]) ** 2 for pairii in pairsii]

            # sort differences
            first = numpy.array(differences).argsort()[0]

            # add to matches
            matches[tuple(pair)] = [comparison, constants[first], pairsii[first], chlorophylls[first]]

        # plot matches
        ones = [match[0] for match in matches.values()]
        twos = [match[1] for match in matches.values()]

        # make plot
        self._make('../studies/diatom/action/plots/gyre')
        self.squid.ink('ones', ones, 'twos', twos, 'plots/gyre/Tedetti_Comparison.h5', 'Tedetti')

        # plot chlorophyll matches
        ones = [green for green in greens]
        twos = [match[-1] for match in matches.values()]

        # make plot
        self._make('../studies/diatom/action/plots/gyre')
        self.squid.ink('ones', ones, 'twos', twos, 'plots/gyre/Tedetti_Comparison_Chloro.h5', 'Tedetti')

        return matches

    def crystallize(self, orbit=2573):
        """Crystallize a moby orbit into interpolated values.

        Arguments:
            None

        Returns:
            None
        """

        # get tables and arrays
        data = self.mobilize(orbit=orbit)
        table = self.tabulate()

        # stamp
        self._stamp('interpolating...', initial=True)

        # for each sample
        fluxes = ['Ed', 'Eu', 'Eod', 'Eou', 'Ed']
        interpolations = {flux: [] for flux in fluxes}
        zipper = list(zip(data['ozone'], data['SZA_OMI'], data['MOD_chl']))

        # go through each flux
        for flux in fluxes:

            # print status
            print('flux table {}...'.format(flux))

            for ozone, zenith, chlorophyll in zipper:

                # only if chlorophyll is small enough
                if chlorophyll <= 10:

                    # copy table
                    xerox = table[flux].copy()

                    # interpolate over ozone
                    interpolation = self.interpolate(xerox, ozone, table['ozones'], 4)

                    # interpolate over zenith
                    interpolation = self.interpolate(interpolation, zenith, table['zeniths'], 2)

                    # interpolate over chlorophyll
                    interpolation = self.interpolate(interpolation, chlorophyll, table['chlorophylls'], 1)

                    # append to interpolations
                    interpolations[flux].append(interpolation)

        # stamp
        self._stamp('interpolated.')

        return data, table, interpolations

    def cycle(self):
        """Plot minimum noons across all julian days.

        Arguments:
            None

        Returns:
            None
        """

        # make sink directory
        sink = 'plots/noon'
        self._make('{}/{}'.format(self.sink, sink))

        # set up latitudes and longitudes
        latitudes = [90, 89, 88, 85, 60, 30, 15, 0, -15, -30, -60, -85, -88, -89, -90]
        longitudes = [-90, -30, 0, 30, 90]

        # construct julian days
        julians = list(range(1, 366))

        # for each latitude
        for index, latitude in enumerate(latitudes):

            # and each longitude
            for longitude in longitudes:

                # construct noon zenith angles for all days
                noons = [min(self.decline(latitude, longitude, julian=julian)) for julian in julians]

                # format latitude and longitude
                north = '{}{}'.format('N' * (latitude >= 0) + 'S' * (latitude < 0), self._pad(abs(latitude)))
                east = '{}{}'.format('E' * (longitude >= 0) + 'W' * (longitude < 0), self._pad(abs(longitude)))

                # make plot
                title = 'Minimum Solar Zenith, {}, {}'.format(north, east)
                address = '{}/Minimum_solar_zenith_{}_{}_{}.h5'.format(sink, self._pad(index), north, east)
                name = 'zenith_{}'.format(east).lower()
                self.squid.ink(name, noons, 'julian_day', julians, address, title)

        return None

    def decline(self, latitude=0, longitude=0, date=None, julian=None):
        """Calculate declination angle and all points of solar zenith angle for ful 24 hour cycle.

        Arguments:
            date: str, yyyy-mm-dd
            latitude: float, latitude
            longitude: float, longitude

        Returns:
            None
        """

        # if no julian given:
        if not julian:

            # determine julian day
            year, month, day = [int(number) for number in date.split('-')]
            julian = datetime.datetime(year, month, day).timetuple().tm_yday

        # calculate radians from degrees as in sunang1.f
        degrees = 57.29578
        theta = 360 * (julian - 1) / 365
        radians = theta / degrees

        # calculate solar declination angle
        declination = 0.396372 - 22.91327 * numpy.cos(radians) + 4.02543 * numpy.sin(radians)
        declination += -0.387205 * numpy.cos(2 * radians) + 0.051967 * numpy.sin(2 * radians)
        declination += -0.154527 * numpy.cos(3 * radians) + 0.084798 * numpy.sin(3 * radians)

        # compute time correction
        correction = 0.004297 + 0.107029 * numpy.cos(radians) - 1.837877 * numpy.sin(radians)
        correction += -0.837378 * numpy.cos(2 * radians) - 2.340475 * numpy.sin(2 * radians)

        # for each hour
        angles = []
        for index in range(24):

            # calculate hour
            hour = float(index + 1) - longitude / 15.

            # correct for hours outside 0 - 24
            if hour < 0: hour += 24
            if hour > 24: hour += -24

            # compute solar hour angle
            angle = (hour - 12) * 15 + longitude + correction

            # correct to get angle within -180 to 180
            if angle > 180: angle += -360
            if angle < -180: angle += 360

            # convert to radians
            angleii = angle / degrees
            latitudeii = latitude / degrees
            longitudeii = longitude / degrees
            declinationii = declination / degrees

            # compute cosine for zenith
            cosine = numpy.sin(latitudeii) * numpy.sin(declinationii)
            cosine += numpy.cos(latitudeii) * numpy.cos(declinationii) * numpy.cos(angleii)
            absolute = abs(cosine)

            # if cosine is between 1 and 1.1
            if (absolute >= 1.0) & (absolute <= 1.1):

                # make correction
                if cosine > 0: cosine = 1
                if cosine < 0: cosine = -1

            # compute zenith
            zenith = numpy.arccos(cosine)

            # compute cosine for azimuth
            numerator = numpy.sin(declinationii) - numpy.sin(latitudeii) * numpy.cos(zenith)
            denominator = numpy.cos(latitudeii) * numpy.sin(zenith)
            cosineii = numerator / denominator

            # compute abosolute
            absoluteii = abs(cosineii)

            # if cosine is between 1 and 1.1
            if (absoluteii >= 1.0) & (absoluteii <= 1.1):

                # make correction
                if cosineii > 0: cosineii = 1
                if cosineii < 0: cosineii = -1

            # compute azimuth
            azimuth = numpy.arccos(cosineii)

            # correct for angle
            if angle > 0: azimuth = (360 / degrees) - azimuth

            # convert to degrees
            zenithii = zenith * degrees
            azimuthii = azimuth * degrees

            # add to list
            angles.append(zenithii)

        return angles

    def define(self):
        """Define the earth science data type.

        Arguments:
            None

        Returns:
            None
        """

        # begin definition
        definition = {}

        # add esdt name and processing level
        definition['ESDT Name'] = 'OMOCNUV'
        definition['Processing Level'] ='L2'

        # add Long Name
        name = 'OMI/Aura Ocean Color and Erythemal Dose Rates, 1-Orbit'
        definition['Long Name'] = name

        # add description
        text = 'Single orbit calculation of ultraviolet atmospheric flux and oceanic penetration '
        text += 'based on L1B data, OMTO3, and OMGLER. '
        definition['Description'] = text

        # add conntacts
        definition['Science Team Contacts'] = ['Alexandre Vassilkov (alexander.vasilkov@ssaihq.com)']
        definition['Science Team Contacts'] += ['Nickolay Krotkov (nickolay.a.krotkov@nasa.gov)']
        definition['Science Team Contacts'] += ['David Haffner (david.haffner@ssaihq.com)']
        definition['Science Team Contacts'] += ['Matthew Bandel (matthew.bandel@ssaihq.com)']
        definition['Support Contact'] = 'Phillip Durbin (pdurbin@sesda.com)'

        # define sizes
        definition['Minimum Size'] = '6 MB'
        definition['Maximum Size'] = '700 MB'

        # define file name pattern
        pattern = '<Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>'
        pattern += '_v<Collection>-*.<File Format>'
        definition['Filename Pattern'] = pattern

        # add filename elements
        definition['Platform'] = 'Aura'
        definition['Instrument'] = 'OMI'
        definition['File Format'] = 'nc'

        # add other processing elements
        definition['Period'] = 'Orbits=1'
        definition['Archive Method'] = 'Compress'
        definition['File SubTable Type'] = 'L1L2'
        definition['Extractor Type'] = 'Filename_L1L2'
        definition['Metadata Type'] = 'orbital'
        definition['Parser'] = 'filename'
        definition['keypattern'] = '<OrbitNumber>'

        # dump into yaml
        destination = '{}/doc/OMOCNUV.txt'.format(self.sink)
        self._dispense(definition, destination)

        # clean up spacing
        self._disperse(destination)

        return None

    def describe(self, version=None, template=None):
        """Generate the descriptions.txt file.

        Arguments:
            version: str, version number
            template: str, template file

        Returns:
            None
        """

        # set default template
        template = 'OMOCNUV_omi_template.nc'

        # load in the esdt
        definition = self._acquire('{}/doc/OMOCNUV.txt'.format(self.sink))

        # begin description
        description = {}

        # add APP Name
        name = definition['ESDT Name']
        description['APP Name'] = name

        # add APP Type
        description['APP Type'] = 'OMI'

        # add APP Version, from input if not specified
        version = version or input('version? ')
        description['APP Version'] = version

        # add Long Name
        description['Long Name'] = definition['Long Name']

        # add description, apply block form
        description['Description'] = '>\n' + definition['Description']

        # add algorithm lead
        description['Lead Algorithm Scientist'] = definition['Science Team Contacts'][0]

        # add other scientists
        description['Other Algorithm Scientists'] = definition['Science Team Contacts'][1:-1]

        # add software developer
        description['Software Developer'] = definition['Science Team Contacts'][-1]

        # add software developer
        description['Support Contact'] = definition['Support Contact']

        # add structure field
        structure = '>\nRun {}.exe as a fortran executable, using the control.txt file path as its argument.'.format(name)
        description['Structure'] = structure

        # add operational scenario
        scenario = '>\nRun on orbits as requested.'
        description['Operational Scenario'] = scenario

        # add the period
        description['Period'] = definition['Period']

        # add the execution
        #description['EXE'] = {'Name': '{}.py'.format(name), 'Program': '{}.py'.format(name)}
        #description['EXE'] = {'Name': '{}.py'.format(name), 'Program': '{}.py'.format(name)}

        # add dependencies
        description['Dependencies'] = []
        dependencies = description['Dependencies']
        #dependencies.append({'CI': 'UVB_UVP', 'Version': '0.0.1'})
        #dependencies.append({'CI': 'TO3_CORE', 'Version': '3.1.0'})
        dependencies.append({'CI': 'MESSAGES', 'Version': '2.2.1'})
        dependencies.append({'CI': 'CONFIG_READER', 'Version': '2.2.1'})
        dependencies.append({'CI': 'OMI_L1B_Reader', 'Version': '4.1.8'})
        dependencies.append({'CI': 'OMI_ANC_Reader', 'Version': '0.0.2'})

        # begin description of static input files
        static = 'Static Input Files'
        description[static] = []
        statics = description[static]

        # add static OMTO3 inputs
        irradiance = 'OMI-Aura_L1-OML1BIRR_2004m1231t1248-o002465_v0401-2022m0615t1814.nc'
        statics.append({'ESDT': 'IRR', 'Filename': irradiance, 'Desc': 'static irradiance'})
        # statics.append({'ESDT': 'IRR', 'Filename': irradiance, 'Desc': 'static irradiance data'})
        # statics.append({'ESDT': 'CLIMT', 'Filename': 'CLIM/TEMP_CLIM.txt', 'Desc': 'temperature climatology'})
        # statics.append({'ESDT': 'CLIMU', 'Filename': 'CLIM/umk_lyr.txt', 'Desc': 'leap year data'})
        # statics.append({'ESDT': 'SLOPER', 'Filename': 'slope_res340.txt', 'Desc': 'residual slopes, 340nm'})
        statics.append({'ESDT': 'V8ANCT', 'Filename': 'terrainPressure0303.he4', 'Desc': 'terrain pressures'})
        statics.append({'ESDT': 'V8ANCO', 'Filename': 'omcldrr_pressure.he4', 'Desc': 'OMCLDRR cloud pressures'})
        statics.append({'ESDT': 'V8ANCS', 'Filename': 'v8snowice.he4', 'Desc': 'snow/ice data'})
        statics.append({'ESDT': 'LUTNVAL', 'Filename': 'NVAL_LUT_OMI_007_MRG_200_newcoe.h5', 'Desc': 'nvalues'})
        # statics.append({'ESDT': 'LUTDNDX', 'Filename': 'DNDX_LUT_OMI_007_042_200.h5', 'Desc': 'nvalue slopes'})
        # statics.append({'ESDT': 'OMTO3MCF', 'Filename': 'OMTO3.MCF', 'Desc': 'OMTO3 data'})
        # statics.append({'ESDT': 'NVADJ', 'Filename': 'NVADJ_2019m1016.he4', 'Desc': 'nvalue adjustments'})
        statics.append({'ESDT': 'LEAPSEC', 'Filename': 'leapsec.dat', 'Desc': 'leap seconds record'})

        # begin description of dynamic input files
        dynamic = 'Dynamic Input Files'
        description[dynamic] = []
        dynamics = description[dynamic]

        # # add OMCLDRR
        # contents = 'OMI Rotational Raman Cloud Pressures'
        # dynamics.append({'ESDT': 'OMCLDRR', 'Rule': 'Required', 'Desc': contents})

        # add OML1BIRR
        contents = 'OMI Level 1B irradiance data'
        dynamics.append({'ESDT': 'OML1BIRR', 'Rule': 'Required', 'Desc': contents})

        # add OML1BRVG
        contents = 'OMI Level 1B radiance data, UV Bands 1 and 2'
        dynamics.append({'ESDT': 'OML1BRUG', 'Rule': 'Required', 'Desc': contents})

        # add OML1BRUG
        contents = 'OMI Level 1B radiance data, Visible Band 3'
        dynamics.append({'ESDT': 'OML1BRVG', 'Rule': 'Required', 'Desc': contents})

        # # add OMTO3RAFLG row anomaly flags
        # contents = 'OMI Level 1B row anomaly flags'
        # dynamics.append({'ESDT': 'OMTO3RAFLG', 'Rule': 'Required', 'Desc': contents})

        # # add OMUFPMET wind speed data
        # contents = 'OMI Level 1B wind speed data'
        # dynamics.append({'ESDT': 'OMUFPMET', 'Rule': 'Required', 'Desc': contents})

        # add OMUANC ancillary data
        contents = 'OMI Ancillary data'
        dynamics.append({'ESDT': 'OMUANC', 'Rule': 'Required', 'Desc': contents})

        # add OMUSNICE ancillary data
        contents = 'OMI Snow and Ice Flag data'
        dynamics.append({'ESDT': 'OMUFPSLV', 'Rule': 'Required', 'Desc': contents})

        # add OMGLER for chlorophyll data
        contents = 'OMI LER, Chlorophyll data'
        # dynamics.append({'ESDT': 'OMGLERPRE', 'Rule': 'Required', 'Desc': contents})
        dynamics.append({'ESDT': 'OMGLER', 'Rule': 'Required', 'Desc': contents})

        # add OMTO3 for total ozone data
        contents = 'OMI Total ozone'
        dynamics.append({'ESDT': 'OMTO3', 'Rule': 'Required', 'Desc': contents})

        # add OMAERUV for aerosol data
        contents = 'OMI aerosol data'
        dynamics.append({'ESDT': 'OMAERUV', 'Rule': 'Required', 'Desc': contents})

        # start outputs
        output = 'Output Files'

        # begin entry for Filename
        pattern = definition['Filename Pattern']

        # for each filename parameterr
        for parameter in ('Instrument', 'Platform', 'ESDT Name', 'Processing Level', 'File Format'):

            # replace with esdt definition
            pattern = pattern.replace('<{}>'.format(parameter), definition[parameter])

        # add start time format and collection for now
        pattern = pattern.replace('<DataDate>', '<StartTime!%Ym%m%dt%H%M%S>')
        pattern = pattern.replace('<Collection>', '<ECSCollection>')

        contents = 'OMOCNUV Ocean color and Erythemal Dose Rates'
        description[output] = [{'Filename': pattern, 'ESDT': name, 'Rule': 'Required', 'Desc': contents}]

        # make entry for Runtime Parameters
        runtime = 'Runtime Parameters'
        description[runtime] = []
        runtimes = description[runtime]
        runtimes.append({'Param': 'AppShortName', 'Value': name})
        runtimes.append({'Param': 'Instrument', 'Value': definition['Instrument']})
        runtimes.append({'Param': 'Platform', 'Value': definition['Platform']})
        runtimes.append({'Param': 'OrbitNumber', 'Value': '"<OrbitNumber>"'})
        runtimes.append({'Param': 'StartTime', 'Value': '"<StartTime>"'})
        # runtimes.append({'Param': 'ProductionTime', 'Value': '"<ProductionTime>"'})
        runtimes.append({'Param': 'ECSCollection', 'Value': '"<ECSCollection>"'})
        runtimes.append({'Param': 'Maximum Level of Detail', 'Value': '2'})

        # make entry for output netcdf template
        runtimes.append({'Param': 'Template', 'Value': template})

        # add addiational runtime parameters from old control file
        runtimes.append({'Param': 'APPVersion', 'Value': version})
        runtimes.append({'Param': 'APRIORIOZONEPROFILESOURCE', 'Value': '"Climatology"'})
        runtimes.append({'Param': 'Aerosol Limit', 'Value': 5.0})
        runtimes.append({'Param': 'AuthorAffiliation', 'Value': '"NASA/GSFC"'})
        runtimes.append({'Param': 'AuthorName', 'Value': '"U.S. OMI Science Team"'})
        runtimes.append({'Param': 'CLOUDPRESSURESOURCE', 'Value': '"OMCLDRR"'})
        runtimes.append({'Param': 'EndTime', 'Value': '2019-08-06T13:26:18.000000Z'})
        runtimes.append({'Param': 'HDFCompress', 'Value' : 1})
        runtimes.append({'Param': 'InstrumentConfigAS', 'Value': '10004'})
        runtimes.append({'Param': 'InstrumentName', 'Value': '"OMI"'})
        runtimes.append({'Param': 'LOCALVERSIONID', 'Value': '"RFC1321 MD5 = not yet calculated"'})
        runtimes.append({'Param': 'OPERATIONMODE', 'Value': '"Normal"'})
        runtimes.append({'Param': 'OrbitNumber', 'Value': '006215'})
        runtimes.append({'Param': 'PGE', 'Value': 'OMTO3'})
        runtimes.append({'Param': 'PGEVersion', 'Value': '"0.0.16.0"'})
        runtimes.append({'Param': 'ProcessLevel', 'Value': '"2"'})
        runtimes.append({'Param': 'ProcessingCenter', 'Value': 'OMI SIPS'})
        runtimes.append({'Param': 'ProcessingHost', 'Value': 'Linux minion7074 3.10.0-1062.12.1.el7.x86_64 x86_64'})
        runtimes.append({'Param': 'ProdruleEngine', 'Value': 'SIPS'})
        runtimes.append({'Param': 'REPROCESSINGACTUAL', 'Value': '"processed 1 time"'})
        runtimes.append({'Param': 'ReprocessingActual', 'Value': 'processed 1 time'})
        runtimes.append({'Param': 'Residual Limit', 'Value': 0.6})
        runtimes.append({'Param': 'SMF Verbosity Threshold', 'Value': 2})
        runtimes.append({'Param': 'SMFVerbosityThreshold', 'Value': 2})
        runtimes.append({'Param': 'SNOWICESOURCE', 'Value': '"Climatology"'})
        runtimes.append({'Param': 'Source', 'Value': 'OMI'})
        runtimes.append({'Param': 'StartTime', 'Value': '2005-01-07T22:48:08.000000Z'})
        runtimes.append({'Param': 'SwathName', 'Value': '"OMI Column Amount O3"'})
        runtimes.append({'Param': 'TDAutoPlan', 'Value': ''})
        runtimes.append({'Param': 'TDOPFIntendedPurpose', 'Value': 'Forward Processing'})
        runtimes.append({'Param': 'TDOPFVersion', 'Value': '1303'})
        runtimes.append({'Param': 'TEMPERATURESOURCE', 'Value': '"FPIT"'})
        runtimes.append({'Param': 'TERRAINPRESSURESOURCE', 'Value': '"ETOPO-5"'})
        runtimes.append({'Param': 'Toolkit version string', 'Value': '5.2.20'})
        runtimes.append({'Param': 'VERSIONID', 'Value': '004'})
        runtimes.append({'Param': 'wl_max', 'Value': 400.0})
        runtimes.append({'Param': 'wl_min', 'Value': 300.0})
        runtimes.append({'Param': 'wl_oz1', 'Value': 317.35})
        runtimes.append({'Param': 'wl_oz2', 'Value': 322.0})
        runtimes.append({'Param': 'wl_ref1', 'Value': 340.0})
        runtimes.append({'Param': 'wl_ref2', 'Value': 380.0})
        runtimes.append({'Param': 'wl_res', 'Value': 331.06})
        runtimes.append({'Param': 'Testing', 'Value': 1})

        # make entry for Production Rules
        rules = 'Production Rules'
        description[rules] = [{'Rule': 'GetOrbitParams'}]

        # add orbital match requirements from dynamic input esdts
        earths = [entry['ESDT'] for entry in dynamics]
        matches = {earth: 'OrbitMatch' for earth in earths}
        additions = {earth: {} for earth in earths}
        # matches['OMTO3RAFLG'] = 'L3Match'
        matches['OML1BIRR'] = 'ClosestOrbit'
        additions['OML1BIRR'] = {'EarlyDelta': 15, 'LateDelta': 15}
        for earth in earths:

            # add description rule
            description[rules] += [{'Rule': matches[earth], 'ESDT': earth, 'Min_Files': 1}]
            description[rules][-1].update(additions[earth])

        # add compiler
        description['Compilers'] = [{'CI': 'python', 'Version': '3.6.8'}]

        # add environment
        description['Environment'] = [{'CI': 'env_setup', 'Version': '0.0.9'}]
        description['Environment'] += [{'CI': 'python', 'Version': '3.6.8'}]

        # add operating system
        description['Operating System'] = [{'CI': 'Linux', 'Version': '2.6.9'}]

        # dump into yaml
        destination = '{}/doc/Description.txt'.format(self.sink)
        self._dispense(description, destination)

        # improve spacing
        self._disperse(destination)

        return None

    def diagram(self, tag, name, search, third=None, digits=2, bracket=None, resolution=10, blocks=7):
        """Diagram the orbit for a particular feature.

        Arguments:
            tag: str, tag of output file
            name: str, name of feature
            search: str, search term for feature
            third: int, third index
            digits: int, digits to round brackets
            bracket: tuple of floats, minimum and maximum
            resolution: int, resolution for polygons
            blocks: number of non-fill blocks

        Returns:
            None
        """

        # set fill value
        fill = -999

        # get the tagged file
        ocean = self._pull(tag)

        # grab feature
        feature = ocean.dig(search)[0]
        array = ocean.grab(search)

        # if third index is offered
        if third is not None:

            # reduce by third index
            array = array[:, :, third]

        # create images and rows arrays
        shape = array.shape
        images = numpy.array([list(range(shape[0]))] * shape[1]).transpose(1, 0)
        rows = numpy.array([list(range(shape[1]))] * shape[0])

        # create polygons
        polygons = self._polymerize(images, rows, resolution)

        # get indices based on resolution
        selection = numpy.array([index for index in range(images.shape[0]) if index % resolution == 0])

        # get minimum and maximum
        minimum = array[array > fill].min()
        maximum = array[array > fill].max()

        # if bracket
        if bracket is not None:

            # reset min and max
            minimum = bracket[0]
            maximum = bracket[1]

        # apply selection
        array = array[selection]
        images = images[selection]
        rows = rows[selection]

        # create bins
        size = (maximum - minimum) / blocks
        bounds = [minimum - size] + [minimum + index * size for index in range(blocks + 1)]
        bounds = [self._round(bound, digits) for bound in bounds[:-1]] + [self._round(bounds[-1], digits, up=True)]

        # elevate fill values to minimum
        array = numpy.where(array < minimum, minimum - size, array)

        # apply dummy  mask
        mask = (array >= fill)
        array = array[mask]
        images = images[mask]
        rows = rows[mask]
        polygons = polygons[mask]

        # set title
        title = '{}, {}'.format(name.replace('_', ' '), tag)

        # make a plot
        unit = feature.attributes.get('units', '_')
        address = 'plots/diagram/{}_{}.h5'.format(name.replace(' ', '_'), tag)
        texts = [name, 'image', 'row']
        data = [array, images, rows]
        self.squid.pulsate(texts, data, polygons, address, bounds, title, unit)

        return None

    def differentiate(self, waves=(310,), resolution=10, degrees=(-72, 58)):
        """Map changes from flipping inputs.

        Arguments:
            waves: wavelengths of choice
            resolution: number of points to meld
            degrees: float, latitude degree bracket

        Returns:
            None
        """

        # make plot directory
        sink = 'plots/differentiation'
        self._make('{}/{}'.format(self.sink, sink))

        # set bounding brackets
        brackets = {}
        brackets[310] = [0, 50, 100, 150, 200, 250, 300, 350, 400]
        brackets[390] = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600]
        brackets['transmittance'] = [0.6, 0.7, 0.8, 0.9, 1.0, 1.1]
        #brackets['percent'] = [-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10]
        brackets['percent'] = [-1.0, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1.0]
        brackets['percentii'] = [-2.0, -1.6, -1.2, -0.8, -0.4, 0, 0.4, 0.8, 1.2, 1.6, 2.0]
        brackets['percentiii'] = [-20, -16, -12, -8, -4, 0, 4, 8, 12, 16, 20]
        #brackets['percent'] = [-0.1, -0.08, -0.06, -0.04, -0.02, 0, 0.02, 0.04, 0.06, 0.08, 0.10]
        brackets['chlorophyll'] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        brackets['ozone'] = [200, 225, 250, 275, 300, 325, 350, 375, 400]
        brackets['difference'] = [-0.1, -0.08, -0.06, -0.04, -0.02, 0, 0.02, 0.04, 0.06, 0.08, 0.10]
        brackets['pressure'] = [0.95, 0.96, 0.97, 0.98, 0.99, 1.0, 1.01, 1.02, 1.03, 1.04, 1.05]
        brackets['process'] = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5]
        brackets['cloud'] = [0, 20, 40, 60, 80, 100]
        brackets['reflectivity'] = [0, 20, 40, 60, 80, 100, 120]
        brackets['residue'] = [-6, -4, -2, 0, 2, 4, 6]

        # make reserovir to hold datasets
        reservoir = {wave: {} for wave in waves}

        # get the latitude tagged entry
        ocean = self._pull('noon')

        # grab the latitude and longitude and bounds
        latitude = ocean.grab('latitude')
        nadir = latitude[:, 30]
        mask = (nadir > -9999)

        # make mask for ascending indices
        minimum = self._pin(float(nadir[mask].min()), latitude)[0][0]
        maximum = self._pin(float(nadir[mask].max()), latitude)[0][0]

        # subset minimum to maximum
        latitude = latitude[minimum: maximum]
        longitude = ocean.grab('longitude')[minimum: maximum]
        bounds = ocean.grab('latitude_bounds')[minimum: maximum]
        boundsii = ocean.grab('longitude_bounds')[minimum: maximum]

        # create polygons from bounds
        polygons = self._polymerize(latitude, longitude, resolution)

        # get indices based on resolution
        selection = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])

        # apply selection and mask to latitude and longitude
        latitude = latitude[selection]
        longitude = longitude[selection]

        # mask the latitude at degrees
        mask = (latitude > degrees[0]) & (latitude < degrees[1])

        # apply mask to polygons and coordinates
        polygons = polygons[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]

        # set tags and reservoirs
        tags = ['noon', 'flags']

        # for each wave
        for wave in waves:

            # set up reservoir
            reservoir[wave] = {tag: {} for tag in tags}

            # tags
            for tag in tags:

                # get the data
                ocean = self._pull(tag)

                # get the wavelength
                wavelength = ocean.grab('wavelength')
                index = self._pin(wave, wavelength)[0][0]

                # grab the fluxes at the wavelength
                planar = ocean.grab('ultraviolet/surface/planar')[minimum: maximum, :, index]
                surface = ocean.grab('ultraviolet/surface/scalar')[minimum: maximum, :, index]
                # submarine = ocean.grab('ultraviolet/underwater/scalar')[minimum: maximum, :, index]
                sky = ocean.grab('ultraviolet/clear_sky')[minimum: maximum, :, index]
                attenuation = ocean.grab('planar_Kd')[minimum: maximum, :, index]
                attenuationii = ocean.grab('scalar_Ko')[minimum: maximum, :, index]
                chlorophyll = ocean.grab('chlorophyll')[minimum: maximum, :]
                transmittance = ocean.grab('transmittance/planar')[minimum: maximum, :, index]
                transmittanceii = ocean.grab('transmittance/scalar')[minimum: maximum, :, index]
                water = ocean.grab('water')[minimum: maximum, :]
                ozone = ocean.grab('ozone')[minimum: maximum, :]
                satellite = ocean.grab('satellite_zenith_angle')[minimum: maximum, :]
                pressure = ocean.grab('pressure', -1)[minimum: maximum, :]
                cloud = ocean.grab('cloud')[minimum: maximum, :]
                zenith = ocean.grab('solar_zenith_angle')[minimum: maximum, :]
                noon = ocean.grab('minimum_solar_zenith_angle')[minimum: maximum, :]
                azimuth = ocean.grab('relative_azimuth_angle')[minimum: maximum, :]
                residue = ocean.grab('residue_331')[minimum: maximum, :]
                latitudeii = ocean.grab('latitude')[minimum: maximum, :]
                longitudeii = ocean.grab('longitude')[minimum: maximum, :]
                value = ocean.grab('n_value')[minimum: maximum, :, 0]
                valueii = ocean.grab('n_value')[minimum: maximum, :, 2]
                valueii = ocean.grab('n_value')[minimum: maximum, :, 2]
                reflectivity = ocean.grab('ler_values')[minimum: maximum, :, 0]

                # calculate five meter scalar depths
                # five = submarine * transmittance * numpy.exp(-5 * attenuationii)
                # fiveii = surface * transmittanceii * numpy.exp(-5 * attenuationii)

                # calculate percent difference
                # percent = 100 * ((fiveii / five) - 1)

                # grab the angle at solar noon
                noon = ocean.grab('minimum_solar_zenith_angle')[minimum: maximum, :]
                zenith = ocean.grab('solar_zenith_angle')[minimum: maximum, :]

                # try to
                try:

                    # get processing flag
                    process = ocean.grab('process')[minimum: maximum, :]

                # unless not in file
                except IndexError:

                    # set dummy processing flag
                    process = numpy.zeros(zenith.shape)

                # remove water fraction
                # five = numpy.where(water < 100, -9999, five)
                # fiveii = numpy.where(water < 100, -9999, fiveii)
                #chlorophyll = numpy.where(water < 100, -9999, chlorophyll)
                # percent = numpy.where(water < 100, -9999, percent)

                # set names and arrays
                arrays = [chlorophyll, ozone, satellite, sky, planar, surface, pressure, value, valueii]
                arrays += [cloud, transmittance, zenith, noon, azimuth, residue, latitudeii, longitudeii]
                arrays += [water, process, reflectivity]
                names = ['chlorophyll', 'ozone', 'satellite', 'sky', 'planar', 'surface', 'pressure', 'value331']
                names += ['value360', 'cloud', 'transmittance', 'zenith', 'noon', 'azimuth', 'residue331', 'latitude']
                names += ['longitude', 'water', 'process', 'reflectivity360']

                # apply selection and masks
                arrays = [array[selection][mask] for array in arrays]

                # add to reservoir
                reservoir[tag] = {name: array for name, array in zip(names, arrays)}
                reservoir[tag].update({'latitude': latitude, 'longitude': longitude})
                reservoir[tag].update({'polygons': polygons})

            # set namees and boxes
            names = ['chlorophyll', 'ozone', 'satellite', 'sky', 'planar', 'surface', 'pressure', 'value331']
            names += ['value360', 'cloud', 'transmittance', 'zenith', 'noon', 'azimuth', 'residue331', 'latitude']
            names += ['longitude', 'reflectivity360']

            # eliminate land
            wet = reservoir['flags']['water'] == 0

            # make titles
            titles = {}
            titles['planar'] = 'Percent difference Ed_surface, {}nm, OMUFLPSLV vs OMTO3 surface pressure'.format(wave)
            titles['sky'] = 'Percent difference Ed_clearsky, {}nm,  OMUFLPSLV vs OMTO3 surface pressure'.format(wave)
            titles['chlorophyll'] = 'Percent difference chlorophyll, 70004/OMGLERPRE vs 10003/OMGLER'
            titles['azimuth'] = 'Percent difference Relative Azimuth Angle, 70004/L1B vs 92005/OMTO3'

            # set boxes
            boxes = {'planar': 'percentiii', 'sky': 'percentii', 'surface': 'percentii'}

            # for each set
            for name in names:

                # set title
                title = 'Percent difference {}, new inputs vs old'.format(name)
                title = titles.get(name, title)

                # make a plot for logarithmic vs linear interpolation
                box = boxes.get(name, 'percent')
                unit = ' % '
                address = '{}/{}_{}.h5'.format(sink, name, wave)
                array = 100 * ((reservoir['flags'][name] / reservoir['noon'][name]) - 1)
                bracket = brackets[box]
                texts = [name, 'latitude', 'longitude']
                data = [array[wet], latitude[wet], longitude[wet]]
                self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

            # examine residue and cloud differences
            for name in ['residue331', 'cloud', 'value331', 'value360', 'pressure', 'reflectivity360']:

                # make a plot for logarithmic vs linear interpolation
                title = 'Difference {}, new inputs - old'.format(name)
                box = 'difference'
                unit = 'difference'
                address = '{}/{}_difference_{}.h5'.format(sink, name, wave)
                array = reservoir['flags'][name] - reservoir['noon'][name]
                bracket = brackets[box]
                texts = [name, 'latitude', 'longitude']
                data = [array, latitude, longitude]
                self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

            # map absolute pressures and other features
            for tag in ('noon', 'flags'):

                # set esdt namees
                earths = {'flags': 'OMUFPSLV', 'noon': 'OMTO3'}

                # plot pressure
                title = 'Surface Pressure, {}, Orbit 2573, 2005-01-07'.format(earths[tag])
                unit = 'atm'
                address = '{}/surface_pressure_{}_{}.h5'.format(sink, tag, wave)
                array = reservoir[tag]['pressure']
                bracket = brackets['pressure']
                texts = [name, 'latitude', 'longitude']
                data = [array[wet], latitude[wet], longitude[wet]]
                self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

                # plot residue
                title = 'Residue 331, {}, Orbit 2573, 2005-01-07'.format(earths[tag])
                unit = '_'
                address = '{}/residue_331_{}_{}.h5'.format(sink, tag, wave)
                array = reservoir[tag]['residue331']
                bracket = brackets['residue']
                texts = [name, 'latitude', 'longitude']
                data = [array[wet], latitude[wet], longitude[wet]]
                self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

                # plot cloud tau
                title = 'Cloud Tau, {}, Orbit 2573, 2005-01-07'.format(earths[tag])
                unit = '_'
                address = '{}/cloud_taue_{}_{}.h5'.format(sink, tag, wave)
                array = reservoir[tag]['cloud']
                bracket = brackets['cloud']
                texts = [name, 'latitude', 'longitude']
                data = [array[wet], latitude[wet], longitude[wet]]
                self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

                # plot reflectivity
                title = 'Reflectivity 360, {}, Orbit 2573, 2005-01-07'.format(earths[tag])
                unit = 'atm'
                address = '{}/reflectivity_360_{}_{}.h5'.format(sink, tag, wave)
                array = reservoir[tag]['reflectivity360']
                bracket = brackets['reflectivity']
                texts = [name, 'latitude', 'longitude']
                data = [array[wet], latitude[wet], longitude[wet]]
                self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

            # map processing flags
            title = 'Proposed Processing Flags, Orbit 2573, 2005-01-07'
            unit = '_'
            address = '{}/processing_flags_{}_{}.h5'.format(sink, tag, wave)
            array = reservoir['flags']['process']
            bracket = brackets['process']
            texts = ['processing_flag', 'latitude', 'longitude']
            data = [array[wet], latitude[wet], longitude[wet]]
            self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

            # map processing flags
            title = 'Ed_clearsky, Clear Sky Irradiance, {}nm, Orbit 2573, 2005-01-07'.format(wave)
            unit = 'mW/m2nm'
            address = '{}/Ed_clearsky_{}_{}.h5'.format(sink, tag, wave)
            array = reservoir['flags']['sky']
            bracket = brackets[wave]
            texts = ['sky', 'latitude', 'longitude']
            data = [array[wet], latitude[wet], longitude[wet]]
            self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

            # map processing flags
            title = 'Ed_surface, Surface Ultraviolet Irradiance, {}nm, Orbit 2573, 2005-01-07'.format(wave)
            unit = 'mW/m2nm'
            address = '{}/Ed_planar_{}_{}.h5'.format(sink, tag, wave)
            array = reservoir['flags']['planar']
            bracket = brackets[wave]
            texts = ['surface', 'latitude', 'longitude']
            data = [array[wet], latitude[wet], longitude[wet]]
            self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)
            # # map pressure difference
            # for tag in ('flags', 'noon'):
            #
            #     # eliminate land
            #     wet = reservoir['flags']['water'] == 0
            #
            #     # plot pressure
            #     title = 'Surface Pressure, {}, Orbit 2573, 2005-01-07'.format(tag)
            #     unit = 'atm'
            #     address = '{}/surface_pressure_{}_{}.h5'.format(sink, tag, wave)
            #     array = reservoir[tag]['pressure']
            #     bracket = brackets['pressure']
            #     texts = [name, 'latitude', 'longitude']
            #     data = [array[wet], latitude[wet], longitude[wet]]
            #     self.squid.pulsate(texts, data, polygons[wet], address, bracket, title, unit)

        return None

    def disperse(self, month, orbits=1, waves=None, source=None, sink=None, initial=0, degrees=62, resolution=20):
        """Plot an orbit of penetration depth and uv data.

        Arguments:
            month: int, the month of the study
            orbits: int, number of orbits to use
            waves: tuple of floats, the wavelengths to check
            source: str, path to source of data
            sink: str, path to sink for plots
            initial: initial orbit in list of daily orbits
            degrees: float, latitude degrees for plotting limit
            resolution: int, number of trackwise pixels to coagulate

        Returns:
            None
        """

        # set defaults
        source = source or '../studies/diatom/cardinals'
        sink = sink or 'plots/dispersion'
        sinkii = 'plots/polygon'
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # set default parameters
        #waves = waves or (305, 310, 380)
        waves = waves or (305, 310)
        #depths = {2: 'two', 5: 'five', 20: 'twenty'}
        depths = {5: 'five'}

        # set month names
        months = {3: 'March', 6: 'June', 9: 'September', 12: 'December'}

        # create hydra
        hydra = Hydra(source)
        paths = [path for path in hydra.paths if '2005m{}'.format(self._pad(month)) in path]

        # set date
        dates = {3: '2005-03-21', 6: '2005-06-21', 9: '2005-09-21', 12: '2005-12-21'}
        date = dates[month]

        # set brackets for plotting heatmaps
        scales = ['depth', 'planar', 'scalar', 'five', 'twenty', 'two', 'constant', 'reflectivity', 'ozone']
        scales += ['chlorophyll']
        brackets = {scale: {} for scale in scales}
        brackets['depth'][305] = [0, 5, 10, 15, 20, 25, 30, 35, 40]
        brackets['depth'][310] = [0, 5, 10, 15, 20, 25, 30, 35, 40]
        brackets['depth'][380] = [0, 10, 20, 30, 40, 50, 60, 70, 80]
        brackets['planar'][305] = [0, 20, 40, 60, 80, 100, 120, 140, 160]
        brackets['planar'][310] = [0, 25, 50, 75, 100, 125, 150, 175, 200]
        brackets['planar'][380] = [0, 150, 300, 450, 600, 750, 900, 1050, 1200]
        brackets['scalar'][305] = [0, 40, 80, 120, 160, 200, 240, 280, 320]
        brackets['scalar'][310] = [0, 60, 120, 180, 240, 300, 360, 420, 480]
        brackets['scalar'][380] = [0, 250, 500, 750, 1000, 1250, 1500, 1750, 2000]
        brackets['two'][305] = [0, 20, 40, 60, 80, 100, 120, 140, 160]
        brackets['two'][310] = [0, 20, 40, 60, 80, 100, 120, 140, 160]
        brackets['two'][380] = [0, 100, 200, 300, 400, 500, 600, 700, 800]
        brackets['five'][305] = [0, 20, 40, 60, 80, 100, 120, 140, 160]
        brackets['five'][310] = [0, 25, 50, 75, 100, 125, 150, 175, 200]
        brackets['five'][380] = [0, 100, 200, 300, 400, 500, 600, 700, 800]
        brackets['twenty'][305] = [0, 10, 20, 30, 40, 50, 60, 70, 80]
        brackets['twenty'][310] = [0, 10, 20, 30, 40, 50, 60, 70, 80]
        brackets['twenty'][380] = [0, 20, 40, 60, 80, 100, 120, 140, 160]
        brackets['constant'][305] = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
        brackets['constant'][310] = [-1.75, -1.5, -1.25, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75]
        brackets['constant'][380] = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
        brackets['reflectivity'][305] = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
        brackets['reflectivity'][310] = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
        brackets['reflectivity'][380] = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
        brackets['ozone'][305] = [225, 250, 275, 300, 325, 350, 375, 400, 425]
        brackets['ozone'][310] = [200, 225, 250, 275, 300, 325, 350, 375, 400]
        brackets['ozone'][380] = [225, 250, 275, 300, 325, 350, 375, 400, 425]
        brackets['chlorophyll'][305] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        brackets['chlorophyll'][310] = [-2, -1.5, -1, -0.5, 0, 0.5, 1.0, 1.5, 2.0]
        brackets['chlorophyll'][380] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        # set fields of interest
        fields = ['latitude', 'longitude', 'flux_planar', 'flux_scalar', 'constant_planar']
        fields += ['constant_scalar', 'water_fraction', 'snow_ice_fraction', 'reflectivity_380']
        fields += ['ozone', 'chlorophyll', 'latitude_bounds', 'longitude_bounds']

        # begin reservoirs
        reservoir = {field: [[] for _ in waves] for field in fields + ['flux_planar_depth']}

        # for each path, for as many orbits as specified
        for path in paths[initial:initial + orbits]:

            # print path
            print('ingesting {}...'.format(path))

            # ingest the path
            hydra.ingest(path)

            # grab fields from hdf file
            data = {field: hydra.grab(field).squeeze() for field in fields}

            # grab wavelengths
            wavelength = hydra.grab('wavelength').squeeze()

            # get indices of minimum and maximum latitude to rule out descending paths
            latitude = data['latitude']
            valid = latitude[latitude > -9999]
            start = self._pin(float(valid.min()), latitude)[0][0]
            finish = self._pin(float(valid.max()), latitude)[0][0]

            # take start and finish indices of dataset based on ascending maode
            data = {field: array[start:finish] for field, array in data.items()}

            # # get corners from latitude and longitude
            # corners = self._polymerize(data['latitude'], data['longitude'])
            # original = corners

            # get corners from latitude and longitude bounds
            corners = self._orientate(data['latitude_bounds'], data['longitude_bounds'])

            # set compressions for latitude and longitude bounds
            trackwise = resolution
            rowwise = 1

            # take only even numbered scan indices for easier plotting
            latitude = data['latitude']
            evens = numpy.array([index for index in range(latitude.shape[0]) if index % trackwise == 0])
            evensii = numpy.array([index for index in range(latitude.shape[1]) if index % rowwise == 0])

            # for all fields except bounds
            for field, array in data.items():

                # check for bounds
                if 'bounds' not in field:

                    # take select indices
                    data[field] = array[evens]
                    data[field] = data[field][:, evensii]

            # compress corners
            corners = self._compress(corners, trackwise, rowwise)

            # construct bounds
            compasses = ('southwest', 'southeast', 'northeast', 'northwest')
            arrays = [corners[compass][:, :, 0] for compass in compasses]
            data['latitude_bounds'] = numpy.stack(arrays, axis=2)
            arrays = [corners[compass][:, :, 1] for compass in compasses]
            data['longitude_bounds'] = numpy.stack(arrays, axis=2)

            # adjust polygon  boundaries to avoid crossing dateline
            #longitude = data['longitude']
            data['longitude_bounds'] = self._cross(data['longitude_bounds'])

            # duplicate flux_planar for depth
            data['flux_planar_depth'] = data['flux_planar']

            # mark as wet anything with water fraction < 1, and no snow /ice, otherwise default to 0
            wet = (data['water_fraction'] >= 100) & (data['snow_ice_fraction'] <= 0)

            # for each wavelength
            for index, wave in enumerate(waves):

                # get the wavlength index
                position = self._pin(wave, wavelength)[0][0]

                # for each field
                for field, array in data.items():

                    # check for three - d array
                    if len(array.shape) == 3:

                        # if not latitude or longitude bounds
                        if field in ('latitude_bounds', 'longitude_bounds'):

                            # zero out all but wet entries, and append
                            #panel = numpy.where(wet, data[field][:, :, position], 0)
                            panel = data[field]
                            reservoir[field][index].append(panel)

                        # otherwise, assume spectral
                        else:

                            # only wet for depth
                            if field in ('flux_planar',):

                                # zero out all but wet entries, and append
                                #panel = numpy.where(wet, data[field][:, :, position], 0)
                                panel = data[field][:, :, position]
                                reservoir[field][index].append(panel)

                            # otherwise
                            else:

                                # zero out all but wet entries, and append
                                panel = numpy.where(wet, data[field][:, :, position], 0)
                                #panel = data[field][:, :, position]
                                reservoir[field][index].append(panel)

                    # otherwise
                    else:

                        # if not latitude or longitude
                        if field not in ('latitude', 'longitude', 'ozone'):

                            # zero out all but wet entries, and append
                            panel = numpy.where(wet, data[field], 0)
                            reservoir[field][index].append(panel)

                        # otherwise
                        else:

                            # add the data
                            panel = data[field]
                            reservoir[field][index].append(panel)

        # stack all orbits into arrays
        for field, arrays in reservoir.items():

            # stack all entries
            stack = [numpy.vstack(arrays[index]) for index, _ in enumerate(waves)]
            reservoir[field] = stack

        # for each wave length
        for index, wave in enumerate(waves):

            # construct the coordiantes
            latitude = reservoir['latitude'][index]
            longitude = reservoir['longitude'][index]
            bounds = reservoir['latitude_bounds'][index]
            boundsii = reservoir['longitude_bounds'][index]

            # get latitude mask ( +/- 60)
            #mask = (abs(latitude) < degrees) & (bounds.sum(axis=2) > -200) & (boundsii.sum(axis=2) > -200)
            mask = (abs(latitude) < degrees)
            ordinate = latitude[mask]
            abscissa = longitude[mask]
            polygons = numpy.hstack([bounds[mask], boundsii[mask]])

            # heat map the uv, planar
            tracer = reservoir['flux_planar'][index][mask]
            folder = '{}/downwelling_irradiance_surface_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = '{}, {}'.format(months[month], date)
            bracket = brackets['planar'][wave]
            name = 'downwelling_irradiance'.format(wave)
            units = '  UV Irradiance {}nm [ mW/m^2/nm ]'.format(wave)
            parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title, units)

            # include polygons
            folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
            texts = [name, 'latitude', 'longitude']
            arrays = [tracer, ordinate, abscissa]
            self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

            # go through many dpeths
            for depth, tag in depths.items():

                # heat map the uv, planar, at 5 m depth
                exponent = numpy.exp(-depth * reservoir['constant_planar'][index][mask])
                tracer = reservoir['flux_planar_depth'][index][mask] * exponent
                folder = '{}/downwelling_uv_{}_{}_{}.h5'.format(sink, depth, wave, self._pad(month))
                title = '{}, {}'.format(months[month], date)
                bracket = brackets[tag][wave]
                name = 'downwelling_irradiance'.format(wave, depth)
                units = '  {}m UV Irradiance {}nm [ mW/m^2/nm ]'.format(depth, wave)
                parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
                self.squid.shimmer(*parameters, folder, bracket, title, units)

                # include polygons
                folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

            # heat map the uv, planar
            tracer = reservoir['flux_scalar'][index][mask]
            folder = '{}/scalar_uv_fLux_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = 'Ultraviolet Scalar Flux, {} nm, {}'.format(wave, date)
            bracket = brackets['scalar'][wave]
            parameters = ('scalar_uv_{}'.format(wave), tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title)

            # heat map penetration depth, correcting for nans
            tracer = -numpy.log(0.1) / reservoir['constant_planar'][index][mask]
            tracer = numpy.where(numpy.isfinite(tracer), tracer, 0)
            folder = '{}/penetration_depth_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = 'Penetration Depth, {}nm, {}, {}'.format(wave, months[month], date)
            bracket = brackets['depth'][wave]
            name = 'penetration_depth_{}'.format(wave)
            units = '     Penetration Depth [ m ]'
            parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title, units)

            # include polygons
            folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
            texts = [name, 'latitude', 'longitude']
            arrays = [tracer, ordinate, abscissa]
            self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

            # heat map attenuation constant Kd
            tracer = reservoir['constant_planar'][index][mask]
            folder = '{}/attenuation_constant_kd_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = 'Diffuse Attenuation Coefficient Kd, {}nm, {}, {}'.format(wave, months[month], date)
            units = '   Diffuse Attenuation Coefficient Kd [ 1/m ]'.format(wave)
            bracket = brackets['constant'][wave]
            name = 'attenuation_constant_kd_{}'.format(wave)
            parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title, units)

            # include polygons
            folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
            texts = [name, 'latitude', 'longitude']
            arrays = [tracer, ordinate, abscissa]
            self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

            # make reflectivity map
            tracer = reservoir['reflectivity_380'][index][mask]
            folder = '{}/reflectivity_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = 'Reflectivity, 380nnm, {}'.format(date)
            bracket = brackets['reflectivity'][wave]
            parameters = ('reflectivity_{}'.format(wave), tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title)

            # heat map ozone
            tracer = reservoir['ozone'][index][mask]
            folder = '{}/ozone_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = '{}, {}'.format(months[month], date)
            units = '   Total Ozone [ DU ]'.format(wave)
            bracket = brackets['ozone'][wave]
            name = 'ozone_{}'.format(wave)
            parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title, units)

            # include polygons
            folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
            texts = [name, 'latitude', 'longitude']
            arrays = [tracer, ordinate, abscissa]
            self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

            # heat map chlorophyll
            tracer = numpy.log10(reservoir['chlorophyll'][index][mask])
            folder = '{}/chlorophyll_{}_{}.h5'.format(sink, wave, self._pad(month))
            title = 'Chlorophyll Concentration, {}, {}'.format(months[month], date)
            units = '   Chlorophyll concentration [ log10 ( mg / L ) ]'.format(wave)
            bracket = brackets['chlorophyll'][wave]
            name = 'chlorophyll_{}'.format(wave)
            parameters = (name, tracer, 'latitude', ordinate, 'longitude', abscissa)
            self.squid.shimmer(*parameters, folder, bracket, title, units)

            # include polygons
            folder = folder.replace('.h5', '_poly.h5').replace(sink, sinkii)
            texts = [name, 'latitude', 'longitude']
            arrays = [tracer, ordinate, abscissa]
            self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        return reservoir

    def dissolve(self, tag='sky', bounds=(-70, 40)):
        """Produce maps of clear sky ultravioliot ascii, omocned differences.

        Arugments:
            tag: str, name of omocned data

        Returns:
            None
        """

        # get clear sky input file
        source = '../studies/diatom/sky/OMI.OUTPUT.CLEAR.MOBY.INPUT.5.2005-01-07.02573'
        text = self._know(source)

        # grab headers
        headers = text[0].split()
        headers = [header.strip(',') for header in headers]

        # create floats from the rest
        rows = [line.split() for line in text[1:]]
        rows = [[float(entry) for entry in line] for line in rows]
        matrix = numpy.array(rows).transpose(1, 0)

        # create data from all but last 110 columns
        data = {name: numpy.array([column]).transpose(1, 0) for name, column in zip(headers, matrix[:-110])}

        # add uv data
        data['uv'] = matrix[-110:].transpose(1, 0)
        data['latitude'] = data['lat']
        data['longitude'] = data['lon']

        # make clear sky lattice
        self.lattice(tag, synthesis=data)

        # grab grid
        grid = self._pull(tag, grid=True)

        # compare uvdata
        latitude = grid.grab('latitude')
        longitude = grid.grab('longitude')
        mask = (latitude >= bounds[0]) & (latitude <= bounds[1])

        # set indices and wavelengths for uv
        waves = ['290', '320', '390']
        indices = ['0', '30', '100']

        # for each one
        for wave, index in zip(waves, indices):

            # get ultraviolet
            ultraviolet = grid.grab('ascii_uv_{}'.format(index))
            ultravioletii = grid.grab('omocned_clear_sky_ultraviolet_spectrum_{}'.format(index))

            # graph ascii clear sky
            title = 'Ascii Clear Sky UV, {}nm'.format(wave)
            address = 'plots/comparisons/Clear_Sky_Ascii_{}.h5'.format(wave)
            brackets = [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1]
            parameters = ['ultraviolet_clear', ultraviolet[mask], 'latitude', latitude[mask]]
            parameters += ['longitude', longitude[mask], address, brackets, title]
            self.squid.shimmer(*parameters)

            # graph omocned clear sky
            title = 'OMOCNUV Clear Sky UV, {}nm'.format(wave)
            address = 'plots/comparisons/Clear_Sky_OMOCNUV_{}.h5'.format(wave)
            brackets = [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1]
            parameters = ['ultraviolet_clear', ultravioletii[mask], 'latitude', latitude[mask]]
            parameters += ['longitude', longitude[mask], address, brackets, title]
            self.squid.shimmer(*parameters)

            # graph error
            title = 'Percent Error, OMOCNUV vs Ascii Clear Sky UV, {}nm'.format(wave)
            address = 'plots/comparisons/Clear_Sky_Error_{}.h5'.format(wave)
            brackets = [-10, -1, -0.1, -0.01, 0.01, 0.1, 1, 10]
            error = 100 * ((ultravioletii / ultraviolet) - 1)
            parameters = ['ultraviolet_clear_error', error[mask], 'latitude', latitude[mask]]
            parameters += ['longitude', longitude[mask], address, brackets, title]
            self.squid.shimmer(*parameters)

        return None

    def distribute(self, year=2005):
        """Check the distribution of quality flags.

        Arguments:
            year: int, year for check

        Returns:
            None
        """

        # create hydra
        hydra = Hydra('../studies/diatom/pace/quality')

        # get only paths with year
        paths = [path for path in hydra.paths if '{}m'.format(year) in path]

        # make reservoirs for bit flags and geo coordiates
        bits = []
        latitudes = []
        longitudes = []

        # for each path
        for path in paths:

            # ingest
            hydra.ingest(path)

            # get the bit flag
            bit = hydra.grab('bit_flag')
            bits.append(bit)

            # latitude and longitudes
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')
            latitudes.append(latitude)
            longitudes.append(longitude)

        # make into arrays
        bits = numpy.vstack(bits).flatten()
        latitudes = numpy.vstack(latitudes).flatten()
        longitudes = numpy.vstack(longitudes).flatten()

        # set total size
        size = bits.shape[0]

        # set accumulations
        percents = 0
        masque = numpy.zeros(bits.shape).astype(bool)

        # set data
        data = {}

        # each power of 2
        powers = [2 ** power for power in range(13)]
        for index, power in enumerate(powers):

            # make bit mask
            mask = (bits.astype('int') & power == power)

            # print
            percent = 100 * (mask.sum() / size)
            formats = (index, power, mask.sum(), percent)
            self._print('bit {}, {}: {}, {} %'.format(*formats))

            # add to percents
            percents += percent

            # add to masks
            masque = masque | mask

            # add to data
            data['bit_{}'.format(index)] = mask
            data['latitude'] = latitudes
            data['longitude'] = longitudes

        # print totals
        inverse = numpy.logical_not(masque)
        data['unflagged'] = inverse
        self._print('')
        self._print('total: {} %'.format(percents))
        self._print('unflagged: {}, {} %'.format(inverse.sum(), 100 * inverse.sum() / size))

        # make file
        self._make('{}/masks'.format(self.sink))
        destination = '{}/masks/Masks_{}.h5'.format(self.sink, year)

        # stash data
        self.spawn(destination, data)

        return None

    def dose(self, point=(20, -120), tag='march'):
        """Create poster line plots for dnd damage dose rate.

        Arguments:
            point: tuple of floats, the lat and lonitude coordinates
            tag: str, tag of dataset

        Returns:
            None
        """

        # set fill
        fill = -999

        # make folder based on tag
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/dose'.format(self.sink))

        # get the DNA Spectrum
        spectra = Hydra('../studies/diatom/simulation/spectra')
        spectra.ingest(1)
        spectrum = spectra.grab('spectrum')
        spectrum = spectrum.reshape(1, 1, -1)

        # set dates
        dates = {'march': '2005-03-21', 'june': '2005-06-21'}
        dates.update({'september': '2005-09-21', 'december': '2005-12-21'})
        dates.update({'colorbars': '2005-03-21'})

        # set tags
        tags = ('march',)
        points = (point, )

        # set up transforms to convert from logs and replace fill values
        # def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        # transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        # transforms.update({'coefficient': powering})

        # for each point, tag pair
        for tag, coordinates in zip(tags, points):

            # create hydra
            hydra = Hydra('{}/{}'.format(self.sink, tag))

            # for each path
            records = []
            for path in hydra.paths:

                # ingest path
                self._print('ingesting {}...'.format(path))
                hydra.ingest(path)

                # grab data
                # irradiance = powering(hydra.grab('underwater_irradiance_planar'))
                # coefficient = powering(hydra.grab('attenuation_coefficient_planar'))
                irradiance = hydra.grab('underwater_irradiance_planar')
                coefficient = hydra.grab('attenuation_coefficient_planar')
                latitude = hydra.grab('latitude')
                longitude = hydra.grab('longitude')
                wavelength = hydra.grab('spectral_wavelength')

                # find pixel closest to point
                pixel = hydra._pin(coordinates, [latitude, longitude])[0]
                distance = (latitude[pixel] - coordinates[0]) ** 2 + (longitude[pixel] - coordinates[1]) ** 2

                # compute five meter depth radiance
                radiation = (irradiance * numpy.exp(-5 * coefficient))[pixel]

                # compute dose rate by weighting with da spectrum
                dose = radiation * spectrum

                # create entry
                record = [path, distance, pixel, latitude[pixel], longitude[pixel], radiation, dose, wavelength]
                records.append(record)

                print(record)

            # sort records by shortest distance
            records.sort(key=lambda record: record[1])
            closest = records[0]
            self._print(closest)

            # get orientations
            orientations = [self._orient(point[0]), self._orient(point[1], east=True)]

            # plot radiation
            formats = (dates[tag], self._orient(coordinates[0]), self._orient(coordinates[1], east=True))
            title = 'Irradiance at 5m depth, {}, {}, {}'.format(*formats)
            address = 'plots/dose/irradiance_{}_{}_{}.h5'.format(dates[tag].replace('-', ''), *orientations)
            self.squid.ink('irradiance', closest[-3], 'wavelength', closest[-1], address, title)

            # plot dose
            formats = (dates[tag], self._orient(coordinates[0]), self._orient(coordinates[1], east=True))
            title = 'DNA damage dose rate at 5m depth, {}, {}, {}'.format(*formats)
            address = 'plots/dose/dose_{}_{}_{}.h5'.format(dates[tag].replace('-', ''), *orientations)
            self.squid.ink('dose', closest[-2], 'wavelength', closest[-1], address, title)

            # plot dna damage spectrum
            formats = (dates[tag], self._orient(coordinates[0]), self._orient(coordinates[1], east=True))
            title = 'DNA damage spectrum'
            address = 'plots/dose/spectrum_{}_{}_{}.h5'.format(dates[tag].replace('-', ''), *orientations)
            self.squid.ink('spectrum', spectrum, 'wavelength', closest[-1], address, title)

        return None

    def drift(self, tag='coefficient', folder='coefficient', scan=994):
        """Compare nvalue differences to closeness to wavelength fwhm boundaries

        Arguments:
            tag: str, tag for data
            folder: str, folder for originals
            scan: int, track number

        Returns:
            None
        """

        # get the duel swath file
        duel = self._pull(tag, duel=True)

        # make a hydra for the statics folder
        hydra = Hydra('../studies/diatom/frames/{}'.format(folder))

        # set exact wavelengths
        exacts = {'n331': 331.06, 'n340': 339.66, 'n360': 359.88, 'n380': 379.95}
        bands = {'n331': 'BAND2', 'n340': 'BAND2', 'n360': 'BAND3', 'n380': 'BAND3'}
        products = {'BAND2': 'OML1BRUG', 'BAND3': 'OML1BRVG'}
        pixels = {'BAND2': 557, 'BAND3': 751}

        # for each wavelength
        for wave, exact in exacts.items():

            # get n360 contents
            value = duel.grab('ascii_{}'.format(wave))
            valueii = duel.grab('omocned_{}'.format(wave))
            difference = valueii - value

            # set band and product
            band = bands[wave]
            product = products[band]

            # get radiance for band 3 and wavelength registration
            paths = [path for path in hydra.paths if product in path]
            hydra.ingest(paths[0])
            radiance = hydra.grab('{}/radiance'.format(band)).squeeze()
            wavelength = hydra.grab('{}/wavelength'.format(band)).squeeze()

            # get irradiance for band 3 and wavelength registration
            paths = [path for path in hydra.paths if 'OML1BIRR' in path]
            hydra.ingest(paths[0])
            irradiance = hydra.grab('{}/irradiance'.format(band)).squeeze()
            wavelengthii = hydra.grab('{}/wavelength'.format(band)).squeeze()

            # plot nvalue differences vs closeness to a wavelength edge
            differences = []
            gaps = []
            tops = []
            bottoms = []
            target = exacts[wave]
            for row in range(60):

                # get difference
                differences.append(abs(difference[scan, row]))

                # for each pixel
                top = []
                bottom = []
                for pixel in range(pixels[band]):

                    # add absolute distances from edges for radiance
                    gap = abs(wavelength[row, pixel] - (target - 1))
                    top.append(float(gap))
                    gap = abs(wavelength[row, pixel] - (target + 1))
                    top.append(float(gap))

                    # add absolute distances from edges for irradiance
                    gap = abs(wavelengthii[row, pixel] - (target - 1))
                    bottom.append(float(gap))
                    gap = abs(wavelengthii[row, pixel] - (target + 1))
                    bottom.append(float(gap))

                # add the minimum gap to edges
                tops.append(min(top))
                bottoms.append(min(bottom))
                gaps.append(min(top + bottom))

            # plot abs edge
            address = 'plots/edges/{}_differences_wavelength_window_edges.h5'.format(wave)
            title = '{} difference vs closeness to wavelength edge'.format(wave)
            self.squid.ink('{}_difference'.format(wave), differences, 'wavelength_edge', gaps, address, title)

            # plot radiancee edge
            address = 'plots/edges/{}_differences_wavelength_window_edges_radiance.h5'.format(wave)
            title = '{} difference vs closeness to wavelength edge, radiance'.format(wave)
            self.squid.ink('{}_difference_rad'.format(wave), differences, 'radiance_edge', tops, address, title)

            # plot irradiance edge
            address = 'plots/edges/{}_differences_wavelength_window_edges_irradiance.h5'.format(wave)
            title = '{} difference vs closeness to wavelength edge, irradiance'.format(wave)
            self.squid.ink('{}_difference_irr'.format(wave), differences, 'irradiance_edge', bottoms, address, title)

        return None

    def duck(self, day=1, give=False):
        """Examine the duck like uv patch near king george island for correlation with ozone.

        Arguments:
            day: int, day of december
            give: boolean, grab output?

        Returns:
            None
        """

        # make directory
        self._make('{}/plots/duck'.format(self.sink))
        self._make('{}/plots/forest'.format(self.sink))

        # construct date
        date = '2020m12{}'.format(self._pad(day))

        # get hydra, and all paths from 12/01
        hydra = Hydra('../studies/diatom/production/island/2020')
        paths = [path for path in hydra.paths if date in path]
        paths.sort()

        # get hydra for ozone data
        hydraii = Hydra('/tis/acps/OMI/10003/OMTO3/2020/12/{}'.format(self._pad(day)))
        pathsii = [path for path in hydraii.paths if path.endswith('.he5')]
        pathsii.sort()

        # set fields
        fields = {'latitude': 'latitude', 'longitude': 'longitude', 'ozone': 'column_amount'}
        fields.update({'quality': 'quality_flag_surface', 'sky': 'clear_sky'})
        fields.update({'lambertian': 'minimum_reflectivity', 'reflectivity': 'reflectivity_360'})
        fields.update({'noon': 'noon_zenith_angle', 'zenith': 'solar_zenith_angle', 'snow': 'snow_ice'})
        fields.update({'bits': 'bit_flag'})

        # set placeholder fields for now, these will be recalculated
        fields.update({'algorithm': 'algorithm_flag', 'multiple': 'algorithm_flag', 'section': 'algorithm_flag'})
        fields.update({'sectionii': 'algorithm_flag'})

        # set stubs
        stubs = {'skyii': 'Random Forest Clear Sky 305nm', 'sky': 'Clear Sky 305nm'}
        stubs.update({'ozone': 'Ozone', 'lambertian': 'Minimum LER', 'reflectivity': 'Reflectivity 360nm'})
        stubs.update({'noon': 'Noon Zenith Angle', 'zenith': 'Solar Zenith Angle', 'snow': 'Snow / Sea Ice'})
        stubs.update({'algorithm': 'Algorithm Flag'})
        stubs.update({'multiple': 'Ozone x Noon Angle', 'section': 'Clear Sky Section'})
        stubs.update({'sectionii': 'Ozone', 'bits': 'Log 2 Quality Bit Flag'})

        # set scales
        scales = {'cloud': (0, 1), 'lambertian': (0, 100), 'ultraviolet': (0, 20), 'snow': (0, 100)}
        scales.update({'ozone': (100, 400), 'surface': (0, 150), 'sky': (0, 150), 'noon': (30, 70), 'skyii': (0, 150)})
        scales.update({'zenith': (30, 70), 'reflectivity': (0, 100)})
        scales.update({'algorithm': (0, 14), 'bits': (0, 20)})
        scales.update({'multiple': (6000, 8000)})
        scales.update({'section': (0, 150)})
        scales.update({'sectionii': (100, 400)})

        # for paths nearby king george island
        data = {field: [] for field in fields}
        for index in (8, 9, 10, 11):

            # ingest the closest to king george
            path = paths[index]
            hydra.ingest(path)

            # collection data
            datum = {field: hydra.grab(search) for field, search in fields.items()}

            # use wavelength 15 for clear sky at 305nm
            datum['sky'] = datum['sky'][:, :, 15]

            # copy into section for later
            datum['section'] = datum['sky']
            datum['sectionii'] = datum['ozone']

            # adjust snow
            datum['snow'] = numpy.where(datum['snow'] == 104, 0, datum['snow'])
            datum['snow'] = numpy.where(datum['snow'] > 100, 100, datum['snow'])

            # take log2 of bit flag
            datum['bits'] = numpy.log2(datum['bits'])

            # get algorithm flag from ozone
            hydraii.ingest(str(int(self._stage(path)['orbit'])))
            datum['algorithm'] = hydraii.grab('AlgorithmFlag')

            # multiply ozone, noon, minler
            datum['multiple'] = datum['ozone'] * datum['noon']

            # add to data
            [data[field].append(datum[field]) for field in fields.keys()]

        # create arrays
        data = {name: numpy.vstack(arrays) for name, arrays in data.items()}

        # create mask
        mask = (data['quality'] < 2) & (data['latitude'] > -70) & (data['latitude'] < -60)

        # set abscissas and stubs
        names = ['ozone', 'lambertian', 'reflectivity', 'noon', 'zenith', 'sky', 'snow', 'algorithm', 'multiple']
        abscissas = [data[name] for name in names]

        # for each absicssas
        for abscissa, name in zip(abscissas, names):

            # plot
            orbit = self._stage(path)['orbit']
            # date = self._stage(path)['day']
            title = 'Clear Sky, 305nm vs {} near King George, o{}, {}'.format(name, orbit, date)
            address = 'plots/duck/king_george_duck_vs_{}_{}.h5'.format(name, date)
            self.squid.ink('sky', data['sky'][mask], name, abscissa[mask], address, title)

        # get forest training data
        train = {name: data[name][mask] for name in names}

        # perform random forest
        self._print('running random forest...')
        forest = self.plant(train, 'sky', '{}/reports/random_forest_duck_{}.txt'.format(self.sink, date), give=True)

        # set up matrix
        shape = data['ozone'].shape
        stack = [data[name].reshape(-1, 1) for name in names[:-1]]
        matrix = numpy.hstack(stack)

        # replace fills
        matrix = numpy.where(numpy.isfinite(matrix), matrix, -999)

        # use random forest to predict sky
        skyii = forest.predict(matrix)
        data['skyii'] = skyii.reshape(shape)

        # get section near clear sky feature on 1201
        pixels = hydra._pin([-63, -70, 90], [data['latitude'], data['longitude'], data['section']])
        pixel = pixels[0]
        section = numpy.zeros(data['section'].shape)
        section[:, pixel[1]] = True
        section[:, pixel[1] - 1] = True
        section[:, pixel[1] + 1] = True
        data['section'] = numpy.where(section, data['section'], -999)

        # make line plots across section
        for name in ('sky', 'ozone', 'noon', 'lambertian', 'algorithm'):

            # create ordinate, abscissa
            ordinate = data[name][-3000:-2500, pixel[1]]
            abscissa = data['latitude'][-3000:-2500, pixel[1]]

            # remove infinities
            maskii = numpy.isfinite(ordinate) & (abs(ordinate) < 1e20) & (ordinate > 0)

            # make plots
            title = '{} across feature, {}'.format(name, date)
            address = 'plots/duck/king_george_feature_{}_{}.h5'.format(name, date)
            self.squid.ink(name, ordinate[maskii], 'latitude', abscissa[maskii], address, title)

        # get streaks near ozone feature on 1203
        pixels = hydra._pin([-65, -55, 1], [data['latitude'], data['longitude'], data['quality']])
        pixel = pixels[0]
        section = numpy.zeros(data['sectionii'].shape)
        section[pixel[0], :] = True
        section[pixel[0] - 1, :] = True
        section[pixel[0] + 1, :] = True
        data['sectionii'] = numpy.where(section, data['sectionii'], -999)

        print('sectionii pixel: {}'.format(pixel))

        # make line plots across section
        for name in ('bits', 'sky', 'ozone', 'noon', 'lambertian', 'algorithm', 'reflectivity'):

            # create ordinate, abscissa
            ordinate = data[name][pixel[0], :]
            abscissa = data['longitude'][pixel[0], :]

            print(name)
            print('pixel: {}'.format(pixel))
            print(ordinate.flatten().tolist())

            # remove infinities
            maskii = numpy.isfinite(ordinate) & (abs(ordinate) < 1e20) & (ordinate > 0)

            # make plots
            title = '{} across feature, {}'.format(name, date)
            address = 'plots/duck/king_george_streak_{}_{}.h5'.format(name, date)
            self.squid.ink(name, ordinate[maskii], 'longitude', abscissa[maskii], address, title)

        # set limits
        limits = (-100, -40, -60, -75)

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # for each field
        for name in ['sectionii', 'section', 'skyii'] + names:

            # get panel, stub, scale
            panel = data[name]
            panel = numpy.where(mask, panel, -9999)
            stub = stubs[name]
            scale = scales[name]

            # create title
            title = 'OMOCNUV, {} {}'.format(stub, date)

            # create new destination and plot
            address = 'plots/forest/{}_random_forest_{}.png'.format(name, date)
            destination = '{}/{}'.format(self.sink, address)
            parameters = {'arrays': [panel, data['latitude'], data['longitude']], 'path': destination}
            parameters.update({'texts': [title, '-']})
            parameters.update({'scale': scale, 'spectrum': [gradient, selection]})
            parameters.update({'limits': limits, 'grid': 1})
            parameters.update({'size': (10, 5), 'log': False, 'bar': True, 'back': 'white', 'south': True})
            self._stamp('plotting {}...'.format(destination))
            self._polarize(**parameters)

        # if give
        package = None
        if give:

            # ste package to data
            package = data

        return package

    def encode(self, path):
        """Attempt to read a dat file with multiple encodings.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # load encodings
        encodings = self._load('../studies/diatom/encode/encode.json')

        # for each encoding
        data = {}
        for encoding in encodings:

            # try to
            try:

                # open in encoding
                content = [line.strip().split() for line in open(path, encoding=encoding).readlines()]
                data[encoding] = content[0]

            # unless and error occurs
            except:

                # in which case, pass
                pass

        return data

    def ensemble(self, target='flux', sink='forest'):
        """Use random forest to determine cause of outliers in percent differences.

        Arguments:
            target: str, target variable

        Returns:
            None
        """

        # set degrees
        degrees = (-65, 40)

        # set tags
        tags = ['noon', 'flags']

        # link feature names to searchs
        searches = {'flux': 'flux_planar', 'zenith': '/solar_zenith_angle', 'cloud': 'cloud'}
        searches.update({'reflectivity': 'ler_value', 'value': 'n_value', 'residue': 'residue'})
        searches.update({'azimuth': 'relative_azimuth', 'latitude': 'latitude', 'longitude': 'longitude'})
        searches.update({'azimuth': 'relative_azimuth', 'sky': 'clear_sky', 'pressure': 'pressure'})
        #searches.update({'azimuth': 'relative_azimuth', 'pressure': 'pressure'})

        # set third indices
        thirds = {'flux': 20, 'reflectivity': 0, 'value': 1, 'sky': 20}
        #thirds = {'flux': 20, 'reflectivity': 0, 'value': 1}

        # set search list positions
        positions = {'pressure': -1}

        # set reservoir
        reservoir = {tag: {} for tag in tags}

        # collect first tage and find latitude indices
        ocean = self._pull(tags[0])
        latitude = ocean.grab('latitude')

        # find the low and high latitude indices
        low = max([index for index, degree in enumerate(latitude[:, 30]) if degree <= degrees[0]])
        high = min([index for index, degree in enumerate(latitude[:, 30]) if degree >= degrees[1]])

        # for each tag
        for tag in tags:

            # grab the data
            ocean = self._pull(tag)

            # for each search feature
            for name, search in searches.items():

                # add the data to the reservoir, subsetting by degree bounds
                position = positions.get(name, 0)
                array = ocean.grab(search, position)[low: high]
                reservoir[tag][name] = array

            # for each third dimension
            for name, third in thirds.items():

                # apply subset
                array = reservoir[tag][name]
                reservoir[tag][name] = array[:, :, third]

        # create percents
        data = {}
        for name in searches.keys():

            # get data
            array = reservoir[tags[0]][name]
            arrayii = reservoir[tags[1]][name]

            # add arrays
            data['{}_{}'.format(name, tags[0])] = array
            data['{}_{}'.format(name, tags[1])] = arrayii

            # calculate percent
            percent = 100 * ((arrayii / array) - 1)
            data[name] = percent

        # create latitudinal and longitudinal crosssections
        maximum = data[target].max()
        minimum = data[target].min()
        pixel = self._pin(maximum, data[target])[0]
        pixelii = self._pin(minimum, data[target])[0]
        modes = {'high': pixel, 'low': pixelii}

        # set crosssection names
        names = ['flux', 'residue', 'reflectivity', 'cloud', 'pressure', 'sky', 'value']

        # grab latitude and longitude
        latitude = ocean.grab('latitude')[low: high]
        longitude = ocean.grab('longitude')[low: high]

        # for each mode
        for mode in modes.keys():

            # and each mame
            for name in names:

                # plot the latitudinal crosssection
                pixel = modes[mode]
                abscissa = latitude[pixel[0] - 30: pixel[0] + 29, pixel[1]]
                ordinate = data['{}_{}'.format(name, tags[0])][pixel[0] - 30: pixel[0] + 29, pixel[1]]
                ordinateii = data['{}_{}'.format(name, tags[1])][pixel[0] - 30: pixel[0] + 29, pixel[1]]
                title = 'Latitudinal crosssection {}, {}'.format(name, mode)
                address = 'plots/cross/Cross_{}_latitudinal_{}.h5'.format(mode, name)
                self.squid.splatter(name, [ordinate, ordinateii], 'latitude', abscissa, address, title)

                # plot the longitudinal crosssection
                pixel = modes[mode]
                abscissa = longitude[pixel[0], :60]
                ordinate = data['{}_{}'.format(name, tags[0])][pixel[0], :60]
                ordinateii = data['{}_{}'.format(name, tags[1])][pixel[0], :60]
                title = 'Longitudinal crosssection {}, {}'.format(name, mode)
                address = 'plots/cross/Cross_{}_longitudinal_{}.h5'.format(mode, name)
                self.squid.splatter(name, [ordinate, ordinateii], 'longitude', abscissa, address, title)

        # for each of the first errors in the first 6
        report = ['Random Forests']

        # print status
        self._stamp('planting random forest...', initial=True)
        report.append(self._print('\nrunning random forest on percent differences in {}...'.format(target)))

        # create matrix
        features = [name for name in data.keys() if name != target] + [target]
        train = numpy.array([data[feature].flatten() for feature in features]).transpose(1, 0)

        # remove rows with NaNs
        train = numpy.array([row for row in train if numpy.isfinite(row).sum() == row.shape[0]])

        # split off truth
        matrix = train[:, :-1]
        truth = train[:, -1]

        # run random forest
        forest = RandomForestRegressor(n_estimators=100, max_depth=5)
        forest.fit(matrix, truth)

        # predict from training
        prediction = forest.predict(matrix)

        # calculate score
        score = forest.score(matrix, truth)
        report.append(self._print('score: {}'.format(score)))

        # get importances
        importances = forest.feature_importances_
        pairs = [(field, importance) for field, importance in zip(features, importances)]
        pairs.sort(key=lambda pair: pair[1], reverse=True)
        for field, importance in pairs:

            # add to report
            report.append(self._print('importance for {}: {}'.format(field, importance)))

        # make report
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, target)
        self._jot(report, destination)

        # make forest object
        ensemble = {'forest': forest, 'matrix': matrix, 'truth': truth}
        ensemble.update({'features': features, 'target': target, 'prediction': prediction})

        # make sink folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # also plot all relations
        for index, feature in enumerate(features[:-1]):

            # create plot
            abscissa = matrix[:, index]
            ordinate = truth
            title = 'Percent difference in {} vs {}'.format(target, feature)
            address = 'plots/{}/{}_{}_forest.h5'.format(sink, target, feature)
            self.squid.ink(target, ordinate, feature, abscissa, address, title)

            # create plot
            abscissa = matrix[:, index]
            ordinate = prediction
            title = 'Percent difference in predicted {} vs {}'.format(target, feature)
            address = 'plots/{}/{}_{}_forest_prediction.h5'.format(sink, target, feature)
            self.squid.ink(target, ordinate, feature, abscissa, address, title)

        # get highest and lowest percents
        high = numpy.argsort(truth)[-1]
        low = numpy.argsort(truth)[0]
        samples = {'high': high, 'low': low}

        # create list of factors
        self._stamp('testing sensitivity...')
        factors = [round(-0.5 + index * 0.01, 2) for index in range(101)]

        # for each feature
        for column, feature in enumerate(features[:-1]):

            # for each point
            for label, sample in samples.items():

                # get nominal values
                row = matrix[sample].copy()
                nominal = row[column]

                # create abscissa
                abscissa = [(factor + 1) * nominal for factor in factors]

                # for each point
                ordinate = []
                for point in abscissa:

                    # insert into row and make prediction
                    row[column] = point
                    prediction = forest.predict(row.reshape(1, -1))[0]
                    ordinate.append(prediction)

                # create plot
                title = 'Percent difference in {} vs {}, forest sensitivity, {}'.format(target, feature, label)
                address = 'plots/sensitivity/{}_{}_{}_{}_sensitivity.h5'.format(sink, target, label, feature)
                self.squid.ink(target, ordinate, feature, abscissa, address, title)

            # print status
            self._stamp('planted.')

        return ensemble

    def enviolet(self, month=3, collect=False):
        """Collection OMUVB and OMOCNUV ultraviolet indices data for monthly comparisons.

        Arguments:
            month: int, the month of choice
            collect: boolean, collection new data?

        Returns:
            None
        """

        # make folder
        folder = '{}/months'.format(self.sink)
        self._make(folder)

        # if collection
        if collect:

            # construct latitudes, starting at 90N, and longitudes, starting at 180W
            half = 0.5
            latitudes = numpy.array([90 - half - index for index in range(180)])
            longitudes = numpy.array([-180 + half + index for index in range(360)])

            # set up reservoirs for OMOCNUV
            counts = numpy.zeros((180, 360))
            sums = numpy.zeros((180, 360))
            squares = numpy.zeros((180, 360))

            # set up reservoirs for OMUVB
            countsii = numpy.zeros((180, 360))
            sumsii = numpy.zeros((180, 360))
            squaresii = numpy.zeros((180, 360))

            # for each day
            for day in range(1, 32):

                # create reader OMOCNUV
                formats = (self._pad(month), self._pad(day))
                hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/2005/{}/{}'.format(*formats), show=False)

                # for each path
                for path in hydra.paths:

                    # ingest
                    hydra.ingest(path)

                    # get data
                    latitude = hydra.grab('latitude')
                    longitude = hydra.grab('longitude')
                    quality = hydra.grab('quality_flag_surface')
                    zenith = hydra.grab('solar_zenith_angle')
                    ultraviolet = hydra.grab('ultraviolet_index')

                    # create mask for valid entries, with quality of 1 or less, and zenith angle less than 80
                    mask = (quality < 2) & (zenith < 80)
                    mask = mask & (latitude > -90) & (latitude < 90)
                    mask = mask & (longitude > -180) & (longitude < 180)
                    for array in (latitude, longitude, ultraviolet):

                        # add mask for valid entries
                        mask = mask & (abs(array) < 1e30) & (array > -998)

                    # apply mask
                    latitude = latitude[mask]
                    longitude = longitude[mask]
                    ultraviolet = ultraviolet[mask]

                    # get latitude indices
                    indices = (89 - numpy.floor(latitude)).astype(int)
                    indicesii = (180 + numpy.floor(longitude)).astype(int)

                    # add to reservoirs
                    counts[indices, indicesii] += 1
                    sums[indices, indicesii] += ultraviolet
                    squares[indices, indicesii] += ultraviolet ** 2

                # create reader for OMUVB using julian day
                julian = datetime.datetime(2005, month, day).timetuple().tm_yday
                hydraii = Hydra('/tis/acps/GES_DISC/003/OMUVB/2005/{}'.format(self._pad(julian, 3)), show=False)

                # for each path
                for path in hydraii.paths:

                    # ingest
                    hydraii.ingest(path)

                    # get data
                    latitude = hydraii.grab('Latitude')
                    longitude = hydraii.grab('Longitude')
                    zenith = hydraii.grab('SolarZenithAngle')
                    ultraviolet = hydraii.grab('UVindex')

                    # create mask for valid entries, with quality of 1 or less, and zenith angle less than 80
                    mask = (zenith < 80)
                    mask = mask & (latitude > -90) & (latitude < 90)
                    mask = mask & (longitude > -180) & (longitude < 180)
                    for array in (latitude, longitude, ultraviolet):

                        # add mask for valid entries
                        mask = mask & (abs(array) < 1e30) & (array > -998)

                    # apply mask
                    latitude = latitude[mask]
                    longitude = longitude[mask]
                    ultraviolet = ultraviolet[mask]

                    # get latitude indices
                    indices = (89 - numpy.floor(latitude)).astype(int)
                    indicesii = (180 + numpy.floor(longitude)).astype(int)

                    # add to reservoirs
                    countsii[indices, indicesii] += 1
                    sumsii[indices, indicesii] += ultraviolet
                    squaresii[indices, indicesii] += ultraviolet ** 2

            # construct file
            data = {'latitude': latitudes, 'longitude': longitudes}
            data.update({'omocnuv_counts': counts, 'omocnuv_sums': sums, 'omocnuv_squares': squares})
            data.update({'omuvb_counts': countsii, 'omuvb_sums': sumsii, 'omuvb_squares': squaresii})

            # spawn file
            destination = '{}/Monthly_UVindex_Collection_2005_{}.h5'.format(folder, self._pad(month))
            self.spawn(destination, data)

        # otherwise
        else:

            # pass for now
            pass

        return None

    def evaluate(self, tag='whole', row=30):
        """Evaluate the nvalue differences.

        Arguments:
            tag: str, tag for omocned file
            row: int, row number

        Returns:
            None
        """

        # create comparisons dictionary
        comparisons = {'latitude': 'latitude', 'longitude': 'longitude'}
        comparisons.update({'chlorophyll': 'chlorophyll', 'ozone': 'ozone'})
        comparisons.update({'n331': 'n_values_0', 'n340': 'n_values_1'})
        comparisons.update({'n360': 'n_values_2', 'n380': 'n_values_3'})
        comparisons.update({'phi': 'relative_azimuth_angle', 'pteran': 'terrain_pressure'})
        comparisons.update({'satza': 'satellite_zenith_angle', 'sza': 'solar_zenith_angle'})

        # add prefixes
        comparisons = {'ascii_{}'.format(first): 'omocned_{}'.format(second) for first, second in comparisons.items()}

        # grab ascii file
        hydra = Hydra('{}/data/grid'.format(self.sink))
        hydra.ingest()

        # grab omocned data
        hydraii = self._pull(tag, grid=True)

        # begin augmentation features
        augmentation = []
        for first, second in comparisons.items():

            # grab the second feature
            feature = hydraii.dig(second)[0]

            # rename it to be consistent with ascii
            feature.fill()
            feature.dub(first.replace('ascii', 'omocned'))
            augmentation.append(feature)

        # create destination
        destination = '{}/data/duel/Asciii_OMOCNUV_n_values_swath_{}.h5'.format(self.sink, tag)
        hydra.augment(hydra.paths[0], augmentation, destination)

        # reopen file
        duel = Hydra(destination)
        duel.ingest()

        # make plots vs latitude, for row 30
        latitude = duel.grab('omocned_latitude')[:, row]
        for first in comparisons.keys():

            # replace second prefix
            second = first.replace('ascii', 'omocned')

            # plot Ascii and OMOCNUV
            stub = first.replace('ascii_', '')
            address = 'plots/bias/Ascii_OMOCNUV_{}_{}_{}.h5'.format(stub, row, tag)
            title = 'Ascii vs OMOCNUV {}, Row {}'.format(stub, row)
            ordinates = [duel.grab(first)[:, row], duel.grab(second)[:, row]]
            self.squid.splatter(stub, ordinates, 'latitude', latitude, address, title)

            # plot difference
            address = 'plots/bias/Ascii_OMOCNUV_{}_Difference_{}_{}.h5'.format(stub, row, tag)
            title = 'Difference, OMOCNUV - Ascii, {}, Row {}'.format(stub, row)
            difference = duel.grab(second)[:, row] - duel.grab(first)[:, row]
            self.squid.ink('{}_difference'.format(stub), difference, 'latitude', latitude, address, title)

            # plot percent error
            address = 'plots/bias/Ascii_OMOCNUV_{}_Percent_{}_{}.h5'.format(stub, row, tag)
            title = 'Percent Difference, OMOCNUV vs Ascii, {}, Row {}'.format(stub, row)
            percent = 100 * ((duel.grab(second)[:, row] / duel.grab(first)[:, row]) - 1)
            self.squid.ink('{}_percent'.format(stub), percent, 'latitude', latitude, address, title)

            # plot average difference across rows
            rows = numpy.array(range(60))
            address = 'plots/bias/Ascii_OMOCNUV_{}_Average_Row_Bias_{}.h5'.format(stub, tag)
            title = 'Average Bias, OMOCNUV vs Ascii, {}, vs Row'.format(stub)
            difference = (duel.grab(second)[400:1400] - duel.grab(first)[400:1400]).mean(axis=0)
            self.squid.ink('{}_avg_bias'.format(stub), difference, 'row', rows, address, title)

            # plot average row multiplers
            rows = numpy.array(range(60))
            address = 'plots/bias/Ascii_OMOCNUV_{}_Average_Row_Multipiers_{}.h5'.format(stub, tag)
            title = 'Average Row Multipliers, OMOCNUV vs Ascii, {}, vs Row'.format(stub)
            difference = (duel.grab(second)[400:1400] - duel.grab(first)[400:1400]).mean(axis=0)
            self.squid.ink('{}_avg_multiplier'.format(stub), numpy.exp(difference), 'row', rows, address, title)

            # plot heat map of nvalue differences
            difference = duel.grab(second) - duel.grab(first)
            latitude = duel.grab('ascii_latitude')
            longitude = duel.grab('ascii_longitude')
            address = 'plots/value/Ascii_OMOCNUV_{}_Heatmap_{}.h5'.format(stub, tag)
            title = '{} differences, OMOCNUV vs Ascii'.format(stub)
            bounds = [-0.10, -0.05, -0.02, -0.01, 0.01, 0.02, 0.05, 0.10]
            parameters = ['{}_difference'.format(stub), difference, 'latitude', latitude]
            parameters += ['longitude', longitude, address, bounds, title]
            self.squid.shimmer(*parameters)

        return None

    def examine(self, tag='pace', resolution=10):
        """Examine several orbital plots.

        Arguments:
            tag: str, tag of output file
            resolution: int, resolution of plots

        Returns:
            None
        """

        # for each reflectivity:
        for index, wave in enumerate((340, 360, 380)):

            # diagram
            name = 'reflectivity, {}nm'.format(wave)
            self.diagram(tag, name, 'reflectivity', third=index, bracket=(0, 100), resolution=resolution)

        # plot surface flux at 310nm
        name = 'planar surface flux 310nm'
        self.diagram(tag, name, 'planar/irradiance', 20, bracket=(0, 250), resolution=resolution)

        # plot clear sky flux at 310nm
        name = 'clear sky surface flux 310nm'
        self.diagram(tag, name, 'sky', 20, bracket=(0, 250), resolution=resolution)

        # plot ozone
        name = 'total column ozone'
        self.diagram(tag, name, 'ozone', digits=1, resolution=resolution)

        # plot cloud tau
        name = 'cloud tau optical depth'
        self.diagram(tag, name, 'cloud', digits=2, resolution=resolution)

        # plot residue 331
        name = 'residue 331nm'
        self.diagram(tag, name, 'residue', digits=2, resolution=resolution, bracket=(-10, 10))

        # plot processing flag
        name = 'processing flags'
        self.diagram(tag, name, 'process', resolution=resolution)

        # plot planar transmittance
        name = 'planar transmittance 310nm'
        self.diagram(tag, name, 'transmittance/planar', 20, digits=3, resolution=resolution)

        # plot attenuation constant
        name = 'diffuse planar attenuation coefficient 310nm'
        self.diagram(tag, name, 'coefficient/planar', 20, digits=2, resolution=resolution)

        return None

    def extract(self, pairs, path, sink='plots/series', anomaly=True, tolerance=1, data=None):
        """Extract time series from concatenated ocean color file

        Arguments:
            pairs: list of latitude, longitude pairs
            path: str, path of concatenation file
            sink: str, destination folder
            tolerance: tolerance in location
            anomaly: boolean, filter out row anomaly?
            data: previous data object

        Returns:
            None
        """

        # ingest path
        self._stamp('ingesting {}...'.format(path), initial=True)
        hydra = Hydra(path)
        hydra.ingest()

        # set fields
        fields = ['latitude', 'longitude', 'chlorophyll', 'ozone', 'pressure']
        fields += ['cloud', 'n_value', 'reflectivity', 'residue', 'clear_sky']
        fields += ['coefficient_planar', 'transmittance_planar', 'irradiance_planar']

        # if anomaly
        if anomaly:

            # also add row anomaly
            fields += ['row_anomaly']

        # if data not already given
        self._stamp('grabbing data...')
        if not data:

            # grab data from files
            data = {field: hydra.grab(field) for field in fields}

        # grab the time field
        time = hydra.grab('time')

        # for each pair
        for latitude, longitude in pairs:

            # print pair
            self._stamp('latitude, longitude: {}, {}'.format(latitude, longitude))

            # get latitudes and longitudes, and their minimums and maximums
            self._stamp('determining mask...')
            latitudes = data['latitude']
            longitudes = data['longitude']

            # remove fill values
            minimum = numpy.array([panel[panel > -999].min() for panel in latitudes])
            maximum = numpy.array([panel[panel > -999].max() for panel in latitudes])
            minimumii = numpy.array([panel[panel > -999].min() for panel in longitudes])
            maximumii = numpy.array([panel[panel > -999].max() for panel in longitudes])

            # construct mask
            mask = (minimum < latitude) & (maximum > latitude) & (minimumii < longitude) & (maximumii > longitude)

            # apply mask to all data
            self._stamp('applying mask...')
            subset = {field: array[mask] for field, array in data.items()}
            timeii = time[mask]

            # construct pairs from latitude and longtides
            self._stamp('getting pixels...')
            latitudes = subset['latitude']
            longitudes = subset['longitude']
            zipper = list(zip(latitudes, longitudes))
            pixels = [self._pin([latitude, longitude], [panel, panelii])[0] for panel, panelii in zipper]

            # get targetted subset
            self._stamp('targetting coordinates...')
            target = {field: [panel[pixel] for panel, pixel in zip(array, pixels)] for field, array in subset.items()}
            target = {field: numpy.array(array) for field, array in target.items()}

            # get times
            timeii = numpy.array([orbit[pixel[0]] for orbit, pixel in zip(timeii, pixels)])

            # if screening anomaly
            if anomaly:

                # get anomous mask
                anomalous = numpy.array([((pixel[1] > 25) & (pixel[1] < 55)) for pixel in pixels])

                # remove from target and time
                target = {field: array[numpy.logical_not(anomalous)] for field, array in target.items()}
                timeii = timeii[numpy.logical_not(anomalous)]

            # add penetration depth
            target['depth'] = -numpy.log(0.1) / target['coefficient_planar']

            # grab new latitudes and longitudes
            latitudes = target['latitude']
            longitudes = target['longitude']

            # check squared distance to latitude, longitude
            difference = abs(latitudes - latitude) ** 2
            differenceii = abs(longitudes - longitude) ** 2
            squares = difference + differenceii
            maskii = (squares <= tolerance)

            # apply mask
            target = {field: array[maskii] for field, array in target.items()}
            timeii = timeii[maskii]

            # create sink folder
            self._make('{}/{}'.format(self.sink, sink))

            # construct plots
            self._stamp('plotting...')
            for field, array in target.items():

                # create formatting function for latitude, longitude
                def formatting(degree): return '{}N'.format(int(degree)) if degree > 0 else '{}S'.format(int(abs(degree)))
                def formattingii(degree): return '{}E'.format(int(degree)) if degree > 0 else '{}W'.format(int(abs(degree)))

                # create mask for infinites
                maskiii = numpy.isfinite(array)

                # # if anomaly
                # if anomaly:
                #
                #     # include row anomaly in maks
                #     maskiii = (numpy.isfinite(array)) & (target['row_anomaly'] < 1)

                # create ordinate using mask
                ordinate = array[maskiii]

                # create abscissa for time as seconds since 2010-01-01
                abscissa = timeii[maskiii]

                # get year
                year = path.split('.')[-2][-4:]
                year = int(year)

                # convert to year fraction
                initial = (year - 2010) * (365.25 * 24 * 60 * 60)
                final = (1 + year - 2010) * (365.25 * 24 * 60 * 60)
                abscissa = (abscissa - initial) / (final - initial)

                # create plot
                formats = (field, year, formatting(latitude), formattingii(longitude), '_row' * int(anomaly))
                address = '{}/{}_{}_{}_{}{}.h5'.format(sink, *formats)
                formats = (field, formatting(latitude), formattingii(longitude))
                title = '{}, 2005 vs 2018, latitude {}, longitude {}'.format(*formats)
                self.squid.ink(field, ordinate, 'year_fraction', abscissa, address, title)

            # print status
            self._stamp('plotted.')

        return data

    def fabricate(self):
        """Fabricate OMGLER and OMAERUV data.

        Arguments:
            None

        Returns:
            None
        """

        # create folder
        folder = '{}/fabrication'.format(self.sink)
        self._make(folder)

        # get colocations
        hydra = Hydra('{}/colocation'.format(self.sink))

        # for each path
        for path in hydra.paths:

            # ingest path
            hydra.ingest(path)

            # get chlorophyll
            chlorophyll = hydra.grab('chlorophyll')

            # fake lambertian
            lambertian = numpy.ones(chlorophyll.shape)
            lambertian = numpy.array([lambertian] * 4).transpose(1, 2, 0)

            # fake aerosols
            aerosol = numpy.ones(chlorophyll.shape) * -1
            aerosols = numpy.array([aerosol] * 3).transpose(1, 2, 0)

            # fake wavelengths
            wavelength = numpy.array([354, 388, 388])

            # create data
            data = {'Geometry_Dependent_Surface_LER/ChlorophyllConcentration': chlorophyll}
            data.update({'Geometry_Dependent_Surface_LER/GLER': lambertian})
            data.update({'SCIDATA/FinalAerosolAbsOpticalDepth': aerosols})
            data.update({'Wavelengths': wavelength})

            # create destination
            destination = path.replace('colocation', 'fabrication').replace('colocated', 'fabricated')

            # create new data
            hydra.spawn(destination, data)

        return None

    def figure(self, suffix=''):
        """Produce figures for aerosol paper.

        Arguments:
            suffix: str, suffix for plot and figure folders

        Returns:
            None
        """

        # set groups
        groups = [['aerosol', 'exponent', 'absorption']]
        groups += [['chlorophyll', 'coefficient', 'depth']]
        groups += [['ultraviolet_cloud', 'ultraviolet_percent', 'aerosol_cloud']]
        groups += [['damage_cloud', 'damage_percent', 'aerosol_cloud']]

        # set figure numbers
        figures = ['2', '4', '3', '5']

        # set month folders
        months = ['march', 'june', 'september', 'december']
        monthsii = ['03', '06', '09', '12']
        calendars = [months, months, monthsii, monthsii]

        # set reservoirs
        clouds = ['cloud'] * 4
        reservoirs = [months, months, clouds, clouds]

        # define the image frame in pixels, top, bottom, left, right
        frame = (150, -345, 330, -90)

        # set clip distances for plots
        top = 120
        bottom = -345
        left = 270
        right = -1
        title = 0
        bar = -1
        date = 0

        # set offsets for plot boundaries
        corner = (title, bottom, date, right)
        upper = (title, bottom, left, right)
        side = (top, bottom, date, right)
        middle = (top, bottom, left, right)
        cornerii = (top, bar, date, right)
        lower = (top, bar, left, right)

        # set margins for clipping plots
        margins = [[corner, upper, upper]]
        margins += [[side, middle, middle]]
        margins += [[side, middle, middle]]
        margins += [[cornerii, lower, lower]]

        # for each ordering
        for group, figure, calendar, reservoir in zip(groups, figures, calendars, reservoirs):

            # begin rows
            rows = []

            # for each month
            for month, folder, margin in zip(calendar, reservoir, margins):

                # collect images
                images = []

                # for each member
                for member, clip in zip(group, margin):

                    # unpack margin
                    top, bottom, left, right = clip

                    # convert image into array
                    formats = (self.sink, suffix, folder, member, month)
                    image = PIL.Image.open('{}/paper/plots{}/{}/OMI_OMOCNUV_{}_{}.png'.format(*formats))
                    array = numpy.array(image)

                    # unpack frame
                    topii, bottomii, leftii, rightii = frame

                    # if removing title
                    if top > title:

                        # blank out top
                        array[0: topii, leftii:rightii, :] = 255

                    # if removing colorbar
                    if bottom < bar:

                        # blank out bottom
                        array[bottomii: -1, leftii:rightii, :] = 255

                    # if removing date
                    if left > date:

                        # blank out left
                        array[topii: bottomii, 0: leftii, :] = 255

                    # crop image and add to images
                    array = array[top: bottom, left: right]
                    images.append(array)

                # stack images and add to rows
                row = numpy.hstack(images)
                rows.append(row)

            # stack rows
            panel = numpy.vstack(rows)

            # save figure
            destination = '{}/paper/figures{}/figure_{}.png'.format(self.sink, suffix, figure)
            PIL.Image.fromarray(panel).save(destination)

            # save as eps
            destinationii = destination.replace('png', 'eps')
            PIL.Image.fromarray(panel).convert('RGB').save(destinationii, format='eps', bbox_inches='tight')

        return None

    def flag(self, field='erythemaii', fieldii='erythema_scalar', bounds=(-40, 40)):
        """Assoicate differences between two fields with the radiance flags at that pixel.

        Arguments:
            field: str, Ascii field
            fieldii: str, OMOCNUV field
            bounds: tuple of float, latitude bounds for study

        Returns:
            None
        """

        # grab the grid file
        grid = Hydra('{}/data/grid'.format(self.sink))
        grid.ingest()

        # get latitude and longitude
        latitude = grid.grab('latitude')
        longitude = grid.grab('longitude')

        # get shape, then flatten coordinates
        shape = latitude.shape
        latitude = latitude.flatten()
        longitude = longitude.flatten()

        # create mask for latitude bounds and apply
        mask = (latitude >= bounds[0]) & (latitude <= bounds[1])
        latitude = latitude[mask]
        longitude = longitude[mask]

        # grab the arrays
        array = grid.grab('ascii_{}'.format(field)).flatten()[mask]
        arrayii = grid.grab('omocned_{}'.format(fieldii)).flatten()[mask]

        # also grab the pressures
        pressure = grid.grab('ascii_{}'.format('pteran')).flatten()[mask]
        pressureii = grid.grab('omocned_{}'.format('terrain_pressure')).flatten()[mask]

        # open frames
        frames = Hydra('../studies/diatom/frames')
        ultraviolet = [path for path in frames.paths if 'OML1BRUG' in path][0]
        visible = [path for path in frames.paths if 'OML1BRVG' in path][0]

        # collect quality flags, band 2
        frames.ingest(ultraviolet)
        pixels = 557
        quality = frames.grab('BAND2/spectral_channel_quality')
        quality = quality[0, :, :, :].flatten().reshape(-1, pixels)[mask]
        wavelength = frames.grab('BAND2/wavelength')[0]
        wavelength = numpy.array([wavelength] * shape[0]).flatten().reshape(-1, pixels)[mask]

        # collect quality flags, band 3
        frames.ingest(visible)
        pixelsii = 751
        qualityii = frames.grab('BAND3/spectral_channel_quality')
        qualityii = qualityii[0, :, :, :].flatten().reshape(-1, pixelsii)[mask]
        wavelengthii = frames.grab('BAND3/wavelength')[0]
        wavelengthii = numpy.array([wavelengthii] * shape[0]).flatten().reshape(-1, pixelsii)[mask]

        # calculate error and absolute error
        error = 100 * ((arrayii / array) - 1)
        absolute = abs(error)

        # get order of errors, biggest first
        order = absolute.argsort().tolist()
        order = order[::-1]

        # begin report
        report = ['report of flags vs erythemal errors']
        window = 10
        for position in order[:100]:

            # list erythema error
            report.append('{} ( {}, {} )'.format(error[position], latitude[position], longitude[position]))

            # list pressures
            report.append('   pressures: {}, {}'.format(pressure[position], pressureii[position]))

            # determine n331 flags from band2
            wave = ((wavelength[position] - 331) ** 2).argsort()[0]
            report.append('   band2, n331: {}'.format(quality[position][wave - window: wave + window]))

            # determine n340 flags from band2
            wave = ((wavelength[position] - 340) ** 2).argsort()[0]
            report.append('   band2, n340: {}'.format(quality[position][wave - window: wave + window]))

            # determine n360 flags from band3
            wave = ((wavelengthii[position] - 360) ** 2).argsort()[0]
            report.append('   band3, n360: {}'.format(qualityii[position][wave - window: wave + window]))

            # determine n380 flags from band3
            wave = ((wavelengthii[position] - 380) ** 2).argsort()[0]
            report.append('   band3, n380: {}'.format(qualityii[position][wave - window: wave + window]))

        # jot report
        destination = '{}/reports/flag_report'.format(self.sink)
        self._jot(report, destination)

        return None

    def flash(self, row, latitude):
        """Create a report for inputs at a particular output.

        Arguments:
            row: index of row for centering comparison
            latitude: latitude of pixel for centering comparison

        Returns:
            None
        """

        # create ascii, inputs data pairs
        inputs = {'n331': 'n_values_0', 'n340': 'n_values_1'}
        inputs.update({'n360': 'n_values_2', 'n380': 'n_values_3'})
        inputs.update({'ozone': 'ozone'})
        inputs.update({'pteran': 'terrain_pressure'})
        inputs.update({'chl': 'chlorophyll'})
        inputs.update({'algflg': 'algorithm_flag'})
        inputs.update({'lat': 'latitude'})
        inputs.update({'lon': 'longitude'})
        inputs.update({'phi': 'relative_azimuth_angle'})
        inputs.update({'satza': 'satellite_zenith_angle'})
        inputs.update({'sza': 'solar_zenith_angle'})

        # create ascii, omocned data pairs, outputs
        outputs = {'erythema': 'erythema_planar_Ed'}
        outputs.update({'pteran': 'terrain_pressure'})

        # grab the grid file
        grid = Hydra('{}/data/grid'.format(self.sink))
        grid.ingest()

        # grab the latitudes grid
        latitudes = grid.grab('omocned_latitude')
        image = numpy.argsort((latitudes[:, row] - latitude) ** 2)[0]
        abscissa = numpy.array(range(60))

        # begin report
        formats = (row, image, latitude)
        report = ['input / outputs comparison, row: {}, image: {}, latitude: {}'.format(*formats)]
        report.append('ascii field     omocned field     ascii file     omocned file     difference     error ( % )')
        report.append('')

        # add outputs
        for name, nameii in outputs.items():

            print(name, nameii)

            # get the data
            first = round(grid.grab('ascii_{}'.format(name))[image, row], 10)
            second = round(grid.grab('omocned_{}'.format(nameii))[image, row], 10)

            # compute difference and error
            difference = round(second - first, 10)
            percent = round(100 * ((second / first) - 1), 10)

            # add to report
            report.append('{} {} {}  {}  {}  {}'.format(name, nameii, first, second, difference, percent))

        # add spacer
        report.append('')

        # repeat for inputs
        for name, nameii in inputs.items():

            # append
            print(name, nameii)

            # get the data
            first = round(grid.grab('ascii_{}'.format(name))[image, row], 6)
            second = round(grid.grab('omocned_{}'.format(nameii))[image, row], 6)

            # compute difference and error
            difference = round(second - first, 6)
            percent = round(100 * ((second / first) - 1), 6)

            # add to report
            report.append('{}   {}   {}   {}   {}   {}'.format(name, nameii, first, second, difference, percent))

        # jot report
        formats = (self.sink, self._pad(row), self._pad(image, 4))
        destination = '{}/reports/ascii_omocned_comparison_row_{}_image_{}'.format(*formats)
        self._jot(report, destination)

        return None

    def flux(self, ozone=315, latitude=2, zenith=77):
        """Examing flux table spectra for antarctic reflectivity effects.

        Arguments:
            ozone: float, ozone amount
            latitude: int, latitude band
            zenith: float, zenith angle

        Returns:
            None
        """

        # make directory
        self._make('{}/plots/flux'.format(self.sink))

        # get flux table
        hydra = Hydra('{}/tables'.format(self.sink))
        hydra.ingest('Flux_Table')

        # grab irradiance and other nodes
        fluxes = hydra.grab('ultraviolet_irradiance')
        ozones = hydra.grab('ozone')
        zeniths = hydra.grab('zenith')
        pressures = hydra.grab('log_pressure')
        reflectivities = hydra.grab('reflectivity')
        wavelengths = hydra.grab('wavelength')

        # find ozone index
        indices = hydra._pin(ozone, ozones, 3)
        indices.sort()
        index = indices[latitude][0]

        # find zenith index
        indexii = hydra._pin(zenith, zeniths, 1)[0][0]

        # for each reflectivity index
        for reflectivity in range(91, 100):

            # get flux spectrum ( pressure, reflectivty, wavelength, ozone, zenith )
            flux = fluxes[0, :, reflectivity, index, indexii]

            # take exponential
            flux = numpy.exp(flux)

            # plot
            formats = (int(ozones[index]), int(zeniths[indexii]), int(reflectivities[reflectivity]))
            title = 'Flux Table, 1 atm, {} DU, {} deg, {} reflectivity'.format(*formats)
            address = 'plots/flux/UV_Flux_{}_{}_{}.h5'.format(*formats)
            self.squid.ink('flux', flux, 'wavelength', wavelengths, address, title)

        return None

    def gleam(self):
        """Compare the two output files on the basis of min and max.

        Arguments:
            None

        Returns:
            None
        """

        # get the ascii data
        data = self.synthesize()['data']

        # load in the hdf5 data
        hydra = Hydra('../studies/diatom/erythema')

        # grab the data
        hydra.ingest(0)

        # create comparison brackets
        comparisons = {'erythema': 'erythema_planar', 'erythemaii': 'erythema_scalar'}
        comparisons.update({'dose': 'dose_planar', 'doseii': 'dose_scalar'})
        comparisons.update({'constant': 'constant_planar', 'constantii': 'constant_scalar'})
        comparisons.update({'ozone': 'ozone', 'chlorophyll': 'chlorophyll'})
        comparisons.update({'latitude': 'latitude', 'longitude': 'longitude'})
        comparisons.update({'zenith': 'solar_zenith', 'ultraviolet': 'ultraviolet_spectrum'})

        # go through comparisons
        report = ['OMOCNUV Comparison Report']
        for field, fieldii in comparisons.items():

            # print header
            report.append(self._print('\n'))
            report.append(self._print('{}:'.format(field)))

            # get the ascii data min and max
            array = data[field]
            mask = (numpy.isfinite(array)) & (array > 0) & (array < 1e10)
            minimum = array[mask].min()
            mean = array[mask].mean()
            maximum = array[mask].max()
            report.append(self._print('ascii min: {}'.format(minimum)))
            report.append(self._print('ascii mean: {}'.format(mean)))
            report.append(self._print('ascii max: {}'.format(maximum)))

            # add spacer
            report.append(self._print('\n'))

            # get the hdf5 data min and max
            array = hydra.grab(fieldii)
            mask = (numpy.isfinite(array)) & (array > 0) & (array < 1e10)
            minimum = array[mask].min()
            mean = array[mask].mean()
            maximum = array[mask].max()
            report.append(self._print('hdf5 min: {}'.format(minimum)))
            report.append(self._print('hdf5 mean: {}'.format(mean)))
            report.append(self._print('hdf5 max: {}'.format(maximum)))

        # jot report
        self._jot(report, '{}/reports/out_file_comparison.txt'.format(self.sink))

        return None

    def glint(self, waves=(310, 360, 390), source='glint', sink='glint'):
        """Plot the sun glint angle versus clear sky spectra.

        Arguments:
            waves: tuple of ints, wavelengths

        Returns:
            None
        """

        # craete sink folder
        self._make('{}/plots/{}'.format(self.sink, sink))

        # get the data
        hydra = Hydra('{}/{}'.format(self.sink, source))

        # ingest top path
        hydra.ingest()

        # grab angles
        zenith = hydra.grab('solar_zenith')
        zenithii = hydra.grab('viewing_zenith')
        azimuth = hydra.grab('relative_azimuth')
        sky = hydra.grab('clear_sky')
        surface = hydra.grab('irradiance_planar')
        wavelength = hydra.grab('spectral_wavelength')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        reflectance = hydra.grab('n_value')
        wavelengthii = hydra.grab('n_value_wavelength')
        wavelengthiii = hydra.grab('reflectivity_wavelength')
        reflectivity = hydra.grab('reflectivity')
        residue = hydra.grab('residue')
        cloud = hydra.grab('cloud_tau')

        # convert to radiancs
        radians = (numpy.pi / 180)
        zenith = zenith * radians
        zenithii = zenithii * radians

        # straight relative azimuth gives wrong results
        azimuth = (180 - azimuth) * radians

        # calculate glint angle arccos[ cos cosii - sin sinii cosiii ]
        cosines = numpy.cos(zenith) * numpy.cos(zenithii)
        sines = numpy.sin(zenith) * numpy.sin(zenithii) * numpy.cos(azimuth)
        glint = numpy.arccos(cosines - sines)

        # convert back to degrees
        glint = glint / radians

        # for each wavelength
        for wave in waves:

            # grab wavelength index
            index = self._pin(wave, wavelength)[0][0]

            # prepare data
            ordinate = sky[:, :, index]
            mask = numpy.isfinite(ordinate) & (ordinate > 0)
            ordinate = ordinate[mask]
            abscissa = glint[mask]

            # get orbit attributes
            date = self._stage(hydra.current)['day']
            orbit = self._stage(hydra.current)['orbit']

            # plot versus sky
            address = 'plots/{}/Sun_glint_angle_versus_clear_sky_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Clear Sky irradiance {}, o{}, {}nm'.format(date, orbit, wave)
            self.squid.ink('clear_sky_irradiance', ordinate, 'sun_glint_angle', abscissa, address, title)

            # plot versus sky
            ordinate = surface[:, :, index][mask]
            address = 'plots/{}/Sun_glint_angle_versus_surface_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Surface Irradiance, {}, o{}, {}nm'.format(date, orbit, wave)
            self.squid.ink('surface_irradiance', ordinate, 'sun_glint_angle', abscissa, address, title)

            # plot sky, surface differnxce
            ordinate = (sky[:, :, index] - surface[:, :, index])[mask]
            address = 'plots/{}/Sun_glint_angle_versus_sky_surface_difference_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Clear Sky - Surface, {}, o{}, {}nm'.format(date, orbit, wave)
            self.squid.ink('sky_surface_difference', ordinate, 'sun_glint_angle', abscissa, address, title)

            # plot versus latitude
            ordinate = latitude[mask]
            address = 'plots/{}/Sun_glint_angle_versus_latitude_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Latitude {}, o{}'.format(date, orbit)
            self.squid.ink('latitude', ordinate, 'sun_glint_angle', abscissa, address, title)

            # plot versus longitude
            ordinate = longitude[mask]
            address = 'plots/{}/Sun_glint_angle_versus_longitude_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Longitude, {}, o{}'.format(date, orbit)
            self.squid.ink('longitude', ordinate, 'sun_glint_angle', abscissa, address, title)

            # plot versus residue
            ordinate = residue[mask]
            maskii = numpy.isfinite(ordinate)
            address = 'plots/{}/Sun_glint_angle_versus_residue_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Residue331, {}, o{}'.format(date, orbit)
            self.squid.ink('residue', ordinate[maskii], 'sun_glint_angle', abscissa[maskii], address, title)

            # plot versus cloud tau
            ordinate = cloud[mask]
            maskii = numpy.isfinite(ordinate)
            address = 'plots/{}/Sun_glint_angle_versus_cloud_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Cloud Tau, {}, o{}'.format(date, orbit)
            self.squid.ink('cloud_tau', ordinate[maskii], 'sun_glint_angle', abscissa[maskii], address, title)

            # plot versus residue
            ordinate = zenith[mask] / radians
            maskii = numpy.isfinite(ordinate)
            address = 'plots/{}/Sun_glint_angle_versus_zenith_{}_{}.h5'.format(sink, date, wave)
            title = 'Sun glint angle versus Solar Zenith Angle, {}, o{}'.format(date, orbit)
            self.squid.ink('zenith', ordinate[maskii], 'sun_glint_angle', abscissa[maskii], address, title)

        # for each nvalue wavelength
        for index, wave in enumerate(wavelengthii):

            # prepare data
            ordinate = reflectance[:, :, index]
            mask = numpy.isfinite(ordinate) & (ordinate > 0) & (abs(latitude) < 20)
            ordinate = ordinate[mask]
            abscissa = glint[mask]

            # plot versus sky
            address = 'plots/{}/Sun_glint_angle_versus_n_value_{}_{}.h5'.format(sink, date, numpy.ceil(wave))
            formats = (date, orbit, numpy.ceil(wave))
            title = 'Sun glint angle sensitivity, N value, {}, o{}, {}nm, 20S- 20N'.format(*formats)
            self.squid.ink('n_value', ordinate, 'sun_glint_angle', abscissa, address, title)

        # for each reflectivity wavelength
        for index, wave in enumerate(wavelengthiii):

            # prepare data
            ordinate = reflectivity[:, :, index]
            mask = numpy.isfinite(ordinate) & (ordinate > 0) & (abs(latitude) < 20)
            ordinate = ordinate[mask]
            abscissa = glint[mask]

            # plot versus sky
            address = 'plots/{}/Sun_glint_angle_versus_ler_{}_{}.h5'.format(sink, date, numpy.ceil(wave))
            formats = (date, orbit, numpy.ceil(wave))
            title = 'Sun glint angle sensitivity, Reflectivity, {}, o{}, {}nm, 20S- 20N'.format(*formats)
            self.squid.ink('ler', ordinate, 'sun_glint_angle', abscissa, address, title)

        return None

    def glisten(self, tag=None, data=None):
        """Compare the two output files and create difference maps.

        Arguments:
            tag: str, tag to pull output file
            data: data instance

        Returns:
            None
        """

        # get the ascii data
        data = data or self.synthesize()['data']

        # get the tagged erythema file
        hydra = self._pull(tag)

        # create comparison brackets
        comparisons = {'erythema': 'erythema_planar', 'erythemaii': 'erythema_scalar'}
        comparisons.update({'dose': 'dose_planar', 'doseii': 'dose_scalar'})
        comparisons.update({'constant': 'constant_planar', 'constantii': 'constant_scalar'})
        comparisons.update({'ozone': 'ozone', 'chlorophyll': 'chlorophyll'})
        comparisons.update({'ultraviolet': 'ultraviolet_surface_flux_planar'})

        # create names
        names = {'erythema': 'Planar Erythemal Dose Rate, Ed ( mW / m^2 )'}
        names.update({'erythemaii': 'Scalar Erythemal Dose Rate, Eo ( mW / m^2 )'})
        names.update({'dose': 'Planar DNA Dose Rate, Kdnad ( mW / m^2 )'})
        names.update({'doseii': 'Scalar DNA Dose Rate, Kdnao ( mW / m^2 )'})
        names.update({'constant': 'Planar Attenuation Constant, Kdm ( 290 nm )'})
        names.update({'constantii': 'Scalar Attenuation Constant, Kom ( 290 nm )'})
        names.update({'ozone': 'Ozone ( DU )', 'chlorophyll': 'Chlorophyll ( mg / m^3 )'})
        names.update({'ultraviolet': 'Ultraviolet Radiance'})

        # create bounding boxes
        boxes = {'erythema': [[0, 100, 200, 300, 400, 500, 600]], 'erythemaii': [[0, 100, 200, 300, 400, 500, 600]]}
        boxes.update({'dose': [[0, 0.05, 0.1, 0.15, 0.2, 0.25, 1.0]], 'doseii': [[0, 0.05, 0.1, 0.15, 0.2, 0.25, 1.0]]})
        boxes.update({'constant': [[0, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 4]], 'constantii': [[0, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 4]]})
        boxes.update({'ozone': [[0, 250, 275, 300, 325, 350, 450]], 'chlorophyll': [[0, 0.05, 0.1, 0.2, 0.5, 1, 15]]})
        boxes.update({'ultraviolet': [[0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1], [0, 200, 400, 600, 800, 1000, 1200], [0, 200, 400, 600, 800, 1000, 1200]]})

        # set waves different indices for 3-D arrays
        waves = {field: [0] for field in comparisons.keys()}
        waves.update({'ultraviolet': [0, 30, -10]})

        # add details
        details = {field: [''] for field in comparisons.keys()}
        details.update({'ultraviolet': ['( 290 nm )', '( 320 nm )', '( 390 nm )']})

        # add stubs for indicating field
        stubs = {field: [''] for field in comparisons.keys()}
        stubs.update({'ultraviolet': ['_290', '_320', '_390']})

        # round latitudes and longitudes
        latitude = numpy.round(data['latitude'], 2).flatten()
        longitude = numpy.round(data['longitude'], 2).flatten()
        latitudeii = numpy.round(hydra.grab('latitude'), 2).flatten()
        longitudeii = numpy.round(hydra.grab('longitude'), 2).flatten()

        # make coordinates dictionaries
        coordinates = {pair: index for index, pair in enumerate(zip(latitude, longitude))}
        coordinatesii = {pair: index for index, pair in enumerate(zip(latitudeii, longitudeii))}

        # begin overlap
        overlap = {pair: [] for pair in list(coordinates.keys()) + list(coordinatesii.keys())}
        [overlap[pair].append(index) for pair, index in coordinates.items()]
        [overlap[pair].append(index) for pair, index in coordinatesii.items()]
        overlap = {pair: indices for pair, indices in overlap.items() if len(indices) > 1}

        # generate index lists
        pairs = list(overlap.keys())
        positions = [indices[0] for indices in overlap.values()]
        positionsii = [indices[1] for indices in overlap.values()]

        # generate final latitudes and longitudes
        latitude = numpy.array([pair[0] for pair in pairs])
        longitude = numpy.array([pair[1] for pair in pairs])

        # go through comparisons
        errors = {}
        original = {}
        novel = {}
        for field, fieldii in comparisons.items():

            # go through each wave
            for wave, detail, stub, box in zip(waves[field], details[field], stubs[field], boxes[field]):

                # flatten ascii data and order
                array = data[field][positions][:, wave]

                # flatten hdf5 data and order
                panel = latitudeii.shape[0]
                arrayii = hydra.grab(fieldii)

                # flatten array
                arrayii = arrayii.reshape(panel, -1)[positionsii][:, wave]

                # add to errors
                key = '{}{}'.format(field, stub)
                errors[key] = 100 * ((arrayii / array) - 1)

                # generate masks to block out infinities, etc
                mask = (numpy.isfinite(array)) & (array > 0) & (array < 1e10)
                maskii = (numpy.isfinite(arrayii)) & (arrayii > 0) & (arrayii < 1e10)
                masque = numpy.logical_and(mask, maskii)

                # # stack latitudes and longitudes
                # width = array.shape[1]
                # stack = numpy.array([latitude] * width).transpose(1, 0)
                # stackii = numpy.array([longitude] * width).transpose(1, 0)

                # apply mask
                ordinate = latitude[masque]
                abscissa = longitude[masque]
                array = array[masque]
                arrayii = arrayii[masque]

                # add to collection
                original[key] = array
                novel[key] = arrayii
                original['latitude_{}'.format(key)] = ordinate
                original['longitude_{}'.format(key)] = abscissa

                # make heatmap for ascii
                title = 'UVB-UVP, {} {}, Orbit 2573'.format(names[field], detail)
                address = 'plots/comparisons/Ascii_{}.h5'.format(key)
                parameters = (key, array, 'latitude', ordinate, 'longitude', abscissa, address)
                self.squid.shimmer(*parameters, bounds=box, title=title)

                # make heatmap for hdf5
                title = 'OMOCNUV, {} {}, Orbit 2573'.format(names[field], detail)
                address = 'plots/comparisons/HDF5_{}.h5'.format(key)
                parameters = (key, arrayii, 'latitude', ordinate, 'longitude', abscissa, address)
                self.squid.shimmer(*parameters, bounds=box, title=title)

                # make heatmap for difference
                title = 'OMOCNUV vs UVB-UVP, Percent Error {} {}, Orbit 2573'.format(names[field], detail)
                bounds = [-10, -1, -0.1, -0.01, 0.01, 0.1, 1, 10]
                error = (100 * ((arrayii / array) - 1))
                address = 'plots/comparisons/Errors_{}.h5'.format(key)
                parameters = ('{}_error'.format(key), error, 'latitude', ordinate, 'longitude', abscissa, address)
                self.squid.shimmer(*parameters, bounds=bounds, title=title)

                # also make histogram of original distribution
                title = 'OMOCNUV vs UVB-UVP, Percent Error {} {}, Orbit 2573'.format(names[field], detail)
                address = 'plots/histograms/Histogram_Err_{}.h5'.format(key)
                self.squid.ripple('{}_error'.format(key), error, address, title)

                # also make histogram of omocned distribution
                title = 'UVB-UVP, {} {}, Orbit 2573'.format(names[field], detail)
                address = 'plots/histograms/UVBUVP_{}.h5'.format(key)
                self.squid.ripple('{}'.format(key), array, address, title)

                # also make histogram of errors
                title = 'OMOCNUV, {} {}, Orbit 2573'.format(names[field], detail)
                address = 'plots/histograms/OMOCNUV_{}.h5'.format(key)
                self.squid.ripple('{}'.format(key), arrayii, address, title)

        # create results
        results = {'original': original, 'novel': novel, 'errors': errors}

        return results

    def glitter(self):
        """Check the new output as a heatmap.

        Arguments:
            None
        """

        # grab file
        hydra = Hydra('../studies/diatom/erythema'.format(self.sink))

        # grab latitude and erythema
        hydra.ingest(0)
        erythema = hydra.grab('erythema_scalar')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        chlorophyll = hydra.grab('chlorophyll')

        # make mask for latitude
        mask = (latitude >= -90) & (latitude <= 90)
        maskii = (longitude >= -180) & (longitude <= 180)
        maskiii = (erythema > 0) & (erythema <= 600)
        maskiv = (chlorophyll > 0)
        masque = numpy.logical_and(mask, maskii)
        masque = numpy.logical_and(masque, maskiii)
        masque = numpy.logical_and(masque, maskiv)

        # apply mask
        erythema = erythema[masque]
        latitude = latitude[masque]
        longitude = longitude[masque]

        print(erythema.shape)
        print(latitude.shape)
        print(longitude.shape)

        # make heatmap
        title = 'Erythemal Dose, new Output'
        bounds = [0, 100, 200, 300, 400, 500, 600]
        #bounds = [0, 40, 80, 120, 160, 200]
        address = 'plots/erythema/New_output_Erythemal_Dose.h5'
        parameters = ('latitude', latitude, 'longitude', longitude, address, bounds, title)
        self.squid.shimmer('erythema_neo', erythema, *parameters)

        return erythema, latitude, longitude

    def granulate(self):
        """Locate the location of pace simulated granules.

        Arguments:
            None

        Returns:
            None
        """

        self._print('starting granulate...')

        # make directory
        self._make('{}/plots/granules'.format(self.sink))

        # create hydra
        hydra = Hydra('../studies/diatom/simulation/ten')

        # for each input model pathpath
        paths = [path for path in hydra.paths if 'MODEL_INPUTS.V10' in path]
        for path in paths:

            # ingest
            hydra.ingest(path)

            # get latitude and longitude
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')
            tracer = hydra.grab('ozone')

            # limit to first and last row
            edges = [0, 1, 2, 3, 4, -5, -4, -3, -2, -1]
            edgesii = [0, 1, -2, -1]
            latitude = latitude[edges][edgesii]
            longitude = longitude[edges][edgesii]
            tracer = tracer[edges][edgesii]

            # get date
            date = re.search("[0-9, A-Z]{15}", path).group()

            # setup pace plot, zoom
            title = 'Simulated PACE granule\n {}'.format(date)
            destination = '{}/plots/granules/PACE_SIM_granule_{}.png'.format(self.sink, date)
            limits = (-90, 90, -180, 180)
            size = (10, 7)
            unit = '_'
            scale = (tracer.min(), tracer.max())
            resolution = 1

            # plot
            self._stamp('plotting {}...'.format(destination))
            parameters = [tracer, latitude, longitude, destination, title, unit]
            parameters += [scale, limits, resolution, size]
            self._paint(*parameters)

        return None

    def gyrate(self, tag='07640', source='../studies/diatom/gyre/gyre.txt', degrees=-20, wave=305):
        """Plot the gyre paper values compared with omocned measurements.

        Arguments:
            tag: str, tag of ocean data
            source: source for table of gyre kd values
            degrees: latitude degrees
            waves: wavelength to check

        Returns:
            None
        """

        # grab the ocean dat
        ocean = self._pull(tag)

        # grab the text
        text = self._know(source)

        # zip line together in pairs, and keep only every other
        pairs = zip(text[:-1], text[1:])
        pairs = [pair for index, pair in enumerate(pairs) if index % 2 == 0]

        # make data
        data = {str(first).strip(): [float(entry) for entry in second.split()] for first, second in pairs}
        data = {field: numpy.array(array) for field, array in data.items()}

        # get fields from ocean data
        latitude = ocean.grab('latitude')
        longitude = ocean.grab('longitude')
        constant = ocean.grab('constant_planar')
        wavelength = ocean.grab('wavelength')

        # set longitudes
        longitudes = [-140, -130, -120, -110, -100, -90, -80, -70]
        constants = []
        for meridian in longitudes:

            # get the closest pixel to -20 latitude
            pixel = self._pin([degrees, meridian], [latitude, longitude])[0]

            # get the wavelength index
            index = wavelength.tolist().index(wave)

            # get the kd value at the pixel
            attenuation = constant[pixel[0], pixel[1], index]
            constants.append(attenuation)

        # plot OMOCNUV data
        address = 'plots/gyre/Gyre_OMOCNUV_Kds.h5'
        title = 'Kd values comparison, 305 nm, South Pacific Gyre'
        self.squid.ink('kd_305', constants, 'longitude', longitudes, address, title)

        # plot papers data
        address = 'plots/gyre/Gyre_Papers_Kds.h5'
        title = 'Kd values comparison, 305 nm, South Pacific Gyre'
        self.squid.ink('kd_305', data['305 nm'], 'longitude', data['Longitude'], address, title)

        # plot locations
        address = 'plots/gyre/Gyre_Locations.h5'
        title = 'Gyre Locations'
        self.squid.ink('latitude', data['Latitude'], 'longitude', data['Longitude'], address, title)

        # plot GYRE location
        address = 'plots/gyre/Gyre_Locations_center.h5'
        title = 'Gyre Locations'
        self.squid.ink('latitude', data['Latitude'][-6:-7], 'longitude', data['Longitude'][-6:-7], address, title)

        return None

    def imitate(self, *words, tag='panama', resolution=100, wave=305, modes=[0, 1, 2], sigmas=3, zoom=None, bar=False):
        """Create plots from simulation results.

        Argumnents:
            words: upnpacked list of str, the plots to make
            tag: str, keyword of simulation result
            resolution: int, number of vertical polygons to meld
            wave: int, wavelength
            modes: list of int or str, the particular plotting modes ( 'full', 'pace', 'omi' )
            sigmas: number of stdevs for plotting
            zoom: tuple of floats, the latitude and longitude bounds
            bar: add colorbar?

        Returns:
            None
        """

        # set fill
        fill = -999

        # make folder based on tag
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/{}'.format(self.sink, tag))

        # create omi hydra for comparison data
        comparison = Hydra('../studies/diatom/simulation/comparison')
        comparison.ingest(0)

        # grab spectral wavelength
        wavelength = comparison.grab('spectral_wavelength')
        position = comparison._pin(wave, wavelength)[0][0]

        # create simulation hydra
        simulation = Hydra('../studies/diatom/simulation/comparison')
        simulation.ingest(tag)
        self._print('sim file: {}'.format(simulation.current))

        # get the DNA Spectrum
        spectra = Hydra('../studies/diatom/simulation/spectra')
        spectra.ingest(1)
        wavelength = spectra.grab('wavelength')
        spectrum = spectra.grab('spectrum')
        spectrum = spectrum.reshape(1, 1, -1)

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure'})

        # create stubs
        stubs = {'sky': 'clear sky irradiance, {}nm'.format(wave), 'cloud': 'cloud optical depth, 360nm'}
        stubs.update({'residue': 'residue 331', 'ozone': 'total column ozone'})
        stubs.update({'coefficient': 'planar diffuse coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'chlorophyll'})
        stubs.update({'surface': 'planar surface irradiance, {}nm'.format(wave), 'reflectance': 'n value 360'})
        stubs.update({'reflectivity': 'reflectivity, 360nm', 'anomaly': 'row_anomaly'})
        stubs.update({'ultraviolet': 'ultraviolet index'})
        stubs.update({'underwater': 'underwater irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': 'penetration depth, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA damage dose rate at 5m depth'})
        stubs.update({'pressure': 'Surface pressure'})

        # create uits
        units = {'sky': '$mW / m^2 s$', 'cloud': '-', 'residue': '-', 'ozone': 'DU'}
        units.update({'coefficient': '-', 'chlorophyll': '$mg / m^3$'})
        units.update({'surface': '$mW / m^2 s$', 'reflectance': '-', 'reflectivity': '-'})
        units.update({'ultraviolet': 'UVI', 'underwater': '$mW / m^2 s$'})
        units.update({'depth': 'm', 'damage': '$mW / m^2 s$', 'pressure': 'atm'})

        # create positions for third index of 3-d data
        positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2, 'reflectivity': 1}
        positions.update({'underwater': position, 'depth': position})

        # declare color scale ranges
        scales = {'sky': (0, 120), 'cloud': (0, 40)}
        scales.update({'residue': (-10, 10), 'ozone': (230, 330)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0, 4)})
        scales.update({'surface': (0, 120), 'reflectance': (50, 150)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 15)})
        scales.update({'underwater': (0, 120)})
        scales.update({'depth': (0, 35)})
        scales.update({'damage': (0, 200)})
        scales.update({'pressure': (0.96, 1.04)})

        # set fields for logarithmic scale
        # logarithms = {'coefficient': True, 'chlorophyll': True}
        logarithms = {}

        # set up transforms to convert from logs and replace fill values
        def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        transforms.update({'coefficient': powering})

        # collect originnal data and apply transformts
        data = {field: simulation.grab(search) for field, search in fields.items()}
        data.update({field: transform(data[field]) for field, transform in transforms.items()})
        dataii = {field: comparison.grab(search) for field, search in fields.items()}
        dataii.update({field: transform(dataii[field]) for field, transform in transforms.items()})

        # calculate dna damage at 5m
        underwater = data['underwater']
        coefficient = data['coefficient']
        irradiance = underwater * numpy.exp(-coefficient * 5)
        damage = (irradiance * spectrum).sum(axis=2)
        data['damage'] = damage

        # calculate dna damage at 5m, omi
        underwater = dataii['underwater']
        coefficient = dataii['coefficient']
        irradiance = underwater * numpy.exp(-coefficient * 5)
        damage = (irradiance * spectrum).sum(axis=2)
        dataii['damage'] = damage

        # calculate penetration depths
        data['depth'] = -1 * numpy.log(0.1) / data['coefficient']
        dataii['depth'] = -1 * numpy.log(0.1) / dataii['coefficient']

        # calculate underwater at 5 meters
        data['underwater'] = data['underwater'] * numpy.exp(-5 * data['coefficient'])
        dataii['underwater'] = dataii['underwater'] * numpy.exp(-5 * dataii['coefficient'])

        # subset positions
        data.update({field: data[field][:, :, position] for field, position in positions.items()})
        dataii.update({field: dataii[field][:, :, position] for field, position in positions.items()})

        # create mask based on simulation bounds, with a margin
        bounds = (data['latitude'].min(), data['latitude'].max())
        boundsii = (data['longitude'].min(), data['longitude'].max())
        margin = 2
        marginii = 2

        # create mask for original data
        means = data['latitude'].mean(axis=1)
        meansii = data['longitude'].mean(axis=0)
        south = (means < bounds[0]).tolist().index(False)
        north = ((means > bounds[1]).tolist() + [True]).index(True)
        west = (meansii < boundsii[0]).tolist().index(False)
        east = ((meansii > boundsii[1]).tolist() + [True]).index(True)

        # apply bounds
        dataii = {field: array[south:north, west:east] for field, array in dataii.items()}

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # change scale to plasma
        gradient = 'plasma'
        selection = [index for index in range(256)]

        # for each field
        words = words or list(stubs.keys())
        for field in words:

            # except for exclusions
            exclusions = ['latitude', 'longitude', 'anomaly']
            if field not in exclusions:

                # set pace components
                tracer = data[field]
                latitude = data['latitude']
                longitude = data['longitude']
                anomaly = data['anomaly']

                # set omi components
                tracerii = dataii[field]
                latitudeii = dataii['latitude']
                longitudeii = dataii['longitude']
                anomalyii = dataii['anomaly']

                # find pace middle latitude min and max
                half = int(latitude.shape[1] / 2)
                lower = latitude[0, half]
                upper = latitude[-1, half]

                # find equivalent indices for omi
                halfii = int(latitudeii.shape[1] / 2)
                first = (lower < latitudeii)[:, halfii].tolist().index(True)
                last = (latitudeii > upper)[:, halfii].tolist().index(True)

                # subset omi data
                tracerii = tracerii[first: last]
                latitudeii = latitudeii[first: last]
                longitudeii = longitudeii[first: last]
                anomalyii = anomalyii[first: last]

                # create masks for finite data
                mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (anomaly < 1) & (tracer > -999)
                maskii = (numpy.isfinite(tracerii)) & (abs(tracerii) < 1e15) & (anomalyii < 1) & (tracerii > -999)

                # get mean and stdev of pace data
                total = tracerii[maskii]
                mean = total.mean()
                deviation = total.std()
                minimum = mean - sigmas * deviation
                minimum = max([minimum, total.min()])
                maximum = mean + sigmas * deviation

                # set plot scale
                # minimum = min(tracer[mask].flatten().tolist() + tracerii[maskii].flatten().tolist())
                # maximum = max(tracer[mask].flatten().tolist() + tracerii[maskii].flatten().tolist())
                # minimum = tracer[mask].min() * 0.5
                # maximum = tracer[mask].max() * 1.5
                scale = scales[field]

                self._print('scale: {}'.format(scale))

                # set units
                unit = units[field]

                print(tracer.max(), tracerii.max())

                # clip at upper bounds
                # tracer = numpy.where(tracer < scale[0], scale[0], tracer)
                tracer = numpy.where(tracer > scale[1] * 0.99, scale[1] * 0.99, tracer)
                tracerii = numpy.where(tracerii > scale[1] * 0.99, scale[1] * 0.99, tracerii)

                print(tracer.max(), tracerii.max())

                # replace masked values with fills
                latitude = numpy.where(mask, latitude, fill)
                longitude = numpy.where(mask, longitude, fill)
                tracer = numpy.where(mask, tracer, fill)
                latitudeii = numpy.where(maskii, latitudeii, fill)
                longitudeii = numpy.where(maskii, longitudeii, fill)
                tracerii = numpy.where(maskii, tracerii, fill)

                # if mode o
                if any(entry in modes for entry in ('full', 0)):

                    # setup pace plot, full scale
                    title = 'PACE {} (simulated), 2022-03-21'.format(stubs[field])
                    title = ''
                    destination = '{}/plots/{}/PACE_SIM_{}_{}_full.png'.format(self.sink, tag, field, tag)
                    limits = (-90, 90, -180, 180)
                    size = (10, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'

                    # plot
                    self._stamp('plotting {}...'.format(destination), initial=True)
                    parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'max'}
                    self._paint(*parameters, **options)

                # if mode 1
                if any(entry in modes for entry in ('pace', 1)):

                    # setup pace plot, zoom
                    title = 'PACE {} (simulated), 2022-03-21'.format(stubs[field])
                    title = ''
                    destination = '{}/plots/{}/PACE_SIM_{}_{}.png'.format(self.sink, tag, field, tag)
                    limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                    size = (5, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'
                    limits = zoom or limits

                    # plot
                    self._stamp('plotting {}...'.format(destination))
                    parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'max'}
                    self._paint(*parameters, **options)

                # if mode 2
                if any(entry in modes for entry in ('omi', 2)):

                    # set up omi plot, zoom
                    title = 'OMI {}, 2022-03-21'.format(stubs[field])
                    title = ''
                    destination = '{}/plots/{}/OMI_{}_{}.png'.format(self.sink, tag, field, tag)
                    limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                    size = (5, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'
                    limits = zoom or limits

                    # plot
                    self._stamp('plotting {}...'.format(destination))
                    parameters = [[tracerii, latitudeii, longitudeii], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'max'}
                    self._paint(*parameters, **options)

                # print status
                self._stamp('plotted.'.format(field))

        return None

    def interpolate(self, table, datum, ticks, axis):
        """Interpolate a given table across a datum.

        Arguments:
            table: numpy array
            datum: float
            ticks: list of floats
            axis: int, axis for interpolation

        Returns:
            numpy array
        """

        # find right bounds
        right = numpy.array(ticks) > datum
        right = right.tolist().index(True)
        left = right - 1

        # create interplation bracket
        bracket = numpy.swapaxes(table, axis, 0)
        bracket = bracket[left: right + 1]

        # interpolate
        factor = (datum - ticks[left]) / (ticks[right] - ticks[left])
        interpolation = bracket[0] + factor * (bracket[1] - bracket[0])

        # reswap axes
        interpolation = numpy.array([interpolation])
        interpolation = numpy.swapaxes(interpolation, axis, 0).squeeze()

        return interpolation

    def irradiate(self, folder='statics'):
        """Plot the irradiance data, in particular looking for 0's.

        Arguments:
            None

        Returns:
            None
        """

        # grab the irradiance file
        hydra = Hydra('../studies/diatom/{}'.format(folder))
        hydra.ingest(0)

        # for each band
        for band in ('BAND2', 'BAND3'):

            # grab the irradiance and wavelengths
            irradiance = hydra.grab('{}/irradiance'.format(band)).squeeze()
            wavelength = hydra.grab('{}/wavelength'.format(band)).squeeze()
            quality = hydra.grab('{}/spectral_channel_quality'.format(band)).squeeze()

            # construct rows
            rows = numpy.array([range(60)] * irradiance.shape[1]).transpose(1, 0)

            # set bounds, and make heatmap of irradiance
            bounds = [0, 1e-9, 1e-7, 1e-5, 1e-3, 1e-1, 10]
            address = 'plots/irradiance/Irradiance_zeros_{}.h5'.format(band)
            title = 'Irradiance Zeros by Row and Wavelength, {}'.format(band)
            self.squid.shimmer('irradiance', irradiance, 'row', rows, 'wavelength', wavelength, address, bounds, title)

            # set bounds, and make heatmap of quality
            bounds = [-0.5, 0.5, 1.5, 7.5, 8.5, 15.5, 16.5]
            address = 'plots/irradiance/Quality_zeros_{}.h5'.format(band)
            title = 'Spectral Channel Quality by Row and Wavelength, {}'.format(band)
            self.squid.shimmer('quality', quality, 'row', rows, 'wavelength', wavelength, address, bounds, title)

            # plot irradiance across waveelngths
            for wave in (360,):

                # get irradiance across wavelength
                pixels = []
                for row in range(irradiance.shape[0]):

                    # find closest pixel
                    pixel = ((wavelength[row, :] - wave) ** 2).argsort()[0]
                    pixels.append(irradiance[row, pixel])

                # create plot
                address = 'plots/irradiance/Irradiance_Wavelength_{}_{}.h5'.format(band, wave)
                title = '{} Irradiance by Row, Wavelength {}nm'.format(band, wave)
                self.squid.ink('irradiance', pixels, 'row', rows[:, 0], address, title)

        return None

    def juxtapose(self, data=None, tag=None):
        """Compare the two sets of inputs create difference maps.

        Arguments:
            data: data instance

        Returns:
            None
        """

        # get the ascii data
        data = data or self.mobilize()

        # load in the hdf5 data
        hydra = Hydra('../studies/diatom/erythema')

        # define path
        path = '{}/OMOCNUV_Erythema_02573.h5'.format(hydra.source)
        if tag:

            # add tag
            path = path.replace('.h5', '_{}.h5'.format(tag))

        # grab the data
        hydra.ingest(path)

        # round latitudes and longitudes
        latitudes = numpy.round(data['lat'], 2).flatten()
        longitudes = numpy.round(data['lon'], 2).flatten()
        latitudesii = numpy.round(hydra.grab('latitude'), 2)
        longitudesii = numpy.round(hydra.grab('longitude'), 2)

        # create indices to project ascii onto hdf5 map
        fill = -9999
        shape = latitudesii.shape
        indices = numpy.array(range(latitudes.shape[0]))
        grid = []

        # create grid pairs
        points = [(image, row) for image in range(shape[0]) for row in range(shape[1])]
        pairs = {(latitudesii[image, row], longitudesii[image, row]): (image, row) for image, row in points}

        # for each asscii point
        for index, latitude, longitude in zip(indices, latitudes, longitudes):

            # try to
            try:

                # retrieve the indices
                image, row = pairs[(latitude, longitude)]

                # add to grid
                grid.append((image, row))

            # unless not found
            except KeyError:

                # in which case skip
                grid.append((0, 0))

        # map feature to grid
        grids = {}
        for field, array in data.items():

            # populate array
            matrix = numpy.ones(shape) * fill
            for index, quantity in enumerate(array.flatten()):

                # try to
                try:

                    # populate
                    image, row = grid[index]
                    matrix[image, row] = quantity

                # unless axis error or keyerror
                except (IndexError,):

                    # skip
                    pass

            # add to grid
            print(field)
            grids[field] = matrix

        # begin features
        features = []

        # create feature for latitude
        name = 'latitude'
        array = latitudesii[:, 30]
        mask = (latitude != -9999)
        feature = Feature(['IndependentVariables', name], array[mask].squeeze())
        features.append(feature)

        # for each field
        waves = ('n331', 'n340', 'n360', 'n380')
        for position, wave in enumerate(waves):

            # create feature for last ascii nvalue
            name = 'ascii_{}'.format(wave)
            array = grids[wave]
            feature = Feature(['Categories', name], array[mask].squeeze())
            features.append(feature)

            # create feature for last hdf5 nvalue
            name = 'hdf5_{}'.format(wave)
            arrayii = hydra.grab('n_values')[:, :, position]
            feature = Feature(['Categories', name], arrayii[mask].squeeze())
            features.append(feature)

            # also plot difference
            name = 'difference_{}'.format(wave)
            difference = arrayii[mask].squeeze() - array[mask].squeeze()
            feature = Feature(['Categories', name], difference)
            features.append(feature)

            # also plot error
            name = 'error_{}'.format(wave)
            error = 100 * ((arrayii[mask].squeeze() / array[mask].squeeze()) - 1)
            feature = Feature(['Categories', name], error)
            features.append(feature)

        # set up field pairs
        fields = {'ozone': 'ozone', 'chl': 'chlorophyll', 'pteran': 'terrain_pressure', 'sza': 'solar_zenith_angle'}
        fields.update({'phi': 'relative_azimuth', 'satza': 'satellite_zenith_angle'})
        fields.update({'kd490': 'attenuation_constant_kd490', 'algflg': 'algorithm_flag'})

        # for each field
        original = {}
        novel = {}
        for field, fieldii in fields.items():

            # create feature for last ascii nvalue
            name = 'ascii_{}'.format(field)
            array = grids[field]
            feature = Feature(['Categories', name], array[mask].squeeze())
            features.append(feature)

            # create feature for last hdf5 nvalue
            name = 'hdf5_{}'.format(field)
            arrayii = hydra.grab(fieldii)
            feature = Feature(['Categories', name], arrayii[mask].squeeze())
            features.append(feature)

            # add to reservoirs
            original[field] = array.copy()
            novel[fieldii] = arrayii.copy()

            # also plot difference
            name = 'difference_{}'.format(field)
            difference = arrayii[mask].squeeze() - array[mask].squeeze()
            feature = Feature(['Categories', name], difference)
            features.append(feature)

            # also plot error
            name = 'error_{}'.format(field)
            error = 100 * ((arrayii[mask].squeeze() / array[mask].squeeze()) - 1)
            feature = Feature(['Categories', name], error)
            features.append(feature)

        # make destination
        destination = '{}/plots/inputs/Input_nvalues.h5'.format(self.sink)
        if tag:

            # add tag to destination
            destination = '{}/plots/inputs/Input_nvalues_{}.h5'.format(self.sink, tag)

        # stash
        self.stash(features, destination)

        # create results
        results = {'grid': grids, 'novel': novel, 'original': original}

        return results

    def lamb(self, year=2005, month=3, day=21, orbit=3630, margin=1, resolution=50, epsilon=1e-4, archive=70004):
        """Examine lambertian glers from OMGLER.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: int, orbit number
            margin: percentile margin for plots
            resolution: int, resolution
            epsilon: epsilon value for comparing floats

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/lambertian'.format(self.sink))

        # create hydras
        formats = (archive, str(year), self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMGLER/{}/{}/{}'.format(*formats))

        # ingest
        orbit = str(orbit)
        hydra.ingest(str(orbit))

        # get date
        date = hydra._stage(hydra.current)['day']

        # set fill
        fill = -9999

        # set positions
        positions = {70004: 2, 10003: 0}
        position = positions[archive]

        # get data
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')
        # ocean = hydra.grab('OceanLER')[:, :, 2] * 100
        # land = hydra.grab('LandLER')[:, :, 2] * 100
        lambertian = hydra.grab('GLER')[:, :, position]

        # also get miimum ler climatology
        hydraii = Hydra('{}/ultraviolet'.format(self.sink))
        hydraii.ingest('minler')
        climatology = hydraii.grab('minimum_reflectivity') / 100

        # create difference
        difference = climatology - lambertian

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-60, 60, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [lambertian, climatology, difference]
        stubs = ['GLER_440nm_AS{}'.format(archive), 'MinLER_Climatology', 'Difference']

        # determine mask for latitude longitude limits
        mask = (latitude > limits[0] - 1) & (latitude < limits[1] + 1)
        mask = mask & (longitude > limits[2] - 1) & (longitude < limits[3] + 1)

        # apply mask
        panels = [numpy.where(mask, panel, fill) for panel in panels]

        # create mask
        mask = (abs(panels[0]) < 1e20) & (panels[0] > -100) & (numpy.isfinite(panels[0]))
        maskii = (abs(panels[1]) < 1e20) & (panels[1] > -100) & (numpy.isfinite(panels[1]))
        masque = mask & maskii

        # construct percentile bounds
        lower = margin
        upper = 100 - margin

        print('masque: {}'.format(masque.sum()))

        # get main scales
        minimum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), lower)
        maximum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), upper)
        scale = (minimum, maximum)
        scales = [scale, scale]

        # get min max diffs
        low = abs(numpy.percentile(difference[masque], lower))
        high = abs(numpy.percentile(difference[masque], upper))
        magnitude = max([low, high])
        scales += [(-magnitude, magnitude)]

        # set default scales
        scales = [(0.02, 0.12), (0.02, 0.12), (-0.08, 0.08)]
        print(scales)

        # for each set
        for panel, stub, scale in zip(panels, stubs, scales):

            # # create mask
            # mask = (abs(panel) < 1e20) & (panel > -100) & (numpy.isfinite(panel))
            # masque = mask
            #
            # # construct percentile bounds
            # lower = margin
            # upper = 100 - margin
            #
            # print('masque: {}'.format(masque.sum()))

            # get main scales
            # minimum = numpy.percentile(panel[masque], lower)
            # maximum = numpy.percentile(panel[masque], upper)
            # minimum = panel[masque].min()
            # maximum = panel[masque].max()
            # scale = (minimum, maximum)

            # apply masques
            panel = numpy.where(masque, panel, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

            # set formats
            formats = (stub, date, orbit, archive)

            # plot OMGLER
            title = '{}, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_{}_{}_{}.png'.format(self.sink, *formats)
            unit = '-'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'white']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def lament(self, tag='minler', tagii='gler', resolution=50, margin=1):
        """Compare the ultraviolet surface ultraviolet irradiance of minler climatology vs OMGLER.

        Arguments:
            tag: str, tag of first file
            tagii: str, tag of second file
            resolution: int, resolution
            margin: float, size of percentile margins

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/lambertian'.format(self.sink))

        # create hydras
        hydra = Hydra('{}/ultraviolet'.format(self.sink))
        hydraii = Hydra('{}/ultraviolet'.format(self.sink))

        # ingest
        hydra.ingest(tag)
        hydraii.ingest(tagii)

        # get orbit and date
        date = hydraii._stage(hydraii.current)['day']
        orbit = hydraii._stage(hydraii.current)['orbit']

        # set fill
        fill = -9999

        # get first data
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        lambertian = hydra.grab('minimum_reflectivity')
        sky = hydra.grab('clear_sky')[:, :, 15]
        surface = hydra.grab('surface_irradiance_planar')[:, :, 15]

        # get omocnuv data
        lambertianii = hydraii.grab('minimum_reflectivity')
        skyii = hydraii.grab('clear_sky')[:, :, 15]
        surfaceii = hydraii.grab('surface_irradiance_planar')[:, :, 15]

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-60, 60, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [lambertian, sky, surface]
        panelsii = [lambertianii, skyii, surfaceii]
        stubs = ['LER', 'Clear_Sky_305nm', 'Surface_Planar_UV_305nm']

        # determine mask for latitude longitude limits
        mask = (latitude > limits[0] - 1) & (latitude < limits[1] + 1)
        mask = mask & (longitude > limits[2] - 1) & (longitude < limits[3] + 1)

        # apply mask
        panels = [numpy.where(mask, panel, fill) for panel in panels]
        panelsii = [numpy.where(mask, panel, fill) for panel in panelsii]

        # for each set
        for panel, panelii, stub in zip(panels, panelsii, stubs):

            # create difference
            difference = self._relate(panel, panelii)
            percent = self._relate(panel, panelii, percent=True)

            # create mask
            mask = (abs(panel) < 1e20) & (panel > 0) & (numpy.isfinite(panel))
            maskii = (abs(panelii) < 1e20) & (panelii > 0) & (numpy.isfinite(panelii))
            masque = numpy.logical_and(mask, maskii)

            # get main scales
            lower = margin
            upper = 100 - margin
            minimum = min([numpy.percentile(panel[masque], lower), numpy.percentile(panelii[masque], lower)])
            maximum = max([numpy.percentile(panel[masque], upper), numpy.percentile(panelii[masque], upper)])
            scale = (minimum, maximum)

            # get difference scale
            minimum = abs(numpy.percentile(difference[masque], lower))
            maximum = abs(numpy.percentile(difference[masque], upper))
            magnitude = max([minimum, maximum])
            scaleii = (-magnitude, magnitude)

            # get percent scale
            minimum = abs(numpy.percentile(percent[masque], lower))
            maximum = abs(numpy.percentile(percent[masque], upper))
            magnitude = max([minimum, maximum])
            scaleiii = (-magnitude, magnitude)

            # set epsilon
            epsilon = 1e-4

            # apply masques
            panel = numpy.where(masque, panel, fill)
            panelii = numpy.where(masque, panelii, fill)
            difference = numpy.where(masque, difference, fill)
            percent = numpy.where(masque, percent, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)
            panelii = numpy.where(panelii <= scale[0], scale[0] + epsilon, panelii)
            panelii = numpy.where(panelii >= scale[1], scale[1] - epsilon, panelii)
            difference = numpy.where(difference <= scaleii[0], scaleii[0] + epsilon, difference)
            difference = numpy.where(difference >= scaleii[1], scaleii[1] - epsilon, difference)
            percent = numpy.where(percent <= scaleiii[0], scaleiii[0] + epsilon, percent)
            percent = numpy.where(percent >= scaleiii[1], scaleiii[1] - epsilon, percent)

            # set formats
            formats = (stub, date, orbit)

            # plot OMUVB
            title = '{}, MinLER Climatology, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_Climatology_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot OMOCNUV
            title = '{}, OceanGLER_354nm, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_GLER_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panelii, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot difference
            title = '{}, OceanGLER_354 - Climatology, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_Difference_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[difference, latitude, longitude], destination, [title, unit]]
            parameters += [scaleii, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot percent
            title = '{}, % OceanGLER_354 - Climatology, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_Percent_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[percent, latitude, longitude], destination, [title, unit]]
            parameters += [scaleiii, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def laminate(self, year=2005, month=3, day=21, orbit=3630, margin=1, resolution=50, epsilon=1e-4):
        """Examine lambertian glers from OMGLER.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: int, orbit number
            margin: percentile margin for plots
            resolution: int, resolution
            epsilon: epsilon value for comparing floats

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/lambertian'.format(self.sink))

        # create hydras
        formats = (str(year), self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/70004/OMGLER/{}/{}/{}'.format(*formats))

        # ingest
        orbit = str(orbit)
        hydra.ingest(str(orbit))

        # get date
        date = hydra._stage(hydra.current)['day']

        # set fill
        fill = -9999

        # get data
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')
        ocean = hydra.grab('OceanLER')[:, :, 2]
        land = hydra.grab('LandLER')[:, :, 2]
        lambertian = hydra.grab('GLER')[:, :, 2]

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-80, 80, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [ocean, lambertian, land]
        stubs = ['Ocean_LER', 'GLER', 'Land_LER']

        # for each set
        for panel, stub in zip(panels, stubs):

            # create mask
            mask = (abs(panel) < 1e20) & (panel > -100) & (numpy.isfinite(panel))
            masque = mask

            # construct percentile bounds
            lower = margin
            upper = 100 - margin

            print('masque: {}'.format(masque.sum()))

            # get main scales
            # minimum = numpy.percentile(panel[masque], lower)
            # maximum = numpy.percentile(panel[masque], upper)
            minimum = panel[masque].min()
            maximum = panel[masque].max()
            scale = (minimum, maximum)

            # apply masques
            panel = numpy.where(masque, panel, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

            # set formats
            formats = (stub, date, orbit)

            # plot OMUVB
            title = '{}, \n{}, o{}'.format(*formats)
            destination = '{}/plots/lambertian/{}_{}_{}.png'.format(self.sink, *formats)
            unit = '-'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def lattice(self, tag=None, mobile=None, synthesis=None):
        """Create the grid file with all inputs and outputs on same grid.

        Arguments:
            tag: str, tag of erythema data file
            mobile: dataset from ascii input
            synthesis: dataset from ascii output

        Returns:
            None
        """

        # get the ascii datasets
        mobile = mobile or self.mobilize()
        synthesis = synthesis or self.synthesize()['data']

        # extend asdcii inputs by 1 dimension
        inputs = {'ascii_{}'.format(name): array.reshape(-1, 1) for name, array in mobile.items()}

        # break synthesize data into separate components if necessary
        outputs = {}
        for name, array in synthesis.items():

            # for each position
            for position in range(array.shape[1]):

                # add array to outputs
                alias = 'ascii_{}_{}'.format(name, position).replace('_0', '_0' * (array.shape[1] > 1))
                outputs[alias] = array[:, position: position + 1]

        # load in the hdf5 data
        tag = tag or ''
        hydra = Hydra('../studies/diatom/erythema')
        path = '{}/OMOCNUV_Erythema_02573_{}.h5'.format(hydra.source, tag).replace('_.h5', '.h5')
        hydra.ingest(path)

        # begin constructing grid
        grids = {}

        # for each dataset
        datasets = (outputs, inputs)
        labels = ('ascii_latitude', 'ascii_lat')
        labelsii = ('ascii_longitude', 'ascii_lon')
        for data, label, labelii in zip((outputs, inputs), labels, labelsii):

            # round latitudes and longitudes from ascii
            latitudes = numpy.round(data[label], 2).flatten()
            longitudes = numpy.round(data[labelii], 2).flatten()

            # round latitudes and longitudes from hdf
            latitudesii = numpy.round(hydra.grab('latitude'), 2)
            longitudesii = numpy.round(hydra.grab('longitude'), 2)

            # create indices to project ascii onto hdf5 map
            fill = -9999
            shape = latitudesii.shape
            indices = numpy.array(range(latitudes.shape[0]))
            grid = []

            # create grid pairs
            points = [(image, row) for image in range(shape[0]) for row in range(shape[1])]
            pairs = {(latitudesii[image, row], longitudesii[image, row]): (image, row) for image, row in points}

            # for each asscii point
            for index, latitude, longitude in zip(indices, latitudes, longitudes):

                # try to
                try:

                    # retrieve the indices
                    image, row = pairs[(latitude, longitude)]

                    # add to grid
                    grid.append((image, row))

                # unless not found
                except KeyError:

                    # in which case skip, but add placeholder at 00
                    grid.append((0, 0))

            # map feature to grid
            for field, array in data.items():

                # populate array
                matrix = numpy.ones(shape) * fill
                for index, quantity in enumerate(array.flatten()):

                    # try to
                    try:

                        # populate
                        image, row = grid[index]
                        matrix[image, row] = quantity

                    # unless axis error or keyerror
                    except (IndexError,):

                        # skip
                        pass

                # add to grid
                grids[field] = matrix

        # for each hdf feature
        for feature in hydra:

            # get the data
            alias = 'omocned_{}'.format(feature.name)
            array = feature.distil()

            # if the shape matches
            shape = latitudesii.shape
            if array.shape[:len(shape)] == shape:

                # expand to three dimensions
                array = array.reshape(*shape, -1)
                for position in range(array.shape[2]):

                    # add slice as an entry, removing 0
                    aliasii = '{}_{}'.format(alias, position).replace('_0', '_0' * (array.shape[2] > 1))
                    arrayii = array[:, :, position]
                    grids[aliasii] = arrayii

        # create file
        tagii = tag or 'latest'
        destination = '{}/data/grid/Ascii_OMOCNUV_02573_Grid_{}.h5'.format(self.sink, tagii)
        hydra.create(destination, grids)

        return None

    def logitize(self):
        """Make histograms of percent differences for log coversion.

        Arguments:
            None

        Returns:
            None
        """

        # create hydras
        hydra = Hydra('../studies/diatom/simulation/optimization')
        hydraii = Hydra(hydra.source)

        # ingest
        hydra.ingest('_unopt')
        hydraii.ingest('_logfactoriiiall')

        # set fields
        fields = ['sky', 'coefficient_planar', 'coefficient_scalar']
        fields += ['irradiance_planar', 'irradiance_scalar']
        fields += ['transmittance_planar', 'transmittance_scalar']

        # for each field
        for field in fields:

            # grab the arrays
            array = hydra.grab(field)
            arrayii = 10 ** hydraii.grab(field)

            # get mask to avoid fills
            mask = (array > -999)

            # apply mask
            array = array[mask]
            arrayii = arrayii[mask]

            # calculate percent
            percent = 100 * ((arrayii / array) - 1)

            # print to scrren
            self._print(field, percent.min(), percent.max())

            # make histogram
            address = 'logarithm/Histogram_log_{}.h5'.format(field)
            title = 'Histogram of {}, log and scale vs normal'.format(field)
            self.squid.ripple(field, percent, address, title)

        return None

    def mediate(self, year=2005, month=3, day=21, orbit=3623, folder='threshold'):
        """Make a metadata comparison csv between OMOCNUV and OMUAC.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit
            folder: str, the folder name

        Returns:
            None
        """

        # get omocnuv files
        hydra = Hydra('{}/{}'.format(self.sink, folder))
        hydra.ingest(str(orbit))

        # get omuac file
        formats = (year, self._pad(month), self._pad(day))
        hydraii = Hydra('/tis/acps/OMI/10004/OMUANC/{}/{}/{}'.format(*formats))
        hydraii.ingest(str(orbit))

        # get attributes
        attributes = hydra.attribute()
        attributesii = hydraii.attribute()

        # collect all fields
        fields = list(set(attributes.keys()) | set(attributesii.keys()))
        fields.sort()

        # create full dictionaries
        ocean = {field: '???' for field in fields}
        ancillary = {field: '???' for field in fields}

        # update
        ocean.update(attributes)
        ancillary.update(attributesii)

        # construct rows
        rows = [['notes', 'field', 'OMOCNUV', 'OMUANC']]
        rows += [['', field, ocean[field], ancillary[field]] for field in fields]

        # create csv
        destination = '{}/metadata/omuanc_comparison.csv'.format(self.sink)
        self._table(rows, destination)

        return None

    def melt(self, year, month, day, sink=None, fields=None, anomaly=False):
        """Merge ocean color products for a month with select fields.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            sink: str, path to folder
            fields: dict of search name and third index ( None if none ).
            anomaly: boolean, add row anomaly?

        Returns:
            None
        """

        # create default sink
        sink = sink or '/user/1001/home/mbandel/studies/diatom/pace/days'

        # if no given fields
        if fields is None:

            # set fields
            fields = {}
            fields['latitude'] = None
            fields['longitude'] = None
            fields['chlorophyll'] = None
            fields['ozone'] = None
            fields['pressure'] = None
            fields['cloud'] = None
            fields['n_value'] = 2
            fields['reflectivity'] = 1
            fields['residue'] = None
            fields['clear_sky'] = 20
            fields['coefficient_planar'] = 20
            fields['transmittance_planar'] = 20
            fields['irradiance_planar'] = 20
            fields['time'] = None

            # if row anomaly desired
            if anomaly:

                # add to list
                fields['row_anomaly'] = None

        # create hydra
        hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/{}/{}/{}'.format(year, self._pad(month), self._pad(day)))
        paths = hydra.paths
        paths.sort()

        # begin data
        data = {}
        attributes = {}
        for path in paths:

            # display status
            self._print('ingesting {}...'.format(path))

            # ingest path
            hydra.ingest(path)

            # grab latitude at -60
            degrees = -60
            block = 1000
            latitude = hydra.grab('latitude')
            track = hydra._pin(degrees, latitude[:, 30])[0][0]

            # for each field
            for field, index in fields.items():

                # get the feature
                feature = hydra.dig(field)[0]
                name = feature.name
                array = feature.distil()[track: track + block]

                # add attributes
                attributes[name] = feature.attributes

                # if a third index
                if index:

                    # get the subset
                    array = array[:, :, index]

                # add to data
                reservoir = data.setdefault(name, [])
                reservoir.append(array)

        # construct numpy arrays from lists
        data = {name: numpy.array(datum) for name, datum in data.items()}

        # print status
        self._print('constructing file...')

        # set destination
        destination = '{}/OMOCNUV_{}m{}{}.h5'.format(sink, year, self._pad(month), self._pad(day))

        # create file
        hydra.spawn(destination, data, attributes)

        return data

    def mimic(self, *words, tag='OCI_v2', tagii='april', resolution=100, wave=305, modes=[0, 1, 2], sigmas=3):
        """Create plots from simulation results.

        Argumnents:
            words: upnpacked list of str, the plots to make
            tag: str, keyword of simulation result
            tagii: str, folder of OMI data
            resolution: int, number of vertical polygons to meld
            wave: int, wavelength
            modes: list of int or str, the particular plotting modes ( 'two', 'pace', 'omi' )
            sigmas: number of stdevs for plotting

        Returns:
            None
        """

        # set defaults
        bar = True
        zoom = None

        # set fill, epsilon
        fill = -999
        epsilon = 1e-5

        # make folder
        folder = 'plots/comparison'
        self._make('{}/plots'.format(self.sink))
        self._make('{}/{}'.format(self.sink, folder))

        # create pace data hydra
        granule = Hydra('{}/output'.format(self.sink))
        granule.ingest(tag)
        self._print('pace file: {}'.format(granule.current))

        # grab spectral wavelength
        wavelength = granule.grab('spectral_wavelength')
        position = granule._pin(wave, wavelength)[0][0]

        # get the DNA Spectrum
        spectra = Hydra('{}/spectra'.format(self.sink))
        spectra.ingest(1)
        wavelength = spectra.grab('wavelength')
        spectrum = spectra.grab('spectrum')
        spectrum = spectrum.reshape(1, 1, -1)

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity_360', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure'})
        fields.update({'aerosol': 'aerosol_optical_depth_354'})
        fields.update({'aerosolii': 'aerosol_optical_depth_388'})

        # create stubs
        stubs = {'sky': 'clear sky irradiance, {}nm'.format(wave), 'cloud': 'cloud optical depth, 360nm'}
        stubs.update({'residue': 'residue 331', 'ozone': 'total column ozone'})
        stubs.update({'coefficient': 'planar diffuse coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'chlorophyll'})
        stubs.update({'surface': 'planar surface irradiance, {}nm'.format(wave), 'reflectance': 'n value 360'})
        stubs.update({'reflectivity': 'reflectivity, 360nm', 'anomaly': 'row_anomaly'})
        stubs.update({'ultraviolet': 'ultraviolet index'})
        stubs.update({'underwater': 'underwater irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': 'penetration depth, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA damage dose rate at 5m depth'})
        stubs.update({'pressure': 'Surface pressure'})
        stubs.update({'aerosol': 'AAOD, 354nm', 'aerosolii': 'AAOD, 388nm'})

        # add title inserts
        inserts = {'omi': {field: 'OMI' for field in stubs.keys()}, 'pace': {field: 'PACE' for field in stubs.keys()}}
        inserts['omi']['chlorophyll'] = 'OMI ( MODIS composite )'
        inserts['pace']['ozone'] = 'PACE ( OMTO3 )'
        inserts['pace']['pressure'] = 'PACE ( from height )'

        # create uits
        units = {'sky': '$mW / m^2 s$', 'cloud': '-', 'residue': '-', 'ozone': 'DU'}
        units.update({'coefficient': '1 / m', 'chlorophyll': '$mg / m^3$'})
        units.update({'surface': '$mW / m^2 s$', 'reflectance': '-', 'reflectivity': '-'})
        units.update({'ultraviolet': 'UVI', 'underwater': '$mW / m^2 s$'})
        units.update({'depth': 'm', 'damage': '$mW / m^2 s$', 'pressure': 'atm'})
        units.update({'aerosol': '-', 'aerosolii': '-'})

        # create positions for third index of 3-d data
        positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2}
        positions.update({'underwater': position, 'depth': position})

        # declare color scale ranges
        scales = {'sky': (10, 90), 'cloud': (0, 50)}
        scales.update({'residue': (-5, 5), 'ozone': (280, 420)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0, 2)})
        scales.update({'surface': (10, 90), 'reflectance': (70, 130)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 10)})
        scales.update({'underwater': (0, 50)})
        scales.update({'depth': (0, 30)})
        scales.update({'damage': (0, 120)})
        scales.update({'pressure': (0.85, 1.05)})
        scales.update({'aerosol': (0, 0.1), 'aerosolii': (0, 0.1)})

        # set color gradients
        gradients = {'sky': 'plasma', 'cloud': 'plasma', 'residue': 'plasma', 'ozone': 'plasma'}
        gradients.update({'coefficient': 'viridis', 'chlorophyll': 'viridis', 'surface': 'plasma'})
        gradients.update({'reflectance': 'plasma', 'reflectivity': 'plasma', 'chlorophyll': 'viridis'})
        gradients.update({'anomaly': 'plasma', 'ultraviolet': 'plasma', 'underwater': 'viridis'})
        gradients.update({'dapth': 'viridis', 'damage': 'viridis', 'pressure': 'plasma'})
        gradients.update({'aerosol': 'plasma', 'aerosolii': 'plasma'})

        # set fields for logarithmic scale
        # logarithms = {'coefficient': True, 'chlorophyll': True}
        logarithms = {}

        # # set up transforms to convert from logs and replace fill values
        # def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        # transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        # transforms.update({'coefficient': powering})

        # collect originnal data and apply transformts
        data = {field: granule.grab(search) for field, search in fields.items()}
        # data.update({field: transform(data[field]) for field, transform in transforms.items()})

        # create omi hydra for comparison data
        comparison = Hydra('{}/{}'.format(self.sink, tagii))
        paths = comparison.paths

        # begin data
        dataii = {field: [] for field in fields.keys()}
        for path in paths:

            # ingest
            comparison.ingest(path)

            # grab the data
            datum = {field: comparison.grab(search) for field, search in fields.items()}

            # append to data
            [dataii[field].append(datum[field]) for field in fields.keys()]

            pixel = (1018, 3)
            pixelii = (993, 59)

            print(path)
            print(datum['latitude'][pixel])
            print(datum['longitude'][pixel])
            print(datum['aerosol'][pixel])
            print(datum['latitude'][pixelii])
            print(datum['longitude'][pixelii])
            print(datum['aerosol'][pixelii])
            print('')

        # create arrays
        dataii = {field: numpy.vstack(arrays) for field, arrays in dataii.items()}

        # for each dataset
        for reservoir in (data, dataii):

            # calculate dna damage at 5m
            underwater = reservoir['underwater']
            coefficient = reservoir['coefficient']
            irradiance = underwater * numpy.exp(-coefficient * 5)
            damage = (irradiance * spectrum).sum(axis=2)
            reservoir['damage'] = damage

            # calculate penetration depths
            reservoir['depth'] = -1 * numpy.log(0.1) / reservoir['coefficient']

            # calculate underwater at 5 meters
            reservoir['underwater'] = reservoir['underwater'] * numpy.exp(-5 * reservoir['coefficient'])

        # subset positions
        data.update({field: data[field][:, :, position] for field, position in positions.items()})
        dataii.update({field: dataii[field][:, :, position] for field, position in positions.items()})

        # create mask based on simulation bounds, with a margin
        bounds = (data['latitude'].min(), data['latitude'].max())
        boundsii = (data['longitude'].min(), data['longitude'].max())
        margin = 2
        marginii = 2

        # create mask for original data
        means = data['latitude'].mean(axis=1)
        meansii = data['longitude'].mean(axis=0)
        south = (means < bounds[0]).tolist().index(False)
        north = ((means > bounds[1]).tolist() + [True]).index(True)
        west = (meansii < boundsii[0]).tolist().index(False)
        east = ((meansii > boundsii[1]).tolist() + [True]).index(True)

        # # apply bounds
        # dataii = {field: array[south:north, west:east] for field, array in dataii.items()}

        # construct trapezoid
        pairs = [(0, 0), (0, -1), (-1, -1), (-1, 0), (0, 0)]
        trapezoid = numpy.array([data['latitude'][pair] for pair in pairs])
        trapezoidii = numpy.array([data['longitude'][pair] for pair in pairs])
        style = 'dashed'

        # make lines
        lines = [(trapezoidii, trapezoid, style)]

        # for each field
        words = words or list(stubs.keys())
        for field in words:

            # set gradient
            gradient = gradients.get(field, 'plasma')
            selection = [index for index in range(256)]

            # except for exclusions
            exclusions = ['latitude', 'longitude', 'anomaly']
            if field not in exclusions:

                # set pace components
                tracer = data[field]
                latitude = data['latitude']
                longitude = data['longitude']
                anomaly = data['anomaly']

                # set omi components
                tracerii = dataii[field]
                latitudeii = dataii['latitude']
                longitudeii = dataii['longitude']
                anomalyii = dataii['anomaly']

                print('aerosol check:')
                print(field)
                pixels = self._pin([35, -10], [latitudeii, longitudeii])
                for pixel in pixels:
                    print(latitudeii[pixel])
                    print(longitudeii[pixel])
                    print(tracerii[pixel])
                print('done aerosol check')

                # find pace middle latitude min and max
                half = int(latitude.shape[1] / 2)
                lower = latitude[0, half]
                upper = latitude[-1, half]

                # find equivalent indices for omi
                halfii = int(latitudeii.shape[1] / 2)
                first = (lower < latitudeii)[:, halfii].tolist().index(True)
                last = (latitudeii > upper)[:, halfii].tolist().index(True)

                # # subset omi data
                # tracerii = tracerii[first: last]
                # latitudeii = latitudeii[first: last]
                # longitudeii = longitudeii[first: last]
                # anomalyii = anomalyii[first: last]

                # create masks for finite data
                mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (anomaly < 1) & (tracer > -999)
                maskii = (numpy.isfinite(tracerii)) & (abs(tracerii) < 1e15) & (anomalyii < 1) & (tracerii > -999)

                # get mean and stdev of pace data
                total = tracerii[maskii]
                mean = total.mean()
                deviation = total.std()
                minimum = mean - sigmas * deviation
                # minimum = max([minimum, total.min()])
                # maximum = mean + sigmas * deviation

                # set plot scale
                scale = scales[field]

                # set units
                unit = units[field]

                # clip at upper and lower bounds
                tracer = numpy.where((tracer < scale[0] + epsilon) & (tracer > fill), scale[0] + epsilon, tracer)
                tracerii = numpy.where((tracerii < scale[0] + epsilon) & (tracerii > fill), scale[0] + epsilon, tracerii)
                tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)
                tracerii = numpy.where(tracerii > scale[1] - epsilon, scale[1] - epsilon, tracerii)

                # replace masked values with fills
                # latitude = numpy.where(mask, latitude, fill)
                # longitude = numpy.where(mask, longitude, fill)
                tracer = numpy.where(mask, tracer, fill)
                # latitudeii = numpy.where(maskii, latitudeii, fill)
                # longitudeii = numpy.where(maskii, longitudeii, fill)
                tracerii = numpy.where(maskii, tracerii, fill)

                # if mode o
                if any(entry in modes for entry in ('two', 0)):

                    # setup pace plot, zoom
                    title = '{} v2 {}, 2024-04-11'.format(inserts['pace'][field], stubs[field])
                    destination = '{}/{}/PACE_Prox_{}_{}.png'.format(self.sink, folder, tag, field, tag)
                    limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                    size = (5, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'
                    limits = zoom or limits

                    # plot
                    self._stamp('plotting {}...'.format(destination))
                    parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution, lines]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                    self._paint(*parameters, **options)

                # if mode 1
                if any(entry in modes for entry in ('pace', 1)):

                    # setup pace plot, zoom
                    title = '{} {}, 2024-04-11'.format(inserts['pace'][field], stubs[field])
                    destination = '{}/{}/PACE_Prox_{}_{}.png'.format(self.sink, folder, tag, field, tag)
                    limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                    size = (5, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'
                    limits = zoom or limits

                    # plot
                    self._stamp('plotting {}...'.format(destination))
                    parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution, lines]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                    self._paint(*parameters, **options)

                # if mode 2
                if any(entry in modes for entry in ('omi', 2)):

                    # set up omi plot, zoom
                    title = '{} {}, 2024-04-11'.format(inserts['omi'][field], stubs[field])
                    destination = '{}/{}/OMI_{}_{}.png'.format(self.sink, folder, tag, field, tag)
                    limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
                    size = (5, 7)
                    logarithm = logarithms.get(field, False)
                    back = 'white'
                    limits = zoom or limits

                    # plot
                    self._stamp('plotting {}...'.format(destination))
                    parameters = [[tracerii, latitudeii, longitudeii], destination, [title, unit]]
                    parameters += [scale, [gradient, selection], limits, resolution, lines]
                    options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                    self._paint(*parameters, **options)

                # print status
                self._stamp('plotted.'.format(field))

        return None

    def mobilize(self, orbit=2573, plots=None, source=None):
        """Read in a MOBY file, and convert to numpy array.

        Arguments:
            fields: fields to plot

        Returns:
            numpy array
        """

        # timestamp
        self._stamp('loading in moby data, orbit {}...'.format(orbit), initial=True)

        # read in file, substituing with source
        sink = '/'.join(self.sink.split('/')[:-1])
        source = source or '{}/input/MOBY.INPUT.5.2005-01-07.02573'.format(sink, orbit)
        lines = self._know(source)

        # first line is fields, but last four is uv data
        fields = lines[0].split()
        fields = [field.strip(',') for field in fields]

        # split lines by space
        rows = [line.split() for line in lines[1:]]

        # create arrays
        arrays = {field: [float(row[index]) for row in rows] for index, field in enumerate(fields)}

        # # load in uv data
        # arrays['uv'] = [[float(entry) for entry in row[-110:]] for row in rows]

        # convert to arrays
        arrays = {field: numpy.array(array) for field, array in arrays.items()}

        # for each field
        plots = plots or []
        for field in plots:

            # plot with matplotlib
            pyplot.clf()
            pyplot.plot(arrays[field], 'bx')
            pyplot.title('MOBY, orbit {}, {}'.format(orbit, field))
            pyplot.ylabel(field)
            # pyplot.ylim(-80, 60)
            # pyplot.yticks([-80, -40, 0, 40, 60])

            # save
            destination = '{}/plots/moby/{}.png'.format(self.sink, field)
            pyplot.savefig(destination)

        # stamp
        self._stamp('loaded.')

        return arrays

    def normalize(self, waves=(310, 390), resolution=10, degrees=(-72, 58)):
        """Map changes during the optimization process.

        Arguments:
            waves: wavelengths of choice
            resolution: number of points to meld
            degrees: float, latitude degree bracket

        Returns:
            None
        """

        # make plot directory
        sink = 'plots/normalization'
        self._make('{}/{}'.format(self.sink, sink))

        # set bounding brackets
        brackets = {}
        brackets[310] = [0, 50, 100, 150, 200, 250, 300, 350, 400]
        brackets[390] = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600]
        brackets['transmittance'] = [0.6, 0.7, 0.8, 0.9, 1.0, 1.1]
        brackets['percent'] = [-0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0]
        brackets['percentii'] = [-1.2, -1.0, -0.8, -0.6, -0.4, -0.2, 0]
        brackets['percentiii'] = [-12, -10, -8, -6, -4, -2, 0, 2, 4, 6]
        brackets['chlorophyll'] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        brackets['ozone'] = [200, 225, 250, 275, 300, 325, 350, 375, 400]

        # make reserovir to hold datasets
        reservoir = {wave: {} for wave in waves}

        # get the latitude tagged entry
        ocean = self._pull('satellite')

        # grab the latitude and longitude and bounds
        latitude = ocean.grab('latitude')
        nadir = latitude[:, 30]
        mask = (nadir > -9999)

        # make mask for ascending indices
        minimum = self._pin(float(nadir[mask].min()), latitude)[0][0]
        maximum = self._pin(float(nadir[mask].max()), latitude)[0][0]

        # subset minimum to maximum
        latitude = latitude[minimum: maximum]
        longitude = ocean.grab('longitude')[minimum: maximum]
        bounds = ocean.grab('latitude_bounds')[minimum: maximum]
        boundsii = ocean.grab('longitude_bounds')[minimum: maximum]

        # create polygons from bounds
        polygons = self._polymerize(latitude, longitude, resolution)

        # get indices based on resolution
        selection = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])

        # apply selection and mask to latitude and longitude
        latitude = latitude[selection]
        longitude = longitude[selection]

        # mask the latitude at degrees
        mask = (latitude > degrees[0]) & (latitude < degrees[1])

        # apply mask to polygons and coordinates
        polygons = polygons[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]

        # set tags and reservoirs
        tags = ['satellite', 'noon', 'linear', 'omlbirb', 'omglerpre']

        # for each wave
        for wave in waves:

            reservoir[wave] = {tag: {} for tag in tags}

            # tags
            for tag in tags:

                # get the data
                ocean = self._pull(tag)

                # get the wavelength
                wavelength = ocean.grab('wavelength')
                index = self._pin(wave, wavelength)[0][0]

                # grab the fluxes at the wavelength
                planar = ocean.grab('ultraviolet/surface/planar')[minimum: maximum, :, index]
                surface = ocean.grab('ultraviolet/surface/scalar')[minimum: maximum, :, index]
                submarine = ocean.grab('ultraviolet/underwater/scalar')[minimum: maximum, :, index]
                sky = ocean.grab('ultraviolet/clear_sky')[minimum: maximum, :, index]
                attenuation = ocean.grab('constant_planar')[minimum: maximum, :, index]
                attenuationii = ocean.grab('constant_scalar')[minimum: maximum, :, index]
                chlorophyll = ocean.grab('chlorophyll')[minimum: maximum, :]
                transmittance = ocean.grab('transmittance/planar')[minimum: maximum, :, index]
                transmittanceii = ocean.grab('transmittance/scalar')[minimum: maximum, :, index]
                water = ocean.grab('water_fraction')[minimum: maximum, :]
                ozone = ocean.grab('ozone')[minimum: maximum, :]

                # calculate five meter scalar depths
                five = submarine * transmittance * numpy.exp(-5 * attenuationii)
                fiveii = surface * transmittanceii * numpy.exp(-5 * attenuationii)

                # calculate percent difference
                percent = 100 * ((fiveii / five) - 1)

                # grab the angle at solar noon
                noon = ocean.grab('minimum_solar_zenith_angle')[minimum: maximum, :]
                zenith = ocean.grab('solar_zenith_angle')[minimum: maximum, :]

                # remove water fraction
                five = numpy.where(water < 100, -9999, five)
                fiveii = numpy.where(water < 100, -9999, fiveii)
                chlorophyll = numpy.where(water < 100, -9999, chlorophyll)
                percent = numpy.where(water < 100, -9999, percent)

                # set arrays, names, etc, apply ma
                names = ['surface_planar', 'clear_sky', 'submarine_scalar', 'submarine_scalar_transmittance']
                names += ['transmittance_planar', 'transmittance_scalar', 'chlorophyll', 'transmittance_error', 'ozone']
                names += ['surface_scalar']
                arrays = [planar, sky, five, fiveii, transmittance, transmittanceii, chlorophyll, percent, ozone]
                arrays += [surface]
                boxes = [wave] * 4 + ['transmittance'] * 2 + ['chlorophyll'] + ['percent'] + ['ozone'] + [wave]
                units = ['mW / m2nm'] * 4 + ['_'] * 2 + ['mg / L'] + [' % '] + ['DU'] + ['mW / m2nm']

                # set titles
                titles = []
                orbit = 'orbit 2573'
                date = '2005-01-07'
                titles.append('Surface ultraviolet irradiance, Es, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('Clear sky surface irradiance, Es, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('5m depth scalar irradiance, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('5m depth scalar irradiance, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('Planar transmittance, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('Scalar transmittance, {}nm, {}, {}'.format(wave, orbit, date))
                titles.append('Chlorophyll, {}, {}'.format(wave, orbit, date))
                titles.append('Percent difference, 5m scalar irradiance, ')
                titles[-1] += 'transmittance vs scaling factor, {}nm'.format(wave)
                titles.append('Ozone, {}, {}'.format(orbit, date))
                titles.append('Surface scalar irradiance, Eso, {}nm, {}, {}'.format(wave, orbit, date))

                # apply selection and masks
                arrays = [array[selection][mask] for array in arrays]

                # add to reservoir
                reservoir[tag] = {name: array for name, array in zip(names, arrays)}
                reservoir[tag].update({'latitude': latitude, 'longitude': longitude})
                reservoir[tag].update({'polygons': polygons})

                # for each set
                for name, array, box, title, unit in zip(names, arrays, boxes, titles, units):

                    # make a plot
                    address = '{}/{}_{}_{}.h5'.format(sink, name, tag, wave)
                    bracket = brackets[box]
                    texts = [name, 'latitude', 'longitude']
                    data = [array, latitude, longitude]
                    self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

            # make a plot for logarithmic vs linear interpolation
            name = 'chlorophyll_difference'
            title = 'Percent difference, 5m scalar irradiance, logarithmic vs linear'
            box = 'percent'
            unit = ' % '
            address = '{}/{}_{}.h5'.format(sink, name, wave)
            array = 100 * ((reservoir['noon']['submarine_scalar'] / reservoir['linear']['submarine_scalar']) - 1)
            bracket = brackets[box]
            texts = [name, 'latitude', 'longitude']
            data = [array, latitude, longitude]
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

            # make a plot for oml1birr vs oml1birb
            name = 'submarine_flux_difference'
            title = 'Percent difference, 5m scalar irradiance, OML1BIRR vs OML1BIRB'
            box = 'percentii'
            unit = ' % '
            address = '{}/{}_{}.h5'.format(sink, name, wave)
            array = 100 * ((reservoir['omlbirb']['submarine_scalar'] / reservoir['linear']['submarine_scalar']) - 1)
            bracket = brackets[box]
            texts = [name, 'latitude', 'longitude']
            data = [array, latitude, longitude]
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

            # make a plot for oml1birr vs oml1birb
            name = 'clear_sky_flux_difference'
            title = 'Percent difference, clear sky irradiance, OML1BIRR vs OML1BIRB'
            box = 'percentii'
            unit = ' % '
            address = '{}/{}_{}.h5'.format(sink, name, wave)
            array = 100 * ((reservoir['omlbirb']['clear_sky'] / reservoir['linear']['clear_sky']) - 1)
            bracket = brackets[box]
            texts = [name, 'latitude', 'longitude']
            data = [array, latitude, longitude]
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

            # make a plot for oml1birr vs oml1birb
            name = 'submarine_flux_difference_omglerpre'
            title = 'Percent difference, 5m scalar irradiance, OML1BIRR vs OML1BIRB'
            box = 'percentiii'
            unit = ' % '
            address = '{}/{}_{}.h5'.format(sink, name, wave)
            array = 100 * ((reservoir['omglerpre']['submarine_scalar'] / reservoir['noon']['submarine_scalar']) - 1)
            bracket = brackets[box]
            texts = [name, 'latitude', 'longitude']
            data = [array, latitude, longitude]
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

        return None

    def nucleate(self, tag='int', bounds=(-70, 40)):
        """Construct intermediate comparison file for cloud tau and residues.

        Arugments:
            tag: str, name of omocned data

        Returns:
            None
        """

        # create ascii, omocned data pairs
        comparisons = {'ler340': 'ler_values_0', 'ler360': 'ler_values_1'}
        comparisons.update({'ler380': 'ler_values_2', 'minLER': 'ler_values_3'})
        comparisons.update({'res331': 'residue_331'})
        comparisons.update({'COT360': 'cloud_tau'})
        comparisons.update({'minSZA': 'minimum_solar_zenith_angle'})

        # get clear sky input file
        source = '../studies/diatom/intermediate/MOBY.OUTPUT110.290_399FWHM5.00box_sza_omi.2005-01-07-02573'
        text = self._know(source)

        # grab headers
        headers = text[0].split()
        headers = [header.strip(',') for header in headers]

        # create floats from the rest
        rows = [line.split() for line in text[1:]]
        rows = [[float(entry) for entry in line] for line in rows]
        matrix = numpy.array(rows).transpose(1, 0)

        # create data from all but last 110 columns
        data = {name: numpy.array([column]).transpose(1, 0) for name, column in zip(headers, matrix[:-110])}

        # add uv data
        data['uv'] = matrix[-110:].transpose(1, 0)
        data['latitude'] = data['lat']
        data['longitude'] = data['lon']

        # make clear sky lattice
        self.lattice(tag, synthesis=data)

        # grab grid
        grid = self._pull(tag, grid=True)

        # compare uvdata
        latitude = grid.grab('latitude')
        longitude = grid.grab('longitude')
        mask = (latitude >= bounds[0]) & (latitude <= bounds[1])

        # for each pair
        for first, second in comparisons.items():

            # grab data
            array = grid.grab('ascii_{}'.format(first))
            arrayii = grid.grab('omocned_{}'.format(second))

            # calculate error
            error = 100 * ((arrayii / array) - 1)

            # graph error
            title = 'Percent Error {}'.format(second)
            address = 'plots/comparisons/Percent_{}.h5'.format(second)
            brackets = [-10, -1, -0.1, -0.01, 0.01, 0.1, 1, 10]
            parameters = ['{}_error'.format(second), error[mask], 'latitude', latitude[mask]]
            parameters += ['longitude', longitude[mask], address, brackets, title]
            self.squid.shimmer(*parameters)

        return None

    def optimize(self, wave=310, resolution=1, degrees=(-72, 58)):
        """Map changes during the optimization process.

        Arguments:
            wave: wavelength of choice
            resolution: number of points to meld
            degrees: float, latitude degree bracket

        Returns:
            None
        """

        # make plot directory
        sink = 'plots/optimization'
        self._make('{}/{}'.format(self.sink, sink))

        # set bounding brackets
        brackets = {}
        brackets['flux'] = [0, 40, 80, 120, 160, 200, 240, 280, 320]
        brackets['transmittance'] = [0.6, 0.7, 0.8, 0.9, 1.0, 1.1]
        brackets['percent'] = [-10, -5, -2, -1, 1, 2, 5, 10]
        brackets['chlorophyll'] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

        # make reserovir to hold datasets
        reservoir = {}

        # get the latitude tagged entry
        ocean = self._pull('latitude')

        # grab the latitude and longitude and bounds
        latitude = ocean.grab('latitude')
        nadir = latitude[:, 30]
        mask = (nadir > -9999)

        # make mask for ascending indices
        minimum = self._pin(float(nadir[mask].min()), latitude)[0][0]
        maximum = self._pin(float(nadir[mask].max()), latitude)[0][0]

        print(minimum, maximum)
        print(latitude[minimum, 30], latitude[maximum, 30])

        # subset minimum to maximum
        latitude = latitude[minimum: maximum]
        longitude = ocean.grab('longitude')[minimum: maximum]
        bounds = ocean.grab('latitude_bounds')[minimum: maximum]
        boundsii = ocean.grab('longitude_bounds')[minimum: maximum]

        # create polygons from bounds
        polygons = self._polymerize(latitude, longitude, resolution)

        # get indices based on resolution
        selection = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])

        # apply selection and mask to latitude and longitude
        latitude = latitude[selection]
        longitude = longitude[selection]

        # mask the latitude at degrees
        mask = (latitude > degrees[0]) & (latitude < degrees[1])

        # apply mask to polygons and coordinates
        polygons = polygons[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]

        # for each tag
        tags = ['original', 'optimized', 'latitude', 'logarithm']
        tags = tags
        for tag in tags:

            # get the data
            ocean = self._pull(tag)

            # get the wavelength
            wavelength = ocean.grab('wavelength')
            index = self._pin(wave, wavelength)[0][0]

            # grab the fluxes at the wavelength
            planar = ocean.grab('ultraviolet/surface/planar')[minimum: maximum, :, index]
            surface = ocean.grab('ultraviolet/surface/scalar')[minimum: maximum, :, index]
            submarine = ocean.grab('ultraviolet/underwater/scalar')[minimum: maximum, :, index]

            # get chlorophyll data
            chlorophyll = ocean.grab('chlorophyll')[minimum: maximum, :]

            # try to
            try:

                # grab the transmittances
                transmittance = ocean.grab('transmittance/planar')[minimum: maximum, :, index]
                transmittanceii = ocean.grab('transmittance/scalar')[minimum: maximum, :, index]

            # unless not found
            except IndexError:

                # in which case make dummies
                transmittance = numpy.ones(planar.shape)
                transmittanceii = numpy.ones(planar.shape)

            # grab the angle at solar noon
            noon = ocean.grab('minimum_solar_zenith_angle')[minimum: maximum, :]
            zenith = ocean.grab('solar_zenith_angle')[minimum: maximum, :]

            # normalize the planar flux by noon
            normalization = planar * numpy.cos(noon * (math.pi / 180)) / numpy.cos(zenith * (math.pi / 180))

            # set arrays, names, etc, apply ma
            names = ['surface_planar', 'surface_scalar', 'submarine_scalar', 'surface_planar_normalized']
            names += ['transmittance_planar', 'transmittance_scalar', 'chlorophyll']
            arrays = [planar, surface, submarine, normalization, transmittance, transmittanceii, chlorophyll]
            boxes = ['flux'] * 4 + ['transmittance'] * 2 + ['chlorophyll']
            units = ['mW/m2nm'] * 4 + ['_'] * 2 + ['mg / L']

            # apply selection and masks
            arrays = [array[selection][mask] for array in arrays]

            # add to reservoir
            reservoir[tag] = {name: array for name, array in zip(names, arrays)}
            reservoir[tag].update({'latitude': latitude, 'longitude': longitude})
            reservoir[tag].update({'polygons': polygons})

            # for each set
            for name, array, box, unit in zip(names, arrays, boxes, units):

                # make a plot
                address = '{}/{}_{}_{}.h5'.format(sink, name, tag, wave)
                title = '{}, {}, {} nm'.format(name, tag, wave)
                bracket = brackets[box]
                texts = [name, 'latitude', 'longitude']
                data = [array, latitude, longitude]
                self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

        # set pairs for difference plots
        pairs = [('original', 'optimized'), ('original', 'latitude'), ('original', 'logarithm')]
        name = 'submarine_scalar'
        for first, second in pairs:

            # get error
            stub = 'difference_{}'.format(name)
            error = 100 * ((reservoir[second][name] / reservoir[first][name]) - 1)

            # make a plot
            address = '{}/{}_{}_{}.h5'.format(sink, stub, second, wave)
            title = '{}, {} - original, {} nm'.format(stub, second, wave)
            bracket = brackets['percent']
            texts = [stub, 'latitude', 'longitude']
            data = [error, latitude, longitude]
            unit = '%'
            self.squid.pulsate(texts, data, polygons, address, bracket, title, unit)

        # calculate error for scale factor vs transmittance
        submarine = reservoir['logarithm']['submarine_scalar']
        surface = reservoir['logarithm']['surface_scalar']
        transmittance = reservoir['logarithm']['transmittance_scalar']
        latitude = reservoir['logarithm']['latitude']
        longitude = reservoir['logarithm']['longitude']

        # calculate percent
        percent = 100 * ((transmittance * surface / submarine) - 1)
        mask = numpy.isfinite(percent)

        # make a plot
        address = '{}/zz_transmittance_percent_{}.h5'.format(sink, wave)
        title = 'percent difference, transmittance x surface vs submarine'.format(wave)
        bracket = brackets['percent']
        texts = ['submarine_error', 'latitude', 'longitude']
        data = [percent, latitude, longitude]
        data = [array[mask] for array in data]
        unit = '%'
        self.squid.pulsate(texts, data, polygons[mask], address, bracket, title, unit)

        return None

    def orbit(self, year, sink=None):
        """Collection OMOCNUV orbit numbers for a particular year.

        Arguments:
            year: int, year to scan
            sink: folder for orbits json

        Returns:
            None
        """

        # set default sink
        sink = sink or '/user/1001/home/mbandel/studies/diatom/pace/orbits'

        # get orbits file
        name = '{}/omocned_orbits.json'.format(sink)
        orbits = self._load(name)

        # for each month
        for month in range(1, 13):

            # connect to the directory
            formats = (year, self._pad(month))
            hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/{}/{}'.format(*formats), 1, 31)

            # for each path
            for path in hydra.paths:

                # get the orbit number and date
                stage = hydra._stage(path)
                orbit = int(stage['orbit'])
                date = stage['date']

                # add entry
                orbits[orbit] = date

            # dump the contents
            self._dump(orbits, name)

        return None

    def picture(self, *names, tag='picture', date='2018m0321', position=15, resolution=20, margin=2, give=False):
        """Plot a map of a day.

        Arguments:
            *names: unpacked list of fields to plot
            tag: str, name of subfolder with output
            date: str, the date of interest in YYYYmMMDD format
            position: int, the 3-dimenional position where appropriate
            resolution: int, resolution of plot
            margin: int, percentile margin
            give: boolean, return data set?

        Returns:
            None
        """

        # create hydra
        self._stamp('beginning', initial=True)
        folder = '{}/{}'.format(self.sink, tag)
        hydra = Hydra('{}/data'.format(folder))

        # subset paths
        paths = [path for path in hydra.paths if date in path]
        paths.sort()

        # begin data collection
        fields = list(names) + ['latitude', 'longitude']
        data = {}

        # for each path
        for path in paths:

            # ingest path
            self._stamp('collecting {}...'.format(path))
            hydra.ingest(path)

            # grab all data
            datum = {field: hydra.grab(field) for field in fields}

            # get the latitude
            latitude = datum['latitude']
            shape = latitude.shape

            # find ascending data
            strip = latitude[:, 30]
            difference = strip[1:] - strip[:-1]
            indices = numpy.where(difference > 0)
            track = indices[0].min()
            trackii = indices[0].max()

            # for each field
            for field in datum.keys():

                # if shape matches
                if datum[field].shape[:2] == shape:

                    # add ascending data to collection
                    data.setdefault(field, []).append(datum[field][track:trackii])

        # create arrays
        self._stamp('stacking arrays...')
        data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

        # set fill value
        fill = -9999
        epsilon = 1e-5

        # for each variable
        for name in names:

            # set omi components
            tracer = data[name]
            latitude = data['latitude']
            longitude = data['longitude']

            # convert bit_flag
            if name == 'bit_flag':

                # take log
                tracer = numpy.log2(tracer)

            # if tracer is 3-d
            if len(tracer.shape) > 2:

                # subset by position
                tracer = tracer[:, :, position]

            # create masks for finite data
            mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (tracer > fill + epsilon)

            # set plot scale
            minimum = numpy.percentile(tracer[mask], margin)
            maximum = numpy.percentile(tracer[mask], 100 - margin)

            # if minimum and maximum are the same
            if minimum == maximum:

                # alter by 2 percent
                minimum = minimum * 0.98
                maximum = maximum * 1.02

            # set scale
            scale = (minimum, maximum)

            # clip at upper bounds
            maskii = (tracer < scale[0] + epsilon) & (tracer > fill + epsilon)
            tracer = numpy.where(maskii, scale[0] + epsilon, tracer)
            tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)

            # replace masked values with fills
            tracer = numpy.where(mask, tracer, fill)

            # setup pace plot, full scale
            destination = '{}/plots/OMI_OMOCNUV_{}_{}.png'.format(folder, date, name)
            limits = (-90, 90, -180, 180)
            size = (14, 5)

            # set title and units
            title = 'OMI OMOCNUV {}, {} ( {} )'.format(date, name, position)
            unit = ''
            gradient = 'gist_rainbow_r'
            selection = list(range(32, 256))

            # plot
            self._stamp('plotting {}...'.format(destination))
            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': size, 'log': False, 'back': 'white', 'font': 10, 'fontii': 10}
            options.update({'bar': True, 'extension': 'both', 'coast': 2})
            self._paint(*parameters, **options)

            # print status
            self._stamp('plotted.')

        # default package to none
        package = None

        # if giving data
        if give:

            # set package to data
            package = data

        return package

    def pixelate(self, north=0, east=-90):
        """Generate single pixel file.

        Arguments:
            north: float, test latitude
            east: float, test longitude

        Returns:
            None
        """

        # create folder
        folder = '{}/pixelation'.format(self.sink)
        self._make(folder)

        # make hydra
        hydra = Hydra('{}/pixel'.format(self.sink))

        # begin pixel collections
        pixels = []
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # get latitude and longitude
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')

            # get the closest pixel
            pixel = self._pin([north, east], [latitude, longitude])[0]

            # calculate the squared distance to nearest pixel
            distance = (latitude[pixel] - north) ** 2 + (longitude[pixel] - east) ** 2

            # add tuple to collection
            tuple = (pixel, path, distance)
            pixels.append(tuple)

        # sort by closest
        pixels.sort(key=lambda triplet: triplet[2])
        pixel, path, _ = pixels[0]

        # ingest path
        hydra.ingest(path)

        # extract all data
        data = hydra.extract()
        attributes = {feature.name: feature.attributes for feature in hydra}
        globals = hydra.attribute()

        # apply pixel to those with appropriate shapes
        shape = data['latitude'].shape
        fields = [field for field, array in data.items() if array.shape[:2] == shape]
        data.update({field: data[field][pixel] for field in fields})

        # stash data
        destination = '{}/{}'.format(folder, self._file(path).replace('.nc', '_{}_{}.h5'.format(*pixel)))
        self.spawn(destination, data, attributes, globals=globals)

        return None

    def plane(self, year=2005, month=12, day=21, orbits=(8, 9), exclusions=None, position=15):
        """Use random forest to predict fill values in omto3 data.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbits: tuple of ints, numbers of orbits
            exclustions: list of str, excluded fields
            position: int, position for 3D options

        Returns:
            None
        """

        # set default exclustions
        exclusions = exclusions or ['CSIrradiance305']

        # make output folders
        self._make('{}/reports'.format(self.sink))

        # get day of year
        julian = datetime.datetime(year, month, day).utctimetuple().tm_yday

        # create hydras
        formats = (year, self._pad(julian, 3))
        hydra = Hydra('/tis/acps/GES_DISC/003/OMUVB/{}/{}'.format(*formats))
        hydraii = Hydra('{}/polar'.format(self.sink))

        # get all polar data paths
        formats = (year, self._pad(month), self._pad(day))
        paths = [path for path in hydraii.paths if '{}m{}{}'.format(*formats) in path]
        paths.sort()
        paths = paths[orbits[0]:orbits[1]]

        # set fill values
        fill = -999
        fillii = 1e20

        # begin data
        data = {}

        # define target
        target = 'surface_uv_305_difference'

        # define three dimensional fields
        threes = []
        # threes += ['ultraviolet_surface_irradiance_planar', 'ultraviolet_surface_irradiance_scalar']
        threes += ['cloud_transmittance_factor', 'aerosol_absorption_factor']

        # for each path
        for path in paths:

            # get the orbit number from the path
            orbit = self._stage(path)['orbit'].strip('0')

            # ingest the data
            hydra.ingest(orbit)
            hydraii.ingest(orbit)

            # get all 2-D feature names
            names = [feature.name for feature in hydra if len(feature.shape) == 2]

            # set all data names to empty list
            data.update({name: data.setdefault(name, []) for name in names})

            # append all new data
            [data[name].append(hydra.grab(name)) for name in names]

            # get all 2-D feature names
            names = [feature.name for feature in hydraii if len(feature.shape) == 2]

            # set all data names to empty list
            data.update({name: data.setdefault(name, []) for name in names})

            # append all new data
            [data[name].append(hydraii.grab(name)) for name in names]

            # set all 3-D names to empty list
            data.update({name: data.setdefault(name, []) for name in threes})

            # append all new data
            [data[name].append(hydraii.grab(name)[:, :, position]) for name in threes]

            # add target
            data[target] = data.setdefault(target, [])

            # make difference
            sky = hydra.grab('CSIrradiance305')
            skyii = hydraii.grab('ultraviolet_surface_irradiance_planar')[:, :, position]
            difference = hydraii._relate(sky, skyii, percent=True)
            data[target].append(difference)

        # prune exclusions
        data = {name: arrays for name, arrays in data.items() if name not in exclusions}

        # concatenate all arrays
        data = {name: numpy.vstack(arrays) for name, arrays in data.items()}

        # get valid data mask
        masks = [(array > fill) & (abs(array) < fillii) & numpy.isfinite(array) for array in data.values()]

        # multiply all masks
        masque = masks[0]
        for mask in masks[1:]:

            # use logical and
            masque = numpy.logical_and(masque, mask)

        # check masked quantity
        masked = masque.sum()
        total = numpy.prod(masque.shape)
        self._print('masque: {} or {}, {} %'.format(masked, total, 100 * masked / total))

        # apply masque
        data = {name: array[masque] for name, array in data.items()}

        # set destiation
        destination = '{}/reports/random_forest_{}.txt'.format(self.sink, target)

        # run forest
        self._plant(data, target, destination)

        return None

    def pollinate(self, year=2019, month=12, day=1, margin=1, orbits=16, resolution=50, south=True, words=None):
        """Examine UV index and other data around St. George's Island.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbits: int, number of orbits
            margin: percentile margin for plots
            resolution: int, resolution
            epsilon: epsilon value for comparing floats
            south: boolean, south pole?
            words: list of string, the specific parameters

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/pollination'.format(self.sink))

        # create hydras
        hydra = Hydra('{}/island/{}'.format(self.sink, year))

        # get all paths on the day
        formats = (year, self._pad(month), self._pad(day))
        paths = [path for path in hydra.paths if '{}m{}{}t'.format(*formats) in path]
        paths.sort()

        # set searches
        searches = {'latitude': 'latitude', 'longitude': 'longitude', 'ultraviolet': 'ultraviolet_index'}
        searches.update({'snow': 'snow_ice_flag', 'quality': 'quality_flag_surface'})
        searches.update({'ozone': 'column_amount_ozone'})
        searches.update({'surface': 'surface_irradiance_planar'})
        searches.update({'reflectivity': 'minimum_reflectivity'})
        searches.update({'cloud': 'cloud_transmittance'})
        searches.update({'sky': 'clear_sky', 'noon': 'noon_zenith_angle'})

        # begin reservoirs
        data = {field: [] for field in searches.keys()}

        # set stubs for titles
        stubs = {'cloud': 'Cloud Transmittance Factor, 305nm'}
        stubs.update({'reflectivity': 'Minimum LER Climatology', 'ultraviolet': 'OMOCNUV Ultraviolet Index'})
        stubs.update({'snow': 'Snow / Sea Ice', 'ozone': 'Column Amount O3'})
        stubs.update({'surface': 'Surface Irradiance, 305nm'})
        stubs.update({'sky': 'Clear Sky Irradiance, 305nm', 'noon': 'Noon Zenith Angle'})

        # set units
        units = {'reflectivity': '%', 'snow': '%', 'ultraviolet': 'UVI', 'surface': 'mW / m^2', 'ozone': 'DU'}
        units.update({'cloud': '-', 'sky': 'mW / m^2', 'noon': 'deg'})

        # set default scales
        scales = {'cloud': (0, 1), 'reflectivity': (0, 100), 'ultraviolet': (0, 20), 'snow': (0, 100)}
        scales.update({'ozone': (100, 400), 'surface': (0, 150), 'sky': (0, 150), 'noon': (30, 70)})

        # for each path
        for path in paths[:orbits]:

            # ingest
            self._print('ingesting {}...'.format(path))
            orbit = str(int(self._stage(path)['orbit']))
            hydra.ingest(path)

            # get date
            date = hydra._stage(hydra.current)['day']

            # set fill
            fill = -9999

            # get data
            [data[field].append(hydra.grab(search)) for field, search in searches.items()]

        # create arrays
        data = {field: numpy.vstack(array) for field, array in data.items()}

        # set ocean to 0, and all permanent ices to 100
        data['snow'] = numpy.where(data['snow'] == 104, 0, data['snow'])
        data['snow'] = numpy.where(data['snow'] > 100, 100, data['snow'])

        # set figure size and limits
        limits = (-80, 80, -180, 180)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels, using 305nm for surface
        panels = {field: data[field] for field in stubs.keys()}
        panels['surface'] = panels['surface'][:, :, 15]
        panels['cloud'] = panels['cloud'][:, :, 15]
        panels['sky'] = panels['sky'][:, :, 15]

        # determine mask for latitude longitude limits
        mask = (data['latitude'] > limits[0] - 1) & (data['latitude'] < limits[1] + 1)
        mask = mask & (data['longitude'] > limits[2] - 1) & (data['longitude'] < limits[3] + 1)

        # add quality mask
        mask = mask & (data['quality'] < 2)

        # apply mask
        panels = {field: numpy.where(mask, panel, fill) for field, panel in panels.items()}

        # construct percentile bounds
        lower = margin
        upper = 100 - margin

        # # get main scales
        # minimum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), lower)
        # maximum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), upper)
        # scale = (minimum, maximum)
        # scales = [scale, scale]

        # # get min max diffs
        # low = abs(numpy.percentile(difference[masque], lower))
        # high = abs(numpy.percentile(difference[masque], upper))
        # magnitude = max([low, high])
        # scales += [(-magnitude, magnitude)]

        # default to north limiits, but if south
        limits = (-180, 180, 60, 90)
        if south:

            # set southhern limits
            limits = (-180, 180, -60, -90)
            # limits = (-100, -40, -60, -75)

        # set words
        words = words or list(stubs.keys())

        # for each set
        for field in words:

            # get stub and panel
            stub = stubs[field]
            panel = panels[field]

            # get scale
            scale = scales[field]

            # specify color gradient and particular indices
            gradient = 'gist_rainbow_r'
            selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

            # # if looking at Difference
            # if 'Difference' in stub:
            #
            #     # specify color gradient and particular indices
            #     gradient = 'seismic'
            #     selection = [index for index in range(256)]

            # create mask for finites
            masque = (abs(panel) < 1e20) & (panel > -100) & (numpy.isfinite(panel))

            # apply masques
            panel = numpy.where(masque, panel, fill)

            # saturate bounds
            epsilon = 1e-4
            # panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

            # # set formats
            # formats = (stub, date, orbit)

            # set suffix
            suffix = 'S' * south + 'N' * (not south)

            # plot polar
            title = '{}, {}, King George Island ( 62S, 59W )'.format(stub, date)
            destination = '{}/plots/pollination/{}_{}_{}.png'.format(self.sink, field, date, suffix)
            unit = units[field]
            # parameters = [[panel, latitude, longitude], destination, [title, unit]]
            # parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters = {'arrays': [panel, data['latitude'], data['longitude']], 'path': destination}
            parameters.update({'texts': [title, unit]})
            parameters.update({'scale': scales[field], 'spectrum': [gradient, selection]})
            parameters.update({'limits': limits, 'grid': resolution})
            parameters.update({'size': size, 'log': logarithm, 'bar': bar, 'back': 'white', 'south': south})
            self._stamp('plotting {}...'.format(destination))
            self._polarize(**parameters)

        return None

    def polish(self, year=2005, month=3, day=21, margin=1, orbits=16, resolution=50, archive=70004, south=False):
        """Examine lambertian glers from OMGLER in polar coordinates.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbits: int, number of orbits
            margin: percentile margin for plots
            resolution: int, resolution
            epsilon: epsilon value for comparing floats
            south: boolean, south pole?

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/polar'.format(self.sink))

        # create hydras
        formats = (archive, str(year), self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMGLER/{}/{}/{}'.format(*formats))

        # for each path
        for path in hydra.paths[:orbits]:

            # ingest
            orbit = str(int(self._stage(path)['orbit']))
            hydra.ingest(path)

            # get date
            date = hydra._stage(hydra.current)['day']

            # set fill
            fill = -9999

            # set positions
            positions = {70004: 2, 10003: 0}
            position = positions[archive]

            # get data
            latitude = hydra.grab('Latitude')
            longitude = hydra.grab('Longitude')
            # ocean = hydra.grab('OceanLER')[:, :, 2] * 100
            # land = hydra.grab('LandLER')[:, :, 2] * 100
            lambertian = hydra.grab('GLER')[:, :, 2]
            lambertianii = hydra.grab('GLER')[:, :, 3]

            # also get miimum ler climatology
            hydraii = Hydra('{}/polar'.format(self.sink))
            hydraii.ingest(str(orbit))
            climatology = hydraii.grab('minimum_reflectivity') / 100

            # create difference
            difference = climatology - lambertian

            # set figure size and limits
            limits = (-80, 80, -180, 180)
            size = (5, 10)
            logarithm = False
            bar = True

            # set up panels
            panels = [lambertian, lambertianii, climatology, difference]
            stubs = ['GLER_440nm_AS{}'.format(archive), 'GLER_466nm_AS{}'.format(archive)]
            stubs += ['MinLER_Climatology', 'MinLER_GLER_Difference']

            # determine mask for latitude longitude limits
            mask = (latitude > limits[0] - 1) & (latitude < limits[1] + 1)
            mask = mask & (longitude > limits[2] - 1) & (longitude < limits[3] + 1)

            # apply mask
            panels = [numpy.where(mask, panel, fill) for panel in panels]

            # create mask
            mask = (abs(panels[0]) < 1e20) & (panels[0] > -100) & (numpy.isfinite(panels[0]))
            maskii = (abs(panels[1]) < 1e20) & (panels[1] > -100) & (numpy.isfinite(panels[1]))
            masque = mask & maskii

            # construct percentile bounds
            lower = margin
            upper = 100 - margin

            print('masque: {}'.format(masque.sum()))

            # get main scales
            minimum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), lower)
            maximum = numpy.percentile(panels[0][masque].flatten().tolist() + panels[1][masque].flatten().tolist(), upper)
            scale = (minimum, maximum)
            scales = [scale, scale]

            # get min max diffs
            low = abs(numpy.percentile(difference[masque], lower))
            high = abs(numpy.percentile(difference[masque], upper))
            magnitude = max([low, high])
            scales += [(-magnitude, magnitude)]

            # set default scales
            scales = [(0.0, 1.0), (0.0, 1.0), (0.0, 1.0), (-0.2, 0.2)]
            print(scales)

            # default to north limiits, but if south
            limits = (-180, 180, 50, 90)
            if south:

                # set southhern limits
                limits = (-180, 180, -50, -90)

            # for each set
            for panel, stub, scale in zip(panels, stubs, scales):

                # specify color gradient and particular indices
                gradient = 'gist_rainbow_r'
                selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

                # if looking at Difference
                if 'Difference' in stub:

                    # specify color gradient and particular indices
                    gradient = 'seismic'
                    selection = [index for index in range(256)]

                # apply masques
                panel = numpy.where(masque, panel, fill)

                # saturate bounds
                epsilon=1e-4
                panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
                panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

                # set formats
                formats = (stub, date, orbit, archive)

                # set suffix
                suffix = 'S' * south + 'N' * (not south)

                # plot OMGLER
                title = '{}, \n{}, o{}'.format(*formats)
                destination = '{}/plots/polar/{}_{}_{}_{}_{}.png'.format(self.sink, *formats[1:], suffix, stub)
                unit = '-'
                # parameters = [[panel, latitude, longitude], destination, [title, unit]]
                # parameters += [scale, [gradient, selection], limits, resolution, size]
                parameters = {'arrays': [panel, latitude, longitude], 'path': destination, 'texts': [title, unit]}
                parameters.update({'scale': scale, 'spectrum': [gradient, selection]})
                parameters.update({'limits': limits, 'grid': resolution})
                parameters.update({'size': size, 'log': logarithm, 'bar': bar, 'back': 'white', 'south': south})
                self._stamp('plotting {}...'.format(destination))
                self._polarize(**parameters)

        return None

    def pose(self, *words, tag='march', wave=305, orbits=None, resolution=20, zone=(-60, 60), bar=True, suffix=''):
        """Create OCS conference plots.

        Arguments:
            *words: unpacked tuple of field ames
            tag: str, name of month to plot
            wave: int, wavelength
            orbit: list of int, orbit indexes, all orbits if None
            resolution: int, number of vertical pixels to coagulate
            zone: tuple of floats, the latitude limits
            bar: boolean, add colorbar?
            suffix: str, suffix for folders

        Returns:
            None
        """

        # set output to false
        output = False

        # set fill
        fill = -999

        # make folder based on tag
        self._make('{}/paper'.format(self.sink))
        self._make('{}/paper/plots{}'.format(self.sink, suffix))
        self._make('{}/paper/plots{}/{}'.format(self.sink, suffix, tag))

        # create omi hydra
        hydra = Hydra('{}/paper/granules{}/{}'.format(self.sink, suffix, tag))

        # grab spectral wavelength
        hydra.ingest(0)
        wavelength = hydra.grab('spectral_wavelength')
        position = hydra._pin(wave, wavelength)[0][0]

        # get the DNA Spectrum
        spectra = Hydra('../studies/diatom/simulation/spectra')
        spectra.ingest(1)
        spectrum = spectra.grab('spectrum')
        spectrum = spectrum.reshape(1, 1, -1)

        # set dates
        dates = {'march': 'March 21, 2005', 'june': 'June 21, 2005'}
        dates.update({'september': 'September 21, 2005', 'december': 'December 21, 2005'})
        dates.update({'colorbars': '2005-12-21'})

        # create fields
        fields = {'sky': 'clear_sky', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'cloud': 'cloud_tau', 'residue': 'residue', 'ozone': 'ozone'})
        fields.update({'coefficient': 'coefficient_planar', 'chlorophyll': 'chlorophyll'})
        fields.update({'surface': 'surface_irradiance_planar', 'reflectance': 'n_value'})
        fields.update({'reflectivity': 'reflectivity_360', 'anomaly': 'row_anomaly'})
        fields.update({'ultraviolet': 'ultraviolet_index', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'pressure': 'surface_pressure', 'bits': 'bit_flag'})
        fields.update({'aerosol': 'aerosol_absorption_optical_depth_354', })
        fields.update({'aerosolii': 'aerosol_absorption_optical_depth_388', })
        fields.update({'absorption': 'aerosol_absorption_factor'})
        fields.update({'algorithm': 'algorithm_flag', 'quality': 'quality_flag_surface'})
        fields.update({'qualityii': 'quality_flag_underwater'})
        fields.update({'water': 'water_fraction'})

        # create stubs
        stubs = {'sky': 'Clear Sky Irradiance, {}nm'.format(wave), 'cloud': 'Cloud Optical Depth, 360nm'}
        stubs.update({'residue': 'Residue 331', 'ozone': 'Total Column Ozone'})
        stubs.update({'coefficient': 'Planar Diffuse Coefficient Kd, {}nm'.format(wave), 'chlorophyll': 'Chlorophyll'})
        stubs.update({'surface': 'Planar Surface Irradiance, {}nm'.format(wave), 'reflectance': 'N Value 360'})
        stubs.update({'reflectivity': 'Reflectivity, 360nm', 'anomaly': 'Row Anomaly'})
        stubs.update({'ultraviolet': 'Ultraviolet Index'})
        stubs.update({'underwater': 'Underwater Irradiance at 5m, {}nm'.format(wave)})
        stubs.update({'depth': '10 % Penetration Depth, zp, {}nm'.format(wave)})
        stubs.update({'damage': 'DNA Damage Dose Rate at 5m Depth'})
        stubs.update({'pressure': 'Surface Pressure'})
        stubs.update({'aerosol': 'Aerosol Absorption Optical Depth, 354nm', })
        stubs.update({'aerosolii': 'Aerosol Absorption Optical Depth, 388nm', })
        stubs.update({'absorption': 'Aerosol Absorption Factor, {}nm'.format(wave)})
        stubs.update({'climatology': 'Aerosol Climatology Used'})
        stubs.update({'exponent': 'Aerosol Absorption Exponent'})

        # create uits
        units = {'sky': '$mW / m^2 s$', 'cloud': ' ', 'residue': ' ', 'ozone': 'DU'}
        units.update({'coefficient': '$K_d$ 305nm ( 1 / m )', 'chlorophyll': 'chlorophyll ( $mg / m^3$ )'})
        units.update({'surface': '$mW / m^2 s$', 'reflectance': ' ', 'reflectivity': ' '})
        units.update({'ultraviolet': 'UVI', 'underwater': '$mW / m^2 s$'})
        units.update({'depth': '$z_p$ 305nm ( m )', 'damage': '$mW / m^2 s$', 'pressure': 'atm'})
        units.update({'aerosol': '$AAOD$ 354nm', 'aerosolii': ' ', 'absorption': '$C_a$ 305nm'})
        units.update({'climatology': ' ', 'exponent': '$AAE$'})

        # create positions for third index of 3-d data
        positions = {'sky': position, 'coefficient': position, 'surface': position, 'reflectance': 2}
        positions.update({'underwater': position, 'depth': position, 'absorption': position})

        # declare color scale ranges
        scales = {'sky': (0, 120), 'cloud': (0, 40)}
        scales.update({'residue': (-10, 10), 'ozone': (230, 330)})
        scales.update({'coefficient': (0, 0.4), 'chlorophyll': (0.01, 20)})
        scales.update({'surface': (0, 120), 'reflectance': (50, 150)})
        scales.update({'reflectivity': (0, 100), 'anomaly': (0, 1)})
        scales.update({'ultraviolet': (0, 15)})
        scales.update({'underwater': (0, 120)})
        scales.update({'depth': (0, 35)})
        scales.update({'damage': (0, 200)})
        scales.update({'pressure': (0.96, 1.04)})
        scales.update({'aerosol': (0, 0.15)})
        scales.update({'aerosolii': (0, 0.15)})
        scales.update({'absorption': (0.5, 1.0)})
        scales.update({'climatology': (0, 1)})
        scales.update({'exponent': (2.5, 3.5)})

        # set quality flag criteria
        algorithms = {'surface': 'quality', 'sky': 'quality', 'aerosol': 'quality', 'aerosolii': 'quality'}
        algorithms.update({'absorption': 'quality', 'cloud': 'quality', 'reflectivity': 'quality', 'ozone': 'quality'})
        algorithms.update({'climatology': 'quality', 'exponent': 'quality'})

        # set background colors
        background = {'aerosol': 'white', 'aerosolii': 'white', 'exponent': 'white', 'absorption': 'white'}

        # set fields for logarithmic scale
        # logarithms = {'coefficient': True, 'chlorophyll': True}
        logarithms = {'chlorophyll': True}
        # logarithms = {'chlorophyll': False}

        # set titles
        titles = {'chlorophyll': 'Chlorophyll Concentration\n'}
        titles.update({'coefficient': 'Diffuse Attenuation Coefficient 305nm, $K_d$\n'})
        titles.update({'depth': '10% Penetration Depth 305nm, $z_p$\n'})
        titles.update({'aerosol': 'Aerosol Absorption Optical Depth 354nm\n'})
        titles.update({'absorption': 'Absorbing Aerosol Correction 305nm, $C_a$\n'})
        titles.update({'exponent': 'Absorption Angstrom Exponent\n'})

        # # set up transforms to convert from logs and replace fill values
        # def powering(array): return numpy.where(array != fill, 10 ** array, fill)
        # transforms = {'sky': powering, 'surface': powering, 'underwater': powering}
        # transforms.update({'coefficient': powering})

        # set paths
        orbits = orbits or [index for index, path in enumerate(hydra.paths)]

        # begin data reservoir
        data = {field: [] for field in fields.keys()}

        # for each orbit
        for orbit in orbits:

            # print
            self._print('collecting orbit {}'.format(orbit))

            # ingest
            hydra.ingest(orbit)

            # add data
            [data[field].append(hydra.grab(search)) for field, search in fields.items()]

        # stack all arrays
        data = {field: numpy.vstack(data[field]) for field in data.keys()}

        # # collect originnal data and apply transformts
        # data.update({field: transform(data[field]) for field, transform in transforms.items()})

        # calculate dna damage at 5m
        underwater = data['underwater']
        coefficient = data['coefficient']
        irradiance = underwater * numpy.exp(-coefficient * 5)
        damage = (irradiance * spectrum).sum(axis=2)
        data['damage'] = damage

        # calculate penetration depths
        data['depth'] = -1 * numpy.log(0.1) / data['coefficient']

        # calculate underwater at 5 meters
        data['underwater'] = data['underwater'] * numpy.exp(-5 * data['coefficient'])

        # add aerosol climatology flag
        data['climatology'] = data['bits'] & 1

        # calculate aerosol absorption exponent
        data['exponent'] = numpy.log(data['aerosol'] / data['aerosolii']) / numpy.log(388 / 354)

        # subset positions
        data.update({field: data[field][:, :, position] for field, position in positions.items()})

        # mask data by aerosol > 0
        mask = (data['aerosol'] > 0)
        data['exponent'] = numpy.where(mask, data['exponent'], fill)
        data['absorption'] = numpy.where(mask, data['absorption'], fill)

        # mask chlorophyll data with water fraction higher than 90
        mask = (data['water'] > 98)
        data['chlorophyll'] = numpy.where(mask, data['chlorophyll'], fill)

        # # specify color gradient and particular indices
        # gradient = 'gist_ncar'
        # selection = [index for index in range(16, 240)]

        # set up color gradients
        spectrum = '{}/colorbars/aerosol_colorbar.txt'.format(self.sink)
        spectrumii = '{}/colorbars/chlorophyll_colorbar.txt'.format(self.sink)
        spectrumiii = '{}/colorbars/chlorophyll_colorbar_r.txt'.format(self.sink)
        gradients = {'ozone': 'viridis', 'surface': 'plasma', 'chlorophyll': spectrumii, 'damage': 'plasma'}
        gradients.update({'aerosol': spectrum, 'absorption': 'plasma_r', 'exponent': 'viridis'})
        gradients.update({'depth': spectrumii, 'coefficient': spectrumiii})

        # set up color selections
        # selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]
        selections = {}

        # set extensions
        extensions = {'absorption': 'min', 'exponent': 'both'}

        # for each field
        words = words or list(stubs.keys())
        for field in words:

            # specify color gradient and particular indices
            gradient = gradients.get(field, 'gist_rainbow_r')
            selection = selections.get(field, range(256))

            # # check for reversed color scales
            # if field in ('absorption',):
            #
            #     # specify color gradient and particular indices
            #     gradient = 'gist_rainbow'
            #     selection = [index for index in range(0, 80)] + [index for index in range(128, 208)]

            # except for exclusions
            exclusions = ['latitude', 'longitude', 'anomaly', 'quality']
            if field not in exclusions:

                # set omi components
                tracer = data[field]
                latitude = data['latitude']
                longitude = data['longitude']
                anomaly = data['anomaly']
                bits = data['bits']

                # create masks for finite data
                ascending = ((bits & 256) == 0) & (data[algorithms.get(field, 'qualityii')] < 3)
                mask = (numpy.isfinite(tracer)) & (abs(tracer) < 1e15) & (anomaly < 1) & (tracer > -999) & ascending

                # set plot scale
                scale = scales[field]

                # set units
                unit = units[field]

                # clip at upper bounds
                epsilon = 1e-8
                maskii = (tracer < scale[0] + epsilon) & (tracer > fill + epsilon)
                tracer = numpy.where(maskii, scale[0] + epsilon, tracer)
                tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)

                # replace masked values with fills
                # latitude = numpy.where(mask, latitude, fill)
                # longitude = numpy.where(mask, longitude, fill)
                tracer = numpy.where(mask, tracer, fill)

                # setup pace plot, full scale
                destination = '{}/paper/plots{}/{}/OMI_OMOCNUV_{}_{}.png'.format(self.sink, suffix, tag, field, tag)
                limits = (*zone, -180, 180)
                size = (14, 5)
                logarithm = logarithms.get(field, False)
                back = background.get(field, 'black')
                title = titles.get(field, '')
                extension = extensions.get(field, 'max')

                # plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer, latitude, longitude], destination, [title, unit, dates[tag]]]
                parameters += [scale, [gradient, selection], limits, resolution]
                options = {'size': size, 'log': logarithm, 'back': back, 'north': False}
                options.update({'bar': bar, 'extension': extension, 'coast': 2, 'font': 18, 'fontii': 14})
                self._paint(*parameters, **options)

                # print status
                self._stamp('plotted.'.format(field))

        # default package to None
        package = None

        # if output requested
        if output:

            # set package to data
            package = data

        return package

    def proxify(self, source='granule', sink='proxy', reflectance=None, subset=(2000, 2000)):
        """Create a proxy input file for running PACE data through OMOCNUV, using live data.

        Arguments:
            source: str, source directory
            ancillary: str, file path for ancillary file
            sink: str, sink folder name
            reflectance: reflectance area ( takes time to load each time )
            subset: tuple of ints, the subset for smaller batches

        Returns:
            None
        """

        # create sink folder
        self._make('{}/{}'.format(self.sink, sink))

        # create hydra
        hydra = Hydra('{}/{}'.format(self.sink, source))

        # ingest l1b file
        hydra.ingest('L1B')

        # get geolocation data
        time = hydra.grab('time')[:subset[0]]
        latitude = hydra.grab('latitude')[:subset[0], :subset[1]]
        longitude = hydra.grab('longitude')[:subset[0], :subset[1]]

        # get angle geomtry
        zenith = hydra.grab('solar_zenith')[:subset[0], :subset[1]]
        zenithii = hydra.grab('sensor_zenith')[:subset[0], :subset[1]]
        azimuth = hydra.grab('solar_azimuth')[:subset[0], :subset[1]]
        azimuthii = hydra.grab('sensor_azimuth')[:subset[0], :subset[1]]
        height = hydra.grab('height')[:subset[0], :subset[1]]

        # get the wavelength
        wavelength = hydra.grab('blue_wavelength')

        # if reflectance not given ( takes time to load in )
        if not numpy.array(reflectance).any():

            # grab from file
            reflectance = hydra.grab('rhot_blue')

        # take reflectance subset
        reflectance = reflectance[:, :subset[0], :subset[1]]

        # set up needed wavelengths
        wavelengthii = numpy.linspace(325, 385, 61)

        # create interpolation function, and interpolate to regular grid
        interpolator = scipy.interpolate.interp1d(wavelength, reflectance, kind='linear', axis=0)
        reflectance = interpolator(wavelengthii)

        # print shapes
        self._print('reflectance: {}'.format(reflectance.shape))
        self._print('wavelength: {}'.format(wavelength.shape))

        # translate height into surface pressure
        sea = 1013.25
        temperature = 15 + 273
        lapse = -0.0065
        constant = 8.31432
        gravity = 9.80665
        mass = 0.0289644
        factor = -gravity * mass / (constant * lapse)
        pressure = sea * (1 + (lapse / temperature) * height) ** factor

        # get the chlorophyll data from BGC file
        hydra.ingest('BGC')
        chlorophyll = hydra.grab('chlor_a')[:subset[0], :subset[1]]

        # extract date
        date = re.search('[0-9]{8}T[0-9]{6}', hydra.current).group()
        year = date[:4]
        month = date[4:6]
        day = date[6:8]

        # get ozone gridded file
        # hydraii = Hydra('/tis/acps/OMPS-NPP/61004/NMTO3-L2G/{}/{}'.format(year, month))
        # hydraii = Hydra('/tis/acps/OMI/10003/OMTO3G/{}/{}'.format(year, month))
        hydraii = Hydra('/tis/acps/OMI/10003/OMTO3/{}/{}/{}'.format(year, month, day))

        # begin collection
        ozones = []
        qualities = []
        latitudes = []
        longitudes = []

        # for each orbit
        for path in hydraii.paths:

            # ingest and get data
            hydraii.ingest(path)
            ozones.append(hydraii.grab('ColumnAmountO3'))
            qualities.append(hydraii.grab('QualityFlags'))
            latitudes.append(hydraii.grab('Latitude'))
            longitudes.append(hydraii.grab('Longitude'))

        # create arrays
        ozoneii = numpy.vstack(ozones)
        qualityii = numpy.vstack(qualities)
        latitudeii = numpy.vstack(latitudes)
        longitudeii = numpy.vstack(longitudes)

        # # grab gridded data in preparation for colocating
        # ozoneii = hydraii.grab('ColumnAmountO3')
        # qualityii = hydraii.grab('QualityFlags')
        # pressureii = hydraii.grab('TerrainPressure')

        # construct latitude and longitude nodes
        # latitudes = ozoneii.shape[1]
        # longitudes = ozoneii.shape[2]
        latitudes = 360
        longitudes = 720
        indexes = numpy.array(range(latitudes))
        indexesii = numpy.array(range(longitudes))
        nodes = [-90 + (90 / latitudes) + index * (180 / latitudes) for index in range(latitudes)]
        nodesii = [-180 + (180 / longitudes) + index * (360 / longitudes) for index in range(longitudes)]

        # construct ozone grid from all positive ozone values
        mask = (ozoneii >= 0) & (numpy.isfinite(ozoneii)) & (qualityii < 1)
        # summation = numpy.where(mask, ozoneii, 0).sum(axis=0)
        # samples = mask.astype(int).sum(axis=0)
        # grid = summation / samples

        # get points for valid grid cells
        # valid = numpy.isfinite(grid)
        # pixels = self._mask(valid)
        # parallels = numpy.array([pixel[0] for pixel in pixels])
        # meridians = numpy.array([pixel[1] for pixel in pixels])
        # quantities = numpy.array([grid[pixel] for pixel in pixels])
        parallels = latitudeii[mask]
        meridians = longitudeii[mask]
        quantities = ozoneii[mask]

        # crate mesh
        # mesh, meshii = numpy.meshgrid(indexes, indexesii, indexing='ij')
        mesh, meshii = numpy.meshgrid(nodes, nodesii, indexing='ij')

        # interpolate
        self._stamp('interpolating...', initial=True)
        interpolator = scipy.interpolate.NearestNDInterpolator((parallels, meridians), quantities)
        # interpolator = scipy.interpolate.RBFInterpolator((parallels, meridians), quantities, kernel='gaussian')
        # interpolator = scipy.interpolate.Rbf(parallels, meridians, quantities, function='gaussian')
        grid = interpolator(mesh, meshii)
        self._stamp('interpolated.')
        self._print('ozone grid:')
        self._print(grid.min(), grid.max())

        # # create average of lattiude block
        # south = self._pin(latitude.min(), nodes)[0][0]
        # north = self._pin(latitude.max(), nodes)[0][0]
        # west = self._pin(longitude.min(), nodesii)[0][0]
        # east = self._pin(longitude.max(), nodesii)[0][0]
        #
        # # create average of block
        # block = grid[south: north, west: east]
        # valid = numpy.isfinite(block)
        # average = block[valid].mean()
        #
        # # create composite
        # grid = self._compose(grid, 10, average)

        # interpolate ozone grid onto pace coordinates
        fill = -9999
        parameters = ((nodes, nodesii), grid, (latitude, longitude))
        ozone = scipy.interpolate.interpn(*parameters, method='linear', bounds_error=False, fill_value=fill)

        # # construct pressure grid from all positive pressure values
        # mask = (pressureii >= 0)
        # summation = numpy.where(mask, pressureii, 0).sum(axis=0)
        # samples = mask.astype(int).sum(axis=0)
        # grid = summation / samples
        #
        # # create average of block
        # block = grid[south: north, west: east]
        # valid = numpy.isfinite(block)
        # average = block[valid].mean()
        #
        # # create composite
        # grid = self._compose(grid, 3, average)
        #
        # # interpolate pressure grid onto pace coordinates
        # parameters = ((nodes, nodesii), grid, (latitude, longitude))
        # pressure = scipy.interpolate.interpn(*parameters, method='linear', bounds_error=False, fill_value=fill)

        # get water mask
        hydraiii = Hydra('{}/water'.format(self.sink))
        hydraiii.ingest('WaterMask')
        wateriii = hydraiii.grab('WaterMask')

        # construct latitude and longitude nodes
        latitudes = wateriii.shape[0]
        longitudes = wateriii.shape[1]
        nodes = [-90 + (90 / latitudes) + index * (180 / latitudes) for index in range(latitudes)]
        nodesii = [-180 + (180 / longitudes) + index * (360 / longitudes) for index in range(longitudes)]

        # interpolate water grid onto pace coordinates
        parameters = ((nodes, nodesii), wateriii, (latitude, longitude))
        water = scipy.interpolate.interpn(*parameters, method='nearest', bounds_error=False, fill_value=fill)

        # begin data addresses and arrays
        addresses = []
        arrays = []
        shapes = []
        attributes = []

        # get base files
        base = Hydra('{}/base'.format(self.sink))

        # begin global attributes
        globals = {}

        # set exlusions
        exclusions = ['BAND_1', 'BAND1']

        # for each base path
        for path in base.paths:

            # ingest
            base.ingest(path, net=True)

            # get global attributes
            globals.update(base.attribute())

            # for each feature
            for feature in base:

                # remove exclusions
                if not any([exclusion in feature.slash for exclusion in exclusions]):

                    # add address to addresses and zero array to arrays
                    addresses.append(feature.slash)
                    shapes.append(feature.shape)
                    attributes.append(feature.attributes)

        # # add dummy metadata to globals
        # globals.update({'RangeBeginningDateTime': globals['time_coverage_start']})
        # globals.update({'RangeEndingDateTime': globals['time_coverage_end']})
        # globals.update({'EquatorCrossingDateTime': '13:35:00'})
        # globals.update({'EquatorCrossingLongitude': 0})

        # determine track length from scanline feature
        track = base.dig('scanline')[0].shape[0]

        # get all standard mode features that are dimensions to transfer to other groups
        modes = ['BAND3_RADIANCE/STANDARD_MODE', 'BAND3_STANDARD_MODE/INSTRUMENT']

        # fpor each mode
        for mode in modes:

            # create transfer features
            transfers = [feature for feature in base if mode in feature.slash]
            transfers = [feature for feature in transfers if feature.attributes.get('dimension', True)]

            # for each transfer
            for feature in transfers:

                # for each address stub
                stubs = ['HDFEOS/SWATHS', 'BAND2_ANCILLARY/STANDARD_MODE']
                for stub in stubs:

                    # add address to addresses and zero array to arrays
                    addresses.append(feature.slash.replace(mode, stub))
                    shapes.append(feature.shape)
                    attributes.append(feature.attributes)

        # create coversion dictionary
        conversion = {track: reflectance.shape[1]}
        conversion.update({30: reflectance.shape[2], 60: reflectance.shape[2]})
        conversion.update({159: reflectance.shape[0], 557: reflectance.shape[0], 751: reflectance.shape[0]})

        # for each array
        for shape, attribute in zip(shapes, attributes):

            # use conversion to rewrite the shape
            sizes = [conversion.get(size, size) for size in shape]
            shapeii = tuple(sizes)

            # if size
            if 'size' in attribute.keys():

                # convert size
                attribute['size'] = conversion.get(attribute['size'], attribute['size'])

            # add dummy array with new size
            arrays.append(numpy.zeros(shapeii))

        # convert reflectance by dividing by pi and multiplying by cosine SZA
        radians = (numpy.pi / 180)
        cosines = numpy.cos(zenith * radians)
        cosines = cosines.reshape(*cosines.shape, 1)

        # add radiance data, rearranging and duplicating for band 3, dividing by 4 pi steridians
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance')
        array = numpy.array([reflectance.transpose(1, 2, 0)])
        [arrays.append(array * cosines / math.pi) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # add irradiance data as dummy of 1's, as radiances are already normalized
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance')
        array = numpy.ones((1, 1, reflectance.shape[2], reflectance.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength coefficients for radiance, bands2 and 3, k0 = 310, k1 = 2.5
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        array = numpy.array([[[[wavelengthii[0], 1, 0, 0, 0]] * reflectance.shape[2]] * reflectance.shape[1]])
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'n_wavelength_poly')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength coefficients for irradiance, bands2 and 3, k0 = 310, k1 = 2.5
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        array = numpy.array([[[[wavelengthii[0], 1, 0, 0, 0]] * reflectance.shape[2]]])
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'n_wavelength_poly')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength reference column for radiance and irradiance, bands2 and 3, r = 0
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        array = numpy.array([0])
        [arrays.append(array) for _ in range(4)]
        dimensions = ('time',)
        [attributes.append({'dimensions': dimensions}) for _ in range(4)]

        # create dummy spectral channel quality for radiance
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        array = numpy.zeros((1, reflectance.shape[1], reflectance.shape[2], wavelengthii.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create dummy spectral channel quality for irradiance
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        array = numpy.zeros((1, 1, reflectance.shape[2], wavelengthii.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # add water fraction from pace water mask
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/water_fraction')
        arrays.append(numpy.array([water * 100]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add land water classification
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/land_water_classification')
        array = numpy.where(water == 0, 1, 7)
        arrays.append(array)
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy ground pixel quality
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/ground_pixel_quality')
        arrays.append(numpy.zeros((1, *water.shape)))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add solar zenith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/solar_zenith_angle')
        arrays.append(numpy.array([zenith]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add solar azimuith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/solar_azimuth_angle')
        arrays.append(numpy.array([azimuth]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add viewing zenith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/viewing_zenith_angle')
        arrays.append(numpy.array([zenithii]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add viewing azimuth angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/viewing_azimuth_angle')
        arrays.append(numpy.array([azimuthii]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add time
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/time')
        arrays.append(numpy.array(time[:1]))
        dimensions = ('time',)
        attributes.append({'dimensions': dimensions})

        # add dummy time deltas
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/delta_time')
        arrays.append(numpy.array(time - time[0]))
        dimensions = ('time', 'scanline')
        attributes.append({'dimensions': dimensions})

        # add ozone, mutliplying by 1000
        addresses.append('HDFEOS/SWATHS/OMI Column Amount O3/Data Fields/ColumnAmountO3')
        arrays.append(ozone)
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy algorithm flag
        addresses.append('HDFEOS/SWATHS/OMI Column Amount O3/Data Fields/AlgorithmFlags')
        arrays.append(numpy.ones(ozone.shape).astype(int))
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add surface pressure
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/PS')
        arrays.append(numpy.array([pressure * 100]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy row anomaly flag
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/row_anomaly')
        arrays.append(numpy.zeros((1, *water.shape)))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy snow / ice flag
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/snow_ice')
        array = numpy.where(water == 0, 0, 104)
        arrays.append(numpy.array([array]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add latitude
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/latitude')
        arrays.append(numpy.array([latitude]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add longitude
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/longitude')
        arrays.append(numpy.array([longitude]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy latitude bounds
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/latitude_bounds_fov75')
        arrays.append(numpy.zeros((1, *latitude.shape, 4)))
        dimensions = ('time', 'scanline', 'ground_pixel', 'ncorner')
        attributes.append({'dimensions': dimensions})

        # add dummy longitude bounds
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/longitude_bounds_fov75')
        arrays.append(numpy.zeros((1, *longitude.shape, 4)))
        dimensions = ('time', 'scanline', 'ground_pixel', 'ncorner')
        attributes.append({'dimensions': dimensions})

        # add chlorphyll scanline dimesion
        addresses.append('scanline')
        arrays.append(numpy.zeros((reflectance.shape[1],)))
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimension': True, 'size': reflectance.shape[1]})

        # add chlorphyll scanline dimesion
        addresses.append('ground_pixel')
        arrays.append(numpy.zeros((reflectance.shape[2],)))
        attributes.append({'dimension': True, 'size': reflectance.shape[2]})

        # add chlorphyll concentration
        addresses.append('Geometry_Dependent_Surface_LER/ChlorophyllConcentration')
        arrays.append(chlorophyll)
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})
        # attributes.append({})

        # add aerosol dimension
        addresses.append('aerosol_wavelength')
        arrays.append(numpy.zeros((3,)))
        attributes.append({'dimension': True, 'size': 3})

        # add aerosol information, double the third wavelength
        fills = numpy.ones(ozone.shape) * fill
        addresses.append('SCIDATA/FinalAerosolAbsOpticalDepth')
        array = numpy.array([fills, fills, fills]).transpose(1, 2, 0)
        arrays.append(array)
        dimensions = ('scanline', 'ground_pixel', 'aerosol_wavelength')
        attributes.append({'dimensions': dimensions})

        # add aerosol information, double the third wavelength
        addresses.append('Wavelengths')
        array = numpy.array([354, 388, 388])
        arrays.append(array)
        dimensions = ('aerosol_wavelength',)
        attributes.append({'dimensions': dimensions})

        # create data
        data = {address: array for address, array in zip(addresses, arrays)}
        attributes = {address: attribute for address, attribute in zip(addresses, attributes)}
        self._view(data)

        # set destination
        # identifier = re.search("[0-9, A-Z]{15}", ancillary).group()
        identifier = date
        destination = '{}/{}/Proxy_PACE_Data_{}.nc'.format(self.sink, sink, identifier)

        # create netcdf4 file
        self._stamp('writing {}...'.format(destination), initial=True)
        self.spawn(destination, data, attributes, net=True, globals=globals)
        self._stamp('wrote {}.'.format(destination))

        return None

    def qualify(self, year=2018, resolution=20, parallels=None, source=None, sink=None):
        """Plot spring and autumn days.

        Arguments:
            resolution: int, number of trackwise pixels to coagulate
            parallels: lisgt of tuples of int, the longitude span
            source: str, path to source of data
            sink: str, path to sink for plots
            sinkii: str, path to sink for line plots

        Returns:
            None
        """

        # set defaults
        source = source or '{}/quality'.format(self.sink)
        sink = sink or 'plots/quality'

        # create sink directory
        self._make('{}/{}'.format(self.sink, sink))

        # set default parallels
        if not parallels:

            # construct parallels
            parallels = [(-95, -75), (-80, -55), (-60, -30), (-40, -10), (-15, 10)]
            parallels += [(5, 35), (25, 55), (50, 75), (70, 95)]

        # set depths and months
        months = {3: 'March'}
        dates = {3: '{}-03-21'.format(year), 6: '{}-06-21'.format(year)}
        dates.update({9: '{}-09-21'.format(year), 12: '{}-12-21'.format(year)})
        waves = (310,)
        degrees = 90

        # set brackets for plotting heatmaps
        scales = ['depth', 'planar', 'scalar', 'five', 'twenty', 'two', 'constant', 'reflectivity', 'ozone']
        scales += ['chlorophyll', 'cloud', 'quality', 'bits', 'algorithm']
        brackets = {scale: {} for scale in scales}
        brackets['depth'][310] = [0, 5, 10, 15, 20, 25, 30, 35, 40]
        brackets['planar'][310] = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
        brackets['five'][310] = [20, 40, 60, 80, 100, 120, 140, 160]
        brackets['constant'][310] = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
        brackets['reflectivity'][310] = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
        brackets['ozone'][310] = [200, 225, 250, 275, 300, 325, 350, 375, 400]
        brackets['cloud'][310] = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
        brackets['chlorophyll'][310] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        brackets['quality'][310] = [-0.99, 0.99, 1.99, 2.99, 3.99]
        brackets['algorithm'][310] = [-0.99, 0.99, 1.99, 2.99]
        brackets['bits'][310] = [-0.5, 0.5, 1.5, 3.5, 7.5, 15.5, 31.5, 63.5, 127.5]
        brackets['bits'][310] += [255.5, 511.5, 1023.5, 2047.5, 4095.5, 8191.5]

        # create hydra
        hydra = Hydra(source)

        # for each month
        for month in months.keys():

            # set date
            date = dates[month]

            # grab paths
            paths = [path for path in hydra.paths if '{}m{}'.format(year, self._pad(month)) in path]
            paths.sort()

            # # get subset
            # paths = [path for index, path in enumerate(paths) if index in orbits]

            # set fields of interest
            fields = ['latitude', 'longitude', 'latitude_bounds', 'longitude_bounds']
            fields += ['quality_flag', 'algorithm_flag', 'bit_flag']

            # begin reservoirs
            reservoir = {field: [[] for _ in waves] for field in fields}

            # add additions
            additions = ['polygons']
            reservoir.update({field: [[] for _ in waves] for field in additions})

            # for each path, for as many orbits as specified
            for path in paths:

                # print path
                print('ingesting {}...'.format(path))

                # ingest the path
                hydra.ingest(path)

                # get latitude
                latitude = hydra.grab('latitude')
                minimum = latitude.min()
                maximum = latitude.max()

                # get indices between min and max
                first = hydra._pin(minimum, latitude)[0][0]
                last = hydra._pin(maximum, latitude)[0][0]

                # grab fields from hdf file
                data = {field: hydra.grab(field).squeeze() for field in fields}
                data = {field: hydra.grab(field).squeeze()[first:last] for field in fields}

                # grab wavelengths
                wavelength = hydra.grab('spectral_wavelength').squeeze()

                # get corners from latitude and longitude bounds
                #polygons = self._polymerize(data['latitude_bounds'], data['longitude_bounds'], resolution)
                polygons = self._polymerize(data['latitude'], data['longitude'], resolution)
                data['polygons'] = polygons

                # set compressions for latitude and longitude bounds
                trackwise = resolution
                rowwise = 1

                # take only even numbered scan indices for easier plotting
                latitude = data['latitude']
                evens = numpy.array([index for index in range(latitude.shape[0]) if index % trackwise == 0])
                evensii = numpy.array([index for index in range(latitude.shape[1]) if index % rowwise == 0])

                # for all fields except bounds
                for field, array in data.items():

                    # check for bounds
                    if 'polygons' not in field:

                        # take select indices
                        data[field] = array[evens]
                        data[field] = data[field][:, evensii]

                # # mark as wet anything not land
                # wet = (data['land_mask'] == 0) & (data['processing_quality'] < 6)

                # for each wavelength
                for index, wave in enumerate(waves):

                    # get the wavelength index
                    position = self._pin(wave, wavelength)[0][0]

                    # for each field
                    for field, array in data.items():

                        # check for three - d array
                        if len(array.shape) == 3:

                            # if latitude or longitude bounds
                            if field in ('latitude_bounds', 'longitude_bounds', 'polygons'):

                                # append
                                #panel = numpy.where(wet, data[field][:, :, position], 0)
                                panel = data[field]
                                reservoir[field][index].append(panel)

                            # otherwise, assume spectral
                            else:

                                # if atmospheric
                                if field in ('clear_sky',):

                                    # zero out all but wet entries, and append
                                    #panel = numpy.where(wet, data[field][:, :, position], 0)
                                    panel = data[field][:, :, position]
                                    reservoir[field][index].append(panel)

                                # otherwise, only wet entries for
                                else:

                                    # # zero out all but wet entries, and append
                                    pass
                                    # panel = numpy.where(wet, data[field][:, :, position], 0)
                                    # #panel = data[field][:, :, position]
                                    # reservoir[field][index].append(panel)

                        # otherwise
                        else:

                            # if not latitude or longitude
                            if field not in fields:

                                # zero out all but wet entries, and append
                                pass
                                # panel = numpy.where(wet, data[field], 0)
                                # reservoir[field][index].append(panel)

                            # otherwise
                            else:

                                # add the data
                                panel = data[field]
                                reservoir[field][index].append(panel)

            # stack all orbits into arrays
            for field, arrays in reservoir.items():

                # stack all entries
                stack = [numpy.vstack(arrays[index]).squeeze() for index, _ in enumerate(waves)]
                reservoir[field] = stack

            # # for each wavelength
            # for index, _ in enumerate(waves):
            #
            #     # get quality
            #     quality = reservoir['processing_quality'][index] < 7
            #
            #     # apply quality to all fields
            #     for field, arrays in reservoir.items():
            #
            #         # stack all entries
            #         reservoir[field][index] = reservoir[field][index][quality]

            # for each wave length
            for index, wave in enumerate(waves):

                # for each parallel set
                for parallel in parallels:

                    # construct the coordiantes
                    latitude = reservoir['latitude'][index]
                    longitude = reservoir['longitude'][index]
                    bounds = reservoir['latitude_bounds'][index]
                    boundsii = reservoir['longitude_bounds'][index]
                    polygons = reservoir['polygons'][index]

                    # get latitude mask ( +/- 60)
                    mask = (abs(latitude) < degrees) & (latitude >= parallel[0]) & (latitude <= parallel[1])
                    maskii = (polygons[:, :, 4:].std(axis=2) < 20)
                    masque = numpy.logical_and(mask, maskii)
                    ordinate = latitude[masque]
                    abscissa = longitude[masque]
                    polygons = polygons[masque]
                    # polygons = numpy.hstack([bounds[masque], boundsii[masque]])

                    # set formats
                    formats = (wave, self._pad(month), self._orient(parallel[0]), self._orient(parallel[1]))
                    tag = '{}_{}_{}_{}'.format(*formats)

                    # heat map quality
                    tracer = reservoir['quality_flag'][index][masque]
                    folder = '{}/quality_flag_{}.h5'.format(sink, tag)
                    title = 'Quality Flag, Surface, {}, {}'.format(months[month], date)
                    bracket = brackets['quality'][wave]
                    name = 'quality'.format(wave)
                    units = '  '.format(wave)
                    texts = [name, 'latitude', 'longitude']
                    arrays = [tracer, ordinate, abscissa]
                    self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                    # heat map quality
                    tracer = reservoir['algorithm_flag'][index][masque]
                    folder = '{}/algorithm_flag_{}.h5'.format(sink, tag)
                    title = 'Algorithm Flag, Ocean, {}, {}'.format(months[month], date)
                    bracket = brackets['algorithm'][wave]
                    name = 'algorithm'.format(wave)
                    units = '  '.format(wave)
                    texts = [name, 'latitude', 'longitude']
                    arrays = [tracer, ordinate, abscissa]
                    self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                    # heat map bit flag
                    tracer = reservoir['bit_flag'][index][masque]
                    folder = '{}/bit_flag_{}.h5'.format(sink, tag)
                    title = 'Bit Flag, {}, {}'.format(months[month], date)
                    bracket = brackets['bits'][wave]
                    name = 'bits'.format(wave)
                    units = ' '.format(wave)
                    texts = [name, 'latitude', 'longitude']
                    arrays = [tracer, ordinate, abscissa]
                    self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        return None

    def radiate(self, tag='panama', point=(10, -90)):
        """Compare reflectance values for omi versus SIM data.

        Arguments:
            tag: str, tag for simulation file
            point: tuple of floats, the latitude and longitude of the point

        Returns:
            None
        """

        # create omi hydra
        hydra = Hydra('../studies/diatom/simulation/reflectance')

        # set bands
        bands = ('BAND2', 'BAND3')

        # set products
        products = ('OML1BRUG', 'OML1BRVG')

        # for each OMI band
        for band, product in zip(bands, products):

            # get irradiance data
            hydra.ingest('OML1BIRR')
            irradiance = hydra.grab('{}/irradiance'.format(band)).squeeze()
            wavelength = hydra.grab('{}/wavelength'.format(band)).squeeze()

            # get radiance data
            hydra.ingest(product)
            radiance = hydra.grab('{}/radiance'.format(band)).squeeze()
            wavelengthii = hydra.grab('{}/wavelength'.format(band)).squeeze()
            latitude = hydra.grab('{}/latitude'.format(band)).squeeze()
            longitude = hydra.grab('{}/longitude'.format(band)).squeeze()

            # find pixel closest to point
            pixel = self._pin(point, [latitude, longitude])[0]
            row = pixel[1]

            # interpolate radiance onto irradiance grid
            radianceii = self._project(wavelength[row], wavelengthii[row], radiance[pixel])

            # calculate reflectance and n value
            reflectance = radianceii / irradiance[row]
            value = -100 * numpy.log10(reflectance)

            # plot reflectance
            formats = [self._stage(hydra.current)['day'], self._stage(hydra.current)['orbit']]
            formats += [self._orient(point[0]), self._orient(point[1], east=True)]
            title = 'OMI Radiance / Irradiance, {}, o{}, {}, {}'.format(*formats)
            address = 'radiance/OMI_Reflectance_{}.h5'.format(band)
            self.squid.ink('reflectance', reflectance, 'wavelength', wavelength[row], address, title)

            # plot n value
            title = 'OMI N_Value, {}, o{}, {}, {}'.format(*formats)
            address = 'radiance/OMI_NValue_{}.h5'.format(band)
            self.squid.ink('n_value', value, 'wavelength', wavelength[row], address, title)

        # create pace hydra
        hydra = Hydra('../studies/diatom/simulation/ten')

        # grab latitude and longitude
        hydra.ingest('20220321T182542.MODEL_INPUTS')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')

        # grab latitude and longitude
        hydra.ingest('20220321T182542.L1B')
        reflectance = hydra.grab('rhot_blue')
        wavelength = hydra.grab('blue_wavelength')
        zenith = hydra.grab('solar_zenith')

        # adjust by cossza / pi
        radians = (numpy.pi / 180)
        zenith = zenith.reshape(1, *zenith.shape)
        reflectance = reflectance * numpy.cos(zenith * radians) / math.pi

        # calculate n value
        value = -100 * numpy.log10(reflectance)

        # find pixel closest to point
        pixel = self._pin(point, [latitude, longitude])[0]

        # plot reflectance
        formats = [self._stage(hydra.current)['day'], self._stage(hydra.current)['orbit']]
        formats += [self._orient(point[0]), self._orient(point[1], east=True)]
        title = 'PACE Radiance / Irradiance, {}, o{}, {}, {}'.format(*formats)
        address = 'radiance/PACE_Reflectance.h5'
        self.squid.ink('reflectance', reflectance[:, pixel[0], pixel[1]], 'wavelength', wavelength, address, title)

        # plot n value
        title = 'PACE N_Value, {}, o{}, {}, {}'.format(*formats)
        address = 'radiance/PACE_NValue.h5'
        self.squid.ink('n_value', value[:, pixel[0], pixel[1]], 'wavelength', wavelength, address, title)

        return None

    def reflect(self):
        """Plot minimum reflectivity values.

        Arguments:
            None

        Returns:
            None
        """

        # get ler values
        hydra = Hydra('{}/data/ler'.format(self.sink))
        hydra.ingest()
        reflectivity = hydra.grab('minimum_ler')

        # only get first month
        reflectivity = reflectivity[0]

        # create latitude coordinates
        latitude = [index - 90 for index in range(180)]
        longitude = [(index * 1.25) - 180 for index in range(288)]

        # expand latitude and longitude
        latitude = numpy.array([latitude] * 288).transpose(1, 0)
        longitude = numpy.array([longitude] * 180)

        # make plot
        address = 'plots/comparisons/Minimum_Lers.h5'
        title = 'Minimum Ler, January'
        bounds = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 100, 1000]
        self.squid.shimmer('mler', reflectivity, 'latitude', latitude, 'longitude', longitude, address, bounds, title)

        return None

    def replicate(self, tag='coefficient', sink='replication'):
        """Replicate the n value calculations from L1b.

        Arguments:
            tag: tag for L1b reservoir.

        Returns:
            None
        """

        # get the hydra
        hydra = Hydra('../studies/diatom/frames/{}'.format(tag))

        # set default wavelength values
        waves = {331: 331.06, 340: 339.66, 360: 359.88, 380: 379.95}
        bands = {'BAND2': (331, 340), 'BAND3': (360, 380)}
        products = {'BAND2': 'OML1BRUG', 'BAND3': 'OML1BRVG', 'IRR': 'OML1BIRR'}
        pixels = {'BAND2': 557, 'BAND3': 751}

        # start data collection
        data = {}

        # for each band
        for band in bands.keys():

            # find the right radiance path
            path = [entry for entry in hydra.paths if products[band] in entry][0]
            hydra.ingest(path)

            # pull the coefficients and radiances
            radiance = hydra.grab('{}/radiance'.format(band))[0]
            coefficient = hydra.grab('{}/wavelength_coefficient'.format(band))[0]
            reference = hydra.grab('{}/wavelength_reference_column'.format(band))
            flag = hydra.grab('{}/spectral_channel_quality'.format(band))[0]

            # calculate wavelengths
            wavelength = self._calculate(coefficient, reference, pixels[band])

            # find the irradiance path
            path = [entry for entry in hydra.paths if products['IRR'] in entry][0]
            hydra.ingest(path)

            # pull the coefficients and radiances
            irradiance = hydra.grab('{}/irradiance'.format(band))[0]
            coefficientii = hydra.grab('{}/wavelength_coefficient'.format(band))[0]
            referenceii = hydra.grab('{}/wavelength_reference_column'.format(band))
            flagii = hydra.grab('{}/spectral_channel_quality'.format(band))[0]

            # calculate wavelengths
            wavelengthii = self._calculate(coefficientii, referenceii, pixels[band])

            # lowercase band
            low = band.lower()

            # add to data collection
            data['{}/radiance'.format(low)] = radiance
            data['{}/radiance_coefficient'.format(low)] = coefficient
            data['{}/radiance_wavelength'.format(low)] = wavelength
            data['{}/radiance_quality_flag'.format(low)] = flag
            data['{}/irradiance'.format(low)] = irradiance
            data['{}/irradiance_coefficient'.format(low)] = coefficientii
            data['{}/irradiance_wavelength'.format(low)] = wavelengthii
            data['{}/irradiance_quality_flag'.format(low)] = flagii

            # for each ewave
            for wave in bands[band]:

                # smooth to get the radiances and irradiances at the wavelength
                numerator, weight = self._smooth(waves[wave], wavelength, radiance)
                denominator, weightii = self._smooth(waves[wave], wavelengthii, irradiance)
                data['{}/numerator_{}'.format(low, wave)] = numerator
                data['{}/denominator_{}'.format(low, wave)] = denominator
                data['{}/radiance_weights_{}'.format(low, wave)] = weight
                data['{}/irradiance_weights_{}'.format(low, wave)] = weightii

                # calcuate ratio
                ratio = numerator / denominator
                data['{}/ratio_{}'.format(low, wave)] = ratio

                # caculate n values
                value = -100 * numpy.log10(ratio)
                data['{}/n_value_{}'.format(low, wave)] = value

        # make file
        self._make('{}/data/{}'.format(self.sink, sink))
        destination = '{}/data/{}/N_Value_Replication_{}.h5'.format(self.sink, sink, tag)
        self.spawn(destination, data)

        return None

    def reside(self, year=2005, month=3, day=21, orbit=3630, resolution=50):
        """Compare the ultraviolet surface ultraviolet irradiance of OMOCNUV vs OMUVB.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: str, orbit number

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/residue'.format(self.sink))

        # create hydras
        hydra = Hydra('{}/ultraviolet'.format(self.sink))

        # ingest orbit
        orbit = str(orbit)
        hydra.ingest(orbit)

        # get orbit and date
        date = hydra._stage(hydra.current)['day']

        # set fill
        fill = -9999

        # set waves
        waves = [305, 310, 324, 380]

        # get data
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        wavelength = hydra.grab('spectral_wavelength')
        positions = [hydra._pin(wave, wavelength)[0][0] for wave in waves]
        correction = hydra.grab('cloud_transmittance')[:, :, positions]
        residue = hydra.grab('residue_331')
        reflectivity = hydra.grab('reflectivity_360')

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-60, 60, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [correction[:, :, position] for position, _ in enumerate(waves)] + [residue, reflectivity]
        stubs = ['Cloud_Transmittance_{}nm'.format(wave) for wave in waves] + ['Residue_331', 'Reflectivity_360']

        # for each set
        for panel, stub in zip(panels, stubs):

            # create mask
            masque = (abs(panel) < 1e20) & (panel > 0) & (numpy.isfinite(panel))

            # get main scales
            minimum = numpy.percentile(panel[masque], 5)
            maximum = numpy.percentile(panel[masque], 95)
            scale = (minimum, maximum)

            # set epsilon
            epsilon = 1e-4

            # apply masques
            panel = numpy.where(masque, panel, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

            # set formats
            formats = (stub, date, orbit)

            # plot data
            title = '{}, OMOCNUV, \n{}, o{}'.format(*formats)
            destination = '{}/plots/residue/{}_OMOCNUV_{}_{}.png'.format(self.sink, *formats)
            unit = ' '
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def scale(self, chlorophyll, ozone, zenith, wavelength):
        """Find interpolation entries that bracket the particular attributes.

        Arguments:
            chlorophyll: float, chlorphyll concentration
            ozone: float, ozone abount
            zenith: float, solar zenith angle
            wavelength: float, wavelength

        Returns:
            None
        """

        # get the table
        hydra = Hydra('../studies/diatom/table')
        hydra.ingest()

        # grab the data
        chlorophylls = hydra.grab('chlorophyll')
        ozones = hydra.grab('ozone')
        zeniths = hydra.grab('zenith')
        wavelengths = hydra.grab('wavelength')
        scale = hydra.grab('scaling_factor')

        # find the ozone indices
        first = (ozones < ozone).tolist().index(False) - 1
        last = (ozones > ozone).tolist().index(True)
        scale = scale[first: last + 1]
        print(first, last, ozones[first], ozone, ozones[last])

        # find the zenith indices
        first = (zeniths < zenith).tolist().index(False) - 1
        last = (zeniths > zenith).tolist().index(True)
        scale = scale[:, first: last + 1]
        print(first, last, zeniths[first], zenith, zeniths[last])

        # find the chlorophyll indices
        first = (chlorophylls < chlorophyll).tolist().index(False) - 1
        last = (chlorophylls > chlorophyll).tolist().index(True)
        scale = scale[:, :, first: last + 1]
        print(first, last, chlorophylls[first], chlorophyll, chlorophylls[last])

        # find the wavelength indices
        first = (wavelengths < wavelength).tolist().index(False) - 1
        last = (wavelengths > wavelength).tolist().index(True)
        scale = scale[:, :, :, first: last + 1]
        print(first, last, wavelengths[first], wavelength, wavelengths[last])

        return scale

    def scan(self, year=2020, month=12, day=2, orbit=87146, scans=(260, 271), row=59):
        """Scan clear sky spectrum across antarctic discontinuity.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, the orbit number
            scans: tuple of ints, the scanline range
            row: int, the row

        Returns:
            None
        """

        # make folder
        self._make('{}/plots/scans'.format(self.sink))

        # get hydra
        hydra = Hydra('{}/island/{}'.format(self.sink, year))

        # ingest orbit
        hydra.ingest(str(orbit))

        # get wavelengths and clear sky data
        wavelength = hydra.grab('spectral_wavelength')
        sky = hydra.grab('clear_sky')

        # for each scan
        for scan in range(*scans):

            # create scatter plot of clearsky by wavelength
            formats = (row, scan, year, self._pad(month), self._pad(day), orbit)
            title = 'Clear Sky irradiance, Row {}, Scan {}, {}m{}{}, o{}'.format(*formats)
            address = 'plots/scans/clear_sky_row_{}_scan_{}_{}m{}{}_{}.h5'.format(*formats)
            self.squid.ink('clear sky', sky[scan, row], 'wavelength', wavelength, address, title)

        return None

    def screen(self, tag='spots', year=2005, month=3, day=21, screens=(1.3, 1.7, 1.0), resolution=20, bar=True):
        """Create abledo maps from OMAERUV product, screening based on UVAI

        Arguments:
            tag: str, the folder name
            year: int, the year
            month: int, the month
            day: int, the day
            resolution: int, number of vertical pixels to coagulate
            screens: uvai index low and high screens
            bar: boolean, add colorbar?

        Returns:
            None
        """

        # set fill
        fill = -999

        # set default latiude zone
        zone = (-90, 90)

        # begin hotspots
        hotspots = []

        # # define equatorial zone ( south, north, west, east )
        # hotspots.append([-20, 20, -180, 180])

        # define southearstern atlantic smoke ( south, north, west, east )
        hotspots.append([-30, 5, -15, 25])

        # define tropical atlantic dust ( south, north, west, east )
        hotspots.append([10, 35, -60, -15])

        # define arabian sea dust ( south, north, west, east )
        hotspots.append([10, 25, 55, 75])

        # define southeast asia smoke ( south, north, west, east )
        hotspots.append([10, 30, 95, 125])

        # construct date
        formats = (year, self._pad(month), self._pad(day))
        date = '{}m{}{}'.format(*formats)

        # create fields
        fields = {'albedo': 'InputSSAforOMACA', 'latitude': 'latitude', 'longitude': 'longitude'}
        fields.update({'quality': 'FinalAlgorithmFlagsACA', 'ultraviolet': 'UVAerosolIndex'})
        fields.update({'land': 'LandWaterClass', 'sky': 'FinalAerosolAbsOpticalDepth'})
        fields.update({'clouded': 'AerosolOpticalDepthOverCloud', 'cloud': 'CloudFraction'})

        # create stubs
        stubs = {'albedo': 'Input Albedo, 354nm', 'sky': 'Clear sky AAOD, 354nm'}
        stubs.update({'clouded': 'Above Clouds AAOD, 354nm'})

        # create uits
        units = {'albedo': '-', 'sky': '-', 'clouded': '-'}

        # declare color scale ranges
        scales = {'albedo': (0.80, 0.95), 'sky': (0, 0.3), 'clouded': (0, 0.15)}

        # make folder based on tags
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/{}'.format(self.sink, tag))

        # create omi hydra
        hydra = Hydra('/tis/acps/OMI/93002/OMAERUV/{}/{}/{}'.format(*formats))

        # create positions for third index of 3-d data
        positions = {'albedo': 0, 'sky': 0, 'clouded': 0}

        # set screened fields
        screened = ['albedo', 'clouded']

        # begin data reservoir
        data = {field: [] for field in fields.keys()}

        # for each orbit
        for path in hydra.paths:

            # print
            self._print('collecting {}...'.format(path))

            # ingest
            hydra.ingest(path)

            # add data
            [data[field].append(hydra.grab(search)) for field, search in fields.items()]

        # stack all arrays
        data = {field: numpy.vstack(data[field]) for field in data.keys()}

        # subset positions
        data.update({field: data[field][:, :, position] for field, position in positions.items()})

        # set up color gradients, using custom
        bar = '{}/bars/aaod_colorbar.txt'.format(self.sink)
        gradients = {'albedo': 'plasma', 'sky': 'plasma', 'clouded': 'plasma'}
        gradients = {'albedo': bar, 'sky': bar, 'clouded': bar}

        # set up color selections
        selections = {}

        # for each field
        words = list(stubs.keys())
        for field in words:

            # specify color gradient and particular indices
            gradient = gradients.get(field, 'gist_rainbow_r')
            selection = selections.get(field, range(256))

            # set components
            tracer = data[field]
            latitude = data['latitude']
            longitude = data['longitude']
            quality = data['quality']
            ultraviolet = data['ultraviolet']
            land = data['land']
            cloud = data['cloud']
            albedo = data['albedo']

            # default low, high to 0
            low, high, fraction = 0, 0, 0

            # if a screened field
            if field in screened:

                # unpack screens
                low, high, fraction = screens

            # if above cloud
            if field == 'clouded':

                # multiple by 1 - albedo
                tracer = tracer * (1 - albedo)

            # create ultraviolet threshold
            threshold = numpy.ones(ultraviolet.shape) * high

            # use low screen for land
            threshold = numpy.where(land == 1, low, threshold)

            # for each hotspot
            for spot in hotspots:

                # create boundaries within hotspot
                within = (latitude >= spot[0]) & (latitude <= spot[1])
                within = within & (longitude >= spot[2]) & (longitude <= spot[3])

                # lower threshold in hotspot
                threshold = numpy.where(within, low, threshold)

            # create masks for finite data, and uvai greater than threshold
            mask = (quality == 0) & (tracer > 0) & (ultraviolet >= threshold) & (cloud >= fraction)

            # set plot scale
            scale = scales[field]

            # set units
            unit = units[field]

            # clip at upper bounds
            epsilon = 1e-8
            tracer = numpy.where((tracer < scale[0] + epsilon), scale[0] + epsilon, tracer)
            tracer = numpy.where((tracer > scale[1] - epsilon), scale[1] - epsilon, tracer)
            tracer = numpy.where(mask, tracer, fill)

            # create hotspot lines
            lines = []
            for spot in hotspots:

                # create abscissa
                abscissa = [spot[2], spot[2], spot[3], spot[3], spot[2]]

                # create ordinate
                ordinate = [spot[0], spot[1], spot[1], spot[0], spot[0]]

                # add to lines
                lines.append((abscissa, ordinate))

            # setup pace plot, full scale
            symbol = '\u2265'
            formats = (stubs[field], date, symbol, high, symbol, low, symbol, fraction)
            title = 'OMAERUV, {}\n{}, UVAI {} {} ( {} {} in hotspots / land ), CF {} {}'.format(*formats)
            formats = (self.sink, tag, field, date, str(high).replace('.', ''))
            destination = '{}/plots/{}/OMAERUV_{}_{}_{}.png'.format(*formats)
            limits = (*zone, -180, 180)
            size = (14, 5)
            logarithm = False
            back = 'white'
            extension = 'neither'

            # plot
            self._stamp('plotting {}...'.format(destination), initial=True)
            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': size, 'log': logarithm, 'back': back, 'north': False}
            options.update({'bar': bar, 'extension': extension, 'lines': lines})
            self._paint(*parameters, **options)

            # print status
            self._stamp('plotted.'.format(field))

        return None

    def scrutinize(self, tag='duel', folderii='statics', pixel=(994, 19)):
        """Scrutinize the nvalue differences at a particular scanline.

        Arguments:
            folder: str, name of data folder for dueling nvalues
            folderii: str, name of folder for level 1b files
            pixel: tuple of int, scanline, row

        Returns:
            None
        """

        # grab the duel file
        hydra = Hydra('{}/data/duel'.format(self.sink))
        paths = [path for path in hydra.paths if tag in path]
        hydra.ingest(paths[0])

        # grab the level 1b files
        hydraii = Hydra('../studies/diatom/{}'.format(folderii))

        # unpack pixel
        scan, row = pixel

        # link wavelengths to bands
        waves = {331: 'BAND2', 340: 'BAND2', 360: 'BAND3', 380: 'BAND3'}
        products = {'BAND2': 'OML1BRUG', 'BAND3': 'OML1BRVG'}

        # begin report
        report = ['Irradaince and Radiance Wavelengths']

        # for each wave
        for wave, band in waves.items():

            # get the nvalues
            value = hydra.grab('ascii_n{}'.format(wave))
            valueii = hydra.grab('omocned_n{}'.format(wave))
            latitude = hydra.grab('ascii_latitude')

            # calculate the difference
            difference = valueii - value

            # make rows
            rows = numpy.array(range(60))

            # plot the differences
            formats = (wave, scan, round(latitude[scan, row], 2))
            title = 'N value difference for {}nm at scanline {}, {} lat'.format(*formats)
            address = 'plots/scanline/N_Value_Difference_{}_{}.h5'.format(wave, tag)
            self.squid.ink('nvalue_difference', difference[scan], 'row', rows, address, title)

            # plot the irradiance
            path = [entry for entry in hydraii.paths if 'OML1BIRR' in entry][0]
            hydraii.ingest(path)

            # get the irradiance and wavelength
            irradiance = hydraii.grab('{}/irradiance'.format(band)).squeeze()
            wavelength = hydraii.grab('{}/wavelength'.format(band)).squeeze()

            # plot the radiance and radiance wavelength
            path = [entry for entry in hydraii.paths if products[band] in entry][0]
            hydraii.ingest(path)

            # get the radiance and wavelengths
            radiance = hydraii.grab('{}/radiance'.format(band)).squeeze()
            wavelengthii = hydraii.grab('{}/wavelength'.format(band)).squeeze()

            # for each row to the left and right
            for rowii in (row - 1, row, row + 1):

                # plot the radiance spectrum
                title = 'Radiance, Scan {}, Row {}'.format(scan, rowii)
                address = 'plots/scanline/Radiance_{}_{}_{}.h5'.format(wave, rowii, tag)
                ordinate = radiance[scan, rowii]
                abscissa = wavelengthii[rowii]
                self.squid.ink('radiance', ordinate, 'wavelength', abscissa, address, title)

                # find closet and add to report
                closest = ((abscissa - wave) ** 2).argsort()[0]
                report.append('')
                report.append('radiance, wave: {}nm, scan: {}, row: {}'.format(wave, scan, rowii))
                intensity = [round(entry, 10) for entry in radiance[scan, rowii].tolist()[closest - 10: closest + 10]]
                grid = [round(entry, 2) for entry in wavelengthii[rowii].tolist()[closest - 10: closest + 10]]
                report.append(str(intensity))
                report.append(str(grid))

                # plot the irradiance spectrum
                title = 'Irradiance, Scan {}, Row {}'.format(scan, rowii)
                address = 'plots/scanline/Irradiance_{}_{}_{}.h5'.format(wave, rowii, tag)
                ordinate = irradiance[rowii]
                abscissa = wavelength[rowii]
                self.squid.ink('irradiance', ordinate, 'wavelength', abscissa, address, title)

                # find closet and add to report
                closest = ((abscissa - wave) ** 2).argsort()[0]
                report.append('')
                report.append('irradiance, wave: {}nm, scan: {}, row: {}'.format(wave, scan, rowii))
                intensity = [round(entry, 10) for entry in irradiance[rowii].tolist()[closest - 10: closest + 10]]
                grid = [round(entry, 2) for entry in wavelength[rowii].tolist()[closest - 10: closest + 10]]
                report.append(str(intensity))
                report.append(str(grid))

        # jot report
        destination = '{}/reports/irradiance_report_{}.txt'.format(self.sink, tag)
        self._jot(report, destination)

        return None

    def season(self, orbits=(0, 1), resolution=20, location=(-20, -100), source=None, sink=None, sinkii=None):
        """Plot spring and autumn days.

        Arguments:
            orbits: tuple of ints, the orbit indices
            resolution: int, number of trackwise pixels to coagulate
            location: tuple of floats, the latitude, longitude coordinates for full spectrum
            source: str, path to source of data
            sink: str, path to sink for plots
            sinkii: str, path to sink for line plots

        Returns:
            None
        """

        # set defaults
        source = source or '{}/equinox'.format(self.sink)
        sink = sink or 'plots/equinox'
        sinkii = sinkii or 'plots/gyre'
        sinkiii = 'plots/swaths'
        degrees = 62
        waves = (310,)

        # set depths and months
        months = {3: 'March', 9: 'September'}
        dates = {3: '2005-03-21', 6: '2005-06-21', 9: '2005-09-21', 12: '2005-12-21'}

        # set brackets for plotting heatmaps
        scales = ['depth', 'planar', 'scalar', 'five', 'twenty', 'two', 'constant', 'reflectivity', 'ozone']
        scales += ['chlorophyll', 'cloud']
        brackets = {scale: {} for scale in scales}
        brackets['depth'][310] = [0, 5, 10, 15, 20, 25, 30, 35, 40]
        brackets['planar'][310] = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
        brackets['five'][310] = [20, 40, 60, 80, 100, 120, 140, 160]
        brackets['constant'][310] = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
        brackets['reflectivity'][310] = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
        brackets['ozone'][310] = [200, 225, 250, 275, 300, 325, 350, 375, 400]
        brackets['cloud'][310] = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
        brackets['chlorophyll'][310] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

        # create hydra
        hydra = Hydra(source)

        # set up gyre
        gyre = {month: [] for month in months.keys()}

        # for each month
        for month in months.keys():

            # set date
            date = dates[month]

            # grab paths
            paths = [path for path in hydra.paths if '2005m{}'.format(self._pad(month)) in path]
            paths.sort()

            # get subset
            paths = [path for index, path in enumerate(paths) if index in orbits]

            # set fields of interest
            fields = ['latitude', 'longitude', 'latitude_bounds', 'longitude_bounds']
            fields += ['ozone', 'chlorophyll', 'clear_sky', 'irradiance_planar', 'coefficient_planar']
            fields += ['transmittance_planar', 'land_mask', 'processing_quality', 'cloud_tau']

            # begin reservoirs
            reservoir = {field: [[] for _ in waves] for field in fields}

            # add additions
            additions = ['irradiance_five', 'depth', 'irradiance_twenty', 'row']
            reservoir.update({field: [[] for _ in waves] for field in additions})

            # for each path, for as many orbits as specified
            for path in paths:

                # print path
                print('ingesting {}...'.format(path))

                # ingest the path
                hydra.ingest(path)

                # grab fields from hdf file
                data = {field: hydra.grab(field).squeeze() for field in fields}

                # add irradiance at 5m depth field
                five = data['irradiance_planar'] * data['transmittance_planar']
                five = five * numpy.exp(-5 * data['coefficient_planar'])
                data['irradiance_five'] = five

                # add irradiance at 20m depth field
                twenty = data['irradiance_planar'] * data['transmittance_planar']
                twenty = twenty * numpy.exp(-20 * data['coefficient_planar'])
                data['irradiance_twenty'] = twenty

                # penetration depth field
                data['depth'] = -numpy.log(0.1) / data['coefficient_planar']

                # create row matrix
                data['row'] = numpy.array([list(range(60))] * data['ozone'].shape[0])

                # grab wavelengths
                wavelength = hydra.grab('spectral_wavelength').squeeze()

                # get pixel closest to location, and calculate distance
                pixel = self._pin(list(location), [data['latitude'], data['longitude']])[0]
                distance = (data['latitude'][pixel] - location[0]) ** 2 + (data['longitude'][pixel] - location[1]) ** 2

                # grab 5m spectrum
                spectrum = data['irradiance_five'][pixel]
                spectrumii = data['irradiance_twenty'][pixel]

                # add contents to gyre
                record = {'distance': distance, 'spectrum': spectrum, 'spectrumii': spectrumii}
                record.update({'wavelength': wavelength})
                gyre[month].append(record)

                # get corners from latitude and longitude bounds
                corners = self._orientate(data['latitude_bounds'], data['longitude_bounds'])

                # set compressions for latitude and longitude bounds
                trackwise = resolution
                rowwise = 1

                # take only even numbered scan indices for easier plotting
                latitude = data['latitude']
                evens = numpy.array([index for index in range(latitude.shape[0]) if index % trackwise == 0])
                evensii = numpy.array([index for index in range(latitude.shape[1]) if index % rowwise == 0])

                # for all fields except bounds
                for field, array in data.items():

                    # check for bounds
                    if 'bounds' not in field:

                        # take select indices
                        data[field] = array[evens]
                        data[field] = data[field][:, evensii]

                # compress corners
                corners = self._compress(corners, trackwise, rowwise)

                # construct bounds
                compasses = ('southwest', 'southeast', 'northeast', 'northwest')
                arrays = [corners[compass][:, :, 0] for compass in compasses]
                data['latitude_bounds'] = numpy.stack(arrays, axis=2)
                arrays = [corners[compass][:, :, 1] for compass in compasses]
                data['longitude_bounds'] = numpy.stack(arrays, axis=2)

                # adjust polygon  boundaries to avoid crossing dateline
                data['longitude_bounds'] = self._cross(data['longitude_bounds'])

                # mark as wet anything not land
                wet = (data['land_mask'] == 0) & (data['processing_quality'] < 6)

                # for each wavelength
                for index, wave in enumerate(waves):

                    # get the wavelength index
                    position = self._pin(wave, wavelength)[0][0]

                    # for each field
                    for field, array in data.items():

                        # check for three - d array
                        if len(array.shape) == 3:

                            # if latitude or longitude bounds
                            if field in ('latitude_bounds', 'longitude_bounds'):

                                # append
                                #panel = numpy.where(wet, data[field][:, :, position], 0)
                                panel = data[field]
                                reservoir[field][index].append(panel)

                            # otherwise, assume spectral
                            else:

                                # if atmospheric
                                if field in ('clear_sky', 'irradiance_planar'):

                                    print('avoid wet, {}'.format(field))

                                    # zero out all but wet entries, and append
                                    #panel = numpy.where(wet, data[field][:, :, position], 0)
                                    panel = data[field][:, :, position]
                                    reservoir[field][index].append(panel)

                                # otherwise, only wet entries for
                                else:

                                    # zero out all but wet entries, and append
                                    panel = numpy.where(wet, data[field][:, :, position], 0)
                                    #panel = data[field][:, :, position]
                                    reservoir[field][index].append(panel)

                        # otherwise
                        else:

                            # if not latitude or longitude
                            if field not in ('latitude', 'longitude', 'ozone', 'processing_quality', 'cloud_tau'):

                                # zero out all but wet entries, and append
                                panel = numpy.where(wet, data[field], 0)
                                reservoir[field][index].append(panel)

                            # otherwise
                            else:

                                print('avoid wet, {}'.format(field))

                                # add the data
                                panel = data[field]
                                reservoir[field][index].append(panel)

            # stack all orbits into arrays
            for field, arrays in reservoir.items():

                # stack all entries
                stack = [numpy.vstack(arrays[index]).squeeze() for index, _ in enumerate(waves)]
                reservoir[field] = stack

            # for each wavelength
            for index, _ in enumerate(waves):

                # get quality
                quality = reservoir['processing_quality'][index] < 7

                # apply quality to all fields
                for field, arrays in reservoir.items():

                    # stack all entries
                    reservoir[field][index] = reservoir[field][index][quality]

            # for each wave length
            for index, wave in enumerate(waves):

                # construct the coordiantes
                latitude = reservoir['latitude'][index]
                longitude = reservoir['longitude'][index]
                bounds = reservoir['latitude_bounds'][index]
                boundsii = reservoir['longitude_bounds'][index]

                # get latitude mask ( +/- 60)
                mask = (abs(latitude) < degrees)
                ordinate = latitude[mask]
                abscissa = longitude[mask]
                polygons = numpy.hstack([bounds[mask], boundsii[mask]])

                # determine pixel closest to -20, -100
                pixel = self._pin(list(location), [latitude[mask], longitude[mask]])[0]

                print('latitude: ', latitude[mask][pixel])
                print('longitude: ', longitude[mask][pixel])

                # set formats
                formats = (wave, self._pad(month), self._pad(orbits[0]), self._pad(orbits[-1]))
                tag = '{}_{}_{}_{}'.format(*formats)

                # heat map ozone
                tracer = reservoir['ozone'][index][mask]
                folder = '{}/total_ozone_{}.h5'.format(sink, tag)
                title = 'Total column ozone, {}, {}'.format(months[month], date)
                bracket = brackets['ozone'][wave]
                name = 'ozone'.format(wave)
                units = '  total ozone [ DU ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map ozone
                tracer = reservoir['cloud_tau'][index][mask]
                folder = '{}/cloud_tau_{}.h5'.format(sink, tag)
                title = 'Cloud optical thickness, 360nm, {}, {}'.format(months[month], date)
                bracket = brackets['cloud'][wave]
                name = 'cloud'.format(wave)
                units = '  cloud optical thickness [ 1 ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map chlorophyll
                tracer = reservoir['chlorophyll'][index][mask]
                folder = '{}/chlorophyll_{}.h5'.format(sink, tag)
                title = 'Chlorophyll concentration, {}, {}'.format(months[month], date)
                bracket = brackets['chlorophyll'][wave]
                name = 'chlorophyll'.format(wave)
                units = '  chlorophyll [ mg / m^3 ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map clear sky radiance
                tracer = reservoir['clear_sky'][index][mask]
                folder = '{}/clear_sky_{}.h5'.format(sink, tag)
                title = 'Clear sky irradiance, Eclr, 310nm, {}, {}'.format(months[month], date)
                bracket = brackets['planar'][wave]
                name = 'clear_sky'.format(wave)
                units = '  irradiance [ mW / m^2 nm ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map surface irradiance
                tracer = reservoir['irradiance_planar'][index][mask]
                folder = '{}/planar_surface_{}.h5'.format(sink, tag)
                title = 'Surface irradiance, Ed, 310nm, {}, {}'.format(months[month], date)
                bracket = brackets['planar'][wave]
                name = 'planar'.format(wave)
                units = '  irradiance [ mW / m^2 nm ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map underwater irradiance
                tracer = reservoir['irradiance_five'][index][mask]
                folder = '{}/planar_five_{}.h5'.format(sink, tag)
                title = 'Irradiance, 5m depth, 310nm, {}, {}'.format(months[month], date)
                bracket = brackets['five'][wave]
                name = 'planar_five'.format(wave)
                units = '  irradiance [ mW / m^2 nm ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map coeefficieent
                tracer = reservoir['coefficient_planar'][index][mask]
                folder = '{}/coefficient_{}.h5'.format(sink, tag)
                title = 'Diffuse attenuation coefficient, Kd, 310nm, {}, {}'.format(months[month], date)
                bracket = brackets['constant'][wave]
                name = 'coefficient'.format(wave)
                units = '  Kd [ 1 / m ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

                # heat map penetrationn depth
                tracer = reservoir['depth'][index][mask]
                folder = '{}/depth_{}.h5'.format(sink, tag)
                title = 'Penetration depth, zp, 310nm, {}, {}'.format(months[month], date)
                bracket = brackets['depth'][wave]
                name = 'depth'.format(wave)
                units = '  penetration depth [ m ]'.format(wave)
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer, ordinate, abscissa]
                self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

                print(name, tracer[pixel])

            # find closest gyre spectrum
            spectra = gyre[month]
            spectra.sort(key=lambda entry: entry['distance'])
            datum = spectra[0]

            print('distance: ', datum['distance'])

            # set formats
            formats = [abs(int(coordinate)) for coordinate in location] + [dates[month]]

            # grab dna damage spectrum
            hydraii = Hydra('/user/1001/home/mbandel/studies/diatom/pace/spectra')
            hydraii.ingest('DNA_Damage')
            damage = hydraii.grab('dna_damage')

            # plot spectrum
            folder = '{}/dna_damage_spectrum_{}.h5'.format(sinkii, self._pad(month))
            title = 'DNA damage spectrum'
            name = 'dna_damage_spectrum'
            self.squid.ink(name, damage, 'wavelength', datum['wavelength'], folder, title)

            # plot spectrum
            folder = '{}/underwater_spectrum_{}_05.h5'.format(sinkii, self._pad(month))
            title = 'Southern Gyre, 5m underwater irradiance, {}S, {}W, {}'.format(*formats)
            name = 'underwater_spectrum'
            self.squid.ink(name, datum['spectrum'], 'wavelength', datum['wavelength'], folder, title)

            # plot spectrum
            folder = '{}/underwater_spectrum_{}_20.h5'.format(sinkii, self._pad(month))
            title = 'Southern Gyre, 20m underwater irradiance, {}S, {}W, {}'.format(*formats)
            name = 'underwater_spectrum'
            self.squid.ink(name, datum['spectrumii'], 'wavelength', datum['wavelength'], folder, title)

            # plot weighted
            folder = '{}/weighted_spectrum_{}_05.h5'.format(sinkii, self._pad(month))
            title = 'Southern Gyre, 5m underwater DNA damage rate, {}S, {}W, {}'.format(*formats)
            name = 'dna_damage_rate'
            self.squid.ink(name, damage * datum['spectrum'], 'wavelength', datum['wavelength'], folder, title)

            # plot weighted
            folder = '{}/weighted_spectrum_{}_20.h5'.format(sinkii, self._pad(month))
            title = 'Southern Gyre, 20m underwater DNA damage rate, {}S, {}W, {}'.format(*formats)
            name = 'dna_damage_rate'
            self.squid.ink(name, damage * datum['spectrumii'], 'wavelength', datum['wavelength'], folder, title)

            # for each wave length
            for index, wave in enumerate(waves):

                # construct the coordiantes
                latitude = reservoir['latitude'][index]
                longitude = reservoir['longitude'][index]
                row = reservoir['row'][index]

                # get ozone and clear sky
                ozone = reservoir['ozone'][index]
                sky = reservoir['clear_sky'][index]

                # for various latitudes
                for parallel in (-60, -40, -20, 0, 20, 40, 60):

                    # define coordinate
                    north = str(abs(parallel)) + 'S' * int(parallel < 0) + 'N' * int(parallel >= 0)

                    # create mask for points within 1 degree
                    mask = ((latitude - parallel) ** 2) < 0.25

                    # plot ozone
                    title = 'Total Ozone, {}, Latitude {}'.format(dates[month], north)
                    address = '{}/Ozone_{}_{}.h5'.format(sinkiii, dates[month], north)
                    self.squid.ink('ozone', ozone[mask], 'longitude', longitude[mask], address, title)

                    # plot clear sky
                    title = 'Clear Sky irradiance, 310nm, {}, Latitude {}'.format(dates[month], north)
                    address = '{}/Clear_sky_{}_{}.h5'.format(sinkiii, dates[month], north)
                    self.squid.ink('clear_sky', sky[mask], 'longitude', longitude[mask], address, title)

                    # plot ozone vs row
                    title = 'Total Ozone, {}, Latitude {}'.format(dates[month], north)
                    address = '{}/Ozone_{}_{}_row.h5'.format(sinkiii, dates[month], north)
                    self.squid.ink('row_ozone', ozone[mask], 'row', row[mask], address, title)

                    # plot clear sky vs row
                    title = 'Clear Sky irradiance, 310nm, {}, Latitude {}'.format(dates[month], north)
                    address = '{}/Clear_sky_{}_{}_row.h5'.format(sinkiii, dates[month], north)
                    self.squid.ink('row_clear_sky', sky[mask], 'row', row[mask], address, title)

                    # plot clear sky vs ozone
                    title = 'Ozone vs Clear Sky irradiance, 310nm, {}, Latitude {}'.format(dates[month], north)
                    address = '{}/Ozone_Clear_sky_{}_{}_row.h5'.format(sinkiii, dates[month], north)
                    self.squid.ink('ozone_clear_sky', sky[mask], 'ozone', ozone[mask], address, title)

        return None

    def section(self, row, latitude):
        """Create sectional difference plots centered on a row and latitude.

        Arguments:
            row: index of row for centering comparison
            latitude: latitude of pixel for centering comparison

        Returns:
            None
        """

        # create ascii, omocned data pairs
        comparisons = {'n331': 'n_values_0', 'n340': 'n_values_1'}
        comparisons.update({'n360': 'n_values_2', 'n380': 'n_values_3'})
        comparisons.update({'erythemaii': 'erythema_scalar_Eo'})
        comparisons.update({'ozone': 'ozone'})
        comparisons.update({'pteran': 'terrain_pressure'})
        comparisons.update({'chlorophyll': 'chlorophyll'})

        # grab the grid file
        grid = Hydra('{}/data/grid'.format(self.sink))
        grid.ingest()

        # grab the latitudes grid
        latitudes = grid.grab('omocned_latitude')
        closest = numpy.argsort((latitudes[:, row] - latitude) ** 2)[0]
        abscissa = numpy.array(range(60))

        # for eadh comparison
        for first, second in comparisons.items():

            # get the datasets
            array = grid.grab('ascii_{}'.format(first))[closest]
            arrayii = grid.grab('omocned_{}'.format(second))[closest]

            # take the difference
            difference = arrayii - array
            formats = (first, row, closest, round(latitude, 2))
            title = 'Difference, OMOCNUV - Ascii {}, row {}, image {} ( {} deg N )'.format(*formats)
            address = 'plots/sections/{}_Section_Difference_{}.h5'.format(first, self._pad(closest, 5))
            self.squid.ink('difference_{}'.format(first), difference, 'row', abscissa, address, title)

            # add the error
            error = 100 * ((arrayii / array) - 1)
            formats = (first, row, closest, round(latitude, 2))
            title = 'Percent Error, OMOCNUV - Ascii {}, row {}, image {} ( {} deg N )'.format(*formats)
            address = 'plots/sections/{}_Section_Error_{}.h5'.format(first, self._pad(closest, 5))
            self.squid.ink('percent_error_{}'.format(first), error, 'row', abscissa, address, title)

        return None

    def seek(self, target='erythemaii', tag='bad', pixel=None):
        """Seek out the biggest difference in a field, and check all other differences.

        Arguments:
            target: str, the field to check
            tag: str, tag of particular run

        Returns:
            None
        """

        # create ascii, omocned data pairs
        comparisons = {'n331': 'n_values_0', 'n340': 'n_values_1'}
        comparisons.update({'n360': 'n_values_2', 'n380': 'n_values_3'})
        comparisons.update({'erythema': 'erythema_planar'})
        comparisons.update({'erythemaii': 'erythema_scalar'})
        comparisons.update({'dose': 'dna_dose_planar'})
        comparisons.update({'doseii': 'dna_dose_scalar'})
        comparisons.update({'ozone': 'ozone'})
        comparisons.update({'pteran': 'terrain_pressure'})
        comparisons.update({'chlorophyll': 'chlorophyll'})
        comparisons.update({'algflg': 'algorithm_flag'})
        comparisons.update({'zenith': 'solar_zenith_angle'})
        comparisons.update({'satza': 'satellite_zenith_angle'})
        comparisons.update({'phi': 'relative_azimuth_angle'})
        comparisons.update({'lat': 'latitude'})
        comparisons.update({'lon': 'longitude'})

        # add all uv wavelengths
        waves = list(range(110))
        comparisons.update({'constant_{}'.format(wave): 'constant_planar_Kdm_{}'.format(wave) for wave in waves})
        comparisons.update({'constantii_{}'.format(wave): 'constant_scalar_Kom_{}'.format(wave) for wave in waves})
        comparisons.update({'ultraviolet_{}'.format(wave): 'ultraviolet_spectrum_{}'.format(wave) for wave in waves})

        # add prefixes
        comparisons = {'ascii_{}'.format(field): 'omocned_{}'.format(fieldii) for field, fieldii in comparisons.items()}

        # grab the grid file
        hydra = Hydra('{}/data/grid'.format(self.sink))
        path = [path for path in hydra.paths if path.endswith('{}.h5'.format(tag))][0]
        hydra.ingest(path)

        # make latitude mask
        latitude = hydra.grab('latitude').flatten()
        mask = abs(latitude) <= 50

        # or pixel mask
        if pixel:

            # get unflattened lattidue
            grid = hydra.grab('latitude')
            check = grid[pixel[0], pixel[1]]
            mask = latitude == check

        # determine highest error
        target = 'ascii_{}'.format(target)
        original = hydra.grab(target).flatten()[mask]
        novel = hydra.grab(comparisons[target]).flatten()[mask]
        error = 100 * ((novel / original) - 1)
        worst = numpy.argsort(abs(error))[-1]

        # begin report
        report = ['Ascii vs OMOCNUV, worst error for {}:'.format(target)]
        report += ['{}'.format(error[worst])]

        # for each comparison pair
        errors = []
        for field, fieldii in comparisons.items():

            # get the data
            original = hydra.grab(field).flatten()[mask]
            novel = hydra.grab(fieldii).flatten()[mask]
            error = 100 * ((novel / original) - 1)

            # add to errors
            errors.append((field, error[worst], original[worst], novel[worst]))

        # sort errors
        errors.sort(key=lambda entry: abs(entry[1]), reverse=True)
        [report.append('{}: {}, ascii: {}, omocned: {}'.format(*entry)) for entry in errors]

        # jot report
        destination = '{}/reports/worst_{}_{}.txt'.format(self.sink, tag, target)
        self._jot(report, destination)

        return None

    def select(self, orbit, date, source, sink, tag='', template=''):
        """Create specifiy control file information.

        Arguments:
            orbit: int, orbit number
            date: str, YYYY-mm-dd, the date
            source: str, path to dscription file folder
            sink: str path to outputs
            tag: str, tag for run

        Returns:
            dict, replace parameters for control file
        """

        # set default template
        template = template or 'OMOCNUV_omi_template.nc'

        # begin replacements
        replacements = {'inputs': {}, 'outputs': {}, 'runtime': {}}

        # get description file
        description = self._acquire('{}/Description.txt'.format(source))

        # deconstruct date
        year, month, day = date.split('-')

        # designate differet archive sets
        archives = {'OMGLER': 70004, 'OMTO3': 10004, 'OMAERUV': 10034}

        # for each dynamic input
        for member in description['Dynamic Input Files']:

            # get esdt nname
            earth = member['ESDT']

            # print
            self._print('gathering {}...'.format(earth))

            # set archive set, defaulting to 70004
            archive = archives.get(earth, 10004)

            # construct folder path
            folder = '/tis/acps/OMI/{}/{}/{}/{}/{}'.format(archive, earth, year, month, day)

            # initialize hydra, and get subset of orbital paths
            hydra = Hydra(folder)
            paths = [path for path in hydra.paths if str(orbit) in path]

            # adjust for RowAnomaly, daily file
            if earth == 'OMTO3RAFLG':

                # construct folder path
                folder = '/tis/acps/OMI/{}/{}/{}/{}'.format(archive, earth, year, month, day)

                # initialize hydra, and get subset of orbital paths
                hydra = Hydra(folder)
                paths = [path for path in hydra.paths if '{}m{}{}'.format(year, month, day) in path]

            # adjust for irradiancd, daily file
            if earth == 'OML1BIRR':

                # construct folder path
                folder = '/tis/acps/OMI/{}/{}/{}/{}/{}'.format(archive, earth, year, month, day)

                # initialize hydra, and get subset of orbital paths
                hydra = Hydra(folder)
                paths = hydra.paths

            # try to
            try:

                # add entry to replacements
                replacements['inputs'][earth] = paths[0]

            # unless not found
            except IndexError:

                # in which case alert, and add blank
                self._print('no {} found!'.format(earth))
                replacements['inputs'][earth] = ''

        # add start date to runtime
        replacements['runtime']['StartTime'] = date

        # add template to runtime
        replacements['runtime']['Template'] = template

        # replace PGE Version
        replacements['runtime']['PGEVersion'] = description['APP Version']

        # make sink folder
        self._make(sink)

        # construct output file
        earth = description['Output Files'][0]['ESDT']
        version = str(description['APP Version']).replace('.', '')
        collection = '004'
        time = re.findall('t[0-9]{4}-', replacements['inputs']['OMTO3'])[0][1:-1]
        stamp = self._note()
        tag = '_' * int(len(tag) > 0) + tag
        formats = (sink, earth, year, month, day, time, self._pad(orbit, 6), collection, stamp, tag)
        name = '{}/OMI-Aura_L2_{}_{}m{}{}t{}-o{}_v{}-{}{}.nc'.format(*formats)

        # add to replacements
        replacements['outputs'][earth] = name

        return replacements

    def sense(self, waves=(310, 360, 390), source='sensitivity', sink='sensitivity', resolution=10):
        """Perform sensitivity analysis to pressure

        Arguments:
            waves: tup of ints, wavelengths
            source: str, source folder
            sink: str, sink folder for plots.

        Returns:
            None
        """

        # make sink directory
        self._make('{}/plots/{}'.format(self.sink, sink))

        # grab hydra
        hydra = Hydra('{}/{}'.format(self.sink, source))

        # ingest original data
        hydra.ingest('original')
        wavelength = hydra.grab('spectral_wavelength')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        sky = hydra.grab('clear_sky')
        surface = hydra.grab('irradiance_planar')
        pressure = hydra.grab('surface_pressure')
        reflectivity = hydra.grab('reflectivity')[:, :, 1]
        reflectance = hydra.grab('n_value')[:, :, 2]
        quality = hydra.grab('quality_flag')
        residue = hydra.grab('residue')
        water = hydra.grab('water_fraction')
        ice = hydra.grab('snow_ice_flag')
        land = hydra.grab('land_classification')
        cloud = hydra.grab('cloud_tau')
        ozone = hydra.grab('column_amount_ozone')
        ground = hydra.grab('ground_pixel_quality')
        bits = hydra.grab('bit_flag')
        minimum = hydra.grab('minimum_reflectivity')
        rows = numpy.array([list(range(60))] * latitude.shape[0])

        # grab angles
        zenith = hydra.grab('solar_zenith')
        zenithii = hydra.grab('viewing_zenith')
        azimuth = hydra.grab('relative_azimuth')

        # convert to radiancs
        radians = (numpy.pi / 180)
        zenith = zenith * radians
        zenithii = zenithii * radians
        azimuth = (180 - azimuth) * radians

        # calculate glint angle arccos[ cos cosii - sin sinii cosiii ]
        cosines = numpy.cos(zenith) * numpy.cos(zenithii)
        sines = numpy.sin(zenith) * numpy.sin(zenithii) * numpy.cos(azimuth)
        glint = numpy.arccos(cosines - sines)
        glint = glint / radians

        # get orbit and date
        date = self._stage(hydra.current)['day']
        orbit = self._stage(hydra.current)['orbit']

        # ingest second set
        hydra.ingest('high')
        skyii = hydra.grab('clear_sky')
        surfaceii = hydra.grab('irradiance_planar')
        pressureii = hydra.grab('surface_pressure')
        residueii = hydra.grab('residue')

        # for each wave
        for wave in waves:

            # get the wave index
            index = self._pin(wave, wavelength)[0][0]

            # construct ordinate for clear sky
            percent = 100 * ((skyii[:, :, index] / sky[:, :, index]) - 1)
            mask = numpy.isfinite(percent) & (sky[:, :, index] > 0) & (skyii[:, :, index] > 0)
            maskii = (pressureii > 1) & (quality < 2)
            masque = mask & maskii
            ordinate = percent[masque]
            abscissa = latitude[masque]

            # make plot
            address = 'plots/{}/Clear_sky_surface_pressure_sensitivity_{}.h5'.format(sink, wave)
            title = 'Clear sky percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('clear_sky_percent', ordinate, 'latitude', abscissa, address, title)

            # construct ordinate for surface
            percent = 100 * ((surfaceii[:, :, index] / surface[:, :, index]) - 1)
            mask = numpy.isfinite(percent) & (surface[:, :, index] > 0) & (surfaceii[:, :, index] > 0)
            maskii = (pressureii > 1) & (quality < 2)
            masque = mask & maskii
            ordinate = percent[masque]
            abscissa = latitude[masque]

            # make plot
            address = 'plots/{}/Surface_irradiance_surface_pressure_sensitivity_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent', ordinate, 'latitude', abscissa, address, title)

            # make abscissa reflectivity
            abscissa = reflectivity[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_sensitivity_{}_reflectivity.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_reflectivity', ordinate, 'reflectivity', abscissa, address, title)

            # make abscissa sun glint angle
            abscissa = glint[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_sensitivity_{}_glint.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_glint', ordinate, 'sun_glint_angle', abscissa, address, title)

            # make abscissa residue
            abscissa = residue[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_residue_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_residue', ordinate, 'residue', abscissa, address, title)

            # make abscissa residueii
            abscissa = residueii[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_residueii_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_residueii', ordinate, 'residueii', abscissa, address, title)

            # make abscissa water fraction
            abscissa = water[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_water_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_water', ordinate, 'water', abscissa, address, title)

            # make abscissa snow ice
            abscissa = ice[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_ice_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_ice', ordinate, 'ice', abscissa, address, title)

            # make abscissa land classification
            abscissa = land[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_land_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_land', ordinate, 'land', abscissa, address, title)

            # make abscissa cloud classification
            abscissa = cloud[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_cloud_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_cloud', ordinate, 'cloud', abscissa, address, title)

            # make abscissa reflectance classification
            abscissa = reflectance[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_reflectance_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_reflectance', ordinate, 'reflectance', abscissa, address, title)

            # make abscissa ozone classification
            abscissa = ozone[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_ozone_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_ozone', ordinate, 'ozone', abscissa, address, title)

            # make abscissa ground classification
            abscissa = ground[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_ground_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_ground', ordinate, 'ground', abscissa, address, title)

            # make abscissa bits classification
            abscissa = bits[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_bits_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_bits', ordinate, 'bits', abscissa, address, title)

            # make abscissa minimum classification
            abscissa = minimum[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_minimum_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_minimum', ordinate, 'minimum', abscissa, address, title)

            # make abscissa longitude classification
            abscissa = longitude[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_longitude_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_longitude', ordinate, 'longitude', abscissa, address, title)

            # make abscissa rows classification
            abscissa = rows[masque]
            address = 'plots/{}/Surface_irradiance_surface_pressure_rows_{}.h5'.format(sink, wave)
            title = 'Surface Irradiance percent difference with 5% pressure increase, '
            title += '{}nm, {}, o{}'.format(wave, date, orbit)
            self.squid.ink('surface_irradiance_percent_rows', ordinate, 'rows', abscissa, address, title)

            # recalcualte percent for map
            percent = 100 * ((surfaceii[:, :, index] / surface[:, :, index]) - 1)
            polygons = self._polymerize(latitude, longitude, resolution)
            percentii = self._resolve(percent, resolution)
            latitudeii = self._resolve(latitude, resolution)
            longitudeii = self._resolve(longitude, resolution)
            pressureiii = self._resolve(pressureii, resolution)
            qualityii = self._resolve(quality, resolution)

            # make mask
            # mask = numpy.isfinite(percent) & (surface[:, :, index] > 0) & (surfaceii[:, :, index] > 0)
            mask = numpy.isfinite(percentii)
            maskii = (pressureiii > 1) & (qualityii < 2)
            maskiii = (polygons[:, :, 4:].std(axis=2) < 20)
            masque = mask & maskii & maskiii

            # make bracket
            bracket = [-12, -10, -8, -6, -4, -2, 0, 2]

            # plot map
            path = hydra.paths[0]
            address = 'plots/sensitivity/Percent_difference_contours_{}.h5'.format(wave)
            formats = (self._stage(path)['day'], self._stage(path)['orbit'], wave)
            title = 'Percent difference 5%, {}, {}_{}'.format(*formats)
            name = 'surface_irradiance_percent_difference'
            units = '%'
            texts = [name, 'latitude', 'longitude']
            arrays = [percentii[masque], latitudeii[masque], longitudeii[masque]]
            self.squid.pulsate(texts, arrays, polygons[masque], address, bracket, title, units)

        return None

    def shift(self, source=None, sourceii=None, images=(998, 1001), rows=(9, 20), pixels=(49, 65)):
        """Compare wavelengths after wavelength shift method to those before.

        Arguments:
            source: str, filepath to fortran texts source folder
            sourceii: str, filepath to hdf level 1b data
            images: tuple of ints, images bracket
            rows: tuple of ints, rows bracket
            pixels: tuple of ints, pixels bracket

        Returns:
            None
        """

        # set default folders
        source = source or '../studies/diatom/shift'
        sourceii = sourceii or '../studies/diatom/six'

        # load up text filase
        text = self._know('{}/shift.txt'.format(source))
        textii = self._know('{}/shiftii.txt'.format(source))

        # make hydra for level1b data and arrange paths by product
        hydra = Hydra(sourceii)
        paths = self._group(hydra.paths, lambda path: self._stage(path)['product'])

        # grab the radiance data
        hydra.ingest(paths['OML1BRVG'][0])
        wavelength = hydra.grab('wavelength').squeeze()
        shift = hydra.grab('wavelength_shift').squeeze()
        coefficient = hydra.grab('wavelength_coefficient').squeeze()
        reference = hydra.grab('wavelength_reference_column')
        latitude = hydra.grab('latitude').squeeze()

        # take subsets based on brackets
        wavelength = wavelength[rows[0]: rows[1], pixels[0]: pixels[1]]
        shift = shift[images[0]: images[1], rows[0]: rows[1]]
        coefficient = coefficient[images[0]: images[1], rows[0]: rows[1], :]
        reference = hydra.grab('wavelength_reference_column') - pixels[0]
        latitude = latitude[images[0]: images[1], rows[0]: rows[1]]

        # calculate by coefficient expansiom
        expansion = self._calculate(coefficient, reference, pixels[1] - pixels[0])

        # calculate by wavelength + wavelength shift
        nominals = numpy.array([wavelength] * shift.shape[0])
        shifts = numpy.array([shift] * wavelength.shape[1]).transpose(1, 2, 0)
        summation = nominals + shifts

        # begin data
        data = {}
        dataii = {}

        # for each set
        for record, reservoir in zip((text, textii), (data, dataii)):

            # make triplets
            zipper = zip(record[:-2], record[1:-1], record[2:])
            triplets = [triplet for index, triplet in enumerate(zipper) if index % 3 == 0]

            # for each triplet
            for one, two, three in triplets:

                # get image and row from first
                tokens = one.split()
                image = tokens[2]
                row = tokens[3]

                # get radiance from second line
                tokens = two.split()
                radiance = [float(entry) for entry in tokens[2:]]

                # get irradiance from thrid
                tokens = three.split()
                irradiance = [float(entry) for entry in tokens[2:]]

                # add entry
                reservoir[(image, row)] = {'radiance': radiance, 'irradiance': irradiance}

        # collect histograms for radiance and irradiance
        histogram = []
        histogramii = []
        
        # collect fortran
        expansionii = []
        summationii = []

        # compare
        for pixel in data.keys():

            # compare radiance and irradiance
            difference = numpy.array(dataii[pixel]['radiance']) - numpy.array(data[pixel]['radiance'])
            differenceii = numpy.array(dataii[pixel]['irradiance']) - numpy.array(data[pixel]['irradiance'])

            # add to histogram
            histogram += difference.tolist()
            histogramii += differenceii.tolist()

            # add radiance data
            expansionii += data[pixel]['radiance']
            summationii += dataii[pixel]['radiance']

        # for each mode
        modes = ('radiance', 'irradiance')
        grams = (histogram, histogramii)
        for mode, gram in zip(modes, grams):

            # plot histograms
            address = 'plots/trouble/wavelength_shift_differences_{}.h5'.format(mode)
            title = 'Wavelength calculation differences Horner vs Wavelength Shift, {}, '.format(mode.capitalize())
            title += 'Scans 998 - 1000, Rows 10 - 20'
            self.squid.ripple('wavelength_difference_{}'.format(mode), gram, address, title)

        # plot all sequences by latitude
        latitude = numpy.array([latitude] * (pixels[1] - pixels[0])).transpose(1, 2, 0)
        latitude = latitude.flatten()
        sequences = (expansion, summation, expansionii, summationii)
        sequences = [numpy.array(sequence).flatten() for sequence in sequences]
        modes = ('python_expansion', 'python_shift', 'fortran_expansion', 'fortran_shift')
        for mode, sequence in zip(modes, sequences):

            # make plots of wavelengths
            address = 'plots/trouble/Wavelengths_{}.h5'.format(mode)
            formats = (*images, *rows, *pixels, mode)
            title = 'Wavelengths, 2006-12-23, Images: {}-{}, Rows: {}-{}, Pixels: {}-{}, {}'.format(*formats)
            self.squid.ink('wavelenegth_{}'.format(mode), sequence, 'latitude', latitude, address, title)

        # compare each to python coefficient expansion
        for mode, sequence in zip(modes[1:], sequences[1:]):

            # make plot of differences
            difference = sequence - sequences[0]
            address = 'plots/trouble/Wavelengths_Diif_{}.h5'.format(mode)
            formats = (*images, *rows, *pixels, mode)
            title = 'Wavelengths, 2006-12-23, Images: {}-{}, Rows: {}-{}, Pixels: {}-{}, {}'.format(*formats)
            self.squid.ink('wavelenegth_difference_{}'.format(mode), difference, 'latitude', latitude, address, title)

        return None

    def simulate(self, path=None, ancillary=None, sink='proxy', reflectance=None, subset=(2000, 2000)):
        """Create a simulation file for running PACE simulated data through OMOCNUV

        Arguments:
            path: str, file path for Level1B file
            ancillary: str, file path for ancillary file
            sink: str, sink folder name
            reflectance: reflectance area ( takes time to load each time )
            subset: tuple of ints, the subset

        Returns:
            None
        """

        # set default paths
        path = path or '../studies/diatom/simulation/ten/PACE_OCI_SIM.20220321T182542.L1B.V10.nc'
        ancillary = ancillary or '../studies/diatom/simulation/ten/PACE_OCI_SIM.20220321T182542.MODEL_INPUTS.V10.nc'

        # ingest level1B file
        hydra = Hydra(path)
        hydra.ingest()

        # get geolocation data
        time = hydra.grab('time')[:subset[0]]
        latitude = hydra.grab('latitude')[:subset[0], :subset[1]]
        longitude = hydra.grab('longitude')[:subset[0], :subset[1]]

        # get angle geomtry
        zenith = hydra.grab('solar_zenith')[:subset[0], :subset[1]]
        zenithii = hydra.grab('sensor_zenith')[:subset[0], :subset[1]]
        azimuth = hydra.grab('solar_azimuth')[:subset[0], :subset[1]]
        azimuthii = hydra.grab('sensor_azimuth')[:subset[0], :subset[1]]

        # get the wavelength
        wavelength = hydra.grab('blue_wavelength')

        # if reflectance not given ( takes time to load in )
        if not numpy.array(reflectance).any():

            # grab from file
            reflectance = hydra.grab('rhot_blue')

        # take reflectance subset
        reflectance = reflectance[:, :subset[0], :subset[1]]

        # set up needed wavelengths
        wavelengthii = numpy.linspace(325, 385, 61)

        print(reflectance.shape)
        print(wavelength.shape)
        print(wavelengthii.shape)

        # create interpolation function, and interpolate to regular grid
        interpolator = scipy.interpolate.interp1d(wavelength, reflectance, kind='linear', axis=0)
        reflectance = interpolator(wavelengthii)

        # # double the density of points with linear interpolation
        # reflectance = self._double(reflectance)
        # wavelength = self._double(wavelength)

        # # extract only the wavelengths of choice +/- 2 nm
        # waves = [331, 340, 360, 380]
        # masque = numpy.zeros(wavelength.shape)
        # width = 2
        # for wave in waves:
        #
        #     # create mask for wavelength
        #     mask = (wavelength > wave - width) & (wavelength < wave + width)
        #     masque = numpy.logical_or(masque, mask)

        # # apply mask
        # masque = numpy.array([[masque] * reflectance.shape[1]] * reflectance.shape[2]).transpose(2, 1, 0)
        # reflectance = numpy.where(masque, reflectance, 0)
        # # wavelength = numpy.where(masque, wavelength, 0)

        self._print('reflectance: {}'.format(reflectance.shape))
        self._print('wavelength: {}'.format(wavelength.shape))

        # get the ancillary data
        hydraii = Hydra(ancillary)
        hydraii.ingest()

        # get ancillary data
        pressure = hydraii.grab('surface_pressure')[:subset[0], :subset[1]]
        ozone = hydraii.grab('ozone')[:subset[0], :subset[1]]
        chlorophyll = hydraii.grab('chlor_a')[:subset[0], :subset[1]]
        water = hydraii.grab('watermask')[:subset[0], :subset[1]]
        aerosol = hydraii.grab('aot_550')[:subset[0], :subset[1]]
        aerosolii = hydraii.grab('aot_865')[:subset[0], :subset[1]]

        # begin data addresses and arrays
        addresses = []
        arrays = []
        shapes = []
        attributes = []

        # get base files
        base = Hydra('../studies/diatom/simulation/base')

        # begin global attributes
        globals = {}

        # set exlusions
        exclusions = ['BAND_1', 'BAND1']

        # for each base path
        for path in base.paths:

            # ingest
            base.ingest(path, net=True)

            # get global attributes
            globals.update(base.attribute())

            # for each feature
            for feature in base:

                # remove exclusions
                if not any([exclusion in feature.slash for exclusion in exclusions]):

                    # add address to addresses and zero array to arrays
                    addresses.append(feature.slash)
                    shapes.append(feature.shape)
                    attributes.append(feature.attributes)

        # # add dummy metadata to globals
        # globals.update({'RangeBeginningDateTime': globals['time_coverage_start']})
        # globals.update({'RangeEndingDateTime': globals['time_coverage_end']})
        # globals.update({'EquatorCrossingDateTime': '13:35:00'})
        # globals.update({'EquatorCrossingLongitude': 0})

        # determine track length from scanline feature
        track = base.dig('scanline')[0].shape[0]

        # get all standard mode features that are dimensions to transfer to other groups
        modes = ['BAND3_RADIANCE/STANDARD_MODE', 'BAND3_STANDARD_MODE/INSTRUMET']

        # fpor each mode
        for mode in modes:

            # create transfer features
            transfers = [feature for feature in base if mode in feature.slash]
            transfers = [feature for feature in transfers if feature.attributes.get('dimension', True)]

            # for each transfer
            for feature in transfers:

                # for each address stub
                stubs = ['HDFEOS/SWATHS', 'BAND2_ANCILLARY/STANDARD_MODE']
                for stub in stubs:

                    # add address to addresses and zero array to arrays
                    addresses.append(feature.slash.replace(mode, stub))
                    shapes.append(feature.shape)
                    attributes.append(feature.attributes)

        # create coversion dictionary
        conversion = {track: reflectance.shape[1]}
        conversion.update({30: reflectance.shape[2], 60: reflectance.shape[2]})
        conversion.update({159: reflectance.shape[0], 557: reflectance.shape[0], 751: reflectance.shape[0]})

        # for each array
        for shape, attribute in zip(shapes, attributes):

            # use conversion to rewrite the shape
            sizes = [conversion.get(size, size) for size in shape]
            shapeii = tuple(sizes)

            # if size
            if 'size' in attribute.keys():

                # convert size
                attribute['size'] = conversion.get(attribute['size'], attribute['size'])

            # add dummy array with new size
            arrays.append(numpy.zeros(shapeii))

        # convert reflectance by dividing by pi and multiplying by cosine SZA
        radians = (numpy.pi / 180)
        cosines = numpy.cos(zenith * radians)
        cosines = cosines.reshape(*cosines.shape, 1)

        # add radiance data, rearranging and duplicating for band 3, dividing by 4 pi steridians
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance')
        array = numpy.array([reflectance.transpose(1, 2, 0)])
        [arrays.append(array * cosines / math.pi) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # add irradiance data as dummy of 1's, as radiances are already normalized
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance')
        array = numpy.ones((1, 1, reflectance.shape[2], reflectance.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength coefficients for radiance, bands2 and 3, k0 = 310, k1 = 2.5
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        array = numpy.array([[[[wavelengthii[0], 1, 0, 0, 0]] * reflectance.shape[2]] * reflectance.shape[1]])
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'n_wavelength_poly')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength coefficients for irradiance, bands2 and 3, k0 = 310, k1 = 2.5
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_coefficient')
        array = numpy.array([[[[wavelengthii[0], 1, 0, 0, 0]] * reflectance.shape[2]]])
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'n_wavelength_poly')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create wavelength reference column for radiance and irradiance, bands2 and 3, r = 0
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/wavelength_reference_column')
        array = numpy.array([0])
        [arrays.append(array) for _ in range(4)]
        dimensions = ('time',)
        [attributes.append({'dimensions': dimensions}) for _ in range(4)]

        # create dummy spectral channel quality for radiance
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        addresses.append('BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        array = numpy.zeros((1, reflectance.shape[1], reflectance.shape[2], wavelengthii.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'ground_pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # create dummy spectral channel quality for irradiance
        addresses.append('BAND2_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        addresses.append('BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality')
        array = numpy.zeros((1, 1, reflectance.shape[2], wavelengthii.shape[0]))
        [arrays.append(array) for _ in range(2)]
        dimensions = ('time', 'scanline', 'pixel', 'spectral_channel')
        [attributes.append({'dimensions': dimensions}) for _ in range(2)]

        # add water fraction from pace water mask
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/water_fraction')
        arrays.append(numpy.array([water * 100]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add land water classification
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/land_water_classification')
        array = numpy.where(water == 0, 1, 7)
        arrays.append(array)
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy ground pixel quality
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/ground_pixel_quality')
        arrays.append(numpy.zeros((1, *water.shape)))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add solar zenith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/solar_zenith_angle')
        arrays.append(numpy.array([zenith]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add solar azimuith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/solar_azimuth_angle')
        arrays.append(numpy.array([azimuth]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add viewing zenith angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/viewing_zenith_angle')
        arrays.append(numpy.array([zenithii]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add viewing azimuth angle
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/GEODATA/viewing_azimuth_angle')
        arrays.append(numpy.array([azimuthii]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add time
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/time')
        arrays.append(numpy.array(time[:1]))
        dimensions = ('time',)
        attributes.append({'dimensions': dimensions})

        # add dummy time deltas
        addresses.append('BAND2_RADIANCE/STANDARD_MODE/OBSERVATIONS/delta_time')
        arrays.append(numpy.array(time - time[0]))
        dimensions = ('time', 'scanline')
        attributes.append({'dimensions': dimensions})

        # add ozone, mutliplying by 1000
        addresses.append('HDFEOS/SWATHS/OMI Column Amount O3/Data Fields/ColumnAmountO3')
        arrays.append(ozone * 1000)
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy algorithm flag
        addresses.append('HDFEOS/SWATHS/OMI Column Amount O3/Data Fields/AlgorithmFlags')
        arrays.append(numpy.ones(ozone.shape).astype(int))
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add surface pressure
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/PS')
        arrays.append(numpy.array([pressure * 100]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy row anomaly flag
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/row_anomaly')
        arrays.append(numpy.zeros((1, *water.shape)))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy snow / ice flag
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/snow_ice')
        array = numpy.where(water == 0, 0, 104)
        arrays.append(numpy.array([array]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add latitude
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/latitude')
        arrays.append(numpy.array([latitude]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add longitude
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/longitude')
        arrays.append(numpy.array([longitude]))
        dimensions = ('time', 'scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})

        # add dummy latitude bounds
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/latitude_bounds_fov75')
        arrays.append(numpy.zeros((1, *latitude.shape, 4)))
        dimensions = ('time', 'scanline', 'ground_pixel', 'ncorner')
        attributes.append({'dimensions': dimensions})

        # add dummy longitude bounds
        addresses.append('BAND2_ANCILLARY/STANDARD_MODE/GEODATA/longitude_bounds_fov75')
        arrays.append(numpy.zeros((1, *longitude.shape, 4)))
        dimensions = ('time', 'scanline', 'ground_pixel', 'ncorner')
        attributes.append({'dimensions': dimensions})

        # add chlorphyll scanline dimesion
        addresses.append('scanline')
        arrays.append(numpy.zeros((reflectance.shape[1],)))
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimension': True, 'size': reflectance.shape[1]})

        # add chlorphyll scanline dimesion
        addresses.append('ground_pixel')
        arrays.append(numpy.zeros((reflectance.shape[2],)))
        attributes.append({'dimension': True, 'size': reflectance.shape[2]})

        # add chlorphyll concentration
        addresses.append('chlorophyll_concentration')
        arrays.append(chlorophyll)
        dimensions = ('scanline', 'ground_pixel')
        attributes.append({'dimensions': dimensions})
        # attributes.append({})

        # add aerosol dimension
        addresses.append('aerosol_wavelength')
        arrays.append(numpy.zeros((3,)))
        attributes.append({'dimension': True, 'size': 3})

        # add aerosol information, double the third wavelength
        addresses.append('SCIDATA/FinalAerosolAbsOpticalDepth')
        array = numpy.array([aerosol, aerosolii, aerosolii]).transpose(1, 2, 0)
        arrays.append(array)
        dimensions = ('scanline', 'ground_pixel', 'aerosol_wavelength')
        attributes.append({'dimensions': dimensions})

        # add aerosol information, double the third wavelength
        addresses.append('Wavelengths')
        array = numpy.array([550, 865, 865])
        arrays.append(array)
        dimensions = ('aerosol_wavelength',)
        attributes.append({'dimensions': dimensions})

        # create data
        data = {address: array for address, array in zip(addresses, arrays)}
        attributes = {address: attribute for address, attribute in zip(addresses, attributes)}
        self._view(data)

        # set destination
        identifier = re.search("[0-9, A-Z]{15}", ancillary).group()
        destination = '{}/{}/Simulated_PACE_Data_{}.nc'.format(self.sink, sink, identifier)

        # create netcdf4 file
        self._stamp('writing {}...'.format(destination), initial=True)
        self.spawn(destination, data, attributes, net=True, globals=globals)
        self._stamp('wrote {}.'.format(destination))

        return None

    def smooth(self):
        """Create smoothed solar spectrums.

        Arguments:
            None

        Returns:
            None
        """

        # open tsis-1 spectrum
        spectrum = self._know('{}/solar/TSIS1.DAT'.format(self.sink))

        # split into wavelengths and radiances
        spectrum = [line.strip().split() for line in spectrum[1:]]

        # get wavelengths and irradiance
        wavelength = numpy.array([float(line[0]) for line in spectrum])
        irradiance = numpy.array([float(line[1]) for line in spectrum])

        # begin boxcars at 0.5nm and 5 nm
        boxcar = []
        boxcarii = []
        for wave in wavelength:

            # track progress
            if wave % 10 == 0:

                # print progress
                print('smoothing {}nm...'.format(wave))

            # gather 0.5nm indices
            indices = [index for index, waveii in enumerate(wavelength) if abs(wave - waveii) <= 0.25]
            irradianceii = irradiance[indices].mean()
            boxcar.append(irradianceii)

            # gather 5nm indices
            indices = [index for index, waveii in enumerate(wavelength) if abs(wave - waveii) <= 2.5]
            irradianceii = irradiance[indices].mean()
            boxcarii.append(irradianceii)

        # take boxcar ratio
        boxcar = numpy.array(boxcar)
        boxcarii = numpy.array(boxcarii)
        ratio = boxcarii / boxcar

        # make plot
        title = 'TSIS-1 solar spectrum'
        address = 'plots/solar/TSIS_00_solar_spectrum.h5'
        self.squid.ink('solar_spectrum', irradiance, 'wavelength', wavelength, address, title)

        # make plot of 0.5 boxcar
        title = 'TSIS-1 solar spectrum, 0.5nm boxcar'
        address = 'plots/solar/TSIS_05_solar_spectrum.h5'
        self.squid.ink('solar_spectrum', boxcar, 'wavelength', wavelength, address, title)

        # make plot of 5nm boxcar
        title = 'TSIS-1 solar spectrum, 5nm boxcar'
        address = 'plots/solar/TSIS_50_solar_spectrum.h5'
        self.squid.ink('solar_spectrum', boxcarii, 'wavelength', wavelength, address, title)

        # make plot of 5nm boxcar / 0.5nm boxcar ratio
        title = 'TSIS-1 solar spectrum, 0.5nm / 5nm boxcar ratio'
        address = 'plots/solar/Ratio_05nm_5nm_solar_spectrum.h5'
        self.squid.ink('ratio', ratio, 'wavelength', wavelength, address, title)

        # make plot of 0.5 boxcar, 1nm spacing
        indices = [index for index, wave in enumerate(wavelength) if wave % 1 == 0]
        title = 'TSIS-1 solar spectrum, 0.5nm boxcar, 1nm'
        address = 'plots/solar/TSIS_05_solar_spectrum_1nm.h5'
        self.squid.ink('solar_spectrum', boxcar[indices], 'wavelength', wavelength[indices], address, title)

        # make plot of 5nm boxcar, 1nm spacing
        title = 'TSIS-1 solar spectrum, 5nm boxcar, 1nm'
        address = 'plots/solar/TSIS_50_solar_spectrum_1nm.h5'
        self.squid.ink('solar_spectrum', boxcarii[indices], 'wavelength', wavelength[indices], address, title)

        return None

    def solarize(self):
        """Plot clear sky day, etc as ratio to solar spectrum.

        Arguments:
            None

        Returns:
            None
        """

        # get the interpolatin tables
        hydra = Hydra('../studies/diatom/simulation/tables')
        hydra.ingest(2)

        # get the latest omi file
        hydraii = Hydra('../studies/diatom/simulation/comparison')
        hydraii.ingest(0)

        # get the radiance table
        flux = hydra.grab('irradiance')
        wavelength = hydra.grab('wavelength')

        # get the solar spectrum from the 0 sza, 125 DU, equatorial, high reflectivity, 1atm case
        solar = numpy.exp(flux[0, :, 100, 6, 0])

        # plot solar
        address = 'plots/solar/125_DU_Solar_Flux.h5'
        title = 'Solar Spectrum, 1 atm, 125 DU, 100 reflectivity, 0 sza'
        self.squid.ink('flux', solar, 'wavelength', wavelength, address, title)

        # set up fields
        fields = ['clear_sky', 'surface_irradiance_planar', 'surface_irradiance_scalar']
        fields += ['underwater_irradiance_planar', 'underwater_irradiance_scalar']
        fields += ['attenuation_coefficient_planar', 'attenuation_coefficient_scalar']

        # get clear sky spectra
        quality = hydraii.grab('quality_flag_underwater')
        latitude = hydraii.grab('latitude')

        # make mask for quality 0
        mask = (quality == 0)

        # for each latitude
        for north in (-60, 0, 60):

            # find the closest pixel
            pixel = self._pin(north, latitude[mask])[0]

            # for each field
            for field in fields:

                # get the array
                array = hydraii.grab(field)

                # apply mask, and exponentiate
                array = array[mask]
                array = 10.0 ** array

                # plot radio of middle spectrum
                spectrum = array[pixel]
                ratio = spectrum / solar
                address = 'plots/solar/{}_flux_ratio_{}.h5'.format(field, self._orient(north))
                title = 'Ratio of {} to solar spectrum, {}'.format(field.replace('_', ''), self._orient(north))
                self.squid.ink('ratio_{}'.format(field), ratio, 'wavelength', wavelength, address, title)

        return None

    def speculate(self):
        """PLot action spectrum.

        Arguments:
            None

        Returns:
            None
        """

        # gather spectrum
        path = '{}/data/spectrum/action_spec.txt'.format(self.sink)
        spectrum = self._know(path)

        # remove comments
        spectrum = [line for line in spectrum if '#' not in line]

        # split into rows
        rows = [line.split() for line in spectrum]

        # begin features for plotting
        features = []

        # create wavelength abscissa
        name = 'wavelength'
        array = numpy.array([float(row[1]) for row in rows])
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # create erythemal
        name = 'erythemal'
        array = numpy.array([float(row[2]) for row in rows])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # create erythemal
        name = 'dna_damage'
        array = numpy.array([float(row[3]) for row in rows])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # create erythemal
        name = 'vitamin_d'
        array = numpy.array([float(row[4]) for row in rows])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # stash
        destination = '{}/plots/spectrum/Action_Spectrum.h5'.format(self.sink)
        self.stash(features,  destination)

        return rows

    def specify(self):
        """Create file specifications document from template file and description.

        Arguments:
            space: int, number of characters per entry

        Returns:
            None
        """

        # create folder for data
        folder = '{}/doc'.format(self.sink)
        folderii = '{}/tbl'.format(self.sink)

        # begin specifcations yaml
        specifications = {}

        # open description file as yaml
        description = self._acquire('{}/Description.txt'.format(folder))

        # grab current date
        now = datetime.datetime.now()

        # add to specifications
        specifications['Shortname'] = description['APP Name']
        specifications['Longname'] = description['Long Name']
        specifications['Version'] = description['APP Version']
        specifications['Date'] = '{} {} {}'.format(self._pad(now.month), self._pad(now.day), now.year)
        specifications['Author'] = description['Software Developer']
        specifications['App Version'] = description['APP Version']
        specifications['Lead Algorithm Scientist'] = description['Lead Algorithm Scientist']
        specifications['Lead Algorithm Developer'] = description['Other Algorithm Scientists'][0]
        specifications['Lead App Developer'] = description['Software Developer']
        specifications['Description'] = description['Description']

        # get netcdf OMOCNUV template file
        template = self._know('{}/OMOCNUV_omi_template.txt'.format(folderii))

        # set data types
        types = ['short', 'int', 'float']

        # strip all spaces
        template = [line.strip() for line in template]

        # get all variable blocks based on datatype
        blocks = [line for line in template if any([line.startswith(kind) for kind in types])]

        # begin variables dictionary
        variables = {}
        for line in blocks:

            # parse line
            name = line.split()[1].split('(')[0]
            kind = line.split()[0]
            dimensions = line.split('(')[1].split(')')[0]

            # create entry
            variables[name] = {'name': name, 'type': kind, 'dimensions': dimensions}

            # get all related lines
            lines = [line for line in template if line.startswith(name + ':')]
            for line in lines:

                # parse info
                field = line.split(':')[1].split('=')[0].strip()
                information = line.split('=')[1].split(';')[0].strip()

                # add to variables
                variables[name].update({field: information})

        # grab metadata yaml
        destination = '{}/metadata.yaml'.format(folderii)
        globals = self._acquire(destination)

        # if globals is empty
        if not globals:

            # get index of global metadata and endpoint
            index = [position for position, line in enumerate(template) if line.startswith('// global attributes')][0]
            indexii = [position for position, line in enumerate(template[index:]) if len(line) < 1][0] + index

            # begin metadata
            globals = []

            # for each metadata entry
            for line in template[index + 1:indexii]:

                # parse line
                name = line.split('=')[0].strip().strip(':')

                # create description
                text = self._serpentize(name).replace('_', ' ')
                if name == name.upper():

                    # just use lowercase
                    text = name.lower()

                # create global
                entry = {'Metadata Name': name}
                entry.update({'Mandatory': 'T'})
                entry.update({'Data Type': 'string'})
                entry.update({'Number of Values': 1})
                entry.update({'Range of Values': 'NA'})
                entry.update({'Data Source': 'netcdf4 template'})
                entry.update({'Description': text})

                # add entry
                globals.append(entry)

            # sort entries
            globals.sort(key=lambda entry: entry['Metadata Name'])

            # dispense yaml
            self._dispense(globals, destination)
            self._disperse(destination)

        # add to file spec
        specifications['Global Metadata'] = globals

        # get flag definitions yaml
        flags = self._acquire('{}/omocnuv_flags.yaml'.format(folderii))

        # get all variable field names
        names = list(variables.keys())
        names.sort()

        # begin file spec data fields
        fields = []
        for name in names:

            # get subset
            variable = variables[name]

            # begin specificaion
            field = {}

            # create entry
            field.update({'Field Name': name})
            field.update({'Data Type': 'netcdf4 {}'.format(variable['type'])})
            field.update({'Dimensions': variable['dimensions']})
            minimum = variable['valid_min'].strip('.f')
            maximum = variable['valid_max'].strip('.f')
            field.update({'Valid Range': '{} to {}'.format(minimum, maximum)})
            field.update({'Missing Value': variable['_FillValue'].strip('.f')})
            field.update({'Add Offset': variable.get('add_offset', '0').strip('.f')})
            field.update({'Scale Factor': variable.get('scale_factor', '1.0').strip('.f')})
            field.update({'Units': variable['units']})
            field.update({'Data Source': variable.get('source', '')})
            field.update({'Title': variable['long_name']})
            field.update({'Description': variable.get('comment', '')})

            # check for flag entries
            if name in flags.keys():

                # get dscription text
                text = field['Description']

                # add spacer
                text +='\n'

                # for each entry
                for bit, label in flags[name][0].items():

                    # add entry to text
                    text +='{}: {}\n'.format(bit, label)

                # update description
                field.update({'Description': text})

            # add to list
            fields.append(field)

        # add to specifications
        specifications['Data Fields'] = fields

        # dump file
        destination = '{}/OMOCNUV.fs'.format(folder)
        self._dispense(specifications, destination)
        self._disperse(destination)

        return None

    def splash(self, tag='coefficient', target=(-20, -110), depths=(5, 20)):
        """Create spectrum from erythema file.

        Arguments:
            tag: str for erythema file
            pixel: tuple of ints, the pixel to plot
            depths: tuple of floats, the depths for calculation

        Returns:
            None
        """

        # grab erythema hydra
        hydra = self._pull(tag)

        # get constants and uv data
        ultraviolet = hydra.grab('ultraviolet/planar')
        ultravioletii = hydra.grab('ultraviolet/scalar')
        constant = hydra.grab('constant_planar')
        constantii = hydra.grab('constant_scalar')
        wavelength = hydra.grab('wavelength')
        spectrum = hydra.grab('dna_damage_spectrum')

        # get latitude and longitude
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')

        # get closet pixel
        pixel = self._pin(target, [latitude, longitude])[0]

        # get the dna spectrum from action file
        action = Hydra('{}/plots/spectrum/Action_Spectrum.h5'.format(self.sink))
        action.ingest()
        spectrum = action.grab('dna_damage')
        wavelengthii = action.grab('wavelength')

        # reset second wavelengths
        spectrum = numpy.array([intensity for intensity, wave in zip(spectrum, wavelengthii) if wave in wavelength])
        wavelengthii = numpy.array([wave for wave in wavelengthii if wave in wavelength])

        # unpack pixel
        image, row = pixel
        circle = abs(round(latitude[pixel], 0))
        meridian = abs(round(longitude[pixel], 0))

        # for eadh depth
        for depth in depths:

            # for planar, scalar
            modes = ('Planar', 'Scalar')
            attenuations = (constant, constantii)
            #attenuations = (constant, constant)
            irradiances = (ultraviolet, ultravioletii)
            #irradiances = (ultraviolet, ultraviolet)
            for mode, attenuation, irradiance in zip(modes, attenuations, irradiances):

                # plot the planar ultraviolet at depth
                flux = (irradiance * numpy.exp(-attenuation * depth))[image, row]
                address = 'plots/spectrum/{}_Flux_{}m_{}.h5'.format(mode, self._pad(depth), tag)
                title = 'UV irradiance at {} m, {} S, {} W'.format(depth, circle, meridian)
                self.squid.ink('ultraviolet_flux', flux, 'wavelength', wavelength, address, title)

                # plot the planar dna damage spectrum at depth
                flux = (irradiance * numpy.exp(-attenuation * depth))[image, row]
                #damage = self._convolve(flux, spectrum, wavelength, wavelengthii)
                damage = spectrum * flux
                address = 'plots/spectrum/{}_DNA_Damage_{}m_{}.h5'.format(mode, self._pad(depth), tag)
                title = 'DNA Damage irradiance at {} m, {} S, {} W'.format(depth, circle, meridian)
                self.squid.ink('dna_damage', damage, 'wavelength', wavelength, address, title)

                # plot the normalized planar ultraviolet at depth
                flux = (irradiance * numpy.exp(-attenuation * depth))[image, row]
                normal = flux / float(flux[-1])
                address = 'plots/spectrum/{}_Flux_Norm_{}m_{}.h5'.format(mode, self._pad(depth), tag)
                formats = (mode, depth, image, row)
                title = '{} Ultraviolet Flux at {} m, Image {}, Row {}, Normalized to 399nm'.format(*formats)
                self.squid.ink('normalized_ultraviolet_flux', normal, 'wavelength', wavelength, address, title)

        # add spectra
        address = 'plots/spectrum/DNA_Damage_Spectrum.h5'
        title = 'DNA damage spectrum'
        self.squid.ink('dna_damage_spectrum', spectrum, 'wavelength', wavelength, address, title)

        return None

    def spline(self):
        """Compare log lineear chlorophyll interpolation with that of a spline.

        Arguments:
            None

        Returns:
            None
        """

        # grab the hydrolight tables
        hydra = Hydra('../studies/diatom/original/table')
        hydra.ingest('Ed_Eo')
        
        # get the flux data and chlorophyll, and depths
        flux = hydra.grab('downward_flux_planar')
        chlorophyll = hydra.grab('chlorophyll')
        depth = hydra.grab('depth')
        
        # take logarithm of chlorophyll
        chlorophyll = numpy.log10(chlorophyll)
        
        # calculate attenuation constants from table
        layers = flux[:, [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21], :, :, :]
        layersii = flux[:, [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22], :, :, :]
        coefficient = numpy.log(layers / layersii) / 0.01
        coefficient = coefficient.mean(axis=1)

        # get coefficients across chlorphyll at ozoner 125, zenith 0, wavelength 292
        coefficient = coefficient[0, 0, :, 0]

        # create sample chlorophylll points and take logartihm
        chlorophyllii = numpy.linspace(0.01, 10, 1000)
        chlorophyllii = numpy.log10(chlorophyllii)

        # create interpolation function, and interpolate to chlorphyll
        interpolator = scipy.interpolate.interp1d(chlorophyll, coefficient, kind='linear')
        coefficientii = interpolator(chlorophyllii)

        # create spline interpolator, and interoplate to chlorphyll
        spline = scipy.interpolate.CubicSpline(chlorophyll, coefficient)
        coefficientiii = spline(chlorophyllii)

        # create difference
        difference = coefficientii - coefficientiii

        # plot coefficients
        address = 'plots/spline/Linear_chlorophyll.h5'
        title = 'linear interpolation of Kd over log chlorophyll'
        self.squid.ink('linear', coefficientii, 'chlorophyll', 10.0 ** chlorophyllii, address, title)

        # plot coefficients
        address = 'plots/spline/Spline_chlorophyll.h5'
        title = 'Spline interpolation of Kd over log chlorophyll'
        self.squid.ink('spline', coefficientiii, 'chlorophyll', 10.0 ** chlorophyllii, address, title)

        # plot differece
        address = 'plots/spline/Difference_chlorophyll.h5'
        title = 'Absolute Difference of Kd over log chlorophyll'
        self.squid.ink('difference', difference, 'chlorophyll', 10.0 ** chlorophyllii, address, title)

        return None

    def stack(self, wavelength):
        """Make a stack of heatmaps for the interpolation table.

        Arguments:
            wavelength: float, the wavelength

        Returns:
            None
        """

        # get the table
        hydra = Hydra('../studies/diatom/table')
        hydra.ingest()

        # grab the data
        chlorophylls = hydra.grab('chlorophyll')
        ozones = hydra.grab('ozone')
        zeniths = hydra.grab('zenith')
        wavelengths = hydra.grab('wavelength')
        scale = hydra.grab('scaling_factor')

        # find the index of the wavelength
        wave = wavelengths.tolist().index(wavelength)

        # for each ozone angle
        for index, ozone in enumerate(ozones):

            # make an ozone, chlorphyll map
            array = scale[index, :, :, wave]
            ordinate = numpy.array([chlorophylls] * zeniths.shape[0])
            abscissa = numpy.array([zeniths] * chlorophylls.shape[0]).transpose(1, 0)
            bounds = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
            address = 'plots/table/Scaling_Factor_Ozone_{}_Wave_{}.h5'.format(index, wave)
            title = 'Interpolation Table, Scaling Factor ( Scalar / Planar ), '
            title += 'Ozone {} DU, Wavelength {} nm'.format(int(ozones[index]), wavelength)
            parameters = ('scaling_factor', array, 'chlorophyll', ordinate, 'zenith', abscissa)
            self.squid.shimmer(*parameters, address, bounds, title)

        # for all ozone brackets
        for index, ozone in enumerate(ozones):

            # and all chlorophyll brackets
            for indexii, chlorophyll in enumerate(chlorophylls):

                # plot along zenith
                array = scale[index, :, indexii, wave]
                address = 'plots/table/Scaline_Factor_Ozone_{}_Chlorophyll_{}_Wave_{}.h5'.format(index, indexii, wave)
                formats = (int(ozones[index]), round(chlorophylls[indexii], 2), wavelength)
                title = 'Interpolation Table, Scaling Factor ( Scalar / Planar ), '
                title += 'Ozone {} DU, Chlorophyll {} mg / L, Wavelength {} nm'.format(*formats)
                self.squid.ink('scaling_factor', array, 'solar_zenith_angle', zeniths, address, title)

        return None

    def state(self, resolution=50):
        """Examine differences due to static irradiance.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: str, orbit number

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/static'.format(self.sink))

        # create hydras
        hydra = Hydra('{}/ultraviolet'.format(self.sink))
        hydraii = Hydra('{}/ultraviolet'.format(self.sink))

        # ingest
        hydra.ingest(0)
        hydraii.ingest(1)

        # get orbit and date
        date = hydraii._stage(hydraii.current)['day']
        orbit = hydraii._stage(hydraii.current)['orbit']

        # set fill
        fill = -9999

        # get data
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        cloud = hydra.grab('cloud_tau')
        cloudii = hydraii.grab('cloud_tau')
        transmittance = hydra.grab('cloud_transmittance')[:, :, 0]
        transmittanceii = hydraii.grab('cloud_transmittance')[:, :, 0]
        surface = 10.0 ** hydra.grab('surface_irradiance_planar')[:, :, 0]
        surfaceii = 10.0 ** hydraii.grab('surface_irradiance_planar')[:, :, 0]
        sky = 10.0 ** hydra.grab('clear_sky')[:, :, 40]
        skyii = 10.0 ** hydraii.grab('clear_sky')[:, :, 40]
        residue = hydra.grab('residue_331')
        residueii = hydraii.grab('residue_331')

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-80, 80, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [residue, cloud, surface, sky, transmittance]
        panelsii = [residueii, cloudii, surfaceii, skyii, transmittanceii]
        stubs = ['Residue_331', 'Cloud_Tau', 'Surface_UV_290nm', 'Clear_Sky_330nm', 'Cloud_Transmittance_290m']

        # for each set
        for panel, panelii, stub in zip(panels, panelsii, stubs):

            # create difference
            difference = self._relate(panel, panelii)
            percent = self._relate(panel, panelii, percent=True)

            # create mask
            mask = (abs(panel) < 1e20) & (panel > 0) & (numpy.isfinite(panel))
            maskii = (abs(panelii) < 1e20) & (panelii > 0) & (numpy.isfinite(panelii))
            masque = numpy.logical_and(mask, maskii)

            # get main scales
            minimum = min([numpy.percentile(panel[masque], 5), numpy.percentile(panelii[masque], 5)])
            maximum = max([numpy.percentile(panel[masque], 95), numpy.percentile(panelii[masque], 95)])
            scale = (minimum, maximum)

            # get difference scale
            maximum = max([abs(numpy.percentile(difference[masque], 10)), abs(numpy.percentile(difference[masque], 90))])
            scaleii = (-maximum, maximum)

            # get percent scale
            # scaleiii = (percent[masque].min(), percent[masque].max())
            scaleiii = (-50, 50)

            # set epsilon
            epsilon = 1e-4

            # apply masques
            panel = numpy.where(masque, panel, fill)
            panelii = numpy.where(masque, panelii, fill)
            difference = numpy.where(masque, difference, fill)
            percent = numpy.where(masque, percent, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)
            panelii = numpy.where(panelii <= scale[0], scale[0] + epsilon, panelii)
            panelii = numpy.where(panelii >= scale[1], scale[1] - epsilon, panelii)
            difference = numpy.where(difference <= scaleii[0], scaleii[0] + epsilon, difference)
            difference = numpy.where(difference >= scaleii[1], scaleii[1] - epsilon, difference)
            percent = numpy.where(percent <= scaleiii[0], scaleiii[0] + epsilon, percent)
            percent = numpy.where(percent >= scaleiii[1], scaleiii[1] - epsilon, percent)

            # set formats
            formats = (stub, date, orbit)

            # plot OMUVB
            title = '{}, Dynamic, \n{}, o{}'.format(*formats)
            destination = '{}/plots/static/{}_Dynamic_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot OMOCNUV
            title = '{}, Static, \n{}, o{}'.format(*formats)
            destination = '{}/plots/static/{}_Static_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panelii, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot difference
            title = '{}, Static - Dynamic, \n{}, o{}'.format(*formats)
            destination = '{}/plots/static/{}_Difference_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[difference, latitude, longitude], destination, [title, unit]]
            parameters += [scaleii, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

            # plot percent
            title = '{}, % Static - Dynamic, \n{}, o{}'.format(*formats)
            destination = '{}/plots/static/{}_Percent_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[percent, latitude, longitude], destination, [title, unit]]
            parameters += [scaleiii, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def stir(self, latitudes=None, date=None, orbit=0):
        """Compare wavelength shift method with coefficient expansion.

        Arguments:
            latitudes: tuple of floats, latitudes to check
            date: str, date of file to check
            orbit: integer, orbit

        Returns:
            None
        """

        # set defaults
        latitudes = latitudes or (-60, -40, -20, 0, 20, 40, 60)
        date = date or '2006-12-23'

        # set bands
        products = {'OML1BRUG': ['BAND1', 'BAND2'], 'OML1BRVG': ['BAND3']}
        rows = {'BAND1': 30, 'BAND2': 60, 'BAND3': 60}
        pixels = {'BAND1': 159, 'BAND2': 557, 'BAND3': 751}

        # deconstruct date
        year, month, day = date.split('-')

        # for each product and bands
        for product, bands in products.items():

            # create hydra for the product and ingest first file
            hydra = Hydra('/tis/acps/OMI/10004/{}/{}/{}/{}'.format(product, year, month, day))
            hydra.ingest(orbit)

            # for each band
            for band in bands:

                # get the data
                wavelength = hydra.grab('{}/wavelength'.format(band)).squeeze()
                shift = hydra.grab('{}/wavelength_shift'.format(band)).squeeze()
                coefficient = hydra.grab('{}/wavelength_coefficient'.format(band)).squeeze()
                reference = hydra.grab('{}/wavelength_reference_column'.format(band))
                latitude = hydra.grab('{}/latitude'.format(band)).squeeze()

                # calculate wavelengths
                calculation = self._calculate(coefficient, reference, pixels[band])

                # add shifts
                wavelength = numpy.array([wavelength] * shift.shape[0])
                shift = numpy.array([shift] * wavelength.shape[2]).transpose(1, 2, 0)
                addition = wavelength + shift

                # calculate the difference
                difference = addition - calculation

                # for each latitude
                for degrees in latitudes:

                    # construct lattitue tag
                    tag = '{}N'.format(degrees)
                    if tag.startswith('-'):

                        # replace with S
                        tag = tag[1:].replace('N', 'S')

                    # get the image number for the latitude
                    image = hydra._pin(degrees, latitude[:, int(rows[band] / 2)])[0][0]

                    # construct rows
                    row = numpy.array([list(range(rows[band]))] * pixels[band]).transpose(1, 0)
                    pixel = numpy.array([list(range(pixels[band]))] * rows[band])

                    # make a heatmap of the differences
                    self._make('{}/plots/shift'.format(self.sink))
                    address = 'plots/shift/wavelength_shifts_{}_{}.h5'.format(band, tag)
                    title = 'Difference in wavelength assignment, wavelength shift - coefficient expansion'
                    title += ', {}, {}, {}'.format(band, tag, date)
                    bounds = [-0.01, -0.001, -0.0001, -0.00001, 0.00001, 0.0001, 0.001, 0.01]
                    parameters = ['wavelength_difference', difference[image], 'row', row, 'pixel', pixel]
                    parameters += [address, bounds, title, 'wavelength difference ( nm )']
                    self.squid.shimmer(*parameters)

        return None

    def synthesize(self, orbit='02573', partial=False, source=None):
        """Digest the output table and plot the data.

        Arguments:
            orbit: str, orbit number
            partial: boolean, accept parital records?

        Returns:
            None
        """

        # get table
        source = source or '../studies/diatom/out/E_K_eryth_{}.dat'.format(orbit)
        table = self._know(source)

        # parse into singlet quantities
        singlets = [float(entry) for row in table for entry in row.split()]

        # grab wavelengths
        wavelengths = singlets[:110]
        singlets = singlets[110:]

        # grab all sections beginning with year
        indices = [index for index, datum in enumerate(singlets) if datum == 2005]
        pairs = [(first, second) for first, second in zip(indices[:-1], indices[1:])]
        pairs += [(indices[-1], len(singlets))]
        segments = [singlets[first:second] for first, second in pairs]

        # match short form
        short = {'year': (0, 1), 'day': (1, 2), 'latitude': (2, 3), 'longitude': (3, 4), 'zenith': (4, 5)}
        short.update({'chlorophyll': (5, 6), 'ozone': (6, 7), 'gcf': (7, 8), 'ultraviolet': (8, 118)})
        short.update({'erythemal': (118, 119)})

        # match long form
        long = {'year': (0, 1), 'day': (1, 2), 'latitude': (2, 3), 'longitude': (3, 4), 'zenith': (4, 5)}
        long.update({'chlorophyll': (5, 6), 'ozone': (6, 7), 'gcf': (7, 8), 'ultraviolet': (8, 118)})
        long.update({'erythema': (118, 119), 'erythemaii': (119, 120), 'constant': (120, 230)})
        long.update({'constantii': (230, 340), 'dose': (340, 341), 'doseii': (341, 342)})

        # set reservoir
        reservoir = long
        if partial:

            # get short reservoir
            reservoir = short

        # create records
        data = {field: [] for field in reservoir.keys()}
        for segment in segments:

            # check for length
            if partial or len(segment) == 342:

                # for each field
                for field, bracket in long.items():

                    # append
                    data[field].append(segment[bracket[0]: bracket[1]])

        # make numpy arrays
        data = {field: numpy.array(array) for field, array in data.items()}

        # print contents
        for field, array in data.items():

            # print
            print(' ')
            print(field, array.shape, array.flatten().min(), array.flatten().max())

        # make heatmap
        features = []
        name = 'heatmap_erythemal_dose'
        array = numpy.vstack([data['erythema'].flatten(), data['latitude'].flatten(), data['longitude'].flatten()])
        bounds = [0, 100, 200, 300, 400, 500, 600]
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {}'.format(first, second) for first, second in brackets]
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # make heatmap
        name = 'heatmap_erythemal_dose_scalar'
        array = numpy.vstack([data['erythemaii'].flatten(), data['latitude'].flatten(), data['longitude'].flatten()])
        bounds = [0, 100, 200, 300, 400, 500, 600]
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {}'.format(first, second) for first, second in brackets]
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # stash
        destination = '{}/plots/erythema/Erythemal_Dose.h5'.format(self.sink)
        self.stash(features, destination)

        # beginn output object
        synthesis = {'wavelengths': wavelengths, 'segments': segments, 'singlets': singlets, 'data': data}

        return synthesis

    def tabulate(self):
        """Read in lookup table.

        Arguments:
            None

        Returns:
            None
        """

        # stamp
        self._stamp('parsing flux table...', initial=True)

        # read in file
        sink = '/'.join(self.sink.split('/')[:-1])
        name = '{}/xerox/Ed_Eu_Eod_Eou_oz.dat'.format(sink)
        lines = self._know(name)

        # split into floats
        numbers = [float(number) for line in lines for number in line.split()]

        # grab first five numbers for parameters
        first = 5
        sizes = numbers[:first]
        second = first + int(sum(sizes))
        parameters = numbers[first:second]
        data = numbers[second:]

        # assemble parameters into table
        table = {}
        def sizing(index): return int(sum(sizes[:index]))
        table['wavelengths'] = numpy.array(parameters[sizing(0):sizing(1)])
        table['chlorophylls'] = numpy.array(parameters[sizing(1):sizing(2)])
        table['zeniths'] = numpy.array(parameters[sizing(2):sizing(3)])
        table['depths'] = numpy.array(parameters[sizing(3):sizing(4)])
        table['ozones'] = numpy.array(parameters[sizing(4):sizing(5)])

        # create table shape
        shape = [int(size) for size in sizes]
        shape[-2] += 1
        shape = tuple(shape)
        block = int(numpy.prod(shape))

        # read in data
        table['Ed'] = numpy.array(data[block * 0: block * 1]).reshape(shape)
        table['Eu'] = numpy.array(data[block * 1: block * 2]).reshape(shape)
        table['Eod'] = numpy.array(data[block * 2: block * 3]).reshape(shape)
        table['Eou'] = numpy.array(data[block * 3: block * 4]).reshape(shape)

        # complete
        self._stamp('tabulated.')

        return table

    def tout(self, year=2005, month=3, day=21, orbit=3630, margin=5, resolution=50, wave=305, scales=None):
        """Examine tau and correction.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: int, orbit number
            margin: percentile margin for plots
            resolution: int, resolution
            wave: float, wavelength of interest
            scales: list of tuples of ints, the scales to use

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/tau'.format(self.sink))

        # create hydras
        formats = (str(year), self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/70004/OMAERUV/{}/{}/{}'.format(*formats))

        # ingest
        orbit = str(orbit)
        hydra.ingest(str(orbit))

        # get date
        date = hydra._stage(hydra.current)['day']

        # set fill
        fill = -9999

        # get data
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        # aerosol = hydra.grab('AerosolOpticalDepthOverCloud')[:, :, 1]
        # aerosol = hydra.grab('AerosolAbsOpticalDepthVsHeight')[:, :, -1, 1]
        aerosol = hydra.grab('FinalAerosolAbsOpticalDepth')[:, :, :2]
        zenith = hydra.grab('SolarZenithAngle')
        wavelength = hydra.grab('Wavelengths')

        # construct correction factor
        aerosol = numpy.where(aerosol < 0, 0, aerosol)
        power = (numpy.log(wave) - numpy.log(wavelength[0])) / (numpy.log(wavelength[1]) - numpy.log(wavelength[0]))
        tau = aerosol[:, :, 0] * (aerosol[:, :, 1] / aerosol[:, :, 0]) ** power
        tau = numpy.where(numpy.isfinite(tau), tau, 0)
        constants = [1, -1.4, 1.09, -0.44]
        factor = (1.23 + numpy.sin(zenith * math.pi / 180)) * tau
        correction = sum([constant * factor ** exponent for exponent, constant in enumerate(constants)])
        correction = numpy.where(correction > 0, correction, 0)

        print(correction.min(), correction.max())
        print(factor.min(), factor.max())
        print(tau.min(), tau.max())

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # set figure size and limits
        limits = (-80, 80, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [tau, correction]
        stubs = ['AerosolAbsOpticalDepth_{}nm'.format(wave), 'Correction_{}nm'.format(wave)]

        # if no given scale
        if not scales:

            # set scales to None
            scales = [None] * len(panels)

        # for each set
        for panel, stub, scale in zip(panels, stubs, scales):

            # create mask
            mask = (abs(panel) < 1e20) & (panel > -100) & (numpy.isfinite(panel))
            masque = mask

            # construct percentile bounds
            lower = margin
            upper = 100 - margin

            # get main scales
            minimum = numpy.percentile(panel[masque], lower)
            maximum = numpy.percentile(panel[masque], upper)
            scale = scale or (minimum, maximum)

            # apply masques
            panel = numpy.where(masque, panel, fill)

            # saturate bounds
            epsilon=1e-4
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)

            # set formats
            formats = (stub, date, orbit)

            # plot OMUVB
            title = '{}, \n{}, o{}'.format(*formats)
            destination = '{}/plots/tau/{}_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution, size]
            parameters += [logarithm, bar, 'black']
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters)

        return None

    def trace(self, tag='', rows=(3, 4, 5), image=700):
        """Trace raw n values compared to outputs.

        Arguments:
            tag: tag for output file
            rows: tuple of ints, row indices
            image: int, image number

        Returns:
            None
        """

        # load in the hdf5 data
        hydra = Hydra('../studies/diatom/erythema')
        path = '{}/OMOCNUV_Erythema_02573_{}.h5'.format(hydra.source, tag).replace('_.h5', '.h5')
        hydra.ingest(path)

        # locate
        hydraii = Hydra('../studies/diatom/frames')

        # set bands
        bands = {'OML1BRUG': 'BAND2', 'OML1BRVG': 'BAND3'}

        # go through each band
        radiances = []
        wavelengths = []
        for product, band in bands.items():

            # ingest radiance file
            entry = [path for path in hydraii.paths if product in path][0]
            hydraii.ingest(entry)

            # get radiance data and wavelengths
            wavelength = hydraii.grab('{}/wavelength'.format(band))[0]
            wavelengths.append(wavelength)
            radiance = hydraii.grab('{}/radiance'.format(band))[0]
            radiances.append(radiance)

        # get irradiances
        irradiances = []
        entry = [path for path in hydraii.paths if 'OML1BIRR' in path][0]
        hydraii.ingest(entry)

        # get band2 irradiances and fill out for full track
        irradiance = hydraii.grab('BAND2/irradiance')[0]
        irradiance = numpy.array([irradiance[0]] * radiances[0].shape[0])
        irradiances.append(irradiance)

        # get band2 irradiances
        irradiance = hydraii.grab('BAND3/irradiance')[0]
        irradiance = numpy.array([irradiance[0]] * radiances[1].shape[0])
        irradiances.append(irradiance)

        # compute raw envalues
        raw = -100 * numpy.log10(radiances[0] / irradiances[0])
        rawii = -100 * numpy.log10(radiances[1] / irradiances[1])

        # set wavelengths
        waves = [331.06, 339.66, 359.88, 379.95]

        # get nvalues
        values = hydra.grab('n_values')

        # for each row
        for row in rows:

            # create nvalue plot
            title = 'OMOCNUV N values, row {}, image: {}'.format(row, image)
            address = 'plots/raw/OMOCNUV_nvalues_row{}_image{}.h5'.format(self._pad(row), self._pad(image, 4))
            self.squid.ink('nvalue', values[image, row], 'wavelength', waves, address, title)

            # create raw plot for band 2
            title = 'Raw Band2 N values, row {}, image: {}'.format(row, image)
            address = 'plots/raw/Raw_Band2_nvalues_row{}_image{}.h5'.format(self._pad(row), self._pad(image, 4))
            abscissa = wavelengths[0][row]
            ordinate = raw[image][row]
            self.squid.ink('nvalue', ordinate, 'wavelength', abscissa, address, title)

            # create raw plot for band 3
            title = 'Raw Band3 N values, row {}, image: {}'.format(row, image)
            address = 'plots/raw/Raw_Band3_nvalues_row{}_image{}.h5'.format(self._pad(row), self._pad(image, 4))
            abscissa = wavelengths[1][row]
            ordinate = rawii[image][row]
            self.squid.ink('nvalue', ordinate, 'wavelength', abscissa, address, title)

        return None

    def transmit(self, tag='o012985', wave=390):
        """Compare surface to underwater ultraviolet scalar flux.

        Arguments:
            tag: str, tag of ocean product file

        Returns:
            None
        """

        # grab the ocean product
        ocean = self._pull(tag)

        # get the chlorophyll, surface planar and scalar, and underwater fluxes
        chlorophyll = ocean.grab('chlorophyll')
        latitude = ocean.grab('latitude')
        longitude = ocean.grab('longitude')
        planar = ocean.grab('ultraviolet_surface_flux_planar')
        scalar = ocean.grab('ultraviolet_surface_flux_scalar')
        water = ocean.grab('ultraviolet_underwater_flux_scalar')
        wavelength = ocean.grab('wavelength')

        # get the wavelength index
        index = self._pin(wave, wavelength)[0][0]
        planar = planar[:, :, index]
        scalar = scalar[:, :, index]
        water = water[:, :, index]

        # make mask
        mask = numpy.isfinite(planar) & (chlorophyll > 0) & (planar >= 0) & (scalar >= 0) & (water >= 0)

        # make heatmaps
        names = ['chlorophyll'] + [name.format(wave) for name in ('planar_{}', 'surface_{}', 'underwater_{}')]
        arrays = [chlorophyll, planar, scalar, water]
        brackets = {'chlorophyll': [index * 0.2 for index in range(9)]}
        brackets.update({'ratio': [1.0 + index * 0.2 for index in range(11)]})
        for name, array in zip(names, arrays):

            # create bounds
            # maximum = max([array.max() for array in arrays[1:]])
            # chunks = math.floor(maximum / 100) + 1
            bounds = [200 * index for index in range(9)]
            bounds = brackets.get(name, bounds)

            # make heatmap
            title = '{}, {}'.format(name, tag)
            address = 'plots/transmittance/{}.h5'.format(name)
            units = 'mW/m2nm'
            parameters = [name, array[mask], 'latitude', latitude[mask], 'longitude', longitude[mask]]
            self.squid.shimmer(*parameters, address, bounds, title, units)

        # plot ratio
        ratio = scalar / water
        maskii = numpy.logical_and(mask, numpy.isfinite(ratio))

        # make heatmap
        name = 'ratio'
        array = ratio
        bounds = brackets.get(name, bounds)
        title = '{}, {}'.format(name, tag)
        address = 'plots/transmittance/{}.h5'.format(name)
        units = ' '
        parameters = [name, array[maskii], 'latitude', latitude[maskii], 'longitude', longitude[maskii]]
        self.squid.shimmer(*parameters, address, bounds, title, units)

        return None

    def trend(self, year=2005, months=(1, 3), tag='trends', collect=False, days=3, source=None, reference=2005):
        """Create trending files and plots across omocnuv records.

        Arguments:
            year: int, the year
            months: tuple of ints, the months
            tag: str, name of folder for output
            collect: boolean, perform collection?
            days: int, number of days per month
            source: path to data folder if not /tis
            reference: reference year for time

        Returns:
            None
        """

        # set wavelengths
        waves = (305, 320, 350, 380)

        # set default coordinates
        coordinates = [(-20, -120), (20, -160), (-20, 150), (30, -70)]
        coordinates += [(0, 0), (0, 80), (30, 150), (-50, 20)]
        coordinates += [(30, -120), (50, -170), (-62, -59)]

        # set associated locations
        locations = ['Southern Gyre', 'Hawaii', 'Great Barrier Reef', 'Sargasso Sea']
        locations += ['North Africa', 'Indian Ocean', 'Japan', 'Southern Ocean']
        locations += ['Southern California', 'Alaska', 'King George Island']
        locations = {coordinate: location for coordinate, location in zip(coordinates, locations)}

        # set stubs
        stubs = ['Gyre', 'Hawaii', 'GBReef', 'Sargasso', 'NAfrica', 'IOcean', 'Japan', 'SAfrica']
        stubs += ['SCalifornia', 'Alaska', 'KGIsland']
        stubs = {coordinate: stub for coordinate, stub in zip(coordinates, stubs)}

        # create folders
        self._make('{}/{}'.format(self.sink, tag))
        self._make('{}/plots/{}'.format(self.sink, tag))

        # get the dna spectrum from action file
        action = Hydra('{}/spectra'.format(self.sink))
        action.ingest('Action')
        spectrum = action.grab('dna_damage')
        wavelength = action.grab('wavelength')

        # create mask for 290 - 399nm
        mask = [index for index, wave in enumerate(wavelength) if wave in range(290, 400)]
        spectrum = spectrum[mask]

        # set fills
        fill = -999
        fillii = 1e20

        # set fields
        fields = {'chlorophyll': 'chlorophyll', 'ozone': 'column_amount_ozone', 'quality': 'quality_flag_underwater'}
        fields.update({'bits': 'bit_flag', 'sky': 'clear_sky'})
        fields.update({'surface': 'surface_irradiance_planar', 'underwater': 'underwater_irradiance_planar'})
        fields.update({'coefficient': 'diffuse_attenuation_coefficient_planar'})
        fields.update({'cloud': 'cloud_transmittance_factor', 'aerosol': 'aerosol_absorption_factor'})
        fields.update({'reflectivity_360': 'reflectivity_360', 'ultraviolet': 'ultraviolet_index'})
        fields.update({'zenith': 'solar_zenith_angle', 'noon': 'noon_zenith_angle'})
        fields.update({'tau_354': 'aerosol_optical_depth_354', 'tau_388': 'aerosol_optical_depth_388'})

        # set three dimensional fields
        threes = ['sky', 'surface', 'underwater', 'coefficient', 'cloud', 'aerosol', 'depth']

        # set units
        units = {'chlorophyll': 'mg / m^3', 'ozone': 'DU', 'quality': '-', 'bits': '-'}
        units.update({'sky': 'mW/m^2', 'surface': 'mW/m^2', 'underwater': 'mW/m^2', 'damage': 'mW/m^2'})
        units.update({'coefficient': '1/m', 'depth': 'm', 'cloud': '-', 'aerosol': '-', 'reflectivity': '-'})
        units.update({'zenith': 'deg', 'noon': 'deg', 'tau_354': '-', 'tau_388': '-'})
        units.update({'tau_305': '-', 'exponent': '-'})

        # update units with three dimensions
        units.update({'{}_{}'.format(field, wave): units[field] for field in threes for wave in waves})

        # begin destinations
        destinations = {}
        destinationsii = {}

        # for each coordinate
        for latitude, longitude in coordinates:

            # create destination
            formats = (self.sink, tag, year, self._orient(latitude), self._orient(longitude, east=True))
            destination = '{}/{}/OMOCNUV_Trends_{}_{}_{}_single.h5'.format(*formats)
            destinationii = '{}/{}/OMOCNUV_Trends_{}_{}_{}_average.h5'.format(*formats)
            destinations[(latitude, longitude)] = destination
            destinationsii[(latitude, longitude)] = destinationii

        # if in collection mode
        if collect:

            # for each month
            for month in range(*months):

                # begin data
                data = {(latitude, longitude): {} for latitude, longitude in coordinates}
                dataii = {(latitude, longitude): {} for latitude, longitude in coordinates}

                # if a source is given
                if source:

                    # create hydra
                    hydra = Hydra(source)
                    paths = hydra.paths
                    paths.sort()

                    # get subset according to month
                    paths = [path for path in paths if '{}m{}'.format(year, self._pad(month)) in path]

                # otherwise
                else:

                    # create hydra
                    hydra = Hydra('/tis/acps/OMI/70004/OMOCNUV/{}/{}'.format(year, self._pad(month)), 1, days)
                    paths = hydra.paths
                    paths.sort()

                # for each path
                for path in paths:

                    # ingest path
                    self._stamp('ingesting {}...'.format(path), initial=True)
                    hydra.ingest(path)

                    # get orbit and days
                    orbit = int(hydra._stage(path)['orbit'])
                    day = int(hydra._stage(path)['day'][-2:])

                    # get latitude, longitude, and wavelength
                    latitudes = hydra.grab('latitude')
                    longitudes = hydra.grab('longitude')
                    wavelengths = hydra.grab('spectral_wavelength')
                    time = hydra.grab('time')

                    # get positions
                    positions = [hydra._pin(wave, wavelengths, 1)[0][0] for wave in waves]

                    # for each coordinate
                    for latitude, longitude in coordinates:

                        # find the closet pixel
                        pixels = hydra._pin([latitude, longitude], [latitudes, longitudes], 50)
                        closest = pixels[0]

                        # check the distance for less tha 1 degree
                        distance = (latitudes[closest] - latitude) ** 2 + (longitudes[closest] - longitude) ** 2
                        if distance < 1:

                            # compute squared distances for all pixels
                            distances = []
                            for pixel in pixels:

                                # append squared distance
                                distance = (latitudes[pixel] - latitude) ** 2
                                distance += (longitudes[pixel] - longitude) ** 2
                                distances.append(distance)

                            # get set of close pixels, within 0.5 degree ( 0.25 squared )
                            close = [pixel for pixel, distance in zip(pixels, distances) if distance < 0.25]

                            # collect all fields
                            collection = {field: hydra.grab(search) for field, search in fields.items()}

                            # calculate aee
                            ratio = numpy.log(collection['tau_354'] / collection['tau_388'])
                            collection['exponent'] = ratio / numpy.log(388 / 354)

                            # calculate tau at 305nm
                            collection['tau_305'] = collection['tau_388'] * (305 / 388) ** -collection['exponent']

                            # calculate dna damage at 5m
                            underwater = collection['underwater']
                            coefficient = collection['coefficient']
                            irradiance = underwater * numpy.exp(-coefficient * 5)
                            damage = (irradiance * spectrum).sum(axis=2)
                            collection['underwater'] = irradiance
                            collection['damage'] = damage

                            # calculate penetration depths
                            depth = -1 * numpy.log(0.1) / collection['coefficient']
                            collection['depth'] = depth

                            # convert bits to bit using log2
                            collection['bits'] = numpy.log2(collection['bits'])

                            # add orbit number, month, year, and day, expanding across array
                            collection['year'] = year * numpy.ones(damage.shape)
                            collection['month'] = month * numpy.ones(damage.shape)
                            collection['day'] = day * numpy.ones(damage.shape)
                            collection['orbit'] = orbit * numpy.ones(damage.shape)
                            collection['time'] = time[0] * numpy.ones(damage.shape)

                            # for each three dimensional field:
                            for field in threes:

                                # for each wavelength
                                for wave, position in zip(waves, positions):

                                    # subset
                                    name = '{}_{}'.format(field, wave)
                                    collection[name] = collection[field][:, :, position]

                                # delete main field
                                del collection[field]

                            # transfer to data
                            for field, array in collection.items():

                                # get reservoir for nearest pixel
                                reservoir = data[(latitude, longitude)].setdefault(field, [])
                                reservoir.append(array[closest])

                                # get all values at close pixels, masking out fills
                                quantities = numpy.array([array[pixel] for pixel in close])

                                # if field is not time
                                if field != 'time':

                                    # mask out fills
                                    mask = (quantities > fill) & (abs(quantities) < fillii)
                                    quantities = quantities[mask]

                                # compute average, if not finite
                                average = quantities.mean()
                                if not numpy.isfinite(average):

                                    # set to fill
                                    average = fill

                                # get reservoir for average of pixels
                                reservoirii = dataii[(latitude, longitude)].setdefault(field, [])
                                reservoirii.append(average)

                    # timestamp
                    self._stamp('done.')

                # for each coordinate
                for coordinate in coordinates:

                    # convert data to arrays
                    data[coordinate] = {field: numpy.array(array) for field, array in data[coordinate].items()}
                    dataii[coordinate] = {field: numpy.array(array) for field, array in dataii[coordinate].items()}

                    # stash data
                    destinationii = destinations[coordinate].replace('.h5', '_{}.h5'.format(self._pad(month)))
                    self.spawn(destinationii, data[coordinate])

                    # stash averaged data
                    destinationii = destinationsii[coordinate].replace('.h5', '_{}.h5'.format(self._pad(month)))
                    self.spawn(destinationii, dataii[coordinate])

        # if not collect
        if not collect:

            # create hydra
            hydra = Hydra('{}/{}'.format(self.sink, tag))

            # for each coordinate
            for coordinate in coordinates:

                # get destination for singlet
                destination = destinations[coordinate]

                # get hydra paths for coordinate
                paths = [path for path in hydra.paths if destination.split('.')[-2] in path]
                paths = [path for path in paths if path != destination]
                paths.sort()
                self.merge(paths, destination, squeeze=True)

                # get destination
                destinationii = destinationsii[coordinate]

                # get hydra paths for coordinate
                paths = [path for path in hydra.paths if destinationii.split('.')[-2] in path]
                paths = [path for path in paths if path != destinationii]
                paths.sort()
                self.merge(paths, destinationii, squeeze=True)

            # create hydra
            hydra = Hydra('{}/{}'.format(self.sink, tag))

            # for each coordinate
            for coordinate in coordinates:

                # for each reservoir
                for reservoir in (destinations, destinationsii):

                    # grab file and extract data
                    destination = reservoir[coordinate]
                    hydra.ingest(destination)
                    data = hydra.extract()

                    # grab time field
                    time = data['time']

                    # create datetime objects form timestamp
                    dates = [datetime.datetime.fromtimestamp(int(stamp)) for stamp in time]

                    # reference all dates to reference year and convert to timestamp
                    datesii = [datetime.datetime(reference, date.month, date.day, date.hour) for date in dates]
                    time = numpy.array([date.timestamp() for date in datesii])

                    # for each field
                    for field, array in data.items():

                        # squeeze array
                        array = array.squeeze()

                        # create fill masque
                        mask = (array > fill) & (abs(array) < fillii) & (numpy.isfinite(array))
                        masque = mask

                        # create title
                        formats = [field, units.get(field, '-'), year]
                        formats += [self._orient(coordinate[0]), self._orient(coordinate[1], east=True)]
                        formats += [locations[coordinate]]
                        title = 'OMOCNUV, {} ( {} ), {}, {}, {} ( {} )'.format(*formats)

                        # create new destination and plot
                        address = destination.replace('.h5', '_{}_{}.h5'.format(field, stubs[coordinate]))
                        address = 'plots/{}/{}'.format(tag, self._file(address))
                        # self.squid.ink(field, array[masque], 'year_fraction', fraction[masque], address, title)
                        self.squid.ink(field, array[masque], 'month', time[masque] * 1000, address, title)

            # plot coordinates
            latitudes = numpy.array([coordinate[0] for coordinate in coordinates])
            longitudes = numpy.array([coordinate[1] for coordinate in coordinates])
            title = 'OMOCNUV, 2005 sampling locations'
            address = 'plots/{}/locations.h5'.format(tag)
            self.squid.ink('latitude', latitudes, 'longitude', longitudes, address, title)

        return None

    def transplant(self, conversions=None, frames=None, coefficients=False, margin=400):
        """Transplant collection3 data into collection 4 shells.

        Arguments:
            conversions: str, conversions folder
            frames: str, frames folder
            coefficients: boolean, also transfer coefficients?
            margin: int, margin for wavelength averaging

        Returns:
            None
        """

        # set default folders
        conversions = conversions or '../OMOCNUV/conversions'
        frames = frames or '../OMOCNUV/frames'

        # define products, bands, and fields
        products = {'OML1BIRR': ('UV-2', 'VIS'), 'OML1BRUG': ('UV-2',), 'OML1BRVG': ('VIS',)}
        fields = {'OML1BIRR': 'irradiance', 'OML1BRUG': 'radiance', 'OML1BRVG': 'radiance'}
        numbers = {'UV-2': '2', 'VIS': '3'}
        pixels = {'UV-2': 557, 'VIS': 751}

        # # define wavelength field names
        # waves = {'WavelengthCoefficient': 'wavelength_coefficient'}
        # waves.update({'WavelengthReferenceColumn': 'wavelength_reference_column'})

        # create hydras
        hydra = Hydra(conversions)
        hydraii = Hydra(frames)

        # for each product
        for product, bands in products.items():

            # get conversion path
            conversion = [path for path in hydra.paths if product in path][0]
            hydra.ingest(conversion)

            # for each band
            for band in bands:

                # grab irradiance data
                exponent = hydra.grab('{}/{}Exponent'.format(band, fields[product].capitalize()))
                mantissa = hydra.grab('{}/{}Mantissa'.format(band, fields[product].capitalize()))
                distance = hydra.grab('EarthSunDistance')

                # define average earth sun distance in meters, and normalize the distance
                sun = 149597870691
                ratio = distance[0][0] / sun

                # define conversion factor from collection 3 to collection 4 units
                factor = 10000 / 6.02214076e23

                # calculate radiance/irradiance, adjusting for conversion factor and sun earth distance
                radiance = (10.0 ** exponent) * mantissa * factor * (ratio ** 2)

                # find path
                frame = [path for path in hydraii.paths if product in path][0]

                # set addresses
                formats = (numbers[band], fields[product].upper(), fields[product])
                address = 'BAND{}_{}/STANDARD_MODE/OBSERVATIONS/{}'.format(*formats)

                # open netcdf
                net = netCDF4.Dataset(frame, 'r+')

                # insert data
                self._print('inserting {}...'.format(address, radiance.shape))
                net[address][:] = radiance

                # get wavelength coefficients and reference
                coefficient = hydra.grab('{}/WavelengthCoefficient'.format(band))[0]
                reference = numpy.array([hydra.grab('{}/WavelengthReferenceColumn'.format(band))[0][0]])

                # if inserting coefficiets
                if coefficients:

                    # insert coefficients
                    formats = (numbers[band], fields[product].upper())
                    address = 'BAND{}_{}/STANDARD_MODE/INSTRUMENT/wavelength_coefficient'.format(*formats)
                    self._print('inserting wavelength coefficient...')
                    net[address][:] = coefficient

                    # insert coefficients
                    formats = (numbers[band], fields[product].upper())
                    address = 'BAND{}_{}/STANDARD_MODE/INSTRUMENT/wavelength_reference_column'.format(*formats)
                    self._print('inserting wavelength reference column...')
                    net[address][:] = reference[0]

                # calcualte wavelengths
                wavelength = self._calculate(coefficient, reference, pixels[band])
                shape = wavelength.shape
                if shape[0] > 1:

                    # calculate wavelength with margins
                    wavelength = numpy.array([wavelength[margin: shape[0] - margin].mean(axis=0)])

                # otherwise
                else:

                    # take mean of entirety
                    wavelength = numpy.array([wavelength.mean(axis=0)])

                # define address
                formats = (numbers[band], fields[product].upper())
                address = 'BAND{}_{}/STANDARD_MODE/INSTRUMENT/wavelength'.format(*formats)

                # insert data
                self._print('inserting {}...'.format(address, wavelength.shape))
                net[address][:] = wavelength

                # get spectral quality data
                quality = hydra.grab('{}/PixelQualityFlags'.format(band))

                # define address
                formats = (numbers[band], fields[product].upper())
                address = 'BAND{}_{}/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality'.format(*formats)

                # insert data
                self._print('inserting {}...'.format(address, quality.shape))
                net[address][:] = quality

                # close file
                net.close()

        return None

    def trouble(self, tag='trouble', orbit=105002, resolution=1, wave=305, sigmas=3):
        """Troubleshoot missing aerosol data in figure.

        Argumnents:
            tag: str, keyword for plots folder
            resolution: int, number of vertical polygons to meld
            wave: int, wavelength
            sigmas: number of stdevs for plotting

        Returns:
            None
        """

        # set defaults
        bar = True
        zoom = None

        # set fill, epsilon
        fill = -999
        epsilon = 1e-5

        # make folder
        folder = 'plots/{}'.format(tag)
        self._make('{}/plots'.format(self.sink))
        self._make('{}/{}'.format(self.sink, folder))

        # make hydra
        hydra = Hydra('{}/april'.format(self.sink))
        hydra.ingest(str(orbit))

        # get aerosol data
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        aerosol = hydra.grab('aerosol_optical_depth_354')

        # set scale
        scale = (0, 0.1)

        # set up mask
        mask = (abs(aerosol) < 1e20) & numpy.isfinite(aerosol) & (aerosol > 0)
        aerosol = numpy.where(mask, aerosol, 0)

        # clip at upper and lower bounds
        aerosol = numpy.where((aerosol < scale[0] + epsilon) & (aerosol > fill), scale[0] + epsilon, aerosol)
        aerosol = numpy.where(aerosol > scale[1] - epsilon, scale[1] - epsilon, aerosol)

        # set bounds
        margin = 0
        marginii = 0
        bounds = (25, 55)
        boundsii = (-40, 10)

        # set up omi plot, zoom
        field = 'aerosol'
        title = 'OMI aaod 354nm, 2024-04-11'
        destination = '{}/{}/OMI_{}_{}_{}.png'.format(self.sink, folder, tag, orbit, field)
        limits = (bounds[0] - margin, bounds[1] + margin, boundsii[0] - marginii, boundsii[1] + marginii)
        size = (5, 7)
        logarithm = False
        back = 'white'
        unit = '-'
        limits = zoom or limits
        lines = []
        gradient = 'plasma'
        selection = [index for index in range(256)]

        # plot
        self._stamp('plotting {}...'.format(destination))
        parameters = [[aerosol, latitude, longitude], destination, [title, unit]]
        parameters += [scale, [gradient, selection], limits, resolution, lines]
        options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
        self._paint(*parameters, **options)

        return None

    def ululate(self, orbit=6312, tags=('free', 'gapless'), zoom=(-10, 10, -90, -30)):
        """Check for uv indxx and aerosol different discrepancy.

        Arguments:
            orbit: int, the orbit number
            tags: tuple of str, the folders for omocnuv data
            zoom: the latitude and longitude limits

        Returns:
            None
        """

        # create plots folder
        folder = 'plots/ululation'
        self._make('{}/{}'.format(self.sink, folder))

        # create second plots folder
        folderii = '{}/plots/ululate'.format(self.sink)
        self._make(folderii)

        # get first data
        hydra = Hydra('{}/{}'.format(self.sink, tags[0]))
        hydra.ingest(str(orbit))
        ultraviolet = hydra.grab('ultraviolet_index')
        quality = hydra.grab('quality_flag_surface')
        aerosol = hydra.grab('aerosol_optical_depth_354')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        bits = hydra.grab('bit_flag')

        # get second data
        hydraii = Hydra('{}/{}'.format(self.sink, tags[1]))
        hydraii.ingest(str(orbit))
        ultravioletii = hydraii.grab('ultraviolet_index')
        qualityii = hydraii.grab('quality_flag_surface')
        aerosolii = hydraii.grab('aerosol_optical_depth_354')
        latitudeii = hydraii.grab('latitude')
        longitudeii = hydraii.grab('longitude')
        bitsii = hydraii.grab('bit_flag')

        # create percent difference uv index
        percent = self._relate(ultraviolet, ultravioletii, percent=True)

        # make mask
        mask = (numpy.isfinite(percent)) & (numpy.isfinite(aerosolii))
        mask = mask & (abs(percent) < 1e20) & (abs(aerosolii) < 1e20) & (aerosolii > 0)
        mask = mask & (latitude >= zoom[0]) & (latitude <= zoom[1])
        mask = mask & (longitude >= zoom[2]) & (longitude <= zoom[3])
        mask = mask & (quality < 2) & (qualityii < 2)
        mask = mask & ((bits & 256) == 0) & ((bitsii & 256) == 0)

        # make scatter plot
        title = 'Aerosol vs UV index diff'
        address = '{}/aerosol_uv_index.h5'.format(folder)
        self.squid.ink('percent', percent[mask], 'aerosol', aerosolii[mask], address, title)

        # get date
        date = hydra._stage(hydra.current)['day']

        # prepare percentage map
        epsilon = 1e-8
        scale = (-50, 0)
        tracer = percent
        tracer = numpy.where(tracer < scale[0] + epsilon, scale[0] + epsilon, tracer)
        tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)
        tracer = numpy.where(mask, tracer, -9999)
        title = 'OMOCNUV, {}, {} % diff UV index'.format(orbit, date)
        destination = '{}/OMI_OMOCNUV_UVindex_{}_{}.png'.format(folderii, orbit, date)
        limits = zoom
        size = (14, 5)
        logarithm = False
        back = 'white'
        bar = True
        extension = 'both'
        gradient = 'BuPu_r'
        selection = list(range(256))
        unit = '%'
        resolution = 1

        # plot
        self._stamp('plotting {}...'.format(destination), initial=True)
        parameters = [[tracer, latitude, longitude], destination, [title, unit]]
        parameters += [scale, [gradient, selection], limits, resolution]
        options = {'size': size, 'log': logarithm, 'back': back, 'north': False}
        options.update({'bar': bar, 'extension': extension})
        self._paint(*parameters, **options)

        # prepare aerosol map
        epsilon = 1e-8
        scale = (0, 0.1)
        tracer = aerosolii
        tracer = numpy.where(tracer < scale[0] + epsilon, scale[0] + epsilon, tracer)
        tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)
        tracer = numpy.where(mask, tracer, -9999)
        title = 'OMOCNUV, {}, {} AAOD 354nm'.format(orbit, date)
        destination = '{}/OMI_OMOCNUV_aerosol_{}_{}.png'.format(folderii, orbit, date)
        limits = zoom
        size = (14, 5)
        logarithm = False
        back = 'white'
        bar = True
        extension = 'both'
        gradient = 'plasma'
        selection = list(range(256))
        unit = '-'
        resolution = 1

        # plot
        self._stamp('plotting {}...'.format(destination), initial=True)
        parameters = [[tracer, latitude, longitude], destination, [title, unit]]
        parameters += [scale, [gradient, selection], limits, resolution]
        options = {'size': size, 'log': logarithm, 'back': back, 'north': False}
        options.update({'bar': bar, 'extension': extension})
        self._paint(*parameters, **options)

        return None

    def validate(self, tag='coefficient', scan=994, rows=(2, 9), pixels=(49, 65), subset=(1, 2)):
        """Compare nvalue differences to wavelengths

        Arguments:
            tag: str, tag for data
            scan: int, track number
            rows: tuple of ints, rows to check
            pixels: tuple of pixels

        Returns:
            None
        """

        # get the duel swath file
        duel = self._pull(tag, duel=True)

        # get n360 contents
        sixty = duel.grab('ascii_n360')
        sixtyii = duel.grab('omocned_n360')
        difference = sixtyii - sixty

        # make a hydra for replication file
        hydra = self._pull(tag, replicant=True)

        # get radiance for band 3 and wavelength registration
        radiance = hydra.grab('band3/radiance')
        wavelength = hydra.grab('band3/radiance_wavelength')
        weight = hydra.grab('band3/radiance_weights_360')
        flag = hydra.grab('band3/radiance_quality_flag')

        # get corresponding irradiance parameters
        irradiance = hydra.grab('band3/irradiance')
        wavelengthii = hydra.grab('band3/irradiance_wavelength')
        weightii = hydra.grab('band3/irradiance_weights_360')
        flagii = hydra.grab('band3/irradiance_quality_flag')

        # get numerator / denominator, n value
        numerator = hydra.grab('band3/numerator_360')
        denominator = hydra.grab('band3/denominator_360')
        ratio = hydra.grab('band3/ratio_360')
        value = hydra.grab('band3/n_value_360')

        # grab the text printout
        text = self._know('../studies/diatom/ens/ens_{}.txt'.format(tag))

        # load in from text
        data = []
        chunk = 26
        for index, row in enumerate(range(*rows)):

            # begin row reservoir
            reservoir = {'row': row}

            # get position of first line
            position = 26 * index

            # parse data
            reservoir['wavelength'] = [float(entry) for entry in text[position + 1].split()[2:]]
            reservoir['radiance'] = [float(entry) for entry in text[position + 2].split()[2:]]
            reservoir['wavelengthii'] = [float(entry) for entry in text[position + 5].split()[2:]]
            reservoir['irradiance'] = [float(entry) for entry in text[position + 6].split()[2:]]
            reservoir['numerator'] = float(text[position + 9].split()[1])
            reservoir['denominator'] = float(text[position + 10].split()[1])
            #reservoir['ratio'] = reservoir['numerator'] / reservoir['denominator']
            reservoir['value'] = float(text[position + 11].split()[1])

            # append to data
            data.append(reservoir)

        # construct table headers
        header = ['scan_0based', 'row_0based', 'n360_ascii', 'n360_calc', 'n360_fortran', 'ratio_calc']
        header += ['final_rad_calc', 'final_rad_fortran', 'final_rad_diff']
        header += ['final_irr_calc', 'final_irr_fortran', 'final_irr_diff']
        header += ['pixel_0based']
        header += ['rad_weight', 'rad_wave_calc', 'rad_wave_fortran', 'rad_wave_diff']
        header += ['rad_calc', 'rad_fortran', 'rad_diff', 'rad_flag']
        header += ['irr_weight', 'irr_wave_calc', 'irr_wave_fortran', 'irr_wave_diff']
        header += ['irr_calc', 'irr_fortran', 'irr_diff', 'irr_flag']
        table = [header]

        # for each reservoir
        for reservoir in data[subset[0]:subset[1]]:

            # set first to false
            first = True

            # go through each pixel
            for index, pixel in enumerate(range(*pixels)):

                # begin line
                line = []
                if first:

                    # add elements
                    row = reservoir['row']
                    line.append(scan)
                    line.append(row)
                    line.append(sixty[scan, row])
                    line.append(value[scan, row])
                    line.append(reservoir['value'] * 100)
                    line.append(ratio[scan, row])
                    line.append(numerator[scan, row])
                    line.append(reservoir['numerator'])
                    line.append(reservoir['numerator'] - numerator[scan, row])
                    line.append(denominator[0, row])
                    line.append(reservoir['denominator'])
                    line.append(reservoir['denominator'] - denominator[0, row])

                    # set first to False
                    first = False

                # otherwise
                else:

                    # add seven blanks
                    for _ in range(12):

                        # add blank
                        line.append('')

                # add other data
                line.append(pixel)
                line.append(weight[scan, row, pixel])
                line.append(wavelength[scan, row, pixel])
                line.append(reservoir['wavelength'][index])
                line.append(line[-1] - line[-2])
                line.append(radiance[scan, row, pixel])
                line.append(reservoir['radiance'][index])
                line.append(line[-1] - line[-2])
                line.append(flag[scan, row, pixel])
                line.append(weightii[0, row, pixel])
                line.append(wavelengthii[0, row, pixel])
                line.append(reservoir['wavelengthii'][index])
                line.append(line[-1] - line[-2])
                line.append(irradiance[0, row, pixel])
                line.append(reservoir['irradiance'][index])
                line.append(line[-1] - line[-2])
                line.append(flagii[0, row, pixel])

                # add to table
                table.append(line)

        # construct csv
        destination = '../studies/diatom/ens/omocned_pixel_weights_{}.csv'.format(tag)
        self._table(table, destination)

        return None

    def viol(self, tag='march', resolution=20, margin=2):
        """Compare ultraviolet index between OMONUV and OMUVB products.

        Arguments:
            tag: str, name of data folder
            resolution: int, trackwise resolution of plots
            margin: float, percentile margin

        Returns:
            None
        """

        # gather data
        hydra = Hydra('{}/ultraviolet/surface/{}'.format(self.sink, tag))
        hydraii = Hydra('{}/ultraviolet/ocean/{}'.format(self.sink, tag))

        # sort paths
        paths = hydra.paths
        pathsii = hydraii.paths
        paths.sort()
        pathsii.sort()

        # begin data
        data = {}

        # for each pair of paths
        for path, pathii in zip(paths, pathsii):

            # get the ultraviolet index from OMUVB
            hydra.ingest(path)
            ultraviolet = hydra.grab('UVindex')

            # get the latitude, longitude, and ultraviolet index from OMOCNUV
            hydraii.ingest(pathii)
            latitude = hydraii.grab('latitude')
            longitude = hydraii.grab('longitude')
            ultravioletii = hydraii.grab('ultraviolet_index')
            zenith = hydraii.grab('solar_zenith_angle')

            # determine descending data range
            strip = latitude[:, 30]
            disparity = strip[1:] - strip[:-1]
            indices = numpy.where(disparity > 0)
            track = indices[0].min()
            trackii = indices[0].max()

            # subset arrays
            ultraviolet = ultraviolet[track:trackii]
            ultravioletii = ultravioletii[track:trackii]
            latitude = latitude[track:trackii]
            longitude = longitude[track:trackii]
            zenith = zenith[track:trackii]

            # calculate difference and percent
            difference = self._relate(ultraviolet, ultravioletii)
            percent = self._relate(ultraviolet, ultravioletii, percent=True)

            # add to data
            data.setdefault('ultraviolet', []).append(ultraviolet)
            data.setdefault('ultravioletii', []).append(ultravioletii)
            data.setdefault('latitude', []).append(latitude)
            data.setdefault('longitude', []).append(longitude)
            data.setdefault('difference', []).append(difference)
            data.setdefault('percent', []).append(percent)
            data.setdefault('zenith', []).append(zenith)

        # create arrays
        data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

        # specify title stubs
        stubs = {'ultraviolet': 'OMUVB ultraviolet index', 'ultravioletii': 'OMOCNUV ultraviolet index'}
        stubs.update({'difference': 'OMOCNUV UVI - OMUVB UVI', 'percent': '% difference OMOCNUV UVI / OMUVB UVI'})

        # specify gradients
        gradients = {'percent': 'coolwarm', 'difference': 'coolwarm'}
        selections = {}

        # specify scales
        scales = {'ultraviolet': (0, 16), 'ultravioletii': (0, 16)}
        scales.update({'difference': (-3, 3), 'percent': (-50, 50)})

        # specify units
        units = {'percent': '%'}

        # set fill
        fill = -9999

        # for each field
        fields = ['ultraviolet', 'ultravioletii', 'difference', 'percent']
        for field in fields:

            # specify color gradient and particular indices
            gradient = gradients.get(field, 'gist_rainbow_r')
            selection = selections.get(field, range(32, 256))

            # set omi components
            tracer = data[field]
            latitude = data['latitude']
            longitude = data['longitude']
            zenith = data['zenith']

            # create masks for finite data
            mask = (numpy.isfinite(data['ultraviolet'])) & (abs(data['ultraviolet']) < 1e15)
            mask = mask & (data['ultraviolet'] > fill + 1)
            mask = mask & (numpy.isfinite(data['ultravioletii'])) & (abs(data['ultravioletii']) < 1e15)
            mask = mask & (data['ultravioletii'] > fill + 1)
            mask = mask & (zenith <= 80)

            # get minimum and maximum of scale
            minimum = numpy.percentile(tracer[mask], margin)
            maximum = numpy.percentile(tracer[mask], 100 - margin)

            # set plot scale
            scale = scales.get(field, (minimum, maximum))

            # set units
            unit = units.get(field, 'UVI')

            # clip at upper bounds
            epsilon = 1e-5
            tracer = numpy.where(tracer < scale[0] + epsilon, scale[0] + epsilon, tracer)
            tracer = numpy.where(tracer > scale[1] - epsilon, scale[1] - epsilon, tracer)

            # replace masked values with fills
            tracer = numpy.where(mask, tracer, fill)

            # setup pace plot, full scale
            destination = '{}/ultraviolet/plots/OMOCNUV_OMUVB_{}_{}'.format(self.sink, field, tag)
            limits = (-90, 90, -180, 180)
            size = (14, 5)
            title = '{}\n{} 21, 2005'.format(stubs[field], tag.capitalize())

            # plot
            self._stamp('plotting {}...'.format(destination), initial=True)
            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': size, 'log': False, 'back': 'white', 'north': False, 'tight': True}
            options.update({'bar': True, 'extension': 'both', 'coast': 2, 'font': 14, 'fontii': 12})
            self._paint(*parameters, **options)

            # print status
            self._stamp('plotted.'.format(field))

        return None

    def violate(self, year=2005, month=3, day=21, orbit='o03630', prefix='', resolution=50):
        """Compare the ultraviolet surface ultraviolet irradiance of OMOCNUV vs OMUVB.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            orbit: str, orbit number

        Returns:
            None
        """

        # make output folders
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/sky'.format(self.sink))

        # get day of year
        julian = datetime.datetime(year, month, day).utctimetuple().tm_yday

        # create hydras
        formats = (year, self._pad(julian, 3))
        hydra = Hydra('/tis/acps/GES_DISC/003/OMUVB/{}/{}'.format(*formats))
        hydraii = Hydra('{}/ultraviolet'.format(self.sink))

        # ingest
        hydra.ingest(orbit.split('_')[0])
        hydraii.ingest(orbit)

        # get orbit and date
        date = hydraii._stage(hydraii.current)['day']
        # orbit = hydraii._stage(hydraii.current)['orbit']

        # set fill
        fill = -9999

        # get uvb data
        waves = [305, 310, 324, 380]
        surface = [hydra.grab('{}Irradiance{}'.format(prefix, wave)) for wave in waves]
        surface = numpy.array(surface).transpose(1, 2, 0)
        ultraviolet = hydra.grab('{}UVindex'.format(prefix))
        reflectivity = hydra.grab('LambertianEquivalentReflectivity')
        latitude = hydra.grab('Latitude')
        longitude = hydra.grab('Longitude')
        ozone = hydra.grab('OMTO3ColumnAmountO3')

        # get omocnuv data
        surfaceii = hydraii.grab('surface_irradiance_planar')
        reflectivityii = hydraii.grab('minimum_reflectivity')
        ozoneii = hydraii.grab('column_amount_ozone')

        # if CS prefix
        if prefix == 'CS':

            # get clear sky values instead
            surfaceii = hydraii.grab('clear_sky')

        # get rest of omocnuv data
        wavelength = hydraii.grab('spectral_wavelength')
        positions = [hydraii._pin(wave, wavelength)[0][0] for wave in waves]
        surfaceii = surfaceii[:, :, positions]
        surfaceii = 10.0 ** surfaceii
        ultravioletii = hydraii.grab('ultraviolet_index')

        # specify color gradient and particular indices
        gradient = 'gist_rainbow_r'
        selection = [index for index in range(32, 144)] + [index for index in range(176, 256)]

        # specify color gradient for difference plots
        gradientii = 'seismic'
        selectionii = [index for index in range(256)]

        # set figure size and limits
        limits = (-60, 60, -50, 50)
        size = (5, 10)
        logarithm = False
        bar = True

        # set up panels
        panels = [surface[:, :, position] for position, _ in enumerate(waves)]
        panels += [ultraviolet, reflectivity, ozone]
        panelsii = [surfaceii[:, :, position] for position, _ in enumerate(waves)]
        panelsii += [ultravioletii, reflectivityii, ozoneii]
        stubs = ['Surface_UV_{}nm'.format(wave) for wave in waves] + ['UVI', 'LER', 'Ozone']

        # set scales for surface irradiance
        scales = {305: (0, 120), 310: (0, 250), 324: (0, 600), 380: (0, 1000)}

        # set scales for irradiance difference
        scalesii = {305: (-10, 10), 310: (-60, 60), 324: (-50, 50), 380: (-150, 150)}

        # for each set
        for panel, panelii, stub, wave in zip(panels, panelsii, stubs, waves):

            # create difference
            difference = self._relate(panel, panelii)
            percent = self._relate(panel, panelii, percent=True)

            # create mask
            mask = (abs(panel) < 1e20) & (panel > 0) & (numpy.isfinite(panel))
            maskii = (abs(panelii) < 1e20) & (panelii > 0) & (numpy.isfinite(panelii))
            masque = numpy.logical_and(mask, maskii)

            # get main scales
            minimum = min([numpy.percentile(panel[masque], 5), numpy.percentile(panelii[masque], 5)])
            maximum = max([numpy.percentile(panel[masque], 95), numpy.percentile(panelii[masque], 95)])
            scale = (minimum, maximum)

            # get difference scale
            maximum = max([abs(numpy.percentile(difference[masque], 10)), abs(numpy.percentile(difference[masque], 90))])
            scaleii = (-maximum, maximum)

            # set absolute scales, ignore above
            scale = scales[wave]
            scaleii = scalesii[wave]

            # get percent scale
            # scaleiii = (percent[masque].min(), percent[masque].max())
            scaleiii = (-50, 50)

            # set epsilon
            epsilon = 1e-4

            # apply masques
            panel = numpy.where(masque, panel, fill)
            panelii = numpy.where(masque, panelii, fill)
            difference = numpy.where(masque, difference, fill)
            percent = numpy.where(masque, percent, fill)

            # saturate bounds
            panel = numpy.where(panel <= scale[0], scale[0] + epsilon, panel)
            panel = numpy.where(panel >= scale[1], scale[1] - epsilon, panel)
            panelii = numpy.where(panelii <= scale[0], scale[0] + epsilon, panelii)
            panelii = numpy.where(panelii >= scale[1], scale[1] - epsilon, panelii)
            difference = numpy.where(difference <= scaleii[0], scaleii[0] + epsilon, difference)
            difference = numpy.where(difference >= scaleii[1], scaleii[1] - epsilon, difference)
            percent = numpy.where(percent <= scaleiii[0], scaleiii[0] + epsilon, percent)
            percent = numpy.where(percent >= scaleiii[1], scaleiii[1] - epsilon, percent)

            print(stub)
            print(scale)
            print(panel.min(), panel.max())

            # set formats
            formats = (prefix, stub, date, orbit)

            # plot OMUVB
            title = '{}{}, OMUVB, \n{}, o{}'.format(*formats)
            destination = '{}/plots/sky/{}{}_OMUVB_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panel, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': (5, 10), 'log': False, 'bar': True, 'back': 'white'}
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters, **options)

            # plot OMOCNUV
            title = '{}{}, OMOCNUV, \n{}, o{}'.format(*formats)
            destination = '{}/plots/sky/{}{}_OMOCNUV_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[panelii, latitude, longitude], destination, [title, unit]]
            parameters += [scale, [gradient, selection], limits, resolution]
            options = {'size': (5, 10), 'log': False, 'bar': True, 'back': 'white'}
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters, **options)

            # plot difference
            title = '{}{}, OMOCNUV - OMUVB, \n{}, o{}'.format(*formats)
            destination = '{}/plots/sky/{}{}_Difference_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[difference, latitude, longitude], destination, [title, unit]]
            parameters += [scaleii, [gradientii, selectionii], limits, resolution]
            options = {'size': (5, 10), 'log': False, 'bar': True, 'back': 'white'}
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters, **options)

            # plot percent
            title = '{}{}, % OMOCNUV - OMUVB, \n{}, o{}'.format(*formats)
            destination = '{}/plots/sky/{}{}_Percent_{}_{}.png'.format(self.sink, *formats)
            unit = 'mW / m2 nm'
            parameters = [[percent, latitude, longitude], destination, [title, unit]]
            parameters += [scaleiii, [gradientii, selectionii], limits, resolution]
            options = {'size': (5, 10), 'log': False, 'bar': True, 'back': 'white'}
            self._stamp('plotting {}...'.format(destination))
            self._paint(*parameters, **options)

        return None

    def waver(self, tag='coefficient', folder='coefficient', scan=994, rows=(2, 9), pixels=(49, 65), target=359.88):
        """Compare nvalue differences to wavelengths

        Arguments:
            tag: str, tag for data
            folder: str, folder for originals
            scan: int, track number
            rows: tuple of ints, rows to check
            pixels: tuple of pixels
            target: float, exact wavelength for assignment

        Returns:
            None
        """

        # get the duel swath file
        duel = self._pull(tag, duel=True)

        # get n360 contents
        sixty = duel.grab('ascii_n360')
        sixtyii = duel.grab('omocned_n360')
        difference = sixtyii - sixty

        # make a hydra for the statics folder
        hydra = Hydra('../studies/diatom/frames/{}'.format(folder))

        # get radiance for band 3 and wavelength registration
        paths = [path for path in hydra.paths if 'OML1BRVG' in path]
        hydra.ingest(paths[0])
        radiance = hydra.grab('BAND3/radiance').squeeze()
        wavelength = hydra.grab('BAND3/wavelength').squeeze()

        # get irradiance for band 3 and wavelength registration
        paths = [path for path in hydra.paths if 'OML1BIRR' in path]
        hydra.ingest(paths[0])
        irradiance = hydra.grab('BAND3/irradiance').squeeze()
        wavelengthii = hydra.grab('BAND3/wavelength').squeeze()

        # grab the text printout
        text = self._know('../studies/diatom/ens/ens_{}.txt'.format(folder))

        # load in from text
        data = []
        chunk = 26
        for index, row in enumerate(range(*rows)):

            # begin row reservoir
            reservoir = {'row': row}

            # get position of first line
            position = 26 * index

            # parse data
            reservoir['wavelength'] = [float(entry) for entry in text[position + 1].split()[2:]]
            reservoir['radiance'] = [float(entry) for entry in text[position + 2].split()[2:]]
            reservoir['wavelengthii'] = [float(entry) for entry in text[position + 5].split()[2:]]
            reservoir['irradiance'] = [float(entry) for entry in text[position + 6].split()[2:]]
            reservoir['numerator'] = float(text[position + 9].split()[1])
            reservoir['denominator'] = float(text[position + 10].split()[1])
            reservoir['value'] = float(text[position + 11].split()[1])

            # append to data
            data.append(reservoir)

        # construct table headers
        header = ['scan_0based', 'row_0based', 'n360_ascii', 'n360_omocned', 'n360_difference']
        header += ['numerator', 'denominator', 'pixel_0based']
        header += ['rad_wave_l1b', 'rad_wave_fortran', 'rad_wave_diff']
        header += ['rad_l1b', 'rad_fortran', 'rad_diff']
        header += ['irr_wave_l1b', 'irr_wave_fortran', 'irr_wave_diff']
        header += ['irr_l1b', 'irr_fortran', 'irr_wave']
        table = [header]

        # for each reservoir
        for reservoir in data:

            # set first to false
            first = True

            # go through each pixel
            for index, pixel in enumerate(range(*pixels)):

                # begin line
                line = []
                if first:

                    # add elements
                    row = reservoir['row']
                    line.append(scan)
                    line.append(row)
                    line.append(sixty[scan, row])
                    line.append(sixtyii[scan, row])
                    line.append(difference[scan, row])
                    line.append(reservoir['numerator'])
                    line.append(reservoir['denominator'])

                    # set first to False
                    first = False

                # otherwise
                else:

                    # add seven blanks
                    for _ in range(7):

                        # add blank
                        line.append('')

                # add other data
                line.append(pixel)
                line.append(wavelength[row, pixel])
                line.append(reservoir['wavelength'][index])
                line.append(line[9] - line[8])
                line.append(radiance[scan, row, pixel])
                line.append(reservoir['radiance'][index])
                line.append(line[12] - line[11])
                line.append(wavelengthii[row, pixel])
                line.append(reservoir['wavelengthii'][index])
                line.append(line[15] - line[14])
                line.append(irradiance[row, pixel])
                line.append(reservoir['irradiance'][index])
                line.append(line[18] - line[17])

                # add to table
                table.append(line)

        # construct csv
        destination = '../studies/diatom/ens/omocned_pixels_{}.csv'.format(folder)
        self._table(table, destination)

        # plot nvalue differences vs closeness to a wavelength edge
        differences = []
        edges = []
        tops = []
        bottoms = []
        for row in range(60):

            # get difference
            differences.append(abs(difference[scan, row]))

            # for each pixel
            gaps = []
            for pixel in pixels:

                # add absolute distances from edges for radiance
                gap = abs(wavelength[row, pixel] - (target - 1))
                gaps.append(float(gap))
                gap = abs(wavelength[row, pixel] - (target + 1))
                gaps.append(float(gap))

                # add absolute distances from edges for irradiance
                gap = abs(wavelengthii[row, pixel] - (target - 1))
                gaps.append(float(gap))
                gap = abs(wavelengthii[row, pixel] - (target + 1))
                gaps.append(float(gap))

            # add the minimum gap to edges
            tops.append(min(gaps[:2]))
            bottoms.append(min(gaps[2:]))
            edges.append(min(gaps))

        # plot abs edge
        address = 'plots/edges/N360_differences_wavelength_window_edges.h5'
        title = 'n360 difference vs closeness to wavelength edge'
        self.squid.ink('n360_difference', differences, 'wavelength_edge', edges, address, title)

        # plot radiancee edge
        address = 'plots/edges/N360_differences_wavelength_window_edges_radiance.h5'
        title = 'n360 difference vs closeness to wavelength radiance edge'
        self.squid.ink('n360_difference_radiance', differences, 'radiance_wavelength_edge', tops, address, title)

        # plot irradiance edge
        address = 'plots/edges/N360_differences_wavelength_window_edges_irradiance.h5'
        title = 'n360 difference vs closeness to wavelength irradiance edge'
        self.squid.ink('n360_difference_irradiance', differences, 'irradiance_wavelength_edge', bottoms, address, title)

        return data


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad up to six
    number = 6
    arguments += [''] * number
    arguments = arguments[:number]

    # if melt option
    if '--melt' in options:

        # unpack arguments
        sink, year, month, start, _, _ = arguments

        # make diatom
        diatom = Diatom(sink)

        # for each day in the month
        for day in range(int(start), 32):

            # collect
            diatom.melt(int(year), int(month), int(day), anomaly=True)

    # if trend option
    if '--trend' in options:

        # upack arguemnts
        sink, year, month, _, _, _ = arguments

        # make diatoom
        diatom = Diatom(sink)

        # collect
        diatom.trend(int(year), months=(int(month), int(month)+1), days=31, collect=True)

    # otherwise
    else:

        # unpack arguments
        orbit, date, source, sink, controller, tag = arguments

        # initialize diatom
        diatom = Diatom(sink)

        # create replacements
        replacements = diatom.select(orbit, date, source, sink, tag)
        diatom.control(controller, source, replacements)

