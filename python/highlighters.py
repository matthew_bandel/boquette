# highlighters.py to seek references amongst text files

# import general tools
import os
import sys
import json
import shutil
import pprint


# define class Highlighter to search out references
class Highlighter(list):
    """Class Highlighter to find references amongst text or code files.

    Inherits from:
        list
    """

    def __init__(self, directory=None):
        """Instantiate a Highlighter instance.

        Arguments:
            None
        """

        # add directory
        self.directory = directory

        return

    def __repr__(self):
        """Create onscreen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = '< Highlighter instance at: {} >'.format(self.directory)

        return representation

    def _branch(self, search, instances):
        """Branch into a tree structure.

        Arguments:
            search: word to search for
            instances: list of str, word

        Returns:
            dict
        """

        # begin tree
        tree = {}

        # find all words
        words = instances[search]
        for word in words:

            # add branch to tree
            tree[word] = self._branch(word.split('/')[-1], instances)

        return tree

    def _group(self, members, function):
        """Group a set of members by the result of a function of the member.

        Arguments:
            members: list of dicts
            function: str

        Return dict
        """

        # get all fields
        fields = self._skim([function(member) for member in members])

        # create groups
        groups = {field: [] for field in fields}
        [groups[function(member)].append(member) for member in members]

        return groups

    def _jot(self, lines, destination):
        """Jot down the lines into a text file.

        Arguments:
            lines: list of str
            destination: str, file path

        Returns:
            None
        """

        # add final endline
        lines = [line + '\n' for line in lines]

        # save lines to file
        with open(destination, 'w') as pointer:

            # write lines
            pointer.writelines(lines)

        return None

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # default to empty set
        paths = []

        # make paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        return paths

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _survey(self, directory=None):
        """Recursively survey all paths in a nested directory.

        Argument:
            directory: str, path to directory

        Returns:
            list of str, filepaths
        """

        # set default directory
        directory = directory or self.directory

        # begin collection
        collection = []

        # get all paths in the directory
        paths = self._see(directory)
        for path in paths:

            # try to
            try:

                # add all paths in the next directory
                collection += self._survey(path)

            # unless it is a file
            except NotADirectoryError:

                # add the path directly
                collection.append(path)

        return collection

    def _transcribe(self, path):
        """Transcribe the text file at the path.

        Arguments:
            path: str, file path

        Returns:
            list of str
        """

        # read in file pointer
        with open(path, 'r') as pointer:

            # read in text and eliminate endlines
            lines = pointer.readlines()
            lines = [line.replace('\n', '') for line in lines]

        return lines

    def eliminate(self, *words, ask=True, minimum=0.2):
        """Eliminate duplicate copies of data files.

        Arguments:
            *words: unpacked list of priorty directories
            ask: ask before eliminating?
            minimum: float, minimum file size in GB

        Returns:
            None
        """

        # get all paths
        paths = self._survey()

        # group all paths by file name
        files = self._group(paths, lambda path: path.split('/')[-1])

        # get file sizes
        firsts = [members[0] for members in files.values()]
        sizes = [os.path.getsize(first) / (1024 ** 3) for first in firsts]

        # make zipper
        zipper = list(zip(sizes, files.keys(), files.values()))
        zipper.sort(key=lambda triplet: triplet[0], reverse=True)
        zipper = [triplet for triplet in zipper if triplet[0] > minimum]
        zipper = [triplet for triplet in zipper if len(triplet[2]) > 1]

        # for each set
        for size, name, members in zipper:

            # for each word
            for word in words:

                # sort according to presence
                members.sort(key=lambda member: word in member, reverse=True)

            # print list
            print(f'\n{round(size, 2)} GB: {name}')
            [print(f'\t{index}: {path}') for index, path in enumerate(members)]

            # set eliminations as all but first
            priority = members[0]
            eliminations = members[1:]

            # if ask requested
            if ask:

                # input choice
                choice = int(input('\nkeep? ') or 0)

                # get paths for elimination
                priority = members[choice]
                eliminations = [member for index, member in enumerate(members) if index != choice]

            # for each elimination
            for elimination in eliminations:

                # create text file
                extension = elimination.split('.')[-1]
                receipt = elimination.replace('.' + extension, '.txt')

                # print
                print(f'eliminating {elimination}...')
                self._jot([priority], receipt)

                # delete file
                os.remove(elimination)

        return None

    def light(self, word, folder=None, directory=None, names=False):
        """Seek word references amongst all text files in a directory.

        Arguments:
            word: str, reference to seek
            directory: str, directory path
            folder: str, folder for dumping

        Returns:
            None
        """

        # get default directory and folder
        directory = directory or self.directory
        folder = folder or 'highlighter/highlights'

        # begin report
        report = ['search {} for {}'.format(directory, word)]

        # get all paths from the directory
        paths = self._survey(directory)
        paths.sort(key=lambda path: path.lower().split('.')[0].split('/')[-1])
        paths.sort(key=lambda path: path.lower().split('.')[-1])

        # go through all paths
        for path in paths:

            # if looking for file names only
            transcription = []
            if not names:

                # try to:
                try:

                    # transcribe
                    transcription += self._transcribe(path)

                # unless it can't be read
                except UnicodeDecodeError:

                    # in which case, skip
                    pass

            # add path name to transcription
            transcription += [path]

            # collect instances
            instances = [(index, line) for index, line in enumerate(transcription) if word.lower() in line.lower()]

            # if there are instances
            if len(instances) > 0:

                # add path to report
                report.append(' ')
                report.append(path)

                # add each instance
                for index, line in instances:

                    # add to report, adding 1 to index because editor starts at 1
                    highlight = line.lower().replace(word.lower(), ' [** {} **] '.format(word.lower()))[:100]
                    report.append('    {}) {}'.format(index + 1, highlight))

        # save report
        destination = '{}/highlights_{}_in_{}.txt'.format(folder, word, directory.split('/')[-1])
        self._jot(report, destination)

        return None

    def manifest(self, folder):
        """Replace text ious with actual data files.

        Arguments:
            folder: str, directory path

        Returns:
            None
        """

        # get all text files
        paths = self._see(folder)
        texts = [path for path in paths if '.txt' in path]

        # go through texts
        for text in texts:

            # open the file
            original = self._transcribe(text)[0]

            # replace folder
            manifestation = '{}/{}'.format(folder, original.split('/')[-1])

            # make copy
            print(f'\ncopying {original} into \n{manifestation}...')
            shutil.copy(original, manifestation)

            # delete text
            os.remove(text)

        return None

    def scrounge(self, minimum=0.1, directory=None):
        """Report on directory sizes.

        Arguments:
            minimum: float, minimum size in GB

        Returns:
            None
        """

        # get default directory
        directory = directory or self.directory

        # begin report
        report = ['directory sizes in {}'.format(directory), ' ']

        # get all paths from the directory
        paths = self._survey(directory)

        # function to extract folder from path
        folding = lambda path: '/'.join(path.split('/')[:-1])

        # get all folders
        folders = [folding(path) for path in paths]
        folders = list(set(folders))

        # go through all folders and get size
        sizes = {folder: 0.0 for folder in folders}
        for path in paths:

            # try to:
            try:

                # retrieve the size
                size = os.path.getsize(path)

                # determine folder
                folder = folding(path)

                # convert from bytes and add to sizes
                gigabytes = size / (1024 ** 3)
                sizes[folder] += gigabytes

            # unless an error
            except PermissionError:

                # in which case, nevermind
                pass

        # sort by sizes and exclude minimums
        sizes = [(folder, size) for folder, size in sizes.items() if size > minimum]
        sizes.sort(key=lambda pair: pair[1], reverse=True)

        # add each to report
        [report.append('{} GB: {}'.format(round(size, 2), folder)) for folder, size in sizes]

        # calculate sum
        total = sum([item[1] for item in sizes])
        report += [' ', 'total: {} GB'.format(round(total), 2)]

        # save report
        destination = 'highlighter/usages/usage_in_{}.txt'.format(directory.split('/')[-1])
        self._jot(report, destination)

        return None

    def sniff(self, search, folder, extension='.js', signifier='function', comment='//', exclusions=None, ignores=None):
        """Sniff all instances of a word in a folder of code.

        Arguments
            search: str, word to find
            folder: str, file path of code files
            extension: str, extension of files to seearch
            signifier: new function signifier
            comment: str, comment signifier
            exclusions: list of str, markers for outside function sections
            ignores: list of str, files to skip

        Returns:
            None
        """

        # set default exclusions and skips
        exclusions = exclusions or []
        ignores = ignores or []

        # get all paths from the folder with the right extension
        paths = [path for path in self._see(folder) if path.endswith(extension) and path.split('/')[-1] not in ignores]

        # begin functions dictionary
        functions = {}

        # for each path
        for path in paths:

            # set default name
            stub = path.split('/')[-1]
            name = '{}/initialization'.format(stub)

            # transcribe block of code
            block = []
            code = self._transcribe(path)
            for line in code:

                # check for comment signifire
                if not line.strip().startswith(comment) and all([exclusion not in line for exclusion in exclusions]):

                    # check for new function
                    if line.strip().startswith(signifier):

                        # add block to function
                        functions[name] = block

                        # begin new name and new block
                        name = '{}/{}'.format(stub, line.strip().strip(signifier).split()[0].split('(')[0])
                        block = [line]

                    # otherwise
                    else:

                        # add to current block
                        block.append(line)

            # account for last block
            functions[name] = block

        # set words to word
        words = [search]
        instances = {}
        while len(words) > 0:

            # for each word
            new = []
            for word in words:

                # find all functions with word therein
                harbors = [name for name, block in functions.items() if any(['{}('.format(word) in line for line in block])]
                harbors = [name for name in harbors if not name.endswith(word)]
                instances[word] = harbors

                # add new words
                new += list(set([name.split('/')[-1] for name in instances[word]]))

            # set new words to new
            new = [word for word in new if word not in instances.keys()]
            words = new

        # begin tree
        tree = {search: self._branch(search, instances)}

        # print to log file
        with open('highlighter/trees/tree_{}.txt'.format(search), 'w') as pointer:

            # pprint to ponter
            pprint.pprint(tree, pointer)

        return tree

    def synchronize(self, name, directory=None):
        """Renew all copies of a file, based on last modified.

        Arguments:
            name: str, file name

        Returns:
            None
        """

        # set default directory
        directory = directory or self.directory

        # get all paths
        collection = self._survey(directory)

        # get appropriate paths
        paths = [path for path in collection if name in path]

        # attach to date modified and sort with most recent on top
        modifications = [(path, os.stat(path).st_mtime) for path in paths]
        modifications.sort(key=lambda pair: pair[1], reverse=True)

        # replace older with newer versions
        newest = modifications[0][0]
        older = [modification[0] for modification in modifications[1:]]
        for path in older:

            # check before copying
            approval = input('copy {} into {}?'.format(newest, path))

            # check for blank
            if approval in ('', ' ',):

                # use shutil to copy newest in new spot
                print('copying...'.format(newest, path))
                shutil.copy(newest, path)

            # otherwise
            else:

                # abport
                print('aborted.')

        return None