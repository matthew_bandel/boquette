# machinas.py for Boquette class to view OMI hdf data using bokeh

# import local classes
from hydras import Hydra
from plates import Plate

# import system tools
import sys

# import datetime
import datetime

# import itertools and collections
import itertools
from collections import Counter

# import math and numpy
import math
import numpy
import scipy

# import xarray
import xarray
xarray.set_options(keep_attrs=True)

# import regex
import re

# import pillow for creating icons
from PIL import Image, ImageDraw

# import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.svm import OneClassSVM, SVC
from sklearn.decomposition import PCA

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import gridspec
from matplotlib import colorbar
from matplotlib import colors as Colors
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib import path as Path
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False



# pdf maker
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter

# import xarray
import xarray
xarray.set_options(keep_attrs=True)

# import modules
from cores import Core
from formulas import Formula
from plates import Plate
from hydras import Hydra


# class Machina to do OMI data mining
class Machina(Hydra):
    """Machina class to do OMI data compariwons.

    Inherits from:
        Core
    """

    def __init__(self, source, sink):
        """Initialize an Machina instance.

        Arguments:
            None

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, source)

        # add sink directory
        self.sink = sink

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Machina instance at: {} >'.format(self.source)

        return representation

    def _quantize(self, zee):
        """Convert a zee score into a quantile.

        Arguments:
            zee: zee score

        Returns:
            float
        """

        # use the erf to compute the quantile
        quantile = scipy.stats.norm.cdf(zee)
        percentile = int(quantile * 100)

        return percentile

    def _envision(self, matrix, destination, title='', units='', dependent='', independent='', spectrum=None, coordinates=None):
        """Envision a matrix as a heat map.

        Arguments:
            matrix: 2-D numpy array
            destination: file path for saving image
            title='': str, figure title
            units='': str, figure units
            dependent='': str, figure yaxis label
            independent='': str, figure xaxis label
            coordinates: dict of lists

        Returns:
            None
        """

        # set default zoom
        zoom = (0, len(matrix[0]), 0, len(matrix))

        # make default labels
        independent = independent or 'horizontal gridpoint'
        dependent = dependent or 'vertical gridpoint'

        # set default coordinates
        if not coordinates:

            # default to indices
            coordinates = {independent: list(range(matrix.shape[1]))}
            coordinates.update({dependent: list(range(matrix.shape[0]))})

        # make xarry matrix
        matrix = xarray.DataArray(matrix, dims=[dependent, independent], coords=coordinates)
        matrix.attrs.update({'route': title, 'units': units})

        # make plate object and draw graph
        spectrum = spectrum or 'classic'
        plate = Plate(matrix, ['vertical', 'horizontal'], zoom, title, spectrum=spectrum, unity=units)
        plate.glance()

        # resave glance image to destination
        image = Image.open('../plots/glance.png')
        image.save(destination)

        return None

    def _support(self, data, abscissas, void, window=None, fraction=None):
        """Use a one class support vector machine to detect anamolies.

        Arguments:
            data: list of floats
            abscissas: list of ints
            void: the void value
            window: int, number of past events
            fraction: float, training, test split

        Returns:
            tuple of lists of floats
        """

        # set defaults
        window = window or self.window
        fraction = fraction or self.fraction

        # prepare abscissaal sequences
        training, verification, samples = self._prepare(abscissas, window, fraction, center=True)

        # create registry
        registry = {abscissa: datum for abscissa, datum in zip(abscissas, data) if datum != void}

        # calculate average of all registry data
        average = sum(registry.values()) / len(registry)

        # generate targets and training matrix
        matrix = []
        for sequence in training:

            # try to
            try:

                # create vector for sample and add to matrix
                vector = [registry[number] for number in sequence]
                matrix.append(vector)

            # unless it is missing due to voids
            except KeyError:

                # in which case, skip
                pass

        # perform model fit
        machine = OneClassSVM(gamma='scale', kernel='rbf', nu=0.05)
        machine.fit(matrix)

        # get middle index
        middle = math.floor(window / 2)

        # generate interpolated data to test
        normals = []
        anomalies = []
        for sequence in verification + samples:

            # create vector
            vector = [registry.setdefault(number, average) for number in sequence]

            # make prediction
            prediction = machine.predict([vector])[0]

            # if the normal class is predicted
            if prediction > 0:

                # add to normals and anomalies
                normals.append(registry[sequence[middle]])
                anomalies.append(self.fill)

            # otherwise
            else:

                # add to normals and anomalies
                anomalies.append(registry[sequence[middle]])
                normals.append(self.fill)

        # make arrays
        normals = numpy.array(normals)
        anomalies = numpy.array(anomalies)

        return normals, anomalies

    def highlight(self, orbit, path, window=11):
        """Characterize an orbit according to zscores of file parameters.

        Arguments:
            orbit: int, orbit number
            path: str, filepath
            window: range of orbits within which to look

        Returns:
            None
        """

        # ingest the path
        self.ingest(path)

        # find the appropriate index of the orbit
        orbits = self.dig('orbit_number')[0].distil().squeeze()
        index = orbits.tolist().index(orbit)

        # get half of the window size
        half = math.floor(window / 2)
        indices = list(range(index - half, index + half + 1))

        # subset categories and variables
        features = self.dig('Categories') + self.dig('IndependentVariables')
        features = [feature for feature in features if 'orbit_mean' in feature.name or 'orbit_std' in feature.name]

        # begin accumulating scores
        scores = []
        for count, feature in enumerate(features):

            # check for appropriate shape
            if feature.shape == (len(orbits),):

                # convert feature to zscores
                data = feature.distil()
                mean = float(data.mean())
                deviation = float(data.std()) or 1.0

                # convert to zscores
                zees = (data - mean) / deviation

                # add all entries in the window to scores
                scores += [(entry - index, feature.name, feature.slash, float(zees[entry])) for entry in indices]

        # sort scores by absolute z-score
        scores = [score for score in scores if numpy.isfinite(score[-1])]
        scores.sort(key=lambda score: abs(score[-1]), reverse=True)

        # begin report
        def signing(digit): return str(digit) if digit < 0 else '+' + str(digit)
        def formatting(score): return (signing(score[0]), self._quantize(score[-1]), score[1], score[2][:40])
        report = ['Orbit Highlights Orbit: {}'.format(orbit)]
        report += ['({}) ({}th) {}    {}'.format(*formatting(score)) for score in scores]

        # save report
        destination = '{}/{}_window{}_highlights.txt'.format(self.sink, orbit, window)
        self._jot(report, destination)

        return None

    def novelize(self, trainers=None, testers=None, fraction=0.05, band=3, number=6):
        """Use One-Class SVM for detecting anomalous rows.

        Arguments:
            trainers: list of str, filepaths
            testers: list of str, filepaths
            fraction: fraction of outlier allowed in training
            band: int, band number
            number: int, number of clusters

        Returns:
            None
        """

        # set rows
        rows = 30 if band == 1 else 60

        # set margins
        margin = 400
        marginii = 20

        # sort files by date
        paths = trainers or self.paths
        paths.sort()

        # extract training radiances from first path
        radiances = []
        for path in paths:

            # print status
            self._print('ingesting {} for training data...'.format(path))

            # ingest path and add radiances to traininer
            self.ingest(path)
            radiance = self.dig('BAND{}/radiance'.format(band))[0].distil().squeeze()

            # chop off margins timewise and spectrally
            radiance = radiance[margin:-margin, :, marginii:-marginii]

            # add to radiances
            radiances.append(radiance)

        # concatentate radiances
        radiances = numpy.concatenate(radiances)

        # start timing
        self._stamp('training bank of machines from {}...'.format(paths[0]), initial=True)

        # make machines for each row
        machines = []
        verifications = []
        for row in range(rows):

            # print row
            self._print('row {}...'.format(row))

            # extract training data
            matrix = radiances[:, row, :]

            # perform model fit
            machine = OneClassSVM(gamma='scale', kernel='rbf', nu=fraction)
            machine.fit(matrix)

            # add machine
            machines.append(machine)

            # get training predictions
            verification = machine.score_samples(matrix)
            categories = machine.predict(matrix)
            verification = verification / verification.max()
            verifications.append(verification * categories)

        # plot verifications
        stage = self._stage(paths[0])
        orbit, date = stage['orbit'], stage['date']
        verifications = numpy.array(verifications)
        destination = '{}/plots/verifcation.png'.format(self.sink)
        formats = (band, orbit, date, fraction)
        title = 'Band{} Radiances, Training orbit: {}, {}, One Class SVM, nu = {}'.format(*formats)
        self._envision(verifications, destination, title, 'score', 'row', 'track')

        # make predictions for each remaining path
        details = []
        for path in testers:

            # start timing
            self._stamp('predicting on {}...'.format(path))

            # extract data
            self.ingest(path)
            radiance = self.dig('BAND{}/radiance'.format(band))[0].distil().squeeze()
            radiance = radiance[margin:-margin, :, marginii:-marginii]

            # collect predictions for each row
            predictions = []
            for row in range(rows):

                # extract training data
                matrix = radiance[:, row, :]

                # perform model fit
                machine = machines[row]
                prediction = machine.score_samples(matrix)
                categories = machine.predict(matrix)
                prediction = prediction / prediction.max()
                predictions.append(prediction * categories)

                # get mask of errors and add to masks
                mask = categories < 0

                # add to details
                detail = (path, row, matrix, mask)
                details.append(detail)

            # plot predictions
            stage = self._stage(path)
            orbit, date = stage['orbit'], stage['date']
            predictions = numpy.array(predictions)
            destination = '{}/plots/anomalies_{}.png'.format(self.sink, orbit)
            formats = (band, orbit, date, fraction)
            title = 'Band{} Radiances, Predictions for Orbit: {}, {}, One Class SVM, nu = {}'.format(*formats)
            self._envision(predictions, destination, title, 'score', 'row', 'track')

        # collection all errors
        errors = []
        for path, row, matrix, mask in details:

            # check for non zeros
            error = matrix[mask]
            if error.shape[0] > 0:

                # add to errors
                errors.append(error)

        # get errors
        errors = numpy.concatenate(errors)

        # cluster errors
        self._stamp('clustering errors...')
        clusterer = KMeans(n_clusters=number)
        clusterer.fit(errors)

        # sort details by path
        paths = self._group(details, lambda detail: detail[0])
        for path, details in paths.items():

            # sort by row
            details.sort(key=lambda detail: detail[1])

            # determine categories
            analysis = numpy.full((details[0][2].shape[0], rows), number)
            for _, row, matrix, mask in details:

                # get clusters
                clusters = clusterer.predict(matrix[mask])

                # add to analysis
                analysis[mask, row] = clusters

            # plot predictions
            analysis = analysis.transpose(1, 0)
            stage = self._stage(path)
            orbit, date = stage['orbit'], stage['date']
            destination = '{}/plots/categories_{}.png'.format(self.sink, orbit)
            formats = (band, orbit, date, fraction)
            title = 'Band{} Radiances, Kmeans for Orbit: {}, {}, One Class SVM, nu = {}'.format(*formats)
            self._envision(analysis, destination, title, 'score', 'row', 'track')

        # make cluster map
        clusters = clusterer.predict(errors)

        # add a sample of radiances for regulars
        regulars = radiances[:10000, 10, :]

        # make pca decomposition
        decomposer = PCA(n_components=2)
        errors = decomposer.fit_transform(errors)
        regulars = decomposer.transform(regulars)

        # begin cluster map
        print('making map...')
        pyplot.clf()

        # plot regulars
        abscissa = [regular[0] for regular in regulars]
        ordinate = [regular[1] for regular in regulars]
        pyplot.plot(abscissa, ordinate, marker='x', color='gold', linewidth=0)

        # set colors
        colors = ['black', 'blue', 'darkcyan', 'darkgreen', 'green', 'lightgreen'][:number]

        # plot clusters
        for label, color in enumerate(colors):

            # get subset or errors
            subset = [error for error, cluster in zip(errors, clusters) if cluster == label]

            # plot
            abscissa = [error[0] for error in subset]
            ordinate = [error[1] for error in subset]
            pyplot.plot(abscissa, ordinate, marker='x', color=color, linewidth=0)

        # save plot
        pyplot.title('Anomaly Clusters, after 2-D PCA')
        destination = '{}/plots/error_clusters.png'.format(self.sink)
        pyplot.savefig(destination)

        # clear
        pyplot.clf()

        return None






