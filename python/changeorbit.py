# changeorbit.py to change the metadata orbit numbers in Level1B OMI .he4 files

# import general tools
import sys
import os
import re
import shutil

# import pyhdf to handle Collection 3 hdf4 data
from pyhdf.HDF import HDF, HDF4Error, HC
from pyhdf.SD import SD, SDC, SDAttr
from pyhdf.V import V


# class OrbitChanger to change the orbit of OMI L1B .he4 files
class OrbitChanger(list):
    """OrbitChanger class to swap orbit numbers in OMI Level1B .he4 files.

    Inherits from:
        list
    """

    def __init__(self, path='', destination=''):
        """Initialize a OrbitChanger instance.

        Arguments:
            path: str, file name
            destination: new file name

        Returns:
            None
        """

        # set directory information
        self.path = path
        self.destination = destination

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < OrbitChanger instance at: {} >'.format(self.source)

        return representation

    def _copy(self, path, destination):
        """Copy a file from one path to another.

        Arguments:
            path: str, filepath
            destination: str, filepath

        Returns:
            None
        """

        # try to copy
        try:

            # copy the file
            shutil.copy(path, destination)

        # unless it is a directory
        except IsADirectoryError:

            # in which case, alert and skip
            self._print('{} is a directory'.format(path))

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        # # go through each meassage
        # for message in messages:
        #
        #     # print
        #     print(message)

        return message

    def _stage(self, path):
        """Get the file meta attributes from the file name.

        Arguments:
            None

        Returns:
            dict
        """

        # extract the orbit specifics
        date = re.search('[0-9]{4}m[0-9]{4}t[0-9]{4}', path)
        orbit = re.search('o[0-9]{5,6}', path)
        product = re.search('OM[A-Z,0-9]{3,10}', path)
        production = re.search('[0-9]{4}m[0-9]{4}t[0-9]{4}', path.split('_')[-1])
        version = re.search('_v[0-9]{3,4}', path)
        extension = '.{}'.format(path.split('.')[-1])

        # gather up the date details
        details = {}
        details['date'] = date.group() if date else '_'
        details['year'] = details['date'][:4] if len(str(date)) > 1 else '_'
        details['month'] = details['date'][:7] if len(str(date)) > 1 else '_'
        details['day'] = details['date'][:9] if len(str(date)) > 1 else '_'

        # and other details
        details['orbit'] = ('00' + orbit.group().split('o')[1])[-6:] if orbit else '_'
        details['product'] = product.group() if product else '_'
        details['production'] = production.group() if production else '_'
        details['version'] = version.group().strip('_v') if version else '_'
        details['collection'] = '3' if '3' in details['version'] else '4'
        details['extension'] = extension

        return details

    def swap(self):
        """Swap the orbits numbers in the metadata of an he4 file.

        Arguments:
            path: str, file path
            orbit: int, new orbit number

        Returns:
            None
        """

        # get the file path and destination
        path = self.path
        destination = self.destination

        # copy file
        self._print('\ncopying {} to {}...'.format(path, destination))
        self._copy(path, destination)

        # parse the original and replacement orbit numbers
        orbit = str(int(self._stage(path)['orbit']))
        replacement = str(int(self._stage(destination)['orbit']))
        self._print('\nchanging orbit {} to {}...'.format(orbit, replacement))

        # open up hdf4 interfaces
        science = SD(destination, SDC.WRITE)

        # grab attributes
        core = 'CoreMetadata.0'
        attributes = science.attributes()
        text = attributes[core]

        # copy the string to the new string
        swap = text

        # set replacement fields
        fields = ('LOCALGRANULEID', 'ORBITNUMBER')

        # for each field
        for field in fields:

            # get the first and second orccurence
            first = swap.index(field)
            second = swap.index(field, first + 1)

            # print status
            self._print('\nreplacing: ')
            self._print(swap[first:second])

            # make swap
            swap = swap[:first] + swap[first:second].replace(orbit, replacement) + swap[second:]

            # print status
            self._print('\nwith: ')
            self._print(swap[first:second])

        # rewrite attribute
        attribute = science.attr(core)
        attribute.set(SDC.CHAR, swap)

        # close files
        science.end()

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 2
    arguments = arguments[:2]

    # umpack arguments
    path, destination = arguments

    # create changer instance
    changer = OrbitChanger(path, destination)

    # perform swap
    changer.swap()
