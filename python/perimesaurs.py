#!/usr/bin/env python3

# perimesaurs.py for the Perimesaur class to enclose points within a perimeter

# import core HDF file handler
from hydras import Hydra

# import system
import sys

# import numpy functions
import numpy

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# # import matplotlib for plots
# import matplotlib
# from matplotlib import pyplot
# from matplotlib import style as Style
# from matplotlib import rcParams
# matplotlib.use('Agg')
# Style.use('fast')
# rcParams['axes.formatter.useoffset'] = False
# rcParams['figure.max_open_warning'] = False


# class Perimesaur to segregate points into those within and without a perimeter
class Perimesaur(Hydra):
    """Perimesaur class to segregate points into those within and without a perimeter.

    Inherits from:
        Hydra
    """

    def __init__(self):
        """Initialize instance.

        Arguments:
            None
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Perimesaur instance >'

        return representation

    def generate(self, sides=3):
        """Generate a perimeter with a number of sides.

        Arguments:
            sides: int, number of sides

        Returns:
            (list of floats, list of floats) tuple
        """

        # begin with a triangle at random
        perimeter = numpy.random.random((3, 2))

        # while more sides are desired
        while len(perimeter) < sides:

            # get the center point
            center = perimeter.mean(axis=0)

            # calculate slope and intercept from first and last point
            slope = (perimeter[-1, 0] - perimeter[0, 0]) / (perimeter[-1, 1] - perimeter[0, 1])
            intercept = perimeter[0, 0] - slope * perimeter[0, 1]

            # test if the center is above the line
            polarity = bool(center[0] > slope * center[1] + intercept)

            # while there are no pionts
            points = []
            while len(points) < 1:

                # create random samples
                samples = numpy.random.random((100, 2))

                # check for all samples on the other side of the polarity
                if polarity:

                    # make maske
                    mask = samples[:, 0] < slope * samples[:, 1] + intercept

                # otherwise
                else:

                    # make mask
                    mask = samples[:, 0] > slope * samples[:, 1] + intercept

                # extrack points
                points = samples[mask]

                # only keep those within the latitude limits
                minimum = min([perimeter[0, 0], perimeter[-1, 0]])
                maximum = max([perimeter[0, 0], perimeter[-1, 0]])
                maskii = (points[:, 0] > minimum) & (points[:, 0] < maximum)
                points = points[maskii]

            # add first point to polygon
            perimeter = numpy.concatenate([perimeter, [points[0]]])

        # extracxt latitude and longitude
        latitudes = perimeter[:, 0]
        longitudes = perimeter[:, 1]

        return latitudes, longitudes

    def plot(self, insiders, outsiders, center, perimeter):
        """Create a scatter plot of insiders and outsiders.

        Arguments:
            insiders: numpy.array of lat lon coordinates (n x 2)
            outsiders: numpy.array of lat lon coordinates (n x 2)
            center: pair of floats, lat, lon of polygon center
            perimeter: numpy array, perimeter coordinates

        Returns:
            None
        """

        # create folder
        self._make('./perimesaur')

        # timestamp
        self._stamp('plotting {} points...'.format(len(insiders) + len(outsiders)), initial=True)

        # create plot
        pyplot.clf()
        pyplot.plot(insiders[:, 1], insiders[:, 0], 'r,')
        pyplot.plot(outsiders[:, 1], outsiders[:, 0], 'g,')
        pyplot.plot(perimeter[:, 1], perimeter[:, 0], 'bx-')
        pyplot.plot([center[1]], [center[0]], 'kx')

        # save and xclear
        destination = 'perimesaur/plot_{}_{}.png'.format(len(insiders) + len(outsiders), self._note())
        self._stamp('saving to {}'.format(destination))
        pyplot.savefig(destination)
        pyplot.clf()

        # stamp
        self._stamp('saved.')

        return None

    def segregate(self, latitudes, longitudes, latitudesii, longitudesii):
        """Segregate pairs of geolocation coordinates into those within or without a polygon.

        Arguments:
            latitudes: list of float, latitude coordinates of sample points in question
            longitudes: list of float, longitude coordinates of sample points in question
            latitudesii: list of float, latitude coordinates of enclosing polygon
            longitudesii: list of floats, longitude coordinates of enclosing polygon.

        Returns:
            ( numpy array, numpy array ) tuple, the coordinates of the points within and without the polygon

        Notes:
            Assumes latitudesii and longitudesii are given in order around the perimeter,
            and do not include a redundant final point.

            Assumes all angles between points are <= 180 degrees
        """

        # start clock
        self._stamp('segregaring {} points...'.format(len(latitudes)), initial=True)

        # create samples and perimeter as numpy arrays
        self._stamp('calculating polygon...')
        samples = numpy.array([latitudes, longitudes]).transpose(1, 0)
        perimeter = numpy.array([latitudesii, longitudesii]).transpose(1, 0)

        # duplicate first entry at last position as well
        polygon = numpy.concatenate([perimeter, [perimeter[0]]])

        # create segments from consecutive pairs of points
        segments = [polygon[index: index + 2, :] for index, _ in enumerate(polygon[:-1])]

        # calculate slopes of all segments as delta latitude / delta longitude
        slopes = [(segment[1, 0] - segment[0, 0]) / (segment[1, 1] - segment[0, 1]) for segment in segments]

        # calculate intercepts from first point: b = y0 - m(x0) = lat0 - m(lon0)
        intercepts = [(segment[0, 0] - slope * segment[0, 1]) for segment, slope in zip(segments, slopes)]

        # calculate center point
        center = (latitudesii.mean(), longitudesii.mean())

        # determine polarities (True = up ) for each segment by comparing the midpoint to the center
        zipper = zip(segments, slopes, intercepts)
        polarities = [center[0] > slope * center[1] + intercept for segment, slope, intercept in zipper]

        # create boolean masks for each segment
        self._stamp('making masks for each side...')
        masks = []
        for segment, slope, intercept, polarity in zip(segments, slopes, intercepts, polarities):

            # if polarity is True
            if polarity:

                # create boolean mask for all points where lat >= m * lon + b
                mask = samples[:, 0] > slope * samples[:, 1] + intercept
                masks.append(mask)

            # otherwise
            else:

                # create boolean mask for all points where  lat <= m * lon + b
                mask = samples[:, 0] < slope * samples[:, 1] + intercept
                masks.append(mask)

        # multiply all masks for polygon mask and make inverse
        masque = numpy.prod(masks, axis=0).astype(bool)
        inverse = numpy.logical_not(masque)

        # split into insiders and outsiders
        self._stamp('filtering data...')
        insiders = samples[masque]
        outsiders = samples[inverse, :]

        # timestamp
        self._print('{} insiders, {} outsiders'.format(len(insiders), len(outsiders)))
        self._stamp('segregated.')

        return insiders, outsiders, center, polygon

    def verify(self, trials=100000, sides=3, plot=False):
        """Test a number of random points in a random polygon.

        Arguments:
            trials: int, number of random sample points
            sides: int, number of sides to the enclosing polygon
            plot: boolean, make plot?

        Returns:
            None
        """

        # create a random sample set
        samples = numpy.random.random((trials, 2))
        latitudes = samples[:, 0]
        longitudes = samples[:, 1]

        # crete random perimeter
        latitudesii, longitudesii = self.generate(sides)

        # perform segregation
        insiders, outsiders, center, perimeter = self.segregate(latitudes, longitudes, latitudesii, longitudesii)

        # if plotting
        if plot:

            # perform plot
            self.plot(insiders, outsiders, center, perimeter)

        return None




# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 4
    arguments = arguments[:4]

    # umpack arguments
    year, month, start, finish = arguments

    # create perimesaur instance
    perimesaur = Perimesaur()
    perimesaur.peer(year, month, start, finish)

