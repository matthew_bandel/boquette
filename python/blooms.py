# blooms.py to represent dataset features in bokeh

# import reload
from importlib import reload

# import numpy
import numpy

# import features
from features import Feature


# class Feature to represent data features for boquette
class Bloom(Feature):
    """Class bloom to store bloom attributes.

    Inherits from:
        None
    """

    def __init__(self, feature, address, function=None, subset=None, abscissas=None):
        """Initialize a bloom instance from a feature instance.

        Arguments:
            feature: feature instance
            address: list of str, full addreess
            function: function object on numpy array
        """

        # call super
        parameters = {'route': feature.route, 'data': feature.data, 'format': feature.type, 'path': feature.path}
        parameters.update({'format': feature.type, 'link': feature.link, 'attributes': feature.attributes})
        parameters.update({'tags': feature.tags})
        Feature.__init__(self, **parameters)

        # set attributes
        self.bunch = address[:-1]
        self.stem = address[-2]
        self.petal = address[-1]
        self.sprig = '/'.join(address)

        # set default function
        self.function = function
        self.subset = subset
        self._functionalize()

        # set default designation as address
        self.designation = address

        # set default abscissa
        self.abscissas = abscissas or ['time']

        # set units
        self.units = ''
        self._unify()

        # set void value as fill value
        self.void = -999
        self._avoid()

        # set description
        self.description = '_'
        self._describe()

        # default index to zero
        self.index = 0

        return

    def __repr__(self):
        """Represent the feature on screen.

        Arguments:
            None

        Returns:
            str
        """

        # make a string of the feature's route
        representation = '< Bloom: {} {} {} {} >'.format(self.stem, self.petal, self.shape, self.path)

        return representation

    def _avoid(self):
        """Set void value to fill value if available.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.void
        """

        # check for attributes
        for field in ('_FillValue', 'FillValue'):

            # check in attributes
            if field in self.attributes:

                # set void
                self.void = float(self.attributes[field])

        return None

    def _describe(self):
        """Set long name for the feature.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.description
        """

        # default to '_'
        description = '_'

        # check for attriubutes
        for field in ('long_name', 'Description', 'description'):

            # check in attributes
            if field in self.attributes.keys():

                # set units
                description = self.attributes[field]

        # try to
        try:

            # decode units in case of byte string
            description = description.decode('utf8')

        # otherwise
        except AttributeError:

            # nevermind
            description = str(description)

        # set units
        self.description = description

        return None

    def _functionalize(self):
        """Instate a null function in case none was given.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.function
        """

        # if no function was given
        if not self.function:

            # set to null function
            self.function = lambda tensor: tensor

        # if no subset is given
        if not self.subset:

            # set to empty list
            self.subset = []

        return None

    def _unify(self):
        """Set units for the feature.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.units
        """

        # default to '_'
        units = ' '

        # check for a temperature field
        if 'temp' in self.stem.lower():

            # set units to K
            units = 'deg K'
        #
        # # check for a fraction field
        # if 'frac' in self.stem.lower():
        #
        #     # set units to pixel fraction
        #     units = 'pixel fraction'

        # check for attriubutes
        for field in ('Units', 'units'):

            # check in attributes
            if field in self.attributes.keys():

                # set units
                units = self.attributes[field]

        # try to
        try:

            # decode units incase of byte string
            units = units.decode('utf8')

        # otherwise
        except AttributeError:

            # nevermind
            units = str(units)

        # set units
        self.units = units

        return None

    def clone(self):
        """Produce a copy.

        Arguments:
            None

        Returns:
            None
        """

        # reconstruct feature
        parameters = {'route': self.route, 'data': self.data, 'format': self.type, 'path': self.path}
        parameters.update({'format': self.type, 'link': self.link, 'attributes': self.attributes})
        parameters.update({'tags': self.tags})
        feature = Feature(**parameters)

        # get other attributes
        address = [entry for entry in self.designation] + [self.petal]
        function = self.function
        subset = self.subset
        abscissas = self.abscissas

        # create copy
        xerox = Bloom(feature, address, function, subset, abscissas)

        return xerox

    def plug(self):
        """Plug up nan and infs with the void value.

        Arguments:
            None

        Returns:
            None
        """

        # if data has been filled
        if self.data is not None:

            # convert nans and repopulate
            data = numpy.nan_to_num(self.data, posinf=self.void, neginf=self.void, nan=self.void)
            self.data = data

        return None