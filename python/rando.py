# import
import random
from matplotlib import pyplot

# construct random data
xs = list(range(100))
ys = [random.random() for x in xs]

# clear previous plot
pyplot.clf()

# plot data as a blue line and red circles
pyplot.plot(xs, ys, 'b-')
pyplot.plot(xs, ys, 'ro')

# add title and labels
pyplot.title('random data')
pyplot.xlabel('x-axis')
pyplot.ylabel('y-axis')

# save the plot as a png
pyplot.savefig('random_plot.png')