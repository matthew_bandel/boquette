# hemmingways.py to organize description files

# import cores
from cores import Core

# import collections
from collections import Counter


# create Hemmingway instances
class Hemmingway(Core):
    """Class hemmingway to organize description files.

    Inherits from:
        Core
    """

    def __init__(self):
        """Initialize hemmingway instance.

        Arguments:
            None
        """

        return

    def contemporize(self):
        """Retain only the most recent version of app description.

        Arguments:
            None

        Returns:
            None
        """

        # get all description files
        descriptions = self._see('../bibliotek/descriptions')

        # sort by esdt
        earths = self._group(descriptions, lambda path: path.split('_')[-1].split('-')[0])
        for earth, members in earths.items():

            # get tags
            tags = [path.split('-')[-1].split('.txt')[0] for path in members]

            # split at periods and only keep digits up to 4
            tags = [[digit for digit in tag.split('.') if digit.isdigit()] for tag in tags]

            # expand all digits to 2 places and create as decimal
            def formatting(tag): return [str(digit).zfill(3) for digit in tag] + ['00'] * 4
            decimals = [float('0.{}{}{}{}'.format(*formatting(tag)[:4])) for tag in tags]

            # zip together with paths and sort
            zipper = list(zip(members, decimals))
            zipper.sort(key=lambda pair: pair[1], reverse=True)

            # move all but first to deletions
            deletions = [pair[0] for pair in zipper[1:]]
            [self._move(deletion, '../bibliotek/deletions') for deletion in deletions]

        return None

    def hunt(self, quarry):
        """Find all description files with the particular query term.

        Arguments:
            quarry: str, the query term

        Returns:
            list of str
        """

        # get all description files
        descriptions = self._see('../bibliotek/descriptions')

        # for each description
        trophies = []
        for description in descriptions:

            # get the transcription
            knowledge = self._know(description)

            # if the query exists somewhere
            if any([quarry in line for line in knowledge]):

                # add to trophies
                trophies.append(description)

        # also print the trophies
        trophies.sort()
        self._tell(trophies)

        return trophies

    def narrate(self):
        """Compile a list of all production rules and runtime parameters used.

        Arguments:
            None

        Returns:
            None
        """

        # get all description files
        descriptions = self._see('../bibliotek/descriptions')

        # load up all yamls
        yams = []
        for description in descriptions:

            # try to
            try:

                # acquire yaml
                yam = self._acquire(description)
                yams.append(yam)

            # unless problems
            except ValueError:

                # in which case pass
                self._print('problems with yam: {}'.format(description))

        # for each yaml file
        rules = []
        parameters = []
        for yam in yams:

            # for each production rule
            for rule in yam['Production Rules']:

                # append to the list
                rules.append(str(rule['Rule']))

            # for each runtime parameter
            for parameter in yam['Runtime Parameters']:

                # append to the list
                parameters.append(str(parameter['Param']))

        # count all production rules and sort
        counter = list(Counter(rules).items())
        counter.sort(key=lambda pair: pair[0])

        # create file
        lines = ['Production Rules', '']
        [lines.append('{} {}'.format(*pair)) for pair in counter]
        self._jot(lines, 'outputs/rules.txt')

        # count all runtime parameters and sort
        counter = list(Counter(parameters).items())
        counter.sort(key=lambda pair: pair[0])

        # create file
        lines = ['Runtime Parameters', '']
        [lines.append('{} {}'.format(*pair)) for pair in counter]
        self._jot(lines, 'outputs/parameters.txt')

        return None