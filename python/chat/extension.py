import matplotlib.pyplot as plt
import numpy as np

# Create some example data
data = np.random.rand(10, 10) * 2  # Example data with values > 1

# Create a colormap
cmap = plt.get_cmap('viridis')  # You can choose any colormap

# Create a figure and plot the data with the colormap
fig, ax = plt.subplots()
im = ax.imshow(data, cmap=cmap)

# Create a colorbar and set the 'extend' parameter to both 'both'
cbar = plt.colorbar(im, extend='both')

# Customize the colorbar labels and ticks
cbar.set_label('Data Values')
cbar.set_ticks([0, 1, 2])  # Customize the tick positions as needed

plt.show()