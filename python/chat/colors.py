import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np

# Get the original "rainbow_gist_r" colormap
original_cmap = plt.get_cmap('gist_rainbow_r')

# Define the range of colors you want to use (excluding the first two colors)
start_index = 32
end_index = original_cmap.N

# Create a custom colormap by slicing the original colormap
selection = np.linspace(start_index, end_index, end_index - start_index).astype(int)
selection = [index for index in selection if index < 144 or index > 160]
custom_cmap = ListedColormap(original_cmap(selection))

# Example usage: create a plot with the custom colormap
import numpy as np
data = np.random.rand(10, 10)
plt.imshow(data, cmap=custom_cmap)
plt.colorbar()
plt.show()