#!/usr/bin/env python3

# gorgons.py for the Gorgon class to analyze GEOS-IT data

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys
import os

# import subprocess
import subprocess

# import collections
from collections import Counter

# import regex
import re

# import itertools
import itertools

# import numpy functions
import numpy
import math

# import random forest
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier

# import datetime
import datetime

# import PIL for images
import PIL

# import requests
import requests

# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    #matplotlib.use('Tkagg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Gorgon to do OMI data reduction
class Gorgon(Hydra):
    """Gorgon class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # add squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Gorgon instance at {} >'.format(self.sink)

        return representation

    def _compress(self, corners, trackwise, rowwise):
        """Compress the latitude or longitude bounds based on compression factors.

        Arguments:
            corners: dict of corner arrays
            trackwise: north-south compression factor
            rowwise: east-west compression factor

        Returns:
            numpy array
        """

        # set directions
        compasses = ['southwest', 'southeast', 'northeast', 'northwest']

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = corners[compass][:, :, 1]
        #     corners[compass][:, :, 1] = numpy.where(corner < 0, corner + 360, corner)

        # get shape
        shape = corners['southwest'].shape

        # get selected trackwise indices and rowwise indices
        selection = numpy.array([index for index in range(shape[0]) if index % trackwise == 0])
        selectionii = numpy.array([index for index in range(shape[1]) if index % rowwise == 0])

        # adjust based on factors
        adjustment = numpy.array([min([index + trackwise - 1, shape[0] - 1]) for index in selection])
        adjustmentii = numpy.array([min([index + rowwise - 1, shape[1] - 1]) for index in selectionii])

        # begin compression
        compression = {}

        # adjust southwest
        compression['southwest'] = corners['southwest'][selection]
        compression['southwest'] = compression['southwest'][:, selectionii]
        compression['southeast'] = corners['southeast'][selection]
        compression['southeast'] = compression['southeast'][:, adjustmentii]
        compression['northeast'] = corners['northeast'][adjustment]
        compression['northeast'] = compression['northeast'][:, adjustmentii]
        compression['northwest'] = corners['northwest'][adjustment]
        compression['northwest'] = compression['northwest'][:, selectionii]

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = compression[compass][:, :, 1]
        #     compression[compass][:, :, 1] = numpy.where(corner >= 180, corner - 360, corner)

        return compression

    def _cross(self, bounds):
        """Adjust for longitude bounds that cross the dateline.

        Arguments:
            bounds: numpy array of bounds

        Returns:
            numpy array
        """

        # for each image
        for image in range(bounds.shape[0]):

            # and each row
            for row in range(bounds.shape[1]):

                # get the bounds
                polygon = bounds[image][row]

                # check for discrepancy
                if any([(entry < -170) for entry in polygon]) and any([entry > 170 for entry in polygon]):

                    # adjust bounds
                    bounds[image][row] = numpy.where(polygon > 170, polygon, polygon + 360)

        return bounds

    def _frame(self, latitude, longitude):
        """Construct the corners of polygons from latitude and longitude coordinates.

        Arguments:
            latitude: numpy array
            longitude: numpy array

        Returns:
            dict of numpy arrays, the corner points
        """

        # get main shape
        shape = latitude.shape

        # # adjust longitude for negative values
        # longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # initialize all four corners, with one layer for latitude and one for longitude
        northwest = numpy.zeros((*shape, 2))
        northeast = numpy.zeros((*shape, 2))
        southwest = numpy.zeros((*shape, 2))
        southeast = numpy.zeros((*shape, 2))

        # create latitude frame, with one more row and image on each side
        frame = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frame[1: shape[0] + 1, 1: shape[1] + 1] = latitude

        # extend sides of frame by extrapolation
        frame[1: -1, 0] = 2 * latitude[:, 0] - latitude[:, 1]
        frame[1: -1, -1] = 2 * latitude[:, -1] - latitude[:, -2]
        frame[0, 1:-1] = 2 * latitude[0, :] - latitude[1, :]
        frame[-1, 1:-1] = 2 * latitude[-1, :] - latitude[-2, :]

        # extend corners
        frame[0, 0] = 2 * frame[1, 1] - frame[2, 2]
        frame[0, -1] = 2 * frame[1, -2] - frame[2, -3]
        frame[-1, 0] = 2 * frame[-2, 1] - frame[-3, 2]
        frame[-1, -1] = 2 * frame[-2, -2] - frame[-3, -3]

        # create longitude frame, with one more row and image on each side
        frameii = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frameii[1: shape[0] + 1, 1: shape[1] + 1] = longitude

        # extend sides of frame by extrapolation
        frameii[1: -1, 0] = 2 * longitude[:, 0] - longitude[:, 1]
        frameii[1: -1, -1] = 2 * longitude[:, -1] - longitude[:, -2]
        frameii[0, 1:-1] = 2 * longitude[0, :] - longitude[1, :]
        frameii[-1, 1:-1] = 2 * longitude[-1, :] - longitude[-2, :]

        # extend corners
        frameii[0, 0] = 2 * frameii[1, 1] - frameii[2, 2]
        frameii[0, -1] = 2 * frameii[1, -2] - frameii[2, -3]
        frameii[-1, 0] = 2 * frameii[-2, 1] - frameii[-3, 2]
        frameii[-1, -1] = 2 * frameii[-2, -2] - frameii[-3, -3]

        # populate interior polygon corners image by image
        for image in range(0, shape[0]):

            # and row by row
            for row in range(0, shape[1]):

                # frame indices are off by 1
                imageii = image + 1
                rowii = row + 1

                # by averaging latitude frame at diagonals
                northwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii + 1]) / 2

                # and by averaging longitude longitudes at diagonals
                northwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii + 1]) / 2

        # fix longitude edge cases, by looking for crisscrossing, and truncating
        northwest[:, :, 1] = numpy.where(northwest[:, :, 1] < longitude, northwest[:, :, 1], longitude)
        northeast[:, :, 1] = numpy.where(northeast[:, :, 1] > longitude, northeast[:, :, 1], longitude)
        southwest[:, :, 1] = numpy.where(southwest[:, :, 1] < longitude, southwest[:, :, 1], longitude)
        southeast[:, :, 1] = numpy.where(southeast[:, :, 1] > longitude, southeast[:, :, 1], longitude)

        # average all overlapping corners
        overlap = southeast[1:, :-1, :] + southwest[1:, 1:, :] + northwest[:-1, 1:, :] + northeast[:-1, :-1, :]
        average = overlap / 4

        # transfer average
        southeast[1:, :-1, :] = average
        southwest[1:, 1:, :] = average
        northwest[:-1, 1:, :] = average
        northeast[:-1, :-1, :] = average

        # average western edge
        average = (southwest[1:, 0, :] + northwest[:-1, 0, :]) / 2
        southwest[1:, 0, :] = average
        northwest[:-1, 0, :] = average

        # average eastern edge
        average = (southeast[1:, -1, :] + northeast[:-1, -1, :]) / 2
        southeast[1:, -1, :] = average
        northeast[:-1, -1, :] = average

        # average northern edge
        average = (northwest[-1, 1:, :] + northeast[-1, :-1, :]) / 2
        northwest[-1, 1:, :] = average
        northeast[-1, :-1, :] = average

        # average southern edge
        average = (southwest[0, 1:, :] + southeast[0, :-1, :]) / 2
        southwest[0, 1:, :] = average
        southeast[0, :-1, :] = average

        # # subtact 360 from any longitudes over 180
        # northwest[:, :, 1] = numpy.where(northwest[:, :, 1] >= 180, northwest[:, :, 1] - 360, northwest[:, :, 1])
        # northeast[:, :, 1] = numpy.where(northeast[:, :, 1] >= 180, northeast[:, :, 1] - 360, northeast[:, :, 1])
        # southeast[:, :, 1] = numpy.where(southeast[:, :, 1] >= 180, southeast[:, :, 1] - 360, southeast[:, :, 1])
        # southwest[:, :, 1] = numpy.where(southwest[:, :, 1] >= 180, southwest[:, :, 1] - 360, southwest[:, :, 1])

        # collect corners
        corners = {'northwest': northwest, 'northeast': northeast}
        corners.update({'southwest': southwest, 'southeast': southeast})

        return corners

    def _imbibe(self, bar):
        """Convert a colorbar image into colorbar colors.

        Arguments:
            bar: str, filename to colorbar

        Returns:
            None
        """

        # get colorbar image
        image = PIL.Image.open(bar)
        array = numpy.array(image)

        # get mid point
        half = int(array.shape[0] / 2)

        # get all colors
        colors = array[half, :, :3]

        # write to file
        text = [' '.join([str(channel / 255) for channel in line]) for line in colors]
        destination = bar.replace('.png', '.txt')
        self._jot(text, destination)

        return None

    def _orientate(self, latitude, longitude):
        """Create corners by orienting latitude bounds and longitude bounds.

        Arguments:
            latitude: numpy array, latitude bounds
            longitude: numpy array, longitude bounds

        Returns:
            dict of numpy arrays
        """

        # add 360 to all negative longitude bounnds
        longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # get all cardinal directions
        cardinals = {'south': latitude.min(axis=2), 'north': latitude.max(axis=2)}
        cardinals.update({'west': longitude.min(axis=2), 'east': longitude.max(axis=2)})

        # begin corners with zeros
        compasses = ('northwest', 'northeast', 'southeast', 'southwest')
        compassesii = ('northeast', 'southeast', 'southwest', 'northwest')
        corners = {compass: numpy.zeros((latitude.shape[0], latitude.shape[1], 2)) for compass in compasses}

        # double up bounds
        latitude = latitude.transpose(2, 1, 0)
        longitude = longitude.transpose(2, 1, 0)
        latitude = numpy.vstack([latitude, latitude, latitude]).transpose(2, 1, 0)
        longitude = numpy.vstack([longitude, longitude, longitude]).transpose(2, 1, 0)

        # go through each position
        for index in range(4):

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] < longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compasses):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] > longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compassesii):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

        # subtract 360 from longitudes
        for compass in compasses:

            # subtract form longitude
            corner = corners[compass][:, :, 1]
            corners[compass][:, :, 1] = numpy.where(corner > 180, corner - 360, corner)

        return corners

    def _paint(self, arrays, path, texts, scale, spectrum, limits, grid=1, **options):
        """Make matplotlib polygon figure.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            texts: list of str, title and units
            path: str, filepath to destination
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                north: boolean, use north projection?
                projection: name of projection

        Returns:
            None
        """

        # set defaults
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        bar = options.get('bar', True)
        log = options.get('log', False)
        north = options.get('north', True)
        extension = options.get('extension', 'both')

        # set projections
        native = cartopy.crs.PlateCarree()
        projection = options.get('projection', cartopy.crs.PlateCarree())

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set cartopy axis with coastlines
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=3)
        _ = axis.gridlines(draw_labels=True)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left_margin = 0.1
        right_margin = 0.1
        top_margin = 0.19
        bottom_margin = 0.18
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_xlim(limits[2], limits[3])
        axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=14)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]
                patch = numpy.array(patch)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # try
        try:

            # set up color scheme, removing first two
            colors = matplotlib.cm.get_cmap(gradient)
            selection = numpy.array(selection)

            # set up color map
            colors = matplotlib.colors.ListedColormap(colors(selection))

        # except
        except ValueError:

            # set up color scheme, removing first two
            text = self._know(gradient)
            tuples = [tuple([float(channel) for channel in line.split()]) for line in text]

            # set up color map
            colors = matplotlib.colors.ListedColormap(tuples)

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        collection = axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.08, position.width, 0.02])
            #colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend=extension)
            colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None

    def _polymerize(self, latitude, longitude, resolution):
        """Construct polygons from latitude and longitude bounds.

        Arguments:
            latitude: numpy array of latitude bounds
            longitude: numpy array of longitude bounds
            resolution: int, vertical compression factor

        Returns:
            numpy.array
        """

        # squeeze arrays
        latitude = latitude.squeeze()
        longitude = longitude.squeeze()

        # if bounds are given
        if len(latitude.shape) > 2:

            # orient the bounds
            corners = self._orientate(latitude, longitude)

        # otherwise
        else:

            # get corners from latitude and longitude points
            corners = self._frame(latitude, longitude)

        # compress corners vertically
        corners = self._compress(corners, resolution, 1)

        # construct bounds
        compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        arrays = [corners[compass][:, :, 0] for compass in compasses]
        bounds = numpy.stack(arrays, axis=2)
        arrays = [corners[compass][:, :, 1] for compass in compasses]
        boundsii = numpy.stack(arrays, axis=2)

        # adjust polygon  boundaries to avoid crossing dateline
        boundsii = self._cross(boundsii)

        # make polygons
        polygons = numpy.dstack([bounds, boundsii])

        return polygons

    def stare(self, year=2005, month=3, day=21, choices=None, margin=2, resolution=100, modes=None):
        """Compare new FPIT data with old.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            choices: list of str, the chosen fields
            margin: int, the percentile margin for plotting
            resolution: grid resolution
            modes: list of str, the modes to plot

        Returns:
            None
        """

        # create folder
        folder = '{}/stare'.format(self.sink)
        self._make(folder)

        # set archive sets
        archives = (10004, 70005)

        # construct date
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        address = '{}/{}/{}'.format(year, self._pad(month), self._pad(day))

        # set products
        products = ('OMUFPMET', 'OMVFPMET', 'OMUFPSLV', 'OMVFPSLV')

        # set fields and searches
        fields = ['DELP', 'PHIS', 'PL', 'PS', 'T']
        fieldsii = ['PBLTOP', 'PS', 'TROPPB', 'TROPPT', 'TROPPV', 'TS', 'U10M', 'V10M']
        searches = {product: [field for field in fields] for product in products[:2]}
        searches.update({product: [field for field in fieldsii] for product in products[2:]})

        # set units
        units = {'DELP': '$Pa$', 'PHIS': '$m^2/s^2$', 'PL': '$Pa$', 'PS': '$Pa$', 'T': '$K$'}
        units.update({'PBLTOP': '$Pa$', 'PS': '$Pa$', 'TROPPB': '$Pa$', 'TROPPT': '$Pa$', 'TROPPV': '$Pa$'})
        units.update({'TS': '$K$', 'U10M': '$m/s$', 'V10M': '$m/s$'})

        # set 3-d positions for three dimenionsal fields
        layers = [0, 35, 71]
        threes = {'DELP': layers, 'PL': layers, 'T': layers}

        # set scales and difference and percent scales
        ranges = {}
        rangesii = {}
        rangesiii = {}

        # for each product
        for product in products:

            # begin data reservoir
            data = {archives[0]: {}, archives[1]: {}}

            # for each archive set
            for archive in archives:

                # create hydra
                hydra = Hydra('/tis/acps/OMI/{}/{}/{}'.format(archive, product, address), show=False)

                # for each path
                for path in hydra.paths:

                    # ingest path
                    hydra.ingest(path)

                    # get latitude along mid row
                    latitude = hydra.grab('latitude').squeeze()[:, 30]

                    # find the first and last indices of ascending data
                    difference = numpy.diff(latitude)
                    mask = (difference > 0)
                    first = mask.tolist().index(True)
                    last = (mask[first:].tolist() + [False]).index(False) + first

                    # for each field
                    for field in searches[product] + ['latitude', 'longitude']:

                        # get the data, excluding descending data
                        array = hydra.grab(field).squeeze()[first: last]

                        # if two d
                        if field not in threes.keys():

                            # expand to third dimension
                            array = array.reshape(*array.shape, 1)

                        # append
                        data[archive].setdefault(field, []).append(array)

                # stack all arrays
                data[archive] = {field: numpy.vstack(arrays) for field, arrays in data[archive].items()}

            # begin differences and percents
            differences = {}
            percents = {}

            # for each field
            for field in searches[product]:

                # create difference and percent
                differences[field] = self._relate(data[archives[0]][field], data[archives[1]][field])
                percents[field] = self._relate(data[archives[0]][field], data[archives[1]][field], percent=True)

                # correct for nans
                percents[field] = numpy.where(numpy.isfinite(percents[field]), percents[field], 0)

            # for each field
            fields = choices or searches[product]
            fields = [field for field in fields if field in searches[product]]
            for field in fields:

                # get the latitude and longitudes
                latitude = data[archives[0]]['latitude']
                longitude = data[archives[0]]['longitude']

                # for each layer
                for layer in threes.get(field, [0]):

                    # default field name to field
                    name = field

                    # if 3-d field
                    if field in threes.keys():

                        # append layer
                        name = '{}({})'.format(field, layer)

                    # get the arrays
                    arrays = [data[archives[0]][field], data[archives[1]][field], differences[field], percents[field]]
                    arrays = [array[:, :, layer] for array in arrays]

                    # set the gradients
                    gradients = ['plasma', 'plasma', 'coolwarm', 'coolwarm']

                    # set stubs
                    stubs = [str(archives[0]), str(archives[1]), 'diff', 'percent']

                    # set title words
                    words = ['AS {}'.format(archives[0]), 'AS {}'.format(archives[1])]
                    words += ['AS {} - AS {}'.format(archives[1], archives[0])]
                    words += ['% AS {} / AS {}'.format(archives[1], archives[0])]

                    # set epsilon
                    epsilon = 1e-4

                    # begin scales
                    scales = []

                    # if scale is set
                    if name in ranges.keys():

                        # add scale
                        scale = ranges[name]
                        scales += [scale, scale]

                    # otherwise
                    else:

                        # construct scale from percentiles
                        stack = numpy.vstack(arrays[:2])
                        minimum = numpy.percentile(stack, margin)
                        maximum = numpy.percentile(stack, 100 - margin)

                        # if they are equal
                        if abs(maximum - minimum) < 2 * epsilon:

                            # differ by 5 percent
                            minimum = (minimum * 0.95)
                            maximum = (maximum * 1.05)

                        # set scale
                        scale = (minimum, maximum)
                        scales += [scale, scale]

                    # if difference scale is set
                    if name in rangesii.keys():

                        # add scale
                        scale = rangesii[name]
                        scales += [scale]

                    # otherwise
                    else:

                        # construct scale from percentiles
                        difference = arrays[2]
                        minimumii = numpy.percentile(difference, margin)
                        maximumii = numpy.percentile(difference, 100 - margin)
                        absolute = max([abs(minimumii), abs(maximumii)])

                        # if absolute is zero:
                        buffer = 0
                        if absolute < 2 * epsilon:

                            # adjust by difference between minimum and maximum
                            buffer = ((maximum - minimum) / 5)

                        # set scale
                        scale = (-absolute - buffer, absolute + buffer)
                        scales += [scale]

                    # if percent scale is set
                    if name in rangesiii.keys():

                        # add scale
                        scale = rangesiii[name]
                        scales += [scale]

                    # otherwise
                    else:

                        # construct scale from percentiles
                        percent = arrays[3]
                        minimumiii = numpy.percentile(percent, margin)
                        maximumiii = numpy.percentile(percent, 100 - margin)
                        absolute = max([abs(minimumiii), abs(maximumiii)])

                        # if absolute is zero:
                        buffer = 0
                        if absolute < 2 * epsilon:

                            # make a buffer equal to 0.01
                            buffer = 0.01

                        # set scale
                        scale = (-absolute - buffer, absolute + buffer)
                        scales += [scale]

                    # for array
                    modes = modes or stubs
                    for array, gradient, stub, word, scale in zip(arrays, gradients, stubs, words, scales):

                        # if selected modes:
                        if stub in modes:

                            # create title destination
                            title = '{}, {}, {}, {}'.format(date, product, name, word)
                            formatsii = (folder, date, product.lower(), field.lower(), self._pad(layer), stub.lower())
                            destination = '{}/GEOSIT_{}_{}_{}_{}_{}.png'.format(*formatsii)

                            # set parameters
                            limits = (-90, 90, -180, 180)
                            size = (10, 7)
                            logarithm = False
                            back = 'white'
                            unit = units[field]
                            selection = list(range(256))
                            bar = True

                            # get field attributes
                            attributes = hydra.dig(field)[0].attributes
                            description = attributes['long_name']
                            measure = units[field]
                            unit = '{} ( {} )'.format(description, measure)

                            # if percent
                            if stub == 'percent':

                                # units are percent
                                unit = '{} ( % diff )'.format(description)

                            # create mask and apply, clipping to scale
                            fill = 1e20
                            mask = (numpy.isfinite(array)) & (abs(array) < fill)
                            tracer = numpy.where(mask, array, numpy.nan)
                            tracer = tracer.astype('float64')
                            tracer = numpy.where((tracer > (scale[1] - epsilon)), scale[1] - epsilon, tracer)
                            tracer = numpy.where((tracer < (scale[0] + epsilon)), scale[0] + epsilon, tracer)

                            # make plot
                            self._stamp('plotting {}...'.format(destination), initial=True)
                            parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                            parameters += [scale, [gradient, selection], limits, resolution]
                            options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                            self._paint(*parameters, **options)

        return None

    def verify(self, year=2005, month=3, day=21, margin=2, resolution=100):
        """Verify that the new GEOSIT data differs from the old in terms of temperture.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            margin: int, the percentile margin for plotting
            resolution: grid resolution

        Returns:
            None
        """

        # create folder
        folder = '{}/verify'.format(self.sink)
        self._make(folder)

        # set archive sets
        archive = 70003

        # construct date
        date = '{}m{}{}'.format(year, self._pad(month), self._pad(day))
        address = '{}/{}/{}'.format(year, self._pad(month), self._pad(day))

        # set products
        products = ('DFPITT3NVASM', 'GEOSIT_ASM_T3_L_V72')

        # set fields and searches
        fields = ['T']
        searches = {product: fields for product in products[:2]}

        # set units
        units = {'DELP': '$Pa$', 'PHIS': '$m^2/s^2$', 'PL': '$Pa$', 'PS': '$Pa$', 'T': '$K$'}
        units.update({'PBLTOP': '$Pa$', 'PS': '$Pa$', 'TROPPB': '$Pa$', 'TROPPT': '$Pa$', 'TROPPV': '$Pa$'})
        units.update({'TS': '$K$', 'U10M': '$m/s$', 'V10M': '$m/s$'})

        # begin data reservoir
        data = {products[0]: {}, products[1]: {}}

        # set layer
        layer = 0

        # for each product
        for product in products:

            # create hydra
            hydra = Hydra('/tis/acps/OMI/{}/{}/{}'.format(archive, product, address), show=False)

            # ingest path
            hydra.ingest(0)

            # for each field
            for field in searches[product] + ['lat', 'lon']:

                # get the data, excluding descending data
                array = hydra.grab(field).squeeze()

                # append
                data[product][field] = array

        # create differences
        differences = {field: data[products[1]][field] - data[products[0]][field] for field in searches[product]}

        # for each field
        for field in fields:

            # get the latitude and longitudes
            latitude = data[products[0]]['lat']
            longitude = data[products[0]]['lon']

            # construct grids
            longitude, latitude = numpy.meshgrid(longitude, latitude, indexing='xy')

            # set layer
            name = '{}({})'.format(field, layer)

            # get the arrays
            arrays = [data[products[0]][field][layer], data[products[1]][field][layer], differences[field][layer]]

            # set the gradients
            gradients = ['plasma', 'plasma', 'coolwarm']

            # set stubs
            stubs = [str(products[0]), str(products[1]), 'diff']

            # set title words
            words = ['{}'.format(products[0]), '{}'.format(products[1])]
            words += ['{} - {}'.format(products[1], products[0])]

            # begin scales
            scales = []

            # construct scale from percentiles
            stack = numpy.vstack(arrays[:2])
            minimum = numpy.percentile(stack, margin)
            maximum = numpy.percentile(stack, 100 - margin)

            # if they are equal
            if minimum == maximum:

                # differ by 5 percent
                minimum = minimum * 0.95
                maximum = maximum * 1.05

            # set scale
            scale = (minimum, maximum)
            scales += [scale, scale]

            # construct scale from percentiles
            difference = arrays[2]
            minimumii = numpy.percentile(difference, margin)
            maximumii = numpy.percentile(difference, 100 - margin)
            absolute = max([abs(minimumii), abs(maximumii)])

            # if absolute is zero:
            buffer = 0
            if absolute == 0:

                # adjust by difference between minimum and maximum
                buffer = (maximum - minimum) / 5

            # set scale
            scale = (-absolute - buffer, absolute + buffer)
            scales += [scale]

            # for array
            for array, gradient, stub, word, scale in zip(arrays, gradients, stubs, words, scales):

                # create title destination
                title = '{}, {}, {}'.format(date, word, name)
                formatsii = (folder, date, field.lower(), self._pad(layer), stub.lower())
                destination = '{}/GEOSIT_{}_{}_{}_{}.png'.format(*formatsii)

                # set parameters
                limits = (-90, 90, -180, 180)
                size = (10, 7)
                logarithm = False
                back = 'white'
                unit = units[field]
                selection = list(range(256))
                bar = True

                # create mask and apply, clipping to scale
                fill = 1e20
                epsilon = 1e-4
                mask = (numpy.isfinite(array)) & (abs(array) < fill)
                tracer = numpy.where(mask, array, numpy.nan)
                tracer = tracer.astype('float64')
                tracer = numpy.where((tracer > (scale[1] - epsilon)), scale[1] - epsilon, tracer)
                tracer = numpy.where((tracer < (scale[0] + epsilon)), scale[0] + epsilon, tracer)

                # make plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer, latitude, longitude], destination, [title, unit]]
                parameters += [scale, [gradient, selection], limits, resolution]
                options = {'log': logarithm, 'bar': bar, 'size': size, 'back': back, 'extension': 'both'}
                self._paint(*parameters, **options)
                self._stamp('plotted {}'.format(destination))

        return None