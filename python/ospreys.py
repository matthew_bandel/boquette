#!/usr/bin/env python3

# ospreys.py for the Osprey class to build omi climatology

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import regex
import re

# import itertools
import itertools

# import numpy functions
import numpy
import math
from scipy import stats

# import datetime
import datetime

# import sklearn
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree
from sklearn.ensemble import RandomForestRegressor

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# try to
try:

    # import land sea mask
    import global_land_mask

# unless not found ( ubuntu? )
except ImportError:

    # nevermind
    print('global land mask not available')


# class Millipede to do OMI data reduction
class Osprey(Hydra):
    """Osprey class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # add squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Osprey instance qt {} >'.format(self.sink)

        return representation

    def _align(self, times, timesii, latitudes):
        """Determine best alignment amonst two files' orbits, using only ascending data

        Arguments:
            times: numpy array of times, omcldrr
            timesii: numpy array of times, omto3

        Returns:
            tuple of ints, the indices
        """

        # determine latitude bounds
        south = latitudes.min(axis=1).argsort()[0]
        north = latitudes.max(axis=1).argsort()[-1]

        # find the differences between the southern most time
        differences = (timesii - times[south]) ** 2

        # get starting indices
        start = south
        startii = numpy.argsort(differences)[0]

        # get minimum lengths
        length = min([timesii.shape[0] - startii, times.shape[0] - (south + north)])

        # get stopping indices
        stop = start + length
        stopii = startii + length

        return start, stop, startii, stopii

    def _bite(self, tensor, bit, masquerade):
        """Combine boolean masks for a particular bit.

        Arguments:
            tensor: numpy.array
            bit: int
            masquerade: dict of boolean masks

        Returns:
            numpy array
        """

        # convert to int
        bit = int(bit)

        # decompose all uniques
        masks = []
        for unique, mask in masquerade.items():

            # decompose into binary form
            decomposition = bin(unique).replace('0b', '0' * 20)
            decomposition = decomposition[::-1]
            if decomposition[bit] == '1':

                # add to codes
                masks.append(mask)

        # add empty mask
        empty = numpy.zeros(tensor.shape)

        # summ together for new mask
        masque = empty
        for entry in masks:

            # add to the running sum
            masque += entry

        return masque

    def _crystalize(self, data, latitudes, longitudes, segment=20):
        """Bin the geographic data by percentile bins.

        Arguments:
            data: numpy array of measurement data
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes
            segment=20: size of bin

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # begin geodes
        geodes = []

        # for each bracket
        brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
        for first, second in brackets:

            self._print(first, second)

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            datum = data[mask].flatten()
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            # concatenate
            geode = numpy.vstack([datum, latitude, longitude])

            # order along latitude, then longitude
            geode = geode[:, numpy.argsort(latitude)]
            geode = geode[:, numpy.argsort(longitude)]

            # append
            geodes.append(geode)

        return geodes, brackets

    def _exchange(self, path, destination, codex):
        """Swap out yaml attributes:

        Arguments:
            path: str, file path to source yaml
            destination: str, file path to destination yaml
            codex: dict, the update dictionary

        Returns:
            None
        """

        # open up yaml
        yam = self._acquire(path)

        # make updates
        yam.update(codex)

        # redispence
        self._dispense(yam, destination)

        return None

    def _exclude(self, quality, flags):
        """Create boolean mask for excluding quality flagged data.

        Arguments:
            quality: numpy array
            flags: list of ints, the exclusion flags

        Returns:
            boolean mask, absence of excluded flags
        """

        # get the masquerade of unique value masks
        masquerade = self._masquerade(quality)

        # begin list off masks
        masks = []

        # for each flag
        for flag in flags:

            # get the boolean mask for that bit
            mask = self._bite(quality, flag, masquerade)
            masks.append(mask)

        # sum masks, and take innverse
        masque = numpy.array(masks).sum(axis=0).astype(int)
        inverse = (masque == 0)

        return inverse

    def _masquerade(self, tensor):
        """Create boolean feature masks for each unique feature in the tensor.

        Arguments:
            tensor: numpy array

        Returns:
            dict
        """

        # get all unique values, using a method faster than numpy.unique
        integers = numpy.array(tensor).astype(int)
        zeros = numpy.zeros(numpy.max(integers) + 1, dtype=int)
        zeros[integers.ravel()] = 1
        uniques = numpy.nonzero(zeros)[0]

        # create boolean masks and convert to ints
        masks = {unique: tensor == unique for unique in uniques}

        return masks

    def _segregate(self, latitudes, longitudes, latitudesii, longitudesii):
        """Segregate pairs of geolocation coordinates into those within or without a polygon.

        Arguments:
            latitudes: list of float, latitude coordinates of sample points in question
            longitudes: list of float, longitude coordinates of sample points in question
            latitudesii: list of float, latitude coordinates of enclosing polygon
            longitudesii: list of floats, longitude coordinates of enclosing polygon.

        Returns:
            ( numpy array, numpy array ) tuple, the coordinates of the points within and without the polygon

        Notes:
            Assumes latitudesii and longitudesii are given in order around the perimeter,
            and do not include a redundant final point.

            Assumes all angles between points are <= 180 degrees
        """

        # start clock
        #self._stamp('segregaring {} points...'.format(len(latitudes)), initial=True)

        # create samples and perimeter as numpy arrays
        #self._stamp('calculating polygon...')
        samples = numpy.array([latitudes, longitudes]).transpose(1, 0)
        perimeter = numpy.array([latitudesii, longitudesii]).transpose(1, 0)

        # duplicate first entry at last position as well
        polygon = numpy.concatenate([perimeter, [perimeter[0]]])

        # create segments from consecutive pairs of points
        segments = [polygon[index: index + 2, :] for index, _ in enumerate(polygon[:-1])]

        # calculate slopes of all segments as delta latitude / delta longitude
        slopes = [(segment[1, 0] - segment[0, 0]) / (segment[1, 1] - segment[0, 1]) for segment in segments]

        # calculate intercepts from first point: b = y0 - m(x0) = lat0 - m(lon0)
        intercepts = [(segment[0, 0] - slope * segment[0, 1]) for segment, slope in zip(segments, slopes)]

        # calculate center point
        center = (latitudesii.mean(), longitudesii.mean())

        # determine polarities (True = up ) for each segment by comparing the midpoint to the center
        zipper = zip(segments, slopes, intercepts)
        polarities = [center[0] > slope * center[1] + intercept for segment, slope, intercept in zipper]

        # create boolean masks for each segment
        #self._stamp('making masks for each side...')
        masks = []
        for segment, slope, intercept, polarity in zip(segments, slopes, intercepts, polarities):

            # if polarity is True
            if polarity:

                # create boolean mask for all points where lat >= m * lon + b
                mask = samples[:, 0] > slope * samples[:, 1] + intercept
                masks.append(mask)

            # otherwise
            else:

                # create boolean mask for all points where  lat <= m * lon + b
                mask = samples[:, 0] < slope * samples[:, 1] + intercept
                masks.append(mask)

        # multiply all masks for polygon mask and make inverse
        masque = numpy.prod(masks, axis=0).astype(bool)
        inverse = numpy.logical_not(masque)

        # split into insiders and outsiders
        #self._stamp('filtering data...')
        insiders = samples[masque]
        outsiders = samples[inverse, :]

        # timestamp
        #self._print('{} insiders, {} outsiders'.format(len(insiders), len(outsiders)))
        #self._stamp('segregated.')

        return insiders, outsiders, center, polygon

    def _smooth(self, pressure, number, land, sea, degrees=[3]):
        """Smooth data using 3 x 3 grid and land, sea masks.

        Arguments:
            pressure: numpy array
            number: numpy array
            land: numpy array
            sea: numpy array
            degrees: list of ints, the degrees for progressive smoothing

        Returns:
            numpy array
        """

        # create 30 degree brackets and default value to 0
        brackets = [(0, 30), (30, 60), (60, 90), (90, 120), (120, 150), (150, 180)]
        lands = {north: 0.0 for north, _ in brackets}
        seas = {north: 0.0 for north, _ in brackets}

        # create weights matrix to weigh the average based on sample count
        weights = numpy.where(number < 10, number, 10)

        # compute the weighted average for inner brackets
        for north, south in brackets[1:-1]:

            # make all weights at least 1 for inner brackets
            weights[north: south, :] = numpy.where(number[north: south, :] > 1, weights[north: south, :], 1)

            # create land average
            band = pressure[north: south, :]
            weight = weights[north: south, :]
            mask = land[north: south, :] * (band > 0)
            average = (band[mask] * weight[mask]).sum() / weight[mask].sum()
            lands[north] = average

            # create sea average
            band = pressure[north: south, :]
            mask = sea[north: south, :] * (band > 0)
            average = (band[mask] * weight[mask]).sum() / weight[mask].sum()
            seas[north] = average

        # begin grid
        grid = numpy.zeros((180, 360))

        # for each degree
        smoothers = []
        for degree in degrees:

            # determine steps
            half = math.floor(degree / 2)
            steps = list(range(-half, half + 1))

            # begin shifts
            shifts = []
            for step in steps:

                # for each additional step
                for stepii in steps:

                    # add to shifts
                    shift = (step, 0, stepii, 1)
                    shifts.append(shift)

            # append shifts to smoothers
            smoothers.append(shifts)

        # for each mask
        for filter, averages in zip((land, sea), (lands, seas)):

            # set smear
            smear = pressure.copy()

            # set measure to weigthts
            measures = weights.copy()

            # go through each bracket
            for north, south in brackets:

                # get masque
                masque = filter[north: south, :]

                # fill missing values with avearge
                band = smear[north: south, :]
                band = numpy.where(band > 0, band, averages[north])
                band = numpy.where(masque, band, 0)

                # fill in for measures as, making weight 0 if not in filter
                measures[north: south] = numpy.where(masque, measures[north: south], 0)

                # add band to grid
                smear[north: south, :] = band

            # for each smoother
            for shifts in smoothers:

                # copy filter to get samples
                quantities = smear.copy() * measures.copy()
                samples = measures.copy()

                # begin totals
                totals = []
                counts = []

                # for each shift
                for shift, axis, shiftii, axisii in shifts:

                    # roll the quantities
                    quantity = numpy.roll(quantities.copy(), shift, axis=axis)
                    quantity = numpy.roll(quantity, shiftii, axis=axisii)
                    totals.append(quantity)

                    # roll the sample counts
                    sample = numpy.roll(samples.copy(), shift, axis=axis)
                    sample = numpy.roll(sample, shiftii, axis=axisii)
                    counts.append(sample)

                # sum together totals and counts
                totals = numpy.array(totals).sum(axis=0)
                counts = numpy.array(counts).sum(axis=0)

                # compute the smeared pressure
                smear = totals / counts
                smear = numpy.where(numpy.isfinite(smear), smear, 0)

            # go through each bracket
            for north, south in brackets:

                # get masque
                masque = filter[north: south, :]

                # make latitude band, using fill
                band = smear[north: south, :]

                # band = numpy.where(band > 0, band, averages[north])
                band = numpy.where(masque, band, 0)

                # add band to grid
                grid[north: south, :] = grid[north: south, :] + band

        return grid

    def _understand(self, records, reflectivity, extent, bins=10):
        """Construct the histograms.

        Arguments:
            records: list of list of dicts, the records
            reflectivity: str, reflectivity level
            extent: int, number of records to average
            bins: int, number of bins

        Returns:
            None
        """

        # collect all delta pressures
        pressures = []
        for record in records:

            # average appropriate entries
            pressure = numpy.average([entry['deltas']['pressure ( hPa )'] for entry in record[:extent]])
            pressures.append(pressure)

        # determine 95th and 5th percentile
        lower = numpy.percentile(numpy.array(pressures), 5)
        upper = numpy.percentile(numpy.array(pressures), 95)

        # break into equal pieces of a certain width
        width = (upper - lower) / bins

        # define brackets
        brackets = [(lower + width * index, lower + width * (index + 1)) for index in range(bins)]
        brackets = [(lower - width, lower)] + brackets + [(upper, upper + width)]

        # create middle of each bin
        middles = list([sum(bracket) / 2 for bracket in brackets])

        # get counts for heights
        bounds = brackets
        bounds[0] = (min(pressures), brackets[0][1])
        bounds[1] = (brackets[-1][0], max(pressures))
        boxes = [[pressure for pressure in pressures if bracket[0] <= pressure <= bracket[1]] for bracket in bounds]
        counts = list([len(box) for box in boxes])

        # make labels
        ordinate = 'concurrent pairs'
        abscissa = 'delta Pressure (hPa) NMCLDRR - OMCLDRR'

        # construct title attributes
        pairs = len(records)
        dates = [record[0]['omi']['date'] for record in records]
        dates.sort()
        start = dates[0][:7]
        finish = dates[-1][:7]

        # create bracket descriptions
        descriptions = {'low': 'refl. < 0.3', 'middle': '0.3 < refl. < 0.7', 'high': '0.7 < refl.', 'all': 'all refl.'}

        # make title
        title = 'Cloud Pressure Difference, NMCLDRR - OMCLDRR, {}'.format(descriptions[reflectivity])
        title += '\n ( {} pairs from {} to {}, < 30 sec, 1 km apart, avg of {} )'.format(pairs, start, finish, extent)

        # dump into json
        plot = {'width': width, 'middles': middles, 'counts': counts, 'ordinate': ordinate, 'abscissa': abscissa}
        plot.update({'title': title})
        self._dump(plot, '../studies/osprey/plots/histogram_{}_{}.json'.format(extent, reflectivity))

        # add file for histograms
        route = ['Categories', 'delta_pressure_histogram']
        array = numpy.array(pressures)
        feature = Feature(route, array)
        destination = '../studies/osprey/histograms/histogram_{}_{}.h5'.format(extent, reflectivity)
        self.stash([feature], destination, 'Data')

        return None

    def accumulate(self, year, month, start, stop, sink='months'):
        """Accumulate cloud data.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))

        # create destination of file
        destination = '{}/{}/Zonal_Sums_OMTO3_{}_{}.h5'.format(self.sink, sink, year, self._pad(month))

        # set aliases
        aliases = {'ozone': 'StepOneO3', 'residual': 'ResidualStep1'}
        aliases.update({'thirtyone': 'Reflectivity331', 'sixty': 'Reflectivity360'})

        # initialize data
        data = {}

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date for sample counts, and other features
            for alias, name in aliases.items():

                # add means to data
                key = alias
                field = '{}_sums'.format(self._serpentize(name))
                data[key] = hydra.grab(field)

                # add squares to data
                key = '{}ii'.format(alias)
                field = '{}_squares'.format(self._serpentize(name))
                data[key] = hydra.grab(field)

                # add squares to data
                key = '{}iii'.format(alias)
                field = '{}_counts'.format(self._serpentize(name))
                data[key] = hydra.grab(field)

            # extract differences and sample counts
            data['difference'] = hydra.grab('reflectivity_difference_sums')
            data['differenceii'] = hydra.grab('reflectivity_difference_squares')
            data['differenceiii'] = hydra.grab('reflectivity_difference_counts')

            # extract orbit numbers information
            data['orbit'] = hydra.grab('orbit_number')

        # otherwise
        else:

            # extract date for sample counts, and other features
            for alias, name in aliases.items():

                # add means to data
                key = alias
                data[key] = numpy.zeros((60, 180, 360))

                # add squares to data
                key = '{}ii'.format(alias)
                data[key] = numpy.zeros((60, 180, 360))

                # add counts to data
                key = '{}iii'.format(alias)
                data[key] = numpy.zeros((60, 180, 360))

            # add dimension for resudiual
            data['residual'] = numpy.zeros((60, 180, 360, 12))
            data['residualii'] = numpy.zeros((60, 180, 360, 12))
            data['residualiii'] = numpy.zeros((60, 180, 360, 12))

            # initialize reflectivity difference arrays
            data['difference'] = numpy.zeros((60, 180, 360))
            data['differenceii'] = numpy.zeros((60, 180, 360))
            data['differenceiii'] = numpy.zeros((60, 180, 360))

            # initialize orbits array
            data['orbit'] = numpy.array([])

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/10003/OMTO3/{}/{}'.format(*formats), start, stop)

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths:

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # skip if orbit already made
            if orbit not in data['orbit']:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # ingest the path
                hydra.ingest(path)

                # make the pool
                pool = {alias: hydra.grab(name).flatten() for alias, name in aliases.items()}

                # reform residual array
                pool['residual'] = pool['residual'].reshape(-1, 12)

                # divide reflectivities by 100
                pool['thirtyone'] = pool['thirtyone'] / 100
                pool['sixty'] = pool['sixty'] / 100

                # gather latitude data
                latitudes = hydra.grab('Latitude').flatten()
                longitudes = hydra.grab('Longitude').flatten()

                # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
                quality = hydra.grab('QualityFlags').flatten()
                ground = hydra.grab('GroundPixelQualityFlags').flatten()
                algorithm = hydra.grab('AlgorithmFlags').flatten()

                # add rows
                rows = numpy.array([numpy.linspace(0, 59, 60)] * int(latitudes.shape[0] / 60)).flatten()

                # # create masque for desscending data
                # masque = (latitudes > numpy.roll(latitudes, 60))

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # create stack
                stack = numpy.vstack([rows, latitudes, longitudes]).astype(int)
                stack = stack.transpose(1, 0)

                # exclude particular quality flags
                bits = [1, 2, 3]
                mask = self._exclude(quality, bits)

                # exclude ground quality flags for geolocation error and descending
                bits = [5, 6]
                maskii = self._exclude(ground, bits)

                # exclude algorithm flags
                maskiii = (algorithm != 0) & (algorithm != 10)

                # create collective mask
                masque = numpy.logical_and(mask, maskii)
                masque = numpy.logical_and(masque, maskiii)

                # print exclusion message
                formats = (masque.sum(), len(quality))
                self._print('kept {} out of {} points without designated quality issues'.format(*formats))

                # try to
                try:

                    # for each row
                    for pixel, strip in enumerate(stack):

                        # unpack data
                        row, latitude, longitude = strip

                        # if unmasked
                        if masque[pixel]:

                            # extract date for sample counts, and other features
                            for alias, name in aliases.items():

                                # avoid residual
                                if alias == 'residual':

                                    # go through each wave
                                    for wave in range(12):

                                        # get quantity
                                        quantity = pool[alias][pixel][wave]

                                        # check for valid quantity
                                        if (numpy.isfinite(quantity)) & (abs(quantity < 1000)):

                                            # add means to data
                                            key = alias
                                            data[key][row, latitude, longitude, wave] += quantity

                                            # add squares to data
                                            key = '{}ii'.format(alias)
                                            data[key][row, latitude, longitude, wave] += quantity ** 2

                                            # add squares to data
                                            key = '{}iii'.format(alias)
                                            data[key][row, latitude, longitude, wave] += 1

                                # otherwise
                                else:

                                    # get quantity
                                    quantity = pool[alias][pixel]

                                    # check for valid quantity
                                    if (numpy.isfinite(quantity)) & (abs(quantity < 1000)):

                                        # add means to data
                                        key = alias
                                        data[key][row, latitude, longitude] += quantity

                                        # add squares to data
                                        key = '{}ii'.format(alias)
                                        data[key][row, latitude, longitude] += quantity ** 2

                                        # add squares to data
                                        key = '{}iii'.format(alias)
                                        data[key][row, latitude, longitude] += 1

                            # add to differences
                            difference = pool['sixty'][pixel] - pool['thirtyone'][pixel]
                            data['difference'][row, latitude, longitude] += difference
                            data['differenceii'][row, latitude, longitude] += difference ** 2
                            data['differenceiii'][row, latitude, longitude] += 1

                    # append(orbits)
                    data['orbit'] = numpy.array([entry for entry in data['orbit']] + [orbit])
                    data['orbit'].sort()

                    # stamp
                    self._stamp('processed {}.'.format(orbit))

                # unless mismatch
                except ValueError:

                    # stamp
                    self._stamp('error in orbit {}, skipped!'.format(orbit))

        # construct features
        features = []

        # create features
        for alias, name in aliases.items():

            # print status
            print('featuring {}, {}'.format(alias, name))

            # make snake case
            snake = self._serpentize(name)

            # create feature field name
            field = '{}_sums'.format(snake)
            array = data[alias]
            features.append(Feature([field], array))

            # create squares feature field name
            field = '{}_squares'.format(snake)
            key = '{}ii'.format(alias)
            array = data[key]
            features.append(Feature([field], array))

            # create counts feature field name
            field = '{}_counts'.format(snake)
            key = '{}iii'.format(alias)
            array = data[key]
            features.append(Feature([field], array))

        # add difference features
        features.append(Feature(['reflectivity_difference_sums'], data['difference']))
        features.append(Feature(['reflectivity_difference_squares'], data['differenceii']))
        features.append(Feature(['reflectivity_difference_counts'], data['differenceiii']))
        features.append(Feature(['orbit_number'], data['orbit']))

        # stash file
        self.stash(features, destination, compression=9)

        return None

    def churn(self):
        """Churn out propagation specifics for churning out boquette residual plots.

        Arguments:
            None

        Returns:
            None
        """

        # begin variants
        variants = []

        # set titles
        titles = [{'title': 'September Zonal Means, Step One Residuals, 0-10 deg S, 312.61 nm'}]
        titles += [{'title': 'September Zonal Means, Reflectivity Difference 360 - 331, 0-10 deg S'}]

        # set ordinates
        ordinates = [{'ordinate/name': 'residue', 'ordinate/start': -0.5, 'ordinate/finish': 1.0}]
        ordinates += [{'ordinate/name': 'reflectivity', 'ordinate/start': 0, 'ordinate/finish': 0.02}]

        # set fields and stubs
        fields = ['step_one_residuals', '360_331_reflectivity_differences']
        stubs = ['residue', 'diff']

        # set colors
        colors = ['#ff0000', '#e9b21c', '#19c819', '#5fdddd', '#0000ff']

        # for each set
        for title, ordinate, field, stub in zip(titles, ordinates, fields, stubs):

            # for each set 10 rows
            for block in range(6):

                # set name
                name = '{}_{}-{}'.format(field, self._pad(1 + block * 10), self._pad(10 + block * 10))

                # for both sets
                for initial in (0, 5):

                    # set tag
                    tags = ['_{}'.format(self._pad(increment + initial + 1 + block * 10)) for increment in range(5)]

                    # add all lines
                    rows = [1 + (block * 10) + initial + index for index in range(5)]
                    labels = ['Row {}'.format(row) for row in rows]

                    # create lines
                    lines = {'lines': [{'label': label, 'color': color} for label, color in zip(labels, colors)]}

                    # create parameters:
                    details = [{'file': 0, 'step': 1, 'name': name, 'tag': tag} for tag in tags]
                    parameters = {'propagation/parameters': details}

                    # add stub
                    slide = {'propagation/stub': '{}_{}'.format(stub, self._pad(1 + initial + block * 10))}

                    # make propgation
                    propagation = {}
                    sections = (title, ordinate, lines, parameters, slide)
                    [propagation.update(section) for section in sections]
                    variants.append(propagation)

        return variants

    def digest(self, source='months', sink='zones', sinkii='plots'):
        """Calculate the zonal means for the accumulated ozone files.

        Arugments:
            source: str, source folder for monthly data
            sink: str, sink folder for zonal files
            sinkii: str, sink folder for plots

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # set aliases
        aliases = {'ozone': 'StepOneO3', 'residual': 'ResidualStep1'}
        aliases.update({'thirtyone': 'Reflectivity331', 'sixty': 'Reflectivity360'})
        aliases.update({'difference': 'ReflectivityDifference'})

        # set fill value
        fill = -9999

        # get hydra
        hydra = Hydra('{}/{}'.format(self.sink, source))

        # for each path
        for path in hydra.paths:

            # set destination
            destination = path.replace('Sums', 'Means').replace(source, sink)

            # ingest the file
            hydra.ingest(path)

            # grab all data
            data = {feature.name: hydra.grab(feature.name) for feature in hydra}

            # create zonal sums
            zones = {}
            for name, array in data.items():

                # try to:
                try:

                    # get transposition
                    transposition = (1, 0)
                    if 'residual' in name:

                        # reset transposition
                        transposition = (1, 0, 2)

                    # sum across longitude
                    chunk = 10
                    zone = array.sum(axis=2)
                    zone = [zone[:, index * chunk: chunk + index * chunk].sum(axis=1) for index in range(18)]
                    zone = numpy.array(zone).transpose(*transposition)
                    zones[name] = zone

                # unless shape doesn't match
                except numpy.AxisError:

                    # skip, but alert
                    print('error!', name)

            # for each name
            features = []
            for alias, name in aliases.items():

                # get snake case
                snake = self._serpentize(name)

                # get data
                sums = zones['{}_sums'.format(snake)]
                squares = zones['{}_squares'.format(snake)]
                counts = zones['{}_counts'.format(snake)]

                # calculate mean
                mean = sums / counts
                mean = numpy.where((numpy.isfinite(mean) & (abs(mean) < 1e9)), mean, fill)
                feature = Feature(['{}_avg'.format(snake)], mean)
                features.append(feature)

                # std dev
                deviation = numpy.sqrt(squares / counts - mean ** 2)
                deviation = numpy.where((numpy.isfinite(deviation) & (abs(deviation) < 1e9)), deviation, fill)
                feature = Feature(['{}_dev'.format(snake)], deviation)
                features.append(feature)

            # stash file
            self.stash(features, destination, compression=9)

            # create plot file
            destination = destination.replace(sink, sinkii).replace('.h5', '_Plots.h5')

            # add Category to each route
            [feature.divert('Categories') for feature in features]

            # # add row as independent variable
            # features.append(Feature(['IndependentVariables', 'row'], numpy.linspace(0, 59, 60)))

            # add latitude as independent varaible
            latitude = numpy.array([90 - (5 + index * 10) for index in range(18)])
            features.append(Feature(['IndependentVariables', 'latitude'], latitude))

            # stash file
            self.stash(features, destination, compression=9)

        return None

    def reside(self, source='sequences', sink='plots', wavelength=312.5, latitude=-5):
        """Create time sequence residual plots.

        Arguments:
            source: str, source folder
            sink: str, sink folder
            wavelength: float, wavelength
            latitude: float, latitude

        Returns:
            Nne
        """

        # collect sequences
        hydra = Hydra('{}/{}'.format(self.sink, source))
        hydra.ingest()

        # get the wavelength index
        wavelengths = hydra.grab('residual_wavelengths')
        wave = ((wavelengths - wavelength) ** 2).argsort()[0]

        # get the latitude index
        latitudes = hydra.grab('latitudes')
        degrees = ((latitudes - latitude) ** 2).argsort()[0]

        # create years abscissa
        abscissa = hydra.grab('years')

        # get the residuals
        residuals = hydra.grab('residual_step1_avg')
        residuals = residuals[:, :, degrees, wave]

        # make feautures
        features = {'IndependentVariables/year': abscissa}
        features.update({'Categories/step_one_residuals': residuals})

        # add reflectivity difference
        differences = hydra.grab('reflectivity_difference_avg')
        differences = differences[:, :, degrees]
        features.update({'Categories/360_331_reflectivity_differences': differences})

        # create plot file
        destination = '{}/{}/Step1_Residuals_Zonal_Means_Time.h5'.format(self.sink, sink)
        hydra.create(destination, features)

        return None

    def sequence(self, sink='sequences'):
        """Create time sequence files from zonal means.

        Arguments:
            None

        Returns:
            Nne
        """

        # get hydra for zonal means
        hydra = Hydra('{}/zones'.format(self.sink))
        paths = hydra.paths
        paths.sort()

        # for each path
        sequences = {}
        for path in paths:

            # ingest the data
            hydra.ingest(path)

            # for each feature
            for feature in hydra:

                # add array to list
                sequence = sequences.setdefault(feature.name, [])

                # grab the data and append the array
                array = feature.distil()
                sequence.append(array)

        # create numpy arrays from sequences
        sequences = {name: numpy.array(sequence) for name, sequence in sequences.items()}

        # add years
        sequences['years'] = numpy.array([int(path.split('_')[-2]) for path in paths])

        # add wavelengths
        wavelengths = [308.70001220703125, 310.8000183105469, 311.8500061035156, 312.6100158691406]
        wavelengths += [313.20001220703125, 314.3999938964844, 317.6199951171875, 322.4200134277344]
        wavelengths += [331.3399963378906, 345.3999938964844, 360.1499938964844, 372.8000183105469]
        sequences['residual_wavelengths'] = numpy.array(wavelengths)

        # add latitudes
        latitudes = [85, 75, 65, 55, 45, 35, 25, 15, 5, -5, -15, -25, -35, -45, -55, -65, -75, -85]
        sequences['latitudes'] = numpy.array(latitudes)

        # dump into new folder
        self._make('{}/{}'.format(self.sink, sink))
        destination = '{}/{}/Zonal_Means_OMTO3_Sequences.h5'.format(self.sink, sink)
        hydra.create(destination, sequences)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 5
    arguments = arguments[:5]

    # umpack arguments
    year, month, start, finish, sink = arguments

    # create osprey instance
    osprey = Osprey(sink)

    # run accumulation
    osprey.accumulate(year, month, start, finish)


