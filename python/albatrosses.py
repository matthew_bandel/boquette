#!/usr/bin/env python3

# albatrosses.py for the Albatross class for ml studies on ozone profiles and uv spectra

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import regex
import re

# import numpy functions
import numpy

# import other math
import math
import scipy

# import pickle for pickling models
import pickle

# import datetime
import datetime
from time import sleep

# import sklearn
from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import r2_score, auc, roc_curve
from sklearn.svm import LinearSVC

# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    from mpl_toolkits.mplot3d import Axes3D
    matplotlib.use('TkAgg')
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # pass
    print('no matplotlib available')
    pass

# try to
try:

    # import tensorflow
    import tensorflow

    # import keras
    from tensorflow.keras import optimizers, regularizers
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.models import Model, load_model
    from tensorflow.keras.layers import Dense, Dropout, MaxPooling2D
    from tensorflow.keras.layers import SimpleRNN
    from tensorflow.keras.layers import LSTM
    from tensorflow.keras.layers import Activation
    from tensorflow.keras.layers import TimeDistributed
    from tensorflow.keras.layers import LeakyReLU
    from tensorflow.keras.layers import Masking
    from tensorflow.keras.layers import Conv1D, Conv2D, Flatten, Reshape
    from tensorflow.keras.preprocessing.sequence import pad_sequences

# unless import error
except ImportError:

    # pass
    print('no tensor flow available')
    pass


# class Albatross to do OMI data reduction
class Albatross(Hydra):
    """Albatross class for ml studies on n.

    Inherits from:
        Hydra
    """

    def __init__(self, sink, tag=None, ticket=None):
        """Initialize instance.

        Arguments:
            sink: str, sink diectory
            tag: str, tag for data
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # keep records reservoir
        self.reservoir = []

        # set sink directory
        self.sink = sink

        # set tag to tag all data
        self.tag = tag or sink.split('/')[-1]

        # set boost
        self.boost = None

        # set spline
        self.spline = None

        # set backscatter values
        self.backscatter = None

        # add squid for plotting
        self.squid = Squid(sink)

        # set ticket file
        self.ticket = ticket

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Albatross instance ( {} ) at {} >'.format(self.tag, self.sink)

        return representation

    def _activate(self, model, sample):
        """Activate the network with a particular sample.

        Arguments:
            None

        Returns:
            None
        """

        # get the perceptor
        perceptor = model['machines']['perceptor']

        # get sample predictions
        prediction = perceptor.predict(sample.reshape(1, -1))

        # grab weights and biases
        weights = perceptor.coefs_
        biases = perceptor.intercepts_

        # begin state for all the neurons
        state = [sample]
        layer = sample
        lasts = [False] * (len(weights) - 1) + [True]
        for weight, bias, last in zip(weights, biases, lasts):

            # multipy weights by layer
            activity = weight * numpy.array([layer]).transpose(1, 0)
            activity = activity.sum(axis=0)
            activity = activity + bias

            # if not the last layer
            if not last:

                # apply relu
                activity = numpy.where(activity > 0, activity, 0)

            # reset layer
            layer = activity

            # add to state
            state.append(layer.copy())

        return state

    def _assemble(self, train, loss, percents):
        """Assemble sample traits from training data.

        Arguments:
            train: dict of numpy arrays
            loss: direct model losses
            percents: percent errors

        Returns:
            list of tuples
        """

        # recompose months
        month = train['month']
        months = []
        for row in month:

            # determine placement of 1
            index = list(row).index(1)
            months.append(train['months'][index])

        # recompose latitudes
        latitude = train['latitude']
        latitudes = []
        for row in latitude:

            # determine placement of 1
            index = list(row).index(1)
            latitudes.append(train['latitudes'][index])

        # get albedo and zenith
        albedos = train['albedo'][:, 0].tolist()
        zeniths = train['zenith'][:, 0].tolist()

        # get wavelengths
        wavelengths = train['wavelength']

        # find biggest absolute losses
        losses = []
        waves = []
        for row in abs(loss):

            # get the maximum
            maximum = max(row)
            index = list(row).index(maximum)
            wave = wavelengths[index]

            # add to losses and waves
            losses.append(maximum)
            waves.append(wave)

        # find biggest absolute percent error
        absolutes = []
        wavesii = []
        indices = []
        for number, row in enumerate(abs(percents)):

            # get the maximum
            maximum = max(row)
            index = list(row).index(maximum)
            wave = wavelengths[index]

            # add to losses and waves
            absolutes.append(maximum)
            wavesii.append(wave)

            # add index
            indices.append(number)

        # zip all together
        zipper = zip(latitudes, months, albedos, zeniths, waves, losses, wavesii, absolutes, indices)

        # create traits
        traits = [tuple(entry) for entry in zipper]

        # make into dicts
        fields = ['latitude', 'month', 'albedo', 'zenith', 'wave', 'loss', 'wavelength', 'error', 'index']
        traits = [{field: quantity for field, quantity in zip(fields, trait)} for trait in traits]

        return traits

    def _box(self, row, factor):
        """Apply the box cox transformation

        Arguments:
            row: 1-Dnumpy array

        Returns:
            list of floats, the box cox parameters
        """

        # copy array
        row = row.copy()
        box = row.copy()

        # check for null factor
        if factor is not None:

            # try to
            try:

                # if factor is zero
                if factor == 0:

                    # apply special case for 0 ( log )
                    box = numpy.log(row)

                # otherwise
                else:

                    # apply general box cox
                    box = ((row ** factor) - 1) / factor

            # unless encountering an error:
            except ValueError:

                # in which case, pass
                box = None

        return box

    def _build(self, array, transform, size):
        """Build the machine.

        Arguments:
            array: 2-d numpy.array
            transform, str, name of transformation
            size: int, number of pca vectors

        Returns:
            dict of machinery
        """

        # copy array
        array = array.copy()

        # retrieve method
        method = transform.strip('_')

        # begin machinery
        machinery = {}

        # make scaline
        if method == 'scale':

            # determine scaling
            median, minimum, maximum = self._measure(array)
            machinery['median'] = median
            machinery['minimum'] = minimum
            machinery['maximum'] = maximum

        # make scaline
        if method == 'grow':

            # determine scaling
            median, minimum, maximum = self._measure(array)
            machinery['median'] = median
            machinery['minimum'] = minimum
            machinery['maximum'] = maximum

        # make scaline
        if method == 'shrink':

            # determine scaling
            median, minimum, maximum = self._measure(array)
            machinery['median'] = median
            machinery['minimum'] = minimum
            machinery['maximum'] = maximum

        # make normalization
        if method in ('normalize', 'prenormalize'):

            # determine means and deviations for each row in the array
            mean, deviation = self._state(array)
            machinery['mean'] = mean
            machinery['deviation'] = deviation

        # make pca machine
        if method == 'pca':

            # if size is greater
            if size > 0:

                # create pca machine
                machine = PCA(n_components=size)
                machine.fit(array)
                machinery['size'] = size
                machinery['machine'] = machine

        # make boxcox machine
        if method == 'boxcox':

            # get boxcox factors
            factors = self._improve(array)
            machinery['factor'] = factors

        # make boxcox machine
        if method == 'expand':

            # no machinery needed
            pass

        return machinery

    def _calculate(self, coefficients, references, pixels):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            pixels: int, number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns])
        indices = numpy.array([image])

        # expand tensor of column references
        references = numpy.array([references] * 1)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(5)])
        powers = powers.transpose(1, 2, 3, 0).squeeze()

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=1)

        return wavelengths

    def _comb(self, brackets, augmentation):
        """Retrieve data from pressure calculations file.

        Arguments:
            subset: str, radiance subset
            augmentation: data augmentation dictionary

        Returns:
            dict of numpy arrays
        """

        # map data fields to shorthand aliases
        aliases = {'ozone': 'OzoneMixingRatio', 'altitude': 'Altitude'}
        aliases.update({'latitude': 'LatitudeZone', 'radiance': 'Radiance_SSMS'})
        aliases.update({'pressure': 'Pressure', 'surface': 'SurfacePressure'})
        aliases.update({'temperature': 'Temperature'})
        aliases.update({'month': 'MonthNumber'})
        aliases.update({'scatter': 'OpticalDepth_Rayleigh', 'absorption': 'OpticalDepth_Ozone'})
        aliases.update({'wavelength': 'Wavelength', 'zenith': 'SolarZenithAngle'})
        aliases.update({'layer': 'LayerOzone', 'albedo': 'SurfaceAlbedo'})
        aliases.update({'single': 'Radiance_SS', 'multiple': 'Radiance_MS'})
        aliases.update({'backscatter': 'Backscatter_Sb', 'transmittance': 'Transmittance_T'})
        aliases.update({'naught': 'SS_naught', 'naughtii': 'MS_naught'})
        aliases.update({'backscatterii': 'Backscatter_SS_Sb', 'transmittanceii': 'Transmittance_SS_T'})
        aliases.update({'backscatteriii': 'Backscatter_MS_Sb', 'transmittanceiii': 'Transmittance_MS_T'})

        # begin data obejct with all aliases
        data = {}

        # grab the ozone data
        hydra = Hydra('{}/calculations'.format(self.sink))

        # ingest the ata
        hydra.ingest(0)

        # for each alias
        for alias, field in aliases.items():

            # try to
            try:

                # add to reservoirw
                array = hydra.grab(field)
                data[alias] = array

            # unless not found
            except IndexError:

                # in which case, pass
                pass

        self._view('before interpolation', data, 'single', 'albedo', 'surface', 'zenith')

        # construct average of radiance across zenith angle
        radiance = data['radiance']
        radiance = radiance.mean(axis=2).mean(axis=1)

        # also grab zenith angles
        zenith = data['zenith']

        # construct spline
        spline = scipy.interpolate.CubicSpline(zenith, radiance)
        self.spline = spline

        # grab  main shape
        shape = data['single'].shape

        # add spline field as radiance normalized by spline
        block = numpy.array([zenith] * shape[1])
        block = numpy.array([block] * shape[2])
        block = block.transpose(2, 1, 0)
        data['spline'] = data['radiance'] / spline(block)

        # take logarithms
        for field in ('single', 'multiple', 'radiance', 'spline'):

            # take log
            data[field] = numpy.log10(data[field])

        # restrict zeniths
        zeniths = brackets['zeniths']
        data['zenith'] = data['zenith'][zeniths[0]: zeniths[1]]
        for field in ('single', 'multiple', 'radiance', 'spline'):

            # take log
            data[field] = data[field][zeniths[0]: zeniths[1]]

        # for each radiance component
        angles = None
        length = zeniths[1] - zeniths[0]
        fields = ['single', 'multiple', 'radiance', 'spline']
        # fields += ['naught', 'naughtii', 'backscatter', 'backscatterii', 'backscatteriii']
        # fields += ['transmittance', 'transmittanceii', 'transmittanceiii']
        for field in fields:

            # convert radiance with logarithm, and expand along interpolations
            radiance = data[field]

            print('')
            print('radiance: ', radiance.shape)

            # increase shape to 5-D
            radiance = radiance.reshape(1, *radiance.shape, 1)

            print('radiance: ', radiance.shape)

            # create array from zenith angles
            zenith = data['zenith'][zeniths[0]: zeniths[1]]
            data['zeniths'] = zenith.copy()

            # check for zenith expansion
            expansion = augmentation.get('interpolation', 0)
            random = augmentation.get('random', False)

            # get interpolationn of radiance data
            self._print('\n expanding {}...'.format(field))
            radiance, zenith, angles = self._expand(radiance, zenith, expansion, random=random, axis=1, angles=angles)

            print('radiance: ', radiance.shape)

            # update
            data[field] = radiance
            length = len(zenith)

        self._view('after interpolation', data, 'single', 'albedo', 'surface', 'zenith')

        # result zenith angles
        data['zenith'] = angles
        shape = (length, *shape[1:])

        # stack albedos by other two shapes
        albedo = data['albedo']
        albedo = numpy.array([albedo] * shape[0])
        albedo = numpy.array([albedo] * shape[2])
        #albedo = albedo.transpose(1, 2, 0).transpose(2, 1, 0)
        albedo = albedo.transpose(0, 2, 1)
        data['albedo'] = albedo

        # stack surface pressure by other two shapes
        surface = data['surface']
        surface = numpy.array([surface] * shape[0])
        surface = numpy.array([surface] * shape[1])
        #surface = surface.transpose(1, 0, 2).transpose(2, 1, 0)
        surface = surface.transpose(2, 0, 1)
        data['surface'] = surface

        self._view('after stacking', data, 'single', 'albedo', 'surface', 'zenith')

        # # stack zeniths by other two shapes
        # zenith = data['zenith'].squeeze()
        # zenith = numpy.array([zenith] * shape[1])
        # zenith = numpy.array([zenith] * shape[2])
        # zenith = zenith.transpose(2, 1, 0)
        # data['zenith'] = zenith

        # create cosine
        data['cosine'] = numpy.cos(data['zenith'] * (math.pi / 180))

        self._view('before flattening', data, 'single', 'albedo', 'surface', 'zenith')

        # flatten all data, expand by 1
        for name, array in data.items():

            # flatten and expand
            data[name] = array.flatten().reshape(-1, 1)

        self._view('after flattening', data, 'single', 'albedo', 'surface', 'zenith')

        return data

    def _combine(self, array, size):
        """Add profile layers together according to combination.

        Arguments:
            array: numpy array
            size: int, number of layers to add

        Returns:
            numpy array
        """

        # create brackets
        chunks = math.ceil(array.shape[1] / size)
        brackets = [(size * index, size + size * index) for index in range(chunks)]

        # create stack of summations
        stack = [array[:, first:second].sum(axis=1) for first, second in brackets]

        # recreate array
        stack = numpy.vstack(stack).transpose(1, 0)

        return stack

    def _crate(self, array, factors):
        """Apply box cox transformation to each column of an array.

        Arguments:
            array: numpy array
            factors: list of floats, box cox parameters

        Returns:
            numpy.array
        """
        # transpose array
        array = array.copy()
        array = array.transpose(1, 0)

        # apply box cox transforms
        crate = numpy.array([self._box(row, factor) for row, factor in zip(array, factors)])

        # retranspose
        crate = crate.transpose(1, 0)

        return crate

    def _decompose(self, radiance, waves=(142, 143), transposition=(2, 0, 1, 3, 4)):
        """Decompose radiances into transmittance and backscatter.

        Arguments:
            radiance: numpy array ( wave x zenith x albedo x month x latitude )
            wave: int, wavelenfth index

        Returns:
            numpy array
        """

        # transpose radiance to get albedo up front
        radiance = radiance.copy()[waves[0]:waves[1]].transpose(*transposition)

        # flatten all except albedo
        shape = radiance.shape
        radiance = numpy.array([radiance[index].flatten() for index in range(11)])

        # get naughts
        naught = radiance[0]
        reciprocal = 1 / (radiance[1:] - naught)
        reciprocal = reciprocal.transpose(1, 0)

        # conwtruct albedos
        albedo = numpy.array(list(range(11)))[1:]
        albedo = albedo / 10

        # begin transmittance and backscatter
        backscatter = []
        transmittance = []
        for row in reciprocal:

            # contruct optimization function, 1 / ( i - io ) = ( 1 - RS ) / RT
            function = lambda r, s, t: (1 - r * s) / (r * t)

            # create optimizer
            optimizer = scipy.optimize.curve_fit(function, albedo, row)
            backscatter.append(optimizer[0][0])
            transmittance.append(optimizer[0][1])

        # create arrays
        naught = numpy.log10(naught).reshape(*shape[1:])
        backscatter = numpy.array(backscatter).reshape(*shape[1:])
        transmittance = numpy.array(transmittance).reshape(*shape[1:])

        return naught, backscatter, transmittance

    def _expand(self, radiance, zenith, expansion=2, axis=1, pivot=60, random=False, angles=None):
        """Perform cubic spline interplation to expand radiance data across more zenith angles.

        Arguments:
            radiance: numpy array
            zenith: numpy array
            expansion: int, degree of expansion
            axis: int, axis of expansion
            pivot: float, value at which to flip from cubic to barycentric
            random: boolean, use random interpolation?
            angles: previous angles if already interpolated

        Returns:
            numpy array
        """

        # get shape of array
        shape = radiance.shape
        size = shape[axis]

        # reshape
        radiance = radiance.transpose(0, 4, 3, 2, 1)
        radiance = radiance.flatten()
        radiance = radiance.reshape(-1, size)

        # print
        self._print('interpolating zeniths...')

        # create splines and polynomials for every sample and every wavelength
        splines = []
        polynomials = []
        for count, row in enumerate(radiance):

            # crate splinse
            spline = scipy.interpolate.CubicSpline(zenith, row)
            splines.append(spline)

            # create polynomial
            polynomial = scipy.interpolate.BarycentricInterpolator(zenith, row)
            polynomials.append(polynomial)

        # for each expansion
        for _ in range(expansion):

            # create new zenith angles
            zenith = zenith.tolist()
            insertions = []
            for pair in zip(zenith[:-1], zenith[1:]):

                # create average
                average = (pair[0] + pair[1]) / 2
                insertions.append(average)

            # add to zeniths and sort
            zenith = zenith + insertions
            zenith.sort()
            zenith = numpy.array(zenith)

            # adjust size
            size = (2 * size - 1)

        # set angles
        if angles is None:

            # set to empty list
            angles = []

        # otherwise
        else:

            # flatten
            angles = angles.flatten().tolist()

        # recreate samples from spines
        samples = []
        samplesii = []
        priors = len(angles) > 0
        zenith = zenith.tolist()
        length = len(splines)
        for count, spline, polynomial in zip(range(length), splines, polynomials):

            # if no angles chosen yet
            if not priors:

                # set new angles
                angle = zenith.copy()

                # if random
                if random:

                    # # randommize zenith angles by cosine
                    # number = len(angle)
                    # angle = [math.acos(cosine) * 180 / math.pi for cosine in numpy.random.rand(number)]
                    # factor = 89 / 90
                    # angle = [entry * factor for entry in angle]

                    # randomize just angles, 89 degrees max
                    number = len(angle)
                    minimum = min(angle)
                    maximum = max(angle)

                    # set random seed for reproducibility
                    numpy.random.seed(count)

                    # create random angles
                    angle = [minimum + (maximum - minimum) * entry for entry in numpy.random.rand(number)]

                    # sort angles
                    angle.sort()

                # add to angles
                angles += angle.copy()

            # otherwise
            else:

                # get appropriate angles
                angle = angles[count * size: size + count * size]

            # create interpolation
            sample = spline(angle)
            samples.append(sample)

            # create polynomial interpolation
            sampleii = polynomial(angle)
            samplesii.append(sampleii)

        # create arrays
        samples = numpy.array(samples)
        samplesii = numpy.array(samplesii)

        # merge samples at pivot
        marker = [index for index, entry in enumerate(zenith) if entry >= pivot][0]
        samples = numpy.hstack([samples[:, :marker], samplesii[:, marker:]])

        # make zenith into array
        zenith = numpy.array(zenith)

        # make array from angles
        samples = samples.flatten()
        samples = numpy.array([samples]).transpose(1, 0)
        angles = numpy.array([angles]).transpose(1, 0)

        return samples, zenith, angles

    def _grid(self, optics, pressures, grid):
        """Interpolate optical depths to pressure grid.

        Arguments:
            optics: numpy array, optical depth profiles
            pressure: numpy array, pressure
            grid: numpy array, pressure grid

        Returns:
            numpy array
        """

        # print
        self._print('gridding by pressure...')

        # create accumulation
        accumulation = numpy.array([optics[:, index:].sum(axis=1) for index in range(optics.shape[1])])
        accumulation = accumulation.transpose(1, 0)

        # interpolate to pressure grid
        interpolations = []
        for row, pressure in zip(accumulation, pressures):

            # begin interpolation
            interpolation = []

            # for each gridpoint
            for point in grid:

                # check for greater than pressure
                if point > pressure[0]:

                    # set indicdes
                    index = 0
                    indexii = 1

                    # interpolate
                    slope = (point - pressure[index]) / (pressure[indexii] - pressure[index])
                    novel = row[index] + (row[indexii] - row[index]) * slope
                    interpolation.append(novel)

                # otherwise check for other extreme
                elif point < pressure[-1]:

                    # set indices
                    index = pressure.shape[0] - 2
                    indexii = pressure.shape[0] - 1

                    # interpolate
                    slope = (point - pressure[index]) / (pressure[indexii] - pressure[index])
                    novel = row[index] + (row[indexii] - row[index]) * slope
                    interpolation.append(novel)

                # otheriwse
                else:

                    # find first index greater than point
                    index = [number for number, entry in enumerate(pressure) if entry < point][0]
                    indexii = index + 1

                    # interpolate
                    slope = (point - pressure[index]) / (pressure[indexii] - pressure[index])
                    novel = row[index] + (row[indexii] - row[index]) * slope
                    interpolation.append(novel)

            # add to interpolatinos
            interpolations.append(interpolation)

        # convert to array
        interpolations = numpy.array(interpolations)

        # reverse accumulation step to recover optical depths
        reversals = []
        for row in interpolations:

            # begin reversal
            reversal = [row[-1]]
            for index in range(row.shape[0] -1):

                # add to reversal
                entry = row[-(index + 2)] - sum(reversal)
                reversal.append(entry)

            # reverse reversal and append
            reversal.reverse()
            reversals.append(reversal)

        # make numpy array
        reversals = numpy.array(reversals)

        return reversals

    def _halt(self, base):
        """Generator function for Halton sequence.

        Arguments:
             base: int, the base

        Returns:
            float
        """

        # initial halton sequence generator
        number, digit = 0, 1

        # perform loop
        while True:

            # calculate difference
            difference = digit - number

            # if difference is one
            if difference == 1:

                # multiple by base
                number = 1
                digit *= base

            # otherwise
            else:

                # take floor and iterate
                result = digit // base
                while difference <= result:

                    # compute reulst
                    result //= base

                # get new number
                number = (base + 1) * result - difference

            yield number / digit

    def _inspect(self, row):
        """Inspect a row of data for normalcy.

        Arguments:
            row: 1D numpy array.

        Returns:
            float, the normalcy score
        """

        # sort the row, after copying
        row = row.copy()
        row.sort()

        # calculate mean and deviations
        mean = row.mean()
        deviation = row.std() or 1.0

        # calculate z-scores for the row
        scores = (row - mean) / deviation

        # calculate theoretical quantiles based on position
        length = len(row)
        theory = numpy.array([index + 0.5 for index in range(length)]) / length

        # # calculate predicted quantile based on z-scores ( Q = 1/2 ( 1 + erf (z / rt2) )
        quantiles = (scipy.special.erf(scores / math.sqrt(2)) + 1) / 2

        # calculate coefficient of determination betweenn the two as measure of normalcy
        normalcy = r2_score(quantiles, theory)

        return normalcy

    def _improve(self, array):
        """Find the best box cox parameter that improves the normalcy

        Arguments:
            array: numpy array

        Returns:
            list of floats, the box cox parameters
        """

        # copy array
        array = array.copy()

        # define parameter range
        factors = [-5, -4, -3, -2, -1, -0.5, 0, 0.5, 1, 2, 3, 4, 5]

        # begin list of best normal values
        bests = []

        # add dimension if array is singular
        if len(array.shape) < 2:

            # create dimension
            array = numpy.array([array])

        # otherwise
        else:

            # transpose to get each dimension along rows
            array = array.transpose(1, 0)

        # for each row
        for row in array:

            # determine normalacy, assuming best
            best = None
            normalcy = self._inspect(row)

            # for each box cox factor
            for factor in factors:

                # apply box cox
                box = self._box(row, factor)

                # try to
                try:

                    # get score
                    score = self._inspect(box)

                    # check for improvement
                    if score > normalcy:

                        # and adjust counters
                        best = factor
                        normalcy = score

                # unless an arror
                except ValueError:

                    # in case skip
                    pass

            # add best factor for the row
            bests.append(best)

        return bests

    def _interpolate(self, radiances, wavelengths):
        """Interpolate radiances across a wavelength grid.

        Arguments:
            radiances: numpy array of radiances
            wavelengths: numpy array of floats, calculated wavelengths

        Returns:
            None
        """

        # get the start and finish wavelengths based on the shortest and longest wavelengths amongst all rows
        start = wavelengths.min(axis=0).max()
        finish = wavelengths.max(axis=0).min()

        # convert nanometers to angstroms for a grid based on tenths of nanometers
        start = math.ceil(10 * start)
        finish = math.floor(10 * finish)

        # create wavelength grid for entire range at a resolution of tenth of wavelength
        grid = [number / 10 for number in range(start, finish + 1)]
        grid = numpy.array(grid)

        # for each wavelength assignment in the grid
        interpolations = []
        for assignment in grid:

            # make lesser and greater masks
            lesser = wavelengths <= assignment
            greater = wavelengths > assignment

            # roll the greater mask towards the lesser so trues overlap only left of the assignment
            left = lesser * numpy.roll(greater, -1, axis=0)

            # roll the lesser towards the greater so trues overlap only right of the assignment
            right = numpy.roll(lesser, 1, axis=0) * greater

            # extract initial and final radiances by multiplyiing with masks
            initial = (radiances * left)
            final = (radiances * right)

            # collapse dimension by summing (just one entry should be nonzero per row)
            initial = initial.sum(axis=0)
            final = final.sum(axis=0)

            # extract short and long wavelengths
            short = (wavelengths * left).sum(axis=0)
            long = (wavelengths * right).sum(axis=0)

            # calculate rise and run
            rise = final - initial
            run = long - short

            # apply linear interpolation formula: y = y0 + (x - x0) * (y1 - y0) / (x1 - x0)
            interpolation = initial + (assignment - short) * rise / run

            # traspose and append
            interpolations.append(interpolation)

        # # concatenate and transpose
        interpolations = numpy.array(interpolations)

        return interpolations, grid

    def _measure(self, array):
        """Compute median, minimums, and maximums of all columns in the dataset.

        Arguments:
            array

        Returns:
            tuple of list of floats
        """

        # copy array
        array = array.copy()

        # begin means and deviations
        medians = numpy.median(array, axis=0)
        minimums = array.min(axis=0)
        maximums = array.max(axis=0)

        return medians, minimums, maximums

    def _normalize(self, array, means, deviations):
        """Normalize an array to mean zero and unit standard devitation.

        Arguments:
            array: numpy array
            means: list of float, means per row
            deviations; list of float, deviations per row

        Returns:
            numpy array
        """

        # transpose array to get columns as rows
        array = array.transpose(1, 0)

        # perform normalization
        zipper = zip(array, means, deviations)
        normalization = numpy.array([(row - mean) / deviation for row, mean, deviation in zipper])

        # transpose again
        normalization = normalization.transpose(1, 0)

        return normalization

    def _pack(self, error, confidence, dimension):
        """Determine the number of samples needed for given confidence and error, using SPACC.

        Arguments:
            error: error bounds, 0-1
            confidence: confidence interval, 0-1
            dimension: int, number of input dimensions.

        Returns:
            int, number of samples
        """

        # calculate SPACC limit
        limit = (1 / (2 * error ** 2)) * numpy.log(2 / (1 - confidence))

        # if the dimension is 9 or less
        index = 0
        if dimension < 10:

            # as long as samples small
            samples = index ** dimension
            while (samples < limit):

                # increment and recalculate
                index += 1
                samples = index ** dimension

        # otherwies
        else:

            # use large dimentioin formual
            samples = 10 ** index
            while (samples < limit):

                # increment and recalculate
                index += 1
                samples = 10 ** index

        return samples

    def _pick(self, interpolations, grid):
        """Remove bits of grid uncommon to entire dataset.

        Arguments:
            interpolations: list of numpy arrays
            grid: list of numpy arrays

        Returns:
            numpy array, numpy array
        """

        # determine the end points of the grid
        start = max([min(row) for row in grid])
        finish = min([max(row) for row in grid])

        # create list of all wavelengths
        wavelengths = self._skim([wavelength for row in grid for wavelength in row])
        wavelengths = [wavelength for wavelength in wavelengths if start <= wavelength <= finish]

        # create set of radiances
        radiances = [radiance[numpy.isin(row, wavelengths)] for radiance, row in zip(interpolations, grid)]

        return radiances, wavelengths

    def _reconstitute(self, prediction, feature, auxiliaries):
        """Reconstitute perceptor outputs.

        Arguments:
            prediction: numpy array
            feature: str, feature name
            auxiliaries: dict of auxiliary data

        Returns:
            None
        """

        # set trivial estimate
        estimate = prediction

        # calculate estimate from log of MS
        if feature == 'radiance':

            # use as exponent
            estimate = 10 ** prediction

        # calculate estimate from spline normalized
        if feature == 'spline':

            # use as exponent
            estimate = self.spline(auxiliaries['zenith']) * 10 ** prediction

        # calculate estimate from log of MS
        if feature == 'naughtii':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** prediction
            backscatter = auxiliaries['backscatter']
            transmittance = auxiliaries['transmittance']
            estimate = naught + naughtii + (albedo * transmittance) / (1 - albedo * backscatter)

        # calculate estimate from log of MS
        if feature == 'backscatter':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** auxiliaries['naughtii']
            backscatter = prediction
            transmittance = auxiliaries['transmittance']
            estimate = naught + naughtii + (albedo * transmittance) / (1 - albedo * backscatter)

        # calculate estimate from log of MS
        if feature == 'transmittance':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** auxiliaries['naughtii']
            backscatter = auxiliaries['backscatter']
            transmittance = prediction
            estimate = naught + naughtii + (albedo * transmittance) / (1 - albedo * backscatter)

        # calculate estimate from log of MS
        if feature == 'multiple':

            # percent difference from log multiple
            estimate = 10 ** prediction + 10 ** auxiliaries['single']

        # calculate estimate from log ratio of SS / MS
        if feature == 'ratio':

            # percent difference from log multiple
            estimate = 10 ** (auxiliaries['single'] / prediction) + 10 ** auxiliaries['single']

        # calculate estimate from log ratio of SS / MS
        if feature == 'ratioii':

            # percent difference from log multiple
            estimate = (10 ** auxiliaries['single'] / 10 ** prediction)

        # calculate estimate from log ratio of SS / MS
        if feature == 'ratioiii':

            # percent difference from log multiple
            estimate = 10 ** (auxiliaries['single'] / prediction)

        # calculate precent from ratio
        if feature == 'difference':

            # percent difference from log difference of SS - MS
            estimate = 10 ** (auxiliaries['single'] - prediction) + 10 ** auxiliaries[0]

        # calculate estimate from triplet
        if feature == 'triplet':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** prediction[:, 0:1]
            backscatter = prediction[:, 1:2]
            transmittance = prediction[:, 2:3]
            estimate = naught + naughtii + (albedo * transmittance) / (1 - albedo * backscatter)

        # calculate estimate from Tss / Tms
        if feature == 'transcendence':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** auxiliaries['naughtii']
            backscatterii = auxiliaries['backscatterii']
            transmittanceii = auxiliaries['transmittanceii']
            backscatteriii = auxiliaries['backscatteriii']
            #transmittanceiii = auxiliaries['transmittanceiii']
            estimate = naught + naughtii + (albedo * transmittanceii) / (1 - albedo * backscatterii)
            estimate += (albedo * (transmittanceii / prediction)) / (1 - albedo * backscatteriii)

        # calculate estimate from Tss - Tms
        if feature == 'transcendenceii':

            # caluclate rad: rad = SSo + MSo + RT / ( 1 - RSb )
            naught = 10 ** auxiliaries['naught']
            albedo = auxiliaries['albedo']
            naughtii = 10 ** auxiliaries['naughtii']
            backscatterii = auxiliaries['backscatterii']
            transmittanceii = auxiliaries['transmittanceii']
            backscatteriii = auxiliaries['backscatteriii']
            #transmittanceiii = auxiliaries['transmittanceiii']
            estimate = naught + naughtii + (albedo * transmittanceii) / (1 - albedo * backscatterii)
            estimate += (albedo * (transmittanceii - prediction)) / (1 - albedo * backscatteriii)

        return estimate

    def _reform(self, array, transform, machinery, inputs=True):
        """Reform the original array by undoing the reformation.

        Arguments:
            array: 2-d numpy.array
            transform, str, name of reformation
            size: int, number of pca vectors
            input: boolean, input feature?

        Returns:
            dict of machinery
        """

        # copy array
        reformation = array.copy()

        # retrieve method
        method = transform.strip('_')

        # check for inputs condition
        if (inputs and transform.startswith('_')) or (not inputs and transform.endswith('_')):

            # make scaline
            if method == 'scale':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                reformation = self._unscale(array, median, minimum, maximum)

            # make scaline
            if method == 'grow':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                transformation = self._unscale(array, median, minimum, maximum, bounds=(0, 10))

            # make scaline
            if method == 'shrink':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                reformation = self._unscale(array, median, minimum, maximum, bounds=(-1, 1))

            # make normalization
            if method in ('normalize', 'prenormalize'):

                # determine means and deviations for each row in the array
                mean = machinery['mean']
                deviation = machinery['deviation']
                reformation = self._unnormalize(array, mean, deviation)

            # make pca machine
            if method == 'pca':

                # check for machine
                if 'machine' in machinery.keys():

                    # if size is greater
                    if reformation.shape[1] > 1:

                        # create pca machine
                        machine = machinery['machine']
                        reformation = machine.inverse_transform(array)

            # make boxcox machine
            if method == 'boxcox':

                # get boxcox factors
                factors = machinery['factor']
                reformation = self._uncrate(array, factors)

            # undo polynomial expansion by keeping only the first of each triplicate
            if method == 'expand':

                # constuct every third index
                thirds = [index for index in range(array.shape[1]) if index % 3 == 0]
                reformation = array[:, thirds]

            # make boxcox machine
            if method == 'boost':

                # get boxcox factors
                reformation = array / self.boost

        return reformation

    def _scale(self, array, medians, minimums, maximums, bounds=(0, 1.0)):
        """Scale an array to certain bounds.

        Arguments:
            array: numpy array
            medians: list of float, the medians of each column
            minimums: list of float, the minimums of each column
            maximums: list of float, the maximums of each column
            bounds: tuple of float, minium and maximum bounds

        Returns:
            numpy array
        """

        # expand medians, etc along sample directory
        samples = array.shape[0]
        medians = numpy.array([medians.copy()] * samples)
        minimums = numpy.array([minimums.copy()] * samples)
        maximums = numpy.array([maximums.copy()] * samples)

        # get mask for left tailinng
        left = (medians - minimums) > (maximums - medians)
        right = (medians - minimums) <= (maximums - medians)

        # copy array
        rescale = array.copy()

        # if there is at least some left tailers
        middle = sum(bounds) / 2
        if left.sum() > 0:

            # perform left tailing scale operation
            slope = (middle - bounds[0]) / (medians - minimums)
            rescale[left] = middle + (rescale[left] - medians[left]) * slope[left]

        # if there are are at least some right tialers
        if right.sum() > 0:

            # perform right tailing scale operation
            slope = (bounds[1] - middle) / (maximums - medians)
            rescale[right] = middle + (rescale[right] - medians[right]) * slope[right]

        return rescale

    def _segregate(self, latitudes, longitudes, latitudesii, longitudesii):
        """Segregate pairs of geolocation coordinates into those within or without a polygon.

        Arguments:
            latitudes: list of float, latitude coordinates of sample points in question
            longitudes: list of float, longitude coordinates of sample points in question
            latitudesii: list of float, latitude coordinates of enclosing polygon
            longitudesii: list of floats, longitude coordinates of enclosing polygon.

        Returns:
            ( numpy array, numpy array ) tuple, the coordinates of the points within and without the polygon

        Notes:
            Assumes latitudesii and longitudesii are given in order around the perimeter,
            and do not include a redundant final point.

            Assumes all angles between points are <= 180 degrees
        """

        # start clock
        #self._stamp('segregaring {} points...'.format(len(latitudes)), initial=True)

        # create samples and perimeter as numpy arrays
        #self._stamp('calculating polygon...')
        samples = numpy.array([latitudes, longitudes]).transpose(1, 0)
        perimeter = numpy.array([latitudesii, longitudesii]).transpose(1, 0)

        # duplicate first entry at last position as well
        polygon = numpy.concatenate([perimeter, [perimeter[0]]])

        # create segments from consecutive pairs of points
        segments = [polygon[index: index + 2, :] for index, _ in enumerate(polygon[:-1])]

        # calculate slopes of all segments as delta latitude / delta longitude
        slopes = [(segment[1, 0] - segment[0, 0]) / (segment[1, 1] - segment[0, 1]) for segment in segments]

        # calculate intercepts from first point: b = y0 - m(x0) = lat0 - m(lon0)
        intercepts = [(segment[0, 0] - slope * segment[0, 1]) for segment, slope in zip(segments, slopes)]

        # calculate center point
        center = (latitudesii.mean(), longitudesii.mean())

        # determine polarities (True = up ) for each segment by comparing the midpoint to the center
        zipper = zip(segments, slopes, intercepts)
        polarities = [center[0] > slope * center[1] + intercept for segment, slope, intercept in zipper]

        # create boolean masks for each segment
        #self._stamp('making masks for each side...')
        masks = []
        for segment, slope, intercept, polarity in zip(segments, slopes, intercepts, polarities):

            # if polarity is True
            if polarity:

                # create boolean mask for all points where lat >= m * lon + b
                mask = samples[:, 0] > slope * samples[:, 1] + intercept
                masks.append(mask)

            # otherwise
            else:

                # create boolean mask for all points where  lat <= m * lon + b
                mask = samples[:, 0] < slope * samples[:, 1] + intercept
                masks.append(mask)

        # multiply all masks for polygon mask and make inverse
        masque = numpy.prod(masks, axis=0).astype(bool)
        inverse = numpy.logical_not(masque)

        # split into insiders and outsiders
        #self._stamp('filtering data...')
        insiders = samples[masque]
        outsiders = samples[inverse, :]

        # timestamp
        #self._print('{} insiders, {} outsiders'.format(len(insiders), len(outsiders)))
        #self._stamp('segregated.')

        return insiders, outsiders, center, polygon

    def _space(self, array, increment):
        """Add fill values in between stretches of an array.

        Arguments:
            array: numpy array
            increment: int, number of points in line

        Returns:
            None
        """

        # define fill value
        fill = -9999.0

        # flatten array and convert to list
        stack = array.reshape(-1, increment)

        # begin spaced
        spaced = []
        for row in stack:

            # add row
            spaced += row.tolist()

            # add spacig
            spaced += [fill]

        # create array
        array = numpy.array(spaced)

        print('space:', array.shape)

        return array

    def _state(self, array):
        """Compute mean and std for rows in an array.

        Arguments:
            array

        Returns:
            tuple of list of floats
        """

        # begin means and deviations
        means = array.mean(axis=0)
        deviations = array.std(axis=0)
        deviations = numpy.where(deviations > 0, deviations, 1.0)

        # # transpose the array
        # array = array.transpose(1, 0)
        #
        # # for each row
        # for row in array:
        #
        #     # compute the means and deviations
        #     mean = row.mean()
        #     deviation = row.std() or 1.0
        #
        #     # append
        #     means.append(mean)
        #     deviations.append(deviation)

        return means, deviations

    def _transform(self, array, transform, machinery, inputs=True):
        """Apply the transformation from a machine

        Arguments:
            array: 2-d numpy.array
            transform, str, name of transformation
            size: int, number of pca vectors

        Returns:
            dict of machinery
        """

        # copy array
        transformation = array.copy()

        # retrieve method
        method = transform.strip('_')

        # check for inputs condition
        if (inputs and transform.startswith('_')) or (not inputs and transform.endswith('_')):

            # make scaline
            if method == 'scale':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                transformation = self._scale(array, median, minimum, maximum)

            # make scaline
            if method == 'grow':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                transformation = self._scale(array, median, minimum, maximum, bounds=(0, 10))

            # make scaline
            if method == 'shrink':

                # apply scaling
                median = machinery['median']
                minimum = machinery['minimum']
                maximum = machinery['maximum']
                transformation = self._scale(array, median, minimum, maximum, bounds=(-1, 1))

            # make normalization
            if method in ('normalize', 'prenormalize'):

                # determine means and deviations for each row in the array
                mean = machinery['mean']
                deviation = machinery['deviation']
                transformation = self._normalize(array, mean, deviation)

            # make pca machine
            if method == 'pca':

                # check for machine
                if 'machine' in machinery.keys():

                    # if size is greater
                    if transformation.shape[1] > 1:

                        # create pca machine
                        machine = machinery['machine']
                        transformation = machine.transform(array)

            # make boxcox machine
            if method == 'boxcox':

                # get boxcox factors
                factors = machinery['factor']
                transformation = self._crate(array, factors)

            # construct polynomial expansion
            if method == 'expand':

                # triplicate each row of array with square and cube
                triplicate = []
                for index, row in enumerate(array.transpose(1, 0)):

                    # append row
                    triplicate.append(row)

                    # only for first three pcas
                    if index < 2:

                        # append row squared
                        triplicate.append(row ** 2)

                        # # append row cubed
                        # triplicate.append(row ** 3)

                # tranpose after stacking
                transformation = numpy.vstack(triplicate).transpose(1, 0)

            # make boxcox machine
            if method == 'boost':

                # get boxcox factors
                transformation = array * self.boost

        return transformation

    def _tribulate(self, brackets, augmentation, orbit=48687):
        """Prepare unseen data for testing in the model.

        Arguments:
            brackets: dict of various parameter brackets
            augmentation: dict of data augmentation values
            orbit: int, orbit number for test orbit

        Returns:
            None
        """

        # grab the zeniths
        hydra = Hydra('{}/calculations'.format(self.sink))
        hydra.ingest(0)

        # get zeniths
        zeniths = hydra.grab('SolarZenithAngle')

        # map data fields to shorthands for radiance data
        aliases = {'radiance': 'Radiance_SSMS', 'wavelength': 'Wavelength'}
        aliases.update({'single': 'Radiance_SS', 'multiple': 'Radiance_MS'})
        aliases.update({'albedo': 'SurfaceAlbedo', 'temperature': 'Temperature', 'pressure': 'Pressure'})
        aliases.update({'surface': 'SurfacePressure'})
        aliases.update({'zenith': 'SolarZenithAngle', 'layer': 'LayerOzone'})
        aliases.update({'scatter': 'OpticalDepth_Rayleigh', 'absorption': 'OpticalDepth_Ozone'})
        aliases.update({'altitude': 'Altitude', 'wavelength': 'Wavelength', 'latitude': 'Latitude'})
        aliases.update({'longitude': 'Longitude'})
        aliases.update({'backscatter': 'Backscatter_Sb', 'transmittance': 'Transmittance_T'})
        aliases.update({'naught': 'SS_naught', 'naughtii': 'MS_naught'})
        aliases.update({'backscatterii': 'Backscatter_SS_Sb', 'transmittanceii': 'Transmittance_SS_T'})
        aliases.update({'backscatteriii': 'Backscatter_MS_Sb', 'transmittanceiii': 'Transmittance_MS_T'})

        # add data obejct with second aliases
        data = {}
        data.update({alias: [] for alias in aliases.keys()})

        # grab the radiance data
        hydra = Hydra('{}/orbits/radiance'.format(self.sink))
        paths = [path for path in hydra.paths if str(orbit) in path]

        # for each path
        for path in paths:

            # ingest
            hydra.ingest(path)

            # for each alias
            for alias, field in aliases.items():

                # try to
                try:

                    # add to reservoirw
                    array = hydra.grab(field)
                    data[alias].append(array)

                # unless not found
                except IndexError:

                    # in which case, pass
                    pass

        # vstack all arrays
        data = {alias: arrays for alias, arrays in data.items() if len(arrays) > 0}
        data = {alias: numpy.vstack(arrays) for alias, arrays in data.items()}

        # construct average of radiance across zenith angle
        radiance = data['radiance']
        zenith = data['zenith']

        # grab spline, defaulting to a function that return one
        def identifying(tensor): return 1
        spline = self.spline or identifying

        # grab  main shape
        shape = data['single'].shape

        # add spline field as radiance normalized by spline
        block = numpy.array([zenith] * shape[2])
        block = block.transpose(1, 2, 0)
        data['spline'] = radiance / spline(block)

        # take the log of SS and MS, and extract nadir port
        for field in ('single', 'multiple', 'radiance', 'spline'):

            # extract nadir
            data[field] = data[field][:, 1, :]

            # take log
            data[field] = numpy.log10(data[field])

        # get shape
        shape = data['single'].shape

        # stack albedo by shape
        albedo = data['albedo']
        albedo = numpy.array([albedo] * shape[0])[:, 0, :]
        data['albedo'] = albedo

        # multiple out surface pressure and zenith angle for albedo
        for field in ('surface', 'zenith'):

            # stack by albedos
            array = data[field]
            array = numpy.array([array[:, 1]] * shape[1]).transpose(1, 0)
            data[field] = array

        # create cosine
        data['cosine'] = numpy.cos(data['zenith'] * (math.pi / 180))

        # flatten all data, expand by 1
        for name, array in data.items():

            # flatten and expand
            data[name] = array.flatten()

        # get rid of invalids
        samples = data['single'].shape[0]
        mask = (numpy.isfinite(data['single'])) & (data['zenith'] > 0)
        for name, array in data.items():

            # if shape matches
            if array.shape[0] == samples:

                # apply mask
                data[name] = array[mask]

        # flatten all data, expand by 1
        for name, array in data.items():

            # flatten and expand
            data[name] = array.reshape(-1, 1)

        # sort according to zenith bracket
        bracket = brackets['zeniths']
        #zeniths = data['zeniths']
        flat = data['zenith'].flatten()
        mask = (flat >= zeniths[bracket[0]]) & (flat <= zeniths[bracket[1] - 1])
        samples = data['radiance'].shape[0]

        # use mask on arrays
        for name, array in data.items():

            # if the shape mathches
            if array.shape[0] == samples:

                # apply mask
                data[name] = array[mask]

        return data

    def _unbox(self, row, factor):
        """Undo the box cox transformation

        Arguments:
            row: numpy array

        Returns:
            list of floats, the box cox parameters
        """

        # copy array
        row = row.copy()
        box = row.copy()

        # check for null factor
        if factor is not None:

            # try to
            try:

                # if factor is zero
                if factor == 0:

                    # apply special case for 0 ( log )
                    box = numpy.exp(row)

                # otherwise
                else:

                    # apply general box cox
                    box = ((row * factor) + 1) ** (1 / factor)

            # unless encountering an error:
            except ValueError:

                # in which case, pass
                box = None

        return box

    def _uncrate(self, array, factors):
        """Reverse box cox transformation on each column of the array.

        Arguments:
            array: numpy array
            factors: list of floats, box cox parameters

        Returns:
            numpy.array
        """
        # transpose array
        array = array.copy()
        array = array.transpose(1, 0)

        # apply box cox transforms
        crate = numpy.array([self._unbox(row, factor) for row, factor in zip(array, factors)])

        # retranspose
        crate = crate.transpose(1, 0)

        return crate

    def _understand(self, records, reflectivity, extent, bins=10):
        """Construct the histograms.

        Arguments:
            records: list of list of dicts, the records
            reflectivity: str, reflectivity level
            extent: int, number of records to average
            bins: int, number of bins

        Returns:
            None
        """

        # collect all delta pressures
        pressures = []
        for record in records:

            # average appropriate entries
            pressure = numpy.average([entry['deltas']['pressure ( hPa )'] for entry in record[:extent]])
            pressures.append(pressure)

        # determine 95th and 5th percentile
        lower = numpy.percentile(numpy.array(pressures), 5)
        upper = numpy.percentile(numpy.array(pressures), 95)

        # break into equal pieces of a certain width
        width = (upper - lower) / bins

        # define brackets
        brackets = [(lower + width * index, lower + width * (index + 1)) for index in range(bins)]
        brackets = [(lower - width, lower)] + brackets + [(upper, upper + width)]

        # create middle of each bin
        middles = list([sum(bracket) / 2 for bracket in brackets])

        # get counts for heights
        bounds = brackets
        bounds[0] = (min(pressures), brackets[0][1])
        bounds[1] = (brackets[-1][0], max(pressures))
        boxes = [[pressure for pressure in pressures if bracket[0] <= pressure <= bracket[1]] for bracket in bounds]
        counts = list([len(box) for box in boxes])

        # make labels
        ordinate = 'concurrent pairs'
        abscissa = 'delta Pressure (hPa) NMCLDRR - OMCLDRR'

        # construct title attributes
        pairs = len(records)
        dates = [record[0]['omi']['date'] for record in records]
        dates.sort()
        start = dates[0][:7]
        finish = dates[-1][:7]

        # create bracket descriptions
        descriptions = {'low': 'refl. < 0.3', 'middle': '0.3 < refl. < 0.7', 'high': '0.7 < refl.', 'all': 'all refl.'}

        # make title
        title = 'Cloud Pressure Difference, NMCLDRR - OMCLDRR, {}'.format(descriptions[reflectivity])
        title += '\n ( {} pairs from {} to {}, < 30 sec, 1 km apart, avg of {} )'.format(pairs, start, finish, extent)

        # dump into json
        plot = {'width': width, 'middles': middles, 'counts': counts, 'ordinate': ordinate, 'abscissa': abscissa}
        plot.update({'title': title})
        self._dump(plot, '../studies/albatross/plots/histogram_{}_{}.json'.format(extent, reflectivity))

        # add file for histograms
        route = ['Categories', 'delta_pressure_histogram']
        array = numpy.array(pressures)
        feature = Feature(route, array)
        destination = '../studies/albatross/histograms/histogram_{}_{}.h5'.format(extent, reflectivity)
        self.stash([feature], destination, 'Data')

        return None

    def _unnormalize(self, array, means, deviations):
        """Unnormalize an array from z scores.

        Arguments:
            array: numpy array
            means: list of float, means per row
            deviations; list of float, deviations per row

        Returns:
            numpy array
        """

        # try to
        try:

            # transpose array to get columns as rows
            array = array.transpose(1, 0)

        # unless 1 D
        except ValueError:

            # keep as column
            array = numpy.array([array]).transpose(1, 0)

        # perform normalization
        zipper = zip(array, means, deviations)
        reconstitution = numpy.array([(row * deviation) + mean for row, mean, deviation in zipper])

        # transpose again
        reconstitution = reconstitution.transpose(1, 0)

        return reconstitution

    def _unscale(self, array, medians, minimums, maximums, bounds=(0, 1.0)):
        """Unscale an array from 1.0  median and all positive bounds.

        Arguments:
            array: numpy array
            medians: list of float, the medians of each column
            minimums: list of float, the minimums of each column
            maximums: list of float, the maximums of each column
            bounds: tuple of float, minium and maximum bounds

        Returns:
            numpy array
        """

        # expand medians, etc along sample directory
        samples = array.shape[0]
        medians = numpy.array([medians.copy()] * samples)
        minimums = numpy.array([minimums.copy()] * samples)
        maximums = numpy.array([maximums.copy()] * samples)

        # get mask for left tailinng
        left = (medians - minimums) > (maximums - medians)
        right = (medians - minimums) <= (maximums - medians)

        # copy array
        rescale = array.copy()

        # if there is at least sum left tailers
        middle = sum(bounds) / 2
        if left.sum() > 0:

            # perform left tailing scale operation
            slope = (medians - minimums) / (middle - bounds[0])
            rescale[left] = medians[left] + (rescale[left] - middle) * slope[left]

        # if there are are at least some right tialers
        if right.sum() > 0:

            # perform right tailing scale operation
            slope = (maximums - medians) / (bounds[1] - middle)
            rescale[right] = medians[right] + (rescale[right] - middle) * slope[right]

        return rescale

    def _waver(self, wavelengths, recipe):
        """Retrieve appropriate spectral indices for select wavelength ranges from the recipe

        Arguments:
            wavelengths: list of floats, the wavelengths
            recipe: str, text file of wavelength ranges.

        Returns:
            numpy array of spectral indices
        """

        # set default waves length blocks
        waves = [(wavelengths.min(), wavelengths.max())]

        # if there is a file, construct the wave ranges
        if recipe:

            # transcribe into text
            text = self._know('{}/recipes/{}'.format(self.sink, recipe))

            # only keep lines with numbers
            lines = [line for line in text if line.replace('.', '').replace(' ', '').strip().isdigit()]

            # create wavelength brackets
            waves = [(float(line.strip().split()[0]), float(line.strip().split()[1])) for line in lines]

        print(waves)

        # create boolean mask
        masks = []
        for first, second in waves:

            # create mask
            mask = (wavelengths >= first) & (wavelengths <= second)
            masks.append(mask)

        # add all together
        masque = numpy.vstack(masks).sum(axis=0).astype(bool)

        return masque

    def accumulate(self, year, month, start, stop, sink='surface'):
        """Accumulate surface pressure data from omps.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))

        # create destination of file
        destination = '{}/{}/OMPS_ANC_Surface_Pressures_{}_{}.h5'.format(self.sink, sink, year, self._pad(month))

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date
            squares = hydra.grab('surface_pressure_sum_of_squares')
            totals = hydra.grab('surface_pressure_sum')
            counts = hydra.grab('surface_pressure_sample_count')
            orbits = hydra.grab('orbit_number')

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squares = numpy.zeros((180, 360)).astype('float32')
            totals = numpy.zeros((180, 360)).astype('float32')
            counts = numpy.zeros((180, 360)).astype('int')
            orbits = numpy.array([])

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMPS-NPP/85900/LP-L1-ANC/{}/{}'.format(*formats), start, stop)

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.h5')]
        for path in paths:

            # get orbit
            orbit = int(re.search('o[0-9]{5}', path).group().strip('o'))

            # skip if orbit already made
            if orbit not in orbits:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # ingest the path
                hydra.ingest(path)

                # get other data
                pressures = hydra.grab('SurfacePressure')[:, 1].flatten()
                latitudes = hydra.grab('Latitude')[:, 1].flatten()
                longitudes = hydra.grab('Longitude')[:, 1].flatten()

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # create stack
                stack = numpy.vstack([latitudes, longitudes]).astype(int)
                stack = stack.transpose(1, 0)

                # try to
                try:

                    # for each row
                    for strip, pressure in zip(stack, pressures):

                        # verify no fill
                        if pressure > 0 and numpy.isfinite(pressure):

                            # unpack data
                            latitude, longitude = strip

                            # update
                            totals[latitude, longitude] += pressure
                            squares[latitude, longitude] += pressure ** 2
                            counts[latitude, longitude] += 1

                    # append(orbits)
                    orbits = numpy.array([entry for entry in orbits] + [orbit])
                    orbits.sort()

                    # stamp
                    self._stamp('processed {}.'.format(orbit))

                # unless mismatch
                except ValueError:

                    # stamp
                    self._stamp('error in orbit {}, skipped!'.format(orbit))

        # construct features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared surface_pressure measurements, refl x row x lat x lon'}
        features.append(Feature(['surface_pressure_sum_of_squares'], squares, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of surface_pressure measurements, refl x row x lat x lon'}
        features.append(Feature(['surface_pressure_sum'], totals,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of surface_pressure measurements, refl x row x lat x lon'}
        features.append(Feature(['surface_pressure_sample_count'], counts,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['orbit_number'], orbits, attributes=attributes))

        # stash file
        self.stash(features, destination, compression=9)

        return None

    def annihilate(self):
        """Destroy data reservoirs.

        Arguments:
            None

        Returns:
            None
        """

        # destroy data reservoirs
        for folder in ('rakes', 'reports', 'errors', 'trees', 'losses', 'weights'):

            # destroy all
            self._clean('{}/{}'.format(self.sink, folder))

        # recreate errors file
        errors = {'errors': []}
        self._dump(errors, '{}/errors/errors_{}.json'.format(self.sink, self.tag))

        return None

    def bias(self, examination):
        """Plot loss versus bias to examine local minium.

        Arguments:
            examination: dict of examination data

        Returns:
            None
        """

        # get perceptor, its weights, and biases
        perceptor = examination['storage']['machines']['perceptor']
        weights = perceptor.coefs_
        biases = perceptor.intercepts_

        # grab the input matrix, and the targets
        matrix = examination['matrix']
        target = examination['target']
        result = examination['result']

        # construct hidden activities for each sample ahs = relu(bh + Si ( wih ais ))
        inputs = matrix @ weights[0]
        activities = inputs + biases[0]
        mask = (activities > 0).astype(int)

        # calculate error
        error = result - target

        # multiply in mask and weights
        outputs = 2 * weights[1] @ error.transpose(1, 0)
        derivatives = outputs.transpose(1, 0) * mask
        derivatives = derivatives.sum(axis=0)

        # find maximum absolute derivative
        order = numpy.argsort(abs(derivatives))
        index = order[-1]

        # print derivatives
        print(derivatives.min())
        print(derivatives.max())

        # set up perturbations
        losses = []
        perturbations = [0.99 + 0.001 * index for index in range(21)]
        bias = biases[0][index]
        for perturbation in perturbations:

            # print
            print('perturbation: {}'.format(perturbation))

            # assign bias
            biases[0][index] = bias * perturbation

            # make novel result
            novel = perceptor.predict(matrix)
            loss = (novel - result) ** 2
            loss = loss.sum()
            losses.append(loss)

        # reset bias
        biases[0][index] = bias

        # begin features
        features = []

        # make minimum
        name = 'loss_at_hidden_bias_{}'.format(self._pad(index, 4))
        array = numpy.array(losses)
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # add perturbations
        name = 'perturbations'
        array = numpy.array(perturbations)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash features
        destination = '{}/losses/Local_Minima_vs_Bias.h5'.format(self.sink)
        self.stash(features, destination)

        return None

    def breed(self, heights, pressures):
        """Combine height and pressure files.

        Arguments:
            heights: str, file path for height gridded file
            pressures: str, file path for pressure gridded file

        Returns:
            None
        """

        # grab the pressure grids
        hydra = Hydra(pressures)
        hydra.ingest(0)

        # grab fields
        names = ['OpticalDepth_Ozone', 'OpticalDepth_Rayleigh', 'Pressure']
        aliases = ['OpticalDepth_Ozone_Grid', 'OpticalDepth_Rayleigh_Grid', 'PressureGrid']

        # gather tensors
        functions = []
        functional = lambda tensor: lambda array: tensor
        tensors = [hydra.grab(name) for name in names]
        for tensor in tensors:

            # add tensor
            functions += [functional(tensor)]

        # add altitude for dummy parameter
        names = ['Altitude' for name in names] + ['Altitude']
        aliases = aliases + ['Altitude']
        functions = functions + [lambda array: array]

        # create mimic
        old = '_'
        new = '_'
        tag = 'gridded'
        hydra.mimic([heights], old, new, names, aliases, functions, tag=tag)

        return None

    def compose(self, path, orbit=False, tag='alt'):
        """Add Io, T, Sb decomposition to file.

        Arguments:
            path: file for decomposition
            orbit: boolean, orbital file?
            tag='alt': str, tag for mimic file

        Returns:
            None

        Notes: MS - MSo = RT / ( 1 - R Sb )
        """

        # grab the pressure grids
        hydra = Hydra(path)
        hydra.ingest(0)

        # dig up multiple scattering radiance MS feature
        multiple = hydra.grab('Radiance_MS')
        single = hydra.grab('Radiance_SS')
        stokes = hydra.grab('Radiance_SSMS')

        # begin features
        features = []

        # if an orbital file
        if orbit:

            # get reflectivity differences versus 0 at 0.1 and 1: high = MS(r=1) - MS(r=0)
            naught = single[:, :, 0, :]
            naughtii = multiple[:, :, 0, :]
            low = stokes[:, :, 1, :] - (naught + naughtii)
            high = stokes[:, :, 10, :] - (naught + naughtii)

            # modify for only ss component
            lowii = single[:, :, 1, :] - naught
            highii = single[:, :, 10, :] - naught

            # modify for only ms component
            lowiii = multiple[:, :, 1, :] - naughtii
            highiii = multiple[:, :, 10, :] - naughtii

        # otherwise assume climatology table
        else:

            # get reflectivity differences versus 0 at 0.1 and 1: high = MS(r=1) - MS(r=0)
            naught = single[:, :, 0, :, :]
            naughtii = multiple[:, :, 0, :, :]
            low = stokes[:, :, 1, :, :] - (naught + naughtii)
            high = stokes[:, :, 10, :, :] - (naught + naughtii)

            # modify for only ss component
            lowii = single[:, :, 1, :, :] - naught
            highii = single[:, :, 10, :, :] - naught

            # modify for only ms component
            lowiii = multiple[:, :, 1, :, :] - naughtii
            highiii = multiple[:, :, 10, :, :] - naughtii

        # calculate backscatter: Sb = ( del_high - 10 del_low ) / ( del_high - del_low )
        backscatter = (high - 10 * low) / (high - low)

        # calculate transmittance: T = high ( 1 - Sb )
        transmittance = high * (1 - backscatter)

        # calculate for SS backscatter, transmittance
        backscatterii = (highii - 10 * lowii) / (highii - lowii)
        transmittanceii = highii * (1 - backscatterii)

        # calculate for SS backscatter, transmittance
        backscatteriii = (highiii - 10 * lowiii) / (highiii - lowiii)
        transmittanceiii = highiii * (1 - backscatteriii)

        # add to features
        features.append(Feature(['SS_naught'], naught))
        features.append(Feature(['MS_naught'], naughtii))
        features.append(Feature(['Backscatter_Sb'], backscatter))
        features.append(Feature(['Transmittance_T'], transmittance))
        features.append(Feature(['Backscatter_SS_Sb'], backscatterii))
        features.append(Feature(['Transmittance_SS_T'], transmittanceii))
        features.append(Feature(['Backscatter_MS_Sb'], backscatteriii))
        features.append(Feature(['Transmittance_MS_T'], transmittanceiii))

        # if given a tag, add to destination
        destination = path
        if tag:

            # change destination path
            destination = path.replace('.h5', '_{}.h5'.format(tag))

        # augment new file
        hydra.augment(path, features, destination)

        return None

    def diagnose(self, examination):
        """Diagnose the model errors.

        Arguments:
            examination: dict of results from error analysis

        Returns:
            None
        """

        # get traits
        traits = examination['traits']

        # group traits
        samples = self._group(traits, lambda trait: (trait['latitude'], trait['month']))

        # collect zeniths and albedos
        zeniths = ((examination['data']['zeniths'] * 10000).astype(int) / 10000).tolist()
        albedos = ((examination['data']['albedos'] * 10).astype(int) / 10).tolist()

        # begin indices
        indices = {}

        # create arrays of indices for each sample
        for identifier, members in samples.items():

            # begin with zeroes
            zeros = numpy.zeros((11, 10))
            for member in members:

                # go through each albedo
                albedo = 0
                for index, quantity in enumerate(albedos):

                    # check fro 1% match
                    check = member['albedo'] + 0.5
                    if (quantity * 0.99 < check < quantity * 1.01):

                        # set albedo
                        albedo = index

                # go through each zewnith
                zenith = 0
                for index, quantity in enumerate(zeniths):

                    # check fro 1% match
                    check = member['zenith'] + 40
                    if (quantity * 0.99 < check < quantity * 1.01):

                        # set albedo
                        zenith = index

                # add index to matrix
                zeros[albedo, zenith] = member['index']

            # add to indicex
            indices[identifier] = zeros

        # get target and results
        target = examination['target']
        result = examination['result']

        # begin features
        features = []

        # get the top worst losses
        top = 5
        samples = list(samples.items())
        samples.sort(key=lambda pair: max([member['loss'] for member in pair[1]]), reverse=True)
        worsts = [indices[sample[0]] for sample in samples[:top]]

        # for the top pca components
        for component in range(5):

            # create string
            vector = str(component).zfill(2)

            # create top 5 targets for albedos at mid zenith
            name = 'worst_albedo_targets_{}'.format(vector)
            array = numpy.array([[target[int(index)][component] for index in member[:, 5]] for member in worsts])
            feature = Feature(['Categories', name], array)
            features.append(feature)

            # create top 5 results for albedos at mid zenith
            name = 'worst_albedo_results_{}'.format(vector)
            array = numpy.array([[result[int(index)][component] for index in member[:, 5]] for member in worsts])
            feature = Feature(['Categories', name], array)
            features.append(feature)

            # create top 5 targets for zeniths at mid albedo
            name = 'worst_zenith_targets_{}'.format(vector)
            array = numpy.array([[target[int(index)][component] for index in member[5, :]] for member in worsts])
            feature = Feature(['Categories', name], array)
            features.append(feature)

            # create top 5 results for albedos at mid zenith
            name = 'worst_zenith_results_{}'.format(vector)
            array = numpy.array([[result[int(index)][component] for index in member[5, :]] for member in worsts])
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # add albedos
        name = 'albedo'
        array = numpy.array(albedos)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # add albedos
        name = 'zenith'
        array = numpy.array(zeniths)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash features
        tag = examination.get('tag', 'NN')
        destination = '{}/diagnosis/NN_Diagnosis_{}.h5'.format(self.sink, tag)
        self.stash(features, destination)

        # create radiance pca vectors
        features = []
        machine = examination['storage']['machines']['radiance']['_pca_']['machine']
        wavelengths = examination['data']['wavelength']
        components = machine.components_
        target = examination['target'][0]
        for component in range(5):

            # grab the vector
            vector = components[component]

            # create spread
            name = 'radiance_pca_{}'.format(str(component).zfill(2))
            spread = (0.90, 0.99, 1.0, 1.01, 1.1)
            array = numpy.vstack([vector * entry * target[component] for entry in spread])
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # add wavelength
        name = 'wavelength'
        array = numpy.array(wavelengths)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash features
        tag = examination.get('tag', 'NN')
        destination = '{}/diagnosis/NN_Radiance_PCAS.h5'.format(self.sink)
        self.stash(features, destination)

        # get the top worst percent errors
        top = 10
        traits.sort(key=lambda member: abs(member['error']), reverse=True)
        worsts = traits[:top]
        bests = traits[-top:]

        # get target and results
        target = examination['target']
        result = examination['result']
        percents = examination['percents']
        losses = target - result

        # begin features
        features = []

        # construct worst sample errors along spectra
        name = 'worst_spectra'
        array = numpy.vstack([percents[sample['index']] for sample in worsts])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # construct best sample errors along spectra
        name = 'best_spectra'
        array = numpy.vstack([percents[sample['index']] for sample in bests])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # add wavelength
        name = 'wavelength'
        array = numpy.array(wavelengths)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # construct worst sample losses across pca space
        name = 'worst_losses'
        array = numpy.vstack([losses[sample['index']] for sample in worsts])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # construct best sample errors along spectra
        name = 'best_losses'
        array = numpy.vstack([losses[sample['index']] for sample in bests])
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # add pca space
        name = 'pcas'
        array = numpy.array(list(range(examination['storage']['sizes']['radiance'])))
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash features
        destination = '{}/diagnosis/NN_Spectra.h5'.format(self.sink)
        self.stash(features, destination)

        return None


        # # grab data
        # data = self.strain(recipe=recipe)
        #
        # # open up error file
        # errors = self._load('{}/errors/errors_{}.json'.format(self.sink, self.tag))['errors']
        #
        # # choose example
        # example = list(errors[number].items())
        #
        # # begin arrays
        # tracers = {'albedo': [], 'ozone': [], 'latitude': []}
        # groupers = {'albedo': lambda example: math.floor(int(example[0]) / 153)}
        # groupers.update({'latitude': lambda example: math.floor(int(example[1]['latitude']) / 10)})
        # groupers.update({'ozone': lambda example: math.floor(int(example[1]['latitude']) / 50)})
        #
        # # create brackets
        # brackets = {}
        # brackets['albedo'] = [((index - 0.5) / 10, (index + 0.5) / 10) for index in range(11)]
        # brackets['latitude'] = [(-90 + index * 20, -70 + index * 20) for index in range(9)]
        # brackets['ozone'] = [(250 + index * 50, 300 + index * 50) for index in range(5)]
        #
        # # create labels
        # labels = {}
        # labels['albedo'] = ['albedo {}'.format(round(float(index / 10), 1)) for index in range(11)]
        # labels['latitude'] = ['{} - {} N'.format(first, last) for first, last in brackets['latitude']]
        # labels['ozone'] = ['{} - {} DU'.format(first, last) for first, last in brackets['ozone']]
        #
        # # got through each tracer
        # features = []
        # for trace in tracers.keys():
        #
        #     # begin spectra
        #     spectra = []
        #     wavelengths = []
        #
        #     # group the samples by their albedo
        #     samples = self._group(example, groupers[trace])
        #     for signifier, members in samples.items():
        #
        #         # for each member up to choosen size
        #         for member in members[:size]:
        #
        #             # add the percent
        #             spectra.append(member[1]['spectrum'])
        #             wavelengths.append(data['wavelength'])
        #
        #             # add tracer
        #             row = [member[1][trace]] * len(data['wavelength'])
        #             tracers[trace].append(row)
        #
        #     # flatten arrays
        #     spectra = numpy.array(spectra).flatten()
        #     wavelengths = numpy.array(wavelengths).flatten()
        #     tracers[trace] = numpy.array(tracers[trace]).flatten()
        #
        #     # create heatmap
        #     array = numpy.vstack([tracers[trace], spectra, wavelengths])
        #     name = 'heatmap_percent_error_{}'.format(trace)
        #     attributes = {'brackets': brackets[trace], 'labels': labels[trace]}
        #     feature = Feature(['Categories', name], array, attributes=attributes)
        #     features.append(feature)
        #
        # # add histogram
        # array = numpy.vstack([numpy.array(sample[1]['spectrum']) for sample in example]).flatten()
        # name = 'histogram_percent_error'
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        #
        # # stash file
        # formats = (self.sink, self.tag)
        # destination = '{}/errors/Perceptor_Errors_Spectra_{}.h5'.format(*formats)
        # self.stash(features, destination, 'Data')

        return None

    def dissect(self, model, samples=None, tag='inactive'):
        """Make a weights diagram for the model.

        Arguments:
            model: storage object
            sample: list of numpy array, an incoming sample

        Returns:
            None
        """

        # set default samples
        samples = samples or []

        # get the model weights
        weights = model['machines']['perceptor'].coefs_

        # begin reservoirs
        horizontals = []
        horizontalsii = []
        verticals = []
        verticalsii = []
        labels = []
        brackets = []
        pointer = 0

        # construct percenitle margins
        margins = [(0.990, 0.995), (0.995, 0.999), (0.999, 1.0)]
        for lower, upper in margins:

            # for each layer
            for index, layer in enumerate(weights):

                # calculate weight boundaries in percentiles
                small = numpy.quantile(abs(layer), lower)
                large = numpy.quantile(abs(layer), upper)

                # grab the shape, the flatten
                shape = layer.shape
                flat = layer.flatten()

                # go through each weight
                for number, weight in enumerate(flat):

                    # check for inclusion
                    if small <= weight <= large:

                        # calculate horizontoal coordinates ( layer number )
                        horizontal = index
                        horizontalii = index + 1

                        # calculate vertical coordinate
                        vertical = math.floor(number / shape[1]) - shape[0] / 2
                        verticalii = number % shape[1] - shape[1] / 2

                        # add to points
                        horizontals.append(horizontal)
                        horizontalsii.append(horizontalii)
                        verticals.append(vertical)
                        verticalsii.append(verticalii)

            # update labels, pointer, and bracket
            labels.append('{} to {} quantile'.format(lower, upper))
            final = pointer + len(horizontals) - pointer
            brackets.append((pointer, final))
            pointer = final

        # add singlets for all inputs
        inputs = weights[0].shape[0]
        horizontals += [0 for _ in range(inputs)]
        horizontalsii += [0 for _ in range(inputs)]
        verticals += [index - inputs / 2 for index in range(inputs)]
        verticalsii += [index - inputs / 2 for index in range(inputs)]
        final = pointer + len(horizontals) - pointer
        brackets.append((pointer, final))
        labels.append('inputs')
        pointer = final

        # for each hidden layer
        for number, layer in enumerate(weights[:-1]):

            # add hiddens
            hiddens = layer.shape[1]
            horizontals += [number + 1 for _ in range(hiddens)]
            horizontalsii += [number + 1 for _ in range(hiddens)]
            verticals += [index - hiddens / 2 for index in range(hiddens)]
            verticalsii += [index - hiddens / 2 for index in range(hiddens)]

        # add layer
        final = pointer + len(horizontals) - pointer
        labels.append('hiddens')
        brackets.append((pointer, final))
        pointer = final

        # add wavelengths
        for component in range(3):

            # add weights for radiance pca
            outputs = weights[-1].shape[1]
            wavelengths = 143
            horizontals += [len(weights) for _ in range(wavelengths)]
            horizontalsii += [len(weights) + 1 for _ in range(wavelengths)]
            verticals += [component - outputs / 2 for _ in range(wavelengths)]
            verticalsii += [index - wavelengths / 2 for index in range(wavelengths)]
            final = pointer + len(horizontals) - pointer

        # add brackets
        labels.append('wavelengths')
        brackets.append((pointer, final))
        pointer = final

        # add normalization step
        inputs = weights[0].shape[0]
        horizontals += [-1 for _ in range(inputs)]
        horizontalsii += [0 for _ in range(inputs)]
        verticals += [index - inputs / 2 for index in range(inputs)]
        verticalsii += [index - inputs / 2 for index in range(inputs)]
        final = pointer + len(horizontals) - pointer

        # add brackets
        labels.append('normalization')
        brackets.append((pointer, final))
        pointer = final

        # create sizes for each features
        vectors = {'layer': 82, 'column': 20, 'short': 20, 'long': 20, 'zenith': 1, 'square': 1, 'cube': 1}
        vectors.update({'secant': 1, 'albedo': 1, 'brightness': 1, 'luminosity': 1})

        # get pca sizes
        names = model['names']
        sizes = {name.split('_#')[0]: int(name.split('_#')[1]) + 1 for name in names}

        # calculate total vector length
        vector = sum(vectors.values())
        size = sum(sizes.values())
        position = 0
        positionii = 0
        old = names[0].split('_#')[0]
        for index, name in enumerate(names):

            # get stub and number
            stub, number = name.split('_#')
            number = int(number)
            new = stub
            if number < 3:

                # add weights for radiance pca
                horizontals += [-2 for _ in range(vectors[stub])]
                horizontalsii += [-1 for _ in range(vectors[stub])]
                verticals += [(position + increment) - vector / 2 for increment in range(vectors[stub])]
                verticalsii += [(positionii + number) - size / 2 for _ in range(vectors[stub])]
                final = pointer + len(horizontals) - pointer

            # ade to position
            if old != new:

                # increment position
                position += vectors[old]
                positionii += sizes[old]
                old = new

        # add brackets
        labels.append('raw')
        brackets.append((pointer, final))
        pointer = final

        # # add normalization
        # for component in range(3):
        #
        # # add singlets for all outputs
        # outputs = weights[-1].shape[1]
        # wavelengths = 143
        # horizontals += [len(weights) for _ in range(wavelengths)]
        # horizontalsii += [len(weights) + 1 for _ in range(wavelengths)]
        # verticals += [component - outputs / 2 for _ in range(wavelengths)]
        # verticalsii += [index - wavelengths / 2 for index in range(wavelengths)]
        # final = pointer + len(horizontals) - pointer
        # labels.append('wavelengths')
        # brackets.append((pointer, final))
        # pointer = final

        # add singlets for all outputs
        outputs = weights[-1].shape[1]
        horizontals += [len(weights) for _ in range(outputs)]
        horizontalsii += [len(weights) for _ in range(outputs)]
        verticals += [index - outputs / 2 for index in range(outputs)]
        verticalsii += [index - outputs / 2 for index in range(outputs)]
        final = pointer + len(horizontals) - pointer
        labels.append('outputs')
        brackets.append((pointer, final))
        pointer = final

        # begin features
        features = []

        # create feature
        name = 'network_inactive'
        array = numpy.vstack([verticals.copy(), verticalsii.copy(), horizontals.copy(), horizontalsii.copy()])
        attributes = {'labels': labels, 'brackets': brackets}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # for each given sample
        for identity, sample in enumerate(samples):

            # and get the state
            state = self._activate(model, sample)

            # begin record of top weights
            tops = []

            # for each layer
            for number, layer in enumerate(state):

                # get the top five percent
                margin = numpy.quantile(abs(layer), 0.98)
                indices = [index for index, activity in enumerate(layer) if abs(activity) > margin]
                tops.append(indices)

            # go through each pair
            pairs = list(zip(tops[:-1], tops[1:]))
            for number, (outward, inward) in enumerate(pairs):

                # for each outward
                for outer in outward:

                    # and each inner
                    for inner in inward:

                        # create segments
                        horizontal = number
                        horizontalii = number + 1

                        # calculate vertical coordinate
                        vertical = outer - len(state[number]) / 2
                        verticalii = inner - len(state[number + 1]) / 2

                        # add to points
                        horizontals.append(horizontal)
                        horizontalsii.append(horizontalii)
                        verticals.append(vertical)
                        verticalsii.append(verticalii)

            # create feature
            name = 'network_active_{}'.format(identity)
            array = numpy.vstack([verticals, verticalsii, horizontals, horizontalsii])
            labels.append('activity')
            brackets.append((pointer, len(horizontals)))
            attributes = {'labels': labels, 'brackets': brackets}
            feature = Feature(['Categories', name], array, attributes=attributes)
            features.append(feature)

        # stash features
        destination = '{}/networks/Network_MLP_climatology_network_{}.h5'.format(self.sink, tag)
        self.stash(features, destination)

        return None

    def establish(self, prior=None):
        """Establish project structure.

        Arguments:
            prior: str, prior sink

        Returns:
            None
        """

        # create structure
        self._make(self.sink)

        # make subfolders
        folders = ['yams', 'slides', 'calculations', 'orbits']
        folders += ['orbits/radiance', 'orbits/ozone', 'calculations', 'recipes']
        folders += ['vectors', 'validations', 'errors', 'reports', 'scrutiny']
        folders += ['experiments', 'trouble', 'notes', 'globe', 'albedos', 'overlap']
        folders += ['align']

        # for each folder
        for folder in folders:

            # craete directory
            self._make('{}/{}'.format(self.sink, folder))

        # if a prior sink is given
        if prior:

            # populate various folders
            folders = ['calculations', 'orbits/radiance', 'recipes']
            for folder in folders:

                # get all calcuation paths
                members = self._see('{}/{}'.format(prior, folder))
                for member in members:

                    # copy the file
                    self._copy(member, member.replace(prior, self.sink))

        return None

    def examine(self, year, month, day, number=0, model=None, data=None):
        """Diagnose the model errors.

        Arguments:
            year: int, hear
            month: int, month
            day: int, day
            number: number of training example
            model: previously retrieved storage
            data: dataset, piped in from elsewhere

        Returns:
            None
        """

        # load model from storage
        storage = self.forage(year, month, day, number=number)

        # override with model
        if model:

            # reset storage
            storage = model

        # grab data from files and filter out fill values
        self._stamp('gathering and filtering data...', initial=True)
        brackets = storage['brackets']
        augmentation = storage['augmentation']
        data = data or self.sift(brackets, augmentation)
        #self.boost = data['boost']

        # generate train, test split
        fraction = storage.get('fraction', 0.9)
        memory = storage['memory']
        train, test, memory = self.peck(data, fraction, memory, shuffle=True)

        # vectorize the training data
        features = storage['features']
        sizes = storage['sizes']
        machines = storage['machines']
        transforms = storage['transforms']
        matrix, target, original, names, machines = self.vectorize(test, features, sizes, transforms, machines)

        # add targeta
        if len(target.shape) < 2:

            # reshpae
            target = target.reshape(-1, 1)

        # get mlp
        perceptor = storage['machines']['perceptor']

        # or get tensor flow
        if storage.get('flow', False):

            # load perceptor
            perceptor = load_model(storage['model'])

        # get predictions from perceptor
        prediction = perceptor.predict(matrix)

        # add extra dimention if needed
        if len(prediction.shape) < 2:

            # reshpae
            prediction = prediction.reshape(-1, 1)

        # copy as result
        result = prediction.copy()

        # undo transforms
        output = features[-1]
        for transform in transforms[::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines[output][transform], inputs=False)

        # calculate score
        score = r2_score(original, prediction)
        self._print('total R^2 score: {}'.format(score))

        # reconstitute prediction
        estimate = self._reconstitute(prediction, features[-1], test)
        truth = self._reconstitute(original, features[-1], test)

        # # calculate precent diffence
        # if features[-1] == 'multiple':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** prediction
        #     truth = 10 ** original
        #
        # # calculate precent from ratio
        # if features[-1] == 'ratio':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (test['single'] / prediction)
        #     truth = 10 ** (test['single'] / original)
        #
        # # calculate precent from ratio
        # if features[-1] == 'difference':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (test['single'] - prediction)
        #     truth = 10 ** (test['single'] - original)

        # calculate absolue
        percents = 100 * ((estimate / truth) - 1)

        # gather up sample traits, along with percentages
        loss = target - result
        # traits = self._assemble(train, loss.copy(), percents.copy())
        #
        # # sort traits for rankings
        # ranks = traits.copy()
        # ranks.sort(key=lambda entry: entry['error'], reverse=True)

        # # begin report
        # report = ['Model Errors']
        # [report.append('{}) {}'.format(index, trait)) for index, trait in enumerate(ranks)]
        # destination = '{}/errors/Errors_MLP_climatology.txt'.format(self.sink)
        # self._jot(report, destination)

        # plot loss curve
        curve = storage.get('curve', [0.0])
        epochs = [index for index, _ in enumerate(curve)]
        address = 'errors/Loss_curve.h5'
        self.squid.ink('loss_curve', curve, 'epoch', epochs, address, 'Loss Curve')

        # plot val_Loss curve
        curveii = storage.get('curveii', [0.0])
        epochs = [index for index, _ in enumerate(curveii)]
        address = 'errors/Loss_curve_val.h5'
        self.squid.ink('loss_curve_val', curveii, 'epoch', epochs, address, 'Val Loss Curve')

        # go through each curve
        curves = {'median': [storage.get('medians', [])], 'maximum': [storage.get('maximums', [])], 'mean': [[]]}
        curves = storage.get('curves', curves)

        # go through each validation set
        indices = [index for index, _ in enumerate(curves['median'])]
        for member, median, maximum in zip(indices, curves['median'], curves['maximum']):

            # plot orbital medians
            epochs = [index * 20 for index, _ in enumerate(median)]
            address = 'errors/Loss_curve_med_{}.h5'.format(self._pad(member))
            self.squid.ink('loss_curve_orbital_median', median, 'epoch', epochs, address, 'Median Loss Curve')

            # plot orbital maximums
            epochs = [index * 20 for index, _ in enumerate(maximum)]
            address = 'errors/Loss_curve_max_{}.h5'.format(self._pad(member))
            self.squid.ink('loss_curve_orbital_maximum', maximum, 'epoch', epochs, address, 'Max Loss Curve')

        # plot rate schedule
        cycle = storage.get('cycle', [0.0] * 100)
        abscissa = numpy.linspace(0, len(cycle) - 1, len(cycle))
        self.squid.ink('learning_rate', cycle, 'epoch', abscissa, 'errors/Rate_Schedule.h5', 'Rate schedule')

        # set up scatters for top 100 ranks, vs error
        scatters = [('loss', 5), ('wavelength', 6), ('wave', 4), ('albedo', 2)]
        scatters += [('zenith', 3), ('month', 1), ('latitude', 0)]

        # create offsets
        offsets = {'zenith': 0, 'albedo': 0}

        # # make scatter features
        # for label, index in scatters:
        #
        #     # begin features
        #     features = []
        #
        #     # create feature
        #     name = '{}_vs_error'.format(label)
        #     array = numpy.array([rank['error'] for rank in ranks[:1000]])
        #     feature = Feature(['Categories', name], array)
        #     features.append(feature)
        #
        #     # add absicssas
        #     name = 'abscissa_error_{}'.format(label)
        #     array = numpy.array([rank[label] + offsets.get(label, 0) for rank in ranks[:1000]])
        #     feature = Feature(['IndependentVariables', name], array)
        #     features.append(feature)
        #
        #     # make scatter plots, error vs loss
        #     destination = '{}/errors/Errors_MLP_climatology_scatter_{}.h5'.format(self.sink, label)
        #     self.stash(features, destination)

        # set up scatters for top 100 ranks, vs error
        scatters = [('error', 7), ('wavelength', 6), ('wave', 4), ('albedo', 2)]
        scatters += [('zenith', 3), ('month', 1), ('latitude', 0)]

        # # make scatter features
        # for label, index in scatters:
        #
        #     # begin features
        #     features = []
        #
        #     # create feature
        #     name = '{}_vs_loss'.format(label)
        #     array = numpy.array([rank['loss'] for rank in ranks[:1000]])
        #     feature = Feature(['Categories', name], array)
        #     features.append(feature)
        #
        #     # add absicssas
        #     name = 'abscissa_loss_{}'.format(label)
        #     array = numpy.array([rank[label] + offsets.get(label, 0) for rank in ranks[:1000]])
        #     feature = Feature(['IndependentVariables', name], array)
        #     features.append(feature)
        #
        #     # make scatter plots, error vs loss
        #     destination = '{}/errors/Errors_MLP_climatology_loss_{}.h5'.format(self.sink, label)
        #     self.stash(features, destination)

        # make error histogram
        features = []
        name = 'histogram_percent_errors'
        array = percents.flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # stash features
        destination = '{}/errors/Errors_MLP_climatology_histogram.h5'.format(self.sink)
        self.stash(features, destination)

        # get worst errors
        features = []
        # worsts = numpy.array([prediction[rank['index']] for rank in ranks[:10]])
        # truths = numpy.array([original[rank['index']] for rank in ranks[:10]])

        # name = 'worst_single_predictions'
        # array = numpy.array(worsts)
        # feature = Feature(['Categories', name], array)
        # features.append(feature)

        # # get worst originals
        # name = 'worst_single_originals'
        # array = numpy.array(truths)
        # feature = Feature(['Categories', name], array)
        # features.append(feature)

        # # add wavelegnth
        # name = 'wavelength'
        # array = numpy.array([data['wavelength'][index] for index in range(*brackets['rays'])])
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        #
        # # stash features
        # destination = '{}/errors/Errors_MLP_climatology_predict.h5'.format(self.sink)
        # self.stash(features, destination)

        # get the worst percent errors vs pca loss
        features = []
        absolutes = abs(percents).max(axis=1)
        indices = numpy.argsort(absolutes)
        error = percents[indices[-1000:]]
        absolute = abs(error).max(axis=1)
        losses = loss[indices[-1000:]].transpose(1, 0)

        # make features
        name = 'pca_vectors_losses'
        array = losses
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # make independent
        name = 'worst_percent_errors'
        array = absolute
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash file
        destination = '{}/vectors/Losses_vs_Errors_PCA.h5'.format(self.sink)
        self.stash(features, destination)

        # construct examination
        traits = None
        examination = {'storage': storage, 'data': data, 'matrix': matrix, 'target': target, 'train': train}
        examination.update({'original': original, 'prediction': prediction, 'percents': percents, 'result': result})
        examination.update({'traits': traits, 'loss': loss, 'tag': 'verification', 'test': test})

        # perform validation
        examination['validations'] = []
        orbits = (48687, 48688, 50006)
        for orbit in orbits:

            # perform validations
            validation = self.validate(year, month, day, number, exam=examination, orbit=orbit)
            examination['validations'].append(validation)

        # add validations to storgae
        zipper = zip(orbits, examination['validations'])
        storage['validations'] = {orbit: [validation['median'], validation['maximum']] for orbit, validation in zipper}
        pickle.dump(storage, open(storage['destination'], 'wb'))

        # print scores
        self._print('training:')
        self._print('median absolute percent error: {}'.format(storage['percent']))

        # print max error
        self._print('maximum absolute percent error: {}'.format(storage['maximum']))

        # calculate score
        self._print('training R^2 score: {}'.format(storage['verification']))

        # print scores
        self._print('testing:')
        self._print('median absolute percent error: {}'.format(storage['percentii']))

        # print max error
        self._print('maximum absolute percent error: {}'.format(storage['maximumii']))

        # calculate score
        self._print('training R^2 score: {}'.format(storage['validation']))

        # print validation
        self._print('validation:')
        for orbit, validation in zip(orbits, examination['validations']):

            # print score
            print('orbit: {}...'.format(orbit))

            # print scores
            self._print('median absolute percent error: {}'.format(validation['median']))

            # print max error
            self._print('maximum absolute percent error: {}'.format(validation['maximum']))

        return examination

    def fish(self, year, month, start, finish, criterion=0.1, matches=1):
        """Find the closest latitude, longitude, and time for ozone records and omi uv records.

        Arguments:
            year: int, year
            month: int, month
            start: int, starting day
            finish: int, ending day

        Returns:
            None
        """

        # set spatial extents in rows ( to avoid the row anomaly )
        extents = {'omi': (4, 21), 'merra': (0, 3)}

        # set folder destinations for omi OMCLDRR and merra NMCLDRR
        folders = {'omi': '/tis/OMI/10004/OML1BRUG', 'merra': '/tis/OMPS-NPP/85900/LP-L1-ANC'}

        # go through several days
        for day in range(int(start), int(finish) + 1):

            # times tamp
            self._stamp('hunting {}-{}-{}...'.format(year, str(month).zfill(2), str(day).zfill(2)), initial=True)

            # begin sample and pairs reservoir
            reservoir = []
            sensors = ('omi', 'merra')
            samples = {'omi': [], 'merra': []}

            # for each sensor
            for sensor in sensors:

                # print status
                self._stamp('{}...'.format(sensor))

                # make hydra instance to retrieve omi samples
                hydra = Hydra('{}/{}/{}/{}'.format(folders[sensor], year, str(month).zfill(2), str(day).zfill(2)))

                # for each path
                for path in hydra.paths:

                    # pirnnt path
                    self._stamp('{}...'.format(path))

                    # try to
                    try:

                        # ingest
                        hydra.ingest(path)

                        # if it is omi
                        if sensor == 'omi':

                            # construct times from delta times, and convert from seconds to hours
                            beginning = hydra.grab('time')
                            deltas = hydra.grab('delta_time').squeeze() / 1000
                            times = (deltas + beginning) / 3600

                            # get latitudes and longitudes
                            latitudes = hydra.grab('latitude').squeeze()
                            longitudes = hydra.grab('longitude').squeeze()

                        # otherwise, if it is merra
                        if sensor == 'merra':

                            # set epoch at 2010=01-01
                            epoch = datetime.datetime(2010, 1, 1).timestamp()
                            beginning = datetime.datetime(year, month, day).timestamp() - epoch
                            deltas = hydra.grab('Time').squeeze()
                            times = (deltas + beginning) / 3600

                            # get latitudes and longitudes
                            latitudes = hydra.grab('Latitude').squeeze()
                            longitudes = hydra.grab('Longitude').squeeze()

                        # get date from file
                        date = self._stage(path)['date']

                        # create samples for each timestep
                        for track, time in enumerate(times):

                            # amd for each row in defined extent
                            for row in range(*extents[sensor]):

                                # begin sample
                                sample = {'path': path, 'date': date, 'row': row, 'track': track, 'time': float(time)}

                                # retrieve points at neighboring diagonal corners
                                latitude = latitudes[track][row]
                                longitude = longitudes[track][row]
                                sample.update({'latitude': float(latitude), 'longitude': float(longitude)})

                                # create vector
                                sample['vector'] = (sample['time'], sample['latitude'], sample['longitude'])

                                # add to reservoir
                                samples[sensor].append(sample)

                    # unless can't find
                    except IndexError:

                        # skip
                        self._print('uhoh! ...{}'.format(path))
                        pass

            # create tree from omi locations
            self._stamp('making tree...')
            matrix = numpy.array([sample['vector'] for sample in samples['omi']])
            tree = KDTree(matrix)

            # get the distance to second location
            self._stamp('asking tree...')
            matrixii = numpy.array([sample['vector'] for sample in samples['merra']])
            metrics, references = tree.query(matrixii, k=matches)

            # create records
            self._stamp('organizing locations...')
            for indexii, (distances, indices) in enumerate(zip(metrics, references)):

                # construct pairs for each reference
                pairs = []
                for distance, index in zip(distances, indices):

                    # get pair of samples
                    sample = samples['omi'][index]
                    sampleii = samples['merra'][indexii]

                    # begin sample pair record
                    pair = {'metric': distance, 'omi': sample, 'merra': sampleii, 'deltas': {}}

                    # add delta time, converting from hours to seconds
                    pair['deltas']['time ( sec )'] = (sampleii['time'] - sample['time']) * 3600

                    # add latitude and longitude in km (by multiplying by 111 km / deg (eq)
                    pair['deltas']['latitude ( km )'] = (sampleii['latitude'] - sample['latitude']) * 111
                    pair['deltas']['longitude ( km )'] = (sampleii['longitude'] - sample['longitude']) * 111

                    # # add difference in measurements
                    # pair['deltas']['pressure ( hPa )'] = (sampleii['pressure'] - sample['pressure'])
                    #
                    # # add pixel area ratio
                    # pair['ratio'] = sampleii['area'] / sample['area']

                    # add record
                    pairs.append(pair)

                # append to reservoir
                reservoir.append(pairs)

            # sort by distance
            reservoir.sort(key=lambda record: record[0]['metric'])
            self.reservoir = reservoir
            for record in reservoir[:5]:

                # print
                self._look(record, 2)

            # prune reservoir by criterion
            reservoir = [record for record in reservoir if record[0]['metric'] <= criterion]

            # add new reservoir entries
            name = '../studies/albatross/records/reservoir_{}m{}.json'.format(year, str(month).zfill(2))
            stash = self._load(name) or []

            # add to the reservor
            accumulation = stash + reservoir

            # remove duplicates with dictionary
            prune = {tuple(record[0]['omi']['vector'] + record[0]['merra']['vector']): record for record in accumulation}
            records = list(prune.values())
            records.sort(key=lambda record: record[0]['metric'])

            # dump records
            self._dump(records, name)

        return None

    def flow(self, parameters, matrix, target, flow, validations):
        """Train the tensor flow model instead.

        Arguments:
            parameters: paramters dict
            matrix: training data matrix
            target: target
            flow: dict of tensorflow options
            validations: test, truth pairs

        Returns:
            tensorflow model
        """

        # status
        self._print('building...')

        # get convolution and dropout setting
        convolution = flow.get('convolution', False)
        dropout = flow.get('dropout', 0.0)
        epochs = flow.get('epochs', 20)
        linear = flow.get('linear', True)

        # Initialize the Model
        model = Sequential()

        # make activations dictionary
        activations = {'logistic': 'sigmoid', 'tanh': 'tanh', 'relu': 'relu'}
        activations.update({'leaky': tensorflow.keras.layers.LeakyReLU(alpha=0.05)})

        # if running with CNN
        if convolution:

            # add CNN layers
            convolution = {'kernel_size': 3, 'activation': activations['leaky']}
            convolution.update({'input_shape': (matrix.shape[1], 1,), 'padding': 'same'})
            model.add(Reshape((-1, 1)))
            model.add(Conv1D(200, **convolution))
            model.add(Flatten())

        # add layers
        layers = parameters['hidden_layer_sizes']
        functions = [activations[entry] for entry in parameters['activation'].split(':')]
        for layer, activation in zip(layers, functions):

            # add model layers
            model.add(Dense(layer, activation=activation))
            model.add(Dropout(dropout))

        # add final layer
        if linear:

            # add linear final layer
            model.add(Dense(target.shape[1], activation='linear'))

        # otherwise
        else:

            # add activation layer
            model.add(Dense(target.shape[1], activation=functions[-1]))

        # compile
        model.build(input_shape=(None, matrix.shape[1]))
        details = {'learning_rate': parameters['learning_rate_init']}
        details.update({'epsilon': parameters['epsilon']})
        optimizer = optimizers.Adam(**details)
        model.compile(loss="mse", optimizer=optimizer, metrics=['mse', 'acc'])

        # summarize and set attribute
        model.summary()

        # begin chronicle for recording weights
        chronicle = []

        # partition epochs
        partition = int(epochs / 2)

        # set fast and slow learning rates
        fast = details['learning_rate']
        slow = fast / 10

        # and each epoch, only verbose on last
        length = len(validations)
        verbosity = [True] * (epochs - 1) + [True]
        rates = []
        curve = []
        curveii = []
        curves = {'mean': [[] for _ in range(length)]}
        curves.update({'median': [[] for _ in range(length)]})
        curves.update({'maximum': [[] for _ in range(length)]})
        for epoch in range(partition):

            # calculate learning rate
            decay = -1 * math.log(0.01) / partition
            wave = 10
            amplitude = math.exp(-decay * epoch) * fast
            rate = slow + amplitude * math.sin(epoch * 2 * math.pi / wave) ** 2
            rates.append(rate)

            # grab weights
            weights = model.get_weights()

            # recompile at rate
            details.update({'learning_rate': rate})
            model.compile(loss="mse", optimizer=optimizer, metrics=['mse', 'acc'])

            # reset weights
            model.set_weights(weights)

            # take timepoint
            batch = parameters['batch_size']
            history = model.fit(matrix, target, epochs=20, verbose=verbosity[epoch], batch_size=batch, validation_split=0.1)
            self._stamp('trained epoch {} of {}, rate:{}...'.format(epoch, epochs, rate))

            # add history to curve
            curve += history.history['loss']
            curveii += history.history['val_loss']

            # predict on orbits
            count = 0
            for test, truth in validations:

                # predict on curves
                prediction = model.predict(test)
                loss = (prediction - truth) ** 2

                # add stats
                curves['median'][count].append(numpy.percentile(loss, 50))
                curves['mean'][count].append(loss.mean())
                curves['maximum'][count].append(loss.max())

                # increment count
                count += 1

            # weight scores to chronicle
            chronicle.append((epoch, model.get_weights(), curves['median'][1][-1], curves['maximum'][1][-1]))

        # sort chronicle by lowest product score
        chronicle.sort(key=lambda entry: entry[2] * entry[3])

        # recompile with Adagrad
        #details.update({'nesterov': parameters['nesterovs_momentum'], 'momentum': parameters['momentum']})
        #details.update({'learning_rate': details['learning_rate'] * 50})
        details.update({'learning_rate': 0.01})
        details.update({'initial_accumulator_value': 0.1})
        details.update({'epsilon': 1e-7})
        parameters['batch_size'] = 512
        optimizerii = optimizers.Adagrad(**details)
        model.compile(loss="mse", optimizer=optimizerii, metrics=['mse', 'acc'])
        #
        # # # recompilewith RMSProp
        # # details.update({'learning_rate': 0.001})
        # # details.update({'rho': 0.9, 'momentum': 0.0, 'centered': False})
        # # details.update({'epsilon': 1e-7})
        # # parameters['batch_size'] = 512
        # # optimizerii = optimizers.RMSprop(**details)
        # # model.compile(loss="mse", optimizer=optimizerii, metrics=['mse', 'acc'])
        #
        # reinstate weights
        model.set_weights(chronicle[0][1])
        model.last = chronicle[0][0] * 20

        # run again under new optimizer
        for epoch in range(partition):

            # take timepoint
            batch = parameters['batch_size']
            history = model.fit(matrix, target, epochs=20, verbose=verbosity[epoch], batch_size=batch, validation_split=0.1)
            self._stamp('trained epoch {} of {}...'.format(epoch + partition, epochs))

            # add history to curve
            curve += history.history['loss']
            curveii += history.history['val_loss']

            # predict on orbits
            count = 0
            for test, truth in validations:

                # predict on curves
                prediction = model.predict(test)
                loss = (prediction - truth) ** 2

                # add stats
                curves['median'][count].append(numpy.percentile(loss, 50))
                curves['mean'][count].append(loss.mean())
                curves['maximum'][count].append(loss.max())

                # increment count
                count += 1

            # weight scores to chronicle
            chronicle.append((epoch + partition, model.get_weights(), curves['median'][1][-1], curves['maximum'][1][-1]))

        # sort chronicle by lowest product score
        chronicle.sort(key=lambda entry: entry[2] * entry[3])

        # reinstate weights
        model.set_weights(chronicle[0][1])
        model.last = chronicle[0][0] * 20

        # check the score
        report = []
        report.append(self._print('verifying {} training samples..'.format(len(matrix))))
        predictions = model.predict(matrix)
        score = r2_score(target, predictions)
        report.append(self._print('training sample R^2: {}'.format(score)))
        report.append(self._print(' '))

        # summaryize
        model.summary()

        # add curve
        model.curve = curve
        model.curveii = curveii
        model.curves = curves
        model.cycle = rates

        return model

    def forage(self, year, month, day, number=0):
        """Retrieve a model.

        Arguments:
            year: int, hear
            month: int, month
            day: int, day
            number: number of training example

        Returns:
            None
        """

        # make errors folder
        self._make('{}/errors'.format(self.sink))

        # get all models
        models = self._see('{}/machines'.format(self.sink))
        models.sort(reverse=True)

        # subset date
        query = '{}m{}{}'.format(year, str(month).zfill(2), str(day).zfill(2))
        models = [model for model in models if query in model]
        model = models[number]

        # load models
        storage = pickle.load(open(model, 'rb'))

        return storage

    def generate(self, trial, features, error, confidence):
        """Generate halton sequences of samples for given dimensions, and error bounds.

        Arguments:
            trial: dict of numpy arrays
            features: list of str, the features
            error: error bounds
            confidence: confidence interval

        Returns:
            numpy array
        """

        # get number of samples from SPACC, assuming features includes output as last
        dimension = len(features) - 1
        number = self._pack(error, confidence, dimension)

        # determine primes
        primes = [2,3,5,7,11,13,17,19,21]

        # produce halton sequences
        samples = []
        zipper = zip(range(number), *[self._halt(prime) for prime in primes[:dimension]])
        samples = [datum for datum in zipper]
        samples = numpy.array(samples)
        samples = samples[:, 1:]

        # scale by minimum and maximum
        for index, feature in enumerate(features[:-1]):

            # get min and max
            minimum = trial[feature].min()
            maximum = trial[feature].max()

            # adjust column
            samples[:, index] = minimum + samples[:, index] * (maximum - minimum)

        return samples

    def haul(self):
        """Gather the data from the particular files in the record.

        Arguments:
            reservoir: str, filepath to json file

        Returns:
            None
        """

        # timestamp
        self._stamp('assembling samples...', initial=True)

        # gather all json files
        paths = [path for path in self._see('../studies/albatross/records') if '.json' in path]

        # for each path
        records = []
        for path in paths:

            # accumulate records
            records += self._load(path)

        # only get those between lat 40
        records = [record for record in records if abs(record[0]['omi']['latitude']) <= 40]
        records = [record for record in records if record[0]['metric'] < 0.1]

        print(len(records))
        print(max([record[0]['deltas']['time ( sec )'] for record in records]))
        print(max([record[0]['deltas']['latitude ( km )'] for record in records]))
        print(max([record[0]['deltas']['longitude ( km )'] for record in records]))

        # begin data
        sensors = ('omi', 'merra')
        data = {'omi': {}, 'merra': {}}

        # add fields for omi
        fields = ('latitude', 'longitude', 'radiance', 'grid', 'date', 'time')
        for field in fields:

            # create reservoir
            data['omi'][field] = []

        # and add fields for merra
        fields = ('latitude', 'longitude', 'ozone', 'temperature', 'pressure', 'density', 'date', 'time', 'altitude')
        for field in fields:

            # create reservoir
            data['merra'][field] = []

        # for each record
        for index, record in enumerate(records):

            # pritn sttus
            self._print('record {} of {}...'.format(index, len(records)))

            # add latitudes, longitudes, dates, and times
            fields = ('latitude', 'longitude', 'time')
            [data[sensor][field].append(record[0][sensor][field]) for field in fields for sensor in sensors]

            # ingest the omi path
            self.ingest(record[0]['omi']['path'])

            # grab the row and track information
            row = record[0]['omi']['row']
            track = record[0]['omi']['track']

            # get wavelength and radiance information
            coefficients = self.dig('BAND2/wavelength_coefficient')[0].distil().squeeze()
            coefficients = coefficients[track][row]

            # get reference column
            reference = self.dig('BAND2/wavelength_reference_column')[0].distil()

            # calculate wavelengths
            wavelengths = self._calculate(coefficients, reference, 557)

            # get radiances
            radiances = self.dig('BAND2/radiance')[0].distil().squeeze()
            radiances = radiances[track][row]

            # create interpolations
            interpolations, grid = self._interpolate(radiances, wavelengths)

            # append attributes
            data['omi']['radiance'].append(interpolations)
            data['omi']['grid'].append(grid)

            # ingest the merra path
            self.ingest(record[0]['merra']['path'])

            # grab the row and track information
            row = record[0]['merra']['row']
            track = record[0]['merra']['track']

            # grab the ozone, temperature, pressure, and density profiles
            ozone = self.dig('O3Density')[0].distil().squeeze()[track][row]
            pressure = self.dig('Pressure')[0].distil().squeeze()[track][row]
            density = self.dig('AirDensity')[0].distil().squeeze()[track][row]
            temperature = self.dig('Temperature')[0].distil().squeeze()[track][row]
            altitude = self.dig('Altitude')[0].distil().squeeze()

            # append attributes
            data['merra']['ozone'].append(ozone)
            data['merra']['pressure'].append(pressure)
            data['merra']['density'].append(density)
            data['merra']['temperature'].append(temperature)
            data['merra']['altitude'].append(altitude)

        # shave radiances to a common grid
        interpolations = data['omi']['radiance']
        grid = data['omi']['grid']
        radiances, wavelengths = self._pick(interpolations, grid)

        # update data
        data['omi']['radiance'] = radiances
        data['omi']['grid'] = wavelengths

        # create features
        features = []
        for sensor in ('omi', 'merra'):

            # for each datum
            for name, datum in data[sensor].items():

                # create feature
                feature = Feature(['Categories', '{}_{}'.format(sensor, name)], numpy.array(datum))
                features.append(feature)

        # stash as data set
        destination = '../studies/albatross/data/OMI_MERRA_Dataset_{}.h5'.format(self._note())
        self.stash(features, destination, 'Data')

        return None

    def historicize(self, examination):
        """Make percent error histograms by zenith and albedo.

        Arguments:
            None

        Returns:
            None
        """

        # get traits and percents
        percents = examination['percents']
        traits = examination['traits']

        # begin features
        features = []

        # group tratis by zenith
        zeniths = self._group(traits, lambda trait: trait['zenith'])
        for zenith, members in zeniths.items():

            # create histogram
            name = 'histogram_zenith_{}'.format(zenith)
            array = numpy.vstack([percents[member['index']] for member in members]).flatten()
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # group tratis by zenith
        albedos = self._group(traits, lambda trait: trait['albedo'])
        for albedo, members in albedos.items():

            # create histogram
            name = 'histogram_albedo_{}'.format(albedo)
            array = numpy.vstack([percents[member['index']] for member in members]).flatten()
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # stash file
        destination = '{}/histograms/Zenith_Albedo_Histograms.h5'.format(self.sink)
        self.stash(features, destination)

        return None

    def inflate(self, exam, trials=10):
        """Compute importances and inflated importances with permutation method, weighed by covariances.

        Arguments:
            exam: examination result

        Returns:
            dict of importances and inflated importances
        """

        # status
        self._stamp('calculating correlations...', initial=True)

        # get matrix and names
        matrix = exam['matrix']
        names = exam['storage']['names']
        correlations = {(name, name): 1.0 for name in names}

        # for each name
        for index, name in enumerate(names):

            # and each other name
            for indexii, nameii in enumerate(names):

                # if indexii is greater
                if indexii > index:

                    # get statistics
                    mean = matrix[index].mean()
                    meanii = matrix[indexii].mean()
                    deviation = matrix[index].std()
                    deviationii = matrix[indexii].std()
                    samples = matrix.shape[0]

                    # calculate correlation
                    numerator = ((matrix[index] - mean) * (matrix[indexii] - meanii)).sum()
                    denominator = samples * deviation * deviationii
                    correlation = numerator / denominator

                    # add to dict
                    correlations[(name, nameii)] = correlation
                    correlations[(nameii, name)] = correlation

        # status
        self._stamp('calculating importances over {} trials...'.format(trials))

        # get base prediction
        perceptor = exam['storage']['machines']['perceptor']
        target = exam['target']
        result = perceptor.predict(matrix)
        base = r2_score(target, result)

        # create scores after permuting each column
        importances = {}
        for index, name in enumerate(names):

            # print name
            self._print('{} ({} of {})...'.format(name, index, len(names)))

            # for each trial
            scores = []
            for trial in range(trials):

                # copy sample matrix
                xerox = numpy.copy(matrix)

                # shuffle column
                column = xerox[:, index].tolist()
                column.sort(key=lambda quantity: numpy.random.rand())

                # add back into sample matrix
                xerox[:, index] = column

                # rescore
                result = perceptor.predict(xerox)
                score = r2_score(target, result)
                scores.append(score)

            # calculate averaged importance
            importance = base - numpy.mean(scores)
            importances[name] = importance

        # status
        self._stamp('inflating importances...')

        # compute inflated importances
        inflations = {}
        for name in names:

            # inflated importance is sum of all importances multiplied by absolute covariances
            inflation = sum([abs(correlations[(name, nameii)]) * importances[nameii] for nameii in names])
            inflations[name] = inflation

        # status
        self._stamp('creating report...')

        # create report
        report = ['inflated importances', '/n']

        # sort importancers
        report.append('\nfeature importances:\n')
        importances = list(importances.items())
        importances.sort(key=lambda pair: pair[1], reverse=True)
        [report.append('{}: {}'.format(*pair)) for pair in importances]

        # sort inflations
        report.append('\ninflated importances:\n')
        inflations = list(inflations.items())
        inflations.sort(key=lambda pair: pair[1], reverse=True)
        [report.append('{}: {}'.format(*pair)) for pair in inflations]

        # sort inflations
        report.append('\ncorrelations:\n')
        correlations = list(correlations.items())
        correlations.sort(key=lambda pair: abs(pair[1]), reverse=True)
        [report.append('{}: {}'.format(*pair)) for pair in correlations]

        # jot report
        destination = '{}/inflations/Inflated_Importances.h5'.format(self.sink)
        self._jot(report, destination)

        return None

    def launch(self, trials, features, brackets, augmentation, transforms, sizes, layers, options, fraction, flow=None):
        """Use a multilayer perceptron to predict log radiances from ozone profiles, etc.

        Arguments:
            trials; int, number of trials
            features: list of str, features to include in model
            brackets: dict of tuples, the data sampling brackets
            augmentation: dict of data preparation options
            transforms: list of data transformations ('_' indicates inputs if first or outputs if last )
            sizes: dict of pca sizes
            layers: tuple of ints, the hidden layer sizes
            options: dict of additional scikit options, and options in common
            flow: dict of tensorflow specific options
            fraction: float, fraction of training set

        Returns:
            None
        """

        # get current year, month, day
        now = self._note()
        year = int(now[:4])
        month = int(now[5:7])
        day = int(now[7:9])

        # begin collections
        collection = {'storage': [], 'exams': []}

        # for each trial
        for trial in range(trials):

            # run perceptor and add to collection
            storage = self.perceive(features, brackets, augmentation, transforms, sizes, layers, options, fraction, flow)
            sleep(5)
            collection['storage'].append(storage)

            # try to
            try:

                # make examination
                exam = self.examine(year, month, day, 0, data=storage['data'])
                collection['exams'].append(exam)

                # and update record
                self.remember(year, month, day)

            # unless error
            except KeyError as exception:

                # print the errir
                self._print(exception)

        return collection

    def migrate(self):
        """Plot experimental progress as an hdf file from all experiments.

        Arguments:
            None

        Returns:
            None
        """

        # gather experiments
        folder = '{}/experiments'.format(self.sink)
        experiments = []
        self._tell(self._see(folder))
        for index, jason in enumerate(self._see(folder)):

            # add experiments
            studies = self._load(jason)
            for indexii, study in enumerate(studies):

                # add entry
                study.update({'index': indexii, 'study': jason, 'code': index})
                experiments.append(study)

        # begin features
        features = []

        # create samples based on durationn and sample size
        brackets = {'single': (0, 2), 'band': (4, 12), 'pca': (15, 30), 'bands': (120, 150), 'full': (1100, 1300)}
        samples = self._group(experiments, lambda experiment: experiment['truths'][1])
        for label, bracket in brackets.items():

            # get the members based on the length of the output vector
            studies = [study for study in experiments if bracket[0] < int(study['truths'][1]) < bracket[1]]

            # create validation scores as ordinate
            validations = numpy.array([float(study['validation']) for study in studies])
            name = 'r_squared_scores_{}'.format(label)
            feature = Feature(['Categories', name], validations)
            features.append(feature)

            # add duration as abscissa
            durations = numpy.array([float(study['duration'] / 60) for study in studies])
            name = 'r_squared_scores_{}_abscissa'.format(label)
            feature = Feature(['IndependentVariables', name], durations)
            features.append(feature)

            # add duration as abscissa
            samples = numpy.array([float(study['targets'][0]) for study in studies])
            name = 'training_samples_{}'.format(label)
            feature = Feature(['IndependentVariables', name], samples)
            features.append(feature)

            # add duration as abscissa
            samples = numpy.array([study['index'] for study in studies])
            name = 'training_indices_{}'.format(label)
            feature = Feature(['IndependentVariables', name], samples)
            features.append(feature)

            # add duration as abscissa
            samples = numpy.array([int(study['code']) for study in studies])
            name = 'training_studies_{}'.format(label)
            feature = Feature(['IndependentVariables', name], samples)
            features.append(feature)

        # create file
        destination = '{}/data/experiments/Experiments_{}.h5'.format(self.sink, self.tag)
        self.stash(features, destination, 'Data')

        return None

    def name(self, year, month, day, number=0, name='_'):
        """Tag the model with a name.

        Arguments:
            year: int, hear
            month: int, month
            day: int, day
            number: number of training example
            name: name for model

        Returns:
            None
        """

        # load model from storage
        storage = self.forage(year, month, day, number=number)

        # add name
        storage['name'] = name

        # add to record
        pickle.dump(storage, open(storage['destination'], 'wb'))

        return None

    def nest(self, year, month, day, number, tag='nest'):
        """Create interpolation model and measure trial orbit against.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            number: int, model index
            tag: str, for id

        Returns:
            None
        """

        # retrieve model
        storage = self.forage(year, month, day, number)
        augmentation = storage['augmentation']
        brackets = storage['brackets']
        machines = storage['machines']
        transforms = storage['transforms']
        sizes = storage['sizes']
        features = storage['features']
        last = features[-1]
        perceptor = load_model(storage['model'])

        # get original data
        data = self.sieve(augmentation)
        trial = self.trial(brackets, augmentation)

        # get data
        radiances = numpy.log10(data['radiance'])
        zeniths = data['zenith']
        albedos = data['albedo']
        surfaces = data['surface']

        # get radiance shape
        shape = radiances.shape

        # plot radiance vs zenith
        ordinate = radiances.transpose(2, 1, 0)
        abscissa = numpy.array([zeniths] * shape[1])
        abscissa = numpy.array([abscissa] * shape[2])
        abscissa = abscissa.transpose(0, 1, 2)
        self._print(ordinate.shape, abscissa.shape)
        # ordinate = self._space(ordinate, shape[0])
        # abscissa = self._space(abscissa, shape[0])
        self._print(ordinate.shape, abscissa.shape)
        title = 'LUT Radiance vs Solar Zenith Angle'
        address = 'nest/LUT_Zenith.h5'
        self.squid.ink('log_radiance', ordinate, 'solar_zenith_angle', abscissa, address, title)

        # plot radiance vs albedo
        ordinate = radiances.transpose(2, 0, 1)
        abscissa = numpy.array([albedos] * shape[0])
        abscissa = numpy.array([abscissa] * shape[2])
        abscissa = abscissa.transpose(0, 1, 2)
        self._print(ordinate.shape, abscissa.shape)
        # ordinate = self._space(ordinate, shape[1])
        # abscissa = self._space(abscissa, shape[1])
        self._print(ordinate.shape, abscissa.shape)
        title = 'LUT Radiance vs Reflectivity'
        address = 'nest/LUT_Albedo.h5'
        self.squid.ink('log_radiance', ordinate, 'reflectivity', abscissa, address, title)

        # plot radiance vs surface pressure
        ordinate = radiances.transpose(0, 1, 2)
        abscissa = numpy.array([surfaces] * shape[1])
        abscissa = numpy.array([abscissa] * shape[0])
        abscissa = abscissa.transpose(0, 1, 2)
        self._print(ordinate.shape, abscissa.shape)
        # ordinate = self._space(ordinate, shape[2])
        # abscissa = self._space(abscissa, shape[2])
        self._print(ordinate.shape, abscissa.shape)
        title = 'LUT Radiance vs Surface Pressure'
        address = 'nest/LUT_Surface.h5'
        self.squid.ink('log_radiance', ordinate, 'surface_pressure', abscissa, address, title)

        # collect trial
        radiancesii = trial[last].squeeze()
        zenithsii = trial['zenith'].squeeze()
        albedosii = trial['albedo'].squeeze()
        surfacesii = trial['surface'].squeeze()

        # go each sample
        interpolations = []
        self._print('interpolating...')
        for radiance, zenith, albedo, surface in zip(radiancesii, zenithsii, albedosii, surfacesii):

            # determine albedo
            reflectivity = (albedos == albedo).tolist().index(True)

            # get subset of radiances, and orient along surface prssure axis
            subset = radiances[:, reflectivity, :].transpose(1, 0)

            # for each row
            pressures = []
            for row in subset:

                # create spline
                spline = scipy.interpolate.CubicSpline(zeniths, row)

                # use spline to estimate radiance at sample zenith for all pressures
                pressure = spline(zenith)
                pressures.append(pressure)

            # create a spline for pressures
            splineii = scipy.interpolate.CubicSpline(surfaces, pressures)
            interpolation = splineii(surface)
            interpolations.append(interpolation)

        # get errors
        interpolations = numpy.array(interpolations)
        error = 100 * ((10 ** interpolations / 10 ** radiancesii) - 1)

        # make scatter plot
        address = 'nest/Interpolation_Errors_Zenith.h5'
        title = 'Interpolation errors vs solar zenith angle'
        self.squid.ink('error', error, 'zenith', zenithsii, address, title)

        # make scatter plot
        address = 'nest/Interpolation_Errors_Albedo.h5'
        title = 'Interpolation errors vs reflectivity'
        self.squid.ink('error', error, 'reflectivity', albedosii, address, title)

        # make scatter plot
        address = 'nest/Interpolation_Errors_Pressure.h5'
        title = 'Interpolation errors vs surface pressure'
        self.squid.ink('error', error, 'pressure', surfacesii, address, title)

        # set reflectivity and pressures
        reflectivity = 5
        pressures = (0, 4, 8)
        angles = numpy.linspace(1, 89, 890)

        # for each pressure
        for pressure in pressures:

            # calculate spline
            row = radiances[:, reflectivity, pressure]
            spline = scipy.interpolate.CubicSpline(zeniths, row)

            # calculate curve
            curve = spline(angles)
            length = len(curve)

            # plot
            title = 'Spline interpolation at pressure {}'.format(pressure)
            address = 'nest/Spline_pressure_{}.h5'.format(pressure)
            self.squid.ink('radiance', curve, 'zenith', angles, address, title)

            # construct model inputs, with dummy output
            test = {'cosine': numpy.cos(angles * (math.pi / 180)).reshape(-1, 1)}
            test.update({'albedo': numpy.array([albedos[reflectivity]] * length).reshape(-1, 1)})
            test.update({'surface': numpy.array([surfaces[pressure]] * length).reshape(-1, 1)})

            # set dummy outputs
            for field in ('radiance', 'multiple', 'single'):

                # set to zeros
                test.update({field: numpy.array([0.0] * length).reshape(-1, 1)})

            # vectorize the data
            matrix, _, _, _, _ = self.vectorize(test, features, sizes, transforms, machines)

            # get predictions from perceptor, and undo transformations
            prediction = perceptor.predict(matrix)

            # undo transforms
            for transform in transforms[::-1]:

                # undo transform
                prediction = self._reform(prediction, transform, machines[last][transform], inputs=False)

            # plot predictions
            title = 'Model prediction at pressure {}'.format(pressure)
            address = 'nest/Model_pressure_{}_{}.h5'.format(pressure, tag)
            self.squid.ink('radiance', prediction, 'zenith', angles, address, title)

            # plot error between
            error = 100 * ((10 ** prediction.flatten() / 10 ** curve.flatten()) - 1)
            title = 'Model prediction vs Spline at pressure {}'.format(pressure)
            address = 'nest/Error_pressure_{}_{}.h5'.format(pressure, tag)
            self.squid.ink('error', error, 'zenith', angles, address, title)

        return None

    def overlap(self, brackets, augmentation, *features):
        """Plot histograms and training, orbit datasets versus zenith angle.

        Arguments:
            brackets: dict of feature ranges
            augmentation: dict of training augmentation parameters
            *features: unpacked list of features to test

        Returns:
            None
        """

        # get the data and trial
        data = self.sift(brackets, augmentation)
        trial = self.trial(brackets, augmentation)

        # retrieve zenith data
        zenith = data['zenith']
        zenithii = trial['zenith']

        # for each feature
        for feature in features:

            # plot the lut histogram
            title = 'LUT Histogram for {}'.format(feature)
            self.squid.ripple(feature, data[feature], 'overlap/{}_lut_histogram.h5'.format(feature), title)

            # plot the orbit histogram
            title = 'Orbit 48687 Histogram for {}'.format(feature)
            self.squid.ripple(feature, trial[feature], 'overlap/{}_48687_histogram.h5'.format(feature), title)

            # plot lut vs zenith
            title = 'LUT {} vs solar zenith angle'.format(feature)
            address = 'overlap/{}_LUT_vs_zenith.h5'.format(feature)
            self.squid.ink(feature, data[feature], 'solar zenith angle', zenith, address, title)

            # plot orbit vs zenith
            title = 'Orbit 48687 {} vs solar zenith angle'.format(feature)
            address = 'overlap/{}_48687_vs_zenith.h5'.format(feature)
            self.squid.ink(feature, trial[feature], 'solar zenith angle', zenithii, address, title)

        return None

    def peck(self, data, fraction=0.8, memory=None, shuffle=False, output='radiance'):
        """Pick training and test samples by random split (or remembered split).

        Arguments:
            data: dict of numpy arrays, the dataset
            fraction: float, training, test split fraction
            memory: dict of lists, indices of remembered train / test split
            shuffle: boolean, shuffle data?

        Returns:
            tuple of numpy arrays (trains, targets, tests, truths)
        """

        # determine number of samples
        samples = data[output].shape[0]

        # if no memory yet
        if not memory:

            # determine length of dataset and parition point
            partition = int(fraction * samples)

            # create indices and sort randomly
            indices = list(range(samples))
            indices.sort(key=lambda entry: numpy.random.rand())

            # create memory by splitting at the partition
            first = numpy.array(indices[:partition]).tolist()
            last = numpy.array(indices[partition:]).tolist()
            memory = {'train': first, 'test': last}

        # create the training set
        train = {}
        test = {}
        for name, array in data.items():

            # check for samples axis
            if array.shape[0] == samples and shuffle:

                # split using memory
                train[name] = array[memory['train']]
                test[name] = array[memory['test']]

            # otherwise
            else:

                # copy whole array
                train[name] = array
                test[name] = array

        return train, test, memory

    def peer(self, brackets, features, sizes, transforms, orbits=None):
        """Attempt to classify orbit files from table files.

        Arguments:
            brackets: dict of parameters

        Returns:
            None
        """

        # grab climatology dataset
        self._stamp('gathering and filtering data...', initial=True)
        data = self.sift(brackets)
        data, _, _ = self.peck(data, 1.0, shuffle=True, output='scatter')
        data = {feature: data[feature] for feature in features}

        # collect orbits
        collection = []
        orbits = orbits or (48687, 48688, 50006)
        for orbit in orbits:

            # grab orbits dataset
            self._stamp('gathering and filtering data...', initial=True)
            dataii = self.trial(brackets, orbit=orbit)
            dataii = {feature: dataii[feature] for feature in features}
            collection.append(dataii)

        # stack vectors
        stack = {feature: numpy.vstack([entry[feature] for entry in collection]) for feature in features}

        # minimize datasets to same size
        minimum = min([data[features[0]].shape[0], stack[features[0]].shape[0]])

        # minimize all datasets
        data = {feature: data[feature][:minimum] for feature in features}
        stack = {feature: stack[feature][:minimum] for feature in features}

        # add trainig bucket
        data['bucket'] = numpy.zeros((minimum, 1))
        stack['bucket'] = numpy.ones((minimum, 1))

        # stack all data
        features = features.copy()
        features += ['bucket']
        data = {feature: numpy.vstack([data[feature], stack[feature]]) for feature in features}

        # split into train and test
        fraction = 0.5
        train, test, memory = self.peck(data, fraction, shuffle=True, output='bucket')
        matrix, target, original, names, machines = self.vectorize(train, features, sizes, transforms)
        matrixii, targetii, _, _, _ = self.vectorize(test, features, sizes, transforms, machines)

        # createe randomforest classifier
        forest = RandomForestClassifier(n_estimators=500, max_depth=5)
        forest.fit(matrix, target)

        # Get predictions
        predictions = forest.predict(matrixii)
        probabilities = forest.predict_proba(matrixii)

        # # crate linear svv
        # linear = LinearSVC()
        # linear.fit(matrix, target)
        # print(linear)
        # predictions = forest.predict(matrixii)

        # get feature importances
        importances = forest.feature_importances_
        importances = list(zip(names, importances))
        importances.sort(key=lambda pair: pair[1])

        # priunt
        for name, importance in importances:

            # print
            print(name, importance)

        # print score
        zipper = list(zip(targetii.flatten(), predictions.flatten()))
        hits = [pair for pair in zipper if pair[0] == pair[1]]
        accuracy = len(hits) / predictions.shape[0]
        print('accuracy: {}'.format(accuracy))

        # determine area under roc
        roc = roc_curve(targetii[:, 0], probabilities[:, 0])
        score = auc(roc[1], roc[0])
        print('roc score: {}'.format(score))

        # make histograms
        histograms = []
        for feature in features:

            # gather data
            block = data[feature]
            for index in range(block.shape[1]):

                # only every ten
                if index % 10 == 0:

                    # create histogram
                    name = 'histogram_{}_{}'.format(feature, self._pad(index))
                    array = block[:, index]
                    histogram = Feature(['Categories', name], array)
                    histograms.append(histogram)

                    # create lut histogram
                    mask = data['bucket'][:, 0] == 0
                    name = 'histogram_lut_{}_{}'.format(feature, self._pad(index))
                    array = block[:, index][mask]
                    histogram = Feature(['Categories', name], array)
                    histograms.append(histogram)

                    # create orbit histogram
                    mask = data['bucket'][:, 0] == 1
                    name = 'histogram_orbit_{}_{}'.format(feature, self._pad(index))
                    array = block[:, index][mask]
                    histogram = Feature(['Categories', name], array)
                    histograms.append(histogram)

        # construct file
        destination = '{}/histograms/LUT_Orbits_histograms_{}.h5'.format(self.sink, ''.join(features))
        self.stash(histograms, destination)

        # mnake analysis
        analysis = {'data': data,'target': target, 'targetii': targetii}
        analysis.update({'matrix': matrix, 'matrixii': matrixii})
        analysis.update({'prediction': predictions, 'probability': probabilities, 'curve': roc, 'auc': score})

        return analysis

    def perceive(self, features, brackets, augmentation, transforms, sizes, layers, options, fraction=0.5, flow=None):
        """Use a multilayer perceptron to predict log radiances from ozone profiles, etc.

        Arguments:
            features: list of str, features to include in model
            brackets: dict of tuples, the data sampling brackets
            augmentation: dict of data preparation options
            transforms: list of data transformations ('_' indicates inputs if first or outputs if last )
            sizes: dict of pca sizes
            layers: tuple of ints, the hidden layer sizes
            options: dict of additional scikit options, and options in common
            flow: dict of tensorflow specific options
            fraction: fraction of training set

        Returns:
            None
        """

        # read in parameters
        if self.ticket:

            # open up the ticketl
            ticket = self._load('{}/{}'.format(self.sink, self.ticket))

            # set parameters
            features = ticket['features']
            brackets = ticket['brackets']
            augmentation = ticket['augmentation']
            transforms = ticket['transforms']
            sizes = ticket['sizes']
            layers = ticket['layers']
            options = ticket['options']
            fraction = ticket['fraction']
            flow = ticket['flow']

        # begin timing
        self._stamp('vectorizing data...', initial=True)

        # set defaults for scikit MLP Regressor
        weights = False
        bounds = (-90, 90)
        double = True

        # set brackets, defaulting to all data
        brackets = brackets or {}
        full = {'albedos': (0, 11), 'zeniths': (0, 10), 'months': (0, 12), 'latitudes': (0, 36)}
        full.update(brackets)
        brackets = full

        # begin pickle for storing model contents
        storage = {'bounds': bounds, 'machines': None, 'memory': None, 'weights': weights, 'double': double}
        storage.update({'features': features, 'brackets': brackets, 'augmentation': augmentation})
        storage.update({'transforms': transforms, 'sizes': sizes, 'layers': layers, 'options': options})
        storage.update({'flow': flow})

        # grab data from files and filter out fill values
        self._stamp('gathering and filtering data...', initial=True)
        data = self.sift(brackets, augmentation)

        # generate train, test split
        train, test, memory = self.peck(data, fraction, shuffle=True)

        # vectorize the training data
        matrix, target, original, names, machines = self.vectorize(train, features, sizes, transforms)
        storage.update({'memory': memory, 'machines': machines, 'names': names, 'fraction': fraction})

        # vectorize the testing data
        matrixii, targetii, _, _, _ = self.vectorize(test, features, sizes, transforms, machines)

        # vectorize the oribital set
        orbit = self.trial(brackets, augmentation)
        matrixiii, targetiii, _, _, _ = self.vectorize(orbit, features, sizes, transforms, machines)

        # set mlp parameters
        self._stamp('training Multilayer Perceptor on {} samples...'.format(len(train)))
        parameters = {'max_iter': 1, 'hidden_layer_sizes': layers, 'activation': 'relu', 'tol': 1e-16}
        parameters.update({'verbose': True, 'n_iter_no_change': 10, 'solver': 'adam', 'epsilon': 1e-4})
        parameters.update({'learning_rate': 'adaptive', 'learning_rate_init': options['rates'][0]})
        parameters.update({'momentum': 0.9, 'nesterovs_momentum': True, 'warm_start': True})
        parameters.update({'alpha': 1e-6, 'shuffle': True, 'beta_1': 0.9, 'beta_2': 0.999})
        parameters.update({'batch_size': 200})
        parameters.update(options)
        storage['parameters'] = parameters

        # create model destination file
        unique = self._pad(numpy.random.randint(9999), 4)
        destination = '{}/machines/MLP_{}_{}_{}.sav'.format(self.sink, self.tag, self._note(), unique)

        # if using tensorflow
        if flow:

            # train with tensorflow
            initial = datetime.datetime.now()
            validations = [(matrixii, targetii), (matrixiii, targetiii)]
            perceptor = self.flow(parameters, matrix, target, flow, validations)
            final = datetime.datetime.now()

            # save the model
            path = '{}/flows/tensor_{}.h5'.format(self.sink, self._note())
            storage['model'] = path
            perceptor.save(path)

            # add loss information
            storage['curve'] = perceptor.curve
            storage['loss'] = perceptor.loss
            storage['duration'] = (final - initial).seconds
            storage['curveii'] = perceptor.curveii
            storage['curves'] = perceptor.curves
            storage['iterations'] = perceptor.last
            storage['cycle'] = perceptor.cycle

            # save model
            self._make('{}/machines'.format(self.sink))
            pickle.dump(storage, open(destination, 'wb'))

        # otherwise
        else:

            # make MLP and train for first epoch
            perceptor = MLPRegressor(**parameters)

            # change interations to 1 and perform first fit
            perceptor.fit(matrix, target)

            # change iterations
            perceptor.max_iter = 400

            # if initializing weights
            if weights:

                # define weighting function to create cos distributed weights from uniform
                def weighting(tensor): return (1 / (5 * math.pi)) * numpy.arcsin(2 * tensor - 1)

                # get layers, plus inputs and outputs
                neurons = [perceptor.n_features_in_] + list(layers) + [perceptor.n_outputs_]
                shapes = zip(neurons[:-1], neurons[1:])
                weights = []
                biases = []
                for first, second in shapes:

                    # create weight array
                    weight = numpy.random.rand(first, second)
                    weight = weighting(weight)
                    weights.append(weight)

                    # create bias array
                    bias = numpy.random.rand(second)
                    bias = weighting(bias)
                    biases.append(bias)

                # make assignment
                perceptor.coefs_ = weights
                perceptor.intercepts_ = biases

            # perform training
            perceptor.fit(matrix, target)

            # add attributes
            storage['curve'] = perceptor.loss_curve_

            # if double
            if double:

                # retrain at lower rate with sgd
                print('\nretraining with sgd...')
                perceptor.solver = 'sgd'
                perceptor.max_iter = 200
                perceptor.learning_rate_init = options['rates'][1]
                perceptor.fit(matrix, target)

                # retrain at lower rate with sgd
                print('\nretraining with second sgd...')
                perceptor.solver = 'sgd'
                perceptor.max_iter = 600

                perceptor.learning_rate_init = options['rates'][2]
                perceptor.fit(matrix, target)

                # add to curve
                storage['curve'] = perceptor.loss_curve_

            # calculate duration
            duration = self._stamp('trained by {}.'.format(parameters['solver']))
            storage['duration'] = duration
            storage['machines']['perceptor'] = perceptor
            storage['loss'] = perceptor.loss_curve_[-1]

            # save model
            self._make('{}/machines'.format(self.sink))
            pickle.dump(storage, open(destination, 'wb'))

        # get predictions from perceptor
        prediction = perceptor.predict(matrix)

        # calculate loss
        if flow:

            # calculate loss
            model = load_model(storage['model'])
            storage['loss'] = numpy.array(model.loss(prediction, target)).mean()

        # add extra dimention if needed
        if len(prediction.shape) < 2:

            # reshpae
            prediction = prediction.reshape(-1, 1)

        # undo transforms
        output = features[-1]
        for transform in transforms[::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines[output][transform], inputs=False)

        # # calculate precent diffence
        # if features[-1] == 'multiple':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** prediction
        #     truth = 10 ** original
        #
        # # calculate precent from ratio
        # if features[-1] == 'ratio':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (train['single'] / prediction)
        #     truth = 10 ** (train['single'] / original)
        #
        # # calculate precent from ratio
        # if features[-1] == 'difference':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (train['single'] - prediction)
        #     truth = 10 ** (train['single'] - original)

        # reconstitute prediction
        estimate = self._reconstitute(prediction, features[-1], train)
        truth = self._reconstitute(original, features[-1], train)

        # calculate absolue
        percent = 100 * ((estimate / truth) - 1)
        percent = abs(percent)
        maximum = float(percent.max())
        median = float(numpy.percentile(percent, 50))
        self._print('median absolute percent error: {}'.format(median))
        storage['percent'] = median

        # print max error
        self._print('maximum absolute percent error: {}'.format(maximum))
        storage['maximum'] = maximum

        # calculate score
        score = r2_score(original, prediction)
        self._print('training R^2 score: {}'.format(score))
        storage['verification'] = float(score)

        # save model
        storage['destination'] = destination
        pickle.dump(storage, open(destination, 'wb'))

        # vectorize the test data with same set of machines
        info = self.vectorize(test, features, sizes, transforms, machines)
        matrix, target, original, names, machines = info

        # get predictions from perceptor, and undo transformations
        prediction = perceptor.predict(matrix)

        # add extra dimention if needed
        if len(prediction.shape) < 2:

            # reshpae
            prediction = prediction.reshape(-1, 1)

        # undo transforms
        for transform in transforms[::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines[output][transform], inputs=False)

        # reconstitute prediction
        estimate = self._reconstitute(prediction, features[-1], test)
        truth = self._reconstitute(original, features[-1], test)

        # # calculate precent diffence
        # if features[-1] == 'multiple':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** prediction
        #     truth = 10 ** original
        #
        # # calculate precent from ratio
        # if features[-1] == 'ratio':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (test['single'] / prediction)
        #     truth = 10 ** (test['single'] / original)
        #
        # # calculate precent from ratio
        # if features[-1] == 'difference':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (test['single'] - prediction)
        #     truth = 10 ** (test['single'] - original)

        # calculate absolue
        percent = 100 * ((estimate / truth) - 1)
        percent = abs(percent)
        maximum = float(percent.max())
        median = float(numpy.percentile(percent, 50))
        self._print('median absolute percent error: {}'.format(median))
        storage['percentii'] = median

        # print max error
        self._print('maximum absolute percent error: {}'.format(maximum))
        storage['maximumii'] = maximum

        # calculate score
        score = r2_score(original, prediction)
        self._print('testing R^2 score: {}'.format(score))
        storage['validation'] = float(score)

        # save model
        pickle.dump(storage, open(destination, 'wb'))

        # add data attribute, can't pickle due to numpy arrays
        storage['data'] = data

        return storage

    def perch(self, brackets, data=None, samples=[0, 100, 200, 300, 400]):
        """Optimize a funtional fit to the zenith data.

        Arguments:
            None

        Returns:
            None
        """

        # get all data
        data = data or self.sift(brackets)

        # grab zeniths and radiances
        zenith = data['zenith']
        zeniths = data['zeniths']
        radiance = data['radiance']

        # begin feaures
        features = []

        # for each sample
        splines = []
        size = zeniths.shape[0]
        for sample in samples:

             # fit sample
            abscissa = zenith[size * sample:size + size * sample].flatten()
            ordinate = radiance[size * sample:size + size * sample].flatten()

            # crate spline
            spline = scipy.interpolate.interp1d(abscissa, ordinate, kind='cubic')
            #spline = scipy.interpolate.lagrange(abscissa, ordinate)
            splines.append(spline)

            # fit on random points
            points = abscissa.min() + (abscissa.max() - abscissa.min()) * numpy.random.rand(100)
            prediction = spline(points)

            # add features
            tag = (self._pad(sample, 3))
            features.append(Feature(['Categories', 'grid_{}'.format(tag)], ordinate))
            features.append(Feature(['IndependentVariables', 'zenith_{}'.format(tag)], abscissa))
            features.append(Feature(['Categories', 'random_{}'.format(tag)], prediction))
            features.append(Feature(['IndependentVariables', 'points_{}'.format(tag)], points))

            # also predict half and quarter points
            halves = [(first + second) / 2 for first, second in zip(abscissa[:-1], abscissa[1:])]
            halves = numpy.array(halves)
            prediction = spline(halves)
            features.append(Feature(['Categories', 'halves_{}'.format(tag)], prediction))
            features.append(Feature(['IndependentVariables', 'halves_{}'.format(tag)], halves))

            # also predict quarter points
            quarters = [(first + second) / 2 for first, second in zip(abscissa[:-1], halves)]
            quarters += [(first + second) / 2 for first, second in zip(abscissa[1:], halves)]
            quarters.sort()
            quarters = numpy.array(quarters)
            prediction = spline(quarters)
            features.append(Feature(['Categories', 'quarters_{}'.format(tag)], prediction))
            features.append(Feature(['IndependentVariables', 'quarters_{}'.format(tag)], quarters))

        # create histogram of halfway zenith angles
        radiance = radiance.reshape(-1, size)
        even = numpy.array([index for index in range(size) if index % 2 == 0])
        odd = numpy.array([index for index in range(size) if index % 2 == 1])
        errors = []
        for sample in radiance:

            # create spline
            #spline = scipy.interpolate.interp1d(zeniths[even], sample[even], kind='cubic')
            #spline = scipy.interpolate.interp1d(zeniths[even], sample[even], kind='cubic')
            # spline = scipy.interpolate.lagrange(zeniths[even], sample[even])
            spline = scipy.interpolate.CubicSpline(zeniths[even], sample[even])
            #spline = scipy.interpolate.BarycentricInterpolator(zeniths[even], sample[even])
            prediction = spline(zeniths[odd])

            # # create polynomial
            # polynomial = lambda x, a, b, c, d, e, f: a + b * x + c * x ** 2 + d * x ** 3 + e * x ** 4 + f * x ** 5
            # polynomial = lambda x, a, b, c, d, e: a + b * x + c * x ** 2 + d * x ** 3 + e * x ** 4
            # #polynomial = lambda x, a, b, c, d: a + b * x + c * x ** 2 + d * x ** 3
            # vector, covariance = scipy.optimize.curve_fit(polynomial, zeniths[even], sample[even])
            # prediction = numpy.array([polynomial(quantity, *vector) for quantity in zeniths[odd]])

            # capture errors
            error = (prediction / sample[odd])
            errors += error.tolist()

        # make histogram
        name = 'histogram_interpolation_errors'
        array = numpy.array(errors)
        features.append(Feature(['Categories', name], array))

        # make scatter plot
        name = 'interpolation_errors'
        array = numpy.array(errors)

        features.append(Feature(['Categories', name], array))
        name = 'interpolation_errors_scatter'
        array = numpy.hstack([zeniths[odd].flatten()] * radiance.shape[0])
        features.append(Feature(['IndependentVariables', name], array))

        # stash features
        self.stash(features, '{}/validations/Zenith_Spline_Interpolation.h5'.format(self.sink))

        # create object
        optimization = {'splines': splines, 'data': data}

        return optimization

    def pressurize(self):
        """Make histogram of surface pressures from accumulation.

        Arguments:
            None

        Returns:
            None
        """

        # load in pressures
        hydra = Hydra('{}/pressures'.format(self.sink))
        hydra.ingest(0)

        # collect data
        counts = hydra.grab('surface_pressure_sample_count')
        pressures = hydra.grab('surface_pressure_sum')

        # calculate average pressures
        averages = pressures / counts
        averages = averages[numpy.isfinite(averages)]

        # begin features
        histograms = []

        # make pressure histogram
        name = 'histogram_surface_pressure_omps'
        array = averages.flatten()
        histogram = Feature(['Categories', name], array)
        histograms.append(histogram)

        # load in pressures
        hydra = Hydra('{}/calculations'.format(self.sink))
        hydra.ingest(0)

        # collect data
        pressures = hydra.grab('Pressure')[0, :, :]

        # make pressure histogram
        name = 'histogram_surface_pressure_lut'
        array = pressures.flatten()
        histogram = Feature(['Categories', name], array)
        histograms.append(histogram)

        # stash
        destination = '{}/histograms/Pressure_histograms.h5'.format(self.sink)
        self.stash(histograms, destination)

        return None

    def probe(self, storage, exam, zenith, top=5, steps=100):
        """Probe the model at specific zenith sample.

        Arguments:
            storage: perception model object
            exam: examination object
            zenith: zenith again of sample for probing
            top: number of top entries

        Returns:
            None
        """

        # grab the validation data set
        test = exam['validations'][0]['data']

        # try:
        try:

            # grab the train data set
            train = storage['data']

        # unless absent, in which case generate
        except KeyError:

            # create data
            storage['data'] = self.sift(storage['brackets'], storage['augmentation'])
            train = storage['data']

        # instantiate the model
        perceptor = load_model(storage['model'])

        # find the sample closest to the zenith angle
        sample = ((test['zenith'].flatten() - zenith) ** 2).argsort()[0]

        # also get the scattering amount and radiances
        scatter = test['scatter'].flatten()[sample]
        multiple = test['multiple'].flatten()[sample]
        single = test['single'].flatten()[sample]
        albedo = test['albedo'].flatten()[sample]

        # reset zenith to actual measurement
        zenith = test['zenith'].flatten()[sample]

        # find the top 100 members from the training that are closest based on scattering
        closest = ((train['scatter'].flatten() - scatter) ** 2).argsort()[:100].tolist()

        # construct zenith mappings, and sort by zenith closeness
        records = [(index, train['zenith'].flatten()[index]) for index in closest]
        records.sort(key=lambda record: (record[1] - zenith) ** 2)

        # find the top 100 members from the training that are closest based on zenith
        closest = ((train['zenith'].flatten() - zenith) ** 2).argsort()[:100].tolist()

        # construct zenith mappings, and sort by zenith closeness
        records = [(index, train['scatter'].flatten()[index]) for index in closest]
        records.sort(key=lambda record: (record[1] - scatter) ** 2)
        best = [record[0] for record in records[:top]]
        indices = best

        # for each index
        bracket = (0, 89)
        for index in indices:

            # grab the charaterisicts
            zenithii = train['zenith'].flatten()[index]
            scatterii = train['scatter'].flatten()[index]
            multipleii = train['multiple'].flatten()[index]
            singleii = train['single'].flatten()[index]
            albedoii = train['albedo'].flatten()[index]

            # plot all zeniths
            zeniths = [bracket[0] + (bracket[-1] - bracket[0]) * (chunk / steps) for chunk in range(steps)]

            print('\ttraining run {}...'.format(index))
            print('zenith',  zenithii)
            print('scatter', scatterii)
            print('multiple', multipleii)
            print('single', singleii)

            # construct cosines
            cosines = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zeniths])
            cosines = cosines.reshape(-1, 1)

            # construct rayleights and single scatterings
            reflectivities = numpy.array([albedoii] * len(cosines))
            reflectivities = reflectivities.reshape(-1, 1)
            rayleighs = numpy.array([scatterii] * len(cosines))
            rayleighs = rayleighs.reshape(-1, 1)
            simples = numpy.array([singleii] * len(cosines))
            simples = simples.reshape(-1, 1)

            # perfrom transformations
            machines = storage['machines']
            for transform in storage['transforms']:

                # undo transform
                reflectivities = self._transform(reflectivities, transform, machines['albedo'][transform], inputs=True)
                cosines = self._transform(cosines, transform, machines['cosine'][transform], inputs=True)
                rayleighs = self._transform(rayleighs, transform, machines['scatter'][transform], inputs=True)
                simples = self._transform(simples, transform, machines['single'][transform], inputs=True)

            # make matrix
            reflectivities = reflectivities.squeeze()
            rayleighs = rayleighs.squeeze()
            cosines = cosines.squeeze()
            simples = simples.squeeze()
            matrix = numpy.vstack([reflectivities, rayleighs, cosines, simples]).transpose(1, 0)

            print('matrix', matrix.shape, matrix.min(), matrix.max())

            # make predictions
            prediction = perceptor.predict(matrix)

            # undo transforms
            machines = storage['machines']
            for transform in storage['transforms'][::-1]:

                # undo transform
                prediction = self._reform(prediction, transform, machines['multiple'][transform], inputs=False)

            print('cosines', cosines.min(), cosines.max())
            print('rayleigh', rayleighs.min(), rayleighs.max())
            print('zeniths', numpy.array(zeniths).min(), numpy.array(zeniths).max())
            print('predictions', prediction.min(), prediction.max(), prediction.shape)

            # make plot
            title = 'Prediction_vs_Zenith, Rayleigh: {}, {}'.format(scatter, index)
            folder = 'model/model_scrounge_zenith_{}_{}_{}.h5'.format(round(scatterii, 3), index, round(zenithii, 2))
            self.squid.ink('prediction', prediction, 'zenith', zeniths, folder, title)

            # also plot training point
            title = 'Training Example: {}, {}'.format(scatter, index)
            folder = 'model/model_train_zenith_{}_{}_{}.h5'.format(round(scatterii, 3), index, round(zenithii, 2))
            self.squid.ink('train', numpy.array([multipleii] * 2), 'zenith', numpy.array([zenithii] * 2), folder, title)

        print('\ttesting run...')
        print('zenith',  zenith)
        print('scatter', scatter)
        print('single', single)
        print('mulitple', multiple)

        matrixii = matrix

        # plot all zeniths
        zeniths = [bracket[0] + (bracket[-1] - bracket[0]) * (chunk / steps) for chunk in range(steps)]

        # construc cosines
        cosines = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zeniths])
        cosines = cosines.reshape(-1, 1)

        # construct rayleights
        reflectivities = numpy.array([albedo] * len(cosines))
        reflectivities = reflectivities.reshape(-1, 1)

        # construct rayleights
        rayleighs = numpy.array([scatter] * len(cosines))
        rayleighs = rayleighs.reshape(-1, 1)

        # construct simpoles
        simples = numpy.array([single] * len(cosines))
        simples = simples.reshape(-1, 1)

        # perfrom transformations
        machines = storage['machines']
        for transform in storage['transforms']:

            # undo transform
            reflectivities = self._transform(reflectivities, transform, machines['albedo'][transform], inputs=True)
            cosines = self._transform(cosines, transform, machines['cosine'][transform], inputs=True)
            rayleighs = self._transform(rayleighs, transform, machines['scatter'][transform], inputs=True)
            simples = self._transform(simples, transform, machines['single'][transform], inputs=True)

        # make matrix
        reflectivities = reflectivities.squeeze()
        rayleighs = rayleighs.squeeze()
        cosines = cosines.squeeze()
        simples = simples.squeeze()
        matrix = numpy.vstack([reflectivities, rayleighs, cosines, simples]).transpose(1, 0)

        print('matrrix', matrix.shape, matrix.min(), matrix.max())

        # make predictions
        prediction = perceptor.predict(matrix)

        # undo transforms
        machines = storage['machines']
        for transform in storage['transforms'][::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines['multiple'][transform], inputs=False)

        print('cosines', cosines.min(), cosines.max())
        print('rayleigh', rayleighs.min(), rayleighs.max())
        print('zeniths', numpy.array(zeniths).min(), numpy.array(zeniths).max())
        print('predictions', prediction.min(), prediction.max(), prediction.shape)

        # make plot
        title = 'Testing_Prediction_vs_Zenith, Rayleigh: {}, {}'.format(scatter, index)
        folder = 'model/model_scrounge_test_zenith_{}_{}_{}.h5'.format(round(float(scatter), 3), index, round(float(zenith), 2))
        self.squid.ink('prediction_test', prediction, 'zenith', zeniths, folder, title)

        # also plot test point
        title = 'Testing Example: {}, {}'.format(scatter, index)
        folder = 'model/model_test_zenith_{}_{}_{}.h5'.format(round(float(scatter), 3), index, round(float(zenith), 2))
        self.squid.ink('test', numpy.array([multiple] * 2), 'zenith', numpy.array([zenith] * 2), folder, title)

        return matrix, matrixii

    def query(self, year, month, day):
        """Print most recent model information.

        Arguments:
            year: int, year
            month: int, month
            day: int, day

        Returns:
            None
        """

        # get all models
        models = self._see('{}/machines'.format(self.sink))
        models.sort(reverse=True)

        # subset date
        query = '{}m{}{}'.format(year, str(month).zfill(2), str(day).zfill(2))
        models = [model for model in models if query in model]

        # create records
        records = []
        for index, model in enumerate(models):

            # load model
            storage = pickle.load(open(model, 'rb'))

            # create record
            record = {}
            record['index'] = index
            record['layers'] = storage['parameters'].get('hidden_layer_sizes', '')
            record['brackets'] = storage.get('brackets', '')
            record['options'] = storage.get('options', {})
            record['sizes'] = storage.get('sizes', '')
            record['transforms'] = storage.get('transforms', '')
            record['features'] = storage.get('features', '')
            record['augmentation'] = storage.get('augmentation', {})
            record['verification'] = storage.get('verification', 0.0)
            record['validation'] = storage.get('validation', 0.0)
            record['percent'] = storage.get('percent', 0.0)
            record['maximum'] = storage.get('maximum', 0.0)
            record['percentii'] = storage.get('percentii', 0.0)
            record['maximumii'] = storage.get('maximumii', 0.0)
            record['duration'] = storage.get('duration', 0.0)
            record['rates'] = storage.get('rates', []) or storage['parameters'].get('learning_rate_init')
            record['loss'] = storage.get('loss', 0.0)
            record['iterations'] = storage.get('iterations', None) or len(storage.get('curve', []))
            record['double'] = storage.get('double', '')
            record['alpha'] = storage['parameters'].get('alpha')
            record['tolerance'] = storage['parameters'].get('tol')
            record['batch'] = storage['parameters'].get('batch_size')
            record['epsilon'] = storage['parameters'].get('epsilon')
            record['fraction'] = storage.get('fraction', 1.0)
            record['activation'] = storage['parameters'].get('activation')
            record['orbits'] = list(storage.get('validations', {}).values())
            record['flow'] = storage.get('flow', {})
            record['name'] = storage.get('name', '_')
            record['hypers'] = storage.get('hypers', {})

            # append
            records.append(record)

        return records

    def rake(self, data, tag, tracer, abscissa=None, brackets=None):
        """Perform PCA decomposition to generate overviews of data pca vectors.

        Arguments:
            data: numpy array
            tracer: list of floats, tracer colummn
            abscissa: list of float, the abscissa scale for the vectors
            brackets: list of ( float, float ) tuples, tracer brackets

        Returns:
            None
        """

        # begin report
        report = [self._print('PCA_Decomposition_Report')]
        report.append(self._print(tag))

        # perform pca decomposition on ozones (middle column only)
        machine = PCA(n_components=5, random_state=1)
        matrix = machine.fit_transform(data)
        variances = machine.explained_variance_ratio_
        report.append(self._print('variance: {}'.format(machine.explained_variance_ratio_)))

        # begin features
        features = []

        # if there are no bracktes
        if brackets is None:

            # set percentiles for 8 bins
            percentiles = [0, 12.5, 25, 37.5, 50, 62.5, 75, 87.5, 100]
            bounds = [numpy.percentile(tracer, percentile) for percentile in percentiles]
            brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
            brackets = [bracket for bracket in brackets if bracket[0] != bracket[1]]

        # construct bracket labels
        swaths = ['{} to {}'.format(round(first, 4), round(second, 4)) for first, second in brackets]

        # for each component
        for component in range(1, 5):

            # construct heatmap for ozone
            name = 'heatmap_pca_{}_0x{}'.format(tag, component)
            array = numpy.array([tracer, matrix[:, component], matrix[:, 0]])
            attributes = {'brackets': brackets, 'variances': variances, 'labels': swaths}
            feature = Feature(['Categories', name], array, attributes=attributes)
            features.append(feature)

        # for each ozone component
        for index, component in enumerate(machine.components_):

            # add for radiance matrix
            name = '{}_pca_{}'.format(tag, index)
            array = machine.components_[index]
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # create abscissa
        if abscissa is None:

            # create abscissa
            abscissa = list(range(data.shape[1]))

        # add independentvariable
        name = '{}_pca_abscissa'.format(tag)
        array = numpy.array(abscissa)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash hdf file
        self._make('{}/rakes'.format(self.sink))
        destination = '{}/rakes/Ozone_PCAs_{}_{}.h5'.format(self.sink, tag, self.tag)
        self.stash(features, destination, 'Data')

        # jot down report
        self._make('{}/reports'.format(self.sink))
        destination = '{}/reports/Ozone_PCA_{}_{}_report.txt'.format(self.sink, tag, self.tag)
        self._jot(report, destination)

        return None

    def reflect(self, data, zeniths=[0], trial=None):
        """Examine reflectivity functions in the datasets.

        Arguments:
            data: dict of arrays
            zeniths: list of ints, zeniths to check
            trial: dict of array

        Returns:
            None
        """

        # grab multiple scattering
        multiple = data['multiple']
        single = data['single']
        albedo = data['albedo']
        scatter = data['scatter']
        latitude = data['latitude']

        # reshape
        months = data['months'].shape[0]
        latitudes = data['latitudes'].shape[0]
        albedos = data['albedos'].shape[0]
        multiple = multiple.reshape(latitudes, months, albedos, -1)
        single = single.reshape(latitudes, months, albedos, -1)
        albedo = albedo.reshape(latitudes, months, albedos, -1)
        scatter = scatter.reshape(latitudes, months, albedos, -1)

        # create latitude record
        latitude = numpy.array([row.tolist().index(1) for row in latitude])
        latitude = latitude.reshape(latitudes, months, albedos, -1)

        # plot subsets
        for zenith in zeniths:

            # get subset by month and latitude
            multipleii = multiple[:, :, :, zenith]
            singleii = single[:, :, :, zenith]
            albedoii = albedo[:, :, :, zenith]
            scatterii = albedo[:, :, :, zenith]

            # add fill value as last albedo
            fill = numpy.ones((latitudes, months, 1)) * -9999
            blank = numpy.ones((latitudes, months, 1))

            # stack
            singleii = numpy.vstack([singleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            multipleii = numpy.vstack([multipleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            albedoii = numpy.vstack([albedoii.transpose(2, 1, 0), blank.transpose(2, 1, 0)]).transpose(2, 1, 0)

            # plot vs albedo
            title = 'MS vs Albedo, Zenith {}'.format(zenith)
            address = 'reflectivities/Albedos_Zen_{}.h5'.format(self._pad(zenith))
            self.squid.ink('ms_reflect', multipleii.flatten(), 'albedo', albedoii.flatten(), address, title)

            # plot vs albedo
            title = 'SS vs Albedo, Zenith {}'.format(zenith)
            address = 'reflectivities/Albedos_Zen_SS_{}.h5'.format(self._pad(zenith))
            self.squid.ink('ss_reflect', singleii.flatten(), 'albedo', albedoii.flatten(), address, title)

            # get subset at southern pole
            multipleii = multiple[4:5, 0:1, :, zenith]
            singleii = single[4:5, 0:1, :, zenith]
            albedoii = albedo[4:5, 0:1, :, zenith]
            scatterii = albedo[4:5, 0:1, :, zenith]

            # add fill value as last albedo
            fill = numpy.ones((1, 1, 1)) * -9999
            blank = numpy.ones((1, 1, 1))

            # stack
            singleii = numpy.vstack([singleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            multipleii = numpy.vstack([multipleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            albedoii = numpy.vstack([albedoii.transpose(2, 1, 0), blank.transpose(2, 1, 0)]).transpose(2, 1, 0)

            # plot vs albedo
            title = 'MS vs Albedo, Zenith {}'.format(zenith)
            address = 'reflectivities/Albedos_Zen_poles_{}.h5'.format(self._pad(zenith))
            self.squid.ink('ms_reflect', multipleii.flatten(), 'albedo', albedoii.flatten(), address, title)

            # get subset at southern pole
            multipleii = multiple[25:26, 5:6, :, zenith]
            singleii = single[25:26, 5:6, :, zenith]
            albedoii = albedo[25:26, 5:6, :, zenith]
            scatterii = albedo[25:26, 5:6, :, zenith]

            # add fill value as last albedo
            fill = numpy.ones((1, 1, 1)) * -9999
            blank = numpy.ones((1, 1, 1))

            # stack
            singleii = numpy.vstack([singleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            multipleii = numpy.vstack([multipleii.transpose(2, 1, 0), fill.transpose(2, 1, 0)]).transpose(2, 1, 0)
            albedoii = numpy.vstack([albedoii.transpose(2, 1, 0), blank.transpose(2, 1, 0)]).transpose(2, 1, 0)

            # plot vs albedo
            title = 'MS vs Albedo, Zenith {}'.format(zenith)
            address = 'reflectivities/Albedos_Zen_equator_{}.h5'.format(self._pad(zenith))
            self.squid.ink('ms_reflect', multipleii.flatten(), 'albedo', albedoii.flatten(), address, title)

        # if trial
        if trial:

            # get subset by month and latitude
            multipleiii = trial['multiple'].reshape(albedos, -1).transpose(1, 0)
            singleiii = trial['single'].reshape(albedos, -1).transpose(1, 0)
            albedoiii = trial['albedo'].reshape(albedos, -1).transpose(1, 0)
            samples = multipleiii.shape[0]

            # add fill value as last albedo
            fill = numpy.ones((samples, 1)) * -9999
            blank = numpy.ones((samples, 1))

            # stack
            multipleiv = numpy.vstack([multipleiii.transpose(1, 0), fill.transpose(1, 0)]).transpose(1, 0)
            singleiv = numpy.vstack([singleiii.transpose(1, 0), fill.transpose(1, 0)]).transpose(1, 0)
            albedoiv = numpy.vstack([albedoiii.transpose(1, 0), blank.transpose(1, 0)]).transpose(1, 0)

            # plot vs albedo
            title = 'MS vs Albedo, Orbit 48687'
            address = 'reflectivities/Albedos_48687.h5'
            self.squid.ink('ms_reflect', multipleiv.flatten(), 'albedo', albedoiv.flatten(), address, title)

            # plot vs albedo
            title = 'MS vs Albedo, Orbit 48687'
            address = 'reflectivities/Albedos_SS_48687.h5'
            self.squid.ink('ss_reflect', singleiv.flatten(), 'albedo', albedoiv.flatten(), address, title)

        # reshape mulitple for one column / zenith, albedo
        multiple = multiple.reshape(latitudes * months, albedos, -1)
        scatter = scatter.reshape(latitudes * months, albedos, -1)
        albedo = albedo.reshape(latitudes * months, albedos, -1)
        latitude = latitude.reshape(latitudes * months, albedos, -1)

        # argsort each column
        grid = numpy.zeros(multiple.shape)
        for reflectivity in range(multiple.shape[1]):

            # for each zenith
            for angle in range(multiple.shape[2]):

                # argsort
                column = multiple[:, reflectivity, angle].argsort()
                pairs = [(position, sample) for position, sample in enumerate(column)]
                pairs.sort(key=lambda pair: pair[1])
                rank = [pair[0] for pair in pairs]
                grid[:, reflectivity, angle] = rank

        # average position for all samples
        averages = numpy.array([grid[index].mean() for index in range(grid.shape[0])])
        deviations = numpy.array([grid[index].std() for index in range(grid.shape[0])])
        modes = numpy.array([scipy.stats.mode(grid[index].flatten())[0][0] for index in range(grid.shape[0])])

        # plot crisscroses at each zenith
        for zenith in zeniths:

            # subset grid at zenith
            subset = grid[:, :, zenith]

            # get positional shifts
            shift = subset[:, 0] - subset[:, 10]
            first = shift.argsort()[0]
            last = shift.argsort()[-1]

            # make histogram of shifts
            pad = self._pad(zenith)
            address = 'reflectivities/Position_Shifts_Zenith_{}.h5'.format(pad)
            title = 'Position shift histogram, zenith {}'.format(pad)
            self.squid.ripple('position_shift_zenith_{}'.format(pad), shift, address, title)

            # plot first MS
            address = 'reflectivities/Mulitple_Scattering_Zenith_{}_Sample_{}.h5'.format(pad, self._pad(first, 3))
            title = 'Mulitple Scatter, Zenith {}, Sample {}'.format(pad, self._pad(first, 3))
            name = 'multiple_scatter'
            nameii = 'reflectivity'
            self.squid.ink(name, multiple[first, :, zenith], nameii, albedo[first, :, zenith], address, title)

            # plot last MS
            address = 'reflectivities/Mulitple_Scattering_Zenith_{}_Sample_{}.h5'.format(pad, self._pad(last, 3))
            title = 'Mulitple Scatter, Zenith {}, Sample {}'.format(pad, self._pad(last, 3))
            name = 'multiple_scatter'
            nameii = 'reflectivity'
            self.squid.ink(name, multiple[last, :, zenith], nameii, albedo[last, :, zenith], address, title)

        # plot pca at each zenith, colored by latitude
        for zenith in zeniths:

            # get subset of albedo functions
            strips = multiple[:, :, zenith]

            # get subset of latitude
            slats = latitude[:, 0, zenith]

            # create pca decomposer
            decomposer = PCA(n_components=2)
            decomposition = decomposer.fit_transform(strips)

            # plot last MS
            pad = self._pad(zenith)
            address = 'reflectivities/Mulitple_Scattering_Zenith_{}_FL.h5'.format(pad)
            title = 'PCA of MS, Zenith {}'.format(pad)
            name = 'multiple_scatter'
            ordinate = strips[:, -1]
            abscissa = strips[:, 0]
            bounds = [0, 5, 10, 15, 20, 25, 30, 35]
            self.squid.shimmer('latitude', slats, 'ordinate', ordinate, 'abscissa', abscissa, address, bounds, title)

            # plot last MS
            pad = self._pad(zenith)
            address = 'reflectivities/Mulitple_Scattering_Zenith_{}_FL_block.h5'.format(pad)
            title = 'PCA of MS, Zenith {}'.format(pad)
            name = 'multiple_scatter'
            ordinate = strips[:, -1]
            abscissa = strips[:, 0]
            bounds = [0, 35]
            self.squid.shimmer('latitude', slats, 'ordinate', ordinate, 'abscissa', abscissa, address, bounds, title)

            # do same for trial
            if trial:

                # get multiple
                multipleii = trial['multiple']
                multipleii = multipleii.reshape(11, -1).transpose(1, 0)

                # get decomposition
                #decompositionii = decomposer.transform(multipleii)

                # plot last MS
                pad = self._pad(zenith)
                address = 'reflectivities/Mulitple_Scattering_Zenith_Trial_{}_PCA.h5'.format(pad)
                title = 'PCA of MS, Zenith {}'.format(pad)
                name = 'multiple_scatter'
                ordinate = multipleii[:, -1]
                abscissa = multipleii[:, 0]
                bounds = [0, 2]
                ones = numpy.ones((multipleii.shape[0],))
                self.squid.shimmer('latitude', ones, 'ordinate', ordinate, 'abscissa', abscissa, address, bounds, title)

        # make reflection object
        reflection = {'multiple': multiple, 'grid': grid, 'modes': modes, 'scatter': scatter, 'albedo': albedo}
        reflection.update({'single': single})

        return reflection

    def remember(self, year, month, day):
        """Print most recent model information.

        Arguments:
            year: int, year
            month: int, month
            day: int, day

        Returns:
            None
        """

        # get model records
        records = self.query(year, month, day)

        # begin report
        report = ['NN experiments', '']

        # for each record
        for record in records:

            # for fields
            report += ['']
            report += ['{})'.format(record['index'])]
            fields = ['name', 'orbits', 'verification', 'percent', 'maximum', 'validation', 'percentii']
            fields += ['maximumii', 'features', 'brackets', 'augmentation', 'transforms', 'sizes', 'layers']
            fields += ['options', 'fraction', 'flow', 'activation']
            fields += ['epsilon', 'tolerance', 'batch', 'alpha', 'duration', 'iterations', 'loss']
            for field in fields:

                # add to report
                report += ['{} = {}'.format(field, str(record[field]))]

        # jot report
        destination = '{}/reports/NN_experiments_report_{}_{}_{}.txt'.format(self.sink, year, month, day)
        self._jot(report, destination)

        # subset records with validation scores
        records = [record for record in records if len(record['orbits']) > 0]

        # plot experimental median vs maximum error
        medians = numpy.array([record['orbits'][0][0] for record in records])
        maximums = numpy.array([record['orbits'][0][1] for record in records])
        losses = numpy.array([record['loss'] for record in records])

        # create auxiliaries
        auxiliaries = {'zzz_number': [record['index'] for record in records]}

        # plot madians vs maximum
        address = 'experiments/NN_experiments_{}_{}_{}.h5'.format(year, month, day)
        title = 'Median error vs Maximum error'
        self.squid.ink('median_error', medians, 'maximum_error', maximums, address, title, auxiliaries)

        # plot madians vs loss
        address = 'experiments/NN_losses_{}_{}_{}.h5'.format(year, month, day)
        title = 'Median error vs Loss'
        self.squid.ink('median_error', medians, 'loss', losses, address, title, auxiliaries)

        # plot maximum vs loss
        address = 'experiments/NN_max_losses_{}_{}_{}.h5'.format(year, month, day)
        title = 'Maximum error vs Loss'
        self.squid.ink('maximum_error', maximums, 'loss', losses, address, title, auxiliaries)
        #
        # # plot experiments
        # features = []
        # destination = '{}/experiments/NN_experiments_{}_{}_{}.h5'.format(self.sink, year, month, day)
        #
        # # only get records with validation scores
        # records = [record for record in records if len(record['orbits']) > 0]
        #
        # # add average error
        # name = 'median_validation_error'
        # array = numpy.array([record['orbits'][0][0] for record in records])
        # features.append(Feature(['Categories', name], array))
        #
        # # add maximum aboslute error
        # name = 'maximum_validation_error'
        # array = numpy.array([record['orbits'][0][1] for record in records])
        # features.append(Feature(['IndependentVariables', name], array))
        #
        # # stash
        # self.stash(features, destination)

        return None

    def sample(self):
        """Create plots of pacc sample for confidencs.

        Arguments:
            None

        Returns:
            None
        """

        # set confidences
        confidences = (0.90, 0.95, 0.99, 0.995)
        errors = [0.0001 * (index + 1) for index in range(1000)]

        # for each confidence
        lines = []
        for confidence in (0.90, 0.95, 0.99, 0.995):

            # for each error
            limits = []
            for error in errors:

                # compute sampling limit
                limit = (1 / (2 * error ** 2)) * numpy.log(2 / (1 - confidence))
                limits.append(numpy.log10(limit))

            # append line
            lines.append(limits)

        # plot the graph
        address = 'smart/PACC_Sampling_Limits.h5'
        title = 'SPACC sampling limits'
        self.squid.splatter('log10_samples', lines, 'error', errors, address, title)

        return None

    def scan(self, exam, validation):
        """Create pca heatmaps of errors vs layers pca.

        Arguments:
            exam: examiniation data
            validation: validation data

        Returns:
            None
        """

        # begin features
        features = []

        # grab matrix and percent errors
        matrix = exam['matrix']
        errors = abs(exam['percents']).max(axis=1)
        target = exam['target']

        # set percentiles for 8 bins
        bounds = [0, 0.1, 0.2, 0.3, 0.4]
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} % to {} %'.format(first, second) for first, second in brackets]

        # construct heatmap for ozone
        name = 'heatmap_climatology_errors'
        array = numpy.array([errors, matrix[:, 1], matrix[:, 0]])
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # grab matrix and percent errors
        matrixii = validation['matrix']
        errorsii = abs(validation['percents']).max(axis=1)
        targetii = validation['target']

        # set percentiles for 8 bins
        bounds = [0, 10, 50, 100, 150]
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} % to {} %'.format(first, second) for first, second in brackets]

        # construct heatmap for ozone
        name = 'heatmap_validation_errors'
        array = numpy.array([errorsii, matrixii[:, 1], matrixii[:, 0]])
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # set comparisons for both ozone pcas
        bounds = [0, 2, 4]
        levels = numpy.array([1 for _ in matrix] + [3 for _ in matrixii])
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} % to {} %'.format(first, second) for first, second in brackets]

        # construct heatmap for ozone
        combo = numpy.vstack([matrix, matrixii])
        name = 'heatmap_ozone_pca_comparison'
        array = numpy.array([levels, combo[:, 1], combo[:, 0]])
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # set comparisons for both ozone pcas
        bounds = [0, 2, 4]
        levels = numpy.array([1 for _ in target] + [3 for _ in targetii])
        brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        labels = ['{} % to {} %'.format(first, second) for first, second in brackets]

        # construct heatmap for ozone
        combo = numpy.vstack([target, targetii])
        name = 'heatmap_radiance_pca_comparison'
        array = numpy.array([levels, combo[:, 1], combo[:, 0]])
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # add error boost function
        name = 'error_boost'
        #array = exam['data']['boost']
        feature = Feature(['Categories', name], array)
        features.append(feature)
        name = 'wavelength'
        array = exam['data']['wavelength']
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)
        #
        # add validation histogram
        name = 'histogram_validation_errors'
        array = validation['percents'].flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # make file
        destination = '{}/scans/NN_climatology_PCA_vector_maps.h5'.format(self.sink)
        self.stash(features, destination)

        return None

    def scatter(self, train, test):
        """Make scatter plots of training vs zenith for all albedos.

        Arguments:
            train: dict of training data
            test: dict of testing data

        Returns:
            None
        """

        # for each albedo
        for reflectivity in range(0, 11):

            # collect single and multiple from training
            single = train['single'].flatten()
            multiple = train['multiple'].flatten()
            albedo = train['albedo'].flatten()
            zenith = train['zenith'].flatten()
            ratio = single / multiple

            # create mask
            mask = (albedo == (reflectivity / 10))

            # plot training ratio
            destination = 'ratios/Training_Reflectivity_{}.h5'.format(self._pad(reflectivity))
            title = 'Ratio of single scattering to multiple scatter, orbit 48687, wavelength = 379.0'
            self.squid.ink('ss_ms_ratio', ratio[mask], 'solar zenith angle', zenith[mask], destination, title)

            # collect single and multiple from testing
            single = test['single'].flatten()
            multiple = test['multiple'].flatten()
            albedo = test['albedo'].flatten()
            zenith = test['zenith'].flatten()
            ratio = single / multiple

            # create mask
            mask = (albedo == (reflectivity / 10))

            # plot training ratio
            destination = 'ratios/Validation_Reflectivity_{}.h5'.format(self._pad(reflectivity))
            title = 'Ratio of single scattering to multiple scatter, orbit 48687, wavelength = 379.0'
            self.squid.ink('ss_ms_ratio', ratio[mask], 'solar zenith angle', zenith[mask], destination, title)

        return None

    def scratch(self, data, field, abscissa, sizes):
        """Decompose data by PCA and plot recomstrucctions.

        Arguments:
            data: dict of numpy arrays, dataset
            field: str, particular field.
            sizes: list of ints, the pca dimensions
            absissca: field for abscissa

        Returns:
            None
        """

        # begin collection reconstructions
        reconstructions = []

        # grab the data for the matrix
        matrix = data[field]

        # for each size
        for size in sizes:

            # construct pcas
            machine = self._build(matrix, '_pca_', size)['machine']
            decomposition = machine.transform(matrix)

            # recontstruct
            reconstruction = machine.inverse_transform(decomposition)
            reconstructions.append(reconstruction)

        # begim features
        features = []

        # begin accumulationsacross all samples
        stacks = []
        errors = []

        # create reconstruction plots
        for index, sample in enumerate(matrix):

            # beginn stacks
            stack = []
            error = []

            # for each reconstruction
            for size, reconstruction in zip(sizes, reconstructions):

                # add to the stack
                stack.append(reconstruction[index])
                error.append(100 * ((reconstruction[index] / sample) - 1))

            # add to stacks
            stacks.append(stack)
            errors.append(error)

            # add original
            stack.append(sample)
            array = numpy.vstack(stack)
            name = 'pca_decomposition_{}'.format(self._pad(index))
            feature = Feature(['Categories', name], array, attributes={'sizes': sizes})
            features.append(feature)

            # add percent error
            error.append(100 * ((sample / sample) - 1))
            array = numpy.vstack(error)
            name = 'pca_decomposition_error_{}'.format(self._pad(index))
            feature = Feature(['Categories', name], array, attributes={'sizes': sizes})
            features.append(feature)

        # add percent errors across all samples
        array = numpy.hstack([numpy.vstack(error) for error in errors])
        name = 'pca_decomposition_error_all'
        feature = Feature(['Categories', name], array, attributes={'sizes': sizes})
        features.append(feature)

        # add absicssa
        array = data[abscissa]
        name = abscissa
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # add absicssa across all
        array = numpy.hstack([data[abscissa] for _ in range(matrix.shape[0])])
        name = '{}_all'.format(abscissa)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # add sizes
        array = numpy.array(sizes)
        name = 'sizes'
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash features
        self._make('{}/vectors'.format(self.sink))
        destination = '{}/vectors/PCA_Decomposition_{}.h5'.format(self.sink, field)
        self.stash(features, destination)

        return None

    def scrounge(self, storage, exam, zenith, top=5, steps=100):
        """Probe the model at specific zenith sample.

        Arguments:
            storage: perception model object
            exam: examination object
            zenith: zenith again of sample for probing
            top: number of top entries

        Returns:
            None
        """

        # grab the validation data set
        test = exam['validations'][0]['data']

        # grab the train data set
        train = storage['data']

        # instantiate the model
        perceptor = load_model(storage['model'])

        # find the sample closest to the zenith angle
        sample = ((test['zenith'].flatten() - zenith) ** 2).argsort()[0]

        # also get the scattering amount and radiance
        scatter = test['scatter'].flatten()[sample]
        radiance = test['radiance'].flatten()[sample]
        #radiance = 10 ** radiance

        # reset zenith to actual measurement
        zenith = test['zenith'].flatten()[sample]

        # find the top 100 members from the training that are closest based on scattering
        closest = ((train['scatter'].flatten() - scatter) ** 2).argsort()[:100].tolist()

        # construct zenith mappings, and sort by zenith closeness
        records = [(index, train['zenith'].flatten()[index]) for index in closest]
        records.sort(key=lambda record: (record[1] - zenith) ** 2)
        best = [record[0] for record in records[:top]]
        #indices = best[:3]

        # find the top 100 members from the training that are closest based on zenith
        closest = ((train['zenith'].flatten() - zenith) ** 2).argsort()[:100].tolist()

        # construct zenith mappings, and sort by zenith closeness
        records = [(index, train['scatter'].flatten()[index]) for index in closest]
        records.sort(key=lambda record: (record[1] - scatter) ** 2)
        best = [record[0] for record in records[:top]]
        indices = best[:5]

        # for each index
        bracket = (0, 90)
        for index in indices:

            # grab the charaterisicts
            zenithii = train['zenith'].flatten()[index]
            scatterii = train['scatter'].flatten()[index]
            radianceii = train['radiance'].flatten()[index]
            #radianceii = 10 ** radianceii

            # plot all zeniths
            zeniths = [bracket[0] + (bracket[-1] - bracket[0]) * (chunk / steps) for chunk in range(steps)]

            print('\ttraining run {}...'.format(index))
            print('zenith',  zenithii)
            print('scatter', scatterii)
            print('radiance', radianceii)

            cosines = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zeniths])
            cosines = cosines.reshape(-1, 1)

            # construct matrix
            rayleighs = numpy.array([scatterii] * len(cosines))
            rayleighs = rayleighs.reshape(-1, 1)

            # perfrom transformations
            machines = storage['machines']
            for transform in storage['transforms']:

                # undo transform
                cosines = self._transform(cosines, transform, machines['cosine'][transform], inputs=True)
                rayleighs = self._transform(rayleighs, transform, machines['scatter'][transform], inputs=True)

            # make matrix
            rayleighs = rayleighs.squeeze()
            cosines = cosines.squeeze()
            matrix = numpy.vstack([rayleighs, cosines]).transpose(1, 0)

            print('matrrix', matrix.shape, matrix[:, 0].min(), matrix[:, 0].max())

            # make predictions
            prediction = perceptor.predict(matrix)

            # undo transforms
            machines = storage['machines']
            for transform in storage['transforms'][::-1]:

                # undo transform
                prediction = self._reform(prediction, transform, machines['radiance'][transform], inputs=False)

            # # take to power
            # prediction = 10 ** prediction

            print('cosines', cosines.min(), cosines.max())
            print('rayleigh', rayleighs.min(), rayleighs.max())
            print('zeniths', numpy.array(zeniths).min(), numpy.array(zeniths).max())
            print('predictions', prediction.min(), prediction.max(), prediction.shape)

            # make plot
            title = 'Prediction_vs_Zenith, Rayleigh: {}, {}'.format(scatter, index)
            folder = 'model/model_scrounge_zenith_{}_{}_{}.h5'.format(round(scatterii, 3), index, round(zenithii, 2))
            self.squid.ink('prediction', prediction, 'zenith', zeniths, folder, title)

            # also plot training point
            title = 'Training Example: {}, {}'.format(scatter, index)
            folder = 'model/model_train_zenith_{}_{}_{}.h5'.format(round(scatterii, 3), index, round(zenithii, 2))
            self.squid.ink('train', numpy.array([radianceii] * 2), 'zenith', numpy.array([zenithii] * 2), folder, title)

        print('\ttesting run...')
        print('zenith',  zenith)
        print('scatter', scatter)
        print('radiance', radiance)

        # # grab the est charaterisicts
        # zenith = train['zenith'].flatten()[index]
        # scatterii = train['scatter'].flatten()[index]
        # radianceii = train['radiance'].flatten()[index]

        # plot all zeniths
        zeniths = [bracket[0] + (bracket[-1] - bracket[0]) * (chunk / steps) for chunk in range(steps)]

        print('\ttesting run...')
        print('zenith',  zenith)
        print('scatter', scatter)
        print('radiance', radiance)

        cosines = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zeniths])
        cosines = cosines.reshape(-1, 1)

        # construct matrix
        rayleighs = numpy.array([scatter] * len(cosines))
        rayleighs = rayleighs.reshape(-1, 1)

        # perfrom transformations
        machines = storage['machines']
        for transform in storage['transforms']:

            # undo transform
            cosines = self._transform(cosines, transform, machines['cosine'][transform], inputs=True)
            rayleighs = self._transform(rayleighs, transform, machines['scatter'][transform], inputs=True)

        # make matrix
        rayleighs = rayleighs.squeeze()
        cosines = cosines.squeeze()
        matrix = numpy.vstack([rayleighs, cosines]).transpose(1, 0)

        print('matrrix', matrix.shape, matrix[:, 0].min(), matrix[:, 0].max())

        # make predictions
        prediction = perceptor.predict(matrix)

        # undo transforms
        machines = storage['machines']
        for transform in storage['transforms'][::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines['radiance'][transform], inputs=False)

        # # take to power
        # prediction = 10 ** prediction

        print('cosines', cosines.min(), cosines.max())
        print('rayleigh', rayleighs.min(), rayleighs.max())
        print('zeniths', numpy.array(zeniths).min(), numpy.array(zeniths).max())
        print('predictions', prediction.min(), prediction.max(), prediction.shape)

        # make plot
        title = 'Testing_Prediction_vs_Zenith, Rayleigh: {}, {}'.format(scatter, index)
        folder = 'model/model_scrounge_test_zenith_{}_{}_{}.h5'.format(round(float(scatter), 3), index, round(float(zenith), 2))
        self.squid.ink('prediction_test', prediction, 'zenith', zeniths, folder, title)

        # # also plot training point
        # title = 'Training Example: {}, {}'.format(scatter, index)
        # folder = 'model/model_train_zenith_{}_{}_{}.h5'.format(round(scatterii, 2), index, round(zenithii, 2))
        # self.squid.ink('train', numpy.array([radianceii] * 2), 'zenith', numpy.array([zenithii] * 2), folder, title)

        # also plot test point
        title = 'Testing Example: {}, {}'.format(scatter, index)
        folder = 'model/model_test_zenith_{}_{}_{}.h5'.format(round(float(scatter), 3), index, round(float(zenith), 2))
        self.squid.ink('test', numpy.array([radiance] * 2), 'zenith', numpy.array([zenith] * 2), folder, title)

        return None

    def scrutinize(self, examination):
        """Compare the datasets in the training data to that in the validation set.

        Argument:
            examination: examination dict

        Returns:
            None
        """

        # collect training data
        data = examination['data']
        scatter = data['scatter']
        surface = data['surface']
        radiance = data['radiance']
        zenith = data['zenith']

        # reconstruct latitude
        latitude = data['latitude']
        latitudes = data['latitudes']
        latitude = latitude @ latitudes

        # default errors to zero for training data
        zeros = numpy.zeros(scatter.shape[0])

        # collect validations
        validations = examination['validations']
        scatterii = []
        surfaceii = []
        radianceii = []
        latitudeii = []
        zenithii = []
        percents = []
        for validation in validations:

            #  get percent
            percent = 100 * ((validation['prediction'] / validation['original']) - 1)
            percents.append(percent)

            # append data
            scatterii.append(validation['data']['scatter'])
            latitudeii.append(validation['data']['latitude'][:, 1:2])
            radianceii.append(validation['data']['radiance'])
            surfaceii.append(validation['data']['surface'].transpose(1, 0))
            zenithii.append(validation['data']['zenith'])

        # make stacks
        scatterii = numpy.vstack(scatterii)
        radianceii = numpy.vstack(radianceii)
        percents = numpy.vstack(percents)
        latitudeii = numpy.vstack(latitudeii)
        surfaceii = numpy.vstack(surfaceii)
        zenithii = numpy.vstack(zenithii)

        # create heatmaps of training data, with no error
        names = ['latitude', 'radiance', 'surface']
        arrays = [latitude, radiance, surface]
        bounds = [-0.1, 0.1]
        for name, array in zip(names, arrays):

            # make heatmap
            destination = 'pressures/LUT_Rayleigh_vs_{}.h5'.format(name)
            title = 'Rayleigh Column Density vs {}'.format(name)
            self.squid.shimmer('percent', zeros, 'rayleigh', scatter, name, array, destination, bounds, title)

        # create heatmaps of training data, with error tracer
        names = ['latitude', 'radiance', 'surface']
        arrays = [latitudeii, radianceii, surfaceii]
        bounds = [-0.25, -0.1, -0.025, 0.025, 0.1, 0.25]
        for name, array in zip(names, arrays):

            # make heatmap
            destination = 'pressures/Orbital_Rayleigh_vs_{}.h5'.format(name)
            title = 'Rayleigh Column Density vs {}'.format(name)
            self.squid.shimmer('percent', percents, 'rayleigh', scatterii, name, array, destination, bounds, title)

        # make 3d plot
        pyplot.clf()
        ax = Axes3D(pyplot.gcf())
        ax.plot(scatter.flatten(), zenith.flatten(), radiance.flatten(), 'gx')
        brackets = [(0, 12.5, 'rx'), (12.5, 25, 'cx'), (25, 37.5, 'mx'), (37.5, 50, 'kx')]
        brackets += [(50, 62.5, 'kx'), (62.5, 75, 'mx'), (75, 87.5, 'cx'), (87.5, 100, 'rx')]
        for left, right, color in brackets:

            # make percentiles
            first = numpy.percentile(percents, left)
            last = numpy.percentile(percents, right)

            # get mask
            mask = (percents > first) & (percents < last)
            ax.plot(scatterii[mask].flatten(), zenithii[mask].flatten(), radianceii[mask].flatten(), color)

        # show
        #pyplot.xlim(0.5, 0.4)
        #pyplot.ylim(20, 50)
        #pyplot.zlim(-1.5, -1.3)
        pyplot.show()

        return None

    def sculpt(self, examination, field='transmittance'):
        """Compare the datasets in the training data to that in the validation set.

        Argument:
            examination: examination dict

        Returns:
            None
        """

        # collect training data
        data = examination['data']
        scatter = data['scatter']
        surface = data['surface']
        radiance = data[field]
        zenith = data['zenith']

        # reconstruct latitude
        latitude = data['latitude']
        latitudes = data['latitudes']
        latitude = latitude @ latitudes

        # default errors to zero for training data
        zeros = numpy.zeros(scatter.shape[0])

        # collect validations
        validations = examination['validations']
        scatterii = []
        surfaceii = []
        radianceii = []
        latitudeii = []
        zenithii = []
        percents = []
        for validation in validations:

            #  get percent
            percent = 100 * ((validation['prediction'] / validation['original']) - 1)
            percents.append(percent)

            # append data
            scatterii.append(validation['data']['scatter'])
            latitudeii.append(validation['data']['latitude'][:, 1:2])
            radianceii.append(validation['data'][field])
            surfaceii.append(validation['data']['surface'].transpose(1, 0))
            zenithii.append(validation['data']['zenith'])

        # make stacks
        scatterii = numpy.vstack(scatterii)
        radianceii = numpy.vstack(radianceii)
        percents = numpy.vstack(percents)
        latitudeii = numpy.vstack(latitudeii)
        surfaceii = numpy.vstack(surfaceii)
        zenithii = numpy.vstack(zenithii)

        # create heatmaps of training data, with no error
        names = ['latitude', field, 'surface']
        arrays = [latitude, radiance, surface]
        bounds = [-0.1, 0.1]
        # for name, array in zip(names, arrays):
        #
        #     # make heatmap
        #     destination = 'pressures/LUT_Rayleigh_vs_{}.h5'.format(name)
        #     title = 'Rayleigh Column Density vs {}'.format(name)
        #     self.squid.shimmer('percent', zeros, 'rayleigh', scatter, name, array, destination, bounds, title)

        # create heatmaps of training data, with error tracer
        names = ['latitude', field, 'surface']
        arrays = [latitudeii, radianceii, surfaceii]
        bounds = [-0.25, -0.1, -0.025, 0.025, 0.1, 0.25]
        # for name, array in zip(names, arrays):
        #
        #     # make heatmap
        #     destination = 'pressures/Orbital_Rayleigh_vs_{}.h5'.format(name)
        #     title = 'Rayleigh Column Density vs {}'.format(name)
        #     self.squid.shimmer('percent', percents, 'rayleigh', scatterii, name, array, destination, bounds, title)

        # make 3d plot
        pyplot.clf()
        ax = Axes3D(pyplot.gcf())
        ax.plot(scatter.flatten(), zenith.flatten(), radiance.flatten(), 'gx')
        # brackets = [(0, 12.5, 'rx'), (12.5, 25, 'cx'), (25, 37.5, 'mx'), (37.5, 50, 'kx')]
        # brackets += [(50, 62.5, 'kx'), (62.5, 75, 'mx'), (75, 87.5, 'cx'), (87.5, 100, 'rx')]
        #for left, right, color in brackets:

            # # make percentiles
            # first = numpy.percentile(percents, left)
            # last = numpy.percentile(percents, right)

            # get mask
            #mask = (percents > first) & (percents < last)

        #print(mask.shape)
        print(scatterii.shape)
        print(zenithii.shape)
        print(radianceii.shape)

        ax.plot(scatterii.flatten(), zenithii.flatten(), radianceii.flatten(), 'rx')

        # show
        #pyplot.xlim(0.5, 0.4)
        #pyplot.ylim(20, 50)
        #pyplot.zlim(-1.5, -1.3)
        pyplot.title('Transmittance')
        pyplot.ylabel('solar zenith angle')
        pyplot.xlabel('total Rayleigh optical depth')
        ax.zlabel('transmittance')
        pyplot.show()

        return None

    def sieve(self, augmentation):
        """Retrieve data from calculations file.

        Arguments:
            subset: str, radiance subset
            augmentation: data augmentation dictionary

        Returns:
            dict of numpy arrays
        """

        # set defaults
        grid = augmentation.get('grid', False)
        subset = augmentation.get('subset', 'SSMS')

        # map data fields to shorthand aliases
        aliases = {'ozone': 'OzoneMixingRatio', 'altitude': 'Altitude'}
        aliases.update({'latitude': 'LatitudeZone'})
        aliases.update({'pressure': 'Pressure', 'surface': 'SurfacePressure'})
        aliases.update({'temperature': 'Temperature'})
        aliases.update({'month': 'MonthNumber'})
        aliases.update({'scatter': 'OpticalDepth_Rayleigh', 'absorption': 'OpticalDepth_Ozone'})
        aliases.update({'wavelength': 'Wavelength', 'zenith': 'SolarZenithAngle'})
        aliases.update({'layer': 'LayerOzone', 'albedo': 'SurfaceAlbedo'})
        aliases.update({'radiance': 'Radiance_{}'.format(subset)})
        aliases.update({'single': 'Radiance_SS', 'multiple': 'Radiance_MS'})
        aliases.update({'backscatter': 'Backscatter_Sb', 'transmittance': 'Transmittance_T'})
        aliases.update({'naught': 'SS_naught', 'naughtii': 'MS_naught'})
        aliases.update({'backscatterii': 'Backscatter_SS_Sb', 'transmittanceii': 'Transmittance_SS_T'})
        aliases.update({'backscatteriii': 'Backscatter_MS_Sb', 'transmittanceiii': 'Transmittance_MS_T'})

        # check for False grid
        if grid:

            # in which case, update opotical depths to Heights
            aliases.update({'scatter': 'OpticalDepth_Rayleigh_Grid'})
            aliases.update({'absorption': 'OpticalDepth_Ozone_Grid'})

        # begin data obejct with all aliases
        data = {}

        # grab the ozone data
        hydra = Hydra('{}/calculations'.format(self.sink))

        # ingest the ata
        hydra.ingest(0)

        # for each alias
        for alias, field in aliases.items():

            # try to
            try:

                # add to reservoirw
                array = hydra.grab(field)
                data[alias] = array

            # unless not found
            except IndexError:

                # in which case, pass
                pass

        return data

    def sift(self, brackets, augmentation):
        """Digest the data, filtering outliers.

        Arguments:
            brackets: dict of tuples, feature brackets
            augmentation: dict of data augmentation parameters

        Returns:
            None
        """

        # if pressure option
        if augmentation.get('pressure'):

            # get data for pressure file
            data = self._comb(brackets, augmentation)

            return data

        # grab boost
        # hydra = Hydra('{}/boost'.format(self.sink))
        # hydra.ingest(0)
        # boost = hydra.dig('average_wavelength_error')[0].distil()
        # self.boost = boost

        # set default brackets
        wavelengths = brackets.get('wavelengths', (0, 143))
        rays = brackets.get('rays', (0, 143))
        latitudes = brackets.get('latitudes', (0, 36))
        months = brackets.get('months', (0, 12))
        zeniths = brackets.get('zeniths', (0, 10))
        albedos = brackets.get('albedos', (0, 11))
        heights = brackets.get('heights', (0, 82))

        # determine waves
        if len(wavelengths) < 3:

            # create wavelength range
            waves = list([wave for wave in range(*wavelengths)])

        # otherwise
        else:

            # keep specific wavelengths
            waves = list(wavelengths)

        # create conversion functions
        conversions = {}
        # conversions = {'ozone': lambda tensor: tensor * (1e9 / 2.687e20)}
        # conversions.update({'zenith': lambda tensor: numpy.cos(tensor * math.pi / 180)})
        #conversions.update({'radiance': lambda tensor: numpy.log10(tensor)})

        # apply logarithms
        fields = augmentation.get('logarithms', [])
        for field in fields:

            # add conversion
            conversions.update({field: lambda tensor: numpy.log10(tensor + 1e-2)})

        # map data fields to shorthand aliases
        data = self.sieve(augmentation)

        # # construct column from scatter
        data['column'] = data['scatter'][waves].sum(axis=1)
        data['absorb'] = data['absorption'][waves].sum(axis=1)

        # get surface pressure
        data['surface'] = data['pressure'][:1, :, :]

        # construct optical depth properties
        data['fraction'] = data['scatter'] / (data['scatter'] + data['absorption'])
        data['combination'] = data['scatter'] + data['absorption']

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['combination']
        accumulation = [combination[:, index:, :, :].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0, 2, 3)
        data['accumulation'] = accumulation

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['scatter']
        accumulation = [combination[:, index:, :, :].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0, 2, 3)
        data['heap'] = accumulation

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['absorption']
        accumulation = [combination[:, index:, :, :].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0, 2, 3)
        data['sponge'] = accumulation

        # expand radiance decomposition along albedos
        fields = ['naught', 'naughtii', 'backscatter', 'backscatterii', 'backscatteriii']
        fields += ['transmittance', 'transmittanceii', 'transmittanceiii']
        for field in fields:

            # try to
            try:

                # get array
                array = data[field]
                stack = numpy.array([array] * 11)
                stack = stack.transpose(1, 2, 0, 3, 4)
                data[field] = stack

            # unless not there
            except KeyError:

                # in which case, nevermind
                pass

        # take logarithms
        for field in ('single', 'multiple', 'radiance', 'naught', 'naughtii'):

            # try to
            try:

                # take logarithm
                data[field] = numpy.log10(data[field])

            # unless not there
            except KeyError:

                # in which case, nevermind
                pass

        # for each radiance component
        angles = None
        length = zeniths[1] - zeniths[0]
        fields = ['naught', 'naughtii', 'backscatter', 'backscatterii', 'backscatteriii']
        fields += ['transmittance', 'transmittanceii', 'transmittanceiii']
        fields += ['single', 'multiple', 'radiance']
        for field in fields:

            # try to
            try:

                # convert radiance with logarithm, and expand along interpolations
                radiance = data[field]

                # select wavelengths
                radiance = radiance[rays[0]: rays[1]]

                # create array from zenith angles
                zenith = data['zenith'][zeniths[0]: zeniths[1]]
                data['zeniths'] = zenith.copy()

                # take all subsets
                radiance = radiance[:, zeniths[0]: zeniths[1], albedos[0]: albedos[1], :, :]
                radiance = radiance[:, :, :, months[0]: months[1], latitudes[0]: latitudes[1]]

                # check for zenith expansion
                expansion = augmentation.get('interpolation', 0)
                random = augmentation.get('random', False)

                # get interpolationn of radiance data
                self._print('\n expanding {}...'.format(field))
                self._print('radiance: {} zenith: {}'.format(radiance.shape, zenith.shape))
                radiance, zenith, angles = self._expand(radiance, zenith, expansion, random=random, axis=1, angles=angles)
                self._print('radiance: {} zenith: {}, angles: {}'.format(radiance.shape, zenith.shape, angles.shape))

                # update
                data[field] = radiance
                length = len(zenith)

            # unless not there
            except KeyError:

                # in which case, nevermind
                pass

        # result zenith angles
        data['zenith'] = angles
        data['square'] = (angles) ** 2
        data['cosine'] = numpy.cos(angles * (math.pi / 180))
        data['root'] = numpy.sqrt(data['cosine'])
        data['secant'] = 1 / (numpy.cos(angles * (math.pi / 180)))
        data['cube'] = (angles) ** 3

        self._print('albedos...')

        # create array from albedo
        albedo = data['albedo']
        data['albedos'] = albedo[albedos[0]: albedos[1]].copy()
        albedo = numpy.array([albedo] * (latitudes[1] - latitudes[0]))
        albedo = numpy.array([albedo] * (months[1] - months[0]))
        albedo = numpy.array([albedo] * length)
        albedo = albedo.transpose(0, 3, 1, 2)
        albedo = numpy.vstack([albedo[:, index - albedos[0]] for index in range(*albedos)])
        albedo = numpy.vstack([albedo[:, index - months[0]] for index in range(*months)])
        albedo = numpy.hstack([albedo[:, index - latitudes[0]] for index in range(*latitudes)])
        albedo = numpy.array([albedo]).transpose(1, 0)
        data['albedo'] = albedo
        # data['brightness'] = (albedo) ** 2
        # data['dimness'] = (albedo) ** 3
        # data['luminosity'] = (albedo) ** 3

        self._print('months...')

        # create one hot encodings for months
        field = 'month'
        data['months'] = numpy.array([index + 1 for index in range(*months)])
        size = months[1] - months[0]
        diagonal = numpy.diag([1] * size)

        # expand across other brackets
        diagonal = numpy.array([diagonal] * (latitudes[1] - latitudes[0]))
        diagonal = numpy.array([diagonal] * (albedos[1] - albedos[0]))
        diagonal = numpy.array([diagonal] * (len(zenith)))

        # transpose into zenith x albedo x  moth x latitude
        diagonal = diagonal.transpose(0, 1, 3, 2, 4)

        # stack into sample ordering
        diagonal = numpy.vstack([diagonal[:, index - albedos[0]] for index in range(*albedos)])
        diagonal = numpy.vstack([diagonal[:, index - months[0]] for index in range(*months)])
        diagonal = numpy.vstack([diagonal[:, index - latitudes[0]] for index in range(*latitudes)])
        data[field] = diagonal

        self._print('latitudes...')

        # create one hot encodings for latitude
        field = 'latitude'
        data['latitudes'] = data['latitude'][latitudes[0]: latitudes[1]]
        size = latitudes[1] - latitudes[0]
        diagonal = numpy.diag([1] * size)

        # expand across other brackets
        diagonal = numpy.array([diagonal] * (months[1] - months[0]))
        diagonal = numpy.array([diagonal] * (albedos[1] - albedos[0]))
        diagonal = numpy.array([diagonal] * (len(zenith)))

        # stack into samples
        diagonal = numpy.vstack([diagonal[:, index - albedos[0]] for index in range(*albedos)])
        diagonal = numpy.vstack([diagonal[:, index - months[0]] for index in range(*months)])
        diagonal = numpy.vstack([diagonal[:, index - latitudes[0]] for index in range(*latitudes)])
        data[field] = diagonal

        self._print('profiles...')

        # go though other profile fields
        fields = ['ozone', 'temperature', 'pressure', 'layer', 'column', 'surface', 'absorb']
        for field in fields:

            # convert profile from 82 x 12 x 36 to 4320 x 3 x 82
            profile = data[field]
            profile = profile.transpose(1, 2, 0)

            # expand by albedo and zenith
            profile = numpy.array([profile] * (albedos[1] - albedos[0]))
            profile = numpy.array([profile] * len(zenith))
            profile = numpy.vstack([profile[:, index] for index in range(*albedos)])
            profile = numpy.vstack([profile[:, index] for index in range(*months)])
            profile = numpy.vstack([profile[:, index] for index in range(*latitudes)])
            data[field] = profile

        self._print('optical depths...')

        # go though optical depth profiles
        fields = ['scatter', 'absorption', 'fraction', 'combination', 'accumulation', 'heap', 'sponge']
        fields = ['scatter']
        for field in fields:

            self._print('transposing {}...'.format(field))

            # convert profile from 82 x 12 x 36 to 4320 x 3 x 82
            profile = data[field]
            profile = profile.transpose(2, 3, 1, 0)
            profile = profile[months[0]: months[1], latitudes[0]:latitudes[1], :, waves[0]]

            # expand by albedo and zenith, and flattern
            profile = numpy.array([profile] * (albedos[1] - albedos[0]))
            profile = numpy.array([profile] * len(zenith))
            profile = profile.transpose(3, 2, 1, 0, 4)
            profile = profile.flatten().reshape(-1, heights[1] - heights[0])
            data[field] = profile

        self._print('masking fills...')

        # construct mask, checking for fill values in middle row
        fill = -999.0
        mask = numpy.array([fill not in sample for sample in data['radiance']])

        # apply mask
        samples = len(data['radiance'])
        for alias, array in data.items():

            # if of the same dimension as samples
            if array.shape[0] == samples:

                # apply the mask
                data[alias] = data[alias][mask]

                # apply conversion
                conversion = conversions.get(alias, lambda tensor: tensor)
                data[alias] = conversion(data[alias]).astype('float64')

        # make wavelengths
        data['wavelengths'] = data['wavelength'][wavelengths[0]: wavelengths[1]]

        # # add boost
        # data['boost'] = boost

        self._print('combining heights...')

        # adjust heights
        fields = ['scatter', 'absorption', 'heap', 'sponge', 'accumulation', 'combination']
        fields = ['scatter']
        for field in fields:

            # adjust height
            data[field] = data[field][:, heights[0]: heights[1]]

            # check for combining layers
            combination = augmentation.get('combine', 1)
            if combination > 1:

                # combine layers
                data[field] = self._combine(data[field], combination)

        # add difference and ratio
        data['ratio'] = data['single'] / data['multiple']
        data['difference'] = data['single'] - data['multiple']

        # add add pre-log ratio
        data['ratioii'] = numpy.log10(10 ** data['single'] / (10 ** data['multiple']) + (10 ** data['single']))

        # add fraction
        data['ratioiii'] = data['single'] / numpy.log((10 ** data['multiple']) + (10 ** data['single']))

        # create triplet
        data['triplet'] = numpy.hstack([data['naughtii'], data['backscatter'], data['transmittance']])

        # create transmittance ratio
        data['transcendence'] = data['transmittanceii'] / data['transmittanceiii']

        # create transmittance difference
        data['transcendenceii'] = data['transmittanceii'] - data['transmittanceiii']

        # print status
        print(' ')
        for name, array in data.items():

            # print
            print(name, array.shape)

        return data

    def spear(self, addresses, orbit=48687, order=None):
        """Make validation predications from a triplicate of networks.

        Arguments:
            addresses: list of tuples of ints, the year, month, day, index of the models involved
            orbit: int, orbit number for testing
            order: list of str, the order of the addrsses
        """

        # set default order
        order = order or ['naughtii', 'backscatter', 'transmittance']

        # get model storages from each set of indices
        storages = [self.forage(*address) for address in addresses]

        # gather trial data from first storage
        data = self.trial(storages[0]['brackets'], storages[0]['augmentation'], orbit=orbit)

        # gather predictions
        predictions = {}
        originals = {}
        for storage, field in zip(storages, order):

            # include name time with orbit
            tag = storage.get('name', '_').replace('_', '0')
            orbit = '{}_{}'.format(orbit, tag)

            # generate train, test split
            fraction = 1.0
            train, test, memory = self.peck(data, fraction, shuffle=False)

            # vectorize the training data
            features = storage['features']
            sizes = storage['sizes']
            transforms = storage['transforms']
            machines = storage['machines']
            matrix, target, original, names, machines = self.vectorize(train, features, sizes, transforms, machines)

            # add targeta
            if len(target.shape) < 2:

                # reshpae
                target = target.reshape(-1, 1)

            # retrieve model
            perceptor = storage['machines']['perceptor']

            # or get tensor flow
            if storage.get('flow', False):

                # load perceptor
                perceptor = load_model(storage['model'])

            # get predictions from perceptor
            prediction = perceptor.predict(matrix)

            # add extra dimention if needed
            if len(prediction.shape) < 2:

                # reshpae
                prediction = prediction.reshape(-1, 1)

            # copy to results
            result = prediction.copy()

            # undo transforms
            output = features[-1]
            for transform in transforms[::-1]:

                # undo transform
                prediction = self._reform(prediction, transform, machines[output][transform], inputs=False)

            # add prediction
            predictions[field] = prediction
            originals[field] = original

        # reconstruct radiance triplet
        triplet = numpy.hstack([predictions[field] for field in order])
        estimate = self._reconstitute(triplet, 'triplet', train)

        # reconstruct orginal triplet
        triplet = numpy.hstack([originals[field] for field in order])
        truth = self._reconstitute(triplet, 'triplet', train)

        # print results
        error = 100 * ((estimate / truth) - 1)
        absolute = abs(error)
        print('median error: {}'.format(numpy.median(absolute)))
        print('maximum error: {}'.format(absolute.max()))

        # make validation plot vs zenith angle
        zenith = train['zenith']
        address = 'validations/Triplet_Error_Zenith.h5'
        title = 'Triplet Error vs Zenith'
        self.squid.ink('triplet_error_zenith_{}'.format(orbit), error, 'solar zenith angle', zenith, address, title)

        # make validation plot vs surfsce pressure
        surface = train['surface']
        address = 'validations/Triplet_Error_Surface.h5'
        title = 'Triplet Error vs Surface Pressure'
        self.squid.ink('triplet_error_pressure_{}'.format(orbit), error, 'surface pressure', surface, address, title)

        # # make heatmap error vs zenith and pressure
        # surface = train['surface']
        # address = 'validations/Triplet_Error_Heatmap.h5'
        # title = 'Triplet Error vs Surface Pressure, Zenith'
        # bounds = [-0.25, -0.1, -0.05, -0.01, 0.01, 0.05, 0.1, 0.25]
        # parameters = ['triplet_error_pressure_zenith_{}'.format(orbit), error, 'surface pressure', surface]
        # parameters += ['zenith angle', zenith]
        # self.squid.shimmer(*parameters, address, bounds, title)

        return None

    def strain(self, start=0, stop=1, recipe=None, bounds=(-90, 90), lists=False):
        """Digest the data, filtering outliers.

        Arguments:
            recipe: str, file path for recipe of wavelengths
            start: int, starting file index
            stop: int, stopping file index
            bounds: tuple of floats, the latitude range
            lists: convert to lists?

        Returns:
            None
        """

        # map data fields to shorthand aliases
        aliases = {'ozone': 'O3Density', 'altitude': 'Altitude'}
        aliases.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
        aliases.update({'density': 'AirDensity', 'pressure': 'Pressure'})
        aliases.update({'surface': 'SurfacePressure', 'temperature': 'Temperature'})
        aliases.update({'tropopause': 'TropopauseAltitude'})

        # begin data obejct with all aliases
        data = {alias: [] for alias in aliases.keys()}

        # grab the ozone data
        hydra = Hydra('{}/ozone'.format(self.sink))
        paths = hydra.paths
        paths.sort()

        # for each path
        for path in paths[start:stop]:

            # ingest
            hydra.ingest(path)

            # for each alias
            for alias, field in aliases.items():

                # add to reservoirw
                array = hydra.grab(field)
                data[alias].append(array)

        # map data fields to shorthands for radiance data
        aliasesii = {'radiance': 'Radiance', 'wavelength': 'Wavelength'}
        aliasesii.update({'albedo': 'SurfaceAlbedo', 'view': 'ViewingZenithAngle'})
        aliasesii.update({'zenith': 'SolarZenithAngle', 'azimuth': 'RelativeAzimuthAngle'})

        # add data obejct with second aliases
        data.update({alias: [] for alias in aliasesii.keys()})

        # grab the radiance data
        hydra = Hydra('{}/radiance'.format(self.sink))
        paths = hydra.paths
        paths.sort()

        # for each path
        for path in paths[start:stop]:

            # ingest
            hydra.ingest(path)

            # for each alias
            for alias, field in aliasesii.items():

                # add to reservoirw
                array = hydra.grab(field)
                data[alias].append(array)

        #  vstack all arrays
        data = {alias: numpy.vstack(arrays) for alias, arrays in data.items()}

        # construct mask, checking for fill values in middle row
        fill = -999.0
        mask = numpy.array([fill not in sample[1] for sample in data['radiance']])

        # get mask from latitude bounds
        maskii = numpy.array([bounds[0] <= latitude[1] <= bounds[1] for latitude in data['latitude']])

        # combine masks
        masque = (mask * maskii).astype(bool)

        # apply mask
        samples = len(data['ozone'])
        for alias, array in data.items():

            # if of the same dimension as samples
            if array.shape[0] == samples:

                # apply the mask
                data[alias] = data[alias][masque]

        # for each abscissas
        for abscissa in ('wavelength', 'albedo', 'altitude'):

            # only keep the firwt row
            data[abscissa] = data[abscissa][0]

        # adjust according to recipe
        if recipe:

            # get the wavelength boolean mask
            waves = self._waver(data['wavelength'], recipe)

            # adjust wavelength and radiance
            data['wavelength'] = data['wavelength'][waves]
            data['radiance'] = data['radiance'][:, :, :, waves]

        # check for lists
        if lists:

            # convert to lists
            for field, array in data.items():

                # try to
                try:

                    # convert to lists
                    data[field] = array.tolist()

                # unless not an array
                except AttributeError:

                    # in which case, nevermiind
                    pass

        return data

    def submit(self, year, month, day, number, destination):
        """Create a ticket with run attributes.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            number: int, number of assay
            destination: str, filename path for destination

        Returns:
            None
        """

        # begin ticketl ticket
        ticket = {}

        # grab record
        storage = self.forage(year, month, day, number)

        # grab fields
        fields = ['features', 'brackets', 'augmentation', 'transforms', 'sizes']
        fields += ['layers', 'options', 'fraction', 'flow']
        for field in fields:

            # set ticket
            ticket[field] = storage[field]

        # dispense
        destination = '{}/{}'.format(self.sink, destination)
        self._dump(ticket, destination)

        return None

    def trace(self, exam, scatters, zeniths, steps=100):
        """Trace model function.

        Arguments:
            exam: examination dict
            scatters: tuple of floats, the scattering range
            zeniths: tuple of floats, the zeniths

        Returns:
            None
        """

        # grab model
        perceptor = load_model(exam['storage']['model'])

        # for every scatter
        for scatter in scatters:

            # plot all zeniths
            zenith = [zeniths[0] + (zeniths[-1] - zeniths[0]) * (index / steps) for index in range(steps)]
            cosine = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zenith])

            # construct matrix
            rayleigh = numpy.array([scatter] * len(cosine))
            matrix = numpy.vstack([rayleigh, cosine]).transpose(1, 0)

            # make predictions
            prediction = perceptor.predict(matrix)

            # make plot
            title = 'Prediction_vs_Zenith, Rayleigh: {}'.format(scatter)
            folder = 'model/model_function_zenith_{}.h5'.format(scatter)
            self.squid.ink('prediction', prediction, 'zenith', zenith, folder, title)

        # for every scatter
        for zenith in zeniths:

            # plot all zeniths
            scatter = [scatters[0] + (scatters[-1] - scatters[0]) * (index / steps) for index in range(steps)]
            #zenith = numpy.array([numpy.cos(angle * math.pi / 180) for angle in zenith])

            # construct matrix
            angle = numpy.array([numpy.cos(zenith * math.pi / 180)] * len(scatter))
            matrix = numpy.vstack([scatter, angle]).transpose(1, 0)

            # make predictions
            prediction = perceptor.predict(matrix)

            # make plot
            title = 'Prediction_vs_Zenith : {}, Rayleigh'.format(zenith)
            folder = 'model/model_function_rayleigh_{}.h5'.format(zenith)
            self.squid.ink('prediction', prediction, 'rayleigh', scatter, folder, title)

        return None

    def trial(self, brackets, augmentation, orbit=48687, recipe='waves.txt'):
        """Prepare unseen data for testing in the model.

        Arguments:
            brackets: dict of various parameter brackets
            augmentation: dict of data augmentation values
            orbit: int, orbit number for test orbit
            recipe: str, text file for wavelength brackets

        Returns:
            None
        """

        # if pressure option
        if augmentation.get('pressure'):

            # get data for pressure file
            data = self._tribulate(brackets, augmentation, orbit=orbit)

            return data

        # grab the pressure grid
        hydra = Hydra('{}/calculations'.format(self.sink))
        hydra.ingest(0)

        #grid = hydra.grab('PressureGrid')

        # get zeniths
        zeniths = hydra.grab('SolarZenithAngle')

        # map data fields to shorthands for radiance data
        subset = brackets.get('subset', 'SSMS')
        aliases = {'radiance': 'Radiance_{}'.format(subset), 'wavelength': 'Wavelength'}
        aliases.update({'single': 'Radiance_SS'.format(subset), 'multiple': 'Radiance_MS'})
        aliases.update({'albedo': 'SurfaceAlbedo', 'temperature': 'Temperature', 'pressure': 'Pressure'})
        aliases.update({'zenith': 'SolarZenithAngle', 'layer': 'LayerOzone'})
        aliases.update({'scatter': 'OpticalDepth_Rayleigh', 'absorption': 'OpticalDepth_Ozone'})
        aliases.update({'altitude': 'Altitude', 'wavelength': 'Wavelength', 'latitude': 'Latitude'})
        aliases.update({'longitude': 'Longitude'})
        aliases.update({'backscatter': 'Backscatter_Sb', 'transmittance': 'Transmittance_T'})
        aliases.update({'naught': 'SS_naught', 'naughtii': 'MS_naught'})
        aliases.update({'backscatterii': 'Backscatter_SS_Sb', 'transmittanceii': 'Transmittance_SS_T'})
        aliases.update({'backscatteriii': 'Backscatter_MS_Sb', 'transmittanceiii': 'Transmittance_MS_T'})

        # check for False grid
        if augmentation.get('grid', False):

            # in which case, update opotical depths to Heights
            aliases.update({'scatter': 'OpticalDepth_Rayleigh_Grid'})
            aliases.update({'absorption': 'OpticalDepth_Ozone_Grid'})

        # add data obejct with second aliases
        data = {}
        data.update({alias: [] for alias in aliases.keys()})

        # add grid
        #data['grid'] = grid

        # grab the radiance data
        hydra = Hydra('{}/orbits/radiance'.format(self.sink))
        paths = [path for path in hydra.paths if str(orbit) in path]

        # for each path
        for path in paths:

            # ingest
            hydra.ingest(path)

            # for each alias
            for alias, field in aliases.items():

                # try to
                try:

                    # add to reservoirw
                    array = hydra.grab(field)
                    data[alias].append(array)

                # unless not found
                except IndexError:

                    # in which case, pass
                    pass

        # vstack all arrays
        data = {alias: numpy.vstack(arrays) for alias, arrays in data.items()}

        # check for False grid
        if augmentation.get('grid', False):

            # rearrage opticaldepths
            data['scatter'] = data['scatter'].transpose(3, 2, 1, 0)
            data['absorption'] = data['absorption'].transpose(3, 2, 1, 0)

        # get nadir port for nadir, and wavelength 379 () index 1090)
        data['layer'] = data['layer'][:, 1, :]
        data['scatter'] = data['scatter'][:, 1, :, :]
        data['absorption'] = data['absorption'][:, 1, :, :]

        # construct mask, checking for fill values in middle row
        fill = -999.0
        mask = numpy.array([fill not in sample[1] for sample in data['radiance']])
        masque = mask

        # apply mask
        samples = len(data['layer'])
        for alias, array in data.items():

            # if of the same dimension as samples
            if array.shape[0] == samples:

                # apply the mask
                data[alias] = data[alias][masque]

        # for each abscissas
        for abscissa in ('wavelength', 'albedo', 'altitude'):

            # only keep the firwt row
            data[abscissa] = data[abscissa][0]

        # get the wavelength boolean mask
        waves = self._waver(data['wavelength'], recipe)

        data['scatter'] = data['scatter'][:, :, waves][:, :, brackets['wavelengths'][0]]
        data['absorption'] = data['absorption'][:, :, waves][:, :, brackets['wavelengths'][0]]

        # set pressure to nadir only
        data['pressure'] = data['pressure'][:, 1, :]

        # sum along columns
        data['column'] = data['scatter'].sum(axis=1).reshape(-1, 1)
        data['absorb'] = data['absorption'].sum(axis=1).reshape(-1, 1)

        # construct optical depth properties
        data['fraction'] = data['scatter'] / (data['scatter'] + data['absorption'])
        data['combination'] = data['scatter'] + data['absorption']

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['combination']
        accumulation = [combination[:, index:].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0)
        data['accumulation'] = accumulation

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['scatter']
        accumulation = [combination[:, index:].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0)
        data['heap'] = accumulation

        # # construct accumulation of combined absorption from top of atm (82) to bottom (0)
        combination = data['absorption']
        accumulation = [combination[:, index:].sum(axis=1) for index in range(82)]
        accumulation = numpy.array(accumulation).transpose(1, 0)
        data['sponge'] = accumulation

        # adjust wavelength for recipe and rays
        rays = brackets['rays']
        data['wavelength'] = data['wavelength'][waves]
        data['wavelength'] = data['wavelength'][rays[0]: rays[1]]

        # expand decomposition along albedo
        fields = ['naught', 'naughtii', 'backscatter', 'backscatterii', 'backscatteriii']
        fields += ['transmittance', 'transmittanceii', 'transmittanceiii']
        for field in fields:

            # expand along albedos
            array = data[field]
            stack = numpy.array([array] * 11)
            stack = stack.transpose(1, 2, 0, 3)
            data[field] = stack

            print(field, stack.shape)

        # take logarithms
        for field in ('naught', 'naughtii', 'radiance', 'single', 'multiple'):

            # take logarithm
            data[field] = numpy.log10(data[field])

        # for all components
        fields = ['naught', 'naughtii', 'backscatter', 'backscatterii', 'backscatteriii']
        fields += ['transmittance', 'transmittanceii', 'transmittanceiii']
        fields += ['single', 'multiple', 'radiance']
        for field in fields:

            # adjust according to waves and ray
            data[field] = data[field][:, :, :, waves]
            data[field] = data[field][:, :, :, rays[0]: rays[1]]

            # adjust field for nadir port only
            data[field] = data[field][:, 1, :, :]
            data[field] = numpy.vstack([data[field][:, index, :] for index in range(*brackets['albedos'])])

        # apply logarithms
        conversions = {}
        fields = brackets.get('logarithms', [])
        for field in fields:

            # add conversion
            conversions.update({field: lambda tensor: numpy.log10(tensor + 1e-2)})
            data[field] = conversions[field](data[field])

        # construct zenith functions
        zenith = data['zenith'][:, 1:2]
        data['zenith'] = zenith
        data['square'] = (zenith) ** 2
        data['cube'] = (zenith) ** 3
        data['secant'] = 1 / (numpy.cos(zenith * (math.pi / 180)))
        data['cosine'] = numpy.cos(zenith * (math.pi / 180))

        # get surface pressure
        data['surface'] = data['pressure'][:, 0]

        # make albedo stacks
        fields = ['layer', 'column', 'absorb', 'absorption', 'zenith', 'square', 'cube', 'secant']
        fields += ['cosine', 'scatter', 'surface']
        for alias in fields:

            # try to
            try:

                # stack albedos
                data[alias] = numpy.vstack([data[alias] for albedo in range(*brackets['albedos'])])

            # unless not there
            except KeyError:

                # pass
                pass

        # construct albedo based features
        length = zenith.shape[0]
        albedos = brackets['albedos']
        albedo = numpy.array([data['albedo'][albedos[0]: albedos[1]] for sample in range(length)])
        albedo = numpy.array([albedo.transpose(1, 0).flatten()]).transpose(1, 0)
        data['albedo'] = albedo
        data['brightness'] = (albedo) ** 2
        data['luminosity'] = (albedo) ** 3

        # adjust heights
        heights = brackets.get('heights', (0, 82))
        for field in ('scatter', 'absorption', 'heap', 'sponge', 'accumulation', 'combination'):

            # adjust height
            data[field] = data[field][:, heights[0]: heights[1]]

            # check for combining layers
            combination = augmentation.get('combine', 1)
            if combination > 1:

                # combine layers
                data[field] = self._combine(data[field], combination)

        # add difference and ratio
        data['ratio'] = data['single'] / data['multiple']
        data['difference'] = data['single'] - data['multiple']

        # add add pre-log ratio
        data['ratioii'] = numpy.log10(10 ** data['single'] / (10 ** data['multiple']) + (10 ** data['single']))

        # add fraction
        data['ratioiii'] = data['single'] / numpy.log((10 ** data['multiple']) + (10 ** data['single']))

        # create triplet
        data['triplet'] = numpy.hstack([data['naughtii'], data['backscatter'], data['transmittance']])

        # create transmittance ratio
        data['transcendence'] = data['transmittanceii'] / data['transmittanceiii']

        # create transmittance difference
        data['transcendenceii'] = data['transmittanceii'] - data['transmittanceiii']

        # sort according to zenith bracket
        bracket = brackets['zeniths']
        #zeniths = data['zeniths']
        flat = data['zenith'].flatten()
        mask = (flat >= zeniths[bracket[0]]) & (flat <= zeniths[bracket[1] - 1])
        samples = data['radiance'].shape[0]

        # use mask on arrays
        for name, array in data.items():

            # if the shape mathches
            if array.shape[0] == samples:

                # apply mask
                data[name] = array[mask]

        # print stats
        for name, array in data.items():

            # print
            self._print(name, array.shape)

        return data

    def validate(self, year, month, day, number=0, exam=None, orbit=48687):
        """Validate a particular model by testing on unseen oribits.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            number: int, index of model ( 0 is most recent )
            exam: dict of examination results

        Returns:
            None
        """

        # retrieve storage
        storage = self.forage(year, month, day, number)

        # get the unseen orbital data
        data = self.trial(storage['brackets'], storage['augmentation'], orbit=orbit)

        # include name time with orbit
        tag = storage.get('name', '_').replace('_', '0')
        orbit = '{}_{}'.format(orbit, tag)

        # generate train, test split
        fraction = 1.0
        train, test, memory = self.peck(data, fraction, shuffle=False)

        # vectorize the training data
        features = storage['features']
        sizes = storage['sizes']
        transforms = storage['transforms']
        machines = storage['machines']
        matrix, target, original, names, machines = self.vectorize(train, features, sizes, transforms, machines)

        # compare features
        self._print(' ')
        for feature in features:

            # print
            self._print(feature)
            self._print(data[feature].shape, exam['data'][feature].shape)
            self._print(data[feature].min(), exam['data'][feature].min())
            self._print(data[feature].max(), exam['data'][feature].max())
            self._print('')

        # add targeta
        if len(target.shape) < 2:

            # reshpae
            target = target.reshape(-1, 1)

        # retrieve model
        perceptor = storage['machines']['perceptor']

        # or get tensor flow
        if storage.get('flow', False):

            # load perceptor
            perceptor = load_model(storage['model'])

        # get predictions from perceptor
        prediction = perceptor.predict(matrix)

        # add extra dimention if needed
        if len(prediction.shape) < 2:

            # reshpae
            prediction = prediction.reshape(-1, 1)

        # copy to results
        result = prediction.copy()

        # undo transforms
        output = features[-1]
        for transform in transforms[::-1]:

            # undo transform
            prediction = self._reform(prediction, transform, machines[output][transform], inputs=False)

        # reconstitute prediction
        estimate = self._reconstitute(prediction, features[-1], train)
        truth = self._reconstitute(original, features[-1], train)

        # # calculate precent diffence
        # if features[-1] == 'multiple':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** prediction
        #     truth = 10 ** original
        #
        # # calculate precent from ratio
        # if features[-1] == 'ratio':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (train['single'] / prediction)
        #     truth = 10 ** (train['single'] / original)
        #
        # # calculate precent from ratio
        # if features[-1] == 'difference':
        #
        #     # percent difference from log multiple
        #     estimate = 10 ** (train['single'] - prediction)
        #     truth = 10 ** (train['single'] - original)

        # calculate absolue
        percent = 100 * ((estimate / truth) - 1)
        maximum = float(abs(percent).max())
        median = numpy.quantile(abs(percent), 0.5)
        self._print('median absolute percent error: {}'.format(median))
        storage['percent'] = percent

        # # plot heatmap of errors vs lat, lon
        # latitude = train['latitude'][:, 1].flatten()
        # longitude = train['longitude'][:, 1].flatten()
        # error = abs(percent).reshape(11, -1).transpose(1, 0).max(axis=1).flatten()
        # address = 'globe/Error_Map_Orbit_{}.h5'.format(orbit)
        # title = 'Percent Errors, Orbit {}'.format(orbit)
        # bounds = [0, 0.05, 0.1, 0.2, 0.5]
        # self.squid.shimmer('error', error, 'latitude', latitude, 'longitude', longitude, address, bounds, title)

        # print max error
        self._print('maximum absolute percent error: {}'.format(maximum))
        storage['maximum'] = maximum

        # calculate score
        score = r2_score(original, prediction)
        self._print('training R^2 score: {}'.format(score))
        storage['verification'] = float(score)

        # # calculate errors
        # percents = 200 * (original - prediction) / (original + prediction)

        # # gather up sample traits, along with percentages
        # loss = target - result
        # traits = self._assemble(train, loss.copy(), percents.copy())

        # create histograms
        ratio = percent
        name = 'histogram_validation_errors_{}'.format(tag)
        array = ratio.flatten()
        feature = Feature(['Categories', name], array)

        # stash
        destination = '{}/validations/Validation_Histogram_{}.h5'.format(self.sink, orbit)
        self.stash([feature], destination)

        # create zenith scatter plot
        features = []
        name = 'scatter_zenith_{}'.format(orbit)
        array = ratio.flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)
        name = 'solar_zenith_angle'
        array = data['zenith'].flatten()
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)
        destination = '{}/validations/Validation_Scatter_zenith_{}.h5'.format(self.sink, orbit)
        self.stash(features, destination)

        print('zenith:', ratio.shape, array.shape)

        # # create zeniths by albedo plot
        # for reflectivity in range(0, 11):
        #
        #     # create mask
        #     mask = data['albedo'].flatten() == reflectivity / 10
        #
        #     # create zenith scatter plot
        #     features = []
        #     name = 'scatter_zenith_albedo_{}_{}'.format(reflectivity, orbit)
        #     array = ratio.flatten()[mask]
        #     feature = Feature(['Categories', name], array)
        #     features.append(feature)
        #     name = 'solar_zenith_angle'
        #     array = data['zenith'].flatten()[mask]
        #     feature = Feature(['IndependentVariables', name], array)
        #     features.append(feature)
        #
        #     # add
        #     destination = '{}/albedos/Validation_Scatter_zenith_alb_{}_{}.h5'.format(self.sink, reflectivity, orbit)
        #     self.stash(features, destination)

        # create albedo scatter plot
        features = []
        name = 'scatter_albedo_{}'.format(orbit)
        array = ratio.flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)
        name = 'albedo'
        array = data['albedo'].flatten()
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)
        destination = '{}/validations/Validation_Scatter_albedo_{}.h5'.format(self.sink, orbit)
        self.stash(features, destination)

        print('albedo:', ratio.shape, array.shape)

        # create radiance error scatter plot by zenith
        features = []
        name = 'scatter_rad_zenith_{}'.format(orbit)
        array = ratio.flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)
        name = 'solar_zenith_angle'
        array = data['zenith'].flatten()
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)
        destination = '{}/validations/Validation_Scatter_rad_zenith_{}.h5'.format(self.sink, orbit)
        self.stash(features, destination)

        # create pressure scatter plot
        features = []
        name = 'scatter_pressure_{}'.format(orbit)
        array = ratio.flatten()
        feature = Feature(['Categories', name], array)
        features.append(feature)
        name = 'surface_pressure'
        array = data['surface'].flatten()
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)
        destination = '{}/validations/Validation_Scatter_pressure_{}.h5'.format(self.sink, orbit)
        self.stash(features, destination)

        print('pressure:', ratio.shape, array.shape)
        print(data['pressure'].shape)

        # # create latitude scatter plot
        # features = []
        # name = 'scatter_latitude_{}'.format(orbit)
        # array = ratio.flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'latitude'
        # array = data['latitude'][:, 1].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_latitude_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # create latitude scatter plot
        # features = []
        # name = 'scatter_zenith_vs_latitude_{}'.format(orbit)
        # array = data['zenith'].flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'latitude'
        # array = data['latitude'][:, 1].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_latitude_zenith_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # creete column scatter
        # features = []
        # name = 'scatter_column_{}'.format(orbit)
        # array = ratio.flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'column'
        # array = data['column'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_column_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # creete absorbance scatter
        # features = []
        # name = 'scatter_absorb_{}'.format(orbit)
        # array = ratio.flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'absorb'
        # array = data['absorb'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_absorb_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # column vs radiance, training
        # features = []
        # name = 'scatter_training_column_{}'.format(orbit)
        # array = exam['data']['column'].flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'column_training'
        # array = exam['data']['radiance'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_rad_train_col_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # create column vs radiance, vaidation
        # features = []
        # name = 'scatter_validation_column_{}'.format(orbit)
        # array = data['column'].flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'column_validation'
        # array = data['radiance'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_rad_val_col_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # absorb vs radiance, training
        # features = []
        # name = 'scatter_training_absorb_{}'.format(orbit)
        # array = exam['data']['absorb'].flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'absorb_training'
        # array = exam['data']['radiance'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_rad_train_abs_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # # create absorb vs radiance, vaidation
        # features = []
        # name = 'scatter_validation_absorb_{}'.format(orbit)
        # array = data['absorb'].flatten()
        # feature = Feature(['Categories', name], array)
        # features.append(feature)
        # name = 'absorb_validation'
        # array = data['radiance'].flatten()
        # feature = Feature(['IndependentVariables', name], array)
        # features.append(feature)
        # destination = '{}/validations/Validation_Scatter_rad_val_abs_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # begin features
        features = []

        # # for absorbance and rayleight
        # for field in ('absorb', 'column'):
        #
        #     # make training heatmap
        #     name = 'heatmap_training_{}'.format(field)
        #     error = numpy.zeros(exam['train'][field].shape).flatten()
        #     abscissa = exam['train'][field].flatten()
        #     ordinate = exam['train']['radiance'].flatten()
        #     array = numpy.vstack([error, ordinate, abscissa])
        #     bounds = [-1, 1]
        #     brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        #     labels = ['{} to {}'.format(first, second) for first, second in brackets]
        #     attributes = {'brackets': brackets, 'labels': labels}
        #     feature = Feature(['Categories', name], array, attributes=attributes)
        #     features.append(feature)
        #
        #     # make validation heatmap
        #     name = 'heatmap_validation_errors_{}'.format(field)
        #     error = (abs((original / prediction) - 1)).flatten()
        #     abscissa = train[field].flatten()
        #     ordinate = train['radiance'].flatten()
        #     array = numpy.vstack([error, ordinate, abscissa])
        #     bounds = [0.0, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.3]
        #     brackets = [(first, second) for first, second in zip(bounds[:-1], bounds[1:])]
        #     labels = ['{} to {}'.format(first, second) for first, second in brackets]
        #     attributes = {'brackets': brackets, 'labels': labels}
        #     feature = Feature(['Categories', name], array, attributes=attributes)
        #     features.append(feature)

        # # stash
        # destination = '{}/validations/Validation_absorb_rad_error_{}.h5'.format(self.sink, orbit)
        # self.stash(features, destination)

        # construct validation
        median = numpy.percentile(abs(percent), 50)
        validation = {'storage': storage, 'data': data, 'traits': None, 'tag': 'validation', 'percents': percent}
        validation.update({'target': target, 'result': result, 'matrix': matrix, 'prediction': prediction})
        validation.update({'original': original, 'machines': machines, 'names': names})
        validation.update({'percent': median, 'maximum': abs(percent).max(), 'median': median})

        return validation

    def vectorize(self, data, features, sizes, transforms, machines=None):
        """Construct the matrix from data, assuming last feature is output.

        Arguments:
            data: dict of matrices
            features: list of str, name of features to include
            sizes: dict of pca sizes
            transform: list of str, the transformations to apply ('pca', 'scale', 'boxcox', 'normalize')
            machines: dict of previous machines

        Returns:
            (numpy array, numpy array, numpy array, list of str, dict)
        """

        # if no machines
        empty = False
        if not machines:

            # begin machines
            empty = True
            machines = {'perceptor': None}

        # begin list of arrays
        matrix = []
        target = []
        names = []

        # get number of samples
        last = features[-1]
        samples = data[last].shape[0]

        # for each feature
        for feature in features:

            # grab the array, upgrading precision to float64
            array = data[feature].astype('float64')

            # only if the shape begins with sample numbers
            if array.shape[0] == samples:

                # add dimension to array if needed
                if len(array.shape) < 2:

                    # add dimension and transpose
                    array = numpy.array([array]).transpose(1, 0)

                # set originals for last feature
                inputs = True
                if feature == last:

                    # copy array to originals
                    original = array.copy()
                    inputs = False

                # go through all transforms
                for transform in transforms:

                    # if empty
                    if empty:

                        # contruct machine first
                        size = sizes.get(feature, 0)
                        machines.setdefault(feature, {})
                        machines[feature][transform] = self._build(array, transform, size)

                    # apply machine
                    array = self._transform(array, transform, machines[feature][transform], inputs)

                # check for final feature
                if feature == last:

                    # add to target
                    target.append(array)

                # otherwise
                else:

                    # add to matrix and names
                    matrix.append(array)
                    names += ['{}_#{}'.format(feature, str(index).zfill(2)) for index in range(array.shape[1])]

        # stack matrices and targets
        matrix = numpy.hstack(matrix)
        target = numpy.hstack(target)

        return matrix, target, original, names, machines

    def weigh(self, tag='weights', number=-1):
        """Put all weights into a file for viewing.

        Arguments:
            tag: str, tag for files
            number: int, index of experiment

        Returns:
            None
        """

        # get the experiment
        experiments = self._load('{}/logs/experiments_{}.json'.format(self.sink, self.tag))
        experiment = experiments[number]
        weights = experiment['weights']

        # for each layer
        for index, layer in enumerate(weights):

            # start features
            features = []

            # create feature
            name = 'weights_layer_{}_t'.format(index)
            array = numpy.array(layer).transpose(1, 0)
            feature = Feature(['Categories', name], array)
            features.append(feature)

            # create variable
            name = 'weights_layer_{}_t_index'.format(index)
            array = numpy.array(list(range(array.shape[1])))
            variable = Feature(['IndependentVariables', name], array)
            features.append(variable)

            # create feature
            name = 'weights_layer_{}_n'.format(index)
            array = numpy.array(layer)
            feature = Feature(['Categories', name], array)
            features.append(feature)

            # create variable
            name = 'weights_layer_{}_n_index'.format(index)
            array = numpy.array(list(range(array.shape[1])))
            variable = Feature(['IndependentVariables', name], array)
            features.append(variable)

            # stash file
            self._make('{}/weights'.format(self.sink))
            destination = '{}/weights/Perceptor_Weights_{}.h5'.format(self.sink, self.tag)
            self.stash(features, destination, 'Data')

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 2
    arguments = arguments[:2]

    # umpack arguments
    sink, ticket = arguments

    # create albatross instance
    albatross = Albatross(sink, ticket=ticket)

    # perform training, using the ticket
    void = [None] * 9
    albatross.perceive(*void)




