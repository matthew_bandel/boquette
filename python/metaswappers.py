# metaswappers.py for the Metaswapper class to parse hdf files

# import system tools
import os
import sys
import shutil

# if in python3, use instruction from wiki
if sys.version_info.major == 3:

    # try to
    try:

        # import distro (not available in python 2)
        import distro

        # define path
        ACpath = '/tis/releases/ac/python-science/1.0.0/'

        # if running on Centos
        if distro.id() == 'centos':

            # define path for python3 distribution
            sys.path.insert(1,os.path.join(ACpath,'lib/python3.6/site-packages'))
            sys.path.insert(1,os.path.join(ACpath,'lib64/python3.6/site-packages'))
            print('Setting path for Python 3 on CentOS')

        # otherwise, for Ubuntu 20
        elif distro.id() == 'ubuntu':

            # define path for python 3 distribution
            sys.path.insert(1,os.path.join(ACpath,'lib/python3.8/site-packages'))
            print('Setting path for Python 3 on Ubuntu')

    # unless on nccs
    except ModuleNotFoundError:

        # skip
        print('skipping distro, on nccs?')

# otherwise, if python 2
if sys.version_info.major == 2:

    # define missing exceptions
    FileExistsError = OSError
    FileNotFoundError = IOError
    PermissionError = OSError

# print path
print('Path is', sys.path[1:])

# import general tools
import os
import re

# try to
try:

    # import pyhdf to handle Collection 3 hdf4 data
    from pyhdf.HDF import HDF, HDF4Error, HC
    from pyhdf.SD import SD, SDC, SDAttr
    from pyhdf.V import V

# unless it is not installed
except (ImportError, SystemError):

    # in which case, nevermind
    pass


# class Metaswapper to parse hdf files
class Metaswapper(list):
    """Metaswapper class to swap orbit numbers in metadata

    Inherits from:
        list
    """

    def __init__(self, source='', path='', orbit='', sink=''):
        """Initialize a Metaswapper instance.

        Arguments:
            source: str, filepath of source directory
            path: str, file name
            orbit: str, new orbit number
            sink: str, filepath of sink directory if difference than source

        Returns:
            None
        """

        # set directory information
        self.source = source
        self.path = path
        self.orbit = int(orbit)
        self.sink = sink or source

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Metaswapper instance at: {} >'.format(self.source)

        return representation

    def _copy(self, path, destination):
        """Copy a file from one path to another.

        Arguments:
            path: str, filepath
            destination: str, filepath

        Returns:
            None
        """

        # try to copy
        try:

            # copy the file
            shutil.copy(path, destination)

        # unless it is a directory
        except IsADirectoryError:

            # in which case, alert and skip
            self._print('{} is a directory'.format(path))

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        # # go through each meassage
        # for message in messages:
        #
        #     # print
        #     print(message)

        return message

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # try to
        try:

            # make paths
            paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # unless the directory does not exist
        except FileNotFoundError:

            # in which case, alert and return empty list
            self._print('{} does not exist'.format(directory))
            paths = []

        return paths

    def _stage(self, path):
        """Get the file meta attributes from the file name.

        Arguments:
            None

        Returns:
            dict
        """

        # extract the orbit specifics
        date = re.search('[0-9]{4}m[0-9]{4}t[0-9]{4}', path)
        orbit = re.search('-o[0-9]{5,6}', path)
        product = re.search('OM[A-Z,0-9]{3,10}', path)
        production = re.search('[0-9]{4}m[0-9]{4}t[0-9]{4}', path.split('_')[-1])
        version = re.search('_v[0-9]{3,4}', path)
        extension = '.{}'.format(path.split('.')[-1])

        # gather up the date details
        details = {}
        details['date'] = date.group() if date else '_'
        details['year'] = details['date'][:4] if len(str(date)) > 1 else '_'
        details['month'] = details['date'][:7] if len(str(date)) > 1 else '_'
        details['day'] = details['date'][:9] if len(str(date)) > 1 else '_'

        # and other details
        details['orbit'] = ('00' + orbit.group().split('-o')[1])[-6:] if orbit else '_'
        details['product'] = product.group() if product else '_'
        details['production'] = production.group() if production else '_'
        details['version'] = version.group().strip('_v') if version else '_'
        details['collection'] = '3' if '3' in details['version'] else '4'
        details['extension'] = extension

        return details

    def _tell(self, queue):
        """Enumerate the contents of a list.

        Arguments:
            queue: list

        Returns:
            None
        """

        # for each item
        for index, member in enumerate(queue):

            # print
            self._print('{}) {}'.format(index, member))

        # print spacer
        self._print(' ')

        return None

    def swap(self):
        """Xerox an hdf4 file.

        Arguments:
            path: str, file path
            orbit: int, new orbit number

        Returns:
            None
        """

        # get the path
        path = [entry for entry in self._see(self.source) if self.path in entry][0]

        # grab the original orbit number
        original = str(int(self._stage(path)['orbit']))
        orbit = str(self.orbit)

        # create destination
        destination = path.replace(original, orbit).replace('.he4', '_from_{}.he4'.format(original))
        if self.sink:

            # replace source with sink
            destination = destination.replace(self.source, self.sink)

        # copy file
        self._print('\ncopying {} to {}...'.format(path, destination))
        self._copy(path, destination)

        # open up hdf4 interfaces
        science = SD(destination, SDC.WRITE)

        # grab attributes
        core = 'CoreMetadata.0'
        attributes = science.attributes()
        text = attributes[core]

        # copy the string to the new string
        swap = text

        # set replacement fields
        fields = ('LOCALGRANULEID', 'ORBITNUMBER')

        # for each field
        for field in fields:

            # get the first and second orccurence
            first = swap.index(field)
            second = swap.index(field, first + 1)

            # print status
            self._print('\nreplacing: ')
            self._print(swap[first:second])

            # make swap
            swap = swap[:first] + swap[first:second].replace(original, orbit) + swap[second:]

            # print status
            self._print('\nwith: ')
            self._print(swap[first:second])

        # rewrite attribute
        attribute = science.attr(core)
        attribute.set(SDC.CHAR, swap)

        # close files
        science.end()

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 4
    arguments = arguments[:4]

    # umpack arguments
    source, path, orbit, sink = arguments

    # create metaswapper instance
    metaswapper = Metaswapper(source, path, orbit, sink)

    # perform swap
    metaswapper.swap()
