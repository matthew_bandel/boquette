#!/usr/bin/env python3

# generate_omto3_zonal_means for generating OMTO3 Zonal Means

# import system
import sys

# import local classes
from osirises import Osiris

# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad to four arguments
    arguments = arguments + [''] * 4
    arguments = arguments[:4]

    # unpack arguments, sink is the root directory for the study
    year, month, archive, sink = arguments

    # create osiris instance for ozone zonal means
    osiris = Osiris(sink)

    # set default number of orbits
    orbits = 500

    # check options
    for option in options:

        # for limited orbits by looking for --limit=x
        if option.startswith('--orbits='):

            # update limit
            orbits = int(option.split('=')[-1])

    # run zonal means collection
    osiris.zone(year, month, archive, limit=orbits)



