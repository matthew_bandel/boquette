# zippers.py to zip files together

# import reload
from importlib import reload

# import os
import os

# import zipfile
from zipfile import ZipFile


# class Zipper to zip files together
class Zipper(object):
    """class Zipper to zip together files.

    Inherits from:
        None
    """

    def __init__(self, title):
        """Initialize with a title, assuming zip directory.

        Arguments:
            title: str, zip file name
        """

        # add attribute
        self.title = title

        return

    def __repr__(self):
        """Create onscreen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = '< Zipper instance for: {} >'.format(self.title)

        return representation

    def zip(self, *names):
        """Zip files together.

        Arguments:
            *names, unpacked tuple of files to zip
            directory: boolean, grab all files within each directory

        Returns:
            None
        """

        # make title
        title = 'zipper/zips/{}'.format(self.title)

        # begin album
        album = ZipFile(title, 'w')

        # add files
        for name in names:

            # assume it is a directory
            if '.' not in name:

                # get all files from directory
                for path in ['{}/{}'.format(name, address) for address in os.listdir(name)]:

                    # add path, assuming directory is also desired
                    album.write(path, '/'.join(path.split('/')[-2:]))

            # otherwise
            else:

                # add name, assuming full path is not desired in the zip
                album.write(name, name.split('/')[-1])

        return None
