#!/usr/bin/env python3

# cumulos.py for the Cumulo class to build omi climatology

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import regex
import re

# import itertools
import itertools


# import numpy functions
import numpy
import math
from scipy import stats

# import datetime
import datetime

# import sklearn
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree
from sklearn.ensemble import RandomForestRegressor

# add warnings module
import warnings

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# try to
try:

    # import land sea mask
    import global_land_mask

# unless not found ( ubuntu? )
except ImportError:

    # nevermind
    print('global land mask not available')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Millipede to do OMI data reduction
class Cumulo(Hydra):
    """Cumulo class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # make squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Cumulo instance qt {} >'.format(self.sink)

        return representation

    def _align(self, times, timesii, latitudes):
        """Determine best alignment amonst two files' orbits, using only ascending data

        Arguments:
            times: numpy array of times, omcldrr
            timesii: numpy array of times, omto3

        Returns:
            tuple of ints, the indices
        """

        # determine latitude bounds
        south = latitudes.min(axis=1).argsort()[0]
        north = latitudes.max(axis=1).argsort()[-1]

        # find the differences between the southern most time
        differences = (timesii - times[south]) ** 2

        # get starting indices
        start = south
        startii = numpy.argsort(differences)[0]

        # get minimum lengths
        length = min([timesii.shape[0] - startii, times.shape[0] - (south + north)])

        # get stopping indices
        stop = start + length
        stopii = startii + length

        return start, stop, startii, stopii

    def _bite(self, tensor, bit, masquerade):
        """Combine boolean masks for a particular bit.

        Arguments:
            tensor: numpy.array
            bit: int
            masquerade: dict of boolean masks

        Returns:
            numpy array
        """

        # convert to int
        bit = int(bit)

        # decompose all uniques
        masks = []
        for unique, mask in masquerade.items():

            # decompose into binary form
            decomposition = bin(unique).replace('0b', '0' * 20)
            decomposition = decomposition[::-1]
            if decomposition[bit] == '1':

                # add to codes
                masks.append(mask)

        # add empty mask
        empty = numpy.zeros(tensor.shape)

        # summ together for new mask
        masque = empty
        for entry in masks:

            # add to the running sum
            masque += entry

        return masque

    def _crystalize(self, data, latitudes, longitudes, segment=20):
        """Bin the geographic data by percentile bins.

        Arguments:
            data: numpy array of measurement data
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes
            segment=20: size of bin

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # begin geodes
        geodes = []

        # for each bracket
        brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
        for first, second in brackets:

            self._print(first, second)

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            datum = data[mask].flatten()
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            # concatenate
            geode = numpy.vstack([datum, latitude, longitude])

            # order along latitude, then longitude
            geode = geode[:, numpy.argsort(latitude)]
            geode = geode[:, numpy.argsort(longitude)]

            # append
            geodes.append(geode)

        return geodes, brackets

    def _exchange(self, path, destination, codex):
        """Swap out yaml attributes:

        Arguments:
            path: str, file path to source yaml
            destination: str, file path to destination yaml
            codex: dict, the update dictionary

        Returns:
            None
        """

        # open up yaml
        yam = self._acquire(path)

        # make updates
        yam.update(codex)

        # redispence
        self._dispense(yam, destination)

        return None

    def _exclude(self, quality, flags):
        """Create boolean mask for excluding quality flagged data.

        Arguments:
            quality: numpy array
            flags: list of ints, the exclusion flags

        Returns:
            boolean mask, absence of excluded flags
        """

        # get the masquerade of unique value masks
        masquerade = self._masquerade(quality)

        # begin list off masks
        masks = []

        # for each flag
        for flag in flags:

            # get the boolean mask for that bit
            mask = self._bite(quality, flag, masquerade)
            masks.append(mask)

        # sum masks, and take innverse
        masque = numpy.array(masks).sum(axis=0).astype(int)
        inverse = (masque == 0)

        return inverse

    def _masquerade(self, tensor):
        """Create boolean feature masks for each unique feature in the tensor.

        Arguments:
            tensor: numpy array

        Returns:
            dict
        """

        # get all unique values, using a method faster than numpy.unique
        integers = numpy.array(tensor).astype(int)
        zeros = numpy.zeros(numpy.max(integers) + 1, dtype=int)
        zeros[integers.ravel()] = 1
        uniques = numpy.nonzero(zeros)[0]

        # create boolean masks and convert to ints
        masks = {unique: tensor == unique for unique in uniques}

        return masks

    def _paint(self, arrays, path, texts, scale, spectrum, limits, grid=1, **options):
        """Make matplotlib polygon figure.

        Arguments:
            arrays: list of numpy arrays, tracer, latitude, longitude
            texts: list of str, title and units
            path: str, filepath to destination
            scale: tracer scale limits
            spectrum: list of (str, list of ints), the gradient name and sub indices
            limits: tuple of ints, lat and lon limits
            grid: trackwise number of combined polygons along latitude
            **options: unpacked dictionary of the following:
                size: tuple of ints, figure size
                log: boolean, use log scale?
                bar: boolean, add colorbar?
                back: str, background color
                north: boolean, use north projection?
                projection: name of projection

        Returns:
            None
        """

        # set defaults
        size = options.get('size', (10, 5))
        back = options.get('back', 'white')
        bar = options.get('bar', True)
        log = options.get('log', False)
        north = options.get('north', True)
        extension = options.get('extension', 'both')

        # set projections
        native = cartopy.crs.PlateCarree()
        projection = options.get('projection', cartopy.crs.PlateCarree())

        # unpack lists and set defaults
        tracer, latitude, longitude = arrays
        title, units = texts
        gradient, selection = spectrum

        # create polygons and resolve tracer
        polygons = self._polymerize(latitude, longitude, grid)
        tracer = self._resolve(tracer, grid)
        polygons, tracer = self._excise(polygons, tracer)

        # set up figure
        pyplot.clf()
        # pyplot.figure(figsize=size, facecolor='black')
        # pyplot.margins(0.9, 0.9)

        # set cartopy axis with coastlines
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=3)
        _ = axis.gridlines(draw_labels=True)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left_margin = 0.1
        right_margin = 0.1
        top_margin = 0.19
        bottom_margin = 0.18
        pyplot.subplots_adjust(left=left_margin, right=1-right_margin, top=1-top_margin, bottom=bottom_margin)

        # if not colorbar
        if not bar:

            # set aspects
            axis.set_aspect('auto')
            axis.set_global()

        # set axis ratiio
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        axis.set_xlim(limits[2], limits[3])
        axis.set_ylim(limits[0], limits[1])
        axis.set_title(title, fontsize=14)

        # constuct patches from polygons
        patches = []
        quantities = []
        polygons = polygons.reshape(-1, 8)
        for sample, quantity in zip(polygons, tracer):

            # if within range
            if (quantity >= scale[0]) & (quantity <= scale[1]):

                # set up patches
                patch = [(sample[4 + index], sample[0 + index]) for index in range(4)]
                patch = numpy.array(patch)
                patch = matplotlib.patches.Polygon(xy=patch, linewidth=0.01)
                patches.append(patch)

                # append to quantities
                quantities.append(quantity)

        # make array from quantities
        quantities = numpy.array(quantities)

        # set up color scheme, removing first two
        colors = matplotlib.cm.get_cmap(gradient)
        # colors = colors[2:]

        # remove first two colors (magenta and light purple) from front of colormap
        # start = 40
        # selection = numpy.linspace(start, colors.N, colors.N - start + 1).astype(int)
        selection = numpy.array(selection)
        colors = matplotlib.colors.ListedColormap(colors(selection))

        # crate color bounds
        bounds = numpy.linspace(*scale, 11)
        normal = matplotlib.colors.BoundaryNorm(bounds, colors.N)

        # if logarithm
        if log:

            # use log scale
            normal = matplotlib.colors.LogNorm(0.001, scale[1])

        # plot polygons with colormap
        collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=1.0)
        collection.set_edgecolor("face")
        collection.set_clim(scale)
        collection.set_array(quantities)
        collection = axis.add_collection(collection)

        # if bar
        if bar:

            # add colorbar
            height = 0.1 * size[1] / 5
            position = axis.get_position()
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            barii = pyplot.gcf().add_axes([position.x0, position.y0 - 0.08, position.width, 0.02])
            #colorbar = pyplot.gcf().colorbar(scalar, cax=barii, orientation='horizontal', extend=extension)
            colorbar = pyplot.gcf().colorbar(collection, cax=barii, orientation='horizontal', extend=extension)
            colorbar.set_label(units)

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        pyplot.savefig(path)
        pyplot.clf()

        return None

    def _segregate(self, latitudes, longitudes, latitudesii, longitudesii):
        """Segregate pairs of geolocation coordinates into those within or without a polygon.

        Arguments:
            latitudes: list of float, latitude coordinates of sample points in question
            longitudes: list of float, longitude coordinates of sample points in question
            latitudesii: list of float, latitude coordinates of enclosing polygon
            longitudesii: list of floats, longitude coordinates of enclosing polygon.

        Returns:
            ( numpy array, numpy array ) tuple, the coordinates of the points within and without the polygon

        Notes:
            Assumes latitudesii and longitudesii are given in order around the perimeter,
            and do not include a redundant final point.

            Assumes all angles between points are <= 180 degrees
        """

        # start clock
        #self._stamp('segregaring {} points...'.format(len(latitudes)), initial=True)

        # create samples and perimeter as numpy arrays
        #self._stamp('calculating polygon...')
        samples = numpy.array([latitudes, longitudes]).transpose(1, 0)
        perimeter = numpy.array([latitudesii, longitudesii]).transpose(1, 0)

        # duplicate first entry at last position as well
        polygon = numpy.concatenate([perimeter, [perimeter[0]]])

        # create segments from consecutive pairs of points
        segments = [polygon[index: index + 2, :] for index, _ in enumerate(polygon[:-1])]

        # calculate slopes of all segments as delta latitude / delta longitude
        slopes = [(segment[1, 0] - segment[0, 0]) / (segment[1, 1] - segment[0, 1]) for segment in segments]

        # calculate intercepts from first point: b = y0 - m(x0) = lat0 - m(lon0)
        intercepts = [(segment[0, 0] - slope * segment[0, 1]) for segment, slope in zip(segments, slopes)]

        # calculate center point
        center = (latitudesii.mean(), longitudesii.mean())

        # determine polarities (True = up ) for each segment by comparing the midpoint to the center
        zipper = zip(segments, slopes, intercepts)
        polarities = [center[0] > slope * center[1] + intercept for segment, slope, intercept in zipper]

        # create boolean masks for each segment
        #self._stamp('making masks for each side...')
        masks = []
        for segment, slope, intercept, polarity in zip(segments, slopes, intercepts, polarities):

            # if polarity is True
            if polarity:

                # create boolean mask for all points where lat >= m * lon + b
                mask = samples[:, 0] > slope * samples[:, 1] + intercept
                masks.append(mask)

            # otherwise
            else:

                # create boolean mask for all points where  lat <= m * lon + b
                mask = samples[:, 0] < slope * samples[:, 1] + intercept
                masks.append(mask)

        # multiply all masks for polygon mask and make inverse
        masque = numpy.prod(masks, axis=0).astype(bool)
        inverse = numpy.logical_not(masque)

        # split into insiders and outsiders
        #self._stamp('filtering data...')
        insiders = samples[masque]
        outsiders = samples[inverse, :]

        # timestamp
        #self._print('{} insiders, {} outsiders'.format(len(insiders), len(outsiders)))
        #self._stamp('segregated.')

        return insiders, outsiders, center, polygon

    def _smooth(self, pressure, number, land, sea, degrees=[3]):
        """Smooth data using 3 x 3 grid and land, sea masks.

        Arguments:
            pressure: numpy array
            number: numpy array
            land: numpy array
            sea: numpy array
            degrees: list of ints, the degrees for progressive smoothing

        Returns:
            numpy array
        """

        # create 30 degree brackets and default value to 0
        brackets = [(0, 30), (30, 60), (60, 90), (90, 120), (120, 150), (150, 180)]
        lands = {north: 0.0 for north, _ in brackets}
        seas = {north: 0.0 for north, _ in brackets}

        # create weights matrix to weigh the average based on sample count
        weights = numpy.where(number < 10, number, 10)

        # compute the weighted average for inner brackets
        for north, south in brackets[1:-1]:

            # make all weights at least 1 for inner brackets
            weights[north: south, :] = numpy.where(number[north: south, :] > 1, weights[north: south, :], 1)

            # create land average
            band = pressure[north: south, :]
            weight = weights[north: south, :]
            mask = land[north: south, :] * (band > 0)
            average = (band[mask] * weight[mask]).sum() / weight[mask].sum()
            lands[north] = average

            # create sea average
            band = pressure[north: south, :]
            mask = sea[north: south, :] * (band > 0)
            average = (band[mask] * weight[mask]).sum() / weight[mask].sum()
            seas[north] = average

        # begin grid
        grid = numpy.zeros((180, 360))

        # for each degree
        smoothers = []
        for degree in degrees:

            # determine steps
            half = math.floor(degree / 2)
            steps = list(range(-half, half + 1))

            # begin shifts
            shifts = []
            for step in steps:

                # for each additional step
                for stepii in steps:

                    # add to shifts
                    shift = (step, 0, stepii, 1)
                    shifts.append(shift)

            # append shifts to smoothers
            smoothers.append(shifts)

        # for each mask
        for filter, averages in zip((land, sea), (lands, seas)):

            # set smear
            smear = pressure.copy()

            # set measure to weigthts
            measures = weights.copy()

            # go through each bracket
            for north, south in brackets:

                # get masque
                masque = filter[north: south, :]

                # fill missing values with avearge
                band = smear[north: south, :]
                band = numpy.where(band > 0, band, averages[north])
                band = numpy.where(masque, band, 0)

                # fill in for measures as, making weight 0 if not in filter
                measures[north: south] = numpy.where(masque, measures[north: south], 0)

                # add band to grid
                smear[north: south, :] = band

            # for each smoother
            for shifts in smoothers:

                # copy filter to get samples
                quantities = smear.copy() * measures.copy()
                samples = measures.copy()

                # begin totals
                totals = []
                counts = []

                # for each shift
                for shift, axis, shiftii, axisii in shifts:

                    # roll the quantities
                    quantity = numpy.roll(quantities.copy(), shift, axis=axis)
                    quantity = numpy.roll(quantity, shiftii, axis=axisii)
                    totals.append(quantity)

                    # roll the sample counts
                    sample = numpy.roll(samples.copy(), shift, axis=axis)
                    sample = numpy.roll(sample, shiftii, axis=axisii)
                    counts.append(sample)

                # sum together totals and counts
                totals = numpy.array(totals).sum(axis=0)
                counts = numpy.array(counts).sum(axis=0)

                # compute the smeared pressure
                smear = totals / counts
                smear = numpy.where(numpy.isfinite(smear), smear, 0)

            # go through each bracket
            for north, south in brackets:

                # get masque
                masque = filter[north: south, :]

                # make latitude band, using fill
                band = smear[north: south, :]

                # band = numpy.where(band > 0, band, averages[north])
                band = numpy.where(masque, band, 0)

                # add band to grid
                grid[north: south, :] = grid[north: south, :] + band

        return grid

    def _understand(self, records, reflectivity, extent, bins=10):
        """Construct the histograms.

        Arguments:
            records: list of list of dicts, the records
            reflectivity: str, reflectivity level
            extent: int, number of records to average
            bins: int, number of bins

        Returns:
            None
        """

        # collect all delta pressures
        pressures = []
        for record in records:

            # average appropriate entries
            pressure = numpy.average([entry['deltas']['pressure ( hPa )'] for entry in record[:extent]])
            pressures.append(pressure)

        # determine 95th and 5th percentile
        lower = numpy.percentile(numpy.array(pressures), 5)
        upper = numpy.percentile(numpy.array(pressures), 95)

        # break into equal pieces of a certain width
        width = (upper - lower) / bins

        # define brackets
        brackets = [(lower + width * index, lower + width * (index + 1)) for index in range(bins)]
        brackets = [(lower - width, lower)] + brackets + [(upper, upper + width)]

        # create middle of each bin
        middles = list([sum(bracket) / 2 for bracket in brackets])

        # get counts for heights
        bounds = brackets
        bounds[0] = (min(pressures), brackets[0][1])
        bounds[1] = (brackets[-1][0], max(pressures))
        boxes = [[pressure for pressure in pressures if bracket[0] <= pressure <= bracket[1]] for bracket in bounds]
        counts = list([len(box) for box in boxes])

        # make labels
        ordinate = 'concurrent pairs'
        abscissa = 'delta Pressure (hPa) NMCLDRR - OMCLDRR'

        # construct title attributes
        pairs = len(records)
        dates = [record[0]['omi']['date'] for record in records]
        dates.sort()
        start = dates[0][:7]
        finish = dates[-1][:7]

        # create bracket descriptions
        descriptions = {'low': 'refl. < 0.3', 'middle': '0.3 < refl. < 0.7', 'high': '0.7 < refl.', 'all': 'all refl.'}

        # make title
        title = 'Cloud Pressure Difference, NMCLDRR - OMCLDRR, {}'.format(descriptions[reflectivity])
        title += '\n ( {} pairs from {} to {}, < 30 sec, 1 km apart, avg of {} )'.format(pairs, start, finish, extent)

        # dump into json
        plot = {'width': width, 'middles': middles, 'counts': counts, 'ordinate': ordinate, 'abscissa': abscissa}
        plot.update({'title': title})
        self._dump(plot, '../studies/cumulo/plots/histogram_{}_{}.json'.format(extent, reflectivity))

        # add file for histograms
        route = ['Categories', 'delta_pressure_histogram']
        array = numpy.array(pressures)
        feature = Feature(route, array)
        destination = '../studies/cumulo/histograms/histogram_{}_{}.h5'.format(extent, reflectivity)
        self.stash([feature], destination, 'Data')

        return None

    def acclimatize(self, strategy=None, plan=None, months=None, source='clouds', sourceii='swap', sink='monthlies', sinkii='plots'):
        """Create test file from climatology, including filling empty spots.

        Arguments:
            strategy: list of tuples of ints, the reflectivity brackets
            plan: list of year, first row, last row tuples
            source: str, source folder for climatology files
            sourceii: str, source folder for swap file
            sink: str, sink folder for trial
            sinkii: str, sink folder for maps

        Returns:
            None
        """

        # set default strategy and months
        months = months or list(range(1, 13))
        strategy = strategy or [(3, 6), (6, 7), (7, 8), (8, 9), (9, 10), (3, 8), (8, 10), (6, 8)]

        # set default plan for years and rows
        plan = plan or [(year, 3, 57) for year in range(2005, 2008)] + [(year, 3, 20) for year in range(2008, 2022)]

        # craete sink folders
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # gather land sea mask
        hydra = Hydra('{}/mask'.format(self.sink))
        hydra.ingest(0)
        mask = hydra.dig('land_sea_mask')[0].distil()
        land = mask.astype(bool)
        sea = numpy.logical_not(land)

        # gather climatology path
        folder = '{}/{}'.format(self.sink, source)
        hydra = Hydra(folder)

        # create hydra for swaps
        swaps = Hydra('{}/{}'.format(self.sink, sink))

        # get source file path
        original = hydra._see('{}/{}'.format(self.sink, sourceii))[0]
        originals = Hydra(original)
        originals.ingest(0)

        # create latitude/ longitude grid centered at half degrees, N to S, W to E
        latitudes = numpy.array([[89.5 - index for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[index - 179.5 for index in range(360)] for _ in range(180)])

        # flatten arrays
        latitudes = latitudes.flatten()
        longitudes = longitudes.flatten()

        # begin climatology data reservoirs
        counts = {}
        totals = {}
        squares = {}

        # create month strings, and go through each month
        months = [str(month).zfill(2) for month in months]
        for month in months:

            # print status
            self._print('month {}...'.format(month))

            # begin data with empty arrays for reflectivity, latitude, and longiurde (rows and years added together)
            counts[month] = numpy.zeros((11, 180, 360))
            totals[month] = numpy.zeros((11, 180, 360))
            squares[month] = numpy.zeros((11, 180, 360))

            # for each year
            for year, left, right in plan:

                # collect particular month at certain years
                climatology = [path for path in hydra.paths if str(year) in path and month in path][0]
                hydra.ingest(climatology)

                # add to data accumulation, summing across appropriate rows
                counts[month] += hydra.dig('ocp_sample_count')[0].distil()[:, left: right, :, :].sum(axis=1)
                totals[month] += hydra.dig('ocp_sum')[0].distil()[:, left: right, :, :].sum(axis=1)
                squares[month] += hydra.dig('ocp_sum_of_squares')[0].distil()[:, left: right, :, :].sum(axis=1)

        # add all months together for annual average
        counts['annual'] = numpy.array([counts[month] for month in months]).sum(axis=0)
        totals['annual'] = numpy.array([totals[month] for month in months]).sum(axis=0)
        squares['annual'] = numpy.array([squares[month] for month in months]).sum(axis=0)

        # for each reflectivity bin
        for low, high in strategy:

            # unpack brackets
            formats = [str(low).zfill(2), str(high).zfill(2)]
            tag = '_monthly_climatology_reflectivity_{}_{}.he4'.format(*formats)
            replacement = original.replace(sourceii, sink).replace('.he4', tag)
            annual = replacement.replace('monthly', 'annual')
            rough = replacement.replace('monthly', 'monthly_unsmoothed')

            # copy original
            self._copy(original, replacement)
            self._copy(original, annual)
            self._copy(original, rough)

            # create monthly reservoirs
            averages = {}
            deviations = {}
            samples = {}
            smears = {}

            # go through each month
            for month in months + ['annual']:

                # print status
                self._print('month {}...'.format(month))

                # create bins
                count = counts[month][low: high, :, :].sum(axis=0)
                total = totals[month][low: high, :, :].sum(axis=0)
                square = squares[month][low: high, :, :].sum(axis=0)

                # calculate average pressures
                average = total / count

                # calculate variance as average of squares - square of average
                variance = (square / count) - average ** 2
                deviation = numpy.sqrt(variance)

                # replace average with fill values 0 or less and change type
                fill = -9999.0
                average = numpy.where(average > 0, average, fill)
                average = average.astype('float32')

                # replace deviations with fill value for 0 or infinite
                deviation = numpy.where(numpy.isfinite(deviation), deviation, fill)
                deviation = numpy.where(deviation > 0, deviation, fill)

                # smear and fill using 3 x 3 grid averages
                smear = self._smooth(average, count, land, sea, degrees=[5, 5]).astype('float32')

                # store for later
                averages[month] = average
                deviations[month] = deviation
                samples[month] = count
                smears[month] = smear

            # go through each month
            for month in months:

                # insert into both the monthly and the annual file
                identity = 'cloudPressures{}'.format(month)
                grid = 'cloud_pressureGrid'
                swaps._insert(smears[month], replacement, identity, grid)
                swaps._insert(smears['annual'], annual, identity, grid)
                swaps._insert(averages[month], rough, identity, grid)

                # begin features for heatmap
                features = []
                extension = '_m{}_heatmaps.h5'.format(month)
                destination = replacement.replace(sink, sinkii).replace('.he4', extension)

                # grab the clouds pressures for the month
                clouds = originals.dig('cloudPressures{}'.format(month))[0].distil()

                # flatten arrays
                clouds = clouds.flatten()
                count = samples[month].flatten()
                average = smears[month].flatten()
                deviation = deviations[month].flatten()

                # add orginal
                name = 'heatmap_original_cloud_pressures'
                measure = clouds
                mask = measure > 0
                array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                bounds = [300, 400, 500, 600, 700, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_unsmoothed_pressure'
                measure = averages[month].flatten()
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [300, 400, 500, 600, 700, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_climatology'
                measure = average
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [300, 400, 500, 600, 700, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_pressure_full'
                measure = average
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [0, 200, 400, 600, 800, 1000, 1200]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_pressure_difference'
                measure = average
                mask = measure > 0
                array = numpy.vstack([[clouds[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [200 * index - 600 for index in range(7)]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create sample counts
                name = 'heatmap_sample_counts'
                measure = count
                mask = measure >= 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [0, 1, 5, 25, 50, 100, 200, 500, 1000]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} samples'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'samples'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_pressure_standard_deviation'
                measure = deviation
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [0, 25, 50, 75, 100, 125, 150]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_standard_avg_error'
                measure = deviation / numpy.sqrt(count)
                mask = (measure > 0) & numpy.isfinite(measure)
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [0, 4, 8, 12, 16, 20]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_smoothed_annual_pressure'
                measure = smears['annual'].flatten()
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [450, 500, 550, 600, 650, 700, 750, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_unsmoothed_annual_pressure'
                measure = averages['annual'].flatten()
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [450, 500, 550, 600, 650, 700, 750, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # create mean pressures
                name = 'heatmap_pressure_annual_standard_deviations'
                measure = deviations['annual'].flatten()
                mask = measure > 0
                array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                bounds = [0, 50, 100, 150, 200, 250]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                feature = Feature(['Categories', name], array, attributes=attributes)
                features.append(feature)

                # stash at destination
                self.stash(features, destination)

        return None

    def accumulate(self, year, month, start, stop, sink='clouds', sinkii='scenes'):
        """Accumulate cloud data.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # create destination of file
        destination = '{}/{}/OMCLDRR_Climatology_{}_{}.h5'.format(self.sink, sink, year, self._pad(month))
        destinationii = '{}/{}/OMCLDRR_Scene_Climatology_{}_{}.h5'.format(self.sink, sinkii, year, self._pad(month))

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date
            squares = hydra.dig('ocp_sum_of_squares')[0].distil()
            totals = hydra.dig('ocp_sum')[0].distil()
            counts = hydra.dig('ocp_sample_count')[0].distil()
            orbits = hydra.dig('orbit_number')[0].distil()

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squares = numpy.zeros((11, 60, 180, 360)).astype('float32')
            totals = numpy.zeros((11, 60, 180, 360)).astype('float32')
            counts = numpy.zeros((11, 60, 180, 360)).astype('int')
            orbits = numpy.array([])

        # check for destination
        if destinationii in self._see('{}/{}'.format(self.sink, sinkii)):

            # load up file
            hydra = Hydra(destinationii)
            hydra.ingest(0)

            # extract date
            squaresii = hydra.dig('ocp_sum_of_squares')[0].distil()
            totalsii = hydra.dig('ocp_sum')[0].distil()
            countsii = hydra.dig('ocp_sample_count')[0].distil()
            orbitsii = hydra.dig('orbit_number')[0].distil()

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squaresii = numpy.zeros((11, 60, 180, 360)).astype('float32')
            totalsii = numpy.zeros((11, 60, 180, 360)).astype('float32')
            countsii = numpy.zeros((11, 60, 180, 360)).astype('int')
            orbitsii = numpy.array([])

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/acps/OMI/10004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # get corresponding total ozone directory for aerosol flags
        #hydraii = Hydra('/tis/OMI/10003/OMTO3/{}/{}'.format(*formats), start, stop)

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths:

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # skip if orbit already made
            if orbit not in orbits:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # ingest the path
                hydra.ingest(path)

                # # find related total ozone path
                # aerosols = [member for member in hydraii.paths if member.endswith('.he5')]
                # aerosols = [member for member in aerosols if int(self._stage(member)['orbit']) == orbit][0]
                # hydraii.ingest(aerosols)

                # # get OMCLDRR times and OMTO3 times
                times = hydra.dig('Time')[0].distil()
                # timesii = hydraii.dig('Time')[0].distil()

                # # gather latitude data
                # latitudes = hydra.dig('Latitude')[0].distil()

                # # determine alignment
                # start, stop, startii, stopii = self._align(times, timesii, latitudes)
                start = 0
                stop = times.shape[0]

                # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
                quality = hydra.dig('ProcessingQualityFlagsforO3')[0].distil()[start: stop].flatten()
                ground = hydra.dig('GroundPixelQualityFlags')[0].distil()[start: stop].flatten()

                # get other data
                clouds = hydra.dig('CloudPressureforO3')[0].distil()[start: stop].flatten()
                latitudes = hydra.dig('Latitude')[0].distil()[start: stop].flatten()
                longitudes = hydra.dig('Longitude')[0].distil()[start: stop].flatten()
                reflectivities = hydra.dig('Reflectivity')[0].distil()[start: stop].flatten()
                rows = numpy.array([list(range(60)) for _ in range(int(len(clouds) / 60))]).flatten()

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # round reflectivity
                reflectivities = numpy.floor(reflectivities * 11)
                reflectivities = numpy.where(reflectivities > 10, 10, reflectivities)
                reflectivities = numpy.where(reflectivities < 0, 0, reflectivities)

                # create stack
                stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
                stack = stack.transpose(1, 0)

                # exclude particular flags
                #bits = [0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15]
                bits = [0, 1, 2, 3, 4, 5, 7, 13, 14, 15]
                masque = self._exclude(quality, bits)

                # exclude descending flag, night side data
                bits = [2, 3]
                masqueii = self._exclude(ground, bits)

                # create masque
                mask = numpy.logical_and(masque, masqueii)

                # exclude same particular flags, except snow/ice
                #bits = [0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15]
                bits = [0, 1, 2, 3, 4, 7, 13, 14, 15]
                unflagged = self._exclude(quality, bits)

                # exclude descending flag, night side data
                bits = [2, 3]
                ascending = self._exclude(ground, bits)

                # only use data from snow/ice
                bits = [5]
                ice = numpy.logical_not(self._exclude(quality, bits))

                # create secondary mask for scene pressure
                maskii = numpy.logical_and(unflagged, ascending)
                maskii = numpy.logical_and(maskii, ice)

                # print exclusion message
                formats = (mask.sum(), len(quality))
                self._print('kept {} out of {} points without designated quality issues'.format(*formats))

                # # dig up aerosol screen
                # aerosol = hydraii.dig('UVAerosolIndex')[0].distil()[startii: stopii].flatten()
                # screen = aerosol < 2

                # try to
                try:

                    # # create final mask
                    # #mask = numpy.logical_and(screen, masque)
                    # mask = masque

                    # apply to stack and clouds
                    cloudsii = clouds[maskii]
                    stackii = stack[maskii]
                    clouds = clouds[mask]
                    stack = stack[mask]

                    # for each row
                    for strip, cloud in zip(stack, clouds):

                        # verify no fill
                        if cloud > 0 and numpy.isfinite(cloud):

                            # unpack data
                            reflectivity, row, latitude, longitude = strip

                            # update
                            totals[reflectivity, row, latitude, longitude] += cloud
                            squares[reflectivity, row, latitude, longitude] += cloud ** 2
                            counts[reflectivity, row, latitude, longitude] += 1

                    # append(orbits)
                    orbits = numpy.array([entry for entry in orbits] + [orbit])
                    orbits.sort()

                    # for each row
                    for strip, cloud in zip(stackii, cloudsii):

                        # verify no fill
                        if cloud > 0 and numpy.isfinite(cloud):

                            # unpack data
                            reflectivity, row, latitude, longitude = strip

                            # update
                            totalsii[reflectivity, row, latitude, longitude] += cloud
                            squaresii[reflectivity, row, latitude, longitude] += cloud ** 2
                            countsii[reflectivity, row, latitude, longitude] += 1

                    # append(orbitsii)
                    orbitsii = numpy.array([entry for entry in orbitsii] + [orbit])
                    orbitsii.sort()

                    # stamp
                    self._stamp('processed {}.'.format(orbit))

                # unless mismatch
                except ValueError:

                    # stamp
                    self._stamp('error in orbit {}, skipped!'.format(orbit))

        # construct features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum_of_squares'], squares, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum'], totals,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sample_count'], counts,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['orbit_number'], orbits, attributes=attributes))

        # stash file
        self.stash(features, destination, compression=9)

        # construct scene pressure features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum_of_squares'], squaresii, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum'], totalsii,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sample_count'], countsii,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['orbit_number'], orbitsii, attributes=attributes))

        # stash file
        self.stash(features, destinationii, compression=9)

        return None

    def aerate(self, year, month, start, stop, sink='aerosols'):
        """Accumulate cloud data, along with aerosol data.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))

        # create destination of file
        destination = '{}/{}/OMCLDRR_Climatology_{}_{}.h5'.format(self.sink, sink, year, month)

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date
            squares = hydra.dig('ocp_sum_of_squares')[0].distil()
            totals = hydra.dig('ocp_sum')[0].distil()
            counts = hydra.dig('ocp_sample_count')[0].distil()
            orbits = hydra.dig('orbit_number')[0].distil()
            aerosols = hydra.dig('aerosol_index')[0].distil()
            #flags = [hydra.dig('processing_flag_count_{}'.format(str(bit).zfill(2)))[0].distil() for bit in range(16)]

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squares = numpy.zeros((11, 60, 180, 360))
            totals = numpy.zeros((11, 60, 180, 360))
            counts = numpy.zeros((11, 60, 180, 360))
            aerosols = numpy.zeros((11, 60, 180, 360))
            orbits = numpy.array([])
            #flags = [numpy.zeros((10, 60, 180, 360)) for bit in range(16)]

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # create hydra for aerosol
        aerizer = Hydra('/tis/OMI/85100/OMTO3/{}/{}'.format(*formats), start, stop)

        # begin sahara collection
        sahara = [[], []]

        # for each path
        for path in hydra.paths:

            # ingest the path
            hydra.ingest(path)

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # get aerosol path
            pathii = [member for member in aerizer.paths if int(self._stage(member)['orbit']) == int(orbit)][0]
            aerizer.ingest(pathii)

            # skip if orbit already made
            if orbit not in orbits:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # append(orbits)
                orbits = numpy.array([entry for entry in orbits] + [orbit])
                orbits.sort()

                # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
                quality = hydra.dig('ProcessingQualityFlagsforO3')[0].distil().flatten()

                # # also, add to flag counts
                # masks = []
                # for bit in range(16):
                #
                #     # invert exclusion mask for single bit for counts of flag occurence
                #     flag = self._exclude(quality, [bit])
                #     flag = (flag == 0).astype(int)
                #     masks.append(flag)

                # get other data
                clouds = hydra.dig('CloudPressureforO3')[0].distil().flatten()
                latitudes = hydra.dig('Latitude')[0].distil().flatten()
                longitudes = hydra.dig('Longitude')[0].distil().flatten()
                reflectivities = hydra.dig('Reflectivity')[0].distil().flatten()
                aerosol = aerizer.dig('UVAerosolIndex')[0].distil().flatten()
                rows = numpy.array([list(range(60)) for _ in range(int(len(clouds) / 60))]).flatten()

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # round reflectivity
                reflectivities = numpy.floor(reflectivities * 11)
                reflectivities = numpy.where(reflectivities > 10, 10, reflectivities)
                reflectivities = numpy.where(reflectivities < 0, 0, reflectivities)

                # create stack
                stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
                stack = stack.transpose(1, 0)

                # # for each flag mask
                # for bit, mask in enumerate(masks):
                #
                #     # convert to int
                #     mask = mask.astype(int)
                #
                #     # for each row
                #     for strip, pixel in zip(stack, mask):
                #
                #         # unpack data
                #         reflectivity, row, latitude, longitude = strip
                #
                #         # update
                #         flags[bit][reflectivity, row, latitude, longitude] += pixel

                # exclude particular flags
                bits = [0, 1, 2, 3, 4, 5, 7, 13, 14, 15]
                masque = self._exclude(quality, bits)

                # print exclusion message
                formats = (masque.sum(), len(quality))
                self._print('kept {} out of {} points without designated quality issues'.format(*formats))

                # apply to stack and clouds
                clouds = clouds[masque]
                aerosol = aerosol[masque]
                stack = stack[masque]

                # # applu masque
                # reflectivities = reflectivities[masque]
                # rows = row[masque]
                # latitudes = latitudes[masque]
                # longitudes = longitudes[masque]
                #
                # # create stack
                # stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
                # stack = stack.transpose(1, 0)

                # for each row
                for strip, cloud, air in zip(stack, clouds, aerosol):

                    # verify no fill
                    if cloud > 0 and numpy.isfinite(cloud):

                        # unpack data
                        reflectivity, row, latitude, longitude = strip

                        # update
                        aerosols[reflectivity, row, latitude, longitude] += air
                        totals[reflectivity, row, latitude, longitude] += cloud
                        squares[reflectivity, row, latitude, longitude] += cloud ** 2
                        counts[reflectivity, row, latitude, longitude] += 1

                        # check for sahara conditions
                        if (reflectivity == 6) and (60 < latitude < 90) and (170 < longitude < 220):

                            # add to sahara
                            sahara[0].append(cloud)
                            sahara[1].append(air)

                # stamp
                self._stamp('processed.'.format(orbit))

        # construct features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['Categories', 'ocp_sum_of_squares'], squares, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['Categories', 'ocp_sum'], totals,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['Categories', 'ocp_sample_count'], counts,  attributes=attributes))
        attributes = {'units': '_', 'description': 'uv aerosol index'}
        features.append(Feature(['Categories', 'aerosols'], aerosols,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['Categories', 'orbit_number'], orbits, attributes=attributes))

        # # add flag counts
        # for bit in range(16):
        #
        #     # create feature from flag count
        #     attributes = {'units': 'counts', 'description': 'flag count for processing flag bit {}'.format(bit)}
        #     name = 'processing_flag_count_{}'.format(str(bit).zfill(2))
        #     features.append(Feature(['Categories', name], flags[bit], attributes=attributes))

        # stash file
        self.stash(features, destination)

        # stash sahara study
        features = []
        features.append(Feature(['Categories', 'saharan_ocp'], numpy.array(sahara[0])))
        features.append(Feature(['IndependentVariables', 'saharan_aerosol_index'], numpy.array(sahara[1])))
        destination = '{}/{}/OMCLDRR_Saharan_Study.h5'.format(self.sink, sink)
        self.stash(features, destination)

        return None

    def align(self, year, month, day, strategy, source='months', sink='differences'):
        """Align a climatology with cldrr measurements via linear interpolation.

        Arguments:
            year: int, year
            month: int, month
            day: int, day
            strategy: list of tuple of ints, the reflectivity binning strategy
            source: str, source folder for climatology
            sink: str, sink folder for differences

        Returns:
            None
        """

        # create sink directory
        self._make('{}/{}'.format(self.sink, sink))

        # create hydra for climatology
        climatology = Hydra('{}/{}'.format(self.sink, source))
        appropriate = [path for path in climatology.paths if '{}_{}'.format(year, str(month).zfill(2)) in path][0]
        climatology.ingest(appropriate)

        # create hydra for cloudrr
        formats = (year, str(month).zfill(2), str(day).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}/{}'.format(*formats))

        # collect originals, interpolations, differences
        originals = []
        interpolations = []
        differences = []

        # for each path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # extract the data
            latitudes = hydra.dig('Latitude')[0].distil()
            longitudes = hydra.dig('Longitude')[0].distil()
            reflectivities = hydra.dig('Reflectivity')[0].distil()
            clouds = hydra.dig('CloudPressureforO3')[0].distil()

            # limit data to every fifth timepoint
            times = numpy.array([index for index in range(clouds.shape[0]) if index % 5 == 0])
            latitudes = latitudes[times, :]
            longitudes = longitudes[times, :]
            reflectivities = reflectivities[times, :]
            clouds = clouds[times, :]

            # for each bracket in the reflectivity strategy
            for bracket in strategy:

                # parse into low and high reflectivities
                low = bracket[0] / 10
                high = bracket[1] / 10

                # get the climatology pressure
                pressures = climatology.dig('ocp_average_{}-{}_reflectivity'.format(low, high))[0].distil()

                # create mask for reflectivity range
                mask = (reflectivities >= low) & (reflectivities <= high)

                # apply mask to data
                cloud = clouds[mask].flatten()
                latitude = latitudes[mask].flatten()
                longitude = longitudes[mask].flatten()

                # round latitude down and up
                south = numpy.floor(latitude).astype(int) + 90
                north = numpy.ceil(latitude).astype(int) + 90
                north = numpy.where(north < 180, north, 179)

                # repeat for longitude
                west = numpy.floor(longitude).astype(int) + 180
                east = numpy.ceil(longitude).astype(int) + 180
                east = numpy.where(east < 360, east, 0)

                # create pressures at endpoints
                southwest = pressures[south, west].flatten()
                southeast = pressures[south, east].flatten()
                northwest = pressures[north, west].flatten()
                northeast = pressures[north, east].flatten()

                # calculate rise for east west interpolations
                rise = southeast - southwest
                riseii = northeast - northwest

                # run is 1 degree
                run = 1.0

                # calculate the fractional longitudinal displacement
                fraction = longitude - numpy.floor(longitude)

                # apply linear interpolation formula: y = y0 + (x - x0) * (y1 - y0) / (x1 - x0)
                interpolation = southwest + fraction * rise / run
                interpolationii = northwest + fraction * riseii / run

                # calculate new rise between interpolated values
                rise = interpolationii - interpolation
                fraction = latitude - numpy.floor(latitude)

                # calculate interpolated pressures
                cloudii = interpolation + fraction * rise / run

                # calculate the difference
                difference = cloud - cloudii

                # stack originals, interpolations, and differenxces
                originals.append(numpy.vstack([cloud, latitude, longitude]))
                interpolations.append(numpy.vstack([cloudii, latitude, longitude]))
                differences.append(numpy.vstack([difference, latitude, longitude]))

        # create features
        features = []

        # create heat map for originals
        name = 'heatmap_original_ocp'
        array = numpy.hstack(originals)
        bounds = [200 * index for index in range(9)]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # create heat map for interpolations
        name = 'heatmap_climatology_ocp'
        array = numpy.hstack(interpolations)
        bounds = [200 * index for index in range(9)]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # create heat map for differences
        name = 'heatmap_differences_ocp'
        array = numpy.hstack(differences)
        bounds = [200 * index -600 for index in range(7)]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # create desination
        formats = (self.sink, sink, year, str(month).zfill(2), str(day).zfill(2))
        destination = '{}/{}/OMCLDRR_Climatology_Comparison_{}_{}_{}.h5'.format(*formats)
        self.stash(features, destination, 'Data')

        return None

    def churn(self, source, sink, template, name, plans):
        """Churn out information for making monthly plots.

        Arguments:
            source: str, source directory for climatology plots
            sink: str, sink directory for climatology plots
            template: str, title template with $M for month and $R for reflectivity
            name: str, parameter name
            plan: list of tuples, beginning index, ending index, stub
            stub: str, slide stub

        Returns:
            tuple of variant attributes
        """

        # create months
        months = ['January', 'February', 'March', 'April', 'May', 'June']
        months += ['July', 'August', 'September', 'October', 'November', 'December']

        # get file paths from source directory
        paths = self._see(source)
        paths.sort()

        # begin variant seeds
        seeds = []

        # create variants information
        for first, last, stub in plans:

            # begin accumulation
            titles = []
            indices = []

            # for each index
            for index in range(first, last):

                # add to indices
                indices.append(index)

                # get path
                path = paths[index]

                # find reflectivity
                reflectivity = re.search('reflectivity_[0-9]{2}_[0-9]{2}', path).group()
                low = int(reflectivity.split('_')[1]) / 10
                high = int(reflectivity.split('_')[2]) / 10

                # find month
                month = re.search('m[0-9]{2}', path).group()
                month = int(month.strip('m')) - 1

                # create title
                title = template.replace('$M', months[month]).replace('$R', '{} to {}'.format(low, high))
                titles.append(title)

            # craete variant
            seed = (titles, name, stub, indices, sink)
            seeds.append(seed)

        return seeds

    def climb(self, year=2005):
        """Create scene pressure climatology file.

        Arguments:
            year: int, the year

        Returns:
            None
        """

        # make climatology directory
        self._make('{}/climatology'.format(self.sink))

        # create hydra
        hydra = Hydra('{}/scenes'.format(self.sink))

        # get all paths matching year
        paths = [path for path in hydra.paths if '{}_'.format(year) in path]
        paths.sort()

        # begin list of arrays
        averages = []
        deviations = []
        counts = []
        months = []

        # create other dimensions
        latitudes = numpy.array([89.5 - index for index in range(180)])
        longitudes = numpy.array([-179.5 + index for index in range(360)])
        reflectivities = numpy.array([0.05 + index / 10 for index in range(11)])

        # for each path
        for path in paths:

            # ingest
            hydra.ingest(path)

            # get month
            month = int(re.search('[0-9]{4}_[0-9]{2}', path).group()[-2:])
            months.append(month)

            # grab the data, summing acrpss rows
            scene = hydra.grab('scp_sum').sum(axis=1)
            count = hydra.grab('scp_sample_count').sum(axis=1)
            square = hydra.grab('scp_sum_of_squares').sum(axis=1)

            # compute average and stdev
            average = scene / count
            variance = (square / count) - (average ** 2)
            deviation = numpy.sqrt(variance)

            # replace nans with fill values
            fill = -9999
            average = numpy.where(numpy.isfinite(average), average, fill)
            deviation = numpy.where(numpy.isfinite(deviation), deviation, fill)

            # force deviations with 1 sample to 0
            deviation = numpy.where((average > 0) & (deviation < 0), 0, deviation)

            # add to arrays
            averages.append(average)
            deviations.append(deviation)
            counts.append(count)

        # create arrays
        months = numpy.array(months)
        averages = numpy.array(averages)
        deviations = numpy.array(deviations)
        counts = numpy.array(counts)

        # create data object
        data = {'scene_pressure': averages, 'sample_counts': counts, 'standard_deviations': deviations}
        data.update({'month': months, 'reflectivity': reflectivities, 'latitude': latitudes, 'longitude': longitudes})

        # create destination
        destination = '{}/climatology/OMCLDRR_Scene_Pressure_Climatology_{}.h5'.format(self.sink, year)

        # stash
        self.spawn(destination, data)

        return None

    def dimerize(self, year, month, start, stop, sink='accumulations'):
        """Accumulate cloud data from the O2 dimer cloud product.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))

        # create destination of file
        destination = '{}/{}/OMCDO2N_Climatology_{}_{}.h5'.format(self.sink, sink, year, self._pad(month))

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date
            squares = hydra.dig('ocp_sum_of_squares')[0].distil()
            totals = hydra.dig('ocp_sum')[0].distil()
            counts = hydra.dig('ocp_sample_count')[0].distil()
            orbits = hydra.dig('orbit_number')[0].distil()

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squares = numpy.zeros((11, 60, 180, 360)).astype('float32')
            totals = numpy.zeros((11, 60, 180, 360)).astype('float32')
            counts = numpy.zeros((11, 60, 180, 360)).astype('int')
            orbits = numpy.array([])

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/10003/OMCDO2N/{}/{}'.format(*formats), start, stop)

        # create hydra for OMCLDRR to grab reflectivtity
        hydraii = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths:

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # skip if orbit already made
            if orbit not in orbits:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # ingest the path
                hydra.ingest(path)

                # find associated OMCLDRR path
                pathsii = [member for member in hydraii.paths if int(self._stage(member)['orbit']) == orbit]
                pathii = [member for member in pathsii if member.endswith('.he5')][0]

                # ingest path and grab reflecgtivity data
                hydraii.ingest(pathii)

                # get OMCLDRR times and OMTO3 times
                times = hydra.dig('Time')[0].distil()
                timesii = hydraii.dig('Time')[0].distil()

                # determine alignment
                start, stop, startii, stopii = self._align(times, timesii)

                # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
                quality = hydra.dig('ProcessingQualityFlags')[0].distil().flatten()

                # get other data
                clouds = hydra.dig('CloudPressure')[0].distil()[start: stop].flatten()
                latitudes = hydra.dig('Latitude')[0].distil()[start: stop].flatten()
                longitudes = hydra.dig('Longitude')[0].distil()[start: stop].flatten()
                rows = numpy.array([list(range(60)) for _ in range(int(len(clouds) / 60))]).flatten()

                # grab reflectivitiew
                reflectivities = hydraii.dig('Reflectivity')[0].distil()[startii: stopii].flatten()

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # round reflectivity
                reflectivities = numpy.floor(reflectivities * 11)
                reflectivities = numpy.where(reflectivities > 10, 10, reflectivities)
                reflectivities = numpy.where(reflectivities < 0, 0, reflectivities)

                # try to
                try:

                    # create stack
                    stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
                    stack = stack.transpose(1, 0)

                    # exclude particular flags
                    bits = [0, 1, 2, 3, 4, 5, 7, 13, 14, 15]
                    masque = self._exclude(quality, bits)

                    # print exclusion message
                    formats = (masque.sum(), len(quality))
                    self._print('kept {} out of {} points without designated quality issues'.format(*formats))

                    # create final mask
                    mask = masque

                    # apply to stack and clouds
                    clouds = clouds[mask]
                    stack = stack[mask]

                    # for each row
                    for strip, cloud in zip(stack, clouds):

                        # verify no fill
                        if cloud > 0 and numpy.isfinite(cloud):

                            # unpack data
                            reflectivity, row, latitude, longitude = strip

                            # update
                            totals[reflectivity, row, latitude, longitude] += cloud
                            squares[reflectivity, row, latitude, longitude] += cloud ** 2
                            counts[reflectivity, row, latitude, longitude] += 1

                    # append(orbits)
                    orbits = numpy.array([entry for entry in orbits] + [orbit])
                    orbits.sort()

                    # stamp
                    self._stamp('processed {}.'.format(orbit))

                # unless mismatch
                except (ValueError, IndexError):

                    # stamp
                    self._stamp('error in orbit {}, skipped!'.format(orbit))

        # construct features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum_of_squares'], squares, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sum'], totals,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of ocp measurements, refl x row x lat x lon'}
        features.append(Feature(['ocp_sample_count'], counts,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['orbit_number'], orbits, attributes=attributes))

        # # add flag counts
        # for bit in range(16):
        #
        #     # create feature from flag count
        #     attributes = {'units': 'counts', 'description': 'flag count for processing flag bit {}'.format(bit)}
        #     name = 'processing_flag_count_{}'.format(str(bit).zfill(2))
        #     features.append(Feature(['Categories', name], flags[bit], attributes=attributes))

        # stash file
        self.stash(features, destination, compression=9)

        return None

    def examine(self, year, month, start, stop, reflectivity, crosstrack, coordinates, sink='histograms'):
        """Examine the histograms of particular grid cells.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            reflectivities: list of tuples, reflectivity brackets
            rows: list of tuples, row brackets
            coordinates: list of latitude, longitude coordinates for inspection
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))

        # create destination of file
        destination = '{}/{}/OMCLDRR_Histograms_{}_{}.h5'.format(self.sink, sink, year, month)

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # begin histograms
        histograms = {}

        # for each path
        for path in hydra.paths:

            # ingest the path
            hydra.ingest(path)

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # stamp
            self._stamp('processing orbit {}...'.format(orbit), initial=True)

            # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
            flags = [0, 1, 2, 3, 4, 6, 7, 13, 14, 15]
            flags += [5]
            quality = hydra.dig('ProcessingQualityFlagsforO3')[0].distil().flatten()
            mask = self._exclude(quality, flags)

            # print exclusio message
            formats = (mask.sum(), len(quality))
            self._print('kept {} out of {} points without designated quality issues'.format(*formats))

            # get other data
            clouds = hydra.dig('CloudPressureforO3')[0].distil().flatten()[mask]
            latitudes = hydra.dig('Latitude')[0].distil().flatten()[mask]
            longitudes = hydra.dig('Longitude')[0].distil().flatten()[mask]
            reflectivities = hydra.dig('Reflectivity')[0].distil().flatten()[mask]
            rows = numpy.array([list(range(60)) for _ in range(int(len(mask) / 60))]).flatten()[mask]

            # round latitudes to integer indices, and flip to number from north to south
            latitudes = numpy.floor(latitudes + 0.5) + 90
            latitudes = numpy.where(latitudes > 179, 179, latitudes)
            latitudes = 179 - latitudes

            # round longitude and correct longitude for boundary
            longitudes = numpy.floor(longitudes + 0.5) + 180
            longitudes = numpy.where(longitudes > 359, 0, longitudes)

            # round reflectivity
            reflectivities = numpy.floor(reflectivities * 10)
            reflectivities = numpy.where(reflectivities > 9, 9, reflectivities)

            # create stack
            stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
            stack = stack.transpose(1, 0)

            # go through reflectivity bins
            for low, high in reflectivity:

                # and row bins
                for first, last in crosstrack:

                    # and latitude longitude
                    for latitude, longitude in coordinates:

                        # begin masks
                        masks = []

                        # create mask to remove negative cloud pressures
                        mask = clouds > 0
                        masks.append(mask)

                        print('clouds: {}, {}'.format(mask.shape, mask.sum()))

                        # create mask for reflectivity
                        mask = (stack[:, 0] >= low) & (stack[:, 0] <= high)
                        masks.append(mask)

                        print('reflectivity: {}, {}'.format(mask.shape, mask.sum()))

                        # create mask for rows
                        mask = (stack[:, 1] >= first) & (stack[:, 1] < last)
                        masks.append(mask)

                        print('row: {}, {}'.format(mask.shape, mask.sum()))

                        # create mask for latitude
                        cell = 179 - (latitude + 90)
                        mask = (stack[:, 2] == cell)
                        masks.append(mask)

                        print('latitude: {}, {}'.format(mask.shape, mask.sum()))

                        # create mask for longitude
                        cell = longitude + 360
                        mask = (stack[:, 3] == cell)
                        masks.append(mask)

                        print('longitude: {}, {}'.format(mask.shape, mask.sum()))

                        # construct masque
                        masque = numpy.array(masks).prod(axis=0).astype(bool)
                        pressures = clouds[masque]

                        print('total: {}, {}'.format(masque.shape, masque.sum()))

                        # add to histograms
                        identifier = (latitude, longitude, low, high, first, last)
                        accumulation = histograms.setdefault(identifier, [])
                        accumulation += pressures.tolist()

        print('stack:')
        print(stack.shape)

        # begin features
        features = []
        for identifier, pressures in histograms.items():

            # create histogram
            name = 'histogram_{}_{}_{}_{}_{}_{}'.format(*identifier)
            array = numpy.array(pressures)
            feature = Feature(['Categories', name], array)
            features.append(feature)

        # stash file
        self.stash(features, destination)

        for name, array in histograms.items():
            print(' ')
            print(name)
            print(array)

        return None

    def fractionate(self, year, month, start, stop, bounds):
        """Create plots of cloud fraction versus reflectivity for a sample set.

        Arguments:
            year: int, year
            month: int, month
            start: int, beginning day
            stop: int, ending day
            bounds: tuple of float, latitude bounds

        Returns:
            None
        """

        # create folders
        self._make('{}/laterals'.format(self.sink))

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # begin collections
        fractions = []
        reflectivities = []
        surfaces = []
        radiations = []

        # for eadh path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # grab reflectivity and latitude
            reflectivity = hydra.dig('Reflectivity')[0].distil()
            fraction = hydra.dig('CloudFractionforO3')[0].distil()
            radiation = hydra.dig('RadiativeCloudFraction')[0].distil()
            surface = hydra.dig('SurfaceReflectivity')[0].distil()
            latitude = hydra.dig('Latitude')[0].distil()

            # create mask for reflectivity limit
            mask = (latitude >= bounds[0]) & (latitude <= bounds[1])

            # apply to latitudes
            fractions += fraction[mask].tolist()
            radiations += radiation[mask].tolist()
            surfaces += surface[mask].tolist()
            reflectivities += reflectivity[mask].tolist()

        # make cloud fraction plot
        features = []
        name = 'cloud_fractions'
        array = numpy.array(fractions)
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # make radiative cloud fraction
        name = 'radiative_cloud_fractions'
        array = numpy.array(radiations)
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # make surface reflectivity
        name = 'surface_reflectivity'
        array = numpy.array(radiations)
        feature = Feature(['Categories', name], array)
        features.append(feature)

        # add reflectivity as independent
        name = 'reflectivity'
        array = numpy.array(reflectivities)
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # make destination
        formats = (self.sink, year, str(month).zfill(2), *bounds)
        destination = '{}/laterals/Scatter_PLots_Reflectivity_{}_{}_{}_{}.h5'.format(*formats)
        self.stash(features, destination)

        return None

    def historize(self, year, month, start, stop, bounds, reflectivity):
        """Create histogram plot of sample counts versus latitude for a particular reflectivity cutoff.

        Arguments:
            year: int, year
            month: int, month
            start: int, beginning day
            stop: int, ending day
            bounds: tuple of float, latitude bounds

        Returns:
            None
        """

        # create folders
        self._make('{}/laterals'.format(self.sink))

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # begin latitudes
        pressures = []

        # for eadh path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # grab reflectivity and latitude
            reflectivities = hydra.dig('Reflectivity')[0].distil()
            latitude = hydra.dig('Latitude')[0].distil()
            pressure = hydra.dig('CloudPressureforO3')[0].distil()

            # create mask for reflectivity limit
            mask = reflectivities > reflectivity
            maskii = (latitude >= bounds[0]) & (latitude <= bounds[1])
            maskiii = pressure > 0
            maskiv = numpy.isfinite(pressure)
            masque = (mask * maskii * maskiii* maskiv).astype(bool)

            # apply to latitudes
            pressures += pressure[masque].tolist()

        # make histogram
        formats = (reflectivity, year, str(month).zfill(2), *bounds)
        name = 'histogram_>{}_{}_{}_{}_{}'.format(*formats)
        array = numpy.array(pressures)
        feature = Feature(['Categories', name], array)

        # make destination
        destination = '{}/laterals/Histograms_by_Latitude_Reflectivity_{}_{}_{}_{}_{}.h5'.format(self.sink, *formats)
        self.stash([feature], destination)

        return None

    def imagine(self, date, reflectivity=(0.6, 0.7), norths=(0, 30), easts=(10, 30), sink='aerosols'):
        """Create the h5 file for viewing in boquette.

        Arguments:
            date: str, date in 'yyyy-mm-dd' format
            reflectivity: reflectivity range
            sink: str, folder for deposit
            norths: tuple, latitude bounds
            easts: tuple. longitufde bounds

        Returns:
            None
        """

        # make sink folder
        self._make(sink)

        # split date
        year, month, day = date.split('-')

        # create hydra
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(year, month), int(day), int(day))

        # for each sensor
        for path in hydra.paths:

            # ingest path
            hydra.ingest(path)

            # dig up attributes
            pressures = hydra.dig('CloudPressureforO3')[0].distil()
            latitudes = hydra.dig('Latitude')[0].distil()
            longitudes = hydra.dig('Longitude')[0].distil()
            reflectivities = hydra.dig('Reflectivity')[0].distil()

            # create mask
            mask = (latitudes > norths[0]) & (latitudes < norths[1])
            maskii = (longitudes > easts[0]) & (longitudes < easts[1])
            maskiii = (reflectivities > reflectivity[0]) & (reflectivities < reflectivity[1])
            masque = numpy.logical_and(mask, maskii)
            masque = numpy.logical_and(masque, maskiii)

            print(masque.sum())

            # check for valid entries
            if masque.sum() > 20:

                # make sum
                number = masque.sum()

                # # determine rows within latitude
                # sahara = [index for index, row in enumerate(latitudes) if norths[0] < row[30] < norths[1]]
                # sahara = numpy.array(sahara).astype(int)

                # take saharan subset
                pressures = pressures[masque]
                latitudes = latitudes[masque]
                longitudes = longitudes[masque]

                # subset and concatenate into block
                tensors = (pressures, latitudes, longitudes)
                block = [[tensor.flatten()] for tensor in tensors]
                block = numpy.concatenate(block, axis=0)

                # begin attributes
                attributes = {'polygons': [], 'values': []}

                # # try:
                # try:
                #
                #     # go through each track
                #     for track in range(pressures.shape[0] - 20, pressures.shape[0] - 10):
                #
                #         # and each row
                #         for row in range(10, 50):
                #
                #             # get latitude, longitude, and pressuree at point
                #             latitude = latitudes[track][row]
                #             longitude = longitudes[track][row]
                #             pressure = pressures[track][row]
                #
                #             # add pressure to values
                #             attributes['values'].append(pressure)
                #
                #             # retrieve points at neighboring diagonal corners
                #             offsets = [(1, -1), (1, 1), (-1, 1), (-1, -1)]
                #             corners = []
                #             for first, second in offsets:
                #
                #                 # make corner and append
                #                 corner = (latitudes[track + first][row + second], longitudes[track + first][row + second])
                #                 corners.append(corner)
                #
                #             # append half the distances to polygon
                #             polygon = [((corner[0] + latitude) / 2, (corner[1] + longitude) / 2) for corner in corners]
                #             attributes['polygons'].append(polygon)

                # set bounds
                bounds = [300, 400, 500, 600, 700, 800]
                brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                attributes['brackets'] = brackets
                attributes['labels'] = labels
                attributes['units'] = 'hPa'
                #attributes.update({'brackets': brackets, 'labels': labels, 'units': 'hPa'})

                # create feature
                name = 'heatmap_cloud_pressure_omi_sahara_{}m{}{}'.format(year, month, day)
                array = block
                feature = Feature(['Categories', name], array, attributes=attributes)

                # stash file
                destination = '{}/{}/Cloud_Pressures_Sahara_{}m{}{}_{}.h5'.format(self.sink, sink, year, month, day, number)
                hydra._stash([feature], destination, 'Data')

                # # umless not enough
                # except IndexError:
                #
                #     # pass
                #     pass

        return None

    def investigate(self, month='03'):
        """Investigate artifact in northern hemisphere, march.

        Arguments:
            month: str, month str

        Returns:
            None
        """

        # make folder
        self._make('{}/investigation'.format(self.sink))

        # grab omclcrr data
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/2005/{}/21'.format(month))

        # collect rows, longitude, latitude
        rows = []
        latitudes = []
        longitudes = []

        # set up monthly brackets
        brackets = {'03': (125, 600, 1525)}
        brackets.update({'06': (50, 500, 1350)})
        brackets.update({'09': (125, 600, 1525)})
        brackets.update({'12': (300, 700, 1700)})

        # get all paths
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths[3:5]:

            # ingest and grab longitudes
            hydra.ingest(path)

            # get longitudes
            longitude = hydra.dig('Longitude')[0].distil()
            latitude = hydra.dig('Latitude')[0].distil()

            # get indices -200 to -400
            bracket = brackets[month]
            for index in range(bracket[0], bracket[1]):

                # add rows
                rows += list(range(3, 20))
                latitudes += latitude[index][3:20].tolist()
                longitudes += longitude[index][3:20].tolist()

            # get indices -200 to -400
            for index in range(1000, min([latitude.shape[0], bracket[2]])):

                # add rows
                rows += list(range(3, 20))
                latitudes += latitude[index][3:20].tolist()
                longitudes += longitude[index][3:20].tolist()

        # create heatmap
        name = 'heatmap_artifact'
        array = numpy.vstack([rows, latitudes, longitudes])
        bounds = [4, 8, 12, 16, 20]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['Row {} to {}'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels, 'units': 'row'}
        feature = Feature(['Categories', name], array, attributes=attributes)

        # dump
        destination = '{}/investigation/Artifact_Investigate_{}.h5'.format(self.sink, month)
        self.stash([feature], destination)

        return None

    def lateralize(self, year, month, start, stop, reflectivity):
        """Create histogram plot of sample counts versus latitude for a particular reflectivity cutoff.

        Arguments:
            year: int, year
            month: int, month
            start: int, beginning day
            stop: int, ending day

        Returns:
            None
        """

        # create folders
        self._make('{}/laterals'.format(self.sink))

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # begin latitudes
        latitudes = []

        # for eadh path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # grab reflectivity and latitude
            reflectivities = hydra.dig('Reflectivity')[0].distil()
            latitude = hydra.dig('Latitude')[0].distil()

            # create mask for reflectivity limit
            mask = reflectivities > reflectivity

            # apply to latitudes
            latitudes += latitude[mask].tolist()

        # make histogram
        formats = (int(reflectivity / 20), year, str(month).zfill(2), str(start).zfill(2), str(stop).zfill(2))
        name = 'histogram_>{}_{}_{}_{}_{}'.format(*formats)
        array = numpy.array(latitudes)
        feature = Feature(['Categories', name], array)

        # make destination
        destination = '{}/laterals/Histograms_by_Latitude_Reflectivity_{}_{}_{}_{}_{}.h5'.format(self.sink, *formats)
        self.stash([feature], destination)

        return None

    def mask(self):
        """Generate land sea mask.

        Arguments:
            None

        Returns:
            None
        """

        # create folder
        self._make('{}/mask'.format(self.sink))

        # create latitude/ longitude grid
        latitudes = numpy.array([[89.5 - index for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[index - 179.5 for index in range(360)] for _ in range(180)])

        # generate mask
        mask = global_land_mask.globe.is_land(latitudes, longitudes).astype(int)

        # create feature
        features = []
        destination = '{}/mask/Land_Sea_Mask.h5'.format(self.sink)
        feature = Feature(['Categories', 'land_sea_mask'], mask)
        features.append(feature)

        # create heatmap
        name = 'heatmap_land_sea_mask'
        array = numpy.vstack([mask.flatten(), latitudes.flatten(), longitudes.flatten()])
        bounds = [0, 0.5, 1]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
        feature = Feature(['Categories', name], array, attributes=attributes)
        features.append(feature)

        # stash
        self.stash(features, destination)

        return None

    def meditate(self):
        """Generate csvs of metadata fields.

        Arguments:
            None

        Returns:
            None
        """

        # get data
        hydra = Hydra('../studies/cumulo/metadata/data')

        # ingest collection 3
        hydra.ingest('v003')

        # grab the core and archived metadata
        core = str(hydra.grab('CoreMetadata'))
        archive = str(hydra.grab('ArchiveMetadata'))

        # make headers
        headers = ('core', 'archive')
        reservoirs = (core, archive)

        # for each pair
        for reservoir, header in zip(reservoirs, headers):

            # split core into lines
            lines = reservoir.split('\\n')

            # default indicds to 0
            indices = [(len(lines), '')]

            # if header is core
            if header == 'core':

                # find the index of additional attributes
                indices = [(index, line) for index, line in enumerate(lines) if 'ADDITIONALATTRIBUTES' in line]

            # get standard set
            standard = lines[:indices[0][0]]
            stubs = ['GROUP', 'END_OBJECT', 'CONTAINER']
            stubsii = ['OBJECT', 'VALUE']
            standard = [line for line in standard if not any([stub in line for stub in stubs])]
            standard = [line for line in standard if any([stub in line for stub in stubsii])]

            # default addition to []
            addition = []

            # if the header is for core
            if header == 'core':

                # get additional set
                addition = lines[indices[0][0]:]
                addition = [line for line in addition if 'VALUE' in line and 'PARAMETERVALUE' not in line]

            # combine and pari up
            combination = standard + addition
            pairs = [pair for index, pair in enumerate(zip(combination[:-1], combination[1:])) if index % 2 == 0]

            # construct rows
            rows = [[pair[0].split('=')[-1].strip(), pair[1].split('=')[-1].strip()] for pair in pairs]

            # dump into csv
            destination = '../studies/cumulo/metadata/table/omcldrr_metadata_{}.csv'.format(header)
            self._table(rows, destination)

        return None

    def parse(self):
        """Parse the data into degree by degree sections.

        Arguments:
            None

        Returns:
            None
        """

        # open up accumulations
        accumulation = '../studies/cumulo/accumulations/clouds.json'
        accumulations = self._load(accumulation)

        # create degrees grid
        grid = {}
        for path, datum in list(accumulations['clouds'].items()):

            # print
            self._print(path)

            # unpack data
            cloud = numpy.array(datum['cloud']).flatten()
            latitude = numpy.array(datum['latitude']).flatten()
            longitude = numpy.array(datum['longitude']).flatten()
            reflectivity = numpy.array(datum['reflectivity']).flatten()
            fraction = numpy.array(datum['fraction']).flatten()
            surface = numpy.array(datum['surface']).flatten()
            rows = numpy.array([list(range(60))] * int(cloud.shape[0] / 60)).flatten()

            # stack all together
            stack = numpy.vstack([cloud, latitude, longitude, reflectivity, fraction, surface, rows]).transpose(1, 0)

            # print
            self._print(stack.shape)

            # remove any with fill values
            mask = stack[:, 0] > 0
            stack = stack[mask]

            # print
            self._print(stack.shape)

            # for each row
            for row in stack:

                # get latitude, longitude
                latitude = math.floor(row[1])
                longitude = math.floor(row[2])

                # make entry in grid
                reservoir = grid.setdefault(str((latitude, longitude)), [])
                reservoir.append(row.tolist())

        # dump into json
        grids = '../studies/cumulo/grids/grid.json'
        self._dump(grid, grids)

        return None

    def press(self, year, month, resolution=50):
        """Plot scene pressure climatology.

        Arguments:
            year: int, the year
            month: int, the month of interest
            resolution: int, resolution

        Returns:
            None
        """

        # make plots directory
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/maps'.format(self.sink))

        # create hydra
        hydra = Hydra('{}/scenes'.format(self.sink))

        # ingest path matching year and month
        hydra.ingest('{}_{}.h5'.format(year, self._pad(month)))

        # grab the data, summing acrpss rows
        scene = hydra.grab('scp_sum').sum(axis=1)
        count = hydra.grab('scp_sample_count').sum(axis=1)
        square = hydra.grab('scp_sum_of_squares').sum(axis=1)

        # compute average and stdev
        average = scene / count
        variance = (square / count) - (average ** 2)
        deviation = numpy.sqrt(variance)
        error = deviation / (numpy.sqrt(count))

        # create latitudes and longitudes
        latitude = numpy.array([[89.5 - index for index in range(180)]] * 360).transpose(1, 0)
        longitude = numpy.array([[-179.5 + index for index in range(360)]] * 180)

        # set gradient and selection
        selection = range(256)

        # set plot attributes
        limits = (-90, 90, -180, 180)
        size = (14, 5)
        back = 'white'
        extension = 'max'

        # set reflectivity brackets
        reflectivities = {index: '{} to {} refl'.format(index / 10, (index + 1) / 10) for index in range(10)}
        reflectivities.update({10: '> 1.0 refl'})

        # set gradiants
        gradients = {'scene': 'viridis', 'deviation': 'plasma', 'count': 'plasma_r', 'error': 'plasma'}

        # set scales
        scales = {'scene': (500, 1100), 'deviation': (0, 100), 'count': (1, 51), 'error': (0, 50)}

        # set units
        units = {'scene': 'hPa', 'deviation': 'hPa', 'count': '-', 'error': 'hPa'}

        # set stubs
        stubs = {'scene': 'Average Scene Pressure', 'deviation': 'Standard Deviation'}
        stubs.update({'count': 'Number of Samples', 'error': 'Standard Error of Mean'})

        # set extensions
        extensions = {'scene': 'both', 'deviation': 'max', 'count': 'max', 'error': 'max'}

        # replace with fill values, and chop upward end
        fill = -9999
        average = numpy.where(numpy.isfinite(average), average, fill)
        deviation = numpy.where(numpy.isfinite(deviation), deviation, fill)
        error = numpy.where(numpy.isfinite(error), error, fill)
        count = numpy.where(count > 0, count, fill)
        average = numpy.where(average > scales['scene'][1], scales['scene'][1], average)
        average = numpy.where((average < scales['scene'][0]) & (average > 0), scales['scene'][0], average)
        deviation = numpy.where(deviation > scales['deviation'][1], scales['deviation'][1], deviation)
        error = numpy.where(error > scales['error'][1], scales['error'][1], error)
        count = numpy.where(count > scales['count'][1], scales['count'][1], count)

        # set traceers
        tracers = {'scene': average, 'deviation': deviation, 'count': count, 'error': error}

        # for each reflectivity
        for reflectivity in range(11):

            # for each tracer
            for name, tracer in tracers.items():

                # set up texts
                stub = stubs[name]
                title = 'OMCLDRR, {}\n{}m{}, {}'.format(stub, year, self._pad(month), reflectivities[reflectivity])
                formats = (self.sink, name, year, self._pad(month), self._pad(reflectivity))
                destination = '{}/plots/maps/Scene_Pressure_{}_{}m{}_{}.png'.format(*formats)

                # plot
                self._stamp('plotting {}...'.format(destination), initial=True)
                parameters = [[tracer[reflectivity], latitude, longitude], destination, [title, units[name]]]
                parameters += [scales[name], [gradients[name], selection], limits, resolution]
                options = {'size': size, 'log': False, 'back': back, 'north': False}
                options.update({'bar': True, 'extension': extensions[name]})
                self._paint(*parameters, **options)
                self._stamp('plotted.')

        return None

    def rain(self, strategy, scheme, source='squares', sink='rains', sinkii='months'):
        """Analyze the accumulations and create stdev, mean pressure maps.

        Arguments:
            strategy: list of tuples, reflectivity bins
            scheme: list of tuples, row bins
            source: str, source folder
            sink: str, sink folder
            sinkii: str, sink folder for monthly climatology

        Returns:
            None
        """

        # create sinks
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # grab all files
        hydra = Hydra('{}/{}'.format(self.sink, source))

        # for each one
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # extract data as 4-d arrays ( reflectivity x row x latitude x longitude
            data = {}
            data['squares'] = hydra.dig('ocp_sum_of_squares')[0].distil()
            data['totals'] = hydra.dig('ocp_sum')[0].distil()
            data['counts'] = hydra.dig('ocp_sample_count')[0].distil()

            # begin climatology features
            climatology = []

            # for each reflectivity bracket
            for low, high in strategy:

                # and each row bracket
                for first, last in scheme:

                    # begin features for heatmap files
                    features = []

                    # extract the data at these bins
                    def binning(array): return array[low: high, first: last, :, :].sum(axis=0).sum(axis=0)
                    extracts = {name: binning(array) for name, array in data.items()}

                    # unpack extracts and flatten
                    counts = extracts['counts']
                    totals = extracts['totals']
                    squares = extracts['squares']

                    # calculate averages
                    averages = totals / counts

                    # calculate variance as average of squares - square of average
                    variance = (squares / counts) - averages ** 2
                    deviations = numpy.sqrt(variance)

                    # create latitude coordinates, flipping north and south
                    latitudes = numpy.array([[179 - (index - 90) for index in range(180)] for _ in range(360)])
                    latitudes = latitudes.transpose(1, 0)
                    latitudes = latitudes.flatten()

                    # create longitude coordinates
                    longitudes = numpy.array([[index - 180 for index in range(360)] for _ in range(180)])
                    longitudes = longitudes.flatten()

                    # create sample counts
                    name = 'heatmap_sample_counts'
                    measure = counts.flatten()
                    mask = measure > 0
                    array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                    bounds = [0, 5, 25, 50, 100, 200, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} samples'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_average_pressure'
                    measure = averages.flatten()
                    mask = measure > 0
                    array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_stdevs'
                    measure = deviations.flatten()
                    mask = measure > 0
                    array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                    bounds = [0, 50, 100, 150, 200, 250, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # stash file
                    formats = [str(entry).zfill(2) for entry in (low, high, first, last)]
                    tag = '_rf_{}_{}_rw_{}_{}_heatmaps.h5'.format(*formats)
                    destination = path.replace(source, sink).replace('.h5', tag)
                    self.stash(features, destination, 'Data')

                    # create sample count feature
                    formats = ['{}-{}'.format(low / 10, high / 10)]
                    formats += ['{}-{}'.format(str(first).zfill(2), str(last).zfill(2))]
                    name = 'ocp_sample_count_{}_reflectivity_rows_{}'.format(*formats)
                    attributes = {'units': 'samples', 'description': 'sample counts, rows 11-50'}
                    feature = Feature(['Categories', name], counts, attributes=attributes)
                    climatology.append(feature)

                    # create sample pressure feature
                    name = 'ocp_average_{}_reflectivity_rows_{}'.format(*formats)
                    attributes = {'units': 'samples', 'description': 'average omcldrr pressure, rows 11-50'}
                    feature = Feature(['Categories', name], averages, attributes=attributes)
                    climatology.append(feature)

                    # create sample pressure feature
                    name = 'ocp_standard_deviations_{}_reflectivity_rows_{}'.format(*formats)
                    attributes = {'units': 'samples', 'description': 'average omcldrr pressure, rows 11-50'}
                    feature = Feature(['Categories', name], deviations, attributes=attributes)
                    climatology.append(feature)

            # add latitude grid
            latitudes = numpy.array([float(index - 90) for index in range(180)])
            feature = Feature(['IndependentVariables', 'latitude'], latitudes)
            climatology.append(feature)

            # add longitude grid
            longitudes = numpy.array([float(index - 180) for index in range(360)])
            feature = Feature(['IndependentVariables', 'longitude'], longitudes)
            climatology.append(feature)

            # stash climatology
            tag = '_row_reflectivity_binned.h5'
            destination = path.replace(source, sinkii).replace('.h5', tag)
            self.stash(climatology, destination, 'Data')

        return None

    def reflect(self, number=10):
        """Bin data by reflectivity bins.

        Arguments:
            number: int, number of reflectivity bins

        Returns:
            None
        """

        # get grid
        grids = '../studies/cumulo/grids/grid.json'
        grid = self._load(grids)

        # create bounds
        chunk = 1 / number
        bounds = [(index * chunk, chunk + index * chunk) for index in range(number)]

        # begin features
        features = []

        # for each bracket
        for index, bracket in enumerate(bounds):

            # print brackdet
            self._print(bracket)

            # set up reservoirs
            means = {}
            deviations = {}

            # for every grid point
            for point, data in list(grid.items()):

                # translate point
                coordinates = point.strip('(').strip(')').split(', ')
                point = (int(coordinates[0]), int(coordinates[1]))

                # determine mean pressure in reflectivity bins
                pressures = [row[0] for row in data if bracket[0] <= row[3] <= bracket[1]]
                mean = numpy.mean(pressures)
                deviation = numpy.std(pressures)

                # add to grid
                means[point] = mean
                deviations[point] = deviation

            # create heatmap data
            latitudes = numpy.array([point[0] for point in means.keys()])
            longitudes = numpy.array([point[1] for point in means.keys()])
            averages = numpy.array([datum for datum in means.values()])
            deviations = numpy.array([datum for datum in deviations.values()])

            # create means heatmap
            chunk = 200
            brackets = [(index * chunk, chunk + index * chunk) for index in range(8)]
            labels = ['{} to {}'.format(*bracket) for bracket in brackets]
            attributes = {'brackets': brackets, 'labels': labels}
            name = 'heatmap_means_reflectivity_bracket_{}_of_{}'.format(index, number)
            array = numpy.vstack([averages, latitudes, longitudes])
            feature = Feature(['Categories', name], array, attributes=attributes)
            features.append(feature)

            # create means heatmap
            chunk = 100
            brackets = [(index * chunk, chunk + index * chunk) for index in range(8)]
            labels = ['{} to {}'.format(*bracket) for bracket in brackets]
            attributes = {'brackets': brackets, 'labels': labels}
            name = 'heatmap_deviations_reflectivity_bracket_{}_of_{}'.format(index, number)
            array = numpy.vstack([deviations, latitudes, longitudes])
            feature = Feature(['Categories', name], array, attributes=attributes)
            features.append(feature)

        # stash file
        destination = '../studies/cumulo/reflections/Cloud_Pressure_Bins_{}.h5'.format(number)
        self.stash(features, destination, 'Data')

        return None

    def scat(self, year, month, bounds=None, boundsii=None):
        """Create scatter plots of scene pressure climatology.

        Arguments:
            year: int, year
            month, int, month
            bounds: tuple of floats, latitude bounds
            boundsii: tuple of floats, longitude bounds

        Returns:
            None
        """

        # create directories
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/scatter'.format(self.sink))

        # set default bounds
        bounds = bounds or (-90, 90)
        boundsii = boundsii or (-180, 180)

        # create hydra
        hydra = Hydra('{}/scenes'.format(self.sink))

        # ingest path matching year and month
        hydra.ingest('{}_{}.h5'.format(year, self._pad(month)))

        # grab the data
        scene = hydra.grab('scp_sum')
        count = hydra.grab('scp_sample_count')

        # set indices for latitude bounds, setting 0 as lowest bound
        north = max([89 - int(bounds[1]), 0])
        south = max([89 - int(bounds[0]), 0])
        west = max([179 + int(boundsii[0]), 0])
        east = max([179 + int(boundsii[1]), 0])

        # subset samples
        scene = scene[:, :, north: south, west: east]
        count = count[:, :, north: south, west: east]

        # sum across rows for now
        scene = scene.sum(axis=1)
        count = count.sum(axis=1)

        # create mean scene pressure
        mean = scene / count

        # create reflectivity abscissa, expanding based on scene pressure shape
        reflectivities = numpy.array([reflectivity / 10 for reflectivity in range(11)])
        reflectivities = numpy.array([[reflectivities] * mean.shape[1]] * mean.shape[2])
        reflectivities = reflectivities.transpose(2, 1, 0)

        # add smll random number to separate poinnts
        reflectivities = reflectivities + numpy.random.rand(*reflectivities.shape) * 0.004 + int(month) * 0.004

        # make scatter plot
        formats = (year, self._pad(month), self._orient(bounds[0]), self._orient(bounds[1]))
        address = 'plots/scatter/Scene_Pressure_{}m{}_{}_{}.h5'.format(*formats)
        title = 'OMCLDRR, AS10004 Mean scene pressure for {}m{}, {} to {}'.format(*formats)
        self.squid.ink('scene pressure', mean, 'reflectivity', reflectivities, address, title)

        return None

    def scatter(self, year, month, start, stop, bounds, boundsii):
        """Create histogram plot of sample counts versus latitude for a particular reflectivity cutoff.

        Arguments:
            year: int, year
            month: int, month
            start: int, beginning day
            stop: int, ending day
            bounds: tuple of float, latitude bounds
            boundsii: tuple of float, longitude bounds

        Returns:
            None
        """

        # create folders
        self._make('{}/scatters'.format(self.sink))

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/OMI/70004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # begin pressures
        pressures = []
        reflectivities = []
        longitudes = []
        qualities = []

        # for eadh path
        for path in hydra.paths:

            # ingest
            hydra.ingest(path)

            # grab reflectivity and latitude
            reflectivity = hydra.dig('Reflectivity')[0].distil()
            latitude = hydra.dig('Latitude')[0].distil()
            pressure = hydra.dig('CloudPressureforO3')[0].distil()
            longitude = hydra.dig('Longitude')[0].distil()
            quality = hydra.dig('ProcessingQualityFlagsforO3')[0].distil()

            # create mask for reflectivity limit
            mask = pressure > 0
            maskii = numpy.isfinite(pressure)
            maskiii = (latitude >= bounds[0]) & (latitude <= bounds[1])
            maskiv = (longitude >= boundsii[0]) & (longitude <= boundsii[1])
            masque = (mask * maskii * maskiii * maskiv).astype(bool)

            # apply to latitudes
            pressures += pressure[masque].tolist()
            reflectivities += reflectivity[masque].tolist()
            longitudes += longitude[masque].tolist()
            qualities += quality[masque].tolist()

        # make scatter plot
        formats = (year, str(month).zfill(2))
        name = 'heatmap_scp_{}_{}'.format(*formats)
        array = numpy.vstack([pressures, reflectivities, longitudes])
        bounds = [200 * index for index in range(9)]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
        feature = Feature(['Categories', name], array, attributes=attributes)

        # make scatter plot
        formats = (year, str(month).zfill(2))
        name = 'heatmap_processing_quality_{}_{}'.format(*formats)
        array = numpy.vstack([qualities, reflectivities, longitudes])
        bounds = [200 * index for index in range(9)]
        brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
        labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
        attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
        feature = Feature(['Categories', name], array, attributes=attributes)

        # make destination
        destination = '{}/scatters/Scatter_by_Longitude_Reflectivity_{}_{}.h5'.format(self.sink, *formats)
        self.stash([feature], destination)

        return None

    def scend(self, year, month, start, stop, sink='scenes', sinkii='scenes'):
        """Accumulate cloud data.

        Arguments:
            year: int, the year
            month: int, the month
            start: int, beginning day
            stop: int ending day
            sink: str, sink folder name

        Returns:
            None
        """

        # create folders
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))
        # self._make('{}/{}'.format(self.sink, sinkii))

        # create destination of file
        destination = '{}/{}/OMCLDRR_Scene_Climatology_{}_{}.h5'.format(self.sink, sinkii, year, self._pad(month))
        # destinationii = '{}/{}/OMCLDRR_Climatology_{}_{}.h5'.format(self.sink, sink, year, self._pad(month))

        # check for destination
        if destination in self._see('{}/{}'.format(self.sink, sink)):

            # load up file
            hydra = Hydra(destination)
            hydra.ingest(0)

            # extract date
            squares = hydra.dig('scp_sum_of_squares')[0].distil()
            totals = hydra.dig('scp_sum')[0].distil()
            counts = hydra.dig('scp_sample_count')[0].distil()
            orbits = hydra.dig('orbit_number')[0].distil()

        # otherwise
        else:

            # create anew, reflectivity bin x row x latitude x longitude
            squares = numpy.zeros((11, 60, 180, 360)).astype('float32')
            totals = numpy.zeros((11, 60, 180, 360)).astype('float32')
            counts = numpy.zeros((11, 60, 180, 360)).astype('int')
            orbits = numpy.array([])

        # # check for destination
        # if destinationii in self._see('{}/{}'.format(self.sink, sinkii)):
        #
        #     # load up file
        #     hydra = Hydra(destinationii)
        #     hydra.ingest(0)
        #
        #     # extract date
        #     squaresii = hydra.dig('scp_sum_of_squares')[0].distil()
        #     totalsii = hydra.dig('scp_sum')[0].distil()
        #     countsii = hydra.dig('scp_sample_count')[0].distil()
        #     orbitsii = hydra.dig('orbit_number')[0].distil()

        # # otherwise
        # else:
        #
        #     # create anew, reflectivity bin x row x latitude x longitude
        #     squaresii = numpy.zeros((11, 60, 180, 360)).astype('float32')
        #     totalsii = numpy.zeros((11, 60, 180, 360)).astype('float32')
        #     countsii = numpy.zeros((11, 60, 180, 360)).astype('int')
        #     orbitsii = numpy.array([])

        # create hydra
        formats = (year, str(month).zfill(2))
        hydra = Hydra('/tis/acps/OMI/10004/OMCLDRR/{}/{}'.format(*formats), start, stop)

        # create hydra for omanc
        hydraii = Hydra('/tis/acps/OMI/10004/OMUANC/{}/{}'.format(*formats), start, stop)

        # get corresponding total ozone directory for aerosol flags
        #hydraii = Hydra('/tis/OMI/10003/OMTO3/{}/{}'.format(*formats), start, stop)

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        pathsii = hydraii.paths
        for path, pathii in zip(paths, pathsii):

            # get orbit
            orbit = int(hydra._stage(path)['orbit'])

            # skip if orbit already made
            if orbit not in orbits:

                # stamp
                self._stamp('processing orbit {}...'.format(orbit), initial=True)

                # ingest the paths
                hydra.ingest(path)
                hydraii.ingest(pathii)

                # # find related total ozone path
                # aerosols = [member for member in hydraii.paths if member.endswith('.he5')]
                # aerosols = [member for member in aerosols if int(self._stage(member)['orbit']) == orbit][0]
                # hydraii.ingest(aerosols)

                # # get OMCLDRR times and OMTO3 times
                times = hydra.dig('Time')[0].distil()
                # timesii = hydraii.dig('Time')[0].distil()

                # # gather latitude data
                # latitudes = hydra.dig('Latitude')[0].distil()

                # # determine alignment
                # start, stop, startii, stopii = self._align(times, timesii, latitudes)
                start = 0
                stop = times.shape[0]

                # set processing flags for excluding data, based on OMCLDRR ReadMe recommendations, as well as snow/ice
                quality = hydra.grab('ProcessingQualityFlagsforO3')[start: stop].flatten()
                ground = hydra.grab('GroundPixelQualityFlags')[start: stop].flatten()
                zenith = hydra.grab('SolarZenithAngle')[start: stop].flatten()

                # get other data
                clouds = hydra.grab('CloudPressureforO3')[start: stop].flatten()
                fractions = hydra.grab('RadiativeCloudFraction')[start: stop].flatten()
                terrains = hydra.grab('TerrainPressure')[start: stop].flatten()
                latitudes = hydra.grab('Latitude')[start: stop].flatten()
                longitudes = hydra.grab('Longitude')[start: stop].flatten()
                reflectivities = hydra.grab('Reflectivity')[start: stop].flatten()
                rows = numpy.array([list(range(60)) for _ in range(int(len(clouds) / 60))]).flatten()

                # get omanc snow / ice data
                snow = hydraii.grab('snow_ice')[start: stop].flatten()

                # round latitudes to integer indices, and flip to number from north to south
                latitudes = 89 - numpy.floor(latitudes)
                latitudes = numpy.where(latitudes < 0, 0, latitudes)

                # round longitude and correct longitude for boundary
                longitudes = 180 + numpy.floor(longitudes)
                longitudes = numpy.where(longitudes > 359, 359, longitudes)

                # round reflectivity
                reflectivities = numpy.floor(reflectivities * 11)
                reflectivities = numpy.where(reflectivities > 10, 10, reflectivities)
                reflectivities = numpy.where(reflectivities < 0, 0, reflectivities)

                # calculate scene pressures
                scenes = terrains * (1 - fractions) + clouds * fractions

                # create stack
                stack = numpy.vstack([reflectivities, rows, latitudes, longitudes]).astype(int)
                stack = stack.transpose(1, 0)

                # # exclude particular flags
                # #bits = [0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15]
                # bits = [0, 1, 2, 3, 4, 5, 7, 13, 14, 15]
                # masque = self._exclude(quality, bits)
                #
                # # exclude descending flag, night side data
                # bits = [2, 3]
                # masqueii = self._exclude(ground, bits)

                # create masque
                # mask = numpy.logical_and(masque, masqueii)

                # # exclude same particular flags, except snow/ice
                # #bits = [0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15]
                # bits = [0, 1, 2, 3, 4, 7, 13, 14, 15]
                # unflagged = self._exclude(quality, bits)
                #
                # # exclude descending flag, night side data
                # bits = [2, 3]
                # ascending = self._exclude(ground, bits)
                #
                # # only use data from snow/ice
                # bits = [5]
                # ice = numpy.logical_not(self._exclude(quality, bits))

                # # create secondary mask for scene pressure
                # maskii = numpy.logical_and(unflagged, ascending)
                # maskii = numpy.logical_and(maskii, ice)

                # # set bogus mask
                # # mask = self._exclude(quality, [])
                # mask = numpy.ones(quality.shape).astype(bool)

                # set mask for snow and ice
                mask = (snow == 0) | (snow == 104)

                # set mask for descending data
                maskii = ((ground & 2) == 0)

                # set mask for solar zenith angle
                maskiii = (zenith <= 88)

                # set mask for only positive values and no vill
                maskiv = (scenes > 0) & (abs(scenes) < 1e15) & (numpy.isfinite(scenes))

                # make sure cloud fraction is legit
                maskv = (fractions >= 0) & (fractions <= 1)

                # make mask for terrain pressure
                maskvi = (terrains > 0) & (abs(terrains) < 1e15) & (numpy.isfinite(terrains))

                # assemble masque
                masque = mask & maskii & maskiii & maskiv & maskv & maskvi

                # print exclusion message
                formats = (masque.sum(), len(quality))
                self._print('kept {} out of {} points without designated quality issues'.format(*formats))

                # # dig up aerosol screen
                # aerosol = hydraii.dig('UVAerosolIndex')[0].distil()[startii: stopii].flatten()
                # screen = aerosol < 2

                # try to
                try:

                    # # create final mask
                    # #mask = numpy.logical_and(screen, masque)
                    # mask = masque

                    # apply to stack and scenes
                    # scenesii = scenes[maskii]
                    # stackii = stack[maskii]
                    scenes = scenes[masque]
                    stack = stack[masque]

                    # for each row
                    for strip, scene in zip(stack, scenes):

                        # verify no fill
                        if scene > 0 and numpy.isfinite(scene):

                            # unpack data
                            reflectivity, row, latitude, longitude = strip

                            # update
                            totals[reflectivity, row, latitude, longitude] += scene
                            squares[reflectivity, row, latitude, longitude] += scene ** 2
                            counts[reflectivity, row, latitude, longitude] += 1

                    # append(orbits)
                    orbits = numpy.array([entry for entry in orbits] + [orbit])
                    orbits.sort()

                    # # for each row
                    # for strip, cloud in zip(stackii, cloudsii):
                    #
                    #     # verify no fill
                    #     if cloud > 0 and numpy.isfinite(cloud):
                    #
                    #         # unpack data
                    #         reflectivity, row, latitude, longitude = strip
                    #
                    #         # update
                    #         totalsii[reflectivity, row, latitude, longitude] += cloud
                    #         squaresii[reflectivity, row, latitude, longitude] += cloud ** 2
                    #         countsii[reflectivity, row, latitude, longitude] += 1
                    #
                    # # append(orbitsii)
                    # orbitsii = numpy.array([entry for entry in orbitsii] + [orbit])
                    # orbitsii.sort()

                    # stamp
                    self._stamp('processed {}.'.format(orbit))

                # unless mismatch
                except ValueError:

                    # stamp
                    self._stamp('error in orbit {}, skipped!'.format(orbit))

        # construct features
        features = []

        # extract date
        attributes = {'units': 'hPa', 'description': 'sum of squared scp measurements, refl x row x lat x lon'}
        features.append(Feature(['scp_sum_of_squares'], squares, attributes=attributes))
        attributes = {'units': 'hPa', 'description': 'sum of scp measurements, refl x row x lat x lon'}
        features.append(Feature(['scp_sum'], totals,  attributes=attributes))
        attributes = {'units': 'samples', 'description': 'count of scp measurements, refl x row x lat x lon'}
        features.append(Feature(['scp_sample_count'], counts,  attributes=attributes))
        attributes = {'units': '_', 'description': 'represented orbit numbers'}
        features.append(Feature(['orbit_number'], orbits, attributes=attributes))

        # stash file
        self.stash(features, destination, compression=9)

        # # construct scene pressure features
        # features = []
        #
        # # extract date
        # attributes = {'units': 'hPa', 'description': 'sum of squared scp measurements, refl x row x lat x lon'}
        # features.append(Feature(['scp_sum_of_squares'], squaresii, attributes=attributes))
        # attributes = {'units': 'hPa', 'description': 'sum of scp measurements, refl x row x lat x lon'}
        # features.append(Feature(['scp_sum'], totalsii,  attributes=attributes))
        # attributes = {'units': 'samples', 'description': 'count of scp measurements, refl x row x lat x lon'}
        # features.append(Feature(['scp_sample_count'], countsii,  attributes=attributes))
        # attributes = {'units': '_', 'description': 'represented orbit numbers'}
        # features.append(Feature(['orbit_number'], orbitsii, attributes=attributes))
        #
        # # stash file
        # self.stash(features, destinationii, compression=9)

        return None

    def scheme(self, strategy, source='../studies/cumulo/squares', sink='../studies/cumulo/reports'):
        """Perform various splits of the data to check for lowest variances.

        Arguments:
            strategy: list of tuples, the reflectivity binning strategy
            source: str filepath to source directory
            sink: str, filepath to sink directory

        Returns:
            None
        """

        # create all combinations for possible splits
        partitions = list(range(1, 10))
        combinations = [itertools.combinations(partitions, length) for length in range(1, 5)]
        combinations = [list(combination) for group in combinations for combination in group]

        # sort all combinations
        [combination.sort() for combination in combinations]

        # multiple by 6 to get row indices
        combinations = [[entry * 6 for entry in combination] for combination in combinations]

        # construct schemes
        schemes = []
        for combination in combinations:

            # begin strategy
            scheme = [(one, two) for one, two in zip([0] + combination, combination + [60])]
            schemes.append(scheme)

        # create sink
        self._make(sink)

        # grab all files
        hydra = Hydra(source)

        # for each one
        for path in hydra.paths:

            # begin report
            report = ['row binning schemes:', '']
            report.append('reflectivity strategy: {}'.format(strategy))

            # ingest
            hydra.ingest(path)

            # extract date
            squares = hydra.dig('sum_of_squares')[0].distil()
            totals = hydra.dig('sums_pressure')[0].distil()
            counts = hydra.dig('sample_counts')[0].distil()

            # create scores
            scores = []

            # for each schme:
            for scheme in schemes:

                # print
                self._print(scheme)

                # for each bracket from the strategy
                numbers = []
                means = []
                deviations = []
                for first, last in strategy:

                    # and for each bracket from the scheme
                    for beginning, ending in scheme:

                        # calculate the mean
                        total = totals[first: last, beginning: ending, :, :].sum(axis=0).sum(axis=0)
                        square = squares[first: last, beginning: ending, :, :].sum(axis=0).sum(axis=0)
                        count = counts[first: last, beginning: ending, :, :].sum(axis=0).sum(axis=0)

                        # calculate the median mean and number of samples
                        averages = total / count
                        average = averages[numpy.isfinite(averages)]
                        mean = numpy.percentile(average, 50)
                        means.append(mean)

                        # calculate sample numbers
                        number = numpy.percentile(count[count > 0], 50)
                        numbers.append(number)

                        # calculate the median stdevs, after removing zeros
                        variances = (square / count) - (total / count) ** 2
                        variance = variances[variances > 0]
                        deviation = numpy.sqrt(numpy.percentile(variance, 50))
                        deviations.append(deviation)

                # get averages of numbers, means, and deviations
                triplet = []
                triplet.append(sum(numbers) / len(numbers))
                triplet.append(sum(means) / len(means))
                triplet.append(sum(deviations) / len(deviations))

                # add strategy to report
                report.append(str(scheme))
                report.append('strategy: {}'.format(strategy))
                report.append('numbers: {}'.format(numbers))
                report.append('means: {}'.format(means))
                report.append('deviations: {}'.format(deviations))
                report.append('averages: {}'.format(triplet))
                report.append(' ')

                # add to scores
                scores.append((scheme, triplet[2]))

            # sort scores
            scores.sort(key=lambda pair: pair[1], reverse=True)
            [report.append(str(score[0]) +'\n' + str(score[1])) for score in scores]

            # write report
            destination = path.replace('.h5', '_scheme.txt').replace('squares', 'reports')
            self._jot(report, destination)

        return None

    def smear(self, years, months, strategy, scheme, source='squares', sourceii='swap', sink='fills', sinkii='smears'):
        """Create test file from climatology, including filling empty spots.

        Arguments:
            years: tuple of ints, the years to combine
            months: tuple of ints, the months to check
            strategy: list of tuples of ints, the reflectivity brackets
            scheme: list of tuples of ints, the row bins
            source: str, source folder for climatology files
            sourceii: str, source folder for swap file
            sink: str, sink folder for trial
            sinkii: str, sink folder for maps

        Returns:
            None
        """

        # craete sink folders
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # gather land sea mask
        hydra = Hydra('{}/mask'.format(self.sink))
        hydra.ingest(0)
        mask = hydra.dig('land_sea_mask')[0].distil()
        land = mask.astype(bool)
        sea = numpy.logical_not(land)

        # gather climatology path
        folder = '{}/{}'.format(self.sink, source)
        hydra = Hydra(folder)

        # create hydra for swaps
        swaps = Hydra('{}/{}'.format(self.sink, sink))

        # get source file path
        original = hydra._see('{}/{}'.format(self.sink, sourceii))[0]
        originals = Hydra(original)
        originals.ingest(0)

        # create latitude/ longitude grid centered at half degrees, N to S, W to E
        latitudes = numpy.array([[89.5 - index for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[index - 179.5 for index in range(360)] for _ in range(180)])

        # flatten arrays
        latitudes = latitudes.flatten()
        longitudes = longitudes.flatten()

        # begin climatology data reservoirs
        counts = {}
        totals = {}
        squares = {}

        # create month strings, and go through each month
        months = [str(month).zfill(2) for month in months]
        for month in months:

            # print status
            self._print('month {}...'.format(month))

            # accumulate data
            counts[month] = numpy.zeros((11, 60, 180, 360))
            totals[month] = numpy.zeros((11, 60, 180, 360))
            squares[month] = numpy.zeros((11, 60, 180, 360))

            # for each year
            for year in years:

                # collect particular month at certain years
                climatology = [path for path in hydra.paths if str(year) in path and month in path][0]
                hydra.ingest(climatology)

                # add to data accumulation, summing across appropriate rows
                counts[month] += hydra.dig('ocp_sample_count')[0].distil()
                totals[month] += hydra.dig('ocp_sum')[0].distil()
                squares[month] += hydra.dig('ocp_sum_of_squares')[0].distil()

        # add all months together for annual average
        counts['annual'] = numpy.array([counts[month] for month in months]).sum(axis=0)
        totals['annual'] = numpy.array([totals[month] for month in months]).sum(axis=0)
        squares['annual'] = numpy.array([squares[month] for month in months]).sum(axis=0)

        # for each reflectivity bin
        for low, high in strategy:

            # and each row bin
            for first, last in scheme:

                # unpack brackets
                formats = [years[0], years[-1]]
                formats += [str(low).zfill(2), str(high).zfill(2)]
                formats += [str(first).zfill(2), str(last).zfill(2)]
                tag = '_monthly_climatology_{}_{}_reflectance_{}_{}_rows_{}_{}.he4'.format(*formats)
                replacement = original.replace(sourceii, sink).replace('.he4', tag)
                annual = replacement.replace('monthly', 'annual')
                rough = replacement.replace('monthly', 'annual_unsmoothed')

                # copy original
                self._copy(original, replacement)
                self._copy(original, annual)
                self._copy(original, rough)

                # create monthly reservoirs
                averages = {}
                deviations = {}
                samples = {}
                smears = {}

                # go through each month
                for month in months + ['annual']:

                    # print status
                    self._print('month {}...'.format(month))

                    # create bins
                    count = counts[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)
                    total = totals[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)
                    square = squares[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)

                    # calculate average pressures
                    average = total / count

                    # calculate variance as average of squares - square of average
                    variance = (square / count) - average ** 2
                    deviation = numpy.sqrt(variance)

                    # replace average with fill values 0 or less and change type
                    average = numpy.where(average > 0, average, -9999.0)
                    average = average.astype('float32')

                    # replace deviations with fill value 0 or infinite
                    deviation = numpy.where(numpy.isfinite(deviation), deviation, -9999.0)
                    deviation = numpy.where(deviation > 0, deviation, -9999.0)

                    # smear and fill using 3 x 3 grid averages
                    smear = self._smooth(average, count, land, sea, degrees=[5, 5]).astype('float32')

                    # store for later
                    averages[month] = average
                    deviations[month] = deviation
                    samples[month] = count
                    smears[month] = smear

                # go through each month again
                for month in months:

                    # insert into both the monthly and the annual file
                    identity = 'cloudPressures{}'.format(month)
                    grid = 'cloud_pressureGrid'
                    swaps._insert(smears[month], replacement, identity, grid)
                    swaps._insert(smears['annual'], annual, identity, grid)
                    swaps._insert(averages['annual'], rough, identity, grid)

                    # begin features for heatmap
                    features = []
                    extension = '_m{}_heatmaps.h5'.format(month)
                    destination = replacement.replace(sink, sinkii).replace('.he4', extension)

                    # grab the clouds pressures for the month
                    clouds = originals.dig('cloudPressures{}'.format(month))[0].distil()

                    # flatten arrays
                    clouds = clouds.flatten()
                    count = samples[month].flatten()
                    average = smears[month].flatten()
                    deviation = deviations[month].flatten()

                    # get annual pressure
                    annum = smears['annual'].flatten()

                    # add orginal
                    name = 'heatmap_original_cloud_pressures'
                    measure = clouds
                    mask = measure > 0
                    array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_unsmoothed_pressure'
                    measure = averages[month].flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_average_pressure'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_smoothed_annual_pressure'
                    measure = annum
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [450, 500, 550, 600, 650, 700, 750, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_unsmoothed_annual_pressure'
                    measure = averages['annual'].flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [450, 500, 550, 600, 650, 700, 750, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_annual_standard_deviations'
                    measure = deviations['annual'].flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 50, 100, 150, 200, 250]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_annual_difference'
                    measure = average
                    measureii = annum
                    mask = (measure > 0) & (measureii > 0)
                    array = numpy.vstack([[measureii[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [-300, -200, -100, 0, 100, 200, 300]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_full'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 200, 400, 600, 800, 1000, 1200]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_difference'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[clouds[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [200 * index - 600 for index in range(7)]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create sample counts
                    name = 'heatmap_sample_counts'
                    measure = count
                    mask = measure >= 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 1, 5, 25, 50, 100, 200, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} samples'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'samples'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_standard_deviations'
                    measure = deviation
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 50, 100, 150, 200, 250, 500]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_standard_avg_error'
                    measure = deviation / numpy.sqrt(count)
                    mask = (measure > 0) & numpy.isfinite(measure)
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 50, 100, 150, 200]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # stash at destination
                    self.stash(features, destination)

        return None

    def splat(self, year=2005, month=9, number=25):
        """Plot scene pressure vs reflectivities for a random set of points.

        Arguments:
            year: int, the year
            month: int, the month
            number: int, number of points

        Returns:
            None
        """

        # create folder
        self._make('{}/plots/splatter'.format(self.sink))

        # get climatology
        hydra = Hydra('{}/climatology'.format(self.sink))
        hydra.ingest('{}.h5'.format(year))

        # get data
        scenes = hydra.grab('scene_pressure')
        deviations = hydra.grab('standard_deviation')
        count = hydra.grab('sample_counts')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        months = hydra.grab('month')
        reflectivity = hydra.grab('reflectivity')

        # find closest month
        position = hydra._pin(month, months)[0][0]

        # tranpose scenes and deviations, extracting month
        scenes = scenes[position].transpose(1, 2, 0)
        deviations = deviations[position].transpose(1, 2, 0)

        # construct latitude and longitude blocks
        latitudes = numpy.array([latitude] * 360).transpose(1, 0)
        longitudes = numpy.array([longitude] * 180)

        # create mask to only pick points with at least 3 points
        fill = -9999
        mask = (scenes.sum(axis=2) > fill * 4)
        scenes = scenes[mask]
        deviations = deviations[mask]
        latitudes = latitudes[mask]
        longitudes = longitudes[mask]

        # create random lists of latitude and longitude indices
        points = [numpy.random.randint(latitudes.shape[0]) for _ in range(number)]

        # set up map plot
        size = (10, 5)
        back = 'white'
        native = cartopy.crs.PlateCarree()
        pyplot.clf()
        axis = pyplot.axes(projection=native)
        axis.coastlines(linewidth=3)
        _ = axis.gridlines(draw_labels=True)
        axis.background_patch.set_facecolor(back)
        axis.set_ymargin(0.1)
        axis.set_xmargin(0.1)

        # Adjust margins around the figure
        left = 0.1
        right = 0.1
        top = 0.19
        bottom = 0.18
        pyplot.subplots_adjust(left=left, right=1-right, top=1-top, bottom=bottom)

        # set axis ratios
        axis.grid(True)
        axis.set_global()
        axis.set_aspect('auto')
        axis.tick_params(axis='both', labelsize=7)

        # set axis limits and title
        title = 'Scene Pressure Climatology\nRandom Points'
        axis.set_xlim(-180, 180)
        axis.set_ylim(-90, 90)
        axis.set_title(title, fontsize=14)

        # for each point
        for index, point in enumerate(points):

            # unpack point
            ordinate = latitudes[point]
            abscissa = longitudes[point]

            # plot
            axis.plot(abscissa, ordinate, marker='X', color='green', markersize=10)
            axis.annotate(str(index), (abscissa, ordinate))

        # Save the plot by calling plt.savefig() BEFORE plt.show()
        destination = '{}/plots/splatter/Random_Points.png'.format(self.sink)
        pyplot.savefig(destination)
        pyplot.clf()

        # silencr runtime warnings
        warnings.filterwarnings("ignore", category=RuntimeWarning)

        # for each point
        for index, point in enumerate(points):

            # clear figure
            pyplot.clf()
            pyplot.subplots_adjust(left=left * 2, right=1-right, top=1-top, bottom=bottom)

            # create title
            formats = (year, self._pad(month), index, latitudes[point], longitudes[point])
            title = 'Scene pressure {}m{}, number {}\n {} lat, {} lon'.format(*formats)

            # set limits
            pyplot.title(title)
            pyplot.xlim(0, 1.1)
            pyplot.ylim(300, 1050)
            pyplot.xlabel('reflectivity')
            pyplot.ylabel('scene pressure ( hPa )')

            # get scene pressure and deviation data
            scene = scenes[point]
            deviation = deviations[point]

            # create mask for fill values
            mask = (scene > fill) & (deviation > fill)
            abscissa = reflectivity[mask]
            ordinate = scene[mask]
            bar = deviation[mask]

            # plot the scene pressure
            # pyplot.plot(reflectivity[mask], scene[mask], marker='x', color='green')
            pyplot.errorbar(abscissa, ordinate, yerr=bar, capsize=5, ecolor="black", color="black")

            print(index)
            print(abscissa)
            print(ordinate)
            print(bar)

            # # for each deviation and scene pressure versus reflectivity
            # for horizontal, vertical, bar in zip(reflectivity, scene, deviation):
            #
            #     # if deviation greater than fill
            #     if vertical > fill:
            #
            #         print(index, bar)
            #
            #         # plot the bar
            #         pyplot.plot([horizontal - 0.05, horizontal + 0.05], [vertical - bar, vertical - bar], 'k-')
            #         pyplot.plot([horizontal - 0.05, horizontal + 0.05], [vertical + bar, vertical + bar], 'k-')

            # Save the plot
            destination = '{}/plots/splatter/Scene_Pressure_{}.png'.format(self.sink, self._pad(index))
            pyplot.savefig(destination)
            pyplot.clf()

        # reset warnings
        warnings.resetwarnings()

        return None

    def strategize(self, source='../studies/cumulo/squares', sink='../studies/cumulo/reports'):
        """Perform various splits of the data to check for lowest variances.

        Arguments:
            source: str filepath to source directory
            sink: str, filepath to sink directory

        Returns:
            None
        """

        # create all combinations for possible splits
        partitions = list(range(1, 10))
        combinations = [itertools.combinations(partitions, length) for length in range(1, 10)]
        combinations = [list(combination) for group in combinations for combination in group]

        # sort all combinations
        [combination.sort() for combination in combinations]

        # construct strategies
        strategies = []
        for combination in combinations:

            # begin strategy
            strategy = [(one, two) for one, two in zip([0] + combination, combination + [10])]
            strategies.append(strategy)

        # create sink
        self._make(sink)

        # grab all files
        hydra = Hydra(source)

        # for each one
        for path in hydra.paths:

            # begin report
            report = ['binning strategies:', '']

            # ingest
            hydra.ingest(path)

            # extract date
            squares = hydra.dig('sum_of_squares')[0].distil()
            totals = hydra.dig('sums_pressure')[0].distil()
            counts = hydra.dig('sample_counts')[0].distil()

            # create scores
            scores = []

            # for each strategy:
            for strategy in strategies:

                # print
                self._print(strategy)

                # for each bracket
                numbers = []
                means = []
                deviations = []
                for bracket in strategy:

                    # calculate the mean
                    total = totals[bracket[0]: bracket[1], :, :, :].sum(axis=0).sum(axis=0)
                    square = squares[bracket[0]: bracket[1], :, :, :].sum(axis=0).sum(axis=0)
                    count = counts[bracket[0]: bracket[1], :, :, :].sum(axis=0).sum(axis=0)

                    # calculate the median mean and number of samples
                    averages = total / count
                    average = averages[numpy.isfinite(averages)]
                    mean = numpy.percentile(average, 50)
                    means.append(mean)

                    # calculate sample numbers
                    number = numpy.percentile(count[count > 0], 50)
                    numbers.append(number)

                    # calculate the median stdevs, after removing zeros
                    variances = (square / count) - (total / count) ** 2
                    variance = variances[variances > 0]
                    deviation = numpy.sqrt(numpy.percentile(variance, 50))
                    deviations.append(deviation)

                # get averages of numbers, means, and deviations
                triplet = []
                triplet.append(sum(numbers) / len(numbers))
                triplet.append(sum(means) / len(means))
                triplet.append(sum(deviations) / len(deviations))

                # add strategy to report
                report.append(str(strategy))
                report.append('numbers: {}'.format(numbers))
                report.append('means: {}'.format(means))
                report.append('deviations: {}'.format(deviations))
                report.append('averages: {}'.format(triplet))
                report.append(' ')

                # add to scores
                scores.append((strategy, triplet[2]))

            # sort scores
            scores.sort(key=lambda pair: pair[1], reverse=True)
            [report.append(str(score[0]) +'\n' + str(score[1])) for score in scores]

            # write report
            destination = path.replace('.h5', '_strategy.txt').replace('squares', 'reports')
            self._jot(report, destination)

        return None

    def submit(self, years, months, strategy, scheme, source='squares', sourceii='swap', sink='trials', sinkii='maps'):
        """Create test file from climatology, in imitation of cloud pressure file.

        Arguments:
            years: tuple of ints, the years to combine
            months: tuple of ints, the months to check
            strategy: list of tuples of ints, the reflectivity brackets
            scheme: list of tuples of ints, the row bins
            source: str, source folder for climatology files
            sourceii: str, source folder for swap file
            sink: str, sink folder for trial
            sinkii: str, sink folder for maps

        Returns:
            None
        """

        # craete sink folders
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # gather climatology path
        folder = '{}/{}'.format(self.sink, source)
        hydra = Hydra(folder)

        # create hydra for swaps
        swaps = Hydra('{}/{}'.format(self.sink, sink))

        # get source file path
        original = hydra._see('{}/{}'.format(self.sink, sourceii))[0]
        originals = Hydra(original)
        originals.ingest(0)

        # create latitude/ longitude grid centered at half degrees, N to S, W to E
        latitudes = numpy.array([[89.5 - index for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[index - 179.5 for index in range(360)] for _ in range(180)])

        # flatten arrays
        latitudes = latitudes.flatten()
        longitudes = longitudes.flatten()

        # begin climatology data reservoirs
        counts = {}
        totals = {}
        squares = {}

        # create month strings, and go through each month
        months = [str(month).zfill(2) for month in months]
        for month in months:

            # print status
            self._print('month {}...'.format(month))

            # accumulate data
            counts[month] = []
            totals[month] = []
            squares[month] = []

            # for each year
            for year in years:

                # collect particular month at certain years
                climatology = [path for path in hydra.paths if str(year) in path and month in path][0]
                hydra.ingest(climatology)

                # add to data accumulation
                counts[month].append(hydra.dig('ocp_sample_count')[0].distil())
                totals[month].append(hydra.dig('ocp_sum')[0].distil())
                squares[month].append(hydra.dig('ocp_sum_of_squares')[0].distil())

            # add together all years
            counts[month] = numpy.array(counts[month]).sum(axis=0)
            totals[month] = numpy.array(totals[month]).sum(axis=0)
            squares[month] = numpy.array(squares[month]).sum(axis=0)

        # add all months together for annual average
        counts['annual'] = numpy.array([counts[month] for month in months]).sum(axis=0)
        totals['annual'] = numpy.array([totals[month] for month in months]).sum(axis=0)
        squares['annual'] = numpy.array([squares[month] for month in months]).sum(axis=0)

        # for each reflectivity bin
        for low, high in strategy:

            # and each row bin
            for first, last in scheme:

                # unpack brackets
                formats = [years[0], years[2]]
                formats += [str(low).zfill(2), str(high).zfill(2)]
                formats += [str(first).zfill(2), str(last).zfill(2)]
                tag = '_monthly_climatology_{}_{}_reflectance_{}_{}_rows_{}_{}.he4'.format(*formats)
                replacement = original.replace(sourceii, sink).replace('.he4', tag)
                annual = replacement.replace('monthly', 'annual')

                # copy original
                self._copy(original, replacement)
                self._copy(original, annual)

                # create monthly reservoirs
                averages = {}
                deviations = {}
                samples = {}

                # go through each month
                for month in months + ['annual']:

                    # print status
                    self._print('month {}...'.format(month))

                    # create bins
                    count = counts[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)
                    total = totals[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)
                    square = squares[month][low: high, first: last, :, :].sum(axis=0).sum(axis=0)

                    # calculate average pressures
                    average = total / count

                    # calculate variance as average of squares - square of average
                    variance = (square / count) - average ** 2
                    deviation = numpy.sqrt(variance)

                    # replace average with fill values 0 or less and change type
                    average = numpy.where(average > 0, average, -9999.0)
                    average = average.astype('float32')

                    # replace deviations with fill value 0 or infinite
                    deviation = numpy.where(numpy.isfinite(deviation), deviation, -9999.0)
                    deviation = numpy.where(deviation > 0, deviation, -9999.0)

                    # store for later
                    averages[month] = average
                    deviations[month] = deviation
                    samples[month] = count

                # go through each month again
                for month in months:

                    # insert into both the monthly and the annual file
                    identity = 'cloudPressures{}'.format(month)
                    grid = 'cloud_pressureGrid'
                    swaps._insert(averages[month], replacement, identity, grid)
                    swaps._insert(averages['annual'], annual, identity, grid)

                    # begin features for heatmap
                    features = []
                    extension = '_m{}_heatmaps.h5'.format(month)
                    destination = replacement.replace(sink, sinkii).replace('.he4', extension)

                    # grab the clouds pressures for the month
                    clouds = originals.dig('cloudPressures{}'.format(month))[0].distil()

                    # flatten arrays
                    clouds = clouds.flatten()
                    count = samples[month].flatten()
                    average = averages[month].flatten()
                    deviation = deviations[month].flatten()

                    # get annual pressure
                    annum = averages['annual'].flatten()

                    # add orginal
                    name = 'heatmap_original_cloud_pressures'
                    measure = clouds
                    mask = measure > 0
                    array = numpy.vstack([measure[mask], latitudes[mask], longitudes[mask]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_average_pressure'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_annual_pressure'
                    measure = annum
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_annual_difference'
                    measure = average
                    measureii = annum
                    mask = (measure > 0) & (measureii > 0)
                    array = numpy.vstack([[measureii[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [-500, -400, -300, -200, -100, 0, 100, 200, 300, 400, 500]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_full'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 200, 400, 600, 800, 1000, 1200]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_difference'
                    measure = average
                    mask = measure > 0
                    array = numpy.vstack([[clouds[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [200 * index - 600 for index in range(7)]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create sample counts
                    name = 'heatmap_sample_counts'
                    measure = count
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 5, 25, 50, 100, 200, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} samples'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'samples'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_standard_deviations'
                    measure = deviation
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 50, 100, 150, 200, 250, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # stash at destination
                    self.stash(features, destination)

        return None

    def trial(self, years, months, strategy, scheme, source='squares', sourceii='swap', sink='trials', sinkii='maps'):
        """Create test file from climatology, in imitation of cloud pressure file.

        Arguments:
            years: tuple of ints, the years to combine
            months: tuple of ints, the months to check
            source: str, source folder for climatology files
            sourceii: str, source folder for swap file
            sink: str, sink folder for trial
            sinkii: str, sink folder for maps

        Returns:
            None
        """

        # craete sink folders
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))

        # gather climatology path
        folder = '{}/{}'.format(self.sink, source)
        hydra = Hydra(folder)

        # create hydra for swaps
        swaps = Hydra('{}/{}'.format(self.sink, sink))

        # get source file path
        original = hydra._see('{}/{}'.format(self.sink, sourceii))[0]
        originals = Hydra(original)
        originals.ingest(0)

        # create latitude and longitude grids, flipping North and South
        latitudes = numpy.array([[float((179 - index) - 90) for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[float(index - 180) for index in range(360)] for _ in range(180)])

        # flatten arrays
        latitudes = latitudes.flatten()
        longitudes = longitudes.flatten()

        # for each reflectivity
        for reflectivity in [(1, 3), (3, 7), (5, 7), (7, 10)]:

            # and each row
            for row in [(0, 60), (0, 20)]:

                # create copy of file
                low = str(reflectivity[0]).zfill(2)
                high = str(reflectivity[1]).zfill(2)
                first = str(row[0]).zfill(2)
                last = str(row[1]).zfill(2)
                formats = (start, finish, low, high, first, last)
                tag = '_climatology_{}_{}_reflectance_{}_{}_rows_{}_{}.he4'.format(*formats)
                replacement = original.replace(sourceii, sink).replace('.he4', tag)

                # copy original
                self._copy(original, replacement)

                # and for each month
                for month in months:

                    # grab the clouds pressures for the month
                    clouds = originals.dig('cloudPressures{}'.format(str(month).zfill(2)))[0].distil()
                    clouds = clouds.flatten()

                    # collect particular month at certain years
                    climatologies = []
                    for year in range(start, finish + 1):

                        # add climatology path
                        climatologies += [path for path in hydra.paths if str(year) in path and str(month).zfill(2) in path]

                    # for each climatology
                    pressures = []
                    deviations = []
                    counts = []
                    for climatology in climatologies:

                        # ingest climatology
                        hydra.ingest(climatology)

                        # grab the reflectivity climatotogy cloud pressures
                        name = 'ocp_average_{}/rows_{}-{}'.format(reflectivity[0] / 10, first, last)
                        pressure = hydra.dig(name)[0].distil()
                        pressures.append(pressure)

                        # grab the reflectivity climatotogy cloud pressures
                        name = 'ocp_standard_deviations_{}/rows_{}'.format(reflectivity[0] / 10, first, last)
                        deviation = hydra.dig(name)[0].distil()
                        deviations.append(deviation)

                        # grab the reflectivity climatotogy standard deviations
                        name = 'ocp_sample_count_{}/rows_{}'.format(reflectivity[0] / 10, first, last)
                        count = hydra.dig(name)[0].distil()
                        counts.append(count)

                    # add together all samples counts
                    samples = numpy.array(counts).sum(axis=0)

                    # reconstruct average overall years by weighting by sample counts
                    sums = [pressure * count for pressure, count in zip(pressures, counts)]
                    average = numpy.array(sums).sum(axis=0) / samples

                    # reconstruct standard deviation over all years
                    variances = [deviation ** 2 for deviation in deviations]
                    zipper = zip(variances, pressures, counts)
                    squares = [(variance + pressure ** 2) * count for variance, pressure, count in zipper]
                    squares = numpy.array(squares).sum(axis=0)
                    spread = numpy.sqrt((squares / samples) - average ** 2)

                    # replace average with fill values
                    average = numpy.where(average > 0, average, -9999.0)
                    average = average.astype('float32')

                    # insert into the file
                    identity = 'cloudPressures{}'.format(str(month).zfill(2))
                    grid = 'cloud_pressureGrid'
                    swaps._insert(average, replacement, identity, grid)

                    # begin features for heatmap
                    features = []

                    extension = '_{}_heatmaps.h5'.format(str(month).zfill(2))
                    destination = replacement.replace(sink, sinkii).replace('.he4', extension)

                    # add orginal
                    name = 'heatmap_original_cloud_pressures'
                    mask = clouds > 0
                    array = numpy.vstack([clouds[mask], latitudes[mask], longitudes[mask]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_average_pressure'
                    measure = average.flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [300, 400, 500, 600, 700, 800]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_differences'
                    measure = average.flatten()
                    mask = measure > 0
                    array = numpy.vstack([[clouds[mask] - measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [200 * index - 600 for index in range(7)]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create sample counts
                    name = 'heatmap_sample_counts'
                    measure = samples.flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 5, 25, 50, 100, 200, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} samples'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'samples'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # create mean pressures
                    name = 'heatmap_pressure_standard_deviations'
                    measure = spread.flatten()
                    mask = measure > 0
                    array = numpy.vstack([[measure[mask], latitudes[mask], longitudes[mask]]])
                    bounds = [0, 50, 100, 150, 200, 250, 500, 1000]
                    brackets = [(first, last) for first, last in zip(bounds[:-1], bounds[1:])]
                    labels = ['{} to {} hPa'.format(int(first), int(last)) for first, last in brackets]
                    attributes = {'brackets': brackets, 'labels': labels, 'units': 'hPa'}
                    feature = Feature(['Categories', name], array, attributes=attributes)
                    features.append(feature)

                    # stash at destination
                    self.stash(features, destination)

        return None

    def track(self, source='squares', sourceii='swap', sink='winter'):
        """Track the OCP across winter months in high latitudes.

        Arguments:
            None

        Returns:
            None
        """

        # # set default strategy and months
        months = (1, 2, 3, 4, 9, 10, 11, 12)
        years = (2005, 2010, 2015)

        # craete sink folders
        self._make('{}/{}'.format(self.sink, sink))

        # gather climatology path
        folder = '{}/{}'.format(self.sink, source)
        hydra = Hydra(folder)

        # create latitude/ longitude grid centered at half degrees, N to S, W to E
        latitudes = numpy.array([[89.5 - index for index in range(180)] for _ in range(360)]).transpose(1, 0)
        longitudes = numpy.array([[index - 179.5 for index in range(360)] for _ in range(180)])

        # flatten arrays
        latitudes = latitudes.flatten()
        longitudes = longitudes.flatten()

        # begin climatology data reservoirs
        counts = {}
        totals = {}
        squares = {}

        # begin features
        features = []

        # create month strings, and go through each month
        months = [str(month).zfill(2) for month in months]
        years = [str(year) for year in years]
        for month in months:

            # go through each year
            for year in years:

                # print status
                self._print('month {}, year {}...'.format(month, year))

                # begin data with empty arrays for reflectivity, latitude, and longiurde (rows and years added together)
                counts[(month, year)] = numpy.zeros((60,))
                totals[(month, year)] = numpy.zeros((60,))
                squares[(month, year)] = numpy.zeros((60,))

                # collect particular month at certain years
                climatology = [path for path in hydra.paths if str(year) in path and month in path][0]
                hydra.ingest(climatology)

                # add to data accumulation, summing across appropriate rows
                counts[(month, year)] += hydra.dig('ocp_sample_count')[0].distil()[3:6, :, :20, :].sum(axis=3).sum(axis=2).sum(axis=0)
                totals[(month, year)] += hydra.dig('ocp_sum')[0].distil()[3:6, :, :20, :].sum(axis=3).sum(axis=2).sum(axis=0)
                squares[(month, year)] += hydra.dig('ocp_sum_of_squares')[0].distil()[3:6, :, :20, :].sum(axis=3).sum(axis=2).sum(axis=0)

                print(counts[(month, year)])

                # create feature
                name = 'avg_ocp_{}_{}_70n'.format(month, year)
                array = totals[(month, year)] / counts[(month, year)]
                feature = Feature(['Categories', name], array)
                features.append(feature)

        # create row feature
        name = 'row'
        array = numpy.array(list(range(60)))
        feature = Feature(['IndependentVariables', name], array)
        features.append(feature)

        # stash file
        destination = '{}/{}/Winter_months_OCP_by_Row.h5'.format(self.sink, sink)
        self.stash(features, destination)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 5
    arguments = arguments[:5]

    # umpack arguments
    year, month, start, finish, sink = arguments

    # create cumulo instance
    cumulo = Cumulo(sink)

    # check for dimer
    if '--dimer' in options:

        # run dimerize
        cumulo.dimerize(year, month, start, finish)

    # otherwise
    else:

        # run accumulation
        cumulo.scend(year, month, start, finish)


