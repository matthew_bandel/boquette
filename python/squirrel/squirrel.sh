#!/bin/bash

rm dumps/*.sql

pg_dump --table '"omi_trendingcategory_parameters"' ozoneaq_prod >> "dumps/omi_trendingcategory_parameters.sql";
pg_dump --table '"omi_trendingcategory"' ozoneaq_prod >> "dumps/omi_trendingcategory.sql";
pg_dump --table '"omi_trendingparameter_indvarchoices"' ozoneaq_prod >> "dumps/omi_trendingparameter_indvarchoices.sql";
pg_dump --table '"omi_trendingparameter"' ozoneaq_prod >> "dumps/omi_trendingparameter.sql";
pg_dump --table '"omi_trendingproduct"' ozoneaq_prod >> "dumps/omi_trendingproduct.sql";
pg_dump --table '"omi_trendingprofile"' ozoneaq_prod >> "dumps/omi_trendingprofile.sql";
pg_dump --table '"omi_trendingprofile_categories"' ozoneaq_prod >> "dumps/omi_trendingprofile_categories.sql";
pg_dump --table '"omi_trendingprofile_parameters"' ozoneaq_prod >> "dumps/omi_trendingprofile_parameters.sql";
pg_dump --table '"omi_trendingprofile_products"' ozoneaq_prod >> "dumps/omi_trendingprofile_products.sql";
pg_dump --table '"omi_trendingconfig"' ozoneaq_prod >> "dumps/omi_trendingconfig.sql";

pg_dump --table '"omi_measurementtype"' ozoneaq_prod >> "dumps/omi_measurementtype.sql";
pg_dump --table '"omi_measurementtype_archivesets"' ozoneaq_prod >> "dumps/omi_measurementtype_archivesets.sql";
pg_dump --table '"omi_archiveset"' ozoneaq_prod >> "dumps/omi_archiveset.sql";
pg_dump --table '"omi_sensor"' ozoneaq_prod >> "dumps/omi_sensor.sql";

