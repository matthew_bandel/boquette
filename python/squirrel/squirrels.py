#!/usr/bin/env python3

# squirrels.py for dumping h5 files into PostgreSQL database

# import sys, os
import sys
import os

# import psycopg2 for postgresql interface
import psycopg2

# import yaml
import yaml

# import pretty print
import pprint

# import hdf5 files
import h5py

# import Pillow and io for making sparklines
from PIL import Image, ImageDraw
from io import StringIO, BytesIO

# import math
import math

# check for python2
if sys.version_info.major == 2:

    # get quoate from Python2 (urllib)
    from urllib import quote

    # and also define missing exceptions
    FileExistsError = OSError
    FileNotFoundError = IOError
    PermissionError = OSError
    NotADirectoryError = OSError

# otherwise, assume python 3
else:

    # get from PYthon3 (urllib.parse)
    from urllib.parse import quote


# class Squirrel to dump h5 files into SQL
class Squirrel(object):
    """Squirrel

    Inherits from:
        object
    """

    def __init__(self, configuration):
        """Initialize instance.

        Arguments:
            configuration: str, file path of yaml data tables configuration file
        """

        # set configuration file
        self.configuration = configuration

        # allocate for attribute tables and yamls
        self.models = {}
        self.yams = []

        # allocate for source paths
        self.source = ''
        self.start = ''
        self.finish = ''
        self.paths = []

        # set source file acceptable extensions
        self.extensions = ('.h5',)

        # populate from configurtion file
        self._configure()

        # set default orbit and dates ids
        self.orbits = 0
        self.dates = 0

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Squirrel instance >'

        return representation

    def _acquire(self, path):
        """Load in a yaml file.

        Arguments:
            path: str, filepath

        Returns:
            dict

        """

        # default information to empty
        information = {}

        # try to
        try:

            # open yaml file
            with open(path, 'r') as pointer:

                # and read contents
                information = yaml.safe_load(pointer)

        # unless not found
        except FileNotFoundError:

            # in which case print error
            self._print('yaml {} not found!'.format(path))

        return information

    def _categorize(self, chain, configuration, cursor):
        """Extract categories from h5 file.

        Arguments:
            chain: list of fields
            configuration: dict, trendingconfig information
            cursor: cursor object

        Returns:
            integer, the category id
        """

        # parse the chain for attributes
        name = chain.split('/')[-1]
        path = chain
        level = len(chain.split('/'))

        # get configuration id
        identifier = configuration['id']

        # construct record
        record = {'name': name, 'full_path': path, 'nesting_level': level, 'config_id': identifier}

        # insert the record
        table = self.models['omi_trendingcategory']
        identifier = self._insert([record], table, cursor, see=True)

        return identifier

    def _configure(self):
        """Load up the yaml configuration files.

        Arguments:
            None

        Returns:
            None
        """

        # load in data table configurations
        configuration = self._acquire(self.configuration)

        # for each file
        for model in configuration['models']:

            # add yaml to yams
            yam = self._acquire(model)
            self.yams.append(yam)

            # for each table
            for table in yam['tables']:

                # add entry to models
                name = table['table']
                attributes = table['attributes']
                database = yam['database']
                user = yam['user']
                service = yam['service']
                entry = {'name': name, 'database': database, 'user': user, 'attributes': attributes}
                entry.update({'service': service})
                self.models[name] = entry

        # get source file information
        self.source = configuration['source']['folder']
        self.start = configuration['source']['start']
        self.finish = configuration['source']['finish']

        # register paths
        self._register()

        return None

    def _connect(self, model):
        """Connect to a datqbase.

        Arguments:
            model: dict, model object

        Returns:
            (connection object, cursor object) tuple
        """

        # unpack model
        service = model['service']
        database = model['database']
        user = model['user']

        # establish connection and cursor
        connection = psycopg2.connect("service={}".format(service))
        cursor = connection.cursor()

        return connection, cursor

    def _construct(self, drop=True):
        """Construct all tables from yaml files.

        Arguments:
            drop: tables first?

        Returns:
            None
        """

        # for each yam
        for yam in self.yams:

            # create postgresql connection
            connection, cursor = self._connect(yam)

            # for each table
            for table in yam['tables']:

                # create table name
                name = table['table'].lower()

                # get attributes
                attributes = table['attributes']

                # create table
                self._create(name, attributes, cursor, drop=drop, see=True)

            # commit changes
            connection.commit()

        return None

    def _create(self, name, attributes, cursor, drop=False, see=False):
        """Create a new table.

        Arguments:
            name: str, table name
            attributes: list of dict, table attributes
            cursor: psycopg cursor object
            see: boolean, display query?
            drop=True: boolean, drop table first?

        Returns:
            None
        """

        # construct attributes list
        specifics = []
        for attribute in attributes:

            # construct specific attributes
            formats = (str(attribute['name']), str(attribute['type']), str(attribute['constraint']))
            specific = """"{}" {} {}""".format(*formats)
            default = attribute.get('default', '')
            if attribute['default']:

                # check for str
                if default == str(default):

                    # add default
                    specific = """{} DEFAULT '{}'""".format(specific, attribute['default'])

                # check for str
                else:

                    # otherwise, assume numeric
                    specific = """{} DEFAULT {}""".format(specific, attribute['default'])

            # append
            specifics.append(specific)

        # join specifics
        specifics = ', '.join(specifics)

        # if droppping table
        if drop:

            # perform deletion
            self._drop(name, cursor)

        # construct creation
        creation = """CREATE TABLE IF NOT EXISTS "{}" ({}); """.format(name, specifics)

        # if for displsy
        if see:

            # print deletion
            self._print(creation)

        # execute table creation
        cursor.execute(creation)

        return None

    def _demolish(self):
        """Clear all tables.

        Arguments:
            None

        Returns:
            None
        """

        # for each yam
        for yam in self.yams[::-1]:

            # create postgresql connection
            database = yam['database']
            connection, cursor = self._connect(yam)

            # for each table
            for table in yam['tables']:

                # create table name
                name = '{}_{}'.format(database, table['table'].lower())

                # create table
                self._drop(name, cursor, see=True)

            # commit changes
            connection.commit()

        return None

    def _dispense(self, information, destination):
        """Load in a yaml file.

        Arguments:
            information: dict
            destination: str, filepath

        Returns:
            None
        """

        # open yaml file
        with open(destination, 'w') as pointer:

            # try to
            try:

                # dump contents, sorted
                yaml.dump(information, pointer, sort_keys=False)

            # unless not an option (python 2)
            except TypeError:

                # just dump unsorted
                yaml.dump(information, pointer)

        return None

    def _drop(self, name, cursor, see=False):
        """Drop all tables in reverse creation order.

        Arguments:
            name: table name
            cursor: cursor object
            see: boolean, print query to stdout?

        Returns:
            None
        """

        # perform deletion
        deletion = """DROP TABLE IF EXISTS "{}" CASCADE;""".format(name)

        # if for displsy
        if see:

            # print deletion
            self._print(deletion)

        # perform deletion
        cursor.execute(deletion)

        return None

    def _establish(self):
        """Populate data tables with default entries.

        Arguments:
            None

        Returns:
            None
        """

        # get Sensor table and associated database
        sensor = self.models['omi_sensor']
        connection, cursor = self._connect(sensor)

        # insert default entry into sensor table
        self._insert([{}], sensor, cursor)

        # insert default entry into archive set table
        archive = self.models['omi_archiveset']
        self._insert([{}], archive, cursor)

        # insert default entry into measurementTypes table
        measurement = self.models['omi_measurementtype']
        self._insert([{}], measurement, cursor)

        # commit change
        connection.commit()

        return None

    def _extract(self, group, indent=0):
        """Extract independent variables.

        Arguments:
            group: hdf5 file or group
            indent: int, number of fields to skip in full path

        Returns:
            None
        """

        # connect to configurationn database
        model = self.models['omi_trendingconfig']
        connection, cursor = self._connect(model)

        # get configuration information from top entry for now
        name = model['name']
        attributes = model['attributes']
        configuration = self._select(name, attributes, cursor=cursor, see=True)

        # get configuration information from top entry for now
        model = self.models['omi_trendingproduct']
        name = model['name']
        attributes = model['attributes']
        product = self._select(name, attributes, cursor=cursor, see=True)

        # # collect all parameter keys
        keys = list(group.keys())

        # collect all references and addresses
        references = [group[key].ref for key in keys]
        addresses = [group[reference].name for reference in references]

        # collect all categories, removing up to indent
        splittings = [address.split('/')[indent + 1:-1] for address in addresses]
        categories = ['/'.join(splitting[:index + 1]) for splitting in splittings for index in range(len(splitting))]
        keys = categories

        # remove duplicates
        categories = list({category: True for category in categories}.keys())

        # add categories
        identifiers = {category: self._categorize(category, configuration, cursor) for category in categories}

        # for each address
        for reference, address, key in zip(references, addresses, keys):

            # try to
            try:

                # add as a dataset to the Parameters table
                array = group[reference][:]
                chain = address.split('/')[indent + 1:]
                attributes = dict(group[reference].attrs)
                identifier = identifiers[key]
                self._parameterize(array, chain, attributes, identifier, configuration, product, cursor)

            # skip if KeyValue for now
            except KeyError:

                # print alert
                print('skipping {}!'.format(address))

        # commit connection
        connection.commit()

        return None

    def _ingest(self):
        """Ingest the hdf5 files into the sql tables.

        Arguments:
            None

        Returns:
            None
        """

        # for each path
        for path in self.paths:

            # print status
            self._print('ingesting {}...'.format(path))

            # open file under context manager to properly close
            with h5py.File(path, 'r') as five:

                # extract independent variables
                self._extract(five['IndependentVariables'], indent=0)

                # extract categories and parameters from the Data group to preserve order
                self._extract(five['Data'], indent=1)
                #
                # # update product model with latest file
                # self._refresh(five, path)

        return None

    def _insert(self, data, table, cursor, see=False):
        """Create a new table.

        Arguments:
            data: list of dicts, data entries
            table: dict, table yaml entry
            drop=True: boolean, drop table first?
            see=False: print the insertion to screen

        Returns:
            None
        """

        # create table name
        name = table['name'].lower()

        # set default identifier
        identifier = 0

        # for each datum
        for datum in data:

            # if the datum is non emptry
            if len(datum) > 0:

                # construct columns
                columns = ''
                for key in list(datum.keys()):

                    # add double quoted string
                    columns += '"{}", '.format(str(key))

                # trim off trailing comma
                columns = columns[:-2]

                # construct values
                values = []
                for value in datum.values():

                    # if it acts like a string
                    if value == str(value):

                        # add with quotes
                        values.append("'{}'".format(value))

                    # otherwise
                    else:

                        # format as simple stri
                        values.append(str(value))

                # join
                places = ', '.join(['%s' for _ in values])

                # construct insertion query
                insertion = """INSERT INTO "{}" ({}) VALUES ({}) RETURNING id;""".format(name, columns, places)

                # if willed
                if see:

                    # print insertion to screen
                    self._print(insertion)

                # apply insertion
                cursor.execute(insertion, values)
                identifier = cursor.fetchone()[0]

            # otherwise
            else:

                # construct insertion query
                insertion = """INSERT INTO "{}" DEFAULT VALUES RETURNING id;""".format(name)

                # if willed
                if see:

                    # print insertion to screen
                    self._print(insertion)

                # apply insertion
                cursor.execute(insertion)
                identifier = cursor.fetchone()[0]

        return identifier

    def _look(self, contents, level=0):
        """Look at the contents of an h5 file, to a certain level.

        Arguments:
            contents: object to look at
            level=2: the max nesting level to see
            five: the h5 file

        Returns:
            None
        """

        # unpack into a dict
        data = self._unpack(contents, level=level)

        # pretty print it
        pprint.pprint(data)

        return None

    def _parameterize(self, array, chain, attributes, category, configuration, product, cursor):
        """Extract parameters from h5 file.

        Arguments:
            array: hdf5 file dataset
            chain: list of str, previous chain of fields
            attributes: dict, dataset attributes
            category: int, id of category column
            configuration: dict, config model
            product: dict, product model
            cursor: cursor

        Returns:
            None
        """

        # get the name as the last entry
        name = chain[-1]

        # print status
        self._print('creating parameter {}...'.format(name))

        # begin data record
        record = {}

        # create the data as a list for json formatting
        record['name'] = name
        record['json'] = [list(array)]
        record['sparkline'] = self._spark(array)
        record['orbits_id'] = self.orbits
        record['dates_id'] = self.dates

        # add default configuration and product
        record['config_id'] = configuration['id']
        record['product_id'] = product['id']

        # add other attributes
        record['FillValue'] = float(attributes['FillValue'])
        record['LongName'] = attributes['LongName']
        record['Units'] = attributes['Units']
        record['Status'] = attributes['Status']
        record['doNotPlot'] = attributes['doNotPlot']
        record['PlotLegend'] = [attributes['PlotLegend']]
        record['AbscissaVariableSelection'] = [attributes['AbscissaVariableSelection']]

        # insert into parameter table
        model = self.models['omi_trendingparameter']
        identifier = self._insert([record], model, cursor, see=False)

        # insert pair into pairs table
        model = self.models['omi_trendingcategory_parameters']
        pair = {'trendingcategory_id': category, 'trendingparameter_id': identifier}
        _ = self._insert([pair], model, cursor, see=False)

        # insert each pair and its inverse (why?) into indvars table
        pairs = []
        model = self.models['omi_trendingparameter_indvarchoices']
        pairs.append({'from_trendingparameter_id': identifier, 'to_trendingparameter_id': self.dates})
        pairs.append({'from_trendingparameter_id': self.dates, 'to_trendingparameter_id': identifier})
        pairs.append({'from_trendingparameter_id': identifier, 'to_trendingparameter_id': self.orbits})
        pairs.append({'from_trendingparameter_id': self.orbits, 'to_trendingparameter_id': identifier})
        _ = self._insert(pairs, model, cursor, see=False)

        # set orbits identifiers
        if name == 'OrbitNumber':

            # set orbites
            self.orbits = identifier

        # set date identifiers
        if name == 'OrbitStartTimeFrYr':

            # set dates
            self.dates = identifier

        return None

    def _populate(self):
        """Populate data tables with default entries.

        Arguments:
            None

        Returns:
            None
        """

        # get Sensor table and associated database
        configuration = self.models['omi_trendingconfig']
        connection, cursor = self._connect(configuration)

        # insert default entry into configuration table
        self._insert([{}], configuration, cursor)

        # insert default entry into trending product table
        product = self.models['omi_trendingproduct']
        self._insert([{}], product, cursor)

        # commit change
        connection.commit()

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        return None

    def _register(self):
        """Construct all file paths.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.paths
        """

        # get all file paths
        paths = []
        self._print('collecting paths...')

        # if a source directory is given
        if self.source:

            # and start and finish are given
            if self.start and self.finish:

                # get all folders
                folders = self._see(self.source)
                for folder in folders:

                    # try to:
                    try:

                        # check for inclusion in range
                        if int(self.start) <= int(folder.split('/')[-1]) <= int(self.finish):

                            # add to paths
                            paths += self._see(folder)

                    # unless the folder is not compatible
                    except ValueError:

                        # in which case, skip
                        pass

            # otherwise
            else:

                # try to
                try:

                    # get all paths in the directory
                    paths += self._see(self.source)

                # unless it is not a directory
                except NotADirectoryError:

                    # in which case, assume it is a file
                    paths += [self.source]

        # retain only certain file types
        paths = [path for path in paths if any([path.endswith(extension) for extension in self.extensions])]
        paths.sort()

        # print paths
        self._tell(paths)
        self._print('{} paths collected.\n'.format(len(paths)))

        # set attribute
        self.paths = paths

        return None

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # try to
        try:

            # make paths
            paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # unless the directory does not exist
        except FileNotFoundError:

            # in which case, alert and return empty list
            self._print('{} does not exist'.format(directory))
            paths = []

        return paths

    def _select(self, name, attributes, cursor, see=False):
        """Select an entry from a table.

        Arguments:
            name: str, table name
            attributes: list of attributes
            cursor: psycopg cursor object
            see: boolean, display query?

        Returns:
            None
        """

        # construct query
        selection = """SELECT * FROM "{}";""".format(name)

        # if willed
        if see:

            # print to screen
            self._print(selection)

        # execute selection
        cursor.execute(selection)
        row = cursor.fetchone()

        # convert to dictionary
        names = [attribute['name'] for attribute in attributes]
        record = {name: datum for name, datum in zip(names, row)}

        return record

    def _spark(self, data):
        """Create sparkline from 1-D data.

        Arguments:
            data: hdf5 or numpy array

        Returns:
            sparkline
        """

        # set image attributes in pixels
        width, height = (200, 20)

        # begin image
        image = Image.new("RGB", (width, height), 'white')

        # determine indices for subsampling based on image width
        chunk = len(data) / width
        indices = [math.floor(index * chunk) for index in range(width)]
        indices = list(set(indices))
        indices.sort()

        # subsample the data,
        sample = data[indices]

        # try to
        try:

            # convert to float and flip the y-axis (PIL images y-axis increases towards bottom)
            sample = [float(datum) * -1 for datum in sample]

        # unless not floats
        except ValueError:

            # set value to 0s
            sample = [0.0] * 20

        # determine extent of y range
        maximum = max(sample)
        minimum = min(sample)
        extent = maximum - minimum

        # calculate a margin and data range
        margin = max([0.1 * extent, 0.001])
        top = maximum + margin
        bottom = minimum - margin

        # in case top and bottom are fills, change gap to 1.0
        gap = (top - bottom) or 1.0

        # translate the data to pixel heights
        pixels = [height * (datum - bottom) / gap for datum in sample]
        points = [(horizontal, vertical) for horizontal, vertical in enumerate(pixels)]

        # make artist and draw the line
        artist = ImageDraw.Draw(image)

        # draw the line in gray
        gray = "#888888"
        artist.line(points, fill=gray)

        # draw a red rectangle at the end
        red = "#FF0000"
        rectangle = [len(sample) - 2, pixels[-1] - 1, len(sample), pixels[-1] + 1]
        artist.rectangle(rectangle, fill=red)

        # create file pointer as bytes
        pointer = BytesIO()
        image.save(pointer, "png")

        # craete sparkline
        sparkline = "data:image/png,{}".format(quote(pointer.getvalue()))

        return sparkline

    def _tell(self, queue):
        """Enumerate the contents of a list.

        Arguments:
            queue: list

        Returns:
            None
        """

        # for each item
        for index, member in enumerate(queue):

            # print
            self._print('{}) {}'.format(index, member))

        # print spacer
        self._print(' ')

        return None

    def _unpack(self, contents, level=100, nest=0):
        """Unpack an h5 file into a dict.

        Arguments:
            contents: h5 file or dict
            level=100: int, highest nesting level kept
            nest=0: current nesting level

        Returns:
            dict
        """

        # check for nesting level
        if nest > level:

            # data is truncated to an ellipsis
            data = '...'

        # otherwise
        else:

            # try
            try:

                # to go through keys
                data = {}
                for field, info in contents.items():

                    # add key to data
                    data[field] = self._unpack(info, level, nest + 1)

            # unless there are no keys
            except AttributeError:

                # just return the contents
                data = contents

        return data

    def scrounge(self):
        """Create all tables and populate from files given in configuration file.

        Arguments:
            None

        Return:
            None
        """

        # drop all tables in reverse order
        self._demolish()

        # clear and reestablish database tables
        self._construct()

        # populate default table values
        self._establish()

        # populate default omi table values
        self._populate()

        # ingest files
        self._ingest()

        # print status
        self._print('ingested.')

# run from command line, given a configuration yaml file
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 1
    arguments = arguments[:1]

    # umpack arguments
    configuration = arguments[0]

    # create squirrel instance
    squirrel = Squirrel(configuration)

    # convert files in SQL tables
    squirrel.scrounge()
