#!/usr/bin/env python3

# heimdals.py for the Heimdal class to crossreference omi and omps orbits

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import regex
import re

# import numpy functions
import numpy
from scipy import stats

# import datetime
import datetime

# import sklearn
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree
from sklearn.ensemble import RandomForestRegressor

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False


# class Millipede to do OMI data reduction
class Heimdal(Hydra):
    """Heimdal class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self):
        """Initialize instance.

        Arguments:
            None
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # keep records reservoir
        self.reservoir = []

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Heimdal instance >'

        return representation

    def _crystalize(self, data, latitudes, longitudes, segment=20):
        """Bin the geographic data by percentile bins.

        Arguments:
            data: numpy array of measurement data
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes
            segment=20: size of bin

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # begin geodes
        geodes = []

        # for each bracket
        brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
        for first, second in brackets:

            self._print(first, second)

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            datum = data[mask].flatten()
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            # concatenate
            geode = numpy.vstack([datum, latitude, longitude])

            # order along latitude, then longitude
            geode = geode[:, numpy.argsort(latitude)]
            geode = geode[:, numpy.argsort(longitude)]

            # append
            geodes.append(geode)

        return geodes, brackets

    def _exchange(self, path, destination, codex):
        """Swap out yaml attributes:

        Arguments:
            path: str, file path to source yaml
            destination: str, file path to destination yaml
            codex: dict, the update dictionary

        Returns:
            None
        """

        # open up yaml
        yam = self._acquire(path)

        # make updates
        yam.update(codex)

        # redispence
        self._dispense(yam, destination)

        return None

    def _segregate(self, latitudes, longitudes, latitudesii, longitudesii):
        """Segregate pairs of geolocation coordinates into those within or without a polygon.

        Arguments:
            latitudes: list of float, latitude coordinates of sample points in question
            longitudes: list of float, longitude coordinates of sample points in question
            latitudesii: list of float, latitude coordinates of enclosing polygon
            longitudesii: list of floats, longitude coordinates of enclosing polygon.

        Returns:
            ( numpy array, numpy array ) tuple, the coordinates of the points within and without the polygon

        Notes:
            Assumes latitudesii and longitudesii are given in order around the perimeter,
            and do not include a redundant final point.

            Assumes all angles between points are <= 180 degrees
        """

        # start clock
        #self._stamp('segregaring {} points...'.format(len(latitudes)), initial=True)

        # create samples and perimeter as numpy arrays
        #self._stamp('calculating polygon...')
        samples = numpy.array([latitudes, longitudes]).transpose(1, 0)
        perimeter = numpy.array([latitudesii, longitudesii]).transpose(1, 0)

        # duplicate first entry at last position as well
        polygon = numpy.concatenate([perimeter, [perimeter[0]]])

        # create segments from consecutive pairs of points
        segments = [polygon[index: index + 2, :] for index, _ in enumerate(polygon[:-1])]

        # calculate slopes of all segments as delta latitude / delta longitude
        slopes = [(segment[1, 0] - segment[0, 0]) / (segment[1, 1] - segment[0, 1]) for segment in segments]

        # calculate intercepts from first point: b = y0 - m(x0) = lat0 - m(lon0)
        intercepts = [(segment[0, 0] - slope * segment[0, 1]) for segment, slope in zip(segments, slopes)]

        # calculate center point
        center = (latitudesii.mean(), longitudesii.mean())

        # determine polarities (True = up ) for each segment by comparing the midpoint to the center
        zipper = zip(segments, slopes, intercepts)
        polarities = [center[0] > slope * center[1] + intercept for segment, slope, intercept in zipper]

        # create boolean masks for each segment
        #self._stamp('making masks for each side...')
        masks = []
        for segment, slope, intercept, polarity in zip(segments, slopes, intercepts, polarities):

            # if polarity is True
            if polarity:

                # create boolean mask for all points where lat >= m * lon + b
                mask = samples[:, 0] > slope * samples[:, 1] + intercept
                masks.append(mask)

            # otherwise
            else:

                # create boolean mask for all points where  lat <= m * lon + b
                mask = samples[:, 0] < slope * samples[:, 1] + intercept
                masks.append(mask)

        # multiply all masks for polygon mask and make inverse
        masque = numpy.prod(masks, axis=0).astype(bool)
        inverse = numpy.logical_not(masque)

        # split into insiders and outsiders
        #self._stamp('filtering data...')
        insiders = samples[masque]
        outsiders = samples[inverse, :]

        # timestamp
        #self._print('{} insiders, {} outsiders'.format(len(insiders), len(outsiders)))
        #self._stamp('segregated.')

        return insiders, outsiders, center, polygon

    def _understand(self, records, reflectivity, extent, bins=10):
        """Construct the histograms.

        Arguments:
            records: list of list of dicts, the records
            reflectivity: str, reflectivity level
            extent: int, number of records to average
            bins: int, number of bins

        Returns:
            None
        """

        # collect all delta pressures
        pressures = []
        for record in records:

            # average appropriate entries
            pressure = numpy.average([entry['deltas']['pressure ( hPa )'] for entry in record[:extent]])
            pressures.append(pressure)

        # determine 95th and 5th percentile
        lower = numpy.percentile(numpy.array(pressures), 5)
        upper = numpy.percentile(numpy.array(pressures), 95)

        # break into equal pieces of a certain width
        width = (upper - lower) / bins

        # define brackets
        brackets = [(lower + width * index, lower + width * (index + 1)) for index in range(bins)]
        brackets = [(lower - width, lower)] + brackets + [(upper, upper + width)]

        # create middle of each bin
        middles = list([sum(bracket) / 2 for bracket in brackets])

        # get counts for heights
        bounds = brackets
        bounds[0] = (min(pressures), brackets[0][1])
        bounds[1] = (brackets[-1][0], max(pressures))
        boxes = [[pressure for pressure in pressures if bracket[0] <= pressure <= bracket[1]] for bracket in bounds]
        counts = list([len(box) for box in boxes])

        # make labels
        ordinate = 'concurrent pairs'
        abscissa = 'delta Pressure (hPa) NMCLDRR - OMCLDRR'

        # construct title attributes
        pairs = len(records)
        dates = [record[0]['omi']['date'] for record in records]
        dates.sort()
        start = dates[0][:7]
        finish = dates[-1][:7]

        # create bracket descriptions
        descriptions = {'low': 'refl. < 0.3', 'middle': '0.3 < refl. < 0.7', 'high': '0.7 < refl.', 'all': 'all refl.'}

        # make title
        title = 'Cloud Pressure Difference, NMCLDRR - OMCLDRR, {}'.format(descriptions[reflectivity])
        title += '\n ( {} pairs from {} to {}, < 30 sec, 1 km apart, avg of {} )'.format(pairs, start, finish, extent)

        # dump into json
        plot = {'width': width, 'middles': middles, 'counts': counts, 'ordinate': ordinate, 'abscissa': abscissa}
        plot.update({'title': title})
        self._dump(plot, '../studies/heimdal/plots/histogram_{}_{}.json'.format(extent, reflectivity))

        # add file for histograms
        route = ['Categories', 'delta_pressure_histogram']
        array = numpy.array(pressures)
        feature = Feature(route, array)
        destination = '../studies/heimdal/histograms/histogram_{}_{}.h5'.format(extent, reflectivity)
        self.stash([feature], destination, 'Data')

        return None

    def cook(self, old, new):
        """Cook up yaml files.

        Arguments:
            old: str or int, old date in 'yyyymmdd' format
            new: str or int, new date in 'yyyymmdd' format

        Returns:
           None
        """

        # convert stringed dates to integers as they are integer keys
        old = int(old)
        new = int(new)

        # open up examples yaml
        examples = self._acquire('../studies/heimdal/yams/clouds.yaml')['examples']

        # open up old yam for omi tiles
        path = '../studies/heimdal/yams/omi/omi_{}.yaml'.format(old)
        yam = self._acquire(path)

        # begin codex
        fields = ('title', 'album', 'abscissa', 'ordinate')
        codex = {field: yam[field] for field in fields}

        # make replacements
        codex['title'] = codex['title'].replace(examples[old]['date'], examples[new]['date'])
        codex['title'] = codex['title'].replace(examples[old]['location'], examples[new]['location'])
        codex['title'] = codex['title'].replace(str(examples[old]['pressure']), str(examples[new]['pressure']))
        codex['album'] = codex['album'].replace(str(old), str(new))
        codex['abscissa']['start'] = examples[new]['west']
        codex['abscissa']['finish'] = examples[new]['east']
        codex['ordinate']['start'] = examples[new]['south']
        codex['ordinate']['finish'] = examples[new]['north']

        # dump yam
        destination = path.replace(str(old), str(new))
        self._exchange(path, destination, codex)

        # open up old yam for omps tiles
        path = '../studies/heimdal/yams/omps/omps_{}.yaml'.format(old)
        yam = self._acquire(path)

        # begin codex
        fields = ('title', 'album', 'abscissa', 'ordinate')
        codex = {field: yam[field] for field in fields}

        # make replacements
        codex['title'] = codex['title'].replace(examples[old]['date'], examples[new]['date'])
        codex['title'] = codex['title'].replace(examples[old]['location'], examples[new]['location'])
        codex['title'] = codex['title'].replace(str(examples[old]['pressureii']), str(examples[new]['pressureii']))
        codex['album'] = codex['album'].replace(str(old), str(new))
        codex['abscissa']['start'] = examples[new]['west']
        codex['abscissa']['finish'] = examples[new]['east']
        codex['ordinate']['start'] = examples[new]['south']
        codex['ordinate']['finish'] = examples[new]['north']

        # dump yam
        destination = path.replace(str(old), str(new))
        self._exchange(path, destination, codex)

        # open up old yam for trackwise pressures
        path = '../studies/heimdal/yams/tracks/tracks_{}.yaml'.format(old)
        yam = self._acquire(path)

        # begin codex
        fields = ('title', 'album', 'abscissa')
        codex = {field: yam[field] for field in fields}

        # make replacements
        codex['title'] = codex['title'].replace(examples[old]['date'], examples[new]['date'])
        codex['title'] = codex['title'].replace(examples[old]['location'], examples[new]['location'])
        codex['title'] = codex['title'].replace(str(examples[old]['difference']), str(examples[new]['difference']))
        codex['album'] = codex['album'].replace(str(old), str(new))
        codex['abscissa']['start'] = examples[new]['west']
        codex['abscissa']['finish'] = examples[new]['east']

        # dump yam
        destination = path.replace(str(old), str(new))
        self._exchange(path, destination, codex)

        # open up old yam for trackwise pressures
        path = '../studies/heimdal/yams/reflections/reflections_{}.yaml'.format(old)
        yam = self._acquire(path)

        # begin codex
        fields = ('title', 'album', 'abscissa')
        codex = {field: yam[field] for field in fields}

        # make replacements
        codex['title'] = codex['title'].replace(examples[old]['date'], examples[new]['date'])
        codex['title'] = codex['title'].replace(examples[old]['location'], examples[new]['location'])
        codex['album'] = codex['album'].replace(str(old), str(new))
        codex['abscissa']['start'] = examples[new]['west']
        codex['abscissa']['finish'] = examples[new]['east']

        # dump yam
        destination = path.replace(str(old), str(new))
        self._exchange(path, destination, codex)

        return

    def decree(self, criterion=0.02, precision=3):
        """Assemble all records and sort them into a report.

        Arguments:
            criterion: max metric value
            precision: int, float precision rounding level
            metric: str, sort metric
            reverse: boolean, reverse sort based on metric?

        Returns:
            None
        """

        # timestamp
        self._stamp('assembling reports...', initial=True)

        # gather all json files
        paths = [path for path in self._see('../studies/heimdal/records') if '.json' in path]

        # for each path
        records = []
        for path in paths:

            # accumulate records
            reservoir = self._load(path)
            records += reservoir

        # sort records by metric
        records.sort(key=lambda record: record[0]['metric'])

        # only keep those meeting criterion
        records = [record for record in records if record[0]['metric'] <= criterion]

        # add to attribute
        self.reservoir = records
        reservoir = self.reservoir

        # set reflectivity brackets
        brackets = {'all': (0.0, 1.0), 'low': (0.0, 0.3), 'middle': (0.3, 0.7), 'high': (0.7, 1.0)}

        # set accumulation numbers (number of omi points to average)
        sizes = [1, 4, 9]
        sizes = [1]

        # go through each one
        for reflectivity, bracket in brackets.items():

            # only get records in reflectivity range
            records = [record for record in reservoir if bracket[0] <= record[0]['omi']['reflectivity'] <= bracket[1]]

            # go through each accumulation number
            for size in sizes:

                # begin report
                report = ['closest data points for omi, omps']
                report += ['criterion: < {}'.format(criterion)]
                report += ['{} points found'.format(len(records))]
                report += ['{} omi measurements averaged'.format(size)]
                report += ['']
                report += ['deltas:']

                # check for records
                if len(records) > 0:

                    # for deltas
                    for parameter in records[0][0]['deltas'].keys():

                        # calculate median
                        data = [numpy.mean([entry['deltas'][parameter] for entry in record[:size]]) for record in records]
                        median = round(numpy.percentile(data, 50), precision)

                        # calculate median average difference
                        difference = round(stats.median_absolute_deviation(data, scale=1), precision)

                        # add to report
                        report += ['{}: median: {}, median absolute difference: {}'.format(parameter, median, difference)]

                        # calculate mean and stdev
                        mean = round(float(numpy.mean(data)), precision)
                        deviation = round(float(numpy.std(data)), precision)

                        # add to report
                        report += ['{}: mean: {}, standard deviation: {}'.format(parameter, mean, deviation)]

                    # add spacer
                    report += ['']

                    # add records to report
                    for index, record in enumerate(records):

                        # add index and date
                        report += ['{}) {}:'.format(index, record[0]['omi']['date'])]

                        # go through each entry
                        for entry in record[:size]:

                            # create line
                            line = '{}) metric: {})'.format(index, round(entry['metric'], precision))
                            for name, quantity in entry['deltas'].items():

                                # add entry
                                line += '| {}: {}'.format(name, round(quantity, precision))

                            # add to report
                            report.append(line)

                        # add spacer
                        report += [' ']

                # jot report
                self._jot(report, '../studies/heimdal/reports/report_{}_{}.txt'.format(size, reflectivity))

                # create histogram
                self._understand(records, reflectivity, size)

        # timestamp
        self._stamp('reports assembled.')

        return None

    def imagine(self, date, sink, rank=0):
        """Create the h5 file for viewing in boquette.

        Arguments:
            date: str, date in 'yyyy-mm-dd' format
            sink: str, folder for deposit
            rank=0: index of record in list for the day

        Returns:
            None
        """

        # make sink folder
        self._make(sink)

        # split date
        year, month, day = date.split('-')

        # retrieve record reservoir
        reservoir = self._load('../studies/heimdal/records/reservoir_{}m{}.json'.format(year, month))

        # get all records on the day
        records = [record for record in reservoir if '{}m{}{}'.format(year, month, day) in record[0]['omi']['date']]

        # sort by metric of first
        records.sort(key=lambda record: record[0]['metric'])

        # grab the record of indicated rank, top entry
        record = records[rank][0]

        # desginate hydras and sensors
        sensors = ('omi', 'omps')
        breadths = {'omi': 20, 'omps': 10}

        # begin blocks,
        blocks = {}
        attributes = {'polygons': [], 'values': []}

        # create hydra
        hydra = Hydra()

        # for each sensor
        for sensor in sensors:

            # ingest path
            hydra.ingest(record[sensor]['path'])

            # get the latitude, longitude, and pressuree data, and flatten
            track = record[sensor]['track']
            row = record[sensor]['row']
            breadth = breadths[sensor]

            # dig up attributes
            pressures = hydra.dig('CloudPressureforO3')[0].distil()
            latitudes = hydra.dig('Latitude')[0].distil()
            longitudes = hydra.dig('Longitude')[0].distil()

            # subset and concatenate into block
            tensors = (pressures, latitudes, longitudes)
            block = [[tensor[track - breadth: track + breadth].flatten()] for tensor in tensors]
            block = numpy.concatenate(block, axis=0)
            blocks[sensor] = block

            # get latitude, longitude, and pressuree at point
            latitude = latitudes[track][row]
            longitude = longitudes[track][row]
            pressure = pressures[track][row]

            # add pressure to values
            attributes['values'].append(pressure)

            # retrieve points at neighboring diagonal corners
            offsets = [(1, -1), (1, 1), (-1, 1), (-1, -1)]
            corners = []
            for first, second in offsets:

                # make corner and append
                corner = (latitudes[track + first][row + second], longitudes[track + first][row + second])
                corners.append(corner)

            # append half the distances to polygon
            polygon = [((corner[0] + latitude) / 2, (corner[1] + longitude) / 2) for corner in corners]
            attributes['polygons'].append(polygon)

        # create feature
        name = 'heatmap_cloud_pressure_omi_omps_{}m{}{}'.format(year, month, day)
        array = numpy.concatenate([blocks['omi'], blocks['omps']], axis=1)
        feature = Feature(['Categories', name], array, attributes=attributes)

        # stash file
        destination = '{}/Cloud_Pressures_OMI_OMPS_{}m{}{}_{}_{}.h5'.format(sink, year, month, day, rank, self._note())
        hydra._stash([feature], destination, 'Data')

        return None

    def juxtapose(self, date, sink, low, high):
        """Create h5 files for viewing separate orbits in boquette.

        Arguments:
            date: str, date in 'yyyy-mm-dd' format
            sink: str, folder for deposit
            low: float, low metric  range
            high: float, high metric range

        Returns:
            None
        """

        # make sink folder
        self._make(sink)

        # split date
        year, month, day = date.split('-')

        # retrieve record reservoir
        reservoir = self._load('../studies/heimdal/records/reservoir_{}m{}.json'.format(year, month))

        # get all records on the day
        records = [record for record in reservoir if '{}m{}{}'.format(year, month, day) in record[0]['omi']['date']]

        # only grab high reflectivity set
        records = [record for record in records if 0.7 <= record[0]['omi']['reflectivity'] <= 1.0]

        # sort by metric of first
        records.sort(key=lambda record: record[0]['metric'])

        # grab the record of indicated by low, high metric range
        record = [entry for entry in records if low <= entry[0]['metric'] <= high][0][0]

        # desginate hydras and sensors
        sensors = ('omi', 'omps')
        breadths = {'omi': 15, 'omps': 6}
        extents = {'omi': 21, 'omps': 35}
        starts = {'omi': 4, 'omps': 1}

        # begin blocks,
        blocks = {}
        tensors = {}
        attributes = {'omi': {'polygons': [], 'values': []}, 'omps': {'polygons': [], 'values': []}}

        # create hydra
        hydra = Hydra()

        # open up clodo2 files
        pathii = record['omi']['path'].replace('OMCLDRR', 'OMCDO2N').replace('70004', '10003')
        orbit = str(self._stage(pathii)['orbit'])[1:]
        folder = '/'.join(pathii.split('/')[:-1])
        paths = self._see(folder)
        secondary = [path for path in paths if orbit in path][0]

        # for each sensor
        for sensor in sensors:

            # ingest senor path and secondary path for dimer pressure
            hydra.ingest(record[sensor]['path'])
            hydra.ingest(secondary, discard=False)

            # get the latitude, longitude, and pressuree data, and flatten
            track = record[sensor]['track']
            row = record[sensor]['row']
            breadth = breadths[sensor]

            # dig up attributes
            pressures = hydra.dig('CloudPressureforO3')[0].distil()
            latitudes = hydra.dig('Latitude')[0].distil()
            longitudes = hydra.dig('Longitude')[0].distil()
            reflectivities = hydra.dig('Reflectivity')[0].distil()
            fractions = hydra.dig('CloudFractionforO3')[0].distil()
            dimers = hydra.dig('CloudPressure')[0].distil().squeeze()

            # subset and concatenate into block
            tensors[sensor] = (pressures, latitudes, longitudes, reflectivities, fractions, dimers)
            block = [[tensor[track - breadth: track + breadth].flatten()] for tensor in tensors[sensor][:3]]
            block = numpy.concatenate(block, axis=0)
            blocks[sensor] = block

            # for each track position
            for position in range(track - breadth, track + breadth):

                # and each row
                for positionii in range(starts[sensor], extents[sensor]):

                    # get latitude, longitude, and pressuree at point
                    latitude = latitudes[position][positionii]
                    longitude = longitudes[position][positionii]
                    pressure = pressures[position][positionii]

                    # retrieve points at neighboring diagonal corners
                    offsets = [(1, -1), (1, 1), (-1, 1), (-1, -1)]
                    corners = []
                    for first, second in offsets:

                        # make corner and append
                        corner = (latitudes[position + first][positionii + second],)
                        corner += (longitudes[position + first][positionii + second],)
                        corners.append(corner)

                    # append half the distances to polygon
                    polygon = [((corner[0] + latitude) / 2, (corner[1] + longitude) / 2) for corner in corners]

                    # if sensor is omi
                    if sensor == 'omps':

                        # make corners
                        cornersii = []

                        # get zone of omi polygons
                        polygonsii = attributes['omi']['polygons']
                        points = [point for polygon in polygonsii for point in polygon]

                        # add the southmost point
                        points.sort(key=lambda point: point[0])
                        cornersii += [points[0]]

                        # add the westmost point
                        points.sort(key=lambda point: point[1])
                        cornersii += [points[0]]

                        # add the northmost point
                        points.sort(key=lambda point: point[0], reverse=True)
                        cornersii += [points[0]]

                        # add the eastmost point
                        points.sort(key=lambda point: point[1], reverse=True)
                        cornersii += [points[0]]

                        # create sample latitudes and longitudes
                        verticals = numpy.array([point[0] for point in polygon])
                        horizontals = numpy.array([point[1] for point in polygon])

                        # create zone latitudes and longitudes
                        verticalsii = numpy.array([point[0] for point in cornersii])
                        horizontalsii = numpy.array([point[1] for point in cornersii])

                        # check for inclusion
                        insiders, _, _, _ = self._segregate(verticals, horizontals, verticalsii, horizontalsii)

                        # if one of the points is an insider
                        if len(insiders) > 0:

                            # add to polygons
                            attributes[sensor]['polygons'].append(polygon)
                            attributes[sensor]['values'].append(pressure)

                    # otherwise
                    else:

                        # add to polygons
                        attributes[sensor]['polygons'].append(polygon)
                        attributes[sensor]['values'].append(pressure)

        # construct brackets every 150 pressures
        size = 200
        chunks = 8
        brackets = [(index * size, size + index * size) for index in range(chunks)]

        # add bracket labels labels
        labels = ['{}-{} hPa'.format(first, second) for first, second in brackets]

        # for each sensor
        for sensor in sensors:

            # add brackets
            attributes[sensor]['brackets'] = brackets
            attributes[sensor]['labels'] = labels

            # begin features
            features = []

            # get specipfics
            track = record[sensor]['track']
            breadth = breadths[sensor]

            # create feature for heatmap
            name = 'heatmap_cloud_pressure_{}_{}m{}{}'.format(sensor, year, month, day)
            array = numpy.array(blocks[sensor])
            feature = Feature(['Categories', name], array, attributes=attributes[sensor])
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_pressure_rw_{}_{}m{}{}'.format(sensor, year, month, day)
            pressures = tensors[sensor][0][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            feature = Feature(['Categories', name], pressures)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_pressure_tk_{}_{}m{}{}'.format(sensor, year, month, day)
            pressures = pressures.transpose(1, 0)
            feature = Feature(['Categories', name], pressures)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_reflectivity_rw_{}_{}m{}{}'.format(sensor, year, month, day)
            reflectivities = tensors[sensor][3][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            feature = Feature(['Categories', name], reflectivities)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_reflectivity_tk_{}_{}m{}{}'.format(sensor, year, month, day)
            reflectivities = reflectivities.transpose(1, 0)
            feature = Feature(['Categories', name], reflectivities)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_fraction_rw_{}_{}m{}{}'.format(sensor, year, month, day)
            fractions = tensors[sensor][4][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            feature = Feature(['Categories', name], fractions)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'cloud_fraction_tk_{}_{}m{}{}'.format(sensor, year, month, day)
            fractions = fractions.transpose(1, 0)
            feature = Feature(['Categories', name], fractions)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'dimer_pressure_rw_{}_{}m{}{}'.format(sensor, year, month, day)
            dimers = tensors[sensor][5][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            feature = Feature(['Categories', name], dimers)
            features.append(feature)

            # create 2-D longitudinal cloud pressure lines
            name = 'dimer_pressure_tk_{}_{}m{}{}'.format(sensor, year, month, day)
            dimers = dimers.transpose(1, 0)
            feature = Feature(['Categories', name], dimers)
            features.append(feature)

            # add track index variable
            name = 'row_index'
            indices = numpy.array(list(range(starts[sensor], extents[sensor])))
            feature = Feature(['IndependentVariables', name], indices)
            features.append(feature)

            # add track index variable
            name = 'track_index'
            indicesii = numpy.array(list(range(track - breadth, track + breadth)))
            feature = Feature(['IndependentVariables', name], indicesii)
            features.append(feature)

            # add latitudes
            latitudes = tensors[sensor][1][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            latitudes = latitudes.transpose(1, 0)
            for strip, index in zip(latitudes, indices):

                # create feature
                name = 'cloud_pressure_latitude_{}'.format(str(round(float(index), 1)).zfill(4))
                feature = Feature(['IndependentVariables', name], strip)
                features.append(feature)

            # add longitudes
            longitudes = tensors[sensor][2][track - breadth: track + breadth, starts[sensor]: extents[sensor]]
            for strip, index in zip(longitudes, indicesii):

                # create feature
                name = 'cloud_pressure_longitude_{}'.format(round(float(index), 1))
                feature = Feature(['IndependentVariables', name], strip)
                features.append(feature)

            # stash file
            metric = round(low, 4)
            formats = (sink, sensor.upper(), year, month, day, metric, self._note())
            destination = '{}/Cloud_Pressures_{}_{}m{}{}_{}_{}.h5'.format(*formats)
            hydra._stash(features, destination, 'Data')

        return None

    def manifest(self, folder):
        """Create all plots from json files.

        Arguments:
            folder: str, folder name of jsons

        Returns:
            None
        """

        # gather all json files
        jasons = [path for path in self._see(folder) if '.json' in path]

        # for each jason file
        for jason in jasons:

            # retrieve histogram details
            histogram = self._load(jason)

            # build the plot
            pyplot.clf()
            pyplot.title(histogram['title'])
            pyplot.xlabel(histogram['abscissa'])
            pyplot.ylabel(histogram['ordinate'])

            # draw bars
            pyplot.bar(histogram['middles'], histogram['counts'], width=histogram['width'])

            # save plot and clear
            pyplot.savefig(jason.replace('.json', '.png'))
            pyplot.clf()

        return None

    def peer(self, year, month, start, finish, criterion=0.1, matches=1):
        """Find the closest latitude and longitude match for a particular day.

        Arguments:
            year: int, year
            month: int, month
            start: int, starting day
            finish: int, ending day

        Returns:
            None
        """

        # set spatial extent in rows ( to avoid the row anomaly )
        extents = {'omi': 25, 'omps': 35}

        # set folder destinations for omi OMCLDRR and omps NMCLDRR
        folders = {'omi': '/tis/OMI/70004/OMCLDRR', 'omps': '/tis/OMPS-NPP/60000/NMCLDRR-L2'}

        # go through several days
        for day in range(int(start), int(finish) + 1):

            # begin sample and pairs reservoir
            reservoir = []
            samples = {'omi': [], 'omps': []}

            # for each sensor
            for sensor in ('omi', 'omps'):

                # make omi hydra instance
                hydra = Hydra('{}/{}/{}/{}'.format(folders[sensor], year, str(month).zfill(2), str(day).zfill(2)))
                self.hydras.append(hydra)

                # for each path
                for path in hydra.paths:

                    # try to
                    try:

                        # ingest
                        hydra.ingest(path)

                        # begin by grabbing time data and converting to hours since epoch
                        times = hydra.dig('Time')[0].distil() / 3600

                        # map fields of interest to aliases
                        aliases = {'latitude': 'Latitude', 'longitude': 'Longitude'}
                        aliases.update({'reflectivity': 'Reflectivity', 'pressure': 'CloudPressureforO3'})
                        aliases.update({'azimuth': 'RelativeAzimuthAngle', 'zenith': 'SolarZenithAngle'})
                        aliases.update({'terrain': 'TerrainHeight', 'viewing': 'ViewingZenithAngle'})
                        aliases.update({'fraction': 'CloudFractionforO3', 'chlorophyll': 'Chlorophyll'})
                        aliases.update({'filling': 'Filling-In', 'surface': 'SurfaceReflectivity'})
                        aliases.update({'flux': 'dIdR'})

                        # grab all the data
                        data = {}
                        #data = {alias: hydra.dig(field)[0].distil() for alias, field in aliases.items()}

                        for alias, field in aliases.items():

                            data[alias] = hydra.dig(field)[0].distil()

                        # grab the date
                        date = hydra._stage(path)['date']

                        # create samples for each timestep
                        for track, time in enumerate(times):

                            # skip first and last
                            if 0 < track < len(times) - 1:

                                # amd for each row
                                for row in range(1, extents[sensor]):

                                    # begin sample
                                    sample = {'path': path, 'date': date, 'row': row, 'track': track, 'time': time}

                                    # retrieve points at neighboring diagonal corners
                                    latitudes = data['latitude']
                                    longitudes = data['longitude']
                                    latitude = latitudes[track][row]
                                    longitude = longitudes[track][row]
                                    offsets = [(1, -1), (1, 1), (-1, 1), (-1, -1)]
                                    corners = []
                                    for first, second in offsets:

                                        # make corner and append
                                        corner = (latitudes[track + first][row + second], longitudes[track + first][row + second])
                                        corners.append(corner)

                                    # append half the distances to polygon
                                    polygon = [((corner[0] + latitude) / 2, (corner[1] + longitude) / 2) for corner in corners]

                                    # calculate the area of the polygon ( in sq degrees )
                                    terms = [(0, 1), (1, 2), (2, 3), (3, 0)]
                                    area = -sum([polygon[term[0]][0] * polygon[term[1]][1] for term in terms])
                                    area += sum([polygon[term[0]][1] * polygon[term[1]][0] for term in terms])
                                    area = abs(area / 2)
                                    sample.update({'area': area})

                                    # add data
                                    sample.update({alias: float(datum[track, row]) for alias, datum in data.items()})

                                    # create vector
                                    sample['vector'] = (sample['time'], sample['latitude'], sample['longitude'])

                                    # add to reservoir
                                    samples[sensor].append(sample)

                    # unless can't find
                    except IndexError:

                        # skip
                        self._print('uhoh! ...{}'.format(path))
                        self._print(field, track, row)
                        pass

            # create tree from omi locations
            self._stamp('making tree...', initial=True)
            matrix = numpy.array([sample['vector'] for sample in samples['omi']])
            tree = KDTree(matrix)

            # get the distance to second location
            self._stamp('asking tree...')
            matrixii = numpy.array([sample['vector'] for sample in samples['omps']])
            metrics, references = tree.query(matrixii, k=matches)

            # create records
            self._stamp('organizing locations...')
            for indexii, (distances, indices) in enumerate(zip(metrics, references)):

                # construct pairs for each reference
                pairs = []
                for distance, index in zip(distances, indices):

                    # get pair of samples
                    sample = samples['omi'][index]
                    sampleii = samples['omps'][indexii]

                    # begin sample pair record
                    pair = {'metric': distance, 'omi': sample, 'omps': sampleii, 'deltas': {}}

                    # add delta time, converting from hours to seconds
                    pair['deltas']['time ( sec )'] = (sampleii['time'] - sample['time']) * 3600

                    # add latitude and longitude in km (by multiplying by 111 km / deg (eq)
                    pair['deltas']['latitude ( km )'] = (sampleii['latitude'] - sample['latitude']) * 111
                    pair['deltas']['longitude ( km )'] = (sampleii['longitude'] - sample['longitude']) * 111

                    # add difference in measurements
                    pair['deltas']['pressure ( hPa )'] = (sampleii['pressure'] - sample['pressure'])

                    # add pixel area ratio
                    pair['ratio'] = sampleii['area'] / sample['area']

                    # add record
                    pairs.append(pair)

                # append to reservoir
                reservoir.append(pairs)

            # sort by distance
            reservoir.sort(key=lambda record: record[0]['metric'])
            self.reservoir = reservoir
            for record in reservoir[:5]:

                # print
                self._look(record, 2)

            # prune reservoir by criterion
            reservoir = [record for record in reservoir if record[0]['metric'] <= criterion]

            # add new reservoir entries
            name = '../studies/heimdal/records/reservoir_{}m{}.json'.format(year, str(month).zfill(2))
            stash = self._load(name) or []

            # add to the reservor
            accumulation = stash + reservoir

            # remove duplicates with dictionary
            prune = {tuple(record[0]['omi']['vector'] + record[0]['omps']['vector']): record for record in accumulation}
            records = list(prune.values())
            records.sort(key=lambda record: record[0]['metric'])

            # dump records
            self._dump(records, name)

        return None

    def plant(self, criterion=0.02, trees=500, depth=10, sensor='omi', relative=False, reflectivity=0.7):
        """Plant a random forest regressor for delta pressure.

        Arguments:
            criterion: float, maximum kdtree metric score
            trees: int, number of trees in the foreest
            depth: int, maximum depth of trees
            sensor: str, name of sensor for predictor information
            relative: boolean, use relative delp ( normalized by pressure reading )

        Returns:
            None
        """

        # timestamp
        self._stamp('assembling samples...', initial=True)

        # gather all json files
        paths = [path for path in self._see('../studies/heimdal/records') if '.json' in path]

        # set relativity based on relative pressures or absolute pressures
        relativity = 'relative ' if relative else ''

        # for each path
        records = []
        for path in paths:

            # accumulate records
            records += self._load(path)

        # only keep those meeting criterion
        records = [record for record in records if record[0]['metric'] <= criterion]

        # only keep those with high reflectivity
        records = [record for record in records if record[0]['omi']['reflectivity'] >= reflectivity]
        records = [record for record in records if record[0]['omps']['reflectivity'] >= reflectivity]

        # only keep those with latitude between +/- 40
        records = [record for record in records if -40 <= record[0]['omi']['latitude'] <= 40]

        # only keep those with omi row between 5 and 20
        records = [record for record in records if 4 <= record[0]['omi']['row'] <= 20]

        # sort by highest absolute pressure
        records.sort(key=lambda record: abs(record[0]['deltas']['pressure ( hPa )']), reverse=True)

        # create histogram of deltapressures
        pressures = [record[0]['deltas']['pressure ( hPa )'] for record in records]

        # add file for histograms
        route = ['Categories', 'delta_pressure_histogram']
        array = numpy.array(pressures)
        feature = Feature(route, array)
        destination = '../studies/heimdal/histograms/histogram_high_{}.h5'.format(reflectivity)
        self.stash([feature], destination, 'Data')

        # gather vectors from particular fields
        fields = ['reflectivity', 'fraction', 'terrain', 'viewing', 'zenith', 'azimuth']
        fields += ['surface', 'chlorophyll', 'latitude', 'longitude', 'filling', 'flux']

        # add more descriptive names
        names = ['reflectivity', 'cloud_fraction', 'terrain_height', 'viewing_zenith_angle', 'solar_zenith_angle']
        names += ['relative_azimuth_angle', 'surface_reflectivity', 'chlorophyll']
        names += ['latitude', 'longitude', 'filling_in', 'dIdR']
        names = {field: name for field, name in zip(fields, names)}

        # create vector of attributes from omi data
        vectors = []
        for field in fields:

            # go through each sensor
            instances = []
            for sensor in ('omi', 'omps'):

                # grab all data
                vector = numpy.array([record[0][sensor][field] for record in records])

                # get median of all non fill values
                mask = vector != -9999
                median = numpy.percentile(vector[mask], 50)

                # fill in the vector
                vector = numpy.where(mask, vector, median)

                # add to vectors
                instances.append(vector)

            # create average and add to vector
            average = numpy.array(instances).mean(axis=0)
            vectors.append(average)

            # create difference and add to vector
            difference = numpy.array(instances[1] - instances[0])
            vectors.append(difference)

        # add vector of pixel area ratios
        ratios = numpy.array([record[0]['ratio'] for record in records])
        vectors.append(ratios)
        names['ratio'] = 'pixel_area_ratio'

        # add vector of average cloud pressure
        pressures = [(record[0]['omi']['pressure'] + record[0]['omps']['pressure']) / 2for record in records]
        pressures = numpy.array(pressures)
        vectors.append(pressures)
        names['average'] = 'cloud_pressure_avg'

        # create features
        features = []
        aliases = [('{}_avg'.format(field), '{}_diff'.format(field)) for field in fields]
        aliases = [entry for pair in aliases for entry in pair]
        for field, vector in zip(aliases + ['ratio', 'average'], vectors):

            # create feature
            feature = Feature(['Categories', field], vector)
            features.append(feature)

        # grab targets as del pressure, and add to end
        def relating(record): return record[0][sensor]['pressure'] if relative else 1
        def pressurizing(record): return record[0]['deltas']['pressure ( hPa )'] / relating(record)
        vector = numpy.array([pressurizing(record) for record in records])
        vectors.append(vector)

        # add as independent variable
        feature = Feature(['IndependentVariables', 'delta_pressure_fraction'], vector)
        features.append(feature)

        # destination
        formats = (reflectivity, relativity, self._note())
        destination = '../studies/heimdal/scatters/Cloud_Pressure_Scatter_{}_{}avg_{}.h5'.format(*formats)
        self.stash(features, destination, 'Data')

        # transpose and shuffle
        transpose = [entry for entry in zip(*vectors)]
        transpose.sort(key=lambda entry: numpy.random.rand())

        # transpose back and split
        fraction = 0.8
        vectors = transpose
        index = int(fraction * len(vectors))
        train = numpy.array(vectors[:index])
        test = numpy.array(vectors[index:])

        # set up training matrix and targets
        matrix = train[:, :-1]
        targets = train[:, -1]

        # create the random forest
        self._stamp('planting forest...')
        forest = RandomForestRegressor(max_depth=depth, n_estimators=trees)
        forest.fit(matrix, targets)

        # begin report
        report = ['predicting OMCLDRR and NMCLDRR {}cloud pressure differences'.format(relativity)]
        report += ['via Random Forest ( {} trees, depth {} )'.format(trees, depth)]
        report.append(' ')

        # add statistics
        seconds = max([record[0]['deltas']['time ( sec )'] for record in records])
        latitude = max([record[0]['deltas']['latitude ( km )'] for record in records])
        longitude = max([record[0]['deltas']['longitude ( km )'] for record in records])
        start = min([record[0][sensor]['date'] for record in records])
        finish = max([record[0][sensor]['date'] for record in records])
        report.append('{} total concurrent samples ( +/- 40 degrees latitude )'.format(len(vectors)))
        report.append('from {} to {}'.format(start, finish))
        report.append('max time separation: {} seconds'.format(seconds))
        report.append('max latitude separation: {} km'.format(latitude))
        report.append('max longitude separation: {} km'.format(longitude))
        report.append(' ')

        # check the score
        report.append('verifying {} training samples..'.format(len(matrix)))
        score = forest.score(matrix, targets)
        report.append('training sample R^2: {}'.format(score))
        report.append(' ')

        # check the test set
        holdouts = test[:, :-1]
        truths = test[:, -1]
        report.append('validating {} test samples...'.format(len(holdouts)))
        score = forest.score(holdouts, truths)
        report.append('test sample R^2: {}'.format(score))
        report.append(' ')

        # get the feature importances
        report.append('feature importances:')
        importances = forest.feature_importances_
        zipper = list(zip(aliases + ['ratio', 'average'], importances))
        zipper.sort(key=lambda pair: pair[1], reverse=True)

        # print importances
        for field, importance in zipper:

            # print
            report.append('{}: {}'.format(field.replace(field.split('_')[0], names[field.split('_')[0]]), importance))

        # jot down report
        self._jot(report, '../studies/heimdal/reports/importances_for_cloud_pressure_{}.txt'.format(reflectivity))

        # print all records
        summary = ['Summary of concurrent measurements, OMI and OMPS']
        summary.append(' ')

        # for each record
        for index, record in enumerate(records):

            # print stats
            summary.append('{})'.format(index))
            summary.append('kdtree metric: {}'.format(record[0]['metric']))
            summary.append('OMI path: {}'. format(record[0]['omi']['path']))
            summary.append('OMPS path: {}'. format(record[0]['omps']['path']))
            summary.append('OMI Date: {}'.format(record[0]['omi']['date']))
            summary.append('OMI latitude: {}'.format(record[0]['omi']['latitude']))
            summary.append('OMI longitude: {}'.format(record[0]['omi']['longitude']))
            summary.append('OMI Row: {}'.format(record[0]['omi']['row']))
            summary.append('OMPS Row: {}'.format(record[0]['omps']['row']))
            summary.append('OMI Track: {}'.format(record[0]['omi']['track']))
            summary.append('OMPS Track: {}'.format(record[0]['omps']['track']))
            summary.append('OMI Reflectivity: {}'.format(record[0]['omi']['reflectivity']))
            summary.append('OMPS Reflectivity: {}'.format(record[0]['omps']['reflectivity']))
            summary.append('time difference: {} seconds'.format(record[0]['deltas']['time ( sec )']))
            summary.append('latitude difference: {} km'.format(record[0]['deltas']['latitude ( km )']))
            summary.append('longitude difference: {} km'.format(record[0]['deltas']['longitude ( km )']))
            summary.append('Pixel Area Ratio: {}'.format(record[0]['ratio']))
            summary.append('OMI Cloud Pressure: {}'.format(record[0]['omi']['pressure']))
            summary.append('OMPS Cloud Pressure: {}'.format(record[0]['omps']['pressure']))
            summary.append('Delta Cloud Pressure: {}'.format(record[0]['deltas']['pressure ( hPa )']))
            summary.append(' ')

        # jot down summary
        destination = '../studies/heimdal/reports/OMI_OMPS_concurrent_pairs_{}.txt'.format(reflectivity)
        self._jot(summary, destination)

        # create csv
        headers = ['kdtree_metric', 'date', 'delta_pressure ( hPa )', 'delta_time ( sec )']
        headers += ['delta_latitude ( km )', 'delta_longitude ( km )', 'pixel_area_ratio']
        headers += ['omps_row', 'omi_row', 'omps_path', 'omi_path', 'omp_cloud_pressure', 'omi_cloud_pressure']
        headers += ['{}_{}'.format(sensor, names[field]) for field in fields for sensor in ('omps', 'omi')]
        rows = [headers]
        for record in records:

            # construct row
            row = [record[0]['metric'], record[0]['omi']['date'], record[0]['deltas']['pressure ( hPa )']]
            row += [record[0]['deltas']['time ( sec )'], record[0]['deltas']['latitude ( km )']]
            row += [record[0]['deltas']['longitude ( km )'], record[0]['ratio']]
            row += [record[0]['omps']['row'], record[0]['omi']['row']]
            row += [record[0]['omps']['path'], record[0]['omi']['path']]
            row += [record[0]['omps']['pressure'], record[0]['omi']['pressure']]

            # add data
            row += [record[0][sensor][field] for field in fields for sensor in ('omps', 'omi')]

            # add row
            rows += [row]

        # create csv
        destination = '../studies/heimdal/reports/OMI_OMPS_concurrent_pairs_{}.csv'.format(reflectivity)
        self._table(rows, destination)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 4
    arguments = arguments[:4]

    # umpack arguments
    year, month, start, finish = arguments

    # create heimdal instance
    heimdal = Heimdal()
    heimdal.peer(year, month, start, finish)

