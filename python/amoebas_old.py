#!/usr/bin/env python3

# amoebas.py for the Amoeba class for ml studies on ozone profiles and uv spectra

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import regex
import re

# import numpy functions
import numpy

# import dates
import datetime

# # import other math
# import math
# import scipy

# import scikit
import sklearn
from sklearn.linear_model import LinearRegression

# # import pickle for pickling models
# import pickle
#
# # import datetime
# import datetime

# import matplotlib for plots
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
from matplotlib import ticker
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False


# class Amoeba to do OMI data reduction
class Amoeba(Hydra):
    """Amoeba class reconstructing col4, col3 flagging files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, sink diectory
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # set sink directory
        self.sink = sink

        # add squid for plotting
        self.squid = Squid(sink)

        # set diffuser info
        self.diffusers = {8: 'vol', 18: 'reg', 30: 'bck'}

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Amoeba instance at {} >'.format(self.sink)

        return representation

    def _calculate(self, coefficients, references, pixels):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            pixels: int, number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        # get relevant dimensions
        orbits, rows, degree = coefficients.shape
        print('orbits: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))
        self._stamp('calculating wavelengths...', initial=True)

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns] * rows)
        indices = numpy.array([image] * orbits)

        # expand tensor of column references
        references = numpy.array([references] * rows)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(degree)])
        powers = powers.transpose(1, 2, 3, 0)

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)
        coefficients = coefficients.transpose(1, 2, 0, 3)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=3)

        # timestamp
        self._stamp('calculated.')

        return wavelengths

    def _interpolate(self, tensor, grid, wave):
        """Interpolate a time series with associated wavelength grid to a single wavelength.

        Arguments:
            tensor: numpy array of data ( 3-d )
            grid: numpy array of wavelengths ( 3-d )
            wave: float, the wavelength to interpolate

        Returns:
            numpy array
        """

        # create lesser and greater matrices
        lesser = grid < wave
        greater = grid > wave

        # create left and right mask
        left = lesser & numpy.roll(greater, -1, axis=2)
        right = numpy.roll(lesser, 1, axis=2) & greater

        # create horizontal brackets
        horizontal = (grid * left.astype(int)).sum(axis=2)
        horizontalii = (grid * right.astype(int)).sum(axis=2)

        # create vertical brackets
        vertical = (tensor * left.astype(int)).sum(axis=2)
        verticalii = (tensor * right.astype(int)).sum(axis=2)

        # perform interpolation
        slope = (verticalii - vertical) / (horizontalii - horizontal)
        interpolation = vertical + slope * (wave - horizontal)

        return interpolation

    def _measure(self, array, time, start):
        """Measure and create the trendline, assuming time as millisecond timestamp.

        Arguments:
            array: numpy array, array to trend
            time: numpy array, time abscissa in milliseconds
            start: float, start of trend in years

        Returns:
            ( numpy array, numpy array, float ) tuple ( adjusted data, trendline, slope )
        """

        # connstruct weight
        mask = (numpy.isfinite(array)) & (array > 0) & (abs(array) < 1e20)
        weight = numpy.where(mask, 1, 0)
        array = numpy.where(mask, array, 0)

        # perform regression
        machine = LinearRegression(fit_intercept=True)
        machine.fit(time.reshape(-1, 1), array, sample_weight=weight.squeeze())

        # get slope and intercept
        slope = float(machine.coef_[0])
        intercept = float(machine.intercept_)

        # convert start to milliseconds and get anchor value
        milliseconds = datetime.datetime(int(start), 1, 1).timestamp() * 1000
        anchor = intercept + slope * milliseconds

        # create trendlinr
        trend = (intercept + slope * time).squeeze()

        # convert rate to percent / year
        rate = 100 * (slope / anchor) * (1000 * 60 * 60 * 24 * 365)

        return trend, rate

    def _polymerize(self, latitude, longitude):
        """Construct the corners of polygons from latitude and longitude coordinates.

        Arguments:
            latitude: numpy array
            longitude: numpy array

        Returns:
            dict of numpy arrays, the corner points
        """

        # get main shape
        shape = latitude.shape

        # initialize all four corners, with one layer for latitude and one for longitude
        northwest = numpy.zeros((*shape, 2))
        northeast = numpy.zeros((*shape, 2))
        southwest = numpy.zeros((*shape, 2))
        southeast = numpy.zeros((*shape, 2))

        # create latitude frame, with one more row and image on each side
        frame = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frame[1: shape[0] + 1, 1: shape[1] + 1] = latitude

        # extend sides of frame by extrapolation
        frame[1: -1, 0] = 2 * latitude[:, 0] - latitude[:, 1]
        frame[1: -1, -1] = 2 * latitude[:, -1] - latitude[:, -2]
        frame[0, 1:-1] = 2 * latitude[0, :] - latitude[1, :]
        frame[-1, 1:-1] = 2 * latitude[-1, :] - latitude[-2, :]

        # extend corners
        frame[0, 0] = 2 * frame[1, 1] - frame[2, 2]
        frame[0, -1] = 2 * frame[1, -2] - frame[2, -3]
        frame[-1, 0] = 2 * frame[-2, 1] - frame[-3, 2]
        frame[-1, -1] = 2 * frame[-2, -2] - frame[-3, -3]

        # create longitude frame, with one more row and image on each side
        frameii = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frameii[1: shape[0] + 1, 1: shape[1] + 1] = longitude

        # extend sides of frame by extrapolation
        frameii[1: -1, 0] = 2 * longitude[:, 0] - longitude[:, 1]
        frameii[1: -1, -1] = 2 * longitude[:, -1] - longitude[:, -2]
        frameii[0, 1:-1] = 2 * longitude[0, :] - longitude[1, :]
        frameii[-1, 1:-1] = 2 * longitude[-1, :] - longitude[-2, :]

        # extend corners
        frameii[0, 0] = 2 * frameii[1, 1] - frameii[2, 2]
        frameii[0, -1] = 2 * frameii[1, -2] - frameii[2, -3]
        frameii[-1, 0] = 2 * frameii[-2, 1] - frameii[-3, 2]
        frameii[-1, -1] = 2 * frameii[-2, -2] - frameii[-3, -3]

        # populate interior polygon corners image by image
        for image in range(0, shape[0]):

            # and row by row
            for row in range(0, shape[1]):

                # frame indices are off by 1
                imageii = image + 1
                rowii = row + 1

                # by averaging latitude frame at diagonals
                northwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii + 1]) / 2

                # and by averaging longitude longitudes at diagonals
                northwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii + 1]) / 2

        # fix longitude edge cases, by looking for crisscrossing, and truncating
        northwest[:, :, 1] = numpy.where(northwest[:, :, 1] < longitude, northwest[:, :, 1], longitude)
        northeast[:, :, 1] = numpy.where(northeast[:, :, 1] > longitude, northeast[:, :, 1], longitude)
        southwest[:, :, 1] = numpy.where(southwest[:, :, 1] < longitude, southwest[:, :, 1], longitude)
        southeast[:, :, 1] = numpy.where(southeast[:, :, 1] > longitude, southeast[:, :, 1], longitude)

        # average all overlapping corners
        overlap = southeast[1:, :-1, :] + southwest[1:, 1:, :] + northwest[:-1, 1:, :] + northeast[:-1, :-1, :]
        average = overlap / 4

        # transfer average
        southeast[1:, :-1, :] = average
        southwest[1:, 1:, :] = average
        northwest[:-1, 1:, :] = average
        northeast[:-1, :-1, :] = average

        # average western edge
        average = (southwest[1:, 0, :] + northwest[:-1, 0, :]) / 2
        southwest[1:, 0, :] = average
        northwest[:-1, 0, :] = average

        # average eastern edge
        average = (southeast[1:, -1, :] + northeast[:-1, -1, :]) / 2
        southeast[1:, -1, :] = average
        northeast[:-1, -1, :] = average

        # average northern edge
        average = (northwest[-1, 1:, :] + northeast[-1, :-1, :]) / 2
        northwest[-1, 1:, :] = average
        northeast[-1, :-1, :] = average

        # average southern edge
        average = (southwest[0, 1:, :] + southeast[0, :-1, :]) / 2
        southwest[0, 1:, :] = average
        southeast[0, :-1, :] = average

        # collect corners
        corners = {'northwest': northwest, 'northeast': northeast}
        corners.update({'southwest': southwest, 'southeast': southeast})

        return corners

    def _regress(self, matrix, time):
        """Perform polynomial regression for a set of data.

        Arguments:
            matrix: numpy array, y-axis coordinates
            time: list of floats, x-axis coordinates

        Returns:
            regression object
        """

        # construct weights
        weights = ((matrix > 0) & (matrix < 1000)).astype(int)

        # begin slopes and intercepts
        slopes = numpy.zeros(matrix.shape[1:])
        intercepts = numpy.zeros(matrix.shape[1:])

        # for each row
        for row in range(matrix.shape[1]):

            # and each wavelength
            for wavelength in range(matrix.shape[2]):

                # get appropriate weight, if summing to zero
                weight = weights[:, row, wavelength]
                if weight.sum() == 0:

                    # change weights to ones
                    weight = numpy.array([1.0] * weight.shape[0])

                # perform regression
                machine = LinearRegression(fit_intercept=True)
                machine.fit(time.reshape(-1, 1), matrix[:, row, wavelength], sample_weight=weight)

                # get slope and intercept
                slopes[row, wavelength] = machine.coef_[0]
                intercepts[row, wavelength] = machine.intercept_

        return slopes, intercepts

    def _shank(self, irradiances, initial, margin, marginii):
        """Prepare the irradiance ratios, using a subset and filling in negative (fill) values.

        Arguments:
            irradiances: numpy array, irradiance time series (time x row x pixel)
            initial: int, index of initial timepoint at which to normalize
            margin: int, number of spectral pixels to disregard at the edge
            marginii: int, number of rows on each edge to disregard

        Returns:
            numpy array
        """

        # clip 20 spectral pixels from each end
        irradiances = irradiances[:, marginii:-marginii or irradiances.shape[1], margin:-margin or irradiances.shape[2]]

        # create normalization
        normalization = irradiances / irradiances[initial]

        # begin timeline
        timeline = []
        for matrix in normalization:

            # remove outliers
            mask = (matrix > 0) & (matrix < 1000)

            # construct average
            average = numpy.isfinite(matrix[mask]).mean()
            timeline.append(average)

        # construdt array
        array = numpy.array(timeline)
        #     # add to timeline
        #     timeline.append(matrix)
        #
        #
        #
        # self._print(normalization.min(), normalization.max())
        #
        # # patch negative values with column average
        # mask = (irradiances > 0) & (irradiances < 1000)
        # normalization = normalization[mask].mean(axis=1).mean(axis=1)

        return array

    def _splay(self, tree, stub='', branches=None):
        """Splay nested tree into branches.

        Arguments:
            tree: dict
            stub: growing member

        Returns:
            list of str
        """

        # set default
        branches = branches or []

        # try to
        try:

            # go through each key
            for field, twig in tree.items():

                # add each branch
                growth = '{}/{}'.format(stub, field)
                branches += self._splay(twig, growth)

        # unless a nod
        except AttributeError:

            # add to branch
            growth = '{}/{}'.format(stub, tree)
            branches += [growth]

        return branches

    def animalize(self):
        """Create trending records from anomaly files.

        Arguments:
            None

        Returns:
            None
        """

        # grab the zonal means trending file
        hydra = Hydra('../studies/amoeba/anomaly/zones/mission')
        hydra.ingest(0)

        # get the dqys file
        days = hydra._load('../studies/amoeba/anomaly/zones/days/OMTO3dRAZM_days.json')

        # get all entrues
        months = list(days.items())
        dates = ['{}{}'.format(month, self._pad(day + 1)) for month, entry in months for day, _ in enumerate(entry)]
        dates.sort()

        # set fill value
        fill = -999

        # round to rough orbit number
        orbits = [int(index * 5300 / 365) for index, _ in enumerate(dates)]

        # convert to datetimes
        dates = [datetime.datetime(int(date[:4]), int(date[5:7]), int(date[7:9])) for date in dates]
        dates = [date.timestamp() for date in dates]
        dates = [date * 1000 for date in dates]

        # beign collecting data
        data = {}
        attributes = {}

        # add time feature, with extra dimension for compatibility
        address = 'IndependentVariables/orbit_start_time_fr_yr'
        data[address] = numpy.array(dates).reshape(-1, 1)

        # create attribute dictionary
        attribute = {'FillValue': fill, 'LongName': 'time in milliseconds', 'Units': 'ms'}
        attribute.update({'Status': True, 'doNotPlot': True})
        attribute.update({'PlotLegend': ['time'], 'AbscissaVariableSelection': [address]})
        attributes[address] = attribute

        # add time feature, with extra dimensionn for compatibility
        address = 'IndependentVariables/orbit_number'
        data[address] = numpy.array(orbits).reshape(-1, 1)

        # create attribute dictionary
        attribute = {'FillValue': fill, 'LongName': 'orbit number', 'Units': 'number'}
        attribute.update({'Status': True, 'doNotPlot': True})
        attribute.update({'PlotLegend': ['number'], 'AbscissaVariableSelection': [address]})
        attributes[address] = attribute

        # grab the 331 residue data
        sums = hydra.grab('ColumnAmountO3/Sums All R331')
        points = hydra.grab('Npts All')

        # for each block of 6 latitudes
        for latitude in range(6):

            # create latitude bracket
            latitudes = [latitude * 6, 6 + latitude * 6]
            latitudesii = [(latitude * 5) - 90 for latitude in latitudes]
            degrees = []
            for latitude in latitudesii:

                # check for negative
                if latitude < 0:

                    # add S
                    latitude = '{}S'.format(int(abs(latitude)))
                    degrees.append(latitude)

                # otherwse
                else:

                    # ads N
                    latitude = '{}N'.format(int(abs(latitude)))
                    degrees.append(latitude)

            # and each block of 10 rows
            for row in range(6):

                # create rows
                rows = [row * 10, 10 + row * 10]
                legend = ['Row {}'.format(row + 1) for row in range(*rows)]

                # create the address
                formats = [self._pad(coordinate) for coordinate in degrees]
                formats += [self._pad(rows[0] + 1), self._pad(rows[1])]
                address = 'Row_Anomaly/Column_O3_331/column_o3_331_latitude_{}_to_{}_rows_{}_to_{}'.format(*formats)

                # create average
                south, north = latitudes
                residue = sums[:, south: north].sum(axis=1) / points[:, south: north].sum(axis=1)
                array = residue[:, rows[0]: rows[1]]
                array = numpy.where(numpy.isfinite(array), array, fill)
                data[address] = array

                # create attribute dictionary
                attribute = {'FillValue': fill, 'LongName': 'column O3 331nm for row anomaly', 'Units': 'DU'}
                attribute.update({'Status': True, 'doNotPlot': False})
                attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [address]})
                attributes[address] = attribute

        # create file
        self._make('../studies/amoeba/trends')
        destination = '../studies/amoeba/trends/Row_Anomaly_Zonal_Trends.h5'
        hydra.spawn(destination, data, attributes)

        return None

    def agglomerate(self, *words, start=2023):
        """Create latest trending files from individual collections.

        Arguments:
            *words: unpacked list of particular merges to perform
            start: int, year to begin at

        Returns:
            None
        """

        # if no words
        if len(words) < 1:

            # assume all
            words = ('zones', 'light', 'radiance', 'irradiance')

        # get current year
        current = datetime.datetime.now().year

        # if mergine radiance zones
        if 'zones' in words:

            # get hydra for radiance trending orbits
            hydra = Hydra('../studies/radiobe/orbits/radiance')

            # for each year
            for year in range(start, current + 1):

                # collect all paths for the year
                paths = [path for path in hydra.paths if '{}m'.format(year) in path]
                paths.sort()

                # make destination file
                destination = '../studies/radiobe/years/radiance/OML1BCAL_Radiance_{}.h5'.format(year)

                # merge paths
                hydra.merge(paths, destination)

            # get hydra for radiance trending years
            hydraii = Hydra('../studies/radiobe/years/radiance')

            # sort paths
            paths = hydraii.paths
            paths.sort()

            # make destination
            destinationii = '../studies/radiobe/mission/radiance/OML1BCAL_Radiance_mission.h5'

            # merge paths
            hydraii.merge(paths, destinationii)

        # if mergine calibration files
        if 'light' in words:

            # set tags
            tags = {'dark': 'Dark_Current', 'light': 'LED', 'white': 'WLS'}

            # for each mode
            for mode in ('dark', 'light', 'white'):

                # get hydra for radiance trending orbits
                hydra = Hydra('../studies/caladrius/orbits/{}'.format(mode))

                # for each year
                for year in range(start, current + 1):

                    # collect all paths for the year
                    paths = [path for path in hydra.paths if '{}m'.format(year) in path]
                    paths.sort()

                    # make destination file
                    destination = '../studies/caladrius/years/{}/OML1BCAL_{}_{}.h5'.format(mode, tags[mode], year)

                    # if there are paths
                    if len(paths) > 0:

                        # merge paths
                        hydra.merge(paths, destination)

                # get hydra for radiance trending years
                hydraii = Hydra('../studies/caladrius/years/{}'.format(mode))

                # sort paths
                paths = hydraii.paths
                paths.sort()

                # make destination
                destinationii = '../studies/caladrius/mission/{}/OML1BCAL_{}_mission.h5'.format(mode, tags[mode])

                # merge paths
                hydraii.merge(paths, destinationii)

        # if merging radiance trends
        if 'radiance' in words:

            # try to:
            try:

                # copy current mission file to prior
                hydra = Hydra('../studies/amoeba/radiance/mission')
                hydra._move(hydra.paths[0], '../studies/amoeba/radiance/prior')

            # unless already done
            except IndexError:

                # in which case, nevermind
                pass

            # begin radiance collection
            collection = []
            for year in range(2005, 2025):

                # create hydra for radiance
                hydra = Hydra('/tis/acps/OMI/70004/OML1BRADHISy/{}/01'.format(year))
                collection.append(hydra.paths[0])

            # get monnthlies from AWS 10004
            for month in range(2, 5):

                # create hydra for radiance
                hydra = Hydra('/tis/acps/OMI/10004/OML1BRADHISm/2024/{}'.format(self._pad(month)))
                collection.append(hydra.paths[0])

            # create destination
            # collection.sort()
            destination = '{}/radiance/years/OML1BRADHISy_2005_2023.h5'.format(self.sink)
            hydra.merge(collection, destination)

            # make hydra for prior file
            hydra = Hydra('../studies/amoeba/radiance/years')

            # grab orbit number and date
            orbit = hydra._stage(collection[-1])['orbit']
            date = hydra._stage(collection[-1])['day']

            # constuct start date
            start = datetime.datetime(int(date[:4]), int(date[5:7]), int(date[7:9]))

            # get current date
            current = datetime.datetime.now()

            # begin paths reservor
            paths = []

            # for each year between last update and current
            while (start < current):

                # create hydra
                formats = (start.year, self._pad(start.month), self._pad(start.day))
                hydraii = Hydra('/tis/acps/OMI/10004/OML1BRADHIS/{}/{}/{}'.format(*formats))

                # add to paths
                paths += hydraii.paths

                # increment
                start = start + datetime.timedelta(days=1)

            # only keep those with a greater oribit, and sort
            paths = [path for path in paths if self._stage(path)['orbit'] > orbit]
            paths.sort()

            # create destination
            folder = '../studies/amoeba/radiance/mission'
            orbitii = self._stage(paths[-1])['orbit']
            dateii = self._stage(paths[-1])['date']
            formats = [current.year, self._pad(current.month), self._pad(current.day), self._pad(current.hour)]
            formats += [self._pad(current.minute), self._pad(current.second)]
            production = '{}m{}{}t{}{}{}'.format(*formats)
            name = 'OMI-Aura_L3-OML1BRADHISy_{}-o{}_v134-{}.h5'.format(dateii, orbitii, production)
            destination = '{}/{}'.format(folder, name)

            # break up by day
            days = self._group(paths, lambda path: self._stage(path)['day'])

            # set tempoary file sink
            sink = '../studies/amoeba/radiance/temporary'

            # for each month
            for day, members in days.items():

                # sort members
                members.sort()

                # create temporary desination and merage all new singlets
                temporary = '{}/Temporary_{}.h5'.format(sink, day)
                self.merge(members, temporary, lead=True)

            # grab all temporary paths
            temporaries = self._see(sink)
            temporaries.sort()

            # merge with prior
            self.merge([hydra.paths[0]] + temporaries, destination)

            # delete temporary files
            self._clean(sink, force=True)

        # if merging irradiance zones
        if 'irradiance' in words:

            # try to:
            try:

                # copy current mission file to prior
                hydra = Hydra('../studies/amoeba/irradiance/mission')
                hydra._move(hydra.paths[0], '../studies/amoeba/irradiance/prior')

            # unless already done
            except IndexError:

                # in which case, nevermind
                pass

            # get current date
            current = datetime.datetime.now()

            # make hydra for prior file
            hydra = Hydra('../studies/amoeba/irradiance/prior')

            # grab orbit number and date
            year = hydra._stage(hydra.paths[-1])['year']

            # # get yearlies
            # hydraii = Hydra('../studies/amoeba/irradiance/years')
            # paths = hydraii.paths
            # paths.sort()

            # get yearly files from acps, AS 70004
            fluxes = []
            for year in range(2005, 2024):

                # make hydra
                hydraii = Hydra('/tis/acps/OMI/70004/OMRAWFLUXy/{}/01'.format(year))
                fluxes.append(hydraii.paths[0])

            # get yearly files from acps, AS 10004
            for year in range(2024, 2025):

                # make hydra
                hydraii = Hydra('/tis/acps/OMI/10004/OMRAWFLUXy/{}/01'.format(year))
                fluxes.append(hydraii.paths[0])

            # get orbit year from years
            # fluxes.sort()
            hydraii.ingest(fluxes[-1])
            years = hydraii.grab('vol/orbit_start_yr')

            # if yaer is the same as current year
            if int(years[0]) == int(year):

                # delete file
                hydraii._clean(paths[-1], force=True)

            # begin orbital paths
            paths = []

            # for each year
            for year in range(2024, 2025):

                # for each month
                for month in range(1, 13):

                    # and each day
                    for day in range(1, 32):

                        # create hydra
                        formats = (year, self._pad(month), self._pad(day))
                        hydraiii = Hydra('/tis/acps/OMI/10004/OMRAWFLUX/{}/{}/{}'.format(*formats))

                        # add to paths
                        paths += hydraiii.paths

            # sort paths and create destination
            folder = '../studies/amoeba/irradiance/years'
            dateiii = self._stage(paths[-1])['date']
            orbitiii = self._stage(paths[-1])['orbit']
            formats = [current.year, self._pad(current.month), self._pad(current.day), self._pad(current.hour)]
            formats += [self._pad(current.minute), self._pad(current.second)]
            production = '{}m{}{}t{}{}{}'.format(*formats)
            name = 'OMI-Aura_L3-OMRAWFLUXy_{}-o{}_v134-{}.h5'.format(dateiii, orbitiii, production)
            destination = '{}/{}'.format(folder, name)
            fluxes += [destination]

            # create year file
            self.devour(paths, '', destination)

            # renew yearlies
            hydraii.renew()

            # create new destination
            folderii = '../studies/amoeba/irradiance/mission'
            formats = (dateiii, orbitiii, production)
            nameii = 'OMI-Aura_L3-OMRAWFLUXs_{}-o{}_v134-{}.h5'.format(*formats)
            destinationii = '{}/{}'.format(folderii, nameii)

            # sort paths
            # paths = hydraii.paths
            # paths.sort()
            self._tell(fluxes)

            # merge
            self.merge(fluxes, destinationii)

        return None

    def construct(self, year, month, combine=False):
        """Construct the row anomaly zonal mean and 10-day zonal mean records by merging dailies.

        Arguments:
            year: int, year
            month: int, month
            combine: boolean, combine all months?

        Returns:
            None
        """

        # pad month
        month = self._pad(month)

        # set products
        products = {'zones': 'OMTO3dRAZM', 'tens': 'OMTO3dRAZM10'}

        # make directories
        modes = list(products.keys())
        folders = ('days', 'months', 'mission')
        [self._make('{}/{}'.format(self.sink, mode)) for mode in modes]
        [self._make('{}/{}/{}'.format(self.sink, mode, folder)) for mode in modes for folder in folders]

        # for each product
        for mode, product in products.items():

            # create hydra and sort paths
            hydra = Hydra('/tis/acps/OMI/10004/{}/{}/{}'.format(product, year, month))
            paths = hydra.paths
            paths.sort()

            # collect days and add to record
            days = [self._stage(path)['day'] for path in paths]

            # load file, add days, and dump
            storage = '{}/{}/days/{}_days.json'.format(self.sink, mode, product)
            record = self._load(storage)
            record['{}m{}'.format(year, month)] = days
            self._dump(record, storage)

            # create destinaion for month file
            formats = (self.sink, mode, product, year, month)
            destination = '{}/{}/months/OMI-Aura_Row_Anomaly_{}_{}m{}00t0000.h5'.format(*formats)

            # merge all paths, using leading dimension
            hydra.merge(paths, destination, lead=True)

            # if combining
            if combine:

                # create new hydra for monthly files and get paths
                hydraii = Hydra('{}/{}/months'.format(self.sink, mode))
                pathsii = hydraii.paths
                pathsii.sort()

                # create destination
                formats = (self.sink, mode, product)
                destinationii = '{}/{}/mission/OMI-Aura_Row_Anomaly_{}_mission.h5'.format(*formats)

                # merge paths
                hydraii.merge(pathsii, destinationii)

        return None

    def degrade(self):
        """Calculate degradation rates for diffusers and sensor.

        Arguments:
            None

        Returns:
            None
        """

        # make hydra
        hydra = Hydra('{}/irradiance/degradations'.format(self.sink))
        hydra.ingest(0)

        # begin features
        features = []

        #  for each path
        for band in ('band1', 'band2', 'band3'):

            # for each diffuser
            for diffuser in ('quartz', 'regular', 'backup'):

                # print status
                self._stamp('calculating degradation rates for {}, {}...'.format(band, diffuser), initial=True)

                # set address
                address = ['{}_diffuser'.format(diffuser), band]

                # pass on orbit number
                orbit = hydra.grab('{}/{}/orbit_number'.format(diffuser, band))
                features.append(Feature(address + ['orbit_number'], orbit))

                # pass on time_fr_year
                milliseconds = hydra.grab('{}/{}/orbit_start_time_fr_yr'.format(diffuser, band))
                features.append(Feature(address + ['orbit_start_time_fr_yr'], milliseconds))

                # create averaged wavelength grid
                wavelength = hydra.grab('{}_diffuser/{}/wavelength_grid'.format(diffuser, band))
                grid = wavelength.mean(axis=0).round(2)
                features.append(Feature(address + ['wavelength_grid'], wavelength))
                features.append(Feature(address + ['averaged_wavelength_grid'], grid))

                # transfer certain fields
                fields = ['year_fraction', 'percent_irradiance_from_2005', 'percent_degradation_rate']
                fields += ['total_exposure_time', 'trendline_year_bracket', 'trendline_percent_irradiance']
                fields += ['irradiance', 'irradiance_corrected', 'trendline_percent_corrected_irradiance']
                fields += ['percent_corrected_irradiance_from_2005', 'percent_corrected_degradation_rate']
                for field in fields:

                    # transfer percents and trendlines
                    array = hydra.grab('{}/{}/{}'.format(diffuser, band, field))
                    features.append(Feature(address + [field], array))

                # print status
                self._stamp('calculated.')

            # print status
            self._stamp('calculating sensor degradation rates, {}...'.format(band), initial=True)

            # calculate exposure time ratio
            exposure = hydra.grab('regular_diffuser/{}/total_exposure'.format(band))
            exposureii = hydra.grab('backup_diffuser/{}/total_exposure'.format(band))
            ratio = float(exposure / exposureii)
            features.append(Feature([band, 'aluminum_exposure_ratio'], numpy.array([ratio])))

            # grab the signal degradation rates
            rate = hydra.grab('regular_diffuser/{}/percent_degradation_rate'.format(band))
            rateii = hydra.grab('backup_diffuser/{}/percent_degradation_rate'.format(band))

            # calculate the regular diffuser degradation rate as rb = (dr - db) / (1 - x)
            backup = (rate - rateii) / (ratio - 1)
            features.append(Feature([band, 'backup_diffuser_degradation'], backup))

            # calculate the backup diffuser degradation rate as rr = rb * x
            regular = backup * ratio
            features.append(Feature([band, 'regular_diffuser_degradation'], regular))

            # calculate the sensor diffuser degradation rate as ri = dr - rr
            sensor = rate - regular
            features.append(Feature([band, 'sensor_degradation'], sensor))

            # calculate the quartz diffuser degradation rate as rq = dq - ri
            rateiii = hydra.grab('quartz_diffuser/{}/percent_degradation_rate'.format(band))
            quartz = rateiii - sensor
            features.append(Feature([band, 'quartz_diffuser_degradation'], quartz))

            # print status
            self._stamp('calculated.')

        # stash file
        destination = '{}/irradiance/calculations/Calculated_Diffuser_Degradation_Rates.h5'.format(self.sink)
        self.stash(features, destination)

        # create tree
        hydra = Hydra('{}/irradiance/calculations'.format(self.sink))
        hydra.ingest(0)
        hydra._grow(4, destination.replace('.h5', '_structure.txt'))

        return None

    def devour(self, paths, previous, destination, temporary='../tmp', size=200000):
        """Fuse all individual trending files into larger aggregates.

        Arguments:
            paths: list of single orbit paths to stack
            previous: str, file path of previous stack
            destination: str, file path of stacked file
            temporary: str, temporary directory
            size: minimum file size, in bytes

        Returns:
            None
        """

        # check file size for minimum size requirment ( weed out empty files )
        paths = [path for path in paths if self._ask(path)['st_size'] > size]
        paths.sort()

        # create reservoirs for each diffuser, as well as its attributes
        reservoir = {diffuser: {} for diffuser in self.diffusers.values()}
        attributes = {diffuser: {} for diffuser in self.diffusers.values()}

        # creae hydra
        hydra = Hydra()

        # print status
        self._print('combining {} orbits...'.format(len(paths)))

        # go through each path
        for path in paths:

            # ingest the path
            hydra.ingest(path)

            # for each diffuser
            for diffuser in self.diffusers.values():

                # check orbit number for other than 0
                orbit = hydra.dig('{}_diffuser/orbit_number'.format(diffuser))[0].distil()
                if orbit > 0:

                    # got through all features
                    features = hydra.dig('{}_diffuser'.format(diffuser))
                    for feature in features:

                        # create entry in reservoir if empty
                        arrays = reservoir[diffuser].setdefault(feature.slash, [])
                        array = feature.distil()
                        arrays.append(array)

                        # add attributes
                        attributes[diffuser][feature.slash] = feature.attributes

        # create new arrays
        features = []
        for diffuser in self.diffusers.values():

            # for each array
            for slash in reservoir[diffuser].keys():

                # vstack array
                array = numpy.vstack(reservoir[diffuser][slash]).squeeze()
                attribute = attributes[diffuser][slash]

                # add to features
                feature = Feature(slash.split('/'), array, attributes=attribute)
                features.append(feature)

        # stash file
        self.stash(features, destination)

        return None

    def diffuse(self, start=2005):
        """Calculate diffuser degradation rates from drifts in aggrergate rawflux files.

        Arguments:
            start: float, starting time in years

        Returns:
            None
        """

        # make hydra
        hydra = Hydra('{}/irradiance/mission'.format(self.sink))

        # set pixel attributes
        pixels = {'band1': 159, 'band2': 557, 'band3': 751}

        # ingest path
        hydra.ingest(0)

        # begin features
        features = []

        #  for each band and diffuser
        diffusers = {'vol': 'quartz', 'reg': 'regular', 'bck': 'backup'}
        for diffuser in diffusers.keys():

            # get alias
            alias = diffusers[diffuser]

            # for each band
            for band in ('band1', 'band2', 'band3'):

                # timestamp
                self._stamp('solving for degradation rates for {}, {}...'.format(diffuser, band), initial=True)

                # create address
                address = ['{}_diffuser'.format(alias), band]

                # pass on orbit number
                orbit = hydra.grab('{}/orbit_number'.format(diffuser))
                features.append(Feature(address + ['orbit_number'], orbit))

                # pass on time_fr_year
                milliseconds = hydra.grab('{}/orbit_start_time_fr_yr'.format(diffuser))
                features.append(Feature(address + ['orbit_start_time_fr_yr'], milliseconds))

                # calculate wavelength grid from coefficients
                coefficient = hydra.grab('{}/{}/wavelength_coefficient'.format(diffuser, band))
                reference = hydra.grab('{}/{}/wavelength_reference'.format(diffuser, band)).squeeze()
                wavelengths = self._calculate(coefficient, reference, pixels[band])
                features.append(Feature(address + ['wavelength_grid'], wavelengths))
                features.append(Feature(address + ['average_wavelength_grid'], wavelengths.mean(axis=0)))

                # calculate slopes vs time
                irradiance = hydra.grab('{}/{}/irradiance_after_relirr_avg'.format(diffuser, band))
                correction = hydra.grab('{}/{}/irradiance_avg'.format(diffuser, band))
                time = hydra.grab('{}/orbit_start_year_fraction'.format(diffuser))
                slope, intercept = self._regress(irradiance, time)
                slopeii, interceptii = self._regress(correction, time)
                features.append(Feature(address + ['degradation_slope'], slope))
                features.append(Feature(address + ['degradation_intercept'], intercept))
                features.append(Feature(address + ['irradiance'], irradiance))
                features.append(Feature(address + ['year_fraction'], time))
                features.append(Feature(address + ['correction_slope'], slope))
                features.append(Feature(address + ['correction_intercept'], intercept))
                features.append(Feature(address + ['irradiance_corrected'], correction))

                # calculate initial values at start time
                initial = intercept + slope * start
                features.append(Feature(address + ['irradiance_at_{}'.format(start)], initial))
                initialii = interceptii + slopeii * start
                features.append(Feature(address + ['corrected_irradiance_at_{}'.format(start)], initialii))

                # convert to percentage degradation rates
                percent = 100 * slope / initial
                features.append(Feature(address + ['percent_degradation_rate'], percent))
                percentii = 100 * slopeii / initialii
                features.append(Feature(address + ['percent_corrected_degradation_rate'], percentii))

                # convert irradiances to ratios
                ratios = 100 * irradiance / initial
                features.append(Feature(address + ['percent_irradiance_from_{}'.format(start)], ratios))
                ratiosii = 100 * correction / initialii
                features.append(Feature(address + ['percent_corrected_irradiance_from_{}'.format(start)], ratiosii))

                # make brackets for trend line
                bracket = numpy.array([time[0], time[-1]])
                trend = numpy.array([(100 * intercept / initial) + percent * entry for entry in bracket])
                trendii = numpy.array([(100 * interceptii / initialii) + percentii * entry for entry in bracket])
                features.append(Feature(address + ['trendline_year_bracket'], bracket))
                features.append(Feature(address + ['trendline_percent_irradiance'], trend))
                features.append(Feature(address + ['trendline_percent_corrected_irradiance'], trendii))

                # calculate total exposure
                track = hydra.grab('{}/orbit_track_length'.format(diffuser))
                exposure = hydra.grab('{}/{}/exposure_time_index_zero'.format(diffuser, band))
                total = numpy.array([((track * exposure / 3600)).sum()])
                features.append(Feature(address + ['total_exposure_time'], total))

        # finish
        self._stamp('finished.')

        # stash file
        destination = '{}/irradiance/degradations/Diffuser_Degradations.h5'.format(self.sink)
        self.stash(features, destination)

        # create tree
        hydra = Hydra('{}/irradiance/degradations'.format(self.sink))
        hydra.ingest(0)
        hydra._grow(4, destination.replace('.h5', '_structure.txt'))

        return None

    def digest(self, combinations=None):
        """Digest the degradations into plots.

        Arguments:
            combinations: list of (str, int, float) tuples, band, row, wavelength
        Returns:
            None
        """

        # make hydra
        hydra = Hydra('{}/irradiance/calculations'.format(self.sink))
        hydra.ingest(0)

        # make diffuser plots for certain wavelength, row, band combinations
        combinations = combinations or [('band1', 10, 270), ('band2', 20, 331), ('band3', 20, 400)]
        for band, row, wavelength in combinations:

            # for each diffusert
            for diffuser in ['quartz', 'regular', 'backup']:

                # find closest wavelength, using averaged quartz diffuser for grid
                grid = hydra.dig('quartz_diffuser/{}/averaged_wavelength_grid'.format(band))[0].distil()
                wave = ((grid[row, :] - wavelength) ** 2).argsort()[0]

                # grab percent and trendline
                slope = hydra.dig('{}_diffuser/{}/percent_degradation_rate'.format(diffuser, band))[0].distil()
                percent = hydra.dig('{}_diffuser/{}/percent_irradiance_from_2005'.format(diffuser, band))[0].distil()
                trend = hydra.dig('{}_diffuser/{}/trendline_percent_irradiance'.format(diffuser, band))[0].distil()
                time = hydra.dig('{}_diffuser/{}/year_fraction'.format(diffuser, band))[0].distil()
                bracket = hydra.dig('{}_diffuser/{}/trendline_year_bracket'.format(diffuser, band))[0].distil()

                # plot percent line
                name = 'percent_irradiance_change'
                formats = (diffuser, band, row, wavelength)
                title = '{} diffuser, percent irradiance change since 2005-01-01, {}, row: {}, {} nm'.format(*formats)
                destination = 'irradiance/plots/degradations/{}_{}_{}_{}_percent_degradation.h5'.format(*formats)
                self.squid.ink(name, percent[:, row, wave], 'year', time, destination, title)

                # plot trend line
                rate = round(slope[row, wave], 2)
                name = 'trend'.format(rate)
                formats = (diffuser, band, row, wavelength)
                title = '{} % / yr, {} diffuser, {}, row: {}, {} nm'.format(rate, *formats)
                destination = 'irradiance/plots/degradations/{}_{}_{}_{}_trend.h5'.format(*formats)
                self.squid.ink(name, trend[:, row, wave], 'year', bracket, destination, title)

            # for each diffusert
            for diffuser in ['quartz_diffuser', 'regular_diffuser', 'backup_diffuser', 'sensor']:

                # find closest wavelength
                grid = hydra.dig('quartz_diffuser/{}/averaged_wavelength_grid'.format(band))[0].distil()
                degradation = hydra.dig('{}/{}_degradation'.format(band, diffuser))[0].distil()

                # plot degradation vs wavelength
                name = '{}_degradation'.format(diffuser)
                title = '{} degradation rate, {}, row: {}'.format(rate, band, row)
                destination = 'irradiance/plots/degradations/{}_degradation_{}_{}.h5'.format(diffuser, band, row)
                self.squid.ink(name, degradation[row], 'wavelength', grid[row], destination, title)

        return None

    def examine(self, years=(2007, 2013)):
        """Examine the annual records for correct date bracket.

        Arguments:
            years: tuple of ints, the years to check

        Returns:
            None
        """

        # make folder
        folder = 'plots/year'
        self._make('{}/{}'.format(self.sink, folder))

        # for each year
        for year in range(*years):

            # create hydra
            hydra = Hydra('/tis/acps/OMI/70004/OML1BRADHISy/{}/01'.format(year))

            # ingest file on fifth
            hydra.ingest('_{}m0105'.format(year))

            # grab time, multiple to get milliseconds
            time = hydra.grab('orbit_start_time').squeeze()

            # grab zenith angle mean
            zenith = hydra.grab('BAND2/solar_zenith_angle')[:, 0]

            # plot
            title = 'Year {}, solar zenith angle'.format(year - 1)
            address = '{}/zenith_angle_year_{}.h5'.format(folder, year - 1)
            self.squid.ink('zenith', zenith, 'month', time, address, title)

        return None

    def extract(self):
        """Extract plots from trending files.

        Arguments:
            None

        Returns:
            None
        """

        # create folder for boulder presentation
        self._make('{}/boulder'.format(self.sink))

        # for each mode
        for mode in ('radiance/trends', 'irradiance/trends', 'highlights', 'light', 'zones'):

            # get radiance trending file
            hydra = Hydra('{}/{}'.format(self.sink, mode))

            # for each path
            for path in hydra.paths:

                # ingest
                hydra.ingest(path)

                # fill all arrays
                [feature.fill() for feature in hydra]

                # get independents and categories
                independents = [feature for feature in hydra if 'Independent' in feature.slash]
                categories = [feature for feature in hydra if 'Independent' not in feature.slash]

                # grab oribt_st_fy_yr
                independents = [feature for feature in independents if 'orbit_start_time_fr_yr' in feature.slash]

                # take only mean values for trending
                # [feature.instil(feature.data[:, :1]) for feature in categories]
                [feature.divert('Categories') for feature in categories]

                # create destination
                destination = hydra.current.replace('.h5', '_cat.h5').replace(mode, 'boulder')

                # stash data
                hydra.stash(independents + categories, destination)

        return None

    def feast(self, *steps):
        """Construct the set of trending files for ozoneaq website from source files.

        Arguments:
            steps: unpacked list of integers, the steps to perform

        Returns:
            None
        """

        # if no steps offereed
        if len(steps) < 1:

            # make them all
            steps = tuple(range(9))

        # make list of procedures
        procedures = [self.agglomerate, self.diffuse, self.degrade, self.digest, self.trend]
        procedures += [self.sieve, self.highlight, self.light, self.radiate]

        # for each step
        for step in steps:

            # perform procedure
            procedures[step]()

        return None

    def flux(self):
        """Collection azimuth and gain code data from cal files for QVD.

        Arguments:
            None

        Returns:
            None
        """

        return None

    def gain(self):
        """Make gain and irradiance diffuser plots for KNMI paper.

        Arguments:
            None

        Returns:
            None
        """

        # make hydra for degradation
        hydra = Hydra('../studies/amoeba/irradiance/degradations')
        hydra.ingest(0)

        # create bands, diffusers
        diffusers = ['quartz_diffuser', 'regular_diffuser', 'backup_diffuser']
        bands = ['band1', 'band2', 'band3']
        rows = {'band1': 10, 'band2': 20, 'band3': 20}

        # for each diffuser
        for diffuser in diffusers:

            # for each band
            for band in bands:

                # get row
                row = rows[band]

                # grab aluminum bakup diffuser
                aluminum = hydra.grab('backup_diffuser/{}/percent_degradation_rate'.format(band))[row]

                # get the average wavelength grid at the row
                grid = hydra.grab('{}/{}/average_wavelength_grid'.format(diffuser, band))[row]
                degradation = hydra.grab('{}/{}/percent_degradation_rate'.format(diffuser, band))[row]

                # take as ratio
                degradation = (100 + (degradation * (2022 - 2005))) / 100
                aluminum = (100 + (aluminum * (2022 - 2005))) / 100

                # create plot
                title = '{}, {}, degradation'.format(diffuser, band)
                address = 'irradiance/plots/knmi/{}_{}_spectral_degradation.h5'.format(diffuser, band)
                self.squid.ink('degradation', degradation, 'wavelength', grid, address, title)

                # create ratio plot
                title = '{}, {}, degradation ratio with backup'.format(diffuser, band)
                address = 'irradiance/plots/knmi/{}_{}_spectral_degradation_ratio.h5'.format(diffuser, band)
                self.squid.ink('aluminum', degradation / aluminum, 'wavelength', grid, address, title)

        # get radiance trending file
        hydraii = Hydra('../studies/amoeba/radiance/trends')
        hydraii.ingest(0)

        # get ds gain code
        gain = hydraii.grab('band1_gain_code_ds')[:, 0]

        # for each gain code
        for code in list(range(1, 5)) + ['ds']:

            # grab time
            time = hydraii.grab('orbit_start_time_fr_yr').squeeze()

            # get mean and stdev of gain
            mean = hydraii.grab('band1_gain_code_{}'.format(code))[:, 0]
            deviation = hydraii.grab('band1_gain_code_{}'.format(code))[:, 5]

            # great lower and upper deviations
            upper = mean + deviation
            lower = mean - deviation

            # calculate ratio
            ratio = mean / gain

            # get beginning of mission
            beginning = numpy.array([mean[0] for _ in time])

            # create mean plot
            title = 'Band 1, mean gain code {}'.format(code)
            address = 'radiance/plots/knmi/mean_gain_{}.h5'.format(code)
            self.squid.ink('mean_gain', mean, 'time', time, address, title)

            # create lower deviation plot
            title = 'Band 1, 1 std lower gain code {}'.format(code)
            address = 'radiance/plots/knmi/lower_gain_{}.h5'.format(code)
            self.squid.ink('lower_gain', lower, 'time', time, address, title)

            # create upper deviation plot
            title = 'Band 1, 1 std upper gain code {}'.format(code)
            address = 'radiance/plots/knmi/upper_gain_{}.h5'.format(code)
            self.squid.ink('upper_gain', upper, 'time', time, address, title)

            # create upper deviation plot
            title = 'Band 1, beginning gain code {}'.format(code)
            address = 'radiance/plots/knmi/beginning_gain_{}.h5'.format(code)
            self.squid.ink('beginning_gain', beginning, 'time', time, address, title)

            # create ratio plot
            title = 'Band 1, beginning gain code {}'.format(code)
            address = 'radiance/plots/knmi/ratio_gain_{}.h5'.format(code)
            self.squid.ink('ratio_gain', ratio, 'time', time, address, title)

        return None

    def gap(self, start='2022-09-01', finish='2024-05-30', track=1644):
        """Make plot of l1b data gaps.

        Arguments:
            start: str, starting date
            end: str, ending date
            track: int, standard track length

        Returns:
            None
        """

        # create folder for boulder presentation
        self._make('{}/boulder'.format(self.sink))

        # get log file
        log = self._know('../studies/komodo/log/Missing_L1B_Data_Report.txt')

        # parse years
        years = (int(start[:4]), int(finish[:4]) + 1)

        # generate dates
        dates = []
        for year in range(*years):

            # for each month
            for month in range(1, 13):

                # for each day
                for day in range(1, 32):

                    # create date
                    date = '{}-{}-{}'.format(year, self._pad(month), self._pad(day))
                    dates.append(date)

        # filter by start, end
        dates = [date for date in dates if date > start and date < finish]

        # create record with default scans
        record = {date: track for date in dates}
        recordii = {date: track for date in dates}

        # get all days and scans from log
        days = [line.split(':')[-1].strip() for line in log[1:] if line.startswith('date:')]
        orbits = [int(line.split(':')[-1].strip()) for line in log[1:] if line.startswith('orbit number:')]
        scans = [int(line.split(':')[-1].strip()) for line in log[1:] if line.startswith('collection 4 scanlines:')]

        # check for recoveries
        recoveries = []
        for orbit, day, scan in zip(orbits, days, scans):

            print(day, orbit)

            # create hydra
            formats = (day[:4], day[5:7], day[8:10])
            hydra = Hydra('/tis/acps/OMI/10004/OML1BRUG/{}/{}/{}'.format(*formats))

            # try to
            try:

                # ingest and grab scan data
                hydra.ingest(str(orbit))
                latitude = hydra.grab('BAND2/latitude').squeeze()
                recovery = latitude.shape[0]
                recoveries.append(recovery)

            # unless not found
            except OSError:

                # add to recoveries
                recoveries.append(scan)

        # for each pair
        for day, scan, recovery in zip(days, scans, recoveries):

            # update record
            record[day] = scan
            recordii[day] = recovery

        # sort items
        pairs = list(record.items())
        pairs.sort(key=lambda pair: pair[0])
        pairsii = list(recordii.items())
        pairsii.sort(key=lambda pair: pair[0])

        # begin arrays
        milliseconds = []
        scans = []
        recoveries = []

        # for each pair
        for pair, pairii in zip(pairs, pairsii):

            # unnpack
            date = pair[0]
            scan = pair[1]
            recovery = pairii[1]

            # try to
            try:

                # convert date to datetime
                year, month, day = [int(component) for component in date.split('-')]
                millisecond = datetime.datetime(year, month, day).timestamp() * 1000

                # add to arrays
                milliseconds.append(millisecond)
                scans.append(scan)
                recoveries.append(recovery)

            # unless ValueError
            except ValueError:

                # in which case, skip
                pass

        # create arrays
        milliseconds = numpy.array(milliseconds)
        scans = numpy.array(scans)
        recoveries = numpy.array(recoveries)

        # make plot
        address = 'boulder/missing_l1b_gaps.h5'
        title = 'Missing L1B Data Gaps'
        self.squid.ink('scanlines', scans, 'time', milliseconds, address, title)

        # make plot
        address = 'boulder/missing_l1b_recoveries.h5'
        title = 'Missing L1B Data Gaps'
        self.squid.ink('recoveries', recoveries, 'time', milliseconds, address, title)

        return None

    def highlight(self):
        """Create highlights trending files.

        Arguments:
            None

        Returns:
            None
        """

        # radiance mission file
        hydra = Hydra('{}/radiance/trends'.format(self.sink))
        hydra.ingest('OML1BRADHIS_Trending')

        # collect orbit number and start time
        orbit = hydra.dig('orbit_number')[0]
        year = hydra.dig('orbit_start_time_fr_yr')[0]

        # begin data
        data = {orbit.slash: orbit.distil(), year.slash: year.distil()}
        attributes = {orbit.slash: orbit.attributes, year.slash: year.attributes}

        # set bands
        bands = ['band{}'.format(number + 1) for number in range(3)]

        # add optical bench temperatures
        temperatures = [hydra.grab('{}/temp_opb'.format(band)) for band in bands]
        #address = 'highlights/temperatures/optical_bench_temperatures'
        address = 'highlights/optical_bench_temperatures'
        array = numpy.hstack([temperature[:, 0:1] for temperature in temperatures])
        data[address] = array
        attributes[address] = hydra.dig('band1/temp_opb')[0].attributes
        attributes[address]['PlotLegend'] = bands
        attributes[address]['LongName'] = 'optical bench temperature'

        # add detector temperatures
        detectors = [hydra.grab('{}/temp_det'.format(band)) for band in bands]
        #address = 'highlights/temperatures/ccd_detector_temperatures'
        address = 'highlights/ccd_detector_temperatures'
        array = numpy.hstack([detector[:, 0:1] for detector in detectors])
        data[address] = array
        attributes[address] = hydra.dig('band1/temp_det')[0].attributes
        attributes[address]['PlotLegend'] = bands
        attributes[address]['LongName'] = 'CCD detector temperatures'

        # for each specgral quality flag:
        flags = ['bad_pixel', 'missing', 'processing_error', 'rts', 'saturated', 'transient', 'underflow']
        pixels = {'band1': 159 * 30, 'band2': 557 * 60, 'band3': 751 * 60}
        for flag in flags:

            # add flag counts
            qualities = [hydra.grab('{}/{}'.format(band, flag)) for band in bands]
            #address = 'highlights/spectral_channel_quality/{}'.format(flag)
            address = 'highlights/spectral_channel_quality_{}'.format(flag)
            array = numpy.hstack([quality[:, 0:1] * 100 / pixels[band] for quality, band in zip(qualities, bands)])
            data[address] = array
            attributes[address] = hydra.dig('band1/{}'.format(flag))[0].attributes
            attributes[address]['PlotLegend'] = bands
            attributes[address]['Units'] = '% flagged'
            attributes[address]['LongName'] = 'percent pixels flagged for {}'.format(flag)

        # dump file
        destination = '{}/highlights/Housekeeping_Highlights.h5'.format(self.sink)
        hydra.spawn(destination, data, attributes)

        # set fill value
        fill = -999

        # get the irradiance degradation data
        hydra = Hydra('{}/irradiance/degradations'.format(self.sink))
        hydra.ingest()

        # collect orbit number and start time
        orbit = hydra.grab('quartz/orbit_number')
        start = hydra.grab('quartz/orbit_start_time_fr_yr')

        # begin data and attributes with independent variables
        data = {'IndependentVariables/orbit_number': orbit}
        data.update({'IndependentVariables/orbit_start_time_fr_yr': start})
        attributes = {}

        # set bands
        bands = ['band{}'.format(number + 1) for number in range(3)]

        # set wavelength ranges
        wavelengths = {'band1': (290, 300), 'band2': (320, 330), 'band3': (390, 400)}

        # set row ranges to avoid row anomaly rows ( 11 - 27 1-based for UV1 )
        rows = {'band1': (10, 26), 'band2': (20, 53), 'band3': (20, 53)}
        edges = {'band1': (1, 10, 28, 30), 'band2': (1, 20, 55, 60), 'band3': (1, 20, 55, 60)}

        # begin wavelength assignment stability array
        stability = []

        # set independent variables
        independent = 'IndependentVariables/orbit_number'
        independentii = 'IndependentVariables/orbit_start_time_fr_yr'

        # begin accumulations
        screens = []
        signals = []
        legends = []

        # for all bands
        for band in bands:

            # begin signal degradation array
            signal = []
            legend = []

            # get irradiances and wavelengths
            irradiance = hydra.grab('quartz/{}/irradiance'.format(band))
            correction = hydra.grab('quartz/{}/irradiance_corrected'.format(band))
            wavelength = hydra.grab('quartz/{}/wavelength_grid'.format(band))
            fraction = hydra.grab('quartz/{}/year_fraction'.format(band))

            # create mask for wavelengths range
            waves = wavelengths[band]
            mask = (wavelength >= waves[0]) & (wavelength <= waves[1]) & numpy.isfinite(wavelength)

            # exclude row anomaly rows ( 11 - 27 1-based for UV1 )
            indices = [row for row in range(irradiance.shape[1]) if (row < rows[band][0]) or (row > rows[band][1])]
            irradiance = irradiance[:, indices, :]
            correction = correction[:, indices, :]
            mask = mask[:, indices, :]

            # get beginning index
            beginning = hydra._pin(2005, fraction)[0][0]

            # apply averaging across rows and wavelengths for each timepoint
            panels = [panel[masque].mean() for panel, masque in zip(irradiance, mask)]
            irradiance = numpy.array(panels)
            ratio = irradiance / irradiance[beginning]
            masque = numpy.isfinite(ratio)
            ratio = numpy.where(masque, ratio, -999)
            legend.append('{} signal, uncorrected'.format(band))

            # create trendline
            conversion = 1000 * 60 * 60 * 24 * 365
            slope, intercept = self._regress(numpy.array([[ratio]]).transpose(2, 0, 1), start)
            trend = intercept + slope * start
            legend.append('{} % / yr'.format(self._round(float(slope) * 100 * conversion, 2)))

            # align yer 2005 with ratio = 1.0
            offset = trend[beginning] - 1
            trend = trend - offset
            ratio = ratio - offset
            signal.append(ratio)
            signal.append(trend.reshape(-1))

            # apply averaging across rows and wavelengths for each timepoint, correctec irradiance
            panels = [panel[masque].mean() for panel, masque in zip(correction, mask)]
            correction = numpy.array(panels)
            ratioii = correction / correction[beginning]
            masque = numpy.isfinite(ratioii)
            ratioii = numpy.where(masque, ratioii, -999)
            legend.append('{} signal, corrected'.format(band))

            # create trendline
            conversion = 1000 * 60 * 60 * 24 * 365
            slope, intercept = self._regress(numpy.array([[ratioii]]).transpose(2, 0, 1), start)
            trendii = intercept + slope * start
            legend.append('{} % / yr'.format(self._round(float(slope) * 100 * conversion, 2)))

            # align yer 2005 with ratio = 1.0
            offset = trendii[beginning] - 1
            trendii = trendii - offset
            ratioii = ratioii - offset
            signal.append(ratioii)
            signal.append(trendii.reshape(-1))

            # recreate mask for wavelengths 290 - 300, first entry
            mask = (wavelength >= waves[0]) & (wavelength <= waves[1])
            mask = mask[0]

            # limit to row 10 or 20
            wavelength = wavelength[:, rows[band][0], :]
            mask = mask[rows[band][0], :]

            # get absolute wavelength differences from 2005
            difference = wavelength - wavelength[0]

            # get the average difference
            panels = [panel[mask] for panel in difference]
            average = numpy.array([panel.mean() for panel in panels])

            # add to stability array
            stability.append(average)

            # add to signals and ligends
            legends.append(legend)
            signal = numpy.array(signal)
            signals.append(signal)

            # add to screens
            screen = numpy.isfinite(signal) & (signal > fill)
            screens.append(screen)

        # combine screens
        screens = [numpy.logical_not(screen).astype(int) for screen in screens]
        screenii = numpy.zeros(screens[0].shape)
        for screen in screens:

            # add to screenii
            screenii += screen

        # get only those with no entries
        screenii = screenii.sum(axis=0).squeeze()
        screenii = (screenii < 1)

        # for all bands
        for index, band in enumerate(bands):

            # add to data
            # address = 'highlights/irradiance/{}_signal'.format(band)
            address = 'irradiance_summary/{}_signal'.format(band)
            array = numpy.array(signals[index])
            data[address] = array.transpose(1, 0)[screenii]
            attributes[address] = {}
            attributes[address]['PlotLegend'] = legends[index]
            formats = (*edges[band], *wavelengths[band])
            name = 'rows {} - {}, {} - {}, {}nm - {}nm'.format(*formats)
            attributes[address]['LongName'] = name
            attributes[address]['FillValue'] = fill
            attributes[address]['Units'] = 'ratio 2005-01-01'
            attributes[address].update({'Status': True, 'doNotPlot': False})
            attributes[address].update({'AbscissaVariableSelection': [independentii]})

        # add inddpendents to cata
        data[independent] = data[independent][screenii]
        data[independentii] = data[independentii][screenii]
        attributes[independent] = {}
        attributes[independentii] = {}
        attributes[independent]['FillValue'] = fill
        attributes[independentii]['FillValue'] = fill
        attributes[independent].update({'Status': True, 'doNotPlot': True})
        attributes[independentii].update({'Status': True, 'doNotPlot': True})
        attributes[independent].update({'AbscissaVariableSelection': [independentii]})
        attributes[independentii].update({'AbscissaVariableSelection': [independentii]})
        attributes[independent].update({'LongName': 'orbit number'})
        attributes[independentii].update({'LongName': 'orbit start time'})
        attributes[independent].update({'Units': '1'})
        attributes[independentii].update({'Units': 'ms'})

        # add wavelength stability
        # address = 'highlights/irradiance/wavelength_stability'
        address = 'irradiance_registration/wavelength_stability'
        array = numpy.array(stability)

        # add data
        data[address] = array.transpose(1, 0)[screenii]
        attributes[address] = {}
        attributes[address]['PlotLegend'] = bands
        attributes[address]['LongName'] = 'wavelength assignment stability'
        attributes[address]['FillValue'] = fill
        attributes[address]['Units'] = 'nm'
        attributes[address].update({'Status': True, 'doNotPlot': False})
        attributes[address].update({'AbscissaVariableSelection': [independentii]})

        # dump file
        destination = '{}/highlights/Irradiance_Highlights.h5'.format(self.sink)
        hydra.spawn(destination, data, attributes)

        # begin data
        data = {}
        attributes = {}

        # for all bands
        for band in bands:

            # begin reservoirs
            signal = []
            legend = []
            orbits = {}
            starts = {}
            ratios = {}
            trends = {}
            slopes = {}

            # set independent addresses
            independent = 'IndependentVariables/orbit_number'
            independentii = 'IndependentVariables/orbit_start_time_fr_yr'

            # for each diffuser
            diffusers = ('quartz', 'regular', 'backup')
            for diffuser in diffusers:

                # print band, diffuser
                print('analyzing {}, {}...'.format(band, diffuser))

                # get orbit numbers
                orbit = hydra.grab('{}/{}/orbit_number'.format(diffuser, band)).squeeze()

                # add attributes
                attributes[independent] = hydra.dig('{}/{}/orbit_number'.format(diffuser, band))[0].attributes

                # get irradiances and wavelengths
                start = hydra.grab('{}/{}/orbit_start_time_fr_yr'.format(diffuser, band)).squeeze()
                irradiance = hydra.grab('{}/{}/irradiance'.format(diffuser, band)).squeeze()
                fraction = hydra.grab('{}/{}/year_fraction'.format(diffuser, band)).squeeze()
                wavelength = hydra.grab('{}/{}/wavelength_grid'.format(diffuser, band)).squeeze()

                # add attributes for start time
                formats = (diffuser, band)
                attributes[independentii] = hydra.dig('{}/{}/orbit_start_time_fr_yr'.format(*formats))[0].attributes

                # create mask for wavelengths range
                waves = wavelengths[band]
                mask = (wavelength >= waves[0]) & (wavelength <= waves[1]) & numpy.isfinite(wavelength)

                # exclude row anomaly rows ( 11 - 27 1-based for UV1 )
                indices = [row for row in range(irradiance.shape[1]) if (row < rows[band][0]) or (row > rows[band][1])]
                irradiance = irradiance[:, indices, :]
                mask = mask[:, indices, :]

                # apply averaging across rows and wavelengths for each timepoint, beginning at 2005
                panels = [panel[maskii].mean() for panel, maskii in zip(irradiance, mask)]
                irradiance = numpy.array(panels)
                beginning = hydra._pin(2005, fraction)[0][0]
                ratio = irradiance / irradiance[beginning]
                masque = numpy.isfinite(ratio)

                # apply fill mask to attributes
                ratio = ratio[masque]
                orbit = orbit[masque]
                start = start[masque]

                # create trendline
                slope, intercept = self._regress(numpy.array([[ratio]]).transpose(2, 0, 1), start)
                trend = (intercept + slope * start).squeeze()

                # align yer 2005 with ratio = 1.0
                offset = trend[beginning] - 1
                trend = trend - offset
                ratio = ratio - offset

                # add to reservoirs
                orbits[diffuser] = orbit
                starts[diffuser] = start
                ratios[diffuser] = ratio
                trends[diffuser] = trend
                slopes[diffuser] = slope

            # for each diffuser
            for diffuser in diffusers:

                # begin accumulations
                accumulation = {}

                # for each other diffuser
                projections = {}
                projectionsii = {}
                projectionsiii = {}
                elements = [element for element in diffusers if element != diffuser]
                for diffuserii in elements:

                    # add projection
                    projections[diffuserii] = self._project(orbits[diffuserii], orbits[diffuser], ratios[diffuser])
                    projectionsii[diffuserii] = self._project(orbits[diffuserii], orbits[diffuser], starts[diffuser])
                    projectionsiii[diffuserii] = self._project(orbits[diffuserii], orbits[diffuser], trends[diffuser])

                # accumulate all lists
                accumulation['orbit'] = orbits[diffuser].tolist() + orbits[elements[0]].tolist()
                accumulation['orbit'] += orbits[elements[1]].tolist()
                accumulation['ratio'] = ratios[diffuser].tolist() + projections[elements[0]].tolist()
                accumulation['ratio'] += projections[elements[1]].tolist()
                accumulation['start'] = starts[diffuser].tolist() + projectionsii[elements[0]].tolist()
                accumulation['start'] += projectionsii[elements[1]].tolist()
                accumulation['trend'] = trends[diffuser].tolist() + projectionsiii[elements[0]].tolist()
                accumulation['trend'] += projectionsiii[elements[1]].tolist()

                # zip together and sort by orbit number
                zipper = list(zip(*[accumulation[field] for field in ('orbit', 'start', 'ratio', 'trend')]))
                zipper.sort(key=lambda quartet: quartet[0])

                # add abswcissas to data
                independent = 'IndependentVariables/orbit_number'
                independentii = 'IndependentVariables/orbit_start_time_fr_yr'
                data[independent] = numpy.array([[quartet[0]] for quartet in zipper])
                data[independentii] = numpy.array([[quartet[1]] for quartet in zipper])

                # add abscissas to attributes
                attributes[independent] = {}
                attributes[independentii] = {}

                # add ratios and trendliens to signal array
                signal.append([quartet[2] for quartet in zipper])
                signal.append([quartet[3] for quartet in zipper])

                # add legend entries, including slop, convert from milliseconds
                conversion = 1000 * 60 * 60 * 24 * 365
                legend.append('{}, {} signal'.format(band, diffuser))
                legend.append('{} % / yr'.format(self._round(float(slopes[diffuser]) * 100 * conversion, 2)))

            # add to data
            array = numpy.array(signal).transpose(1, 0)
            #address = 'highlights/diffuser_summary/{}_signal_change'.format(band)
            address = 'irradiance_summary/{}_signal_change'.format(band)
            data[address] = array

            # add attributes
            attributes[address] = {}
            attributes[address]['PlotLegend'] = legend
            formats = (*edges[band], *wavelengths[band])
            name = 'rows {} - {}, {} - {}, {}nm - {}nm'.format(*formats)
            attributes[address]['LongName'] = name
            attributes[address]['FillValue'] = fill
            attributes[address]['Units'] = 'ratio 2005-01-01'
            attributes[address].update({'Status': True, 'doNotPlot': False})
            attributes[address].update({'AbscissaVariableSelection': [independentii]})

            # add missinng independent attributes
            attributes[independent]['FillValue'] = fill
            attributes[independentii]['FillValue'] = fill
            attributes[independent].update({'Status': True, 'doNotPlot': True})
            attributes[independentii].update({'Status': True, 'doNotPlot': True})
            attributes[independent].update({'AbscissaVariableSelection': [independentii]})
            attributes[independentii].update({'AbscissaVariableSelection': [independentii]})
            attributes[independent].update({'LongName': 'orbit number'})
            attributes[independentii].update({'LongName': 'orbit start time'})
            attributes[independent].update({'Units': '1'})
            attributes[independentii].update({'Units': 'ms'})

        # dump file
        destination = '{}/highlights/Diffuser_Highlights.h5'.format(self.sink)
        hydra.spawn(destination, data, attributes)

        return

    def inspect(self, month=3, day=21, years=(2005, 2025), collect=False):
        """Compare the radiances between radiance and calibration file.

         Arguments:
             month: int, the month
             day: int, the day
             years: tuple of ints, year bracket

        Returns:
            None
        """

        # create folder
        folder = 'radiance/spring'
        folderii = 'radiance/inspection'
        self._make('{}/{}'.format(self.sink, folder))
        self._make('{}/{}'.format(self.sink, folderii))

        # create destination
        formats = (self.sink, folder, self._pad(month), self._pad(day))
        destination = '{}/{}/Radiance_comparison_{}{}.h5'.format(*formats)

        # if collecting
        if collect:

            # begin collection
            radiances = []
            radiancesii = []
            annums = []
            months = []
            days = []

            # for each year
            for year in range(*years):

                # create hydra for uv product
                formats = (year, self._pad(month), self._pad(day))
                hydra = Hydra('/tis/acps/OMI/10004/OML1BRUG/{}/{}/{}'.format(*formats))
                hydra.ingest()
                latitude = hydra.grab('BAND2_RADIANCE/latitude').squeeze()
                radiance = hydra.grab('BAND2_RADIANCE/radiance').squeeze()

                # create hydra for calibration product
                hydraii = Hydra('/tis/acps/OMI/10004/OML1BCAL/{}/{}/{}'.format(*formats))
                hydraii.ingest(0)
                latitudeii = hydraii.grab('BAND2_RADIANCE/MODE_000/latitude').squeeze()
                radianceii = hydraii.grab('BAND2_RADIANCE/MODE_000/radiance_avg').squeeze()

                # line up latitudes
                scan = hydra._pin(latitudeii[0, 0], latitude[:, 0])[0][0]
                shape = latitude.shape

                # create radiance average from latitude block
                average = radiance[scan: scan + shape[0]].mean(axis=0)

                print(radiance.shape)
                print(radianceii.shape)
                print(average.shape)

                # add to arrayss
                radiances.append(average)
                radiancesii.append(radianceii)

                # add time columns
                annums.append(year)
                months.append(month)
                days.append(day)

            # create data
            data = {'radiance_oml1brug': radiances, 'radiance_oml1bcal': radiancesii}
            data.update({'year': annums, 'month': months, 'day': days})
            data = {name: numpy.array(array) for name, array in data.items()}

            # stash
            hydra = Hydra()
            hydra.spawn(destination, data)

        # get file
        hydra = Hydra(self._fold(destination))
        hydra.ingest(destination)
        data = hydra.extract()

        # for various pixels
        rows = (10, 20, 30)
        waves = (100, 200, 300, 400, 500)
        pixels = [(row, wave) for row in rows for wave in waves]
        for row, wave in pixels:

            # for each product
            for product in ('oml1brug', 'oml1bcal'):

                # create abscissa
                abscissa = [(datetime.datetime(year, month, day).timestamp() * 1000) for year in data['year']]
                ordinate = data['radiance_{}'.format(product)][:, row, wave]

                # create plots
                formats = (folderii, row, wave, product, self._pad(month), self._pad(day))
                address = '{}/radiance_row{}_pixel{}_{}_{}.h5'.format(*formats)
                title = 'radiance, row {}, pixel {}, {}, m{}{}'.format(*formats[1:])
                self.squid.ink('radiance_avg', ordinate, 'time', abscissa, address, title)

        return None

    def light(self):
        """Create trending files for dark current, led, and wls.

        Arguments:
            None

        Returns:
            None
        """

        # set lights
        lights = {'dark': 'dark_current', 'light': 'led_signal', 'white': 'wls_signal'}

        # set fill
        fill = -999

        # set independent addresses
        independent = 'IndependentVariables/orbit_number'
        independentii = 'IndependentVariables/orbit_start_time_fr_yr'

        # for each light
        for light, field in lights.items():

            # grab the mission file
            hydra = Hydra('../studies/caladrius/mission/{}'.format(light))
            hydra.ingest()

            # begin data
            data = {}
            attributes = {}

            # add orbit number
            orbit = hydra.grab('orbit_number')
            data[independent] = orbit

            # add start time
            start = hydra.grab('orbit_start_time_fr_yr')
            data[independentii] = start

            # add missing independent attributes
            attributes[independent] = {}
            attributes[independentii] = {}
            attributes[independent]['FillValue'] = fill
            attributes[independentii]['FillValue'] = fill
            attributes[independent].update({'Status': True, 'doNotPlot': True})
            attributes[independentii].update({'Status': True, 'doNotPlot': True})
            attributes[independent].update({'AbscissaVariableSelection': [independentii]})
            attributes[independentii].update({'AbscissaVariableSelection': [independentii]})
            attributes[independent].update({'LongName': 'orbit number'})
            attributes[independentii].update({'LongName': 'orbit start time'})
            attributes[independent].update({'Units': '1'})
            attributes[independentii].update({'Units': 'ms'})

            # collect both detectors
            signal = []
            legend = []

            # for each detector
            for detector in (1, 2):

                # grab data
                name = 'detector_{}/{}'.format(detector, field)
                address = 'calibration/detector_{}_{}'.format(detector, field)
                array = hydra.grab(name)
                data[address] = array

                # add attributes
                attributes[address] = {}
                attributes[address]['PlotLegend'] = ['mean', 'median', 'min', 'max', 'stdev', 'range']
                attributes[address]['LongName'] = 'detector {} {} trending'.format(detector, field)
                attributes[address]['FillValue'] = fill
                attributes[address]['Units'] = 'electrons / s'
                attributes[address].update({'Status': True, 'doNotPlot': False})
                attributes[address].update({'AbscissaVariableSelection': [independentii]})

                # make trendline for detector
                trend, rate = self._measure(array[:, :1], data[independentii], 2005)

                # add to signal and legend
                signal.append(array[:, 0])
                signal.append(trend)
                legend.append('detector {}'.format(detector))
                legend.append('{} % / yr'.format(self._round(rate, 2)))

            # add comparison to data
            address = 'calibration/{}_comparison'.format(field)
            data[address] = numpy.array(signal).transpose(1, 0)

            # add attributes
            attributes[address] = {}
            attributes[address]['PlotLegend'] = legend
            attributes[address]['LongName'] = '{} trending'.format(field)
            attributes[address]['FillValue'] = fill
            attributes[address]['Units'] = 'electrons / s'
            attributes[address].update({'Status': True, 'doNotPlot': False})
            attributes[address].update({'AbscissaVariableSelection': [independentii]})

            # stash
            destination = '{}/light/{}_light_trending.h5'.format(self.sink, field)
            self.spawn(destination, data, attributes)

        return None

    def orbit(self):
        """Create json of orbit numbers and dates.

        Arguments:
            None

        Returns:
            None
        """

        # create orbits
        self._make('{}/orbits'.format(self.sink))

        # begin file
        orbits = {}
        destination = '{}/orbits/orbits.json'.format(self.sink)

        # for each year
        for year in range(2004, 2024):

            # for each month
            for month in range(1, 13):

                # create hydra instance
                hydra = Hydra('/tis/acps/OMI/10004/OML1BRVG/{}/{}'.format(year, self._pad(month)), 1, 31)
                for path in hydra.paths:

                    # extract the information
                    stage = self._stage(path)
                    orbit = int(stage['orbit'])
                    date = stage['date']

                    # add entry to records
                    orbits[orbit] = date

                # save file
                self._dump(orbits, destination)

        return None

    def phagocytose(self):
        """Recontruct flag files for collection 3 collection 4 report.

        Arguments:
            None

        Returns:
            None
        """

        # collect flags
        flags = self._see('{}/flags'.format(self.sink))[0]
        flags = self._know(flags)

        # get spectral and pixelquality flags
        spectral = [line.split()[1] for line in flags[1:8]]
        pixel = [line.split()[1] for line in flags[10:26]]

        # make hydra
        hydra = Hydra('{}/originals'.format(self.sink))

        # define orbit extraction function
        def extracting(entry): return int(str(entry).split('o')[1].strip("'"))

        # collect all orbit numbers
        orbits = []
        for path in hydra.paths:

            # ingest path
            hydra.ingest(path)

            # get orbits
            orbit = hydra.dig('orbit')[0].distil()
            orbit = [extracting(entry) for entry in orbit]
            orbits.append(orbit)

        # get intersection
        sets = [set(orbit) for orbit in orbits]
        intersection = sets[0].intersection(*sets[1:])

        # get orbital subsets
        subsets = []
        for orbit in orbits:

            # check for matches with intersection
            subset = [index for index, number in enumerate(orbit) if number in intersection]
            subsets.append(subset)

        # begin features
        features = []

        # for each path, orbital subset
        for subset, path in zip(subsets, hydra.paths):

            # ingest the path
            hydra.ingest(path)

            # collect dates
            dates = hydra.dig('orbit')[0].distil()[subset]
            days = self._group(dates, lambda date: date[:9])
            days = list(days.items())
            days.sort(key=lambda pair: pair[0])

            # create time features
            years = numpy.array([int(day[0][:4]) for day in days])
            months = numpy.array([int(day[0][5:7]) for day in days])
            dailies = numpy.array([int(day[0][7:9]) for day in days])

            # create features
            features.append(Feature(['Year'], years))
            features.append(Feature(['Month'], months))
            features.append(Feature(['Day'], dailies))

            # go through each
            brackets = []
            for day, members in days:

                # find indices
                indices = [index for index, date in enumerate(dates) if date in members]
                brackets.append(indices)

            # grab parameters
            parameters = hydra.dig('parameter')[0].distil()

            # grab data
            data = hydra[0].distil()

            # check path
            if 'BAND' in path:

                # extract band
                bands = {'BAND1': 'band1', 'BAND2': 'band2', 'BAND3': 'band3'}
                band = [alias for name, alias in bands.items() if name in path][0]

                # create pixel reference
                pixels = {'band1': 159 * 30, 'band2': 557 * 60, 'band3': 751 * 60}

                # for each parameter
                for parameter in spectral:

                    # get index of parameter
                    identifier = [index for index, entry in enumerate(parameters) if parameter in str(entry)][0]

                    # create address
                    address = ['collection_4', band, 'spectral_channel_quality', '{}_day_mean_image'.format(parameter)]
                    #array = [numpy.percentile(data[identifier, 4, subset][bracket], 50) for bracket in brackets]
                    array = [numpy.mean(data[identifier, 0, subset][bracket] * pixels[band]) for bracket in brackets]
                    array = numpy.array(array)
                    feature = Feature(address, array, attributes={'units': 'average pixels / image'})
                    features.append(feature)

                    # create address
                    address = ['collection_4', band, 'spectral_channel_quality', '{}_day_mean_fraction'.format(parameter)]
                    #array = [numpy.percentile(data[identifier, 4, subset][bracket], 50) for bracket in brackets]
                    array = [numpy.mean(data[identifier, 0, subset][bracket]) for bracket in brackets]
                    array = numpy.array(array)
                    feature = Feature(address, array, attributes={'units': 'pixel fraction'})
                    features.append(feature)

            # check swath
            if 'Swath' in path:

                # extract band
                swaths = {'UV-1': 'uv1', 'UV-2': 'uv2', 'VIS': 'vis'}
                swath = [alias for name, alias in swaths.items() if name in path][0]

                # create pixel reference
                pixels = {'uv1': 159 * 30, 'uv2': 557 * 60, 'vis': 751 * 60}

                # for each parameter
                for parameter in pixel:

                    # get index of parameter
                    identifier = [index for index, entry in enumerate(parameters) if parameter in str(entry)][0]

                    # create address
                    address = ['collection_3', swath, 'pixel_quality', '{}_day_mean_image'.format(parameter)]
                    #array = [numpy.percentile(data[identifier, 4, subset][bracket], 50) for bracket in brackets]
                    array = [numpy.mean(data[identifier, 0, subset][bracket] * pixels[swath]) for bracket in brackets]
                    array = numpy.array(array)
                    feature = Feature(address, array, attributes={'units': 'average pixels / image'})
                    features.append(feature)

                    # create address
                    address = ['collection_3', swath, 'pixel_quality', '{}_day_mean_fraction'.format(parameter)]
                    #array = [numpy.percentile(data[identifier, 4, subset][bracket], 50) for bracket in brackets]
                    array = [numpy.mean(data[identifier, 0, subset][bracket]) for bracket in brackets]
                    array = numpy.array(array)
                    feature = Feature(address, array, attributes={'units': 'pixel fraction'})
                    features.append(feature)

        # stash
        destination = '{}/summaries/Cardinal_Days_Col3_Col4_Spectral_Flags.h5'.format(self.sink)
        self.stash(features, destination)

        # create tree
        hydra = Hydra('{}/compact'.format(self.sink))
        hydra.ingest(0)
        hydra._grow()
        hydra._look(hydra.tree, 4, destination.replace('summaries', 'trees').replace('.h5', '_structure.txt'))

        return None

    def radiate(self):
        """Generate radiance trending plots for ozoneaq.

        Arguments:
            None

        Returns:
            None
        """

        # grab the radiances file
        hydra = Hydra('../studies/radiobe/mission/radiance')
        hydra.ingest()

        # begin data
        data = {}
        attributes = {}

        # set fill value
        fill = -999

        # set independent addresses
        independent = 'IndependentVariables/orbit_number'
        independentii = 'IndependentVariables/orbit_start_time_fr_yr'

        # add orbit number
        orbit = hydra.grab('orbit_number')
        data[independent] = orbit

        # add start time
        start = hydra.grab('orbit_start_time_fr_yr')
        data[independentii] = start

        # add missing independent attributes
        attributes[independent] = {}
        attributes[independentii] = {}
        attributes[independent]['FillValue'] = fill
        attributes[independentii]['FillValue'] = fill
        attributes[independent].update({'Status': True, 'doNotPlot': True})
        attributes[independentii].update({'Status': True, 'doNotPlot': True})
        attributes[independent].update({'AbscissaVariableSelection': [independentii]})
        attributes[independentii].update({'AbscissaVariableSelection': [independentii]})
        attributes[independent].update({'LongName': 'orbit number'})
        attributes[independentii].update({'LongName': 'orbit start time'})
        attributes[independent].update({'Units': '1'})
        attributes[independentii].update({'Units': 'ms'})

        # assign wavelengths
        wavelengths = {'band_1': (270, 290, 310), 'band_2': (331, 340, 354, 360), 'band_3': (354, 388, 400, 440, 466)}

        # set row brackets
        brackets = {'band_1': [(index * 5, 5 + index * 5) for index in range(6)]}
        brackets.update({'band_2': [(index * 5, 5 + index * 5) for index in range(12)]})
        brackets.update({'band_3': [(index * 5, 5 + index * 5) for index in range(12)]})

        # set zone names
        zones = {'mode_0': 'tropics', 'mode_1': 'midlatitudes', 'mode_2': 'poles'}

        # for each band
        for band in ('band_1', 'band_2', 'band_3'):

            # and each mode
            for mode in ('mode_0', 'mode_1', 'mode_2'):

                # print status
                self._stamp('trending {}, {}...'.format(band, mode), initial=True)

                # get the radiance and wavelength data
                radiance = hydra.grab('{}/{}/radiance'.format(band, mode))
                wavelength = hydra.grab('{}/{}/wavelength'.format(band, mode))

                # and for each wavelength
                for wave in wavelengths[band]:

                    # print wavelength
                    self._print('{} nm...'.format(wave))

                    # # create interpolation
                    # interpolation = self._interpolate(radiance, wavelength, wave)
                    # print(radiance.shape, wavelength.shape, interpolation.shape)

                    # crate selection of closest pixels
                    average = wavelength.mean(axis=0)
                    distance = abs(average - wave)
                    order = numpy.argmin(distance, axis=1)
                    selection = numpy.array([radiance[:, row, position] for row, position in enumerate(order)])
                    selection = selection.transpose(1, 0)

                    # create trendlines
                    rows = selection.shape[1]
                    pairs = [self._measure(selection[:, row:row + 1], start, 2005) for row in range(rows)]
                    trends = [pair[0] for pair in pairs]
                    rates = [pair[1] for pair in pairs]

                    # and set of rows
                    for bracket in brackets[band]:

                        # and each row
                        stack = []
                        legend = []
                        for row in range(*bracket):

                            # grab the irradiance sequence
                            stack.append(selection[:, row].squeeze())
                            legend.append('Row {}'.format(row + 1))

                            # calculate the trendline for each member of the sequence
                            stack.append(trends[row].squeeze())
                            legend.append('{} % / yr'.format(round(rates[row], 3)))

                        # create array and remove fills, multiple by 1e9, and set fills to 0
                        stack = numpy.array(stack).transpose(1, 0)
                        stack = stack * 1e9
                        stack = numpy.where(abs(stack) < 1e20, stack, 0)

                        # create feature
                        formats = (mode, band, mode, band, wave, self._pad(bracket[0] + 1), self._pad(bracket[1]))
                        address = 'radiance/{}_{}/radiance_{}_{}_{}nm_rows_{}_{}'.format(*formats)
                        data[address] = stack

                        # create descriptions
                        description = 'radiance, {}, {}'.format(zones[mode], band)

                        # create attributes
                        units = '1e-9 mol photons / m2 nm sr'
                        attribute = {'FillValue': -999, 'LongName': description, 'Units': units}
                        attribute.update({'Status': True, 'doNotPlot': False})
                        attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independent]})
                        attributes[address] = attribute

                # print status
                self._stamp('trended.')

        # create file
        destination = '{}/zones/Radiance_Zonal_Trending.h5'.format(self.sink)
        self.spawn(destination, data, attributes)

        return None

    def refuse(self, start=2005):
        """Calculate diffuser degradation rates from drifts in aggrergate rawflux files.

        Arguments:
            start: float, starting time in years

        Returns:
            None
        """

        # make hydra
        hydra = Hydra('{}/originals'.format(self.sink))

        # set pixel attributes
        pixels = {'band1': 159, 'band2': 557, 'band3': 751}

        # begin features
        features = []

        #  for each path
        for path in hydra.paths:

            # get band and diffuser
            band = re.search('BAND[1-3]', path).group().lower()
            diffuser = re.search('quartz|regular|backup', path).group()

            # ingest
            hydra.ingest(path)

            # timestamp
            self._stamp('solving for degradation rates for {}, {}...'.format(diffuser, band), initial=True)

            # create address
            address = ['{}_diffuser'.format(diffuser), band]

            # calculate wavelength grid from coefficients
            coefficient = hydra.dig('wavelength_coefficient'.format(band))[0].distil()
            reference = hydra.dig('wavelength_reference'.format(band))[0].distil()
            wavelengths = self._calculate(coefficient, reference, pixels[band])
            features.append(Feature(address + ['wavelength_grid'], wavelengths))
            features.append(Feature(address + ['average_wavelength_grid'], wavelengths.mean(axis=0)))

            # calculate slopes vs time
            irradiance = hydra.dig('irradiance_after_relirr_avg')[0].distil()
            time = hydra.dig('orbit_start_year_fraction')[0].distil()
            slope, intercept = self._regress(irradiance, time)
            features.append(Feature(address + ['degradation_slope'], slope))
            features.append(Feature(address + ['degradation_intercept'], intercept))
            features.append(Feature(address + ['irradiance'], irradiance))
            features.append(Feature(address + ['year_fraction'], time))

            # calculate initial values at start time
            initial = intercept + slope * start
            features.append(Feature(address + ['irradiance_at_{}'.format(start)], initial))

            # convert to percentage degradation rates
            percent = 100 * slope / initial
            features.append(Feature(address + ['percent_degradation_rate'], percent))

            # convert irradiances to ratios
            ratios = 100 * irradiance / initial
            features.append(Feature(address + ['percent_irradiance_from_{}'.format(start)], ratios))

            # make brackets for trend line
            bracket = numpy.array([time[0], time[-1]])
            trend = numpy.array([(100 * intercept / initial) + percent * entry for entry in bracket])
            features.append(Feature(address + ['trendline_year_bracket'], bracket))
            features.append(Feature(address + ['trendline_percent_irradiance'], trend))

            # calculate total exposure
            track = hydra.dig('OrbitTrackLength')[0].distil()
            exposure = hydra.dig('exposure_time_index_zero')[0].distil()
            total = numpy.array([((track * exposure / 3600)).sum()])
            features.append(Feature(address + ['total_exposure_time'], total))

        # finish
        self._stamp('finished.')

        # stash file
        destination = '{}/degradations/Diffuser_Degradations.h5'.format(self.sink)
        self.stash(features, destination)

        # create tree
        hydra = Hydra('{}/degradations'.format(self.sink))
        hydra.ingest(0)
        hydra._grow(4, destination.replace('.h5', '_structure.txt'))

        return None

    def review(self):
        """Produce plots for senior review.

        Arguments:
            None

        Returns:
            None
        """

        # get orbits file
        orbits = self._load('{}/orbits/orbits.json'.format(self.sink))
        orbits = {int(orbit): date for orbit, date in orbits.items()}

        # set collection files
        collections = {3: 'VIS', 4: 'BAND3'}

        # get collection 3, band three bad pixels
        hydra = Hydra('../studies/highlights/flags/originals')

        # for each collection
        for collection, band in collections.items():

            # ingest corresponding file
            hydra.ingest(hydra._pick(band)[0])

            # get parameters and orbits
            parameters = hydra.grab('parameter')
            numbers = hydra.grab('orbit')
            numbers = numpy.array([int(str(number).strip("'").split('-o')[-1]) for number in numbers])

            # create valid orbit mask
            mask = numpy.array([number in orbits.keys() for number in numbers])
            numbers = numbers[mask]

            # find parameter list with bad pixel and create array from first feature
            bad = [index for index, name in enumerate(parameters) if 'bad_pixel' in str(name)][0]
            array = hydra[0].distil()[bad, 0, :]
            array = array[mask] * 100
            #array = numpy.log10(array * 60 * 751)

            # create times
            dates = [orbits[number] for number in numbers]
            formats = [(0, 4), (5, 7), (7, 9), (10, 12), (12, 14)]
            times = [[int(date[start:finish]) for start, finish in formats] for date in dates]
            times = [datetime.datetime(*entry) for entry in times]

            # add extra datetime for missing 12/21
            times += [datetime.datetime(year=2019, month=12, day=12)]
            stamps = [date.timestamp() for date in times]
            firsts = [datetime.datetime(date.year, 1, 1).timestamp() for date in times]
            years = [date.year for date in times]
            lasts = [datetime.datetime(date.year + 1, 1, 1).timestamp() for date in times]
            zipper = zip(years, stamps, firsts, lasts)
            times = [year + (stamp - first) / (last - first) for year, stamp, first, last in zipper]

            # add extra point and time to fill in gap at 12,21 verified to be 13
            array = numpy.array([entry for entry in array] + [array[-1]])

            # create file
            address = 'plots/Collection_{}_Bad_Pixels.h5'.format(collection)
            title = 'Collection {} Bad Pixels, Band3'.format(collection)
            self.squid.ink('log_bad_pixels', array, 'orbit_start_time', times, address, title)

        # for each year
        for year in (2005, 2020):

            # get collection 4 file
            hydra = Hydra('/tis/acps/OMI/10004/OML1BRVG/{}/06/21'.format(year))
            hydra.ingest(0)

            # find the latitude closest to equator
            latitude = hydra.grab('latitude').squeeze()
            pixel = hydra._pin(0, latitude[:, 30])[0][0]

            # get the quality at latitude and wavelength
            quality = hydra.grab('spectral_channel_quality').squeeze()[pixel]
            wavelength = hydra.grab('wavelength').squeeze()

            # get collection 3 file
            hydra = Hydra('/tis/acps/OMI/10003/OML1BRVG/{}/06/21'.format(year))
            hydra.ingest(0)

            # get the Pixelquality flags
            qualityii = hydra.grab('PixelQualityFlags').squeeze()[pixel]

            # construct rows and pixels
            rows = numpy.array([range(60)] * 751).transpose(1, 0)
            pixels = numpy.array([range(751)] * 60)

            # get polygon corners
            corners = self._polymerize(rows, pixels)

            # construct polygons
            compasses = ('southwest', 'southeast', 'northeast', 'northwest')
            arrays = [corners[compass][:, :, 0] for compass in compasses]
            bounds = numpy.stack(arrays, axis=2)
            arrays = [corners[compass][:, :, 1] for compass in compasses]
            boundsii = numpy.stack(arrays, axis=2)
            polygons = numpy.dstack([bounds, boundsii])
            polygons = polygons.reshape(-1, 8)

            # for each collection and array
            for collection, array in zip((3, 4), (qualityii, quality)):

                # get unique values of flags
                uniques = numpy.unique(array)
                codes = [unique for unique in uniques if bin(unique)[-2] == '1']

                # begin mask with all true, and add each code
                mask = (array < 0)
                for code in codes:

                    # add mask for particular value
                    maskii = (array == code)
                    mask = numpy.logical_or(mask, maskii)

                # convert mask to int
                bad = mask.astype(int)

                # make file
                grid = {'bad_pixel_map': bad}
                hydra.spawn('{}/plots/ZZ_Bad_Pixel_{}_{}_Map.h5'.format(self.sink, collection, year), grid)

                # flatten arrays
                bad = bad.flatten()
                rows = rows.flatten()
                pixels = pixels.flatten()
                wavelength = wavelength.flatten()

                # make mask for bad pixels
                masque = bad > 0

                # heat map the uv, planar
                tracer = bad
                address = 'plots/Bad_Pixel_Collection_{}_{}.h5'.format(collection, year)
                title = 'Collection {} Bad Pixels, {}-06-21'.format(collection, year)
                bracket = [0.5, 1.5]
                name = 'bad_pixels'
                units = 'bad pixel'
                texts = [name, 'latitude', 'longitude']
                arrays = [tracer[masque], rows[masque], pixels[masque]]
                self.squid.pulsate(texts, arrays, polygons[masque], address, bracket, title, units, precision='f4')

        # get the mission file
        hydra = Hydra('../studies/juggernaut/review/mission')
        hydra.ingest()

        # get the year fraction
        fraction = hydra.grab('year_fraction')

        # set pixel amounts
        sizes = {'band1': 159 * 30, 'band2': 557 * 60, 'band3': 751 * 60}

        # for bands1 and 2
        for band in ('band1', 'band2', 'band3'):

            # get the band 2 temp and plot
            temperature = hydra.grab('{}/temp_det'.format(band)).squeeze()
            length = temperature.shape[0]
            indices = [index for index in range(length) if index % 6 == 0]
            temperature = temperature[indices]

            # get rolling average and deviation for temperature
            window = 50
            roll = numpy.array([temperature[index: index + window].mean() for index, _ in enumerate(temperature)])
            deviation = numpy.array([temperature[index: index + window].std() for index, _ in enumerate(temperature)])

            # make mask to block high excursions
            mask = abs((temperature - roll) / deviation) < 3

            # plot mean temp
            title = 'CCD Detector Temperatures'
            title = ' '
            address = 'plots/Detector_Temperature_{}.h5'.format(band)
            self.squid.ink('temperature', temperature[mask], 'year', fraction[mask], address, title)

            # get band 1 bad pixels
            pixels = hydra.grab('{}/bad_pixel'.format(band))
            indices = [index for index in range(length) if index % 6 == 0]
            pixels = pixels[indices]

            # plot bad pixels
            title = 'Bad Pixels'
            address = 'plots/Bad_Pixels_{}.h5'.format(band)
            self.squid.ink('bad_pixels', pixels, 'year', fraction, address, title)

            # get band 1 rts pixels
            pixels = hydra.grab('{}/rts'.format(band))
            indices = [index for index in range(length) if index % 6 == 0]
            pixels = 100 * pixels[indices] / sizes[band]

            # plot bad pixels
            title = 'Percentage of RTS Pixels'
            address = 'plots/RTS_Pixels_{}.h5'.format(band)
            self.squid.ink('percent_rts_pixels', pixels, 'year', fraction, address, title)

        # get the mission file
        hydra = Hydra('../studies/fluxor/mission/degradations')
        hydra.ingest()

        # set wavelength ranges
        wavelengths = {'band1': (290, 300), 'band2': (320, 330), 'band3': (390, 400)}

        # set row ranges
        rows = {'band1': (9, 11, 26, 30), 'band2': (19, 22, 54, 60), 'band3': (19, 22, 54, 60)}

        # for all bands
        for band in ('band1', 'band2', 'band3'):

            # get the year fraction
            fraction = hydra.grab('quartz/year_fraction')

            # get percent degradation, row 15, central pixel
            irradiance = hydra.grab('quartz/{}/irradiance'.format(band))
            correction = hydra.grab('quartz/{}/irradiance_corrected'.format(band))
            wavelength = hydra.grab('quartz/{}/wavelength_grid'.format(band))

            # create mask for wavelengths 290 - 300
            waves = wavelengths[band]
            mask = (wavelength >= waves[0]) & (wavelength <= waves[1]) & numpy.isfinite(wavelength)

            # limit to rows 12-27
            indices = [row for row in range(irradiance.shape[1]) if (row < rows[band][1]) or (row > rows[band][2])]
            irradiance = irradiance[:, indices, :]
            correction = correction[:, indices, :]
            mask = mask[:, indices, :]

            # apply averaging across rows and wavelengths for each timepoint
            panels = [panel[masque].mean() for panel, masque in zip(irradiance, mask)]
            irradiance = numpy.array(panels)
            ratio = irradiance / irradiance[0]
            masque = numpy.isfinite(ratio)

            # plot wavelength pixels
            formats = (1, rows[band][1], rows[band][2] + 2, rows[band][3], waves[0], waves[1])
            title = 'Change in Irradiance Signal, '
            title += 'Rows {}-{}, {}-{}, Wavelengths {}-{} nm'.format(*formats)
            address = 'plots/Degradation_{}.h5'.format(band)
            self.squid.ink('ratio_compared_with_initial', ratio[masque], 'year', fraction[masque], address, title)

            # apply averaging across rows and wavelengths for each timepoint, correctec irradiance
            panels = [panel[masque].mean() for panel, masque in zip(correction, mask)]
            correction = numpy.array(panels)
            ratioii = correction / correction[0]
            masque = numpy.isfinite(ratioii)

            # plot wavelength pixels
            formats = (1, rows[band][1], rows[band][2] + 1, rows[band][3], waves[0], waves[1])
            title = 'Change in Irradiance Signal, '
            title += 'Rows {}-{}, {}-{}, Wavelengths {}-{} nm'.format(*formats)
            address = 'plots/Signal_Corrected_Degradation_{}.h5'.format(band)
            self.squid.ink('ratio_compared_with_initial', ratioii[masque], 'year', fraction[masque], address, title)

            # recreate mask for wavelengths 290 - 300, first entry
            mask = (wavelength >= waves[0]) & (wavelength <= waves[1])
            mask = mask[0]

            # limit to row 10 or 20
            wavelength = wavelength[:, rows[band][0], :]
            mask = mask[rows[band][0], :]

            # get absolute wavelength differences from 2005
            difference = wavelength - wavelength[0]

            # get the average difference
            panels = [panel[mask] for panel in difference]
            average = numpy.array([panel.mean() for panel in panels])

            # plot wavelength pixels
            title = 'Spectral Stability, Shift in Wavelength Assignment'
            address = 'plots/Median_Wavelength_{}.h5'.format(band)
            self.squid.ink('wavelength_shift', average, 'year', fraction, address, title)

        return None

    def sieve(self, source=None, sink=None, yam=None):
        """Apply yaml filter to radiance trend files.

        Arguments:
            source: str, source or radiance data
            sink: str, path of sink directory
            yam: str, path to yaml file

        Returns:
            None
        """

        # set defaults
        source = source or '{}/radiance/mission'.format(self.sink)
        sink = sink or '{}/radiance/trends'.format(self.sink)
        yam = yam or '{}/radiance/yam/radiance_trending_selections.yaml'.format(self.sink)

        # acquire the tree and splay into list of slashed addresses
        tree = self._acquire(yam)
        branches = self._splay(tree)

        # only keep those with '+'
        branches = [branch for branch in branches if branch.endswith('+')]

        # remove 'STANDARD MODE' and '+'
        branches = [branch.replace('/+', '').replace('/STANDARD_MODE', '') for branch in branches]
        groups = [branch.split('/')[1] for branch in branches]
        inversions = ['{}_{}'.format(group.split('_')[1], group.split('_')[0]) for group in groups if '_' in group]
        inversions += [group for group in groups if '_' not in group]
        branchesii = [branch.replace(group, inversion) for branch, group, inversion in zip(branches, groups, inversions)]

        # for each band
        for band in range(1, 4):

            # replace radiance with telemetry
            telemetry = 'Telemetry_&_Flagging'
            branchesii = [branch.replace('RADIANCE_BAND{}'.format(band), telemetry) for branch in branchesii]

        # ingest source file OML1BRADHIS
        hydra = Hydra(source)
        hydra.ingest('OML1BRADHIS')

        # create dataset
        data = {branchii: hydra.grab(branch) for branch, branchii in zip(branches, branchesii)}
        attributes = {branchii: hydra.dig(branch)[0].attributes for branch, branchii in zip(branches, branchesii)}

        # replace fill values
        fill = -999.0
        for name, array in data.items():

            # replace extremes with fill values
            array = numpy.where(abs(array) > 1e20, fill, array)

            # for each line
            for line in range(array.shape[1]):

                # replace fill values with -5 sigmas
                mean = array[:, line].mean()
                deviation = array[:, line].std()

                # if deviation is nan
                if numpy.isfinite(deviation):

                    # replace rill
                    fillii = min([-1e9, mean - 5 * deviation])
                    array[line] = numpy.where(array[line] == fill, fillii, array[line])

                # otherwise
                else:

                    # replace fill with -mean
                    fillii = min([-1e9, -5 * mean])
                    array[line] = numpy.where(array[line] == fill, fillii, array[line])

        # make corrections
        for branch in list(data.keys()):

            # print('')
            # print('branch: ', branch)
            # print('long_name: ', attributes[branch].get('long_name', '?'))
            # print('LongName: ', attributes[branch].get('LongName', '?'))
            # print('Units: ', attributes[branch].get('Units', '?'))
            # print('PlotLegend: ', attributes[branch].get('PlotLegend', '?'))
            # print(data[branch].shape)

            # if branch ends in count
            if branch.endswith('count'):

                # create new branch without count
                replacement = branch.replace('_count', '')
                data[replacement] = data[branch]

                # fix LongName
                attributes[replacement] = attributes[branch]
                name = attributes[branch]['LongName']
                attributes[replacement]['LongName'] = name.replace('_count', '')

                # add flag kind to description in long_name
                flag = ' '.join(name.split('_')[1: -1])
                attributes[replacement]['long_name'] += ', {}'.format(flag)

                # remove average from units
                attributes[replacement]['Units'] = 'flagged pixels / image'

                # only keep mean
                data[replacement] = data[replacement][:, :]

                # fix legend
                legend = attributes[replacement]['PlotLegend']
                attributes[replacement]['PlotLegend'] = legend

                # delete original
                del(data[branch])
                del(attributes[branch])

            # if branch is earth sun distance
            if 'earth' in branch:

                # add units
                attributes[branch]['Units'] = 'au'

                # remove most plotlegends
                legend = attributes[branch]['PlotLegend']
                attributes[branch]['PlotLegend'] = legend[:1]

                # add description
                attributes[branch]['long_name'] = 'distance from earth to the sun'

            # if it is housekeeping
            if 'housekeeping' in branch:

                # add to long_name
                description = attributes[branch]['LongName']
                description = ' '.join(description.split('_')[1:])
                attributes[branch]['long_name'] += ', {}'.format(description)

        # create file
        destination = '{}/OML1BRADHIS_Trending.h5'.format(sink)
        hydra.spawn(destination, data, attributes)

        return None

    def transfuse(self, start=2005):
        """Calculate diffuser degradation rates from drifts in aggrergate Collection 3 rawflux files.

        Arguments:
            start: float, starting time in years

        Returns:
            None
        """

        # make hydra
        hydra = Hydra('{}/originals'.format(self.sink))

        # set pixel attributes
        pixels = {'band1': 159, 'band2': 557, 'band3': 751}

        # begin features
        features = []

        #  for each path
        paths = hydra.paths
        for path in paths:

            # get band and diffuser
            band = re.search('BAND[1-3]', path).group().lower()
            diffuser = re.search('quartz|regular|backup', path).group()

            # ingest
            hydra.ingest(path)

            # timestamp
            self._stamp('solving for degradation rates for {}, {}...'.format(diffuser, band), initial=True)

            # create address
            address = ['{}_diffuser'.format(diffuser), band]

            # calculate wavelength grid from coefficients
            coefficient = hydra.dig('wavelength_coefficient'.format(band))[0].distil()
            reference = hydra.dig('wavelength_reference'.format(band))[0].distil()
            wavelengths = self._calculate(coefficient, reference, pixels[band])
            features.append(Feature(address + ['wavelength_grid'], wavelengths))
            features.append(Feature(address + ['average_wavelength_grid'], wavelengths.mean(axis=0)))

            # calculate slopes vs time
            irradiance = hydra.dig('irradiance_avg')[0].distil()
            time = hydra.dig('orbit_start_year_fraction')[0].distil()
            slope, intercept = self._regress(irradiance, time)
            features.append(Feature(address + ['degradation_slope'], slope))
            features.append(Feature(address + ['degradation_intercept'], intercept))
            features.append(Feature(address + ['irradiance'], irradiance))
            features.append(Feature(address + ['year_fraction'], time))

            # calculate initial values at start time
            initial = intercept + slope * start
            features.append(Feature(address + ['irradiance_at_{}'.format(start)], initial))

            # convert to percentage degradation rates
            percent = 100 * slope / initial
            features.append(Feature(address + ['percent_degradation_rate'], percent))

            # convert irradiances to ratios
            ratios = 100 * irradiance / initial
            features.append(Feature(address + ['percent_irradiance_from_{}'.format(start)], ratios))

            # make brackets for trend line
            bracket = numpy.array([time[0], time[-1]])
            trend = numpy.array([(100 * intercept / initial) + percent * entry for entry in bracket])
            features.append(Feature(address + ['trendline_year_bracket'], bracket))
            features.append(Feature(address + ['trendline_percent_irradiance'], trend))

            # calculate total exposure
            track = hydra.dig('orbit_track_length')[0].distil()
            exposure = hydra.dig('exposure_time_index_zero')[0].distil()
            total = numpy.array([((track * exposure / 3600)).sum()])
            features.append(Feature(address + ['total_exposure_time'], total))

        # finish
        self._stamp('finished.')

        # stash file
        destination = '{}/degradations/Diffuser_Degradations.h5'.format(self.sink)
        self.stash(features, destination)

        # create tree
        hydra = Hydra('{}/degradations'.format(self.sink))
        hydra.ingest(0)
        hydra._grow(4, destination.replace('.h5', '_structure.txt'))

        return None

    def trend(self, source='calculations', sink='trends'):
        """Create the trending file for irradiance degradation.

        Arguments:
            source: str, source folder
            sink: str, sink folder

        Returns:
            None
        """

        # make sink folder
        self._make('{}/irradiance/{}'.format(self.sink, sink))

        # get the source file
        hydra = Hydra('{}/irradiance/{}'.format(self.sink, source))
        hydra.ingest()

        # assign wavelengths
        wavelengths = {'band1': (270, 290, 310), 'band2': (331, 340, 354, 360), 'band3': (354, 388, 400, 440, 466)}

        # set row brackets
        brackets = {'band1': [(index * 5, 5 + index * 5) for index in range(6)]}
        brackets.update({'band2': [(index * 5, 5 + index * 5) for index in range(12)]})
        brackets.update({'band3': [(index * 5, 5 + index * 5) for index in range(12)]})

        diffusers = {'quartz_diffuser': 'vol_diffuser', 'regular_diffuser': 'reg_diffuser'}
        diffusers.update({'backup_diffuser': 'bck_diffuser'})

        # for each diffuser
        for diffuser, stub in diffusers.items():

            # begin features
            data = {}
            attributes = {}

            # and for each band
            bands = ['band{}'.format(band) for band in (1, 2, 3)]
            for band in bands:

                # print status
                self._print('trending {}, {}...'.format(diffuser, band))

                # get relevant features
                orbit = hydra.grab('{}/{}/orbit_number'.format(diffuser, band))
                start = hydra.grab('{}/{}/orbit_start_time_fr_yr'.format(diffuser, band))
                year = hydra.grab('{}/{}/year_fraction'.format(diffuser, band))
                irradiance = hydra.grab('{}/{}/percent_irradiance_from_2005'.format(diffuser, band))
                wavelength = hydra.grab('{}/{}/averaged_wavelength_grid'.format(diffuser, band))
                regression = hydra.grab('{}/{}/trendline_percent_irradiance'.format(diffuser, band))
                bounds = hydra.grab('{}/{}/trendline_year_bracket'.format(diffuser, band))
                correction = hydra.grab('{}/{}/percent_corrected_irradiance_from_2005'.format(diffuser, band))
                regressionii = hydra.grab('{}/{}/trendline_percent_corrected_irradiance'.format(diffuser, band))

                # add time feature, with extra dimension for compatibility
                address = 'IndependentVariables/orbit_start_time_fr_yr'
                data[address] = start.reshape(-1, 1)

                # create attribute dictionary
                attribute = {'FillValue': -999, 'LongName': 'time in milliseconds', 'Units': 'ms'}
                attribute.update({'Status': True, 'doNotPlot': True})
                attribute.update({'PlotLegend': ['time'], 'AbscissaVariableSelection': [address]})
                attributes[address] = attribute

                # add orbit number feature, with extra dimensionn for compatibility
                address = 'IndependentVariables/orbit_number'
                data[address] = orbit.reshape(-1, 1)

                # create attribute dictionary
                attribute = {'FillValue': -999, 'LongName': 'orbit number', 'Units': 'number'}
                attribute.update({'Status': True, 'doNotPlot': True})
                attribute.update({'PlotLegend': ['number'], 'AbscissaVariableSelection': [address]})
                attributes[address] = attribute

                # and for each wavelength
                for wave in wavelengths[band]:

                    # print wavelength
                    self._print('{} nm...'.format(wave))

                    # for corrected vs unncorrected
                    radiations = [irradiance, correction]
                    modes = ['irradiance', 'irradiance_corrected']
                    trends = [regression, regressionii]

                    # for each array and stub
                    for radiation, mode, trend in zip(radiations, modes, trends):

                        # and set of rows
                        for bracket in brackets[band]:

                            # and each row
                            stack = []
                            legend = []
                            for row in range(*bracket):

                                # find the pixel closest to the wavelength
                                pixel = ((wavelength[row, :] - wave) ** 2).argsort()[0]

                                # grab the irradiance sequence
                                sequence = radiation[:, row, pixel]
                                stack.append(sequence.squeeze())
                                legend.append('Row {}'.format(row + 1))

                                # calculate the trendline for each member of the sequence
                                bounds = bounds.squeeze()
                                slope = (trend[1, row, pixel] - trend[0, row, pixel]) / (bounds[1] - bounds[0])
                                intercept = trend[0, row, pixel] - slope * bounds[0]
                                line = intercept + year.squeeze() * slope
                                stack.append(line.squeeze())
                                legend.append('{} % / yr'.format(round(slope, 3)))

                            # create feature
                            formats = (diffuser, band, mode, wave, self._pad(bracket[0] + 1), self._pad(bracket[1]))
                            address = 'irradiance_diffusers/{}/{}/percent_{}_{}nm_rows_{}_{}'.format(*formats)
                            array = numpy.array(stack).transpose(1, 0)
                            array = numpy.where(numpy.isfinite(array), array, -999)
                            data[address] = array

                            # create descriptions
                            descriptions = {'irradiance': 'percent change in irradiance signal'}
                            description = 'percent change in irradiance signal, '
                            description += 'corrected for time dependent degradation'
                            descriptions.update({'irradiance_corrected': description})

                            # create attribute dictionary
                            independent = 'IndependentVariables/orbit_start_time_fr_yr'
                            attribute = {'FillValue': -999, 'LongName': descriptions[mode], 'Units': '%'}
                            attribute.update({'Status': True, 'doNotPlot': False})
                            attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independent]})
                            attributes[address] = attribute

            # create file
            formats = (self.sink, sink, diffuser)
            destination = '{}/irradiance/{}/Irradiance_Degradation_{}.h5'.format(*formats)
            hydra.spawn(destination, data, attributes)

        # begin reservoir
        signals = []
        grids = []

        # set indpendents
        independent = 'IndependentVariables/orbit_number'
        independentii = 'IndependentVariables/orbit_start_time_fr_yr'

        # make diffuser plots for certain wavelength, row, band combinations
        combinations = [('band1', 10, 270), ('band2', 20, 331), ('band3', 20, 400)]
        for band, row, wavelength in combinations:

            # accumulate signal and data
            signal = []
            legend = []
            data = {}
            attributes = {}

            # for each diffusert
            for diffuser in ['quartz_diffuser', 'regular_diffuser', 'backup_diffuser', 'sensor']:

                # find closest wavelength
                grid = hydra.grab('quartz_diffuser/{}/averaged_wavelength_grid'.format(band))[row]
                degradation = hydra.grab('{}/{}_degradation'.format(band, diffuser))[row]

                # reshape grid
                grid = grid.reshape(-1, 1)

                # if bane1
                if band == 'band1':

                    # reverse degradations
                    degradation = numpy.flip(degradation, axis=0)
                    grid = numpy.flip(grid, axis=0)

                # add grid to data, in place of orbit number and start time ( removing ends due to wild readings )
                data[independent] = grid[2:-2]
                data[independentii] = grid[2:-2]

                # add degradation to signal
                signal.append(degradation[2:-2])

                # add legend entry
                legend.append(diffuser)

            # make file
            address = 'irradiance_diffusers/spectral_degradation/{}_component_degradation'.format(band)
            data[address] = numpy.array(signal).transpose(1, 0)
            name = 'sensor degradation, row {}'.format(row)
            attribute = {'FillValue': -999, 'LongName': name, 'Units': '% / yr'}
            attribute.update({'Status': True, 'doNotPlot': False})
            attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independentii]})
            attributes[address] = attribute

            # add to signals
            signals.append(data[address])
            grids.append(data[independent])

            # add independent attributes
            attribute = {'FillValue': -999, 'LongName': 'orbit_number', 'Units': '%'}
            attribute.update({'Status': True, 'doNotPlot': True})
            attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independentii]})
            attributes[independent] = attribute
            attributes[independentii] = attribute

            # create file
            formats = (self.sink, sink, band)
            destination = '{}/irradiance/{}/Sensor_Degradation_{}.h5'.format(*formats)
            hydra.spawn(destination, data, attributes)

        # average last band1 wavelenvth and first band 2 wavelength
        average = (grids[0].max() + grids[1].min()) / 2
        mask = (grids[0] < average).squeeze()
        maskii = (grids[1] > average).squeeze()
        grids[0] = grids[0][mask]
        signals[0] = signals[0][mask]
        grids[1] = grids[1][maskii]
        signals[1] = signals[1][maskii]

        # average last band2 wavelenvth and first band 3 wavelength
        average = (grids[1].max() + grids[2].min()) / 2
        mask = (grids[1] < average).squeeze()
        maskii = (grids[2] > average).squeeze()
        grids[1] = grids[1][mask]
        signals[1] = signals[1][mask]
        grids[2] = grids[2][maskii]
        signals[2] = signals[2][maskii]

        # stack arrays
        signals = numpy.vstack(signals)
        grids = numpy.vstack(grids)

        # construct data
        address = 'irradiance_diffusers/spectral_degradation/all_bands_degradation'
        data = {address: signals}
        data[independent] = grids
        data[independentii] = grids

        # add attributes
        attributes = {}
        name = 'sensor degradation, row {} / {}'.format(combinations[0][1], combinations[1][1])
        attribute = {'FillValue': -999, 'LongName': name, 'Units': '% / yr'}
        attribute.update({'Status': True, 'doNotPlot': False})
        attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independentii]})
        attributes[address] = attribute

        # add independent attributes
        attribute = {'FillValue': -999, 'LongName': 'orbit_number', 'Units': '%'}
        attribute.update({'Status': True, 'doNotPlot': True})
        attribute.update({'PlotLegend': legend, 'AbscissaVariableSelection': [independentii]})
        attributes[independent] = attribute
        attributes[independentii] = attribute

        # create file
        formats = (self.sink, sink)
        destination = '{}/irradiance/{}/Sensor_Degradation_all_bands.h5'.format(*formats)
        hydra.spawn(destination, data, attributes)

        return None
