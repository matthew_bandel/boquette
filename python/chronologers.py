#!/usr/bin/env python3

# chronologers.py for the Chronologer class to make OMI Rawflux hdf5 files

# import local classes
from hydras import Hydra
from cores import Core
from features import Feature
from formulas import Formula

# import general tools
import sys
import re
import ast

# import yaml for reading configuration file
import yaml

# import time and datetime
import datetime
import calendar

# import math functions
import math
import numpy


# class Chronologer to do OMI data reduction
class Chronologer(Core):
    """Chronologer class to collect orbits and instrument ic id information.

    Inherits from:
        list
    """

    def __init__(self):
        """Initialize a Chronologer instance.

        Arguments:
            sink: str, filepath for data dump
            source: str, filepath of source files
            start: str, date-based subdirectory
            finish: str, date-based subdirectory
            **options: kwargs for other options

        Returns:
            None
        """

        # initialize the base Hydra instance
        Core.__init__(self)

        # load data
        self.chronology = {}
        self.orbits = {}
        self.dates = {}
        self.identities = {}
        self._ingest()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Chronologer instance >'

        return representation

    def _ingest(self):
        """Ingest latest records.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.chronology, self.dates, self.orbits
        """

        # set paths
        self.chronology = self._load('../studies/chronologer/chronicles/chronology.json')
        self.orbits = self._load('../studies/chronologer/chronicles/orbits.json')
        self.dates = self._load('../studies/chronologer/chronicles/dates.json')
        self.identities = self._load('../studies/chronologer/identities/identities.json')

        return None

    def ask(self, query):
        """Ask about an orbit or date.

        Arguments:
            query; str, date or orbit

        Returns:
            None
        """

        # print spacer
        self._print(' ')

        # try to
        try:

            # create integer from string
            query = int(query)

            # pad with zeros
            query = self._pad(query, 6)

            # try to find
            try:

                # retrieve ids and date
                identities = self.chronology[query]
                date = self.orbits[query]

                # print
                self._print('orbit: {}, date: {}, icids: {}'.format(query, date, identities))

            # unless not found
            except KeyError:

                # alert
                self._print('{} not found'.format(query))

        # unless it is a date
        except ValueError:

            # try to
            try:

                # assume date
                orbits = self.dates[query]

                # for each orbit
                for orbit in orbits:

                    # retrieve datae and icids
                    identities = self.chronology[orbit]

                    # print to screen
                    self._print('orbit: {}, date: {}, icids: {}'.format(orbit, query, identities))

            # unless not found
            except KeyError:

                # alert
                self._print('{} not found'.format(query))

        return None

    def chronicle(self, year, singlet=''):
        """Chronicle the record of irradiance calibrations using collection 3 .met files.

        Arguments:
            year: int, year
            singlet: optional single month

        Returns:
            None
        """

        # set reservoirs for chroology, orbits, dates
        reservoir = '../studies/chronologer/years/chronology_{}.json'.format(year)
        reservoirii = '../studies/chronologer/years/orbits_{}.json'.format(year)
        reservoiriii = '../studies/chronologer/years/dates_{}.json'.format(year)

        # open up files
        chronology = self._load(reservoir)
        orbits = self._load(reservoirii)
        dates = self._load(reservoiriii)

        # set months
        months = (0, 12)
        if singlet:

            # set montns to sinlge month
            months = (int(singlet) - 1, int(singlet))

        # for each month
        for month in range(*months):

            # for each day
            for day in range(31):

                # construct hydra from arcive set 10003
                formats = (year, self._pad(month + 1), self._pad(day + 1))
                hydra = Hydra('/tis/acps/OMI/10003/OML1BCAL/{}/{}/{}'.format(*formats), extensions=('.met',))

                # only get .met files
                paths = [path for path in hydra.paths if path.endswith('.met')]

                # for each path
                for path in paths:

                    # get the orbit number and date
                    orbit = self._pad(hydra._stage(path)['orbit'], 6)
                    date = hydra._stage(path)['day']

                    # get text from .met file
                    text = self._know(path)

                    # find configuration ids
                    lines = [(index, line) for index, line in enumerate(text) if 'InstrumentConfigurationIDs' in line]
                    index = lines[0][0]

                    # find the following value
                    information = [line for line in text[index + 1: index + 20] if ' VALUE' in line][0]

                    # try to
                    try:

                        # translate into ids
                        identifiers = [int(member) for member in ast.literal_eval(information.split('=')[-1].strip())]

                    # unless it hits an eof
                    except SyntaxError:

                        # add additional parenthesis and continue as before
                        information = information.strip('"').strip().strip(',') + ')'
                        identifiers = [int(member) for member in ast.literal_eval(information.split('=')[-1].strip())]

                    # add ids to chronology
                    chronology[orbit] = identifiers

                    # add date to orbits
                    orbits[orbit] = date

                    # add orbit to dates
                    group = dates.setdefault(date, [])
                    group.append(orbit)
                    group = self._skim(group)
                    group.sort()
                    dates[date] = group

                # dump data
                self._dump(chronology, reservoir)
                self._dump(orbits, reservoirii)
                self._dump(dates, reservoiriii)

        return None

    def compile(self):
        """Compile all yearly records together.

        Arguments:
            None

        Returns:
            None
        """

        # get all chronology files
        paths = self._see('../studies/chronologer/years')

        # for each mode
        for mode in ('chronology', 'orbits', 'dates'):

            # get all chronology paths
            chronologies = [path for path in paths if '{}_'.format(mode) in path]
            chronologies.sort()

            # begin main record
            chronology = {}

            # for each path
            for path in chronologies:

                # open yearly path
                year = self._load(path)

                # update
                chronology.update(year)

            # dump chronology
            self._dump(chronology, '../studies/chronologer/chronicles/{}.json'.format(mode))

        # reingest
        self._ingest()

        return None

    def identify(self, identity):
        """Identity the configuration for the instrumenet configurtion id:

        Arguments:
            identity: str or int, the icid

        Returns:
            None
        """

        # print spacer
        self._print(' ')

        # try to
        try:

            # get the configuration
            configuration = self.identities[str(identity)]

            # print
            self._print('{}: {}'.format(identity, configuration))

        # unless invalid
        except KeyError:

            # print
            self._print('{} not found.'.format(identity))

        return None

    def inspect(self):
        """Display first and last orbit of records.

        Arguments:
            None

        Returns:
            None
        """

        # pritnt spacer
        self._print(' ')

        # collect chronicles
        chronicles = self._see('../studies/chronologer/years')
        paths = [path for path in chronicles if 'dates' in path]
        paths.sort()

        # for each year
        for path in paths:

            # load records
            records = self._load(path)

            # sort keys
            keys = list(records.keys())
            keys.sort()

            # print
            self._print('{}: {} to {}'.format(self._file(path), keys[0], keys[-1]))

        return None


# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 2
    arguments = arguments[:2]

    # get year argument
    query, queryii = arguments

    # create chroologer
    chronologer = Chronologer()

    # if there are options
    if len(options) > 0:

        # if compile option
        if '--compile' in options:

            # compile
            chronologer.compile()

        # if ask option
        if '--ask' in options:

            # compile
            chronologer.ask(query)

        # if ask option
        if '--identify' in options:

            # compile
            chronologer.identify(query)

        # if ask option
        if '--inspect' in options:

            # compile
            chronologer.inspect()

    # otherwise
    else:

        # chronicle the year
        year = query
        month = queryii
        chronologer.chronicle(year, month)