# wyverns.py for Wyvern class to translate OMI hdf files into World View boquette files

# import reload
from importlib import reload

# import system tools
import os
import sys
import re
import itertools

# import datetime
import datetime

# import math and numpy
import math
import numpy

# import local classes
from formulas import Formula
from hydras import Hydra


# class Wyvern
class Wyvern(Hydra):
    """Class Wyvern to make world view files from OMI files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink, source):

        # initialize the base Hydra instance
        super().__init__(sink, source, '', '')

        return

    def __repr__(self):
        """Create string representation.

        Arguments:
            None

        Returns:
            str
        """

        # create designation
        designation = '< Wyvern instance at: {} >'.format(self.sink)

        return designation

    def _hoard(self, data, latitudes, longitudes):
        """Bin the geographic data by percentile bins.

        Arguments:
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes
            data: numpy array of measurement data

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # remove trivial dimension from measurements and reorganize
        data = data.squeeze()
        data = data.transpose(2, 0, 1)

        # stack onto measurements
        geode = numpy.vstack([data, latitudes, longitudes])

        # transpose
        geode = geode.transpose(1, 2, 0)

        return geode

    def _hoard_old(self, measurements, latitudes, longitudes):
        """Bin the geographic data by percentile bins.

        Arguments:
            data: numpy array of measurement data
            latitudes: numpy array of associated latitudes
            longitudes: numpy array of assoicated longitudes

        Returns:
            list of numpy arrays, data parsed by bin
        """

        # condense measurements to two-D
        data = measurements.squeeze().mean(axis=2)
        latitudes = latitudes.squeeze()
        longitudes = longitudes.squeeze()

        # define percentile markers
        percents = [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100]
        percentiles = [numpy.percentile(data, percent) for percent in percents]

        # begin geodes
        geodes = []

        # for each bracket
        brackets = [(first, second) for first, second in zip(percentiles[:-1], percentiles[1:])]
        for first, second in brackets:

            # create mask for particular range
            mask = (data >= first) & (data <= second)

            # get all data in the bin
            datum = data[mask].flatten()
            latitude = latitudes[mask].flatten()
            longitude = longitudes[mask].flatten()

            print(' ')
            print('datum: {}'.format(datum.shape))
            print('latitude: {}, {} - {}'.format(latitude.shape, latitude.min(), latitude.max()))
            print('longitude: {}, {} - {}'.format(longitude.shape, longitude.min(), longitude.max()))

            # concatenate
            geode = numpy.vstack([datum, latitude, longitude])

            print('geode: {}'.format(geode.shape))

            # order along latitude, then longitude
            geode = geode[:, numpy.argsort(latitude)]
            geode = geode[:, numpy.argsort(longitude)]

            print('geode: {}'.format(geode.shape))

            # append
            geodes.append(geode[1, :])
            geodes.append(geode[2, :])

            print(geode[1, :20])
            print(geode[2, :20])

        return geodes

    def gaze(self):
        """Construct a geode file from OMI file.

        Arguments:
            None

        Returns:
            None
        """

        # create geodes folder
        self._make('{}/geodes'.format(self.sink))

        # for each path
        for path in self.paths:

            # ingest path
            self.ingest(path)

            # get all processor features
            processor = self.dig('PROCESSOR')

            # group all feature by band
            bands = self._group(self, lambda feature: re.search('BAND[1-3]', feature.slash).group())

            # go through bands
            for band, members in bands.items():

                # print status
                self._stamp('subsetting {}...'.format(band), initial=True)

                # begin formulas
                formula = Formula()

                # bin radiances according to latitude and longitude
                parameters = ['radiance', 'latitude', 'longitude']
                def hoarding(radiance, latitude, longitude): return self._hoard(radiance, latitude, longitude)
                formula.formulate(parameters, hoarding, 'radiance_geode')

                # perform cascade to calculate all new features
                features = self.cascade(formula, members + processor)

                # update route with Categories designation and link designation
                categories = self.apply(lambda feature: 'IndependentVariables' not in feature.slash, features=features)
                [feature.divert('Categories') for feature in categories]
                [feature.update({'link': True}) for feature in categories]

                # create destination name and stash featues
                now = self._note()
                stage = self._stage(path)
                orbit = stage['orbit']
                date = stage['date']
                destination = '{}/geodes/OMI_Geode_{}_{}-o{}_{}.h5'.format(self.sink, band, date, orbit, now)
                self._stash(features, destination, 'Data')

        return None