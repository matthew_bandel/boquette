# overwrite_irradiance.py to change the metadata orbit numbers in Level1B OMI .he4 files

# import general tools
import sys
import os
import re
import shutil

# import pyhdf to handle Collection 3 hdf4 data
from pyhdf.HDF import HDF, HDF4Error, HC
from pyhdf.SD import SD, SDC, SDAttr
from pyhdf.V import V
from pyhdf.error import HDF4Error


# class IrradianceOverwriter to replace hdf4 irradiance data
class IrradianceOverwriter(list):
    """IrradianceOverwriter class to swap orbit numbers in OMI Level1B .he4 files.

    Inherits from:
        list
    """

    def __init__(self, path='', mantissa='', exponent=''):
        """Initialize a IrradianceOverwriter instance.

        Arguments:
            destination: new file name
            mantissa: numpy.array
            exponent: numpy.array

        Returns:
            None
        """

        # set path information
        self.path = path

        # set arrays
        self.mantissa = mantissa
        self.exponent = exponent

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < IrradianceOverwriter instance at: {} >'.format(self.source)

        return representation

    def _insert(self, array, destination, name, category):
        """Insert a feature into an hdf4 file.

        Arguments:
            array: numpy array,
            destination: str, filepath
            name: str, name of science
            category: list of str, address of group

        Returns:
            None
        """

        # begin hdf4 file
        four = HDF(destination, HC.WRITE | HC.CREATE)
        science = SD(destination, SDC.WRITE | SDC.CREATE)
        groups = four.vgstart()
        tables = four.vstart()

        # scan for registry
        registry = list(self._scan(four).items())

        # sort for best match
        registry.sort(key=lambda pair: category == pair[1], reverse=True)
        identity = registry[0][0]

        # find the correctly named sd
        group = groups.attach(identity)
        tags = group.tagrefs()
        for tag, number in tags:

            # try to
            try:

                # get attributes
                dataset = science.select(science.reftoindex(number))
                nameii, rank, dims, type, _ = dataset.info()

                # insert new data
                if nameii == name:

                    # insert
                    print('inserting {}...'.format(name))
                    dataset[:] = array[0]

            # otherwise
            except HDF4Error as error:

                # alert
                pass
                #self._print(error)

        # close all apis
        groups.end()
        tables.end()
        science.end()
        four.close()

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        # # go through each meassage
        # for message in messages:
        #
        #     # print
        #     print(message)

        return message

    def _scan(self, four):
        """Scan all reference ids from the hdf4 file.

        Arguments:
            four: hdf4 file pointer.

        Returns:
            list of ints
        """

        # begin list of references
        references = []

        # open HDF4 group instance
        groups = four.vgstart()

        # begin with -1 and loop through references
        reference = -1
        while True:

            # try to
            try:

                # get new reference number by feeding in old
                reference = groups.getid(reference)
                references.append(reference)

            # unless the end is reached
            except HDF4Error:

                # in which case close the groups and break
                break

        # get the name of each member
        names = [groups.attach(reference)._name for reference in references]

        # try to
        try:

            # get terminal index where internal groups occur
            terminus = names.index('RIG0.0')
            names = names[:terminus]
            references = references[:terminus]

        # unless abscent
        except ValueError:

            # in which case, keep all names
            pass

        # make registry of routes
        registry = {reference: [name] for reference, name in zip(references, names)}

        # go through each referencre
        for reference, name in zip(references, names):

            # search for tags
            tags = groups.attach(reference).tagrefs()
            if all([tag[0] == HC.DFTAG_VG for tag in tags]):

                # append group name to beginning
                for tag in tags:

                    # add group name
                    registry[tag[1]] = [name] + registry[tag[1]]

                # delete from registry
                del(registry[reference])

        # close groups
        groups.end()

        return registry

    def overwrite(self):
        """Overwrite hdf4 irradiance data with external mantissa and exponent.

        Arguments:
            path: str, file path
            orbit: int, new orbit number

        Returns:
            None
        """

        # get the file path and destination
        path = self.path

        # get the arrays
        mantissa = self.mantissa
        exponent = self.exponent

        # identify groups by shape
        groups = {159: ['Sun Volume UV-1 Swath', 'Data Fields']}
        groups.update({557: ['Sun Volume UV-2 Swath', 'Data Fields']})
        groups.update({751: ['Sun Volume VIS Swath', 'Data Fields']})

        # grap appropriate group
        group = groups[mantissa.shape[-1]]

        # insert mantissa
        name = 'IrradianceMantissa'
        self._print('inserting {} into {}...'.format(name, group))
        self._insert(mantissa, path, name, group)

        # insert exopnent
        name = 'IrradianceExponent'
        self._print('inserting {} into {}...'.format(name, group))
        self._insert(exponent, path, name, group)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 3
    arguments = arguments[:3]

    # umpack arguments
    path, mantissa, exponent = arguments

    # create changer instance
    overwriter = IrradianceOverwriter(path, mantissa, exponent)

    # perform overwrite
    overwriter.overwrite()
