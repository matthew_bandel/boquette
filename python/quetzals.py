#!/usr/bin/env python3

# quetzals.py for the Quetzal class to build omi climatology

# import local classes
from hydras import Hydra
from squids import Squid

# import system
import sys

# import numpy functions
import numpy

# import datetime
import datetime

# import random forest
from sklearn.svm import OneClassSVM
from sklearn.decomposition import PCA

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# imoprt cartopy
import cartopy

# import PIL
import PIL


# class Quetzal to do OMOCNUV analysis
class Quetzal(Hydra):
    """Quetzal class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # create squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Quetzal instance qt {} >'.format(self.sink)

        return representation

    def _configure(self, features, yam=None):
        """Create yaml file for configuring exclusions.

        Arguments:
            features: list of features
            yam: str, path to yaml file

        Returns:
            list of str, the exclusions
        """

        # begin with empty exclusions
        exclusions = []

        # if yaml given
        if yam:

            # load yanl
            fields = self._acquire(yam)

            # if yaml is emtpy
            if not fields:

                # get all names
                names = [feature.name for feature in features]
                names.sort()

                # default all field names to inclusion
                inclusions = {name: '+' for name in names}

                # stash yam
                self._dispense(inclusions, yam)

            # get exclusions
            exclusions = [field for field, status in fields.items() if status.endswith('x') or status.endswith('X')]

        return exclusions

    def _prepare(self, hydra, yam=None):
        """Prepare training data.

        Arguments:
            hydra: hdf reader
            yam: str, path to yaml file

        Returns:
            list of str, numpy array, numpy array tuple ( features, matrix, normalization )
        """

        # gather all data
        data = {feature.slash: feature.distil().squeeze() for feature in hydra}

        # gather all shapes
        shapes = [array.shape for array in data.values()]

        # group all two-d shapes, and find the most common
        twos = self._group([shape for shape in shapes if len(shape) == 2], lambda shape: shape)
        clusters = list(twos.values())
        clusters.sort(key=lambda cluster: len(cluster))
        common = clusters[-1][0]

        # begin training dtaa
        train = {}

        # get exclusions from yaml
        exclusions = self._configure(list(hydra), yam)

        # add all two-d arrays to
        for address, array in data.items():

            # if not in exclusions
            if self._file(address) not in exclusions:

                # if shape is common
                if array.shape == common:

                    # add to training
                    train[address] = array

                # if array is three-d
                if len(array.shape) == 3 and array.shape[:2] == common:

                    # determine halfway
                    half = int(array.shape[2] / 2)
                    train[address] = array[:, :, half]

        # add array for shapes
        train['track'] = numpy.array([list(range(common[0]))] * common[1]).transpose(1, 0)
        train['row'] = numpy.array([list(range(common[1]))] * common[0])

        # assemble the matrix
        matrix = numpy.array([array.flatten() for array in train.values()]).transpose(1, 0)
        features = list(train.keys())

        # eliminate nas
        undefined = numpy.logical_not(numpy.isfinite(matrix).sum(axis=1)) < 1
        matrix = matrix[undefined]

        # eliminate fill values
        valid = ((abs(matrix) > 1e20) | (matrix == -999) | (matrix == -9999)).sum(axis=1) < 1
        matrix = matrix[valid]

        # normalize each column
        mean = matrix.mean(axis=0)
        deviation = matrix.std(axis=0)

        # replace deviations with 1
        deviation = numpy.where(deviation == 0, 1, deviation)

        # normalize
        normalization = (matrix - mean) / deviation

        return features, matrix, normalization

    def ask(self, orbit, latitude, label=None):
        """Ask about the details of an event.

        Arguments:
            orbit: int, orbit number
            latitude: float, latitude
            label: str, key word of error label

        Returns:
            None
        """

        # grab the file
        names = self._see('{}/records'.format(self.sink))
        names = [name for name in names if str(orbit) in name]

        # grab the records
        records = self._load(names[0])['outliers']
        records = list(records.values())

        # get subset +/- 10 latitude
        records = [record for record in records if abs(record['Latitude'] - latitude) < 10]

        # if error given
        if label:

            # only get those with error in key
            records = [record for record in records if label in record['error']]

        # sort by sigmas
        records.sort(key=lambda record: record['sigmas'])

        # print
        for index, record in enumerate(records):

            # print
            self._print('')
            self._print('record {} of {}'.format(index, len(records)))
            self._look(record, 3)

        return None

    def inspect(self, path, yam=None, limit=3, number=5):
        """Inspect the data of a file.

        Arguments:
            path: str, pathname to hdf5 file
            words: unpacked list of search words
            limit: z-score limit

        Returns:
            None
        """

        # ingest the path
        hydra = Hydra(path)
        hydra.ingest()

        # begin report
        stage = hydra._stage(path)
        formats = [stage[field] for field in ('product', 'orbit', 'day')]
        report = ['outlier report for {}, o{}, {}'.format(*formats)]

        # set tab spacig
        tab = ' ' * 5

        # sort by name
        features = list(hydra)
        features.sort(key=lambda feature: feature.name)

        # get exclusions from yaml
        exclusions = self._configure(features, yam)

        # remove exclusions
        features = [feature for feature in features if feature.name not in exclusions]

        # begin records
        records = []

        # for each feature
        for feature in features:

            # extract the data
            array = feature.distil()

            # begin recrod
            record = {'nan': 0, 'nans': [], 'fill': 0, 'fills': [], 'high': 0, 'highs': [], 'error': ''}
            record.update({'mean': 0, 'median': 0, 'deviation': 0})
            record.update({'name': feature.name, 'address': feature.slash})

            # try
            try:

                # get nan mask
                undefined = numpy.logical_not(numpy.isfinite(array))
                record.update({'nan': undefined.sum()})

                # get pixels
                pixels = hydra._pin(1, undefined, min([number, undefined.sum()]))
                record.update({'nans': pixels})

                # check for fills
                fill = (abs(array) > 1e20) | (array == -999) | (array == -9999)
                record.update({'fill': fill.sum()})

                # get pixels
                pixels = hydra._pin(1, fill, min([number, fill.sum()]))
                record.update({'fills': (pixels, [array[pixel] for pixel in pixels])})

                # check valid data
                masque = numpy.logical_not(undefined | fill)
                valid = array[masque]

                # calculate mean, median, and std
                mean = valid.mean()
                median = numpy.quantile(valid, 0.5)
                deviation = valid.std()
                record.update({'mean': self._round(mean)})
                record.update({'median': self._round(median)})
                record.update({'deviation': self._round(deviation)})

                # calculate z scores, marking fill and nan as 0
                score = abs((array - mean) / deviation)
                score = numpy.where(masque, score, 0)
                record.update({'high': (score > limit).sum()})

                # get pixels
                pixels = hydra._pin(score.max(), score, min([number, (score > limit).sum()]))
                record.update({'highs': (pixels, [array[pixel] for pixel in pixels])})

            # unless an error
            except (IndexError, TypeError) as error:

                # print error
                record.update({'error': error})

            # add record
            records.append(record)

        # sort records
        records.sort(key=lambda record: record['fill'], reverse=True)
        records.sort(key=lambda record: record['nan'], reverse=True)
        records.sort(key=lambda record: record['high'], reverse=True)

        # for each feature
        for record in records:

            # add to report
            report.append('')
            report.append(self._print('{}:'.format(record['name'])))
            report.append(self._print('{}:'.format(record['address'])))

            # add fill info
            report.append(self._print('fill elements: {}'.format(record['fill'])))
            zipper = zip(*record['fills'])
            [report.append(self._print('{} {}: {}'.format(tab, pixel, quantity))) for pixel, quantity in zipper]

            # add nan info
            report.append(self._print('nan elements: {}'.format(record['nan'])))
            [report.append(self._print('{} {}'.format(tab, pixel))) for pixel in record['nans']]

            # calculate mean, median, and std
            report.append(self._print('mean: {}'.format(record['mean'])))
            report.append(self._print('median: {}'.format(record['median'])))
            report.append(self._print('stdev: {}'.format(record['deviation'])))

            # add fill info
            report.append(self._print('elements with z-score > {}: {}'.format(limit, record['high'])))
            zipper = zip(*record['highs'])
            [report.append(self._print('{} {}: {}'.format(tab, pixel, quantity))) for pixel, quantity in zipper]

            # add error
            report.append(self._print('error: {}'.format(record['error'])))

        # jot down report
        destination = '{}/reports/QC_Inspection_{}_{}_{}.txt'.format(self.sink, *formats)
        self._jot(report, destination)

        return None

    def elucidate(self, path, yam=None, limit=3, number=5):
        """Inspect the data of a file.

        Arguments:
            path: str, pathname to hdf5 file
            limit: z-score limit
            number: int, number of entries to print

        Returns:
            None
        """

        # ingest the path
        hydra = Hydra(path)
        hydra.ingest()

        # get file details
        stage = hydra._stage(path)
        formats = [stage[field] for field in ('product', 'orbit', 'day')]

        # prepare data
        features, matrix, normalization = self._prepare(hydra, yam)

        # caculate euclidian distances
        prediction = -(normalization ** 2).sum(axis=1)

        # get order of predictions
        order = numpy.argsort(prediction)

        # begin report
        report = ['euclidian report for {}, o{}, {}'.format(*formats)]

        # for each index in order
        for index in order[:number]:

            # rank decisions
            rank = numpy.argsort(abs(normalization[index])).flatten().tolist()
            rank.reverse()

            # add space
            report.append(self._print(' '))

            # find latitude and longitude
            latitude = [index for index, feature in enumerate(features) if 'latitude' in feature.lower()][0]
            longitude = [index for index, feature in enumerate(features) if 'longitude' in feature.lower()][0]

            # add score
            track = features.index('track')
            row = features.index('row')
            pixel = (matrix[index, track], matrix[index, row])
            location = (self._round(matrix[index, latitude]), self._round(matrix[index, longitude]))
            report.append(self._print('score: {}, {}, {}'.format(self._round(prediction[index]), pixel, location)))

            # for each feature
            for indexii in rank[:5]:

                # add value to report
                details = [self._round(normalization[index, indexii])]
                details += [self._file(features[indexii]), self._round(matrix[index, indexii])]
                report.append(self._print('({}) {}: {}'.format(*details)))

        # jot down report
        destination = '{}/reports/QC_Euclidian_{}_{}_{}.txt'.format(self.sink, *formats)
        self._jot(report, destination)

        #  apply PCA to matrix
        machineii = PCA(n_components=2)
        flat = machineii.fit_transform(normalization)

        # plot interior
        address = 'plots/PCA_Euclidian_interior.h5'
        title = 'PCA, interior points'
        self.squid.ink('inside', flat[:, 0], 'PCA', flat[:, 1], address, title)

        # for each index in order
        for position, index in enumerate(order[:number]):

            # plot point
            address = 'plots/PCA_Euclidian_z_{}.h5'.format(position)
            title = 'PCA, chosen point'
            self.squid.ink('point', [flat[index, 0]] * 2, 'PCA', [flat[index, 1]] * 2, address, title)

        return None

    def analyze(self, path, yam=None):
        """Perform QC analysis on a file, with option yaml configuration.

        Arguments:
            path: str, file pathname
            yam: str, yaml file pathname

        Returns:
            None
        """

        # use one class support vector machine
        self.support(path, yam)

        # use euclidian measurements
        self.elucidate(path, yam)

        # check each parameter
        self.inspect(path, yam)

        return None

    def hiss(self, year, month, day, archive):
        """Make histograms of residuals.

        Arguments:
            year: int, year
            month: int, month,
            day: int, day
            archive: int, archive set

        Returns:
            None
        """

        # create folder
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/histograms'.format(self.sink))

        # construct hydra
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))

        # get paths
        paths = hydra.paths

        # set wavelength filed name, assuming V9
        wavelengths = 'wavelength'

        # if V8, make adjustments
        if paths[0].endswith('.he5'):

            # remove mets
            paths = [path for path in paths if not path.endswith('met')]

            # set wavelength field
            wavelengths = 'Wavelength'

        # gather up residuals
        residuals = []
        algorithms = []
        for path in paths:

            # print status
            self._print('collecting {}'.format(path))

            # ingest
            hydra.ingest(path)

            # get residuals and append
            residual = hydra.grab('ResidualStep1')
            residuals.append(residual)

            # get algorithm flags
            algorithm = hydra.grab('AlgorithmFlags')
            algorithms.append(algorithm)

        # make array
        residuals = numpy.vstack(residuals)
        algorithms = numpy.vstack(algorithms)

        # grab wavelengths
        waves = hydra.grab(wavelengths)

        # set study wavelengths
        study = [312.5, 317.5, 322, 331]

        # set algorithm flags
        flags = [(1, 11), (2, 12)]

        # for each wavelength
        for wave in study:

            # and each algoirithm pair
            for pair in flags:

                # print status
                self._print('algorithm: {}, wavelength: {}...'.format(pair, wave))

                # find the index for the wavelength
                position = self._pin(wave, waves)[0][0]

                # get the array for a histogram
                array = residuals[:, :, position]

                # set pair mask for algorithm, and remove fills
                mask = (algorithms == pair[0]) | (algorithms == pair[1])
                mask = mask & (abs(array) < 1e20)

                # apply mask
                array = array[mask]

                # create histogram
                nanometers = int(self._round(waves[position], 0))
                formats = (nanometers, archive, year, self._pad(month), self._pad(day), pair[0], pair[1])
                title = 'Step1 Residual histogram, {}nm, AS {}, {}m{}{}, Alg {} / {}'.format(*formats)
                folder = 'plots/histograms/Residuals_{}nm_AS{}_{}m{}{}_alg{}_{}.h5'.format(*formats)
                name = 'residuals_{}'.format(nanometers)
                self.squid.ripple(name, array, folder, title)

        return None

    def osmose(self, path, reservoir=None, number=5):
        """QC OMTO3 file.

        Arguments
            path: filepath to ozone file
            reservoir: json file of qc information
            number: int, number of outliers to keep

        Returns:
            None
        """

        # ingest hydra
        hydra = Hydra(path)
        hydra.ingest()

        # get latitude and ozone
        latitude = hydra.grab('Latitude').squeeze()
        ozone = hydra.grab('ColumnAmountO3').squeeze()

        # create row and columns
        rows = numpy.array([list(range(ozone.shape[1]))] * ozone.shape[0])
        columns = numpy.array([list(range(ozone.shape[0]))] * ozone.shape[1]).transpose(1, 0)

        # create data, and flatten all entries
        data = {'latitude': latitude,  'ozone': ozone, 'row': rows, 'column': columns}

        # remove fill values and nans
        mask = (numpy.isfinite(ozone)) & (abs(ozone) < 1e20)
        data = {name: array[mask] for name, array in data.items()}

        # flatten
        data = {name: array.flatten() for name, array in data.items()}
        features = ['ozone', 'latitude']

        # create matrix
        matrix = numpy.vstack([data['ozone'], data['latitude']]).transpose(1, 0)

        # normalize each column
        mean = matrix.mean(axis=0)
        deviation = matrix.std(axis=0)

        # replace deviations with 1
        deviation = numpy.where(deviation == 0, 1, deviation)

        # normalize
        normalization = (matrix - mean) / deviation

        # perform model fit
        self._stamp('fitting SVM to {}...'.format(matrix.shape), initial=True)
        machine = OneClassSVM(gamma=0.9, kernel='rbf', degree=3, nu=0.02)
        machine.fit(normalization)
        self._stamp('fit.')

        # predict
        self._stamp('predicting distances...', initial=True)
        prediction = machine.decision_function(normalization)
        self._stamp('predicted.')

        # get order of predictions
        order = numpy.argsort(prediction)

        # # for each index in order
        # for index in order[:number]:
        #
        #     # create finite difference multiplier by 1%
        #     length = len(features)
        #     multiplier = numpy.ones((length, length))
        #     numpy.fill_diagonal(multiplier, 1.01)
        #
        #     # create matrix from particular sample and multiply in multiplier
        #     gradient = numpy.array([matrix[index]] * length)
        #     gradient = gradient * multiplier
        #
        #     # determine absolute differences in decisions
        #     differentials = abs(machine.decision_function(gradient) - prediction[index])
        #
        #     # rank decisions
        #     rank = numpy.argsort(differentials).flatten().tolist()
        #     rank.reverse()
            #
            # # find latitude and longitude
            # latitude = [index for index, feature in enumerate(features) if 'latitude' in feature.lower()][0]
            # longitude = [index for index, feature in enumerate(features) if 'longitude' in feature.lower()][0]
            #
            # # add score
            # track = features.index('track')
            # row = features.index('row')
            # pixel = (matrix[index, track], matrix[index, row])
            # location = (self._round(matrix[index, latitude]), self._round(matrix[index, longitude]))
            # report.append(self._print('score: {}, {}, {}'.format(self._round(prediction[index]), pixel, location)))

        #     # for each feature
        #     for indexii in rank[:5]:
        #
        #         # add value to report
        #         details = [self._round(numpy.log10(differentials[indexii] + 1e-11))]
        #         details += [self._file(features[indexii]), self._round(matrix[index, indexii])]
        #         report.append(self._print('({}) {}: {}'.format(*details)))
        #
        # # jot down report
        # destination = '{}/reports/QC_SVM_{}_{}_{}.txt'.format(self.sink, *formats)
        # self._jot(report, destination)

        # get class labels
        self._stamp('predicting labels...', initial=True)
        labels = machine.predict(normalization)
        self._stamp('predicted.')

        # #  apply PCA to matrix
        # machineii = PCA(n_components=2)
        # matrix = machineii.fit_transform(matrix)

        # plot interior
        interior = (labels == 1)
        address = 'plots/PCA_SVM_interior.h5'
        stage = self._stage(path)
        title = 'OMTO3, {}, o{}'.format(stage['day'], stage['orbit'])
        self.squid.ink('inside', matrix[:, 0][interior], 'PCA', matrix[:, 1][interior], address, title)

        # plot exterior
        exterior = (labels == -1)
        address = 'plots/PCA_SVM_yexterior.h5'
        # title = 'PCA, exterior points'
        self.squid.ink('outside', matrix[:, 0][exterior], 'PCA', matrix[:, 1][exterior], address, title)

        # for each index in order
        for position, index in enumerate(order[:number]):

            # plot point
            address = 'plots/PCA_SVM_z_{}.h5'.format(position)
            # title = 'PCA, chosen point'
            self.squid.ink('point', [matrix[index, 0]] * 2, 'PCA', [matrix[index, 1]] * 2, address, title)

        return None

    def pixelate(self, path, pixel):
        """Add data for a particular pixel to a reservoir.

        Arguments:
            path: str, filepath for file
            pixel: tuple of int, the particular pixel

        Returns:
            dict
        """

        # create hydra
        hydra = Hydra(path)
        hydra.ingest()

        # get features
        features = [feature for feature in hydra]
        features.sort(key=lambda feature: feature.name)

        # determine most common 2-d shape
        shapes = [feature.shape for feature in hydra if len(feature.shape) == 2]
        shapes = self._group(shapes, lambda shape: shape)
        shapes = list(shapes.items())
        shapes.sort(key=lambda pair: len(pair[1]))
        shape = shapes[-1][0]

        # begin record
        record = {}

        # sort features by dimension
        ones = [feature for feature in features if len(feature.shape) == 1 and feature.shape[0] == shape[0]]
        twos = [feature for feature in features if len(feature.shape) == 2 and feature.shape == shape]
        threes = [feature for feature in features if len(feature.shape) == 3 and feature.shape[:2] == shape]

        # for one dimension feature
        for feature in ones:

            # add to record
            record[feature.name] = feature.distil()[pixel[0]]

        # for two dimensional features
        for feature in twos:

            # check for initial shape match
            record[feature.name] = feature.distil()[pixel]

        # for three dimensional features
        for feature in threes:

            # check for initial shape match
            record[feature.name] = feature.distil()[pixel].tolist()

        return record

    def qualify(self, year, month, day, orbits=16, archive=95042, sigmas=4):
        """Produce qa qc plots for a day of OMTO3 orbits.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbits: int, number of orbits
            archive: int, the archive set
            sigmas: float, number of stdev window

        Returns:
            None
        """

        # create hydra for orbits
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))

        # set up fields
        fields = {'ozone': 'ColumnAmountO3', 'reflectivity': 'Reflectivity331', 'reflectivityii': 'Reflectivity360'}
        fields.update({'quality': 'QualityFlags', 'latitude': 'Latitude', 'algorithm': 'AlgorithmFlags'})
        fields.update({'longitude': 'Longitude'})

        # set units
        units = {'ozone': 'ozone ( DU )', 'reflectivity': 'reflectivity', 'reflectivityii': 'reflectivity'}

        # set error codes
        errors = {'good_sample': 0, 'glint_corrected': 1, 'sza_over_84': 2}
        errors.update({'residual_360_threshold': 3, 'residual_unused_4_sigma': 4})
        errors.update({'soi_4_sigma': 5, 'nonconvergence': 6, 'abs_residual_over_16': 7})

        # set hexes
        hexes = {'green': '#008000', 'yellow': '#ffff00', 'orange': '#ffa500', 'blue': '#0000ff'}
        hexes.update({'cyan': '#00ffff', 'magenta': '#ff00ff', 'orchid': '#da70d6', 'darkblue': '#00008b'})
        hexes.update({'black': '#000000', 'red': '#ff0000', 'crimson': '#dc143c', 'orangered': '#ff4500'})
        hexes.update({'pink': '#ffc0cb', 'violet': '#ee82ee', 'turquoise': '#40e0d0'})
        hexes.update({'lime': '#00ff00', 'chartreuse': '#7fff00', 'coral': '#ff7f50'})
        hexes.update({'gold': '#ffd700', 'goldenrod': '#daa520', 'lightblue': '#add8e6'})
        hexes.update({'purple': '#800080', 'skyblue': '#87ceeb', 'yellowgreen': '#9acd32'})

        # set palette
        palette = {'good_sample': hexes['green'], 'glint_corrected': hexes['lime'], 'sza_over_84': hexes['yellowgreen']}
        palette.update({'residual_360_threshold': hexes['blue'], 'residual_unused_4_sigma': hexes['cyan']})
        palette.update({'soi_4_sigma': hexes['magenta'], 'nonconvergence': hexes['orchid']})
        palette.update({'abs_residual_over_16': hexes['black']})
        palette.update({'center': hexes['black'], 'margin': hexes['black']})
        palette.update({'ozone_nan': hexes['red'], 'r331_nan': hexes['crimson'], 'r360_nan': hexes['orangered']})
        palette.update({'good_ozone_fill': hexes['orange'], 'good_ozone_<100_>600': hexes['coral']})
        palette.update({'good_r331_<0_>100': hexes['gold'], 'good_r360_<0_>100': hexes['goldenrod']})
        palette.update({'flag_ozone_fill': hexes['cyan'], 'flag_ozone_<100_>600': hexes['turquoise']})
        palette.update({'flag_r331_<0_>100': hexes['lime'], 'flag_r360_<0_>100': hexes['chartreuse']})
        palette.update({'out_of_range': hexes['red']})

        # for each path
        paths = [path for path in hydra.paths if path.endswith('.he5')]
        for path in paths[:orbits]:

            # get details
            orbit = self._stage(path)['orbit']
            date = self._stage(path)['day']

            # begin record
            record = {'orbit': orbit, 'date': date, 'outliers': {}}

            # ingest and grab data
            hydra.ingest(path)
            original = {field: hydra.grab(search) for field, search in fields.items()}

            # add row and column arrays to origianls
            shape = original['latitude'].shape
            rows = numpy.array([numpy.linspace(0, shape[0] - 1, shape[0])] * shape[1]).transpose(1, 0)
            columns = numpy.array([numpy.linspace(0, shape[1] - 1, shape[1])] * shape[0])
            original['rows'] = rows.astype(int)
            original['columns'] = columns.astype(int)
            fields['rows'] = 'Row'
            fields['columns'] = 'Column'

            # weed out row anomaly, nans, and fill values
            mask = ((original['quality'] & 64) == 0) & (abs(original['ozone']) < 1e20)
            mask = mask & (numpy.isfinite(original['ozone']))
            data = {field: array[mask] for field, array in original.items()}

            # create ascending and descending masks
            quality = data['quality'] & 15
            ascending = {error: (quality < 10) & ((quality % 10) == code) for error, code in errors.items()}
            descending = {error: (quality >= 10) & ((quality % 10) == code) for error, code in errors.items()}

            # set mask for row anomaly
            mask = ((original['quality'] & 64) == 0)
            dataii = {field: array[mask] for field, array in original.items()}

            # set quality
            qualityii = dataii['quality'] & 15

            # set alarms for ozone and reflectivity conditions
            alarms = {}
            alarms.update({'r331_<0_>100': (dataii['reflectivity'] <= 0) | (dataii['reflectivity'] >= 105)})
            alarms.update({'r360_<0_>100': (dataii['reflectivityii'] <= 0) | (dataii['reflectivityii'] >= 105)})
            alarms.update({'ozone_<100_>600': (dataii['ozone'] <= 100) | (dataii['ozone'] >= 600)})
            alarms.update({'ozone_fill': (abs(dataii['ozone'] > 1e20)) & ((dataii['algorithm'] % 10) > 0)})

            # add good and flag versions of alarms to extremes
            extremes = {}
            # extremes.update({'flag_{}'.format(name): mask & (qualityii > 1) for name, mask in alarms.items()})
            extremes.update({'good_{}'.format(name): mask & (qualityii < 2) for name, mask in alarms.items()})

            # set extreme nan masks
            extremes.update({'r331_nan': numpy.logical_not(numpy.isfinite(dataii['reflectivity']))})
            extremes.update({'r360_nan': numpy.logical_not(numpy.isfinite(dataii['reflectivityii']))})
            extremes.update({'ozone_nan': numpy.logical_not(numpy.isfinite(dataii['ozone']))})

            # set modes
            modes = {'absolute': lambda array, mean: array}
            modes.update({'deviation': lambda array, mean: array - mean})
            # modes.update({'ratio': lambda array, mean: array / mean})
            brackets = {'absolute': lambda mean, deviation: deviation}
            brackets.update({'deviation': lambda mean, deviation: deviation})
            # brackets.update({'ratio': lambda mean, deviation: deviation / mean})
            bases = {'absolute': lambda mean: mean, 'deviation': lambda mean: 0, 'ratio': lambda mean: 1}
            limits = {'absolute': (100, 650), 'deviation': (-100, 100), 'ratio': (0.5, 1.5)}
            heights = {'absolute': 640, 'deviation': 95, 'ratio': 95}

            # set widths
            widths = {'absolute': 8, 'deviation': 4, 'ratio': 4, 'extreme': 8, 'legend': 4}

            # for each field of ozone, reflectivities
            for field in ('ozone',):

                # set dependent labels
                dependents = {'absolute': units[field], 'deviation': '', 'ratio': ''}

                # set titles
                formats = (fields[field], date, orbit, sigmas)
                titles = {'absolute': '{}, {}, o{}, +/- {} sigma'.format(*formats)}
                titles.update({'deviation': '+/- {} sigma'.format(sigmas)})
                titles.update({'ratio': '+/- {} sigma / mean'.format(sigmas)})

                # begin destinations
                destinations = []

                # for each mode
                for mode in modes.keys():

                    # create destinatation
                    formats = (self.sink, fields[field], date, orbit, mode)
                    destination = '{}/zones/OMTO3_QAQC_{}_{}_o{}_{}.png'.format(*formats)
                    destinations.append(destination)

                    # begin plot
                    pyplot.clf()
                    pyplot.figure(figsize=(widths[mode], 5))
                    pyplot.margins(0.3, 1.0)
                    pyplot.title(titles[mode])
                    pyplot.xlabel('latitude')
                    pyplot.ylabel(dependents[mode])
                    pyplot.xlim(-90, 90)
                    pyplot.ylim(*limits[mode])

                    # set markersize
                    size = 4

                    # create labels, masks, and colors
                    masques = list(descending.values()) + list(ascending.values())
                    labels = ['descending_{}'.format(error) for error in errors.keys()]
                    labels += ['ascending_{}'.format(error) for error in errors.keys()]
                    colors = [palette[error] for error in errors.keys()] * 2
                    markers = ['o'] * len(errors) + ['D'] * len(errors)

                    # revise abscissa and ordinate for good ascending data only
                    abscissa = data['latitude'][ascending['good_sample']]
                    ordinate = data[field][ascending['good_sample']]

                    # create orbital zonal means
                    half = 5
                    width = 10
                    points = [90 - (width + index * width - half) for index in range(18)]
                    arrays = [ordinate[(abscissa >= point - half) & (abscissa <= point + half)] for point in points]
                    means = [array.mean() for array in arrays]
                    deviations = [array.std() for array in arrays]

                    # for each mean
                    for point, mean, deviation in zip(points, means, deviations):

                        # for each style
                        for masque, color, marker, label in zip(masques, colors, markers, labels):

                            # grab masked abscissas
                            abscissaii = data['latitude'][masque]
                            ordinateii = data[field][masque]

                            # generate latitude zone from abscissa
                            zone = (abscissaii >= point - half) & (abscissaii <= point + half)

                            # shift according to mode
                            shift = modes[mode](ordinateii[zone], mean)

                            # plot data
                            parameters = {'color': color, 'label': label, 'markersize': size, 'marker': marker}
                            parameters.update({'linestyle': "None"})
                            pyplot.plot(abscissaii[zone], shift, **parameters)

                            # if absolute
                            if mode == 'absolute':

                                # get set of points greater than four sigma
                                scores = abs((ordinateii[zone] - mean) / deviation)
                                outside = (scores > sigmas)

                                # get info about outsiders
                                outsiders = {name: array[masque][zone][outside] for name, array in data.items()}

                                # for each index
                                for index, score in enumerate(scores[outside]):

                                    # get pixel
                                    pixel = (int(outsiders['rows'][index]), int(outsiders['columns'][index]))

                                    # add to record
                                    details = {fields[name]: float(array[index]) for name, array in outsiders.items()}
                                    details.update({'sigmas': float(score), 'error': label})
                                    details.update({'date': date, 'orbit': orbit})
                                    record['outliers'].update({str(pixel): details})

                        # set horizontal extent
                        horizontal = [point - half, point + half]

                        # for each distance
                        margins = (sigmas, 0, -sigmas)
                        lines = ('margin', 'center', 'margin')
                        for margin, line in zip(margins, lines):

                            # plot the line
                            vertical = [bases[mode](mean) + margin * brackets[mode](mean, deviation)] * 2
                            parameters = {'color': palette[line], 'linestyle': 'solid'}
                            pyplot.plot(horizontal, vertical, **parameters)

                    # for each extreme mask
                    for name, mask in extremes.items():

                        # create latitude and longitude
                        longitude = dataii['longitude'][mask]
                        latitude = dataii['latitude'][mask]

                        # reconstruct longitude as limit
                        height = numpy.where(longitude, heights[mode], heights[mode])

                        # plot the marker
                        size = 7
                        marker = 'x'
                        parameters = {'color': palette['out_of_range'], 'label': name, 'markersize': size}
                        parameters.update({'marker': marker, 'linestyle': "None"})
                        pyplot.plot(latitude, height, **parameters)

                        # get info about outsiders
                        outsiders = {name: array[mask] for name, array in dataii.items()}

                        # for each index
                        for index, _ in enumerate(outsiders['rows']):

                            # get pixel
                            pixel = (int(outsiders['rows'][index]), int(outsiders['columns'][index]))

                            # add to record
                            details = {fields[name]: float(array[index]) for name, array in outsiders.items()}
                            details.update({'sigmas': 100, 'error': name, 'date': date, 'orbit': orbit})
                            record['outliers'].update({str(pixel): details})

                    # save figure and clear
                    self._print('saving {}...'.format(destination))
                    pyplot.savefig(destination)
                    pyplot.clf()

                # create destinatation for extremes
                formats = (self.sink, fields[field], date, orbit)
                destination = '{}/zones/OMTO3_QAQC_{}_{}_o{}_extreme.png'.format(*formats)
                destinations.append(destination)

                # begin map
                pyplot.clf()
                pyplot.figure(figsize=(widths['extreme'], 5))
                pyplot.margins(0.5, 1.0)

                # set cartopy axis with coastlines
                axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
                axis.coastlines()
                _ = axis.gridlines(draw_labels=True)
                axis.set_aspect('auto')
                axis.set_global()

                # for each extreme mask
                for name, mask in extremes.items():

                    # create latitude and longitude
                    longitude = dataii['longitude'][mask]
                    latitude = dataii['latitude'][mask]

                    # plot the marker
                    size = 7
                    marker = 'x'
                    parameters = {'color': palette['out_of_range'], 'label': name, 'markersize': size}
                    parameters.update({'marker': marker, 'linestyle': "None"})
                    pyplot.plot(longitude, latitude, **parameters)

                # save figure and clear
                self._print('saving {}...'.format(destination))
                pyplot.savefig(destination)
                pyplot.clf()

                # for each legend
                for legend, reservoir in zip(('legend',), (ascending,)):

                    # set destination
                    formats = (self.sink, fields[field], date, orbit, legend)
                    destination = '{}/zones/OMTO3_QAQC_{}_{}_o{}_{}.png'.format(*formats)
                    destinations.append(destination)

                    # begin first legend plot
                    pyplot.clf()
                    pyplot.figure(figsize=(widths['legend'], 5))
                    pyplot.margins(0.2, 0.2)
                    pyplot.xlim(-1, 1)
                    pyplot.ylim(-1, 1)
                    pyplot.gca().get_xaxis().set_visible(False)
                    pyplot.gca().get_yaxis().set_visible(False)

                    # for each entry
                    for name in reservoir.keys():

                        # construct label
                        label = name.replace('_', ' ')

                        # plot marker in distance
                        parameters = {'color': palette[name], 'label': label, 'marker': 'D'}
                        parameters.update({'linestyle': "None"})
                        pyplot.plot([100, 100], [100, 100], **parameters)

                    # add extreme to legend
                    label = 'out_of_range'
                    parameters = {'color': palette[label], 'label': label, 'marker': 'x'}
                    parameters.update({'linestyle': "None"})
                    pyplot.plot([100, 100], [100, 100], **parameters)

                    # add legend
                    pyplot.legend(loc='upper left', prop={'size': 13})

                    # save figure and clear
                    self._print('saving {}...'.format(destination))
                    pyplot.savefig(destination)
                    pyplot.clf()

                # combine plots
                destinationii = destinations[0].replace('absolute', 'quartet')

                # for each plot
                arrays = []
                for destination in destinations:

                    # print the shape
                    image = PIL.Image.open(destination)
                    array = numpy.array(image)
                    arrays.append(array)

                # combine arrays into top and bottom
                top = numpy.hstack([arrays[0], arrays[1]])
                bottom = numpy.hstack([arrays[3], arrays[2]])

                # combine top and bottom
                quartet = numpy.vstack([top, bottom])

                # save to destination
                self._print('saving {}...'.format(destinationii))
                PIL.Image.fromarray(quartet).save(destinationii)

                # for each singlet
                for destination in destinations:

                    # erase
                    self._clean(destination, force=True)

            # store record
            destination = '{}/records/OMTO3_QAQC_Points_{}.json'.format(self.sink, self._pad(orbit, 6))
            self._dump(record, destination)

        return

    def sample(self, year, percent=3, divisions=4):
        """Sample omto3 records at random days in the year.

        Arguments:
            year: int, year
            percent: int, percent of data
            divisions: int, number of equal divisions

        Returns:
            None
        """

        # get percentage of days
        percentage = (percent / 100) * 365

        # divivde by divisions
        days = int(numpy.ceil(percentage / divisions))

        # make day blocks
        blocks = [int(index * 365 / divisions) for index in range(5)]
        brackets = [(one, two) for one, two in zip(blocks[:-1], blocks[1:])]

        # for each division
        for bracket in brackets:

            # and each day
            for day in range(days):

                # get random day
                random = int(bracket[0] + numpy.ceil(numpy.random.rand() * (bracket[1] - bracket[0])))

                # determine date
                date = datetime.datetime(year=year, month=1, day=1) + datetime.timedelta(days=random)

                # run qaqc
                self.qualify(date.year, date.month, date.day)

        return None

    def scatter(self, out=False):
        """Create scatter plots for all 4sigma ozone values.

        Arguments:
            out: boolean, return output?

        Returns:
            None
        """

        # make plots folder and scatter folder
        self._make('{}/plots'.format(self.sink))
        self._make('{}/plots/scatter'.format(self.sink))

        # get all files from reports folder
        paths = self._see('{}/reports'.format(self.sink))
        paths = [path for path in paths if 'sigma' in path]

        # begin records
        records = []

        # load up all paths
        for path in paths:

            # append to records
            records += self._load(path)

        # begin data
        data = {}

        # collect attributes
        data['ozone'] = [record['ColumnAmountO3'] for record in records]
        data['sigma'] = [record['sigmas'] for record in records]
        data['latitude'] = [record['latitude'] for record in records]
        data['longitude'] = [record['longitude'] for record in records]
        data['algorithm'] = [record['AlgorithmFlags'][0] for record in records]
        data['residue'] = [record['Residual'][10][1] for record in records]
        data['reflectance'] = [record['NValue'][10][1] for record in records]
        data['cloud'] = [record['CloudPressure'] for record in records]
        data['row'] = [record['row'] for record in records]
        data['reflectivity'] = [record['Reflectivity354'] for record in records]
        data['zenith'] = [record['solar_zenith_angle'] for record in records]

        # set up flags
        flags = {'quality': 'QualityFlags', 'ground': 'ground_pixel_quality'}
        flags.update({'measurement': 'measurement_quality'})

        # for each flag
        for flag, name in flags.items():

            # collect quality flags
            data[flag] = []
            for record in records:

                # try to
                try:

                    # append quality
                    data[flag].append(max(record[name][0]))

                # unless empty
                except ValueError:

                    # in which case, append -1 ( 0 flag is taken )
                    data[flag].append(-1)

        # make numpy arrays
        data = {name: numpy.array(array) for name, array in data.items()}

        # create generic title
        generic = '[], ( > 4 sigma ozone ), OMTO3, AS 96100, cardinal days 2005'

        # create names
        names = {'latitude': 'latitude', 'longitude': 'longitude', 'algorithm': 'algorithm flag'}
        names.update({'residue': 'residual 354nm', 'reflectance': 'n value 354nm'})
        names.update({'cloud': 'cloud pressure', 'reflectivity': 'reflectivity 354nm'})
        names.update({'row': 'row', 'zenith':'solar zenith angle', 'quality':'quality flag'})
        names.update({'ground': 'ground pixel quality flag', 'measurement': 'measurement quality flag'})

        # create latitude heat map
        name = 'latitude'
        bounds = [-90, -70, -50, -30, -10, 10, 30, 50, 70, 90]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'longitude'
        bounds = [-180, -140, -100, -60, -20, 20, 60, 100, 140, 180]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'algorithm'
        bounds = [ 0.5, 1.5, 2.5, 11.5, 12.5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'residue'
        bounds = [-5, -4, -3, -2, 1, 0, 1, 2, 3, 4, 5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'reflectance'
        bounds = [50, 70, 90, 110, 130, 150, 170, 190, 210]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'cloud'
        bounds = [-10000, 0, 200, 400, 600, 800, 1000, 1200, 1400, 1600]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'row'
        bounds = [-0.5, 0.5, 1.5, 2.5, 9.5, 19.5, 29.5, 39.5, 49.5, 59.5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'reflectivity'
        bounds = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'zenith'
        bounds = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'quality'
        bounds = [-1.5, -0.5, 0.5, 1.5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'ground'
        bounds = [-1.5, -0.5, 1.5, 2.5, 4.5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # create latitude heat map
        name = 'measurement'
        bounds = [-1.5, -0.5, 4.5]
        title = generic.replace('[]', names[name])
        parameters = [name, data[name], 'sigma', data['sigma'], 'ozone', data['ozone']]
        parameters += ['plots/scatter/heatmap_sigma_{}.h5'.format(name), bounds, title]
        self.squid.shimmer(*parameters)

        # view data
        self._view(data)

        # set up output
        output = None
        if out:

            # set to data
            output = data

        return output

    def support(self, path, yam=None, limit=3, number=5):
        """Inspect the data of a file.

        Arguments:
            path: str, pathname to hdf5 file
            limit: z-score limit
            number: int, number of entries to print

        Returns:
            None
        """

        # ingest the path
        hydra = Hydra(path)
        hydra.ingest()

        # get file details
        stage = hydra._stage(path)
        formats = [stage[field] for field in ('product', 'orbit', 'day')]

        # prepare data
        features, matrix, normalization = self._prepare(hydra, yam)

        # perform model fit
        self._stamp('fitting SVM...', initial=True)
        machine = OneClassSVM(gamma='scale', kernel='rbf', nu=0.05)
        machine.fit(normalization)
        self._stamp('fit.')

        # predict
        prediction = machine.decision_function(normalization)

        # get order of predictions
        order = numpy.argsort(prediction)

        # begin report
        report = ['svm report for {}, o{}, {}'.format(*formats)]

        # for each index in order
        for index in order[:number]:

            # create finite difference multiplier by 1%
            length = len(features)
            multiplier = numpy.ones((length, length))
            numpy.fill_diagonal(multiplier, 1.01)

            # create matrix from particular sample and multiply in multiplier
            gradient = numpy.array([normalization[index]] * length)
            gradient = gradient * multiplier

            # determine absolute differences in decisions
            differentials = abs(machine.decision_function(gradient) - prediction[index])

            # rank decisions
            rank = numpy.argsort(differentials).flatten().tolist()
            rank.reverse()

            # add space
            report.append(self._print(' '))

            # find latitude and longitude
            latitude = [index for index, feature in enumerate(features) if 'latitude' in feature.lower()][0]
            longitude = [index for index, feature in enumerate(features) if 'longitude' in feature.lower()][0]

            # add score
            track = features.index('track')
            row = features.index('row')
            pixel = (matrix[index, track], matrix[index, row])
            location = (self._round(matrix[index, latitude]), self._round(matrix[index, longitude]))
            report.append(self._print('score: {}, {}, {}'.format(self._round(prediction[index]), pixel, location)))

            # for each feature
            for indexii in rank[:5]:

                # add value to report
                details = [self._round(numpy.log10(differentials[indexii] + 1e-11))]
                details += [self._file(features[indexii]), self._round(matrix[index, indexii])]
                report.append(self._print('({}) {}: {}'.format(*details)))

        # jot down report
        destination = '{}/reports/QC_SVM_{}_{}_{}.txt'.format(self.sink, *formats)
        self._jot(report, destination)

        # get class labels
        labels = machine.predict(normalization)

        #  apply PCA to matrix
        machineii = PCA(n_components=2)
        flat = machineii.fit_transform(normalization)

        # plot interior
        interior = (labels == 1)
        address = 'plots/PCA_SVM_interior.h5'
        title = 'PCA, interior points'
        self.squid.ink('inside', flat[:, 0][interior], 'PCA', flat[:, 1][interior], address, title)

        # plot exterior
        exterior = (labels == -1)
        address = 'plots/PCA_SVM_yexterior.h5'
        title = 'PCA, exterior points'
        self.squid.ink('outside', flat[:, 0][exterior], 'PCA', flat[:, 1][exterior], address, title)

        # for each index in order
        for position, index in enumerate(order[:number]):

            # plot point
            address = 'plots/PCA_SVM_z_{}.h5'.format(position)
            title = 'PCA, chosen point'
            self.squid.ink('point', [flat[index, 0]] * 2, 'PCA', [flat[index, 1]] * 2, address, title)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # grab first argument
    year = int(arguments[0])

    # create quetzal instance
    quetzal = Quetzal('../studies/quetzal/quality')

    # run quality check
    quetzal.sample(year)
