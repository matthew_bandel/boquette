### OMTO3ZonalMeans

Produces Zonal Means from OMTO3 data

### Usage

Run from the src directory as follows, where $year, $month, and $archive are integers representing the year, month amd archive set for the OMTO3 data.  The $sink is the location for output files, which will populate a "collection" subdirectory for orbit level collection of sums and sums of squares, and a "zones" subdirectory for the monthly zonal means.

```buildoutcfg
OMTO3ZonalMeans/src $ python3 generate_omto3_zonal_means.py $year $month $archive $sink
```

For a quick test, setting a number of orbits will limit the number of orbital files pulled:

```buildoutcfg
OMTO3ZonalMeans/src $ python3 generate_omto3_zonal_means.py $year $month $archive $sink --orbits=3
```

Collecting one month takes some time on the TLCF as each file must be pulled over from /tis. Multiple collections may be run in parallel in the background using the following (tcsh shell):

```buildoutcfg
OMTO3ZonalMeans/src $ set sink = omto3/zonal_means
OMTO3ZonalMeans/src $ set year = 2005
OMTO3ZonalMeans/src $ set archive = 95042
OMTO3ZonalMeans/src $ foreach month ( 1 2 3 4 5 6 7 8 9 10 11 12 )
OMTO3ZonalMeans/src $ nice +19 python3 generate_omto3_zonal_means.py $year $month $archive $sink > $sink/logs/log_$year$month$archive.txt &
```

### Data screening

Data is screened according to the following criteria:

- Reflectivity 331 > 0 and < 100
- Algorithm Flag % 10 > 0 ( sample was not skipped, snow / ice allowed )
- Quality Flag & 15 == 0 | 1 ( bits 0 - 3 indicate good retrieval and not descending )
- Quality Flag & 64 == 0 ( bit 6 is 0, indicating no row anomaly )

Also, SO2Index and UVAerosol index still contained fill value after this initial screening.  Therefore, all data has been further screened for abs(data) <= 1e10, to remove fill values on the order of -1e30

### Additional Notes

- Reflectivities are divided by 100 to be on 0 to 1 scale
- Quality Flag is right shifted by 7 to denote 0 for OMI cloud source and 1 for Climatology cloud source

### Latitude bands

- Latitude bands are 36 latitude zones, 5 degrees each, beginning with the northern most band ( 85 N - 90 N) at index 0