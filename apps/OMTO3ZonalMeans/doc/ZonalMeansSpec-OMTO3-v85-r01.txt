Zonal Means Specification for V8.5 OMTO3 data 7/3/23

For each row, select data in 5 deg zonal mean bins

(QualityFlags & 15) == 0 | 1 # ascending good retrievals
(AlgorithmFlag % 10) > 0 # sample not skipped
(QualityFlags & 64) == 0 # row anomaly free
(ref_min < Reflectivity331/100.0 < ref_max) is True  (initial values: ref_min = 0.0; ref_max = 1.0)

Compute mean, standard deviation, and number of points for the following:

1-D fields:
Time (double)

2-D fields:
float((QualityFlags >> 7) & 1) 
float(AlgorithmFlags % 10) 
float(TerrainPressure)
CloudPressure
fc
Reflectivity331/100.0
Reflectivity360/100.0
StepOneO3
StepTwoO3
ColumnAmountO3
SO2index
UVAerosolIndex
Latitude
Longitude
RelativeAzimuthAngle
SolarZenithAngle
ViewingZenithAngle

3-D fields (all 12 wavelengths)
NValue
Residual
ResidualStep1
ResidualStep2
dN_dR
Sensitivity


