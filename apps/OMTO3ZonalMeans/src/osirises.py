#!/usr/bin/env python3

# osirises.py for the Osiris class to process OMTO3 data

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula
from squids import Squid

# import system
import sys

# import regex
import re

# import itertools
import itertools

# import numpy functions
import numpy
import math

# import datetime
import datetime

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False


# class Millipede to do OMI data reduction
class Osiris(Hydra):
    """Osiris class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # keep hydras
        self.hydras = []

        # set sink
        self.sink = sink

        # keep records reservoir
        self.reservoir = []

        # add squid
        self.squid = Squid(sink)

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Osiris instance at {} >'.format(self.sink)

        return representation

    def _compress(self, corners, trackwise, rowwise):
        """Compress the latitude or longitude bounds based on compression factors.

        Arguments:
            corners: dict of corner arrays
            trackwise: north-south compression factor
            rowwise: east-west compression factor

        Returns:
            numpy array
        """

        # set directions
        compasses = ['southwest', 'southeast', 'northeast', 'northwest']

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = corners[compass][:, :, 1]
        #     corners[compass][:, :, 1] = numpy.where(corner < 0, corner + 360, corner)

        # get shape
        shape = corners['southwest'].shape

        # get selected trackwise indices and rowwise indices
        selection = numpy.array([index for index in range(shape[0]) if index % trackwise == 0])
        selectionii = numpy.array([index for index in range(shape[1]) if index % rowwise == 0])

        # adjust based on factors
        adjustment = numpy.array([min([index + trackwise - 1, shape[0] - 1]) for index in selection])
        adjustmentii = numpy.array([min([index + rowwise - 1, shape[1] - 1]) for index in selectionii])

        # begin compression
        compression = {}

        # adjust southwest
        compression['southwest'] = corners['southwest'][selection]
        compression['southwest'] = compression['southwest'][:, selectionii]
        compression['southeast'] = corners['southeast'][selection]
        compression['southeast'] = compression['southeast'][:, adjustmentii]
        compression['northeast'] = corners['northeast'][adjustment]
        compression['northeast'] = compression['northeast'][:, adjustmentii]
        compression['northwest'] = corners['northwest'][adjustment]
        compression['northwest'] = compression['northwest'][:, selectionii]

        # # for each corner
        # for compass in compasses:
        #
        #     # shift negative longitudes to positive
        #     corner = compression[compass][:, :, 1]
        #     compression[compass][:, :, 1] = numpy.where(corner >= 180, corner - 360, corner)

        return compression

    def _cross(self, bounds):
        """Adjust for longitude bounds that cross the dateline.

        Arguments:
            bounds: numpy array of bounds

        Returns:
            numpy array
        """

        # for each image
        for image in range(bounds.shape[0]):

            # and each row
            for row in range(bounds.shape[1]):

                # get the bounds
                polygon = bounds[image][row]

                # check for discrepancy
                if any([(entry < -170) for entry in polygon]) and any([entry > 170 for entry in polygon]):

                    # adjust bounds
                    bounds[image][row] = numpy.where(polygon > 170, polygon, polygon + 360)

        return bounds

    def _frame(self, latitude, longitude):
        """Construct the corners of polygons from latitude and longitude coordinates.

        Arguments:
            latitude: numpy array
            longitude: numpy array

        Returns:
            dict of numpy arrays, the corner points
        """

        # get main shape
        shape = latitude.shape

        # # adjust longitude for negative values
        # longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # initialize all four corners, with one layer for latitude and one for longitude
        northwest = numpy.zeros((*shape, 2))
        northeast = numpy.zeros((*shape, 2))
        southwest = numpy.zeros((*shape, 2))
        southeast = numpy.zeros((*shape, 2))

        # create latitude frame, with one more row and image on each side
        frame = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frame[1: shape[0] + 1, 1: shape[1] + 1] = latitude

        # extend sides of frame by extrapolation
        frame[1: -1, 0] = 2 * latitude[:, 0] - latitude[:, 1]
        frame[1: -1, -1] = 2 * latitude[:, -1] - latitude[:, -2]
        frame[0, 1:-1] = 2 * latitude[0, :] - latitude[1, :]
        frame[-1, 1:-1] = 2 * latitude[-1, :] - latitude[-2, :]

        # extend corners
        frame[0, 0] = 2 * frame[1, 1] - frame[2, 2]
        frame[0, -1] = 2 * frame[1, -2] - frame[2, -3]
        frame[-1, 0] = 2 * frame[-2, 1] - frame[-3, 2]
        frame[-1, -1] = 2 * frame[-2, -2] - frame[-3, -3]

        # create longitude frame, with one more row and image on each side
        frameii = numpy.zeros((shape[0] + 2, shape[1] + 2))
        frameii[1: shape[0] + 1, 1: shape[1] + 1] = longitude

        # extend sides of frame by extrapolation
        frameii[1: -1, 0] = 2 * longitude[:, 0] - longitude[:, 1]
        frameii[1: -1, -1] = 2 * longitude[:, -1] - longitude[:, -2]
        frameii[0, 1:-1] = 2 * longitude[0, :] - longitude[1, :]
        frameii[-1, 1:-1] = 2 * longitude[-1, :] - longitude[-2, :]

        # extend corners
        frameii[0, 0] = 2 * frameii[1, 1] - frameii[2, 2]
        frameii[0, -1] = 2 * frameii[1, -2] - frameii[2, -3]
        frameii[-1, 0] = 2 * frameii[-2, 1] - frameii[-3, 2]
        frameii[-1, -1] = 2 * frameii[-2, -2] - frameii[-3, -3]

        # populate interior polygon corners image by image
        for image in range(0, shape[0]):

            # and row by row
            for row in range(0, shape[1]):

                # frame indices are off by 1
                imageii = image + 1
                rowii = row + 1

                # by averaging latitude frame at diagonals
                northwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 0] = (frame[imageii, rowii] + frame[imageii - 1][rowii + 1]) / 2

                # and by averaging longitude longitudes at diagonals
                northwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii - 1]) / 2
                northeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii + 1][rowii + 1]) / 2
                southwest[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii - 1]) / 2
                southeast[image, row, 1] = (frameii[imageii, rowii] + frameii[imageii - 1][rowii + 1]) / 2

        # fix longitude edge cases, by looking for crisscrossing, and truncating
        northwest[:, :, 1] = numpy.where(northwest[:, :, 1] < longitude, northwest[:, :, 1], longitude)
        northeast[:, :, 1] = numpy.where(northeast[:, :, 1] > longitude, northeast[:, :, 1], longitude)
        southwest[:, :, 1] = numpy.where(southwest[:, :, 1] < longitude, southwest[:, :, 1], longitude)
        southeast[:, :, 1] = numpy.where(southeast[:, :, 1] > longitude, southeast[:, :, 1], longitude)

        # average all overlapping corners
        overlap = southeast[1:, :-1, :] + southwest[1:, 1:, :] + northwest[:-1, 1:, :] + northeast[:-1, :-1, :]
        average = overlap / 4

        # transfer average
        southeast[1:, :-1, :] = average
        southwest[1:, 1:, :] = average
        northwest[:-1, 1:, :] = average
        northeast[:-1, :-1, :] = average

        # average western edge
        average = (southwest[1:, 0, :] + northwest[:-1, 0, :]) / 2
        southwest[1:, 0, :] = average
        northwest[:-1, 0, :] = average

        # average eastern edge
        average = (southeast[1:, -1, :] + northeast[:-1, -1, :]) / 2
        southeast[1:, -1, :] = average
        northeast[:-1, -1, :] = average

        # average northern edge
        average = (northwest[-1, 1:, :] + northeast[-1, :-1, :]) / 2
        northwest[-1, 1:, :] = average
        northeast[-1, :-1, :] = average

        # average southern edge
        average = (southwest[0, 1:, :] + southeast[0, :-1, :]) / 2
        southwest[0, 1:, :] = average
        southeast[0, :-1, :] = average

        # # subtact 360 from any longitudes over 180
        # northwest[:, :, 1] = numpy.where(northwest[:, :, 1] >= 180, northwest[:, :, 1] - 360, northwest[:, :, 1])
        # northeast[:, :, 1] = numpy.where(northeast[:, :, 1] >= 180, northeast[:, :, 1] - 360, northeast[:, :, 1])
        # southeast[:, :, 1] = numpy.where(southeast[:, :, 1] >= 180, southeast[:, :, 1] - 360, southeast[:, :, 1])
        # southwest[:, :, 1] = numpy.where(southwest[:, :, 1] >= 180, southwest[:, :, 1] - 360, southwest[:, :, 1])

        # collect corners
        corners = {'northwest': northwest, 'northeast': northeast}
        corners.update({'southwest': southwest, 'southeast': southeast})

        return corners

    def _orientate(self, latitude, longitude):
        """Create corners by orienting latitude bounds and longitude bounds.

        Arguments:
            latitude: numpy array, latitude bounds
            longitude: numpy array, longitude bounds

        Returns:
            dict of numpy arrays
        """

        # add 360 to all negative longitude bounnds
        longitude = numpy.where(longitude < 0, longitude + 360, longitude)

        # get all cardinal directions
        cardinals = {'south': latitude.min(axis=2), 'north': latitude.max(axis=2)}
        cardinals.update({'west': longitude.min(axis=2), 'east': longitude.max(axis=2)})

        # begin corners with zeros
        compasses = ('northwest', 'northeast', 'southeast', 'southwest')
        compassesii = ('northeast', 'southeast', 'southwest', 'northwest')
        corners = {compass: numpy.zeros((latitude.shape[0], latitude.shape[1], 2)) for compass in compasses}

        # double up bounds
        latitude = latitude.transpose(2, 1, 0)
        longitude = longitude.transpose(2, 1, 0)
        latitude = numpy.vstack([latitude, latitude, latitude]).transpose(2, 1, 0)
        longitude = numpy.vstack([longitude, longitude, longitude]).transpose(2, 1, 0)

        # go through each position
        for index in range(4):

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] < longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compasses):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

            # check against corners and add if south and west are the same
            mask = (latitude[:, :, index] == cardinals['north'])
            maskii = (longitude[:, :, index] > longitude[:, :, index + 2])
            masque = numpy.logical_and(mask, maskii)

            # for each point
            for way, compass in enumerate(compassesii):

                # add corners
                corners[compass][:, :, 0] = numpy.where(masque, latitude[:, :, index + way], corners[compass][:, :, 0])
                corners[compass][:, :, 1] = numpy.where(masque, longitude[:, :, index + way], corners[compass][:, :, 1])

        # subtract 360 from longitudes
        for compass in compasses:

            # subtract form longitude
            corner = corners[compass][:, :, 1]
            corners[compass][:, :, 1] = numpy.where(corner > 180, corner - 360, corner)

        return corners

    def _polymerize(self, latitude, longitude, resolution):
        """Construct polygons from latitude and longitude bounds.

        Arguments:
            latitude: numpy array of latitude bounds
            longitude: numpy array of longitude bounds
            resolution: int, vertical compression factor

        Returns:
            numpy.array
        """

        # squeeze arrays
        latitude = latitude.squeeze()
        longitude = longitude.squeeze()

        # if bounds are given
        if len(latitude.shape) > 2:

            # orient the bounds
            corners = self._orientate(latitude, longitude)

        # otherwise
        else:

            # get corners from latitude and longitude points
            corners = self._frame(latitude, longitude)

        # compress corners vertically
        corners = self._compress(corners, resolution, 1)

        # construct bounds
        compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        arrays = [corners[compass][:, :, 0] for compass in compasses]
        bounds = numpy.stack(arrays, axis=2)
        arrays = [corners[compass][:, :, 1] for compass in compasses]
        boundsii = numpy.stack(arrays, axis=2)

        # adjust polygon  boundaries to avoid crossing dateline
        boundsii = self._cross(boundsii)

        # make polygons
        polygons = numpy.dstack([bounds, boundsii])

        return polygons

    def heat(self, orbit='100172', resolution=20):
        """Make heatmap of cloud pressure differences.

        Arguments:
            orbit: str, orbit number
            resolution: int, number of pixels to coagulate

        Returns:
            None
        """

        # set defaults
        source = 'three'
        sourceii = 'four'
        sourceiii = 'radiance'
        sink = 'plots/heatmap'

        # set names
        names = {'three': 'Col3', 'four': 'Col4', 'hybrid': 'Col4L1B+Col3 OMCLDRR'}

        # create hydras
        hydra = Hydra('{}/{}'.format(self.sink, source))
        hydraii = Hydra('{}/{}'.format(self.sink, sourceii))
        hydraiii = Hydra('{}/{}'.format(self.sink, sourceiii))

        # ingest paths
        hydra.ingest('o' + orbit)
        hydraii.ingest('o' + orbit)
        hydraiii.ingest('o' + orbit)

        # grab geolocation from first
        latitude = hydraii.grab('Latitude')
        longitude = hydraii.grab('Longitude')

        # grab latitude and longitude bounds
        bounds = hydraiii.grab('latitude_bounds').squeeze()
        boundsii = hydraiii.grab('longitude_bounds').squeeze()

        # grab quality flags and clouds
        cloud = hydra.grab('CloudPressure')
        cloudii = hydraii.grab('CloudPressure')
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')
        reflectivity = hydraii.grab('Reflectivity331')
        fraction = hydra.grab('CloudFraction')
        fractionii = hydraii.grab('CloudFraction')
        terrain = hydra.grab('Residual')[:, :, 5]
        terrainii = hydraii.grab('Residual')[:, :, 5]

        # calculate cloud difference
        difference = cloudii - cloud

        # unpack bits and get bit 7 for cloud pressure source
        bits = numpy.unpackbits(quality.astype('uint8')).reshape(*quality.shape, -1)[:, :, 0]
        bitsii = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 0]
        climatology = bitsii - bits

        # unpack bits and get bit 6 for row anomaly
        anomaly = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 1]

        # unpack bits for descending pixels
        quartet = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)
        quartet[:, :, :4] = 0
        descension = numpy.packbits(quartet.flatten()).reshape(qualityii.shape)

        # construct polygons from points
        polygons = self._polymerize(latitude, longitude, resolution)

        # construct polygons from bounds
        polygons = self._polymerize(bounds, boundsii, resolution)

        # # get corners from latitude and longitude bounds
        # corners = self._orientate(bounds, boundsii)
        # corners = self._compress(corners, resolution, 1)
        #
        # # construct bounds
        # data = {}
        # compasses = ('southwest', 'southeast', 'northeast', 'northwest')
        # arrays = [corners[compass][:, :, 0] for compass in compasses]
        # data['latitude_bounds'] = numpy.stack(arrays, axis=2)
        # arrays = [corners[compass][:, :, 1] for compass in compasses]
        # data['longitude_bounds'] = numpy.stack(arrays, axis=2)
        #
        # # adjust polygon  boundaries to avoid crossing dateline
        # data['longitude_bounds'] = self._cross(data['longitude_bounds'])
        #
        # # make polygons
        # bounds = data['latitude_bounds']
        # boundsii = data['longitude_bounds']
        # polygons = numpy.dstack([bounds, boundsii])

        # apply resolution to other fields
        evens = numpy.array([index for index in range(latitude.shape[0]) if index % resolution == 0])
        difference = difference[evens]
        climatology = climatology[evens]
        anomaly = anomaly[evens]
        latitude = latitude[evens]
        longitude = longitude[evens]
        descension = descension[evens]
        reflectivity = reflectivity[evens]
        cloud = cloud[evens]
        cloudii = cloudii[evens]
        fraction = fraction[evens]
        fractionii = fractionii[evens]
        terrain = terrain[evens]
        terrainii = terrainii[evens]

        # create mask for no anomaly, no source difference, and no descending orbit
        mask = (climatology == 0) & (anomaly == 0) & (descension < 10)

        # apply mask
        difference = difference[mask]
        latitude = latitude[mask]
        longitude = longitude[mask]
        polygons = polygons[mask]
        reflectivity = reflectivity[mask]
        cloud = cloud[mask]
        cloudii = cloudii[mask]
        fraction = fraction[mask]
        fractionii = fractionii[mask]
        terrain = terrain[mask]
        terrainii = terrainii[mask]

        # print highest difference pixel
        maximum = difference.max()
        pixels = self._pin(maximum, difference, number=20)
        for pixel in pixels:

            # print
            formats = [array[pixel] for array in (latitude, longitude, difference, cloud, cloudii)]
            formats += [array[pixel] for array in (terrain, terrainii)]
            self._print('lat: {} lon: {} diff: {} col3: {} col4: {} ter3: {} ter4: {}'.format(*formats))

        # create color bracket
        bracket = [-1000, -600, -200, 200, 600, 1000]

        # get date
        date = self._stage(hydra.current)['day']

        # heat map differences
        tracer = difference
        folder = '{}/cloud_difference_heatmap.h5'.format(sink)
        title = 'Cloud Pressure differences, same source, Col4 - Col3, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure_difference'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # create color bracket
        bracket = [0, 50, 100, 150, 200]

        # heat map reflectivity
        tracer = reflectivity
        folder = '{}/reflectivity_heatmap.h5'.format(sink)
        title = 'Reflectivity331, same source, Col4 - Col3, orbit {}, {}'.format(orbit, date)
        name = 'reflectivity'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # create color bracket
        bracket = [200, 400, 600, 800, 1000, 1200]

        # heat map reflectivity
        tracer = cloudii
        folder = '{}/cloud_pressure_col4_heatmap.h5'.format(sink)
        title = 'Cloud Pressure, Col4, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # heat map reflectivity
        tracer = cloud
        folder = '{}/cloud_pressure_col3_heatmap.h5'.format(sink)
        title = 'Cloud Pressure, Col3, orbit {}, {}'.format(orbit, date)
        name = 'cloud_pressure'
        units = '  hPa'
        texts = [name, 'latitude', 'longitude']
        arrays = [tracer, latitude, longitude]
        self.squid.pulsate(texts, arrays, polygons, folder, bracket, title, units)

        # scatter plot cloud pressure diff vs cloud fraction diff
        differenceii = fractionii - fraction
        maskii = (abs(difference) < 10000) & (abs(differenceii) < 10000)
        folder = 'plots/scatters/cloud_pressure_diff_fraction_diff.h5'
        title = 'Cloud Pressure differences vs Cloud Fraction differences'
        name = 'cloud_fraction_pressure'
        self.squid.ink(name, difference[maskii], 'fraction', differenceii[maskii], folder, title)

        return None

    def scatter(self, orbit='100172', wave=317, limit=15, limitii=84, rows=(0, 20), sourceii='hybrid'):
        """Compare two OMTO3 files, making scatter plots.

        Arguments:
            orbit: str, orbit number
            rows: tuple of ints, the rows to use
            sink: str, folder for dumping plots
            wave: int, wavelength
            limit: float, reflectivity331 limit
            limitii: float, solar zenith angle limit

        Returns:
            None
        """

        # set defaults
        source = 'three'
        sink = 'plots/scatters'

        # set names
        names = {'three': 'Col3', 'four': 'Col4', 'hybrid': 'Col4L1B+Col3 OMCLDRR'}

        # set reflectivity limits
        limits = {}
        if limit < 100:

            # set limit
            limits[limit] = 'R331 < {}'.format(round(limit / 100, 2))

        # otherwise
        else:

            # set all limits
            limits[limit] = 'All R331'

        # set zenith limits
        limitsii = {}
        if limitii < 90:

            # set limit
            limitsii[limitii] = ', SZA < {}'.format(limitii)

        # otherwise
        else:

            # set all limits
            limitsii[limitii] = ''

        # set fields
        fields = {}
        fields.update({'value': 'NValue', 'reflectivity': 'Reflectivity331'})
        fields.update({'step': 'StepOneO3', 'ozone': 'ColumnAmountO3'})
        fields.update({'cloud': 'CloudPressure', 'correction': 'CalibrationAdjustment'})
        fields.update({'fraction': 'CloudFraction'})

        # create hydras
        hydra = Hydra('{}/{}'.format(self.sink, source))
        hydraii = Hydra('{}/{}'.format(self.sink, sourceii))

        # ingest paths
        hydra.ingest('o' + orbit)
        hydraii.ingest('o' + orbit)

        # grab latitudes
        latitude = hydra.grab('Latitude')
        wavelength = hydra.grab('Wavelength')
        reflectivity = hydra.grab('Reflectivity331')
        zenith = hydra.grab('SolarZenithAngle')

        # grab quality flags and unpack bits
        quality = hydra.grab('QualityFlags')
        qualityii = hydraii.grab('QualityFlags')

        # unpack bits and get bit 7 for cloud pressure source
        bits = numpy.unpackbits(quality.astype('uint8')).reshape(*quality.shape, -1)[:, :, 0]
        bitsii = numpy.unpackbits(qualityii.astype('uint8')).reshape(*qualityii.shape, -1)[:, :, 0]

        # get indices for waves
        position = self._pin(wave, wavelength)[0][0]

        # unpack rows
        first, second = rows

        # for each field
        for field, search in fields.items():

            # get mask for reflectivity limit
            mask = (reflectivity[:, first:second] < limit) & (zenith[:, first:second] < limitii)

            # get data
            datum = hydra.grab(search)
            datumii = hydraii.grab(search)

            # get date
            date = self._stage(hydra.current)['day']

            # get difference
            difference = datumii - datum

            # create absiccsas from latitude and zenith
            abscissa = latitude
            dependent = 'latitude'
            abscissaii = zenith
            dependentii = 'solar zenith angle'

            # adjust for 3-D
            if len(difference.shape) > 2:

                # extend abscissa by 12
                #abscissa = numpy.array([latitude] * 12).transpose(1, 2, 0)
                difference = difference[:, first:second, position]
                difference = difference[mask]
                abscissa = abscissa[:, first:second]
                abscissa = abscissa[mask]
                abscissaii = abscissaii[:, first:second]
                abscissaii = abscissaii[mask]
                search = search + '_{}'.format(wave)
                field = field + '_{}'.format(wave)

            # adjust for calibration
            elif difference.shape[0] < 1000:

                # extend abscissa by 12
                difference = difference[first:second, :]
                abscissa = numpy.array([list(range(difference.shape[0]))] * 12).transpose(1, 0)
                abscissaii = numpy.array([list(range(difference.shape[0]))] * 12).transpose(1, 0)
                dependent = 'row'
                dependentii = 'row'

            # otherwise, adjust for rows 1-20
            else:

                # adjust
                difference = difference[:, first:second]
                difference = difference[mask]
                abscissa = abscissa[:, first:second]
                abscissa = abscissa[mask]
                abscissaii = abscissaii[:, first:second]
                abscissaii = abscissaii[mask]

            # only keep finites
            maskii = (numpy.isfinite(difference)) & (abs(difference) < 1e10)
            difference = difference[maskii]
            abscissa = abscissa[maskii]
            abscissaii = abscissaii[maskii]

            # make plot
            formats = [search, names[sourceii], names[source], orbit, date, first + 1, second]
            formats += [limits[limit], limitsii[limitii]]
            address = '{}/{}_OMTO3v8_differences_latitude.h5'.format(sink, field)
            title = '{} Differences, OMTO3 v8 {} - {}, Orbit {}, {}, Rows {}-{}, {} {}'
            title = title.format(*formats)
            self.squid.ink('{}_difference'.format(search.lower()), difference, dependent, abscissa, address, title)

            # if cloud pressure
            if field == 'cloud':

                # get cloud flag difference
                differenceii = bitsii.astype(int) - bits.astype(int)
                differenceii = differenceii[:, first:second][mask][maskii]

                # plot
                formats = [search, names[sourceii], names[source], orbit, date, first + 1, second]
                formats += [limits[limit], limitsii[limitii]]
                address = '{}/cloud_source_OMTO3v8_differences_latitude.h5'.format(sink, field)
                title = '{} Differences, OMTO3 v8 {} - {}, vs Cloud Source, Orbit {}, {}, Rows {}-{}, {} {}'
                title = title.format(*formats)
                self.squid.ink('cloud_source', differenceii, field, difference, address, title)

            # # make plot for zenith
            # formats = (search, orbit, date, first + 1, second, limit)
            # address = '{}/{}_OMTO3v8_differences_zenith.h5'.format(sink, field)
            # title = '{} Differences by SZA, OMTO3 v8 Col4 - Col3, Orbit {}, {}, Rows {}-{}, R331 < 0.{}'
            # title = title.format(*formats)
            # self.squid.ink('{}_difference'.format(search.lower()), difference, dependentii, abscissaii, address, title)

        return None

    def zone(self, year=2005, month=6, archive=95042, limit=500, sink='collection', sinkii='zones'):
        """Collect 5 degree zonal means from omto3 files.

        Arguments:
            year: int, year
            month: int, month
            archive: int, archive set
            limit: number orbits
            sink: str, folder for collection files
            sinkii: str, folder for zonal means files

        Returns:
            None
        """

        # create destination folders and logs folder
        self._make(self.sink)
        self._make('{}/{}'.format(self.sink, sink))
        self._make('{}/{}'.format(self.sink, sinkii))
        self._make('{}/logs'.format(self.sink))

        # create destination
        formats = (self.sink, sink, year, self._pad(month), archive)
        destination = '{}/{}/OMTO3_Zonal_Collection_{}m{}_{}.h5'.format(*formats)

        # set up latitude brackets, reversing for northmost at first index
        brackets = [(-90 + index * 5, -90 + 5 + index * 5) for index in range(36)]
        brackets.reverse()

        # create hydra
        hydra = Hydra('/tis/OMI/{}/OMTO3/{}/{}'.format(archive, year, self._pad(month)), 1, 31)

        # get paths
        paths = [path for path in hydra.paths if not path.endswith('.met')]

        # begin data
        data = {}
        sums = {}
        squares = {}

        # for each path ( within limited number of paths )
        paths = paths[:limit]
        for number, path in enumerate(paths):

            # print
            self._print('ingesting {}, {} of {}...'.format(path, number, len(paths)))

            # ingest path
            hydra.ingest(path)

            # set up searches for grabbing data
            searches = {'quality': 'QualityFlags', 'algorithm': 'AlgorithmFlags'}
            searches.update({'terrain': 'TerrainPressure', 'cloud': 'CloudPressure', 'fraction': 'fc'})
            searches.update({'reflectivity': 'Reflectivity331', 'reflectivityii': 'Reflectivity360'})
            searches.update({'step': 'StepOneO3', 'stepii': 'StepTwoO3', 'ozone': 'ColumnAmountO3'})
            searches.update({'sulfur': 'SO2index', 'aerosol': 'UVAerosolIndex'})
            searches.update({'latitude': 'Latitude', 'longitude': 'Longitude'})
            searches.update({'zenith': 'SolarZenithAngle', 'zenithii': 'ViewingZenithAngle'})
            searches.update({'azimuth': 'RelativeAzimuthAngle'})
            searches.update({'reflectance': 'NValue', 'residue': 'Residual'})
            searches.update({'residual': 'ResidualStep1', 'residualii': 'ResidualStep2'})
            searches.update({'slope': 'dN_dR', 'sensitivity': 'Sensitivity'})

            # collect data
            collection = {name: hydra.grab(search) for name, search in searches.items()}

            # additionally get wavelengths and time
            wavelength = hydra.grab('Wavelength')
            time = hydra.grab('Time')

            # collect attributes
            attributes = {name: hydra.dig(search)[0].attributes for name, search in searches.items()}

            # construct reflectivity mask for all cases > 0, < 100
            mask = (collection['reflectivity'] > 0) & (collection['reflectivity'] < 100)

            # construct algorithm flag mask for all samples not skipped ( those with snow / ice are allowed )
            maskii = (collection['algorithm'] % 10 > 0)

            # set quality mask for ascending and good retrievals (QualityFlags & 15) == 0 | 1
            maskiii = (collection['quality'] & 15 < 2)

            # set quality mask for no anomaly ( not bit 6 )
            maskiv = (collection['quality'] & 64 == 0)

            # # make mask for sulfur and aerosols >= 0 to weed out fill values
            # maskv = (collection['sulfur'] > 0) & (collection['aerosol'] > 0)

            # combine all masks
            masque = mask & maskii & maskiii & maskiv

            # set transformations to be applied
            transforms = {'reflectivity': lambda tensor: tensor / 100.0}
            transforms.update({'reflectivityii': lambda tensor: tensor / 100.0})
            transforms.update({'algorithm': lambda tensor: (tensor % 10).astype(float)})
            transforms.update({'quality': lambda tensor: (tensor >> 7 & 1).astype(float)})

            # apply transformations
            collection.update(({name: transform(collection[name]) for name, transform in transforms.items()}))

            # apply mask to all data
            collection.update({name: collection[name][masque] for name in collection.keys()})

            # begin data arrays
            names = list(collection.keys())
            sums.update({name: data.setdefault(name, []) for name in collection.keys()})
            squares.update({name: data.setdefault(name, []) for name in collection.keys()})

            # allocate 2-D zero arrays
            [sums[name].append(numpy.zeros((36,))) for name in names if len(collection[name].shape) == 2]
            [squares[name].append(numpy.zeros((36,))) for name in names if len(collection[name].shape) == 2]

            # allocate 3-D zero arrays
            [sums[name].append(numpy.zeros((36, 12))) for name in names if len(collection[name].shape) == 3]
            [squares[name].append(numpy.zeros((36, 12))) for name in names if len(collection[name].shape) == 3]

            # add counts, latitude brackets
            data['counts'] = data.setdefault('counts', [])
            data['counts'].append(numpy.zeros((36,)))

            # add orbit number
            orbit = int(self._stage(path)['orbit'])
            data['orbit'] = data.setdefault('orbit', [])
            data['orbit'].append(orbit)

            # add date and time
            date = self._stage(path)['date']
            partitions = {'year': (0, 4), 'month': (5, 7), 'day': (7, 9), 'hour': (10, 12), 'minute': (12, 14)}
            for partition, (first, last) in partitions.items():

                # append data
                data[partition] = data.setdefault(partition, [])
                data[partition].append(int(date[first: last]))

            # add wavelength
            data['wavelength'] = wavelength

            # add latitude brackets
            data['latitude_bracket'] = numpy.array(brackets)
            data['latitude_midpoint'] = numpy.array(brackets).mean(axis=1)

            # for each latitude bracket
            for index, (south, north) in enumerate(brackets):

                # construct mask from latitude limits
                mask = (collection['latitude'] >= south) & (collection['latitude'] < north)

                # for each name in the collection
                for name in names:

                    # add sum and sum of squares to data
                    sums[name][-1][index] += collection[name][mask].sum(axis=0)
                    squares[name][-1][index] += (collection[name][mask] ** 2).sum(axis=0)

                # add to counts
                data['counts'][-1][index] += mask.sum()

        # add sums and squares
        data.update({'{}_sums'.format(self._serpentize(name)): arrays for name, arrays in sums.items()})
        data.update({'{}_squares'.format(self._serpentize(name)): arrays for name, arrays in squares.items()})

        # form into arrays
        data.update({name: numpy.array(arrays) for name, arrays in data.items()})

        # create file
        hydra.spawn(destination, data)

        # view data
        self._view(data)
        #
        # # ingest destination
        # hydra = Hydra(destination)
        # hydra.ingest()
        #
        # # grab latitude brackets
        # brackets = hydra.grab('latitude_bracket')
        #
        # # get all features with sums
        # features = [feature for feature in hydra if 'sum' in feature.name]
        #
        # # construct times as month fractions
        # day = hydra.grab('day')
        # hour = hydra.grab('hour')
        # minute = hydra.grab('minute')
        # time = day + hour / 24 + minute / (24 * 60)
        #
        # # construct latitudes
        # latitudes = hydra.grab('latitude_bracket').mean(axis=1)
        #
        # # for each latitue
        # for feature in features:
        #
        #     # grab counts
        #     counts = hydra.grab('counts')
        #
        #     # grab the data
        #     array = feature.distil()
        #
        #     # create mask for finites, and apply
        #     mask = (numpy.isfinite(counts)) & (numpy.isfinite(array))
        #     array = numpy.where(mask, array, 0)
        #     counts = numpy.where(mask, counts, 0)
        #
        #     # calculate average
        #     average = array.sum(axis=0) / counts.sum(axis=0)
        #
        #     # create mask for finite average
        #     mask = numpy.isfinite(average)
        #
        #     # construct plot
        #     name = feature.name.replace('_sum', '')
        #     formats = (name, year, self._pad(month), archive)
        #     title = 'OMTO3 {}, Zonal Means'.format(name.replace('_', ' ').capitalize())
        #     address = 'plots/{}/OMTO3_Zonal_Means_{}_{}m{}_AS{}.h5'.format(sink, *formats)
        #     self.squid.ink(name, average[mask], 'latitude', latitudes[mask], address, title)

        return






