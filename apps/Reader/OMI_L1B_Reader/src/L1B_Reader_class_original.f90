!!
! MODULE L1B_reader_class contains the OMI L1B data structure and
! the functions for the creation, deletion and access of specific
! information stored in the data structure.  A user can use the
! following functions:  L1Br_open, L1Br_close, L1Br_getDIMsize,
! L1Br_getSWdims, L1Br_getGEOpix, L1Br_getGEOline, L1Br_getDATA,
! L1Br_getSIGpix, 1Br_getSIGline, L1Br_getSIGpixWL, L1Br_getSIGlineWL,
! and L1Br_getDATAFIELDline.
!!
!
! Revision history
! May 2020-Updating reader to read collection4 netCDF4 format
! March 2023 - MB
!   corrected SpecChan, QualLevel mixup in L1Br_getSIGline
!   added nc4 file closure to L1Br_close
!   returned wavelength calculation to Horner's method with coefficients
!     after testing wavelength shift method in calc_wl_line

MODULE L1B_Reader_class
  USE nc4read
  USE nc4utils , ONLY : check_nf90
  USE netcdf
  use pge_logging
  use nc4_compound_mod
  IMPLICIT NONE
  ! INTEGER (KIND = 4):: OMI_SMF_setmsg !!this external function writes the PGS
  ! EXTERNAL              OMI_SMF_setmsg !!SMF error messages to the Log files
  PUBLIC  :: L1Br_open
  PUBLIC  :: L1Br_close
  PUBLIC  :: L1Br_getDIMsize
  PUBLIC  :: L1Br_getSWdims
  PUBLIC  :: L1Br_getGEOpix
  PUBLIC  :: L1Br_getGEOline
  PUBLIC  :: L1Br_getDATA
  PUBLIC  :: L1Br_getSIGpix
  PUBLIC  :: L1Br_getSIGline
  PUBLIC  :: L1Br_getSIGpixWL
  PUBLIC  :: L1Br_getSIGlineWL
  PUBLIC  :: L1Br_getGlobalAttr
  ! PUBLIC  :: L1Br_getDATAFIELDline
  ! PUBLIC  :: L1Br_getDATAline

  PRIVATE:: fill_l1b_blk
  PRIVATE:: init_l1b_blk
  PRIVATE:: calc_wl_pix
  PRIVATE:: calc_wl_line
  PRIVATE:: check_blk_line
  PRIVATE:: check_blk_pix
  INTEGER (KIND = 4), PARAMETER, PUBLIC:: MAX_NAME_LENGTH = 255
  INTEGER, PRIVATE:: ierr  !!error code returned from a function
  INTEGER, PARAMETER, PRIVATE:: zero = 0
  INTEGER, PARAMETER, PRIVATE:: one  = 1
  INTEGER, PARAMETER, PRIVATE:: two  = 2
  REAL (KIND = 4), PARAMETER, PRIVATE:: rfill  = -1.0*(2.0**100)
  REAL (KIND = 8), PARAMETER, PRIVATE:: r8fill  = -1.0*(2.0**100)
  INTEGER (KIND = 1), PARAMETER, PRIVATE:: int8fill  = INT(-127, 1)
  INTEGER (KIND = 2), PARAMETER, PRIVATE:: int16fill  = INT(-32767, 2)
  INTEGER (KIND = 4), PARAMETER, PRIVATE:: int32fill  = -2147483647
  INTEGER, PARAMETER, PRIVATE ::   OMI_S_SUCCESS = 15565312
  INTEGER, PARAMETER, PRIVATE ::   OMI_E_FAILURE = 15568385
  REAL (KIND = 4), dimension(:,:,:), ALLOCATABLE:: arr_3drk4
  REAL (KIND = 4), dimension(:,:,:,:), ALLOCATABLE:: arr_4drk4, arr_4drk4_cor
  REAL (KIND = 8), dimension(:,:,:), ALLOCATABLE:: arr_3drk8
  REAL (KIND = 4), dimension(:,:), ALLOCATABLE:: arr_2drk4
  REAL (KIND = 8), dimension(:,:), ALLOCATABLE:: arr_2drk8
  INTEGER (KIND = 4), dimension(:,:,:), ALLOCATABLE:: arr_3dik4
  INTEGER (KIND = 8), dimension(:,:,:), ALLOCATABLE:: arr_3dik8
  INTEGER (KIND = 4), dimension(:,:,:,:), ALLOCATABLE:: arr_4dik4
  INTEGER (KIND = 4), dimension(:,:), ALLOCATABLE:: arr_2dik4
  INTEGER (KIND = 8), dimension(:,:), ALLOCATABLE:: arr_2dik8
  INTEGER, dimension(2):: start_2d, count_2d, stride_2d
  INTEGER, dimension(3):: start_3d, count_3d, stride_3d
  INTEGER, dimension(4):: start_4d, count_4d, stride_4d, count_4d_cor, &
    count_4d_coadd
  CHARACTER (LEN = 256), PRIVATE:: msg
  logical, PRIVATE:: is_nominal= .TRUE.

  !!
  ! L1B_block_type is designed to store a block of L1B swath data.
  ! A block contains all the geolocation, radiance and wavelength
  ! data fields for a number of "scan" lines or "exposure times"
  ! in a L1B swath.
  !
  ! The fields in the L1B_block_type are defined as follows:
  !
  ! initialized: the logical variable to indicate whether the
  !              block has been initialized.  The block data structure
  !              has to be created (initialized) before it can be used.
  !
  ! filename: the file name for the L1B file (including path)
  ! swathname: the swath name in the L1B file, either "Earth UV-1 Swath",
  !            "Earth UV-2 Swath", or "Earth VIS Swath"
  ! iLine: the starting line for the block.  It is a reference to the
  !        L1B file, where 0 is the first line, and (NumTimes-1)
  !        is the last line.
  ! eLine: the ending line for the block
  ! nLine: the number of lines containing in the block with iLine
  !        as start and eLine as the end (inclusive)
  !
  ! NumTimes: the total number of lines contained in the L1B swath
  ! nXtrack: the number of cross-track pixels in the swath
  ! nCorner: the number of cross-track pixels in the swath
  ! nWavel: the number of wavelengths in the swath
  ! nWavelCoef: the number of wavelength coefficients
  ! NumTimesSmallPixel: the total number of small pixel columns contained in the L1B swath
  !
  ! The remaining elements in the L1B_block_type are directly associated with datasets in
  ! the L1B file.  They are listed here, associated with the L1B Dataset Name whose data
  ! they contain.
  !
  ! Element Name    L1B Dataset Name
  ! ============    ================
  ! GEOLOCATION FIELDS: (extractable with the getGEO functions)
  ! Time            Time
  ! SecInDay        SecondsInDay [delta_time]/1000
  ! ScLat           SpacecraftLatitude
  ! ScLon           SpacecraftLongitude
  ! ScAlt           SpacecraftAltitude
  ! Lon             Longitude
  ! Lat             Latitude
  ! SolZenAng       SolarZenithAngle
  ! SolAziAng       SolarAzimuthAngle
  ! ViewZenAng      ViewingZenithAngle
  ! ViewAziAng      ViewingAzimuthAngle
  ! TerrainHeight   TerrainHeight
  ! GPQFlag         GroundPixelQualityFlags
  !
  ! Fields added in netCDF FORMAT
  ! =============================
  ! TerrainHeightPrec surface_altitude_precision
  ! OrbitPhase      satellite_orbit_phase
  ! WaterFrac       water_fraction
  ! SpacecraftShadowFraction satellite_shadow_fraction
  !
  ! Fields deleted in netCDF4 FORMAT
  !=================================
  ! XTQFlag         XTrackQualityFlags
  ! SolElevation    SolarElevation
  ! SolElevMin      SolarElevationMinimum
  ! SolElevMax      SolarElevationMaximum
  ! SolAzimuth      SolarAzimuth
  ! SolAziMin       SolarAzimuthMinimum
  ! SolAziMax       SolarAzimuthMaximum
  !
  !
  ! RADIANCE RELATED FIELDS: (extractable with the getSIG functions)
  ! WavelenCoef     WavelengthCoefficient
  ! WavelenPrec     WavelengthCoefficientPrecision
  ! WavelenRefCol   WavelengthReferenceColumn
  ! SmPixRad        SmallPixelRadiance or SmallPixelIrradiance or SmallPixelSignal
  ! SmPixWavelen    SmallPixelWavelength
  ! NumSmPixCol     NumberSmallPixelColumns
  ! NumSmPix        NumberSmallPixelColumns for calculation
  ! SmPixCol        SmallPixelColumn
  !
  ! Fields added in netcdf4 format
  !=================================
  ! Radiance                    Radiance
  ! RadianceError               radiance_error
  ! RadianceNoise               radiance_noise
  ! WavelenNom                  nominal_wavelength
  ! WavelenShift                wavelength_shift
  ! QualLevel                   quality_level (8-bit unsigned)
  ! SpecQual                    spectral_channel_quality (8-bit unsigned)
  ! earth_sun_dist              Earth sun distance
  ! land_water_classificaiton   land water classification(8-bit unsigned)
  ! xtrack_quality              XTrackQualityFlags(16-bit unsigned)
  !
  ! Fields deleted in netCDF4 format
  !=================================
  ! RadMantissa     RadianceMantissa or IrradianceMantissa or SignalMantissa
  ! RadPrecision    RadiancePrecision or IrradiancePrecision or SignalPrecision
  ! RadExponent     RadianceExponent or IrradianceExponent or SignalExponent
  ! PQFlag          PixelQualityFlags
  !
  !
  ! OTHER DATA FIELDS: (extractable with the getDATA function)
  ! Config          InstrumentConfigurationId
  ! MeasClass       MeasurementClass
  ! ExposureType    ExposureType
  ! ImBinFact       ImageBinningFactor
  ! GC1             GainCode1
  ! GC2             GainCode2
  ! GC3             GainCode3
  ! GC4             GainCode4
  ! DSGC            DSGainCode
  ! LSLABF          LowerStrayLightAreaBinningFactor
  ! USLABF          UpperStrayLightAreaBinningFactor
  ! LDABF           LowerDarkAreaBinningFactor
  ! UDABF           UpperDarkAreaBinningFactor
  ! MQFlag          MeasurementQualityFlags
  ! CalSet          CalibrationSettings
  ! GSC1            GainSwitchingColumn1
  ! GSC2            GainSwitchingColumn2
  ! GSC3            GainSwitchingColumn3
  ! SR1             SkipRows1
  ! SR2             SkipRows2
  ! SR3             SkipRows3
  ! SR4             SkipRows4
  ! BinImgRows      BinnedImageRows
  ! StopColumn      StopColumn
  ! MasterClkPer    MasterClockPeriod
  ! ExpTime         ExposureTime
  ! ReadTime        ReadoutTime
  ! DetcTemp        DetectorTemperature
  ! OptBenchTemp    OpticalBenchTemperature
  !
  ! The Geolocation fields can be accessed via the L1Br_getGEOpix and L1Br_getGEOline
  ! functions.  These functions simply return the requested values from the L1B file and swath
  ! for the input pixel (L1Br_getGEOpix) or line (L1Br_getGEOline).
  !
  ! The Radiance related fields can be accessed via the L1Br_getSIGpix, L1Br_getSIGline,
  ! L1Br_getSIGpixWL, and L1Br_getSIGlineWL functions.  These functions will recreate the
  ! signal (Radiance, Irradiance, or Signal) and the wavelengths from the raw data.
  ! L1Br_getSIGpix and L1Br_getSIGline will also return the raw data for a specific
  ! pixel (L1Br_getSIGpix) or line (L1Br_getSIGline).  L1Br_getSIGpixWL and L1Br_getSIGlineWL
  ! will not return raw data, but will interpolate to give approximate values of the signal
  ! for specific input wavelengths, whether or not those wavelengths are exactly present
  ! in the L1B file (as long as the requested wavelengths fall within the overall spread
  ! of wavelengths in the pixel/line in question).
  !
  ! The Other Data fields can be accessed via the L1Br_getDATA function.  This function simply
  ! returns the requested values from the L1B file and swath for the input line (all values
  ! accessed by this function have only a single value per line in the L1B file).
  !
  ! The function L1Br_getDATAFIELDline can be used to access any datasets not covered
  ! by other functions and not contained in the L1B_block_type data block.
  !!

  TYPE, PUBLIC:: L1B_block_type
    PRIVATE
    CHARACTER (LEN = MAX_NAME_LENGTH):: filename, modename, bandname
    CHARACTER (LEN = MAX_NAME_LENGTH):: fieldname
    INTEGER (KIND = 4):: iLine
    INTEGER (KIND = 4):: eLine
    INTEGER (KIND = 4):: nLine
    INTEGER (KIND = 4):: NumTimes
    INTEGER (KIND = 4):: NumTimesSmallPixel
    INTEGER (KIND = 4):: nCoad
    INTEGER (KIND = 4):: nXtrack
    INTEGER (KIND = 4):: nCorner
    INTEGER (KIND = 4):: nWavel
    INTEGER (KIND = 4):: nWavelCoef
    INTEGER:: id_Band, id_mode, id_geo_grp, id_nc, id_inst_grp, id_obs_grp
    LOGICAL:: initialized=.FALSE.
    INTEGER (KIND = 4):: SpaZoomIndex

    ! Data Type                                    Element Name    L1B Dataset Name
    ! ====================================         ============    ================
    ! Geolocation fields
    REAL (KIND = 8),    DIMENSION(:),   POINTER:: Time          => NULL ()  ! Time
    REAL (KIND = 4),    DIMENSION(:),   POINTER:: SecInDay      => NULL ()  ! SecondsInDay
    REAL (KIND = 4),    DIMENSION(:),   POINTER:: ScLat         => NULL ()  ! SpacecraftLatitude
    REAL (KIND = 4),    DIMENSION(:),   POINTER:: ScLon         => NULL ()  ! SpacecraftLongitude
    REAL (KIND = 4),    DIMENSION(:),   POINTER:: ScAlt         => NULL ()  ! SpacecraftAltitude
    REAL (KIND = 4),    DIMENSION(:),   POINTER:: OrbitPhase         => NULL ()  ! satellite_orbit_phase
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: Lon          => NULL ()  ! Longitude
    REAL (KIND = 4),   DIMENSION(:),   POINTER:: SpacecraftShadowFraction    => NULL ()  ! satellite_shadow_fraction
    REAL (KIND = 4),    DIMENSION(:,:,:), POINTER:: LatBounds   => NULL ()  ! LatitudeBounds
    REAL (KIND = 4),    DIMENSION(:,:,:), POINTER:: LonBounds   => NULL ()  ! LongitudeBounds
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: Lat           => NULL ()  ! Latitude
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: SolZenAng     => NULL ()  ! SolarZenithAngle
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: SolAziAng     => NULL ()  ! SolarAzimuthAngle
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: ViewZenAng    => NULL ()  ! ViewingZenithAngle
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: ViewAziAng    => NULL ()  ! ViewingAzimuthAngle
    INTEGER (KIND = 2), DIMENSION(:,:), POINTER:: TerrainHeight => NULL ()  ! TerrainHeight
    INTEGER (KIND = 2), DIMENSION(:,:), POINTER:: TerrainHeightPrec => NULL ()  ! TerrainHeightPrecision
    INTEGER (KIND = 1), DIMENSION(:,:), POINTER:: WaterFraction => NULL ()  ! WaterFraction
    INTEGER (KIND = 2), DIMENSION(:,:), POINTER:: GPQFlag       => NULL ()  ! GroundPixelQualityFlags
    INTEGER (KIND = 1), DIMENSION(:,:,:), POINTER:: SpecQual     => NULL ()  !  spectral_channel_quality
    INTEGER (KIND = 1), DIMENSION(:,:,:), POINTER:: QualLevel    => NULL () !  quality_level
    INTEGER (KIND = 1), DIMENSION(:,:), POINTER:: LandWater     => NULL ()  !  land_water_classification

    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolElevation  => NULL ()  ! SolarElevation
    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolAzimuth    => NULL ()  ! SolarAzimuth
    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolElevMin    => NULL ()  ! SolarElevationMinimum
    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolElevMax    => NULL ()  ! SolarElevationMaximum
    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolAziMin     => NULL ()  ! SolarAzimuthMinimum
    ! REAL (KIND = 4),    DIMENSION(:),   POINTER:: SolAziMax     => NULL ()  ! SolarAzimuthMaximum
    ! INTEGER (KIND = 1), DIMENSION(:,:), POINTER:: XTQFlag       => NULL ()  ! XTrackQualityFlags
    INTEGER (KIND = 2), DIMENSION(:,:), POINTER:: XTQFlag       => NULL ()  ! XTrackQualityFlags

    ! Data fields
    REAL (KIND = 4), DIMENSION(:,:,:), POINTER:: Radiance => NULL ()  ! RadianceMantissa or
    ! IrradianceMantissa or
    ! SignalMantissa
    INTEGER (KIND = 4), DIMENSION(:,:,:), POINTER:: RadPrecision=> NULL ()  ! RadiancePrecision or
    ! IrradiancePrecision or
    ! SignalPrecision
    REAL (KIND = 4), DIMENSION(:,:,:), POINTER:: RadianceError => NULL ()  ! RadianceExponent or
    ! IrradianceExponent or
    ! SignalExponent
    REAL (KIND = 4), DIMENSION(:,:,:), POINTER:: RadianceNoise => NULL ()
    INTEGER (KIND = 2), DIMENSION(:,:,:), POINTER:: PQFlag      => NULL ()  ! PixelQualityFlags
    INTEGER (KIND = 2), DIMENSION(:,:),   POINTER:: PFlag       => NULL ()  ! PixelQualityFlags
    REAL (KIND = 4),    DIMENSION(:,:,:), POINTER:: WavelenCoef => NULL ()  ! WavelengthCoefficient
    REAL (KIND = 4),    DIMENSION(:,:,:), POINTER:: WavelenPrec => NULL ()
    ! WavelengthCoefficientPrecision
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: Wavelen => NULL ()  ! Wavelength
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: WavelenNom => NULL ()  ! Nominal Wavelength
    REAL (KIND = 4),    DIMENSION(:,:), POINTER:: WavelenShift => NULL ()  ! wavelength_shift
    ! INTEGER (KIND = 2), DIMENSION(:), POINTER:: WavelenRefCol   => NULL ()  ! WavelengthReferenceColumn
    INTEGER (KIND = 4)                         :: WavelenRefCol=-999.0  ! WavelengthReferenceColumn
    REAL (KIND = 4),  DIMENSION(:,:,:), POINTER:: SmPixRad        => NULL()  ! SmallPixelRadiance or
    ! SmallPixelIrradiance or
    ! SmallPixelSignal
    REAL (KIND = 4),  DIMENSION(:,:), POINTER:: SmPixWavelen => NULL ()  ! SmallPixelWavelength

    INTEGER (KIND = 1), DIMENSION(:), POINTER:: MeasClass    => NULL ()  ! MeasurementClass
    INTEGER (KIND = 4), DIMENSION(:), POINTER:: Config       => NULL ()  ! InstrumentConfigurationId
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: MQFlag       => NULL ()  ! MeasurementQualityFlags
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: NumSmPixCol  => NULL ()  ! NumberSmallPixelColumns
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: NumSmPix     => NULL ()  ! NumberSmallPixelColumns_cal
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: ExposureType => NULL ()  ! ExposureType
    REAL (KIND = 4),    DIMENSION(:), POINTER:: MasterClkPer => NULL ()  ! MasterClockPeriod
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: CalSet       => NULL ()  ! CalibrationSettings
    REAL (KIND = 4),    DIMENSION(:), POINTER:: ExpTime      => NULL ()  ! ExposureTime
    REAL (KIND = 4),    DIMENSION(:), POINTER:: ReadTime     => NULL ()  ! ReadoutTime
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: SmPixCol     => NULL ()  ! SmallPixelColumn
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: GSC1         => NULL ()  ! GainSwitchingColumn1
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: GSC2         => NULL ()  ! GainSwitchingColumn2
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: GSC3         => NULL ()  ! GainSwitchingColumn3
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: GC1          => NULL ()  ! GainCode1
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: GC2          => NULL ()  ! GainCode2
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: GC3          => NULL ()  ! GainCode3
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: GC4          => NULL ()  ! GainCode4
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: DSGC         => NULL ()  ! DSGainCode
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: LSLABF => NULL ()  ! LowerStrayLightAreaBinningFactor
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: USLABF => NULL ()  ! UpperStrayLightAreaBinningFactor
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: LDABF        => NULL ()  ! LowerDarkAreaBinningFactor
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: UDABF        => NULL ()  ! UpperDarkAreaBinningFactor
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: SR1          => NULL ()  ! SkipRows1
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: SR2          => NULL ()  ! SkipRows2
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: SR3          => NULL ()  ! SkipRows3
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: SR4          => NULL ()  ! SkipRows4
    REAL (KIND = 4),    DIMENSION(:), POINTER:: DetcTemp     => NULL ()  ! DetectorTemperature
    REAL (KIND = 4),    DIMENSION(:), POINTER:: OptBenchTemp => NULL ()  ! OpticalBenchTemperature
    INTEGER (KIND = 1), DIMENSION(:), POINTER:: ImBinFact    => NULL ()  ! ImageBinningFactor
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: BinImgRows   => NULL ()  ! BinnedImageRows
    INTEGER (KIND = 2), DIMENSION(:), POINTER:: StopColumn   => NULL ()  ! StopColumn
    REAL(KIND=4)    :: earth_sun_dist  ! Earth-sun distance


  END TYPE L1B_block_type

CONTAINS

  !! 1. L1Br_open
  !
  !    Functionality:
  !
  !       This function should be called first to initiate the
  !       the interface with the L1B swath.  This function allocated memory
  !       in the data block, based on metadata located within the L1B file
  !       and swath to be accessed.  It also sets parameter values within
  !       the data block with values read from the L1B file and swath.
  !
  !    Calling Arguments:
  !
  !    Inputs:
  !
  !       this: the block data structure
  !       fn  : the L1B file name
  !       bdn : the band name in the L1B file
  !       modn : the mode name in the L1B file
  !       nL  : the number of lines the data block can store.  This is
  !             an optional input.  If it is not present, then a default
  !             value of 100 is used.
  !
  !    Outputs:
  !
  !       status: the return PGS_SMF status value
  !
  !    Change History:
  !
  !       Date            Author          Modifications
  !       ====            ======          =============
  !       January 2005    Jeremy Warner   Original Source (derived from original
  !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  !
  !!
  FUNCTION L1Br_open(this, fn, bdn, modn, nL) RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    CHARACTER (LEN = *), INTENT(IN):: fn, bdn, modn
    INTEGER (KIND = 4), OPTIONAL, INTENT(IN):: nL
    INTEGER (KIND = 4):: status
    INTEGER (KIND = 4):: dum
    CHARACTER (LEN = 256):: message
    INTEGER (KIND = 4):: rank
    INTEGER (KIND = 4):: fldflg
    INTEGER (KIND = 4), DIMENSION(1:3):: dims
    CHARACTER (LEN = 256):: func_name
    ! INTEGER (KIND = 4) ::  IposL1bIdf
    ! CHARACTER (LEN = 6):: L1bIdf
    ! CHARACTER (LEN = 1):: ZoomIdf

    ! open the L1B file
    this%filename = fn
    status = nf90_open(TRIM(fn), nf90_nowrite, this%id_nc)
    msg = 'Error in opening file : '//TRIM(fn)
    call check_nf90(status, msg)

    ! Get Band ID
    this%bandname = TRIM(bdn)
    status = nf90_inq_grp_ncid(this%id_nc, TRIM(bdn), this%id_Band)
    msg = 'Error in inquiring groupid for'//TRIM(fn)//":"//TRIM(bdn)
    call check_nf90(status, msg)

    ! Get Mode ID
    this%modename = TRIM(modn)
    status = nf90_inq_grp_ncid(this%id_Band, TRIM(modn), this%id_mode)
    msg = 'Error in inquiring groupid for'//TRIM(fn)//":"//TRIM(bdn)
    call check_nf90(status, msg)

    !netCDF4 : Check if GEODATA group exists
    status = nf90_inq_grp_ncid(this%id_mode, 'GEODATA', this%id_geo_grp)
    msg = 'Warning: Group does not exist : '//TRIM(fn)//":"//TRIM('GEODATA')
    if (status /= nf90_noerr) PRINT *,msg

    !netCDF4 : Check if INSTRUMENT group exists
    status = nf90_inq_grp_ncid(this%id_mode, 'INSTRUMENT', this%id_inst_grp)
    msg = 'Warning: Group does not exist : '//TRIM(fn)//":"//TRIM('INSTRUMENT')
    if (status /= nf90_noerr) PRINT *,msg

    !netCDF4 : Check if OBSERVATIONS group exists
    status = nf90_inq_grp_ncid(this%id_mode, 'OBSERVATIONS', this%id_obs_grp)
    msg = 'Warning: Group does not exist : '//TRIM(fn)//":"//TRIM('OBSERVATIONS')
    if (status /= nf90_noerr) PRINT *,msg

    ! intitialize the start and end line to invalid values
    this%iLine      = -1
    this%eLine      = -1

    ! retrieve the dimension info from the swath file.
    ! dimension names are obtained from file spec
    status = nc4_get_dim(this%id_mode, 'scanline',this%NumTimes)

    !   status = swrdattr(swid, "NumTimesSmallPixel", this%NumTimesSmallPixel)
    !   If(status < zero) THEN
    !      status = OMI_E_FAILURE
    !      ierr = OMI_SMF_setmsg(OMI_E_HDFEOS, "get NumTimesSmallPixel size failed", &
    !                            "L1Br_open", zero)
    !      RETURN
    !   ENDIF

    !TODO-RAM : Check status message
    status = nc4_get_dim(this%id_mode, 'ground_pixel',this%nXtrack)

    status = nc4_get_dim(this%id_mode, 'ncorner',this%nCorner)

    status = nc4_get_dim(this%id_mode, 'spectral_channel',this%nWavel)

    status = nc4_get_dim(this%id_mode, 'n_wavelength_poly',this%nWavelCoef)

    status = nc4_get_dim(this%id_mode, 'nr_coad',this%nCoad)

    ! set the block size according to input, if present
    ! if not set it to 100
    IF(.not. PRESENT(nL)) THEN
      this%nLine = 100  ! this%NumTimes
      IF (this%nLine > this%NumTimes) THEN
        this%nLine = this%NumTimes
      ENDIF
    ELSE
      IF(nL <= 0) THEN
        this%nLine = 100
        IF (this%nLine > this%NumTimes) THEN
          this%nLine = this%NumTimes
        ENDIF
      ELSE
        this%nLine = nL
      ENDIF
    ENDIF

    ! the maximum number of lines cannot exceed the total
    ! number of lines in the L1B swath.
    IF(this%nLine > this%NumTimes) THEN
      status = OMI_E_FAILURE
      !   ierr = OMI_SMF_setmsg(OMI_E_INPUT, "input nL too large ", &
      !                         "L1Br_open", zero)
      PRINT *,'input nL too large'
      RETURN
    ENDIF

    !   L1bIdf='OML1BR'
    !   IposL1bIdf = index(fn, L1bIdf)
    !   ZoomIdf = fn(IposL1bIdf+7:IposL1bIdf+7)
    !   this%SpaZoomIndex = 0
    !   IF (ZoomIdf == 'Z') THEN
    !      this%SpaZoomIndex = 1
    !   ENDIF
    ! Allocate the memory for storage of geolocation and
    ! radiance fields.  First make sure pointers are not
    ! associated with any specific memory location.  If
    ! they are, deallocate the memory, then allocate the
    ! the proper amount of memory for each data field,
    ! error checking for each memory allocation to make
    ! sure memory is allocated successfully.

    IF (ASSOCIATED(this%Time)) DEALLOCATE(this%Time)
    IF (ASSOCIATED(this%SecInDay)) DEALLOCATE(this%SecInDay)
    IF (ASSOCIATED(this%ScLat)) DEALLOCATE(this%ScLat)
    IF (ASSOCIATED(this%SpacecraftShadowFraction)) DEALLOCATE(this%SpacecraftShadowFraction)
    IF (ASSOCIATED(this%ScLon)) DEALLOCATE(this%ScLon)
    IF (ASSOCIATED(this%ScAlt)) DEALLOCATE(this%ScAlt)
    IF (ASSOCIATED(this%OrbitPhase)) DEALLOCATE(this%OrbitPhase)
    ! IF (ASSOCIATED(this%SolElevation)) DEALLOCATE(this%SolElevation)
    ! IF (ASSOCIATED(this%SolElevMin)) DEALLOCATE(this%SolElevMin)
    ! IF (ASSOCIATED(this%SolElevMax)) DEALLOCATE(this%SolElevMax)
    ! IF (ASSOCIATED(this%SolAzimuth)) DEALLOCATE(this%SolAzimuth)
    ! IF (ASSOCIATED(this%SolAziMin)) DEALLOCATE(this%SolAziMin)
    ! IF (ASSOCIATED(this%SolAziMax)) DEALLOCATE(this%SolAziMax)
    IF (ASSOCIATED(this%Lon)) DEALLOCATE(this%Lon)
    IF (ASSOCIATED(this%Lat)) DEALLOCATE(this%Lat)
    IF (ASSOCIATED(this%SolZenAng)) DEALLOCATE(this%SolZenAng)
    IF (ASSOCIATED(this%SolAziAng)) DEALLOCATE(this%SolAziAng)
    IF (ASSOCIATED(this%ViewZenAng)) DEALLOCATE(this%ViewZenAng)
    IF (ASSOCIATED(this%ViewAziAng)) DEALLOCATE(this%ViewAziAng)
    IF (ASSOCIATED(this%TerrainHeight)) DEALLOCATE(this%TerrainHeight)
    IF (ASSOCIATED(this%TerrainHeightPrec)) DEALLOCATE(this%TerrainHeightPrec)
    IF (ASSOCIATED(this%WaterFraction)) DEALLOCATE(this%WaterFraction)
    IF (ASSOCIATED(this%GPQFlag)) DEALLOCATE(this%GPQFlag)
    IF (ASSOCIATED(this%XTQFlag)) DEALLOCATE(this%XTQFlag)
    IF (ASSOCIATED(this%Radiance)) DEALLOCATE(this%Radiance)
    IF (ASSOCIATED(this%RadPrecision)) DEALLOCATE(this%RadPrecision)
    IF (ASSOCIATED(this%PFlag)) DEALLOCATE(this%PFlag)
    IF (ASSOCIATED(this%RadianceError)) DEALLOCATE(this%RadianceError)
    IF (ASSOCIATED(this%RadianceNoise)) DEALLOCATE(this%RadianceNoise)
    IF (ASSOCIATED(this%SmPixRad)) DEALLOCATE(this%SmPixRad)
    IF (ASSOCIATED(this%SmPixWavelen)) DEALLOCATE(this%SmPixWavelen)
    IF (ASSOCIATED(this%WavelenCoef)) DEALLOCATE(this%WavelenCoef)
    IF (ASSOCIATED(this%WavelenPrec)) DEALLOCATE(this%WavelenPrec)
    ! IF (ASSOCIATED(this%WavelenRefCol)) DEALLOCATE(this%WavelenRefCol)
    IF (ASSOCIATED(this%Config)) DEALLOCATE(this%Config)
    IF (ASSOCIATED(this%MeasClass)) DEALLOCATE(this%MeasClass)
    IF (ASSOCIATED(this%NumSmPixCol)) DEALLOCATE(this%NumSmPixCol)
    IF (ASSOCIATED(this%NumSmPix)) DEALLOCATE(this%NumSmPix)
    IF (ASSOCIATED(this%ExposureType)) DEALLOCATE(this%ExposureType)
    IF (ASSOCIATED(this%ImBinFact)) DEALLOCATE(this%ImBinFact)
    IF (ASSOCIATED(this%GC1)) DEALLOCATE(this%GC1)
    IF (ASSOCIATED(this%GC2)) DEALLOCATE(this%GC2)
    IF (ASSOCIATED(this%GC3)) DEALLOCATE(this%GC3)
    IF (ASSOCIATED(this%GC4)) DEALLOCATE(this%GC4)
    IF (ASSOCIATED(this%DSGC)) DEALLOCATE(this%DSGC)
    IF (ASSOCIATED(this%LSLABF)) DEALLOCATE(this%LSLABF)
    IF (ASSOCIATED(this%USLABF)) DEALLOCATE(this%USLABF)
    IF (ASSOCIATED(this%LDABF)) DEALLOCATE(this%LDABF)
    IF (ASSOCIATED(this%UDABF)) DEALLOCATE(this%UDABF)
    IF (ASSOCIATED(this%MQFlag)) DEALLOCATE(this%MQFlag)
    IF (ASSOCIATED(this%CalSet)) DEALLOCATE(this%CalSet)
    IF (ASSOCIATED(this%SmPixCol)) DEALLOCATE(this%SmPixCol)
    IF (ASSOCIATED(this%GSC1)) DEALLOCATE(this%GSC1)
    IF (ASSOCIATED(this%GSC2)) DEALLOCATE(this%GSC2)
    IF (ASSOCIATED(this%GSC3)) DEALLOCATE(this%GSC3)
    IF (ASSOCIATED(this%SR1)) DEALLOCATE(this%SR1)
    IF (ASSOCIATED(this%SR2)) DEALLOCATE(this%SR2)
    IF (ASSOCIATED(this%SR3)) DEALLOCATE(this%SR3)
    IF (ASSOCIATED(this%SR4)) DEALLOCATE(this%SR4)
    IF (ASSOCIATED(this%BinImgRows)) DEALLOCATE(this%BinImgRows)
    IF (ASSOCIATED(this%StopColumn)) DEALLOCATE(this%StopColumn)
    IF (ASSOCIATED(this%MasterClkPer)) DEALLOCATE(this%MasterClkPer)
    IF (ASSOCIATED(this%ExpTime)) DEALLOCATE(this%ExpTime)
    IF (ASSOCIATED(this%ReadTime)) DEALLOCATE(this%ReadTime)
    IF (ASSOCIATED(this%DetcTemp)) DEALLOCATE(this%DetcTemp)
    IF (ASSOCIATED(this%OptBenchTemp)) DEALLOCATE(this%OptBenchTemp)

    ! What follows is the tedious memory allocation
    ALLOCATE (this%Time(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Time"
      GOTO 999
    ENDIF
    ALLOCATE (this%SecInDay(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SecInDay"
      GOTO 999
    ENDIF
    ALLOCATE (this%ScLat(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ScLat"
      GOTO 999
    ENDIF
    ALLOCATE (this%SpacecraftShadowFraction(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SpacecraftShadowFraction"
      GOTO 999
    ENDIF
    ALLOCATE (this%ScLon(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ScLon"
      GOTO 999
    ENDIF
    ALLOCATE (this%ScAlt(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ScAlt"
      GOTO 999
    ENDIF
    ALLOCATE (this%OrbitPhase(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "OrbitPhase"
      GOTO 999
    ENDIF
    ! ALLOCATE (this%SolElevation(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolElevation"
    !   GOTO 999
    ! ENDIF
    ! ALLOCATE (this%SolAzimuth(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolAzimuth"
    !   GOTO 999
    ! ENDIF
    ! ALLOCATE (this%SolElevMin(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolElevMin"
    !   GOTO 999
    ! ENDIF
    ! ALLOCATE (this%SolElevMax(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolElevMax"
    !   GOTO 999
    ! ENDIF
    ! ALLOCATE (this%SolAziMin(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolAziMin"
    !   GOTO 999
    ! ENDIF
    ! ALLOCATE (this%SolAziMax(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SolAziMax"
    !   GOTO 999
    ! ENDIF
    ALLOCATE (this%Lat(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Lat"
      GOTO 999
    ENDIF
    ALLOCATE (this%Lon(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Lon"
      GOTO 999
    ENDIF
    ALLOCATE (this%LatBounds(this%nCorner, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "LatBounds"
      GOTO 999
    ENDIF
    ALLOCATE (this%LonBounds(this%nCorner, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "LonBounds"
      GOTO 999
    ENDIF
    ALLOCATE (this%SolZenAng(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SolZenAng"
      GOTO 999
    ENDIF
    ALLOCATE (this%SolAziAng(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SolAziAng"
      GOTO 999
    ENDIF
    ALLOCATE (this%ViewZenAng(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ViewZenAng"
      GOTO 999
    ENDIF
    ALLOCATE (this%ViewAziAng(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ViewAziAng"
      GOTO 999
    ENDIF
    ALLOCATE (this%TerrainHeight(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "TerrainHeight"
      GOTO 999
    ENDIF
    ALLOCATE (this%TerrainHeightPrec(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "TerrainHeightPrec"
      GOTO 999
    ENDIF
    ALLOCATE (this%WaterFraction(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "WaterFraction"
      GOTO 999
    ENDIF
    ALLOCATE (this%GPQFlag(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GPQFlag"
      GOTO 999
    ENDIF
    ALLOCATE (this%XTQFlag(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "XTQFlag"
      GOTO 999
    ENDIF
    ALLOCATE (this%Radiance(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Radiance"
      GOTO 999
    ENDIF
    ALLOCATE (this%RadPrecision(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "RadPrecision"
      GOTO 999
    ENDIF
    ALLOCATE (this%RadianceError(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "RadianceError"
      GOTO 999
    ENDIF
    ALLOCATE (this%RadianceNoise(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "RadianceNoise"
      GOTO 999
    ENDIF
    ALLOCATE (this%QualLevel(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "QualFlag"
      GOTO 999
    ENDIF
    ALLOCATE (this%SpecQual(this%nWavel, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SpecQual"
      GOTO 999
    ENDIF

    ALLOCATE (this%LandWater(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "LandWater"
      GOTO 999
    ENDIF

    ALLOCATE (this%PFlag(this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "PFlag"
      GOTO 999
    ENDIF

    ALLOCATE (this%WavelenCoef(this%nWavelCoef, this%nXtrack, this%nLine), &
      STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "WavelenCoef"
      GOTO 999
    ENDIF
    ALLOCATE (this%WavelenShift(this%nXtrack, this%nLine), &
      STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "WavelenShift"
      GOTO 999
    ENDIF
    ALLOCATE (this%Wavelen(this%nWavel, this%nXtrack), &
      STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Wavelen"
      GOTO 999
    ENDIF
    ALLOCATE (this%WavelenNom(this%nWavel, this%nXtrack), &
      STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "WavelenNom"
      GOTO 999
    ENDIF
    ALLOCATE (this%WavelenPrec(this%nWavelCoef, this%nXtrack, this%nLine), &
      STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "WavelenPrec"
      GOTO 999
    ENDIF
    ! ALLOCATE (this%WavelenRefCol(this%nLine), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "WavelenRefCol"
    !   GOTO 999
    ! ENDIF
    ALLOCATE(this%SmPixRad(this%nCoad, this%nXtrack, this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SmPixRad"
      GOTO 999
    ENDIF
    ! ALLOCATE(this%SmPixWavelen(this%nXtrack, this%NumTimesSmallPixel), STAT = ierr)
    ! IF(ierr .NE. 0) THEN
    !   message = "SmPixWavelen"
    !   GOTO 999
    ! ENDIF
    ALLOCATE (this%MeasClass(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "MeasClass"
      GOTO 999
    ENDIF
    ALLOCATE (this%Config(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "Config"
      GOTO 999
    ENDIF
    ALLOCATE (this%MQFlag(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "MQFlag"
      GOTO 999
    ENDIF
    ALLOCATE (this%NumSmPixCol(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "NumSmPixCol"
      GOTO 999
    ENDIF
    ALLOCATE (this%NumSmPix(this%NumTimes), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "NumSmPix"
      GOTO 999
    ENDIF

    ALLOCATE (this%ExposureType(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ExposureType"
      GOTO 999
    ENDIF
    ALLOCATE (this%MasterClkPer(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "MasterClkPer"
      GOTO 999
    ENDIF
    ALLOCATE (this%CalSet(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "CalSet"
      GOTO 999
    ENDIF
    ALLOCATE (this%ExpTime(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ExpTime"
      GOTO 999
    ENDIF
    ALLOCATE (this%ReadTime(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ReadTime"
      GOTO 999
    ENDIF
    ALLOCATE (this%SmPixCol(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SmPixCol"
      GOTO 999
    ENDIF
    ALLOCATE (this%GSC1(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GSC1"
      GOTO 999
    ENDIF
    ALLOCATE (this%GSC2(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GSC2"
      GOTO 999
    ENDIF
    ALLOCATE (this%GSC3(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GSC3"
      GOTO 999
    ENDIF
    ALLOCATE (this%GC1(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GC1"
      GOTO 999
    ENDIF
    ALLOCATE (this%GC2(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GC2"
      GOTO 999
    ENDIF
    ALLOCATE (this%GC3(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GC3"
      GOTO 999
    ENDIF
    ALLOCATE (this%GC4(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "GC4"
      GOTO 999
    ENDIF
    ALLOCATE (this%DSGC(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "DSGC"
      GOTO 999
    ENDIF
    ALLOCATE (this%LSLABF(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "LSLABF"
      GOTO 999
    ENDIF
    ALLOCATE (this%USLABF(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "USLABF"
      GOTO 999
    ENDIF
    ALLOCATE (this%LDABF(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "LDABF"
      GOTO 999
    ENDIF
    ALLOCATE (this%UDABF(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "UDABF"
      GOTO 999
    ENDIF
    ALLOCATE (this%SR1(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SR1"
      GOTO 999
    ENDIF
    ALLOCATE (this%SR2(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SR2"
      GOTO 999
    ENDIF
    ALLOCATE (this%SR3(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SR3"
      GOTO 999
    ENDIF
    ALLOCATE (this%SR4(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "SR4"
      GOTO 999
    ENDIF
    ALLOCATE (this%DetcTemp(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "DetcTemp"
      GOTO 999
    ENDIF
    ALLOCATE (this%OptBenchTemp(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "OptBenchTemp"
      GOTO 999
    ENDIF
    ALLOCATE (this%ImBinFact(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "ImBinFact"
      GOTO 999
    ENDIF
    ALLOCATE (this%BinImgRows(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "BinImgRows"
      GOTO 999
    ENDIF
    ALLOCATE (this%StopColumn(this%nLine), STAT = ierr)
    IF(ierr .NE. 0) THEN
      message = "StopColumn"
      GOTO 999
    ENDIF

    !Zoom mode
    this%initialized = .TRUE.
    status = OMI_S_SUCCESS
    RETURN

    999     CONTINUE
    status = OMI_E_FAILURE
    !   ierr = OMI_SMF_setmsg(OMI_E_MEM_ALLOC, message, "L1Br_open", zero)
    PRINT *,'Error in allocating memory'
    RETURN

  END FUNCTION L1Br_open

  !! 2. L1Br_getDIMsize
  !
  !    Functionality:
  !
  !       This function retrieves the dimension size for the named dimension.
  !       A user can use this function to find the size of each dimension in
  !       the L1B file and plan the amount of storage needed to in the calling
  !       procedure.
  !
  !    Calling Arguments:
  !
  !    Inputs:
  !
  !       this:    the block data structure
  !       dimname: the dimension name
  !
  !    Outputs:
  !
  !       size: the size for the input dimension, if error occurs, -1 is returned.
  !
  !    Change History:
  !
  !       Date            Author          Modifications
  !       ====            ======          =============
  !       January 2005    Jeremy Warner   Original Source (derived from original
  !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  !
  !!
  FUNCTION L1Br_getDIMsize(this, dimname) RESULT(size)
    !   INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    CHARACTER (LEN = *), INTENT(IN):: dimname
    INTEGER (KIND = 4):: size
    CHARACTER (LEN = 256):: func_name

    func_name = 'L1Br_getDIMsize'
    IF(.NOT. this%initialized) THEN
      !      ierr = OMI_SMF_setmsg(OMI_E_INPUT, &
      !                            "input block not initialized", &
      !                            ! "L1Br_getDIMsize", zero)
      PRINT *,'input block not initialized'
      size = -1
      RETURN
    ENDIF

    SELECT CASE(dimname)
    CASE("NumTimes")
      size = this%NumTimes
      RETURN
    CASE("nCorner")
      size = this%nCorner
      RETURN
    CASE("nXtrack")
      size = this%nXtrack
      RETURN
    CASE("nWavel")
      size = this%nWavel
      RETURN
    CASE("nWavelCoef")
      size = this%nWavelCoef
      RETURN
      ! CASE("NumTimesSmallPixel")
      !   size = this%NumTimesSmallPixel
      !   RETURN
    CASE("nCoad")
      size = this%nCoad
      RETURN
    CASE DEFAULT
      msg = "unknown dimension name: " // dimname
      PRINT *,msg
      ! ierr = OMI_SMF_setmsg(OMI_E_INPUT, msg, "L1Br_getDIMsize", zero)
      size = -1
      RETURN
    END SELECT
  END FUNCTION L1Br_getDIMsize

  !! 3. L1Br_getSWdims
  !
  !    Functionality:
  !
  !       This function retrieves the dimension sizes for the named dimension.
  !       A user can use this function to find the size of each dimension in
  !       the L1B file and plan the amount of storage needed to in the calling
  !       procedure.  It is similar in function to L1Br_getDIMsize.
  !
  !    Calling Arguments:
  !
  !    Inputs:
  !
  !       this:    the block data structure
  !
  !    Outputs:
  !
  !       The following are keyword arguments.  Only those present in the argument
  !       list will be set by the function.
  !
  !       Keyword                    Corresponding L1B Geolocation Data Field
  !       =======                    ========================================
  !       NumTimes_k                 total number of lines in the swath
  !       nXtrack_k                  number of cross-track pixels in the swath
  !       nWavel_k                   number of wavelengths for each line and pixel number
  !       nWavelCoef_k               number of wavelength coefficients
  !       NumTimesSmallPixel_k       total number of small pixel columns in the swath
  !       nCoad_k                    number of Co-adds (usually 5)
  !
  !       status: the return PGS_SMF status value
  !
  !    Change History:
  !
  !       Date            Author          Modifications
  !       ====            ======          =============
  !       January 2005    Jeremy Warner   Original Source (derived from original
  !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  !
  !!
  FUNCTION L1Br_getSWdims(this, NumTimes_k, nXtrack_k, &
      nCoad_k, &
      nWavel_k, nWavelCoef_k, fieldname_k) RESULT(status)
    !  INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: NumTimes_k
    ! INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: NumTimesSmallPixel_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: nCoad_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: nXtrack_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: nWavel_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: nWavelCoef_k
    CHARACTER (LEN = *), OPTIONAL, INTENT(OUT):: fieldname_k
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    status = OMI_S_SUCCESS
    func_name='L1Br_getSWdims'
    IF(.NOT. this%initialized) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF( PRESENT(NumTimes_k) )           NumTimes_k           = this%NumTimes
    IF( PRESENT(nXtrack_k) )            nXtrack_k            = this%nXtrack
    IF( PRESENT(nWavelCoef_k) )         nWavelCoef_k         = this%nWavelCoef
    ! IF( PRESENT(NumTimesSmallPixel_k) ) NumTimesSmallPixel_k = this%NumTimesSmallPixel
    IF( PRESENT(nCoad_k) )              nCoad_k              = this%nCoad
    IF( PRESENT(fieldname_k) )          fieldname_k          = this%fieldname
    IF( PRESENT(nWavel_k) )             nWavel_k             = this%nWavel
    RETURN
  END FUNCTION L1Br_getSWdims
  !
  !!! Private function: init_l1b_blk
  ! !
  ! !    Functionality:
  ! !
  ! !       This is a private function which is only used by other functions
  ! !       in this MODULE to initialize the block data structure with fill values.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this:    the block data structure
  ! !
  ! !    Outputs:
  ! !
  ! !       status:  the return PGS_SMF status value; this function only returns OMI_S_SUCCESS
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source
  !         May 2020        Ramaswamy T     Modify netCDF4 interface
  ! !
  !!!
  FUNCTION init_l1b_blk(this) RESULT(status)
    !NC4       INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4):: status

    status = 0

    this%Time(1:this%nLine) = r8fill
    this%SecInDay(1:this%nLine) = rfill
    this%ScLat(1:this%nLine) = rfill
    this%SpacecraftShadowFraction(1:this%nLine) = rfill
    this%ScLon(1:this%nLine) = rfill
    this%ScAlt(1:this%nLine) = rfill
    this%OrbitPhase(1:this%nLine) = rfill
    ! this%SolElevation(1:this%nLine) = rfill
    ! this%SolElevMin(1:this%nLine) = rfill
    ! this%SolElevMax(1:this%nLine) = rfill
    ! this%SolAzimuth(1:this%nLine) = rfill
    ! this%SolAziMin(1:this%nLine) = rfill
    ! this%SolAziMax(1:this%nLine) = rfill
    this%Lon(1:this%nXtrack, 1:this%nLine) = rfill
    this%Lat(1:this%nXtrack, 1:this%nLine) = rfill
    this%LonBounds(1:this%nCorner, 1:this%nXtrack, 1:this%nLine) = rfill
    this%LatBounds(1:this%nCorner, 1:this%nXtrack, 1:this%nLine) = rfill
    this%SolZenAng(1:this%nXtrack, 1:this%nLine) = rfill
    this%SolAziAng(1:this%nXtrack, 1:this%nLine) = rfill
    this%ViewZenAng(1:this%nXtrack, 1:this%nLine) = rfill
    this%ViewAziAng(1:this%nXtrack, 1:this%nLine) = rfill
    this%TerrainHeight(1:this%nXtrack, 1:this%nLine) = int16fill
    this%TerrainHeightPrec(1:this%nXtrack, 1:this%nLine) = int16fill
    this%WaterFraction(1:this%nXtrack, 1:this%nLine) = int8fill
    this%GPQFlag(1:this%nXtrack, 1:this%nLine) = int16fill
    ! this%XTQFlag(1:this%nXtrack, 1:this%nLine) = int8fill
    this%XTQFlag(1:this%nXtrack, 1:this%nLine) = int16fill
    this%Radiance(1:this%nWavel, 1:this%nXtrack, 1:this%nLine) = rfill
    this%RadPrecision(1:this%nWavel, 1:this%nXtrack, 1:this%nLine) = int16fill
    this%RadianceError(1:this%nWavel, 1:this%nXtrack, 1:this%nLine) = rfill
    this%RadianceNoise(1:this%nWavel, 1:this%nXtrack, 1:this%nLine) = rfill
    this%PFlag(1:this%nXtrack, 1:this%nLine) = int16fill
    this%WavelenCoef(1:this%nWavelCoef, 1:this%nXtrack, 1:this%nLine) = rfill
    this%WavelenPrec(1:this%nWavelCoef, 1:this%nXtrack, 1:this%nLine) = rfill
    ! this%WavelenRefCol(1:this%nLine) = int16fill
    this%SmPixRad(:,1:this%nXtrack, 1:this%nLine) = rfill
    ! this%SmPixWavelen(1:this%nXtrack, 1:this%NumTimesSmallPixel) = rfill
    this%MeasClass(1:this%nLine) = int8fill
    this%Config(1:this%nLine) = int32fill
    this%MQFlag(1:this%nLine) = int16fill
    this%SpecQual(this%nWavel, this%nXtrack, this%nLine) = int8fill
    this%QualLevel(this%nWavel, this%nXtrack, this%nLine) = int8fill
    this%LandWater( this%nXtrack, this%nLine) = int8fill
    this%NumSmPixCol(1:this%nLine) = int8fill
    this%NumSmPix(1:this%NumTimes) = int8fill
    this%ExposureType(1:this%nLine) = int8fill
    this%MasterClkPer(1:this%nLine) = rfill
    this%CalSet(1:this%nLine) = int16fill
    this%ExpTime(1:this%nLine) = rfill
    this%ReadTime(1:this%nLine) = rfill
    this%SmPixCol(1:this%nLine) = int16fill
    this%GSC1(1:this%nLine) = int16fill
    this%GSC2(1:this%nLine) = int16fill
    this%GSC3(1:this%nLine) = int16fill
    this%GC1(1:this%nLine) = int8fill
    this%GC2(1:this%nLine) = int8fill
    this%GC3(1:this%nLine) = int8fill
    this%GC4(1:this%nLine) = int8fill
    this%DSGC(1:this%nLine) = int8fill
    this%LSLABF(1:this%nLine) = int8fill
    this%USLABF(1:this%nLine) = int8fill
    this%LDABF(1:this%nLine) = int8fill
    this%UDABF(1:this%nLine) = int8fill
    this%SR1(1:this%nLine) = int16fill
    this%SR2(1:this%nLine) = int16fill
    this%SR3(1:this%nLine) = int16fill
    this%SR4(1:this%nLine) = int16fill
    this%DetcTemp(1:this%nLine) = rfill
    this%OptBenchTemp(1:this%nLine) = rfill
    this%ImBinFact(1:this%nLine) = int8fill
    this%BinImgRows(1:this%nLine) = int16fill
    this%StopColumn(1:this%nLine) = int16fill

  END FUNCTION init_l1b_blk
  !
  !!! Private function: fill_l1b_blk
  ! !
  ! !    Functionality:
  ! !
  ! !       This is a private function which is only used by other functions
  ! !       in this MODULE to fill the block data structure with data from the L1B file.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this:    the block data structure
  ! !       iLine:   the starting line for the data block
  ! !
  ! !    Outputs:
  ! !
  ! !       status:  the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION fill_l1b_blk(this, iLine) RESULT(status)
    use iso_c_binding
    !       INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4):: status
    INTEGER (KIND = 4):: rank
    INTEGER (KIND = 4):: nL, swid, swfid, fldflg
    INTEGER (KIND = 4), DIMENSION(1:3):: dims
    INTEGER (KIND = 4):: i, j, k
    CHARACTER (LEN = 256):: func_name
    ! Instrument settings
    TYPE (inst_settings_type), dimension(:), allocatable :: inst_set
    TYPE (house_keeping_type), dimension(:), allocatable :: house_data

    func_name ='L1B_Reader_class:: fill_l1b_blk'
    IF(.NOT. this%initialized) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    this%iLine = iLine

    IF(iLine < 1 .OR. iLine > this%NumTimes) THEN
      call PGE_WARN(func_name, 'I/O Error','iLine out of range')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    ! If, from the starting line, the number of lines to be read into
    ! the block data structure goes past the final line in the L1B swath,
    ! then recalculate the number of lines to be read.

    ! NC4 - Increasing nL by 1 for last block as we are dealing with 1-based
    !         indexing instead of zero based indexing
    IF((iLine+this%nLine) > this%NumTimes) THEN
      ! nL = this%NumTimes-iLine
      nL = this%NumTimes-iLine+1
    ELSE
      nL = this%nLine
    ENDIF
    ! this%eLine = this%iLine+nL-1
    this%eLine = this%iLine+nL

    ! Open the L1B swath file
    !TODO-RAM Check if L1B file open required

    !  swfid = swopen(this%filename, DFACC_READ)
    !  IF(swfid < zero) THEN
    !     status = OMI_E_FAILURE
    !     ierr = OMI_SMF_setmsg(OMI_E_FILE_OPEN, this%filename, &
    !           "fill_l1b_blk", zero)
    !     RETURN
    !  ENDIF
    ! Attach to the swath
    !TODO-RAM Check if Initialization required

    ! Initialize L1B block to fill values

    ierr = init_l1b_blk(this)

    ! Initialize start, count, stride for all dimensions
    start_2d = (/iLine, 1/)
    start_3d = (/1, iLine, 1/)
    start_4d = (/1, 1, iLine, 1/)
    stride_2d = (/1, 1/)
    stride_3d = (/1, 1, 1/)
    stride_4d = (/1, 1, 1, 1/)
    count_2d = (/nL, 1/)
    count_3d = (/this%nXtrack, nL, 1/)
    count_4d = (/this%nWavel, this%nXtrack, nL, 1/)
    count_4d_cor = (/this%nCorner, this%nXtrack, nL, 1/)
    count_4d_coadd = (/this%nCoad, this%nXtrack, nL, 1/)
    ! Allocate temporary arrays to hold arrays read from netCDF4 datasets.
    if (allocated(arr_2dik4)) deallocate(arr_2dik4)
    if (allocated(arr_3dik4)) deallocate(arr_3dik4)
    if (allocated(arr_4dik4)) deallocate(arr_4dik4)
    if (allocated(arr_2dik8)) deallocate(arr_2dik8)
    if (allocated(arr_2drk4)) deallocate(arr_2drk4)
    if (allocated(arr_3drk4)) deallocate(arr_3drk4)
    if (allocated(arr_4drk4)) deallocate(arr_4drk4)
    if (allocated(arr_4drk4_cor)) deallocate(arr_4drk4_cor)
    if (allocated(arr_2drk8)) deallocate(arr_2drk8)
    if (allocated(arr_3drk8)) deallocate(arr_3drk8)
    allocate(arr_3drk4(this%nXtrack, nL, 1))
    allocate(arr_2drk4(nL, 1))
    allocate(arr_2dik4(nL, 1))
    allocate(arr_3drk8(this%nXtrack, nL, 1))
    allocate(arr_2drk8(nL, 1))
    allocate(arr_3dik4(this%nXtrack, nL, 1))
    allocate(arr_4dik4(this%nWavel, this%nXtrack, nL, 1))
    allocate(arr_4drk4(this%nWavel, this%nXtrack, nL, 1))
    allocate(arr_4drk4_cor(this%nCorner, this%nXtrack, nL, 1))

    ! Read all L1B datasets

    status = nc4read_arrays(this%id_obs_grp, "time_TAI93", &
      arr_2drk8, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%Time(1:nL) = arr_2drk8(1:nL, 1)
    endif

    status = nc4read_arrays(this%id_obs_grp, "delta_time", &
      arr_2dik4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      ! convert milli-seconds to seconds
      this%SecInDay(1:nL) = float(arr_2dik4(1:nL, 1))/1000.0
    endif

    status = nc4read_arrays(this%id_geo_grp, "satellite_latitude", &
      arr_2drk4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%ScLat(1:nL) = arr_2drk4(1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "satellite_longitude", &
      arr_2drk4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%ScLon(1:nL) = arr_2drk4(1:nL, 1)
    endif


    status = nc4read_arrays(this%id_geo_grp, "satellite_altitude", &
      arr_2drk4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%ScAlt(1:nL) = arr_2drk4(1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "satellite_orbit_phase", &
      arr_2drk4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%OrbitPhase(1:nL) = arr_2drk4(1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "satellite_shadow_fraction", &
      arr_2drk4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%SpacecraftShadowFraction(1:nL) = arr_2drk4(1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "longitude", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%Lon(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "latitude", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%Lat(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "longitude_bounds", &
      arr_4drk4_cor, start_4d, count_4d_cor, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%LonBounds(:,:,1:nL) = arr_4drk4_cor(:,:,1:nL, 1)
    endif


    status = nc4read_arrays(this%id_geo_grp, "latitude_bounds", &
      arr_4drk4_cor, start_4d, count_4d_cor, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%LatBounds(:,:,1:nL) = arr_4drk4_cor(:,:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "solar_zenith_angle", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%SolZenAng(:,1:nL) = arr_3drk4(:,:,1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "solar_azimuth_angle", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%SolAziAng(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "viewing_zenith_angle", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%ViewZenAng(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "viewing_azimuth_angle", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%ViewAziAng(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_geo_grp, "surface_altitude", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%TerrainHeight(:,1:nL) = INT(arr_3dik4(:,1:nL, 1), kind = 2)
    endif

    status = nc4read_arrays(this%id_geo_grp, "surface_altitude_precision", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%TerrainHeightPrec(:,1:nL) = INT(arr_3dik4(:,1:nL, 1), kind = 2)
    endif

    status = nc4read_arrays(this%id_geo_grp, "water_fraction", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%WaterFraction(:,1:nL) = ibits(arr_3dik4(:,1:nL, 1), 0, 8)
    endif

    status = nc4read_arrays(this%id_obs_grp, "measurement_quality", &
      arr_2dik4, start_2d, count_2d, stride_2d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      do i = 1, nL
        this%MQFlag(i) = ibits(arr_2dik4(i, 1), 0, 16)
      enddo
    endif

    status = nc4read_arrays(this%id_obs_grp, "ground_pixel_quality", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%GPQFlag(:,1:nL) = INT(arr_3dik4(:,1:nL, 1), kind = 2)
    endif

    status = nc4read_arrays(this%id_obs_grp, "xtrack_quality", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%XTQFlag(:,1:nL) = ibits(arr_3dik4(:,1:nL, 1), 0, 16)
    endif
  
    status = nc4read_arrays(this%id_obs_grp, "quality_level", &
      arr_4dik4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%QualLevel(:,:, 1:nL ) = ibits(arr_4dik4(:,:,1:nL, 1), 0, 8)
    endif

    status = nc4read_arrays(this%id_geo_grp, "land_water_classification", &
      arr_3dik4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%LandWater(:,1:nL ) = ibits(arr_3dik4(:,1:nL, 1), 0, 8)
    endif

    status = nc4read_arrays(this%id_obs_grp, "spectral_channel_quality", &
      arr_4dik4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%SpecQual(:,:, 1:nL ) = ibits(arr_4dik4(:,:,1:nL, 1), 0, 8)
    endif

    status = nc4read_arrays(this%id_obs_grp, "radiance", &
      arr_4drk4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%Radiance(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_obs_grp, "radiance_error", &
      arr_4drk4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%RadianceError(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
    endif

    status = nc4read_arrays(this%id_obs_grp, "radiance_noise", &
      arr_4drk4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%RadianceNoise(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
    endif

    ! wavelength parameters
    status = nc4read_arrays(this%id_inst_grp, "wavelength_reference_column", &
      this%WavelenRefCol)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    endif

    !wavelength_shift has dimension nXTrack, NumTimes, 1
    status = nc4read_arrays(this%id_inst_grp, "wavelength_shift", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%WavelenShift(:,1:nL) = arr_3drk4(:,1:nL, 1)
    endif

    ! nominal_wavelength has dimension this%nWavel, this%nXtrack, 1
    if (allocated(arr_3drk4)) deallocate(arr_3drk4)
    allocate(arr_3drk4(this%nWavel, this%nXtrack, 1))
    start_3d = (/1, 1, 1 /)
    stride_3d = (/1, 1, 1 /)
    count_3d = (/this%nWavel, this%nXtrack, 1/)
    status = nc4read_arrays(this%id_inst_grp, "wavelength", &
      arr_3drk4, start_3d, count_3d, stride_3d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%wavelenNom(:,:) = arr_3drk4(:,:,1)
    endif

    ! wavelength_coefficient
    if (allocated(arr_4drk4 )) deallocate(arr_4drk4)
    allocate(arr_4drk4(this%nWavelCoef, this%nXtrack, this%NumTimes, 1))
    start_4d = (/1, 1, iLine, 1/)
    stride_4d = (/1, 1, 1, 1/)
    count_4d = (/this%nWavelCoef, this%nXtrack, nL, 1/)
    status = nc4read_arrays(this%id_inst_grp, "wavelength_coefficient", &
      arr_4drk4, start_4d, count_4d, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%WavelenCoef(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
    endif

!RAM     status = nc4read_arrays(this%id_inst_grp, "wavelength_coefficient_precision", &
!RAM       arr_4drk4, start_4d, count_4d, stride_4d)
!RAM     IF(status .NE. OMI_S_SUCCESS) then
!RAM       goto 1001
!RAM     else
!RAM       this%WavelenPrec(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
!RAM     endif

    if (allocated(arr_2drk4)) deallocate(arr_2drk4)
    allocate(arr_2drk4( 1, 1))
    arr_2drk4 = rfill
    status = nc4read_arrays(this%id_geo_grp, "earth_sun_distance", &
      arr_2drk4)
    IF(status .NE. OMI_S_SUCCESS) then
      write(msg, '(A, A)')TRIM(msg), 'earth_sun_distance'
      call PGE_WARN(func_name, 'I/O Error', msg)
      goto 1001
    else
      this%earth_sun_dist = arr_2drk4(1, 1)
    endif

    if (allocated(arr_4drk4 )) deallocate(arr_4drk4)
    allocate(arr_4drk4(this%nCoad, this%nXtrack, this%NumTimes, 1))
    status = nc4read_arrays(this%id_obs_grp, "small_pixel_radiance", &
      arr_4drk4, start_4d, count_4d_coadd, stride_4d)
    IF(status .NE. OMI_S_SUCCESS) then
      goto 1001
    else
      this%SmPixRad(:,:, 1:nL ) = arr_4drk4(:,:,1:nL, 1)
    endif

    ! read instrument settings
    !===========================
    if (allocated(inst_set)) deallocate(inst_set)
    allocate(inst_set(nL))
    call  read_inst_settings(this%filename, this%bandname, this%modename,  &
          this%iline, nL, inst_set)
    this%Config(1:nL)     = inst_set(1:nL)%ic_id
    this%ReadTime(1:nL)   = inst_set(1:nL)%readout_time
    this%GC1(1:nL) = ibits(inst_set(1:nL)%gain_code_1, 0, 8 )
    this%GC2(1:nL) = ibits(inst_set(1:nL)%gain_code_2, 0, 8 )
    this%GC3(1:nL) = ibits(inst_set(1:nL)%gain_code_3, 0, 8 )
    this%GC4(1:nL) = ibits(inst_set(1:nL)%gain_code_4, 0, 8 )
    this%DSGC(1:nL) = ibits(inst_set(1:nL)%gain_code_ds, 0, 8 )
    this%LSLABF(1:nL) = inst_set(1:nL)%lsa_binning_factor
    this%USLABF(1:nL) = inst_set(1:nL)%usa_binning_factor
    this%LDABF(1:nL) = inst_set(1:nL)%lda_binning_factor
    this%UDABF(1:nL) = inst_set(1:nL)%uda_binning_factor
    this%GSC1(1:nL) = inst_set(1:nL)%gain_switch_column_1
    this%GSC2(1:nL) = inst_set(1:nL)%gain_switch_column_2
    this%GSC3(1:nL) = inst_set(1:nL)%gain_switch_column_3
    this%SR1(1:nL) = inst_set(1:nL)%skip_rows_1
    this%SR2(1:nL) = inst_set(1:nL)%skip_rows_2
    this%SR3(1:nL) = inst_set(1:nL)%skip_rows_3
    this%SR4(1:nL) = inst_set(1:nL)%skip_rows_4
    this%MeasClass(1:nL) = ibits(inst_set(1:nL)%measurement_class, 0, 8)
    this%BinImgRows(1:nL) = inst_set(1:nL)%binned_image_rows
    this%ImBinFact(1:nL) = inst_set(1:nL)%img_binning_factor
    this%ExpTime(1:nL) = inst_set(1:nL)%exposure_time
    this%StopColumn(1:nL) = inst_set(1:nL)%stop_column

    ! add master clock period and small pixel column - MB 10/22/23
    this%MasterClkPer(1:nL) = inst_set(1:nL)%master_cycle_period
    this%SmPixCol(1:nL) = inst_set(1:nL)%small_pixel_column

    ! (1:nL) = inst_set(1:nL)%
    ! (1:nL) = inst_set(1:nL)%

    if (allocated(inst_set)) deallocate(inst_set)

    ! read housekeeping data
    if (allocated(house_data)) deallocate(house_data)
    allocate(house_data(nL))
    call  read_house_keeping(this%filename, this%bandname, this%modename,  &
          this%iline, nL, house_data)
    this%detctemp(1:nL)     = house_data(1:nL)%temp_det
    this%optbenchtemp(1:nL)     = house_data(1:nL)%temp_opb
    if (allocated(house_data)) deallocate(house_data)

    1001    continue
    IF(status .NE. OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'I/O Error','Unrecoverable error reading L1B file')
    ENDIF
    RETURN
  END FUNCTION fill_l1b_blk
  !
  !!! Private function: check_blk_pix
  ! !
  ! ! Functionality:
  ! !
  ! !   This function checks to make sure the requested indecies are within
  ! !   range and returns 1-based indecies (from 0-based inputs).  Note that
  ! !   input indicies refer to the file, while output indicies refer to
  ! !   the structure in memory (only a part of the input L1B file is
  ! !   stored memory at a given time).
  ! !
  ! ! Calling Arguments:
  ! !
  ! ! Inputs:
  ! !
  ! !    this      L1B block
  ! !    iLine     requested line (0-indexed) of L1B file
  ! !    iPix      requested Xtrack position (0-indexed) of L1B file
  ! !
  ! ! Outputs:
  ! !
  ! !    j         Index into L1B structure, line number, (1-indexed)
  ! !    i         Index into L1B structure, Xtrack position, (1-indexed)
  ! !
  ! !    status    the return PGS_SMF status value
  ! !
  ! ! Change History:
  ! !
  ! !    Date            Author          Modifications
  ! !    ====            ======          =============
  ! !    January 2005    Jeremy Warner   Original Source
  ! !    May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
  FUNCTION check_blk_pix(this, iLine, iPix, j, i) RESULT(status)
    !  INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4), INTENT(IN):: iPix
    INTEGER (KIND = 4), INTENT(OUT):: i, j
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    func_name='check_blk_pix'
    status = OMI_S_SUCCESS
    IF(.NOT. this%initialized) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF(iLine < 1 .OR. iLine > this%NumTimes) THEN
      call PGE_WARN(func_name, 'I/O Error','iLine out of range')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF(iPix < 1 .OR. iPix > this%nXtrack) THEN
      write(msg, '(A, I5, A, I5)')'iPix=',iPix, 'nXtrack=',this%nXtrack
      call PGE_WARN(func_name, 'iPix out of range', msg)
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF(iLine < this%iLine .OR. iLine > this%eLine) THEN
      status = fill_l1b_blk(this, iLine)
      IF(status .NE. OMI_S_SUCCESS) THEN
        call PGE_WARN(func_name, 'I/O Error','retrieve data block')
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    i = iPix
    j = iLine-this%iLine+1
    RETURN
  END FUNCTION check_blk_pix
  !
  !!! Private function: check_blk_line
  ! !
  ! ! Functionality:
  ! !
  ! !   This function checks to make sure the requested index is within
  ! !   range and returns 1-based index (from 0-based input).  Note that
  ! !   input index refers to the file, while output index refers to
  ! !   the structure in memory (only a part of the input L1B file is
  ! !   stored memory at a given time).
  ! !
  ! ! Calling Arguments:
  ! !
  ! ! Inputs:
  ! !
  ! !    this      L1B block
  ! !    iLine     requested line (0-indexed) of the L1B file
  ! !
  ! ! Outputs:
  ! !
  ! !    j         Index into L1B structure, line number, (1-indexed)
  ! !
  ! !    status    the return PGS_SMF status value
  ! !
  ! ! Change History:
  ! !
  ! !    Date            Author          Modifications
  ! !    ====            ======          =============
  ! !    January 2005    Jeremy Warner   Original Source
  !      May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
  FUNCTION check_blk_line(this, iLine, j) RESULT(status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4), INTENT(OUT):: j
    INTEGER (KIND = 4):: ierr
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    func_name='check_blk_line'
    status = OMI_S_SUCCESS
    IF(.NOT. this%initialized) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      RETURN
    ENDIF

    IF(iLine < 1 .OR. iLine > this%NumTimes) THEN
      write(msg, '(A, I5, A, I5)')'iLine=',iLine, 'NumTimes=',this%NumTimes
      call PGE_WARN(func_name, 'iLine out of range', TRIM(msg))
      RETURN
    ENDIF

    !NC4 Change condition from > to >= for iline check
    ! IF(iLine < this%iLine .OR. iLine > this%eLine) THEN
    IF(iLine < this%iLine .OR. iLine >= this%eLine) THEN
      status = fill_l1b_blk(this, iLine)
      IF(status .NE. OMI_S_SUCCESS) THEN
        call PGE_WARN(func_name, "I/O Error", "retrieve data block")
        RETURN
      ENDIF
    ENDIF

    j = iLine-this%iLine+1
    RETURN
  END FUNCTION check_blk_line
  !
  !!! 4. L1Br_getGEOpix
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more geolocation data field values from
  ! !       the data block.  This function returns only a single pixel value
  ! !       for each given keyword (i.e., a value for a single line and a single
  ! !       Xtrack position).
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !       iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !             range from 0 to (nXtrack-1) inclusive.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.
  ! !
  ! !       Keyword                    Corresponding L1B Geolocation Data Field
  ! !       =======                    ========================================
  ! !       Time_k                     Time [TAI 93]
  ! !       SecondsInDay_k             SecondsInDay
  ! !       SpacecraftLatitude_k       SpacecraftLatitude [satellite_latitude]
  ! !       SpacecraftLongitude_k      SpacecraftLongitude [satellite_longitude]
  ! !       SpacecraftAltitude_k       SpacecraftAltitude [satellite_altitude]
  ! !       OrbitPhase_k               OrbitPhase [satellite_orbit_phase]
  ! !       Latitude_k                 Latitude
  ! !       LongitudeBounds_k          LongitudeBounds[longitude_bounds]
  ! !       LatitudeBounds_k           LatitudeBounds[latitude_bounds]
  ! !       Longitude_k                Longitude
  ! !       SolarZenithAngle_k         SolarZenithAngle
  ! !       SolarAzimuthAngle_k        SolarAzimuthAngle
  ! !       ViewingZenithAngle_k       ViewingZenithAngle
  ! !       ViewingAzimuthAngle_k      ViewingAzimuthAngle
  ! !       TerrainHeight_k            TerrainHeight [surface_altitude]
  ! !       GroundPixelQualityFlags_k  GroundPixelQualityFlags[ground_pixel_quality]
  ! !       TerrainHeightPrec_k        TerrainHeightPrecision [surface_altitude_precision]
  ! !       LandWaterClassification_k  land_water_classification [8-bit unsigned]
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source ((derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getGEOpix(this, iLine, iPix, Time_k, SecondsInDay_k, &
      SpacecraftLatitude_k, SpacecraftLongitude_k, &
      SpacecraftAltitude_k,  OrbitPhase_k, &
      LatitudeBounds_k, LongitudeBounds_k, &
      Latitude_k, Longitude_k, SolarZenithAngle_k, &
      SolarAzimuthAngle_k, ViewingZenithAngle_k, &
      ViewingAzimuthAngle_k, TerrainHeight_k, &
      GroundPixelQualityFlags_k, TerrainHeightPrec_k, &
      SpacecraftShadowFraction_k, WaterFraction_k, &
      EarthSunDistance_k, LandWaterClassification_k, &
      XTrackQualityFlags_k) RESULT (status)

    !  INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4), INTENT(IN):: iPix
    REAL (KIND = 8), OPTIONAL, INTENT(OUT):: Time_k
    REAL (KIND = 4), OPTIONAL, INTENT(OUT):: LatitudeBounds_k(4), &
      LongitudeBounds_k(4)
    REAL (KIND = 4), OPTIONAL, INTENT(OUT):: Latitude_k, Longitude_k, &
      SpacecraftLatitude_k, SpacecraftLongitude_k, &
      SpacecraftAltitude_k, OrbitPhase_k, &
      SolarZenithAngle_k, &
      SolarAzimuthAngle_k, ViewingZenithAngle_k, &
      ViewingAzimuthAngle_k, SecondsInDay_k, SpacecraftShadowFraction_k, &
      EarthSunDistance_k
    INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: TerrainHeight_k
    INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: GroundPixelQualityFlags_k
    INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: TerrainHeightPrec_k
    INTEGER (KIND = 1), OPTIONAL, INTENT(OUT):: WaterFraction_k
    INTEGER (KIND = 1), OPTIONAL, INTENT(OUT):: LandWaterClassification_k
    INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: XTrackQualityFlags_k
    INTEGER:: i, j
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    func_name='L1Br_getGEOpix'
    status = check_blk_pix(this, iLine, iPix, j, i)
    ! PRINT *, iLine, iPix, i, j
    IF(status .NE. OMI_S_SUCCESS) THEN
      !  ierr = OMI_SMF_setmsg(OMI_E_GENERAL, "Failed retrieving geolocation data.", &
      !                        "L1Br_getGEOpix", one)
      call PGE_WARN(func_name, 'I/O Error','Failed retrieving geolocation data.')
      RETURN
    ENDIF

    IF(PRESENT(EarthSunDistance_k))        EarthSunDistance_k = & 
      this%earth_sun_dist
    IF(PRESENT(Time_k))                    Time_k = this%Time(j)
    IF(PRESENT(SecondsInDay_k))            SecondsInDay_k = this%SecInDay(j)
    IF(PRESENT(SpacecraftLatitude_k))      SpacecraftLatitude_k = this%ScLat(j)
    IF(PRESENT(SpacecraftLongitude_k))     SpacecraftLongitude_k = this%ScLon(j)
    IF(PRESENT(SpacecraftAltitude_k))      SpacecraftAltitude_k = this%ScAlt(j)
    IF(PRESENT(OrbitPhase_k))              OrbitPhase_k = this%OrbitPhase(j)
    IF(PRESENT(SpacecraftShadowFraction_k))  &
      SpacecraftShadowFraction_k =this%SpacecraftShadowFraction(j)
    IF(PRESENT(Latitude_k))                Latitude_k = this%Lat(i, j)
    IF(PRESENT(Longitude_k))               Longitude_k = this%Lon(i, j)
    IF(PRESENT(LatitudeBounds_k))          LatitudeBounds_k(:) = this%LatBounds(:,i, j)
    IF(PRESENT(LongitudeBounds_k))         LongitudeBounds_k(:) = this%LonBounds(:,i, j)
    IF(PRESENT(SolarZenithAngle_k))        SolarZenithAngle_k = this%SolZenAng(i, j)
    IF(PRESENT(SolarAzimuthAngle_k))       SolarAzimuthAngle_k = this%SolAziAng(i, j)
    IF(PRESENT(ViewingZenithAngle_k))      ViewingZenithAngle_k = this%ViewZenAng(i, j)
    IF(PRESENT(ViewingAzimuthAngle_k))     ViewingAzimuthAngle_k = this%ViewAziAng(i, j)
    IF(PRESENT(TerrainHeight_k))           TerrainHeight_k = this%TerrainHeight(i, j)
    IF(PRESENT(GroundPixelQualityFlags_k)) GroundPixelQualityFlags_k = this%GPQFlag(i, j)
    IF(PRESENT(TerrainHeightPrec_k))      TerrainHeightPrec_k = this%TerrainHeightPrec(i, j)
    IF(PRESENT(WaterFraction_k))          WaterFraction_k = this%WaterFraction(i,j)
    IF(PRESENT(LandWaterClassification_k)) LandWaterClassification_k = this%LandWater(i,j)
    IF(PRESENT(XTrackQualityFlags_k)) XTrackQualityFlags_k = this%XTQFlag(i, j)

    RETURN
  END FUNCTION L1Br_getGEOpix
  !
  !!! 5. L1Br_getGEOline
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more geolocation data field values from
  ! !       the data block.  This function returns a single line of values
  ! !       for each given keyword (i.e., for fields with only a single value
  ! !       per line of the L1B file (e.g., Time), a single value is returned,
  ! !       but for fields with multiple values per line (e.g., Latitude), all values
  ! !       associated with that line are returned in an array; the caller has
  ! !       the responsibility of allocating sufficient space to hold the returned
  ! !       array).
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.
  ! !
  ! !       Keyword                    Corresponding L1B Geolocation Data Field
  ! !       =======                    ========================================
  ! !       Time_k                     Time [TAI 93]
  ! !       SecondsInDay_k             SecondsInDay
  ! !       SpacecraftLatitude_k       SpacecraftLatitude [satellite_latitude]
  ! !       SpacecraftLongitude_k      SpacecraftLongitude [satellite_longitude]
  ! !       SpacecraftAltitude_k       SpacecraftAltitude [satellite_altitude]
  ! !       OrbitPhase_k               OrbitPhase [satellite_orbit_phase]
  ! !       Latitude_k                 Latitude
  ! !       LongitudeBounds_k          LongitudeBounds[longitude_bounds]
  ! !       LatitudeBounds_k           LatitudeBounds[latitude_bounds]
  ! !       Longitude_k                Longitude
  ! !       SolarZenithAngle_k         SolarZenithAngle
  ! !       SolarAzimuthAngle_k        SolarAzimuthAngle
  ! !       ViewingZenithAngle_k       ViewingZenithAngle
  ! !       ViewingAzimuthAngle_k      ViewingAzimuthAngle
  ! !       TerrainHeight_k            TerrainHeight [surface_altitude]
  ! !       GroundPixelQualityFlags_k  GroundPixelQualityFlags[ground_pixel_quality]
  ! !       TerrainHeightPrec_k        TerrainHeightPrecision [surface_altitude_precision]
  ! !       EarthSunDistance_k ? TODO- Do we need to add earth_sun_dist
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source ((derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !       May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getGEOline(this, iLine, Time_k, SecondsInDay_k, &
      SpacecraftLatitude_k, SpacecraftLongitude_k, &
      SpacecraftAltitude_k, OrbitPhase_k, &
      LatitudeBounds_k, LongitudeBounds_k, &
      Latitude_k, Longitude_k, SolarZenithAngle_k, &
      SolarAzimuthAngle_k, ViewingZenithAngle_k, &
      ViewingAzimuthAngle_k, TerrainHeight_k, &
      GroundPixelQualityFlags_k, TerrainHeightPrec_k, &
      SpacecraftShadowFraction_k, WaterFraction_k, &
      EarthSunDistance_k, LandWaterClassification_k, &
      XTrackQualityFlags_k) RESULT (status)

    ! USE Szoom_Parameter_Module
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER, PARAMETER:: i1 = 1
    INTEGER (KIND = i1), PARAMETER:: global_mode = 8_i1, szoom_mode = 4_i1
    INTEGER (KIND = 4), INTENT(IN):: iLine
    REAL (KIND = 8), OPTIONAL, INTENT(OUT):: Time_k
    REAL (KIND = 4), OPTIONAL, INTENT(OUT) :: &
      SpacecraftLatitude_k, SpacecraftLongitude_k, &
      SpacecraftAltitude_k, OrbitPhase_k, &
      SecondsInDay_k, SpacecraftShadowFraction_k, EarthSunDistance_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:,:), INTENT(OUT) :: &
      LatitudeBounds_k, LongitudeBounds_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT(OUT) :: &
      Latitude_k, Longitude_k, SolarZenithAngle_k, &
      SolarAzimuthAngle_k, ViewingZenithAngle_k, &
      ViewingAzimuthAngle_k
    REAL (KIND = 4), DIMENSION(this%nXtrack) :: &
      tmp_Latitude_k, tmp_Longitude_k, tmp_SolarZenithAngle_k, &
      tmp_SolarAzimuthAngle_k, tmp_ViewingZenithAngle_k, &
      tmp_ViewingAzimuthAngle_k
    INTEGER (KIND = 2), OPTIONAL, DIMENSION(:), INTENT(OUT):: TerrainHeight_k
    INTEGER (KIND = 2), OPTIONAL, DIMENSION(:), INTENT(OUT):: GroundPixelQualityFlags_k
    INTEGER (KIND = 2), OPTIONAL, DIMENSION(:), INTENT(OUT):: TerrainHeightPrec_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT(OUT):: WaterFraction_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT(OUT):: LandWaterClassification_k
    INTEGER (KIND = 2), OPTIONAL, DIMENSION(:), INTENT(OUT):: XTrackQualityFlags_k
    INTEGER (KIND = 2),  DIMENSION(this%nXtrack):: tmp_TerrainHeight_k
    INTEGER (KIND = 2),  DIMENSION(this%nXtrack):: tmp_GroundPixelQualityFlags_k
    INTEGER (KIND = 2),  DIMENSION(this%nXtrack):: tmp_TerrainHeightPrec_k

    INTEGER:: i
    INTEGER:: itmp
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    func_name='L1Br_getGEOline'
    status = check_blk_line(this, iLine, i)
    IF (status /= OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'Size Error','Failed retrieving geolocation data.')
      RETURN
    ENDIF

    ! These first ones have only 1 value per line

    IF(PRESENT(EarthSunDistance_k))        EarthSunDistance_k = & 
          this%earth_sun_dist
    IF(PRESENT(Time_k))                    Time_k = this%Time(i)
    IF(PRESENT(SecondsInDay_k))            SecondsInDay_k = this%SecInDay(i)
    IF(PRESENT(SpacecraftLatitude_k))      SpacecraftLatitude_k = this%ScLat(i)
    IF(PRESENT(SpacecraftLongitude_k))     SpacecraftLongitude_k = this%ScLon(i)
    IF(PRESENT(SpacecraftAltitude_k))      SpacecraftAltitude_k = this%ScAlt(i)
    IF(PRESENT(OrbitPhase_k))              OrbitPhase_k = this%OrbitPhase(i)
    IF(PRESENT(SpacecraftShadowFraction_k))  SpacecraftShadowFraction_k = this%SpacecraftShadowFraction(i)
    ! These next ones have multiple values per line

    IF(PRESENT(Latitude_k)) THEN
      IF(SIZE(Latitude_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input latitude array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      Latitude_k = this%Lat(1:this%nXtrack, i)
      ! Zoom mode

    ENDIF

    IF(PRESENT(Longitude_k)) THEN
      IF(SIZE(Longitude_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input longitude array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      Longitude_k = this%Lon(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(LongitudeBounds_k)) THEN
      IF(SIZE(LongitudeBounds_k, 2) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input longitudebounds array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      LongitudeBounds_k(:,:) = this%LonBounds(:,1:this%nXtrack, i)
    ENDIF

    IF(PRESENT(LatitudeBounds_k)) THEN
      IF(SIZE(LatitudeBounds_k, 2) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input latitudebounds array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      LatitudeBounds_k(:,:) = this%LatBounds(:,1:this%nXtrack, i)
    ENDIF

    IF(PRESENT(SolarZenithAngle_k)) THEN
      IF(SIZE(SolarZenithAngle_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input solarZenith array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      SolarZenithAngle_k = this%SolZenAng(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(SolarAzimuthAngle_k)) THEN
      IF(SIZE(SolarAzimuthAngle_k) <  this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input solarAzimuth array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      SolarAzimuthAngle_k = this%SolAziAng(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(ViewingZenithAngle_k)) THEN
      IF(SIZE(ViewingZenithAngle_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input viewZenith array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      ViewingZenithAngle_k  = this%ViewZenAng(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(ViewingAzimuthAngle_k)) THEN
      IF(SIZE(ViewingAzimuthAngle_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input viewAzimuth array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      ViewingAzimuthAngle_k = this%ViewAziAng(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(TerrainHeight_k)) THEN
      IF(SIZE(TerrainHeight_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input height array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      TerrainHeight_k  = this%TerrainHeight(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(WaterFraction_k)) THEN
      IF(SIZE(WaterFraction_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input height array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WaterFraction_k  = this%WaterFraction(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(LandWaterClassification_k)) THEN
      IF(SIZE(LandWaterClassification_k) < this%nXtrack) THEN
        call PGE_WARN(func_name, 'Size Error',&
          "input land water classification too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      LandWaterClassification_k  = this%LandWater(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(GroundPixelQualityFlags_k)) THEN
      IF(SIZE(GroundPixelQualityFlags_k) < this%nXtrack ) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input geolocation flag array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      GroundPixelQualityFlags_k = this%GPQFlag(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(XTrackQualityFlags_k)) THEN
      IF(SIZE(XTrackQualityFlags_k) < this%nXtrack ) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input geolocation flag array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      XTrackQualityFlags_k = this%XTQFlag(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    IF(PRESENT(TerrainHeightPrec_k)) THEN
      IF(SIZE(TerrainHeightPrec_k) < this%nXtrack ) THEN
        call PGE_WARN(func_name, 'Size Error', &
          "input geolocation flag array too small")
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      TerrainHeightPrec_k = this%TerrainHeightPrec(1:this%nXtrack, i)
      ! Zoom mode
    ENDIF

    RETURN
  END FUNCTION L1Br_getGEOline
  !
  !!! 6. L1Br_getDATA
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more data field values from
  ! !       the data block.  This function returns a single line of values
  ! !       for each given keyword.  All of the data fields accessed by this
  ! !       function have a single value per line, so only a single value is
  ! !       ever passed back for any given keyword.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.
  ! !
  ! !       Keyword                             Corresponding L1B Data Field
  ! !       =======                             ============================
  ! !       MeasurementClass_k                  MeasurementClass
  ! !       InstrumentConfigurationId_k         InstrumentConfigurationId
  ! !       MeasurementQualityFlags_k           MeasurementQualityFlags
  ! !       ExposureType_k                      ExposureType
  ! !       MasterClockPeriod_k                 MasterClockPeriod
  ! !       CalibrationSettings_k               CalibrationSettings
  ! !       ExposureTime_k                      ExposureTime
  ! !       ReadoutTime_k                       ReadoutTime
  ! !       GainSwitchingColumn1_k              GainSwitchingColumn1
  ! !       GainSwitchingColumn2_k              GainSwitchingColumn2
  ! !       GainSwitchingColumn3_k              GainSwitchingColumn3
  ! !       GainCode1_k                         GainCode1
  ! !       GainCode2_k                         GainCode2
  ! !       GainCode3_k                         GainCode3
  ! !       GainCode4_k                         GainCode4
  ! !       DSGainCode_k                        DSGainCode
  ! !       LowerStrayLightAreaBinningFactor_k  LowerStrayLightAreaBinningFactor
  ! !       UpperStrayLightAreaBinningFactor_k  UpperStrayLightAreaBinningFactor
  ! !       LowerDarkAreaBinningFactor_k        LowerDarkAreaBinningFactor
  ! !       UpperDarkAreaBinningFactor_k        UpperDarkAreaBinningFactor
  ! !       SkipRows1_k                         SkipRows1
  ! !       SkipRows2_k                         SkipRows2
  ! !       SkipRows3_k                         SkipRows3
  ! !       SkipRows4_k                         SkipRows4
  ! !       DetectorTemperature_k               DetectorTemperature
  ! !       OpticalBenchTemperature_k           OpticalBenchTemperature
  ! !       ImageBinningFactor_k                ImageBinningFactor
  ! !       BinnedImageRows_k                   BinnedImageRows
  ! !       StopColumn_k                        StopColumn
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source
  !       May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
        FUNCTION L1Br_getDATA(this, iLine, MeasurementClass_k, InstrumentConfigurationId_k, &
  	                    MeasurementQualityFlags_k, &
                        ExposureType_k, MasterClockPeriod_k, &
  			    CalibrationSettings_k, ExposureTime_k, ReadoutTime_k, &
  			    GainSwitchingColumn1_k, GainSwitchingColumn2_k, &
  			    GainSwitchingColumn3_k, GainCode1_k, GainCode2_k, GainCode3_k, &
  			    GainCode4_k, DSGainCode_k, LSLABF_k, USLABF_k, LDABF_k, &
  			    UDABF_k, SkipRows1_k, SkipRows2_k, &
  			    SkipRows3_k, SkipRows4_k, DetectorTemperature_k, &
  			    OpticalBenchTemperature_k, ImageBinningFactor_k, &

                ! adding SmallPIxel Colulmn_k - MB 10/22/23
  			    BinnedImageRows_k, StopColumn_k, SmallPixelColumn_k &
            ) RESULT (status)
  
          ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
          TYPE (L1B_block_type), INTENT(INOUT):: this
          INTEGER (KIND = 4), INTENT(IN):: iLine
          INTEGER (KIND = 1), OPTIONAL, INTENT(OUT):: MeasurementClass_k, &
  			      InstrumentConfigurationId_k, ExposureType_k, GainCode1_k, &
  			      GainCode2_k, GainCode3_k, GainCode4_k, DSGainCode_k, &
  			      LSLABF_k, USLABF_k, LDABF_k, UDABF_k, ImageBinningFactor_k
          INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: MeasurementQualityFlags_k, &
                                CalibrationSettings_k, GainSwitchingColumn1_k, &
  			      GainSwitchingColumn2_k, GainSwitchingColumn3_k, &
                                SkipRows1_k, SkipRows2_k, SkipRows3_k, SkipRows4_k, &

                                ! add small pixel column k
                                BinnedImageRows_k, StopColumn_k, SmallPixelColumn_k
          REAL (KIND = 4), OPTIONAL, INTENT(OUT):: MasterClockPeriod_k, ExposureTime_k, &
  	                      ReadoutTime_k, DetectorTemperature_k, OpticalBenchTemperature_k
          INTEGER:: i
          INTEGER (KIND = 4):: status
          CHARACTER (LEN = 256):: func_name 
  
          func_name='L1Br_getDATA'
          status = check_blk_line(this, iLine, i)
          IF(status .NE. OMI_S_SUCCESS) THEN
            call PGE_WARN(func_name, 'Size Error','Failed retrievingL1B data.')
             ! ierr = OMI_SMF_setmsg(OMI_E_GENERAL, "Failed retrieving L1B data.", &
             !                       "L1Br_getDATA", one)
             RETURN
          ENDIF
  
          IF(PRESENT(MeasurementClass_k)) MeasurementClass_k = this%MeasClass(i)
          IF(PRESENT(InstrumentConfigurationId_k)) InstrumentConfigurationId_k = this%Config(i)
          IF(PRESENT(MeasurementQualityFlags_k)) MeasurementQualityFlags_k = this%MQFlag(i)
          IF(PRESENT(ExposureType_k)) ExposureType_k = this%ExposureType(i)
          IF(PRESENT(MasterClockPeriod_k)) MasterClockPeriod_k = this%MasterClkPer(i)
          IF(PRESENT(CalibrationSettings_k)) CalibrationSettings_k = this%CalSet(i)
          IF(PRESENT(ExposureTime_k)) ExposureTime_k = this%ExpTime(i)
          IF(PRESENT(ReadoutTime_k)) ReadoutTime_k = this%ReadTime(i)
          IF(PRESENT(GainSwitchingColumn1_k)) GainSwitchingColumn1_k = this%GSC1(i)
          IF(PRESENT(GainSwitchingColumn2_k)) GainSwitchingColumn2_k = this%GSC2(i)
          IF(PRESENT(GainSwitchingColumn3_k)) GainSwitchingColumn3_k = this%GSC3(i)
          IF(PRESENT(GainCode1_k)) GainCode1_k = this%GC1(i)
          IF(PRESENT(GainCode2_k)) GainCode2_k = this%GC2(i)
          IF(PRESENT(GainCode3_k)) GainCode3_k = this%GC3(i)
          IF(PRESENT(GainCode4_k)) GainCode4_k = this%GC4(i)
          IF(PRESENT(DSGainCode_k)) DSGainCode_k = this%DSGC(i)
          IF(PRESENT(LSLABF_k)) LSLABF_k = this%LSLABF(i)
          IF(PRESENT(USLABF_k)) USLABF_k = this%USLABF(i)
          IF(PRESENT(LDABF_k)) LDABF_k = this%LDABF(i)
          IF(PRESENT(UDABF_k)) UDABF_k = this%UDABF(i)
          IF(PRESENT(SkipRows1_k)) SkipRows1_k = this%SR1(i)
          IF(PRESENT(SkipRows2_k)) SkipRows2_k = this%SR2(i)
          IF(PRESENT(SkipRows3_k)) SkipRows3_k = this%SR3(i)
          IF(PRESENT(SkipRows4_k)) SkipRows4_k = this%SR4(i)
          IF(PRESENT(DetectorTemperature_k)) DetectorTemperature_k = this%DetcTemp(i)
          IF(PRESENT(OpticalBenchTemperature_k)) OpticalBenchTemperature_k = this%OptBenchTemp(i)
          IF(PRESENT(ImageBinningFactor_k)) ImageBinningFactor_k = this%ImBinFact(i)
          IF(PRESENT(BinnedImageRows_k)) BinnedImageRows_k = this%BinImgRows(i)
          IF(PRESENT(StopColumn_k)) StopColumn_k = this%StopColumn(i)

          ! adding small Pixel Column_k - MB 10/22/23
          IF(PRESENT(SmallPixelColumn_k)) SmallPixelColumn_k = this%SmPixCol(i)

          RETURN
        END FUNCTION L1Br_getDATA
  !
  !!! Private function: calc_wl_pix
  ! !
  ! ! Functionality:
  ! !
  ! !   This function calculates the wavelengths values for a specific pixel
  ! !   and returns those values, plus the limits of the specified wavelength
  ! !   range.
  ! !
  ! ! Calling Arguments:
  ! !
  ! ! Inputs:
  ! !
  ! !    j         Index into L1B structure, line number
  ! !    i         Index into L1B structure, Xtrack position
  ! !    this      L1B block
  ! !    minwl     minimum wavelength requested
  ! !    maxwl     maximum wavelength requested
  ! !
  ! ! Outputs:
  ! !
  ! !    wl_local  wavelength values calculated from L1B coefficients
  ! !    il        index of lower bound of wavelength range (in wl_local)
  ! !    ih        index of upper bound of wavelength range (in wl_local)
  ! !    Nwl_l     number of wavelengths in range
  ! !
  ! !    status    the return PGS_SMF status value
  ! !
  ! ! Change History:
  ! !
  ! !    Date            Author          Modifications
  ! !    ====            ======          =============
  ! !    January 2005    Jeremy Warner   Original Source
  !       May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
  FUNCTION calc_wl_pix(j, i, this, minwl, maxwl, wl_local, il, ih, &
      Nwl_l) RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: i, j
    REAL (KIND = 4), INTENT(INOUT):: minwl, maxwl
    INTEGER (KIND = 4), INTENT(OUT):: il, ih, Nwl_l
    REAL (KIND = 4), DIMENSION(:), INTENT(OUT):: wl_local

    INTEGER (KIND = 4):: status
    INTEGER (KIND = 4):: fflag, k, q, i_foo(1)
    INTEGER (KIND = 4):: fac
    CHARACTER (LEN = 256):: func_name

    ! First calculate the wavelength values based on the wavelength
    ! coefficients from the L1B file.  Set fflag if we find any
    ! fill values.

    status = OMI_S_SUCCESS
    func_name = 'calc_wl_pix'
    fflag = 0
    DO k = 1, this%nWavel
      wl_local(k) = 1.0
      DO q = 1, this%nWavelCoef
        IF(this%WavelenCoef(q,i,j) .le. rfill) wl_local(k) = rfill
      ENDDO
      fac = k-1-this%WavelenRefCol
      IF (wl_local(k) > 0.0) THEN
        wl_local(k) = fac*this%WavelenCoef(this%nWavelCoef,i,j)
        DO q = this%nWavelCoef -1, 2, -1
          wl_local(k) = fac*(wl_local(k) + this%WavelenCoef(q,i,j))
        ENDDO
        wl_local(k) = wl_local(k) + this%WavelenCoef(1,i,j)
      ELSE
        fflag = 1
      ENDIF
    ENDDO

    ! Now find the limits of the wavelength range.

    IF (fflag .EQ. 0) THEN  ! We found no fill values, so do it the fast way.

      ! Check to see if minwl and maxwl are consistent (i.e., minwl < maxwl)

      IF(minwl .gt. 0.0 .AND. maxwl .gt. 0.0) THEN
        IF((minwl-maxwl) > 0.0) THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'min_wl = ',minwl, 'maxwl = ', maxwl
          call PGE_WARN(func_name, &
            "input Wlmin_k greater than Wlmax_k", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
      ENDIF

      ! Check to see if minwl is compatable with the wavelength range
      ! If minwl was not given, set the lower bound to the first element.

      IF(minwl .gt. 0.0) THEN
        IF(minwl > MAXVAL(wl_local))THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'min_wl = ',minwl, &
            'max wl_local = ',  maxval(wl_local)
          call PGE_WARN(func_name, &
            "input Wlmin_k out of bound", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        ! Set the lower bound based on wl_local and minwl

        IF(minwl < MINVAL(wl_local)) THEN
          il = 1
        ELSE
          i_foo = MAXLOC(wl_local, MASK = wl_local-minwl <= 0.0)
          il    = i_foo(1)
        ENDIF
      ELSE
        il = 1
      ENDIF

      ! Check to see if maxwl is compatable with the wavelength range
      ! If maxwl was not given, set the upper bound to the last element.

      IF(maxwl .gt. 0.0) THEN
        IF(maxwl < MINVAL(wl_local)) THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'maxwl = ',maxwl, 'min wl_local', &
            MINVAL(wl_local)
          call PGE_WARN(func_name, &
            "input Wlmax_k out of bound", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        ! Set the upper bound based on wl_local and maxwl

        IF(maxwl > MAXVAL(wl_local)) THEN
          ih = this%nWavel
        ELSE
          i_foo = MINLOC(wl_local, MASK = wl_local-maxwl >= 0.0)
          ih    = i_foo(1)
        ENDIF
      ELSE
        ih = this%nWavel
      ENDIF

    ELSE  ! => we had a fill value

      ! Initialize il and ih

      ih = 0
      il = 0

      ! Loop through all wavelengths, and defind bounds based on minwl and maxwl

      if (minwl < 0.0) minwl = 0.0
      if (maxwl < 0.0) maxwl = 1000.0
      DO k = 1, this%nWavel
        IF (wl_local(k) >= minwl .AND. wl_local(k) <= maxwl) THEN
          IF (il .EQ. 0) il = k
          IF (ih .EQ. 0) ih = k
          IF (ih .LT. k) ih = k
        ENDIF
      ENDDO
      IF (il > 1) il = il-1
      IF (ih < this%nWavel .AND. ih .NE. 0) ih = ih+1
    ENDIF

    ! Set the number of wavelengths in the span to Nwl_l

    IF (il .EQ. 0 .AND. ih .EQ. 0) THEN
      Nwl_l = 0
    ELSE
      Nwl_l = ih-il+1
    ENDIF

    RETURN
  END FUNCTION calc_wl_pix
  !
  !!! Private function: calc_wl_line
  ! !
  ! ! Functionality:
  ! !
  ! !   This function calculates the wavelengths values for a specific line
  ! !   and returns those values, plus the limits of the specified wavelength
  ! !   range.
  ! !
  ! ! Calling Arguments:
  ! !
  ! ! Inputs:
  ! !
  ! !    i         Index into L1B structure, line number
  ! !    this      L1B block
  ! !    minwl     minimum wavelength requested
  ! !    maxwl     maximum wavelength requested
  ! !
  ! ! Outputs:
  ! !
  ! !    wl_local  wavelength values calculated from L1B coefficients
  ! !    il        index of lower bound of wavelength range (in wl_local)
  ! !    ih        index of upper bound of wavelength range (in wl_local)
  ! !    Nwl_l     number of wavelengths in range
  ! !
  ! !    status    the return PGS_SMF status value
  ! !
  ! ! Change History:
  ! !
  ! !    Date            Author          Modifications
  ! !    ====            ======          =============
  ! !    January 2005    Jeremy Warner   Original Source
  !       May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
  FUNCTION calc_wl_line(j, this, minwl, maxwl, wl_local, il, ih, &
      Nwl_l) RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: j
    REAL (KIND = 4), INTENT(INOUT):: minwl, maxwl
    INTEGER (KIND = 4), INTENT(OUT):: il, ih, Nwl_l
    REAL (KIND = 4), DIMENSION(:,:), INTENT(OUT):: wl_local

    INTEGER (KIND = 4):: status
    INTEGER (KIND = 4):: fflag, i, k, q, i_foo(1)
    INTEGER (KIND = 4):: fac
    CHARACTER (LEN = 256):: func_name


    ! First calculate the wavelength values based on the wavelength
    ! coefficients from the L1B file.  Set fflag if we find any
    ! fill values.

    status = OMI_S_SUCCESS
    func_name = 'calc_wl_line'
    fflag = 0
      DO i = 1, this%nXtrack
        DO k = 1, this%nWavel
           wl_local(k,i) = 1.0
           DO q = 1, this%nWavelCoef
              IF(this%WavelenCoef(q,i,j) .LE. rfill) wl_local(k,i) = rfill
           ENDDO
!NC4 - WavelenRefCol is a scalar in col4 L1B, instead of array(col 3)
         fac = k-1-this%WavelenRefCol
         IF (wl_local(k,i) > 0.0) THEN
            wl_local(k,i) = fac*this%WavelenCoef(this%nWavelCoef,i,j)
            DO q = this%nWavelCoef -1 , 2, -1
               wl_local(k,i) = fac*(wl_local(k,i) + this%WavelenCoef(q,i,j))
            ENDDO
            wl_local(k,i) = wl_local(k,i) + this%WavelenCoef(1,i,j)
           ELSE
              fflag = 1
           ENDIF
        ENDDO
      ENDDO

!      ! calculate from wavelength shift - MB 2/2023
!      fflag = 0
!      DO i = 1, this%nXtrack
!        DO k = 1, this%nWavel
!           wl_local(k,i) = 1.0
!            IF (this%WavelenNom(k, j).le. rfill) wl_local(k,i) = rfill
!            IF (this%WavelenShift(k, i).le. rfill) wl_local(k,i) = rfill
!            IF (wl_local(k,i) > 0.0) THEN
!              wl_local(k,i) = this%WavelenNom(k, i) + this%WavelenShift(i, j)
!            ELSE
!              fflag = 1
!           ENDIF
!        ENDDO
!      ENDDO

    ! Now find the limits of the wavelength range.

    IF (fflag .EQ. 0) THEN  ! We found no fill values, so do it the fast way.

      ! Check to see if minwl and maxwl are consistent (i.e., minwl < maxwl)

      IF(minwl .gt. 0.0 .AND. maxwl .gt. 0.0) THEN
        IF((minwl-maxwl) > 0.0) THEN
          write(msg, '(A, F9.3, 3X, A, F9.3)')'min_wl = ',minwl, 'maxwl = ', maxwl
          call PGE_WARN(func_name, &
            "input Wlmin_k greater than Wlmax_k", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
      ENDIF

      ! Check to see if minwl is compatable with the wavelength range
      ! If minwl was not given, set the lower bound to the first element.

      IF(minwl .gt. 0.0) THEN
        IF(minwl > MAXVAL(wl_local))THEN
          write(msg, '(A, F9.3, 3X, A, F9.3)')'min_wl = ',minwl, &
            'max wl_local = ',  maxval(wl_local)
          call PGE_WARN(func_name, &
            "input Wlmin_k out of bound", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        !     ! Set the lower bound based on wl_local and minwl

        IF(minwl < MINVAL(wl_local)) THEN
          il = 1
        ELSE
          il = 0
          DO i = 1, this%nXtrack
            DO k = 1, this%nWavel
              if (wl_local(k, i) > minwl) THEN
                if (il .eq. 0 .or. il .gt. k) il = k
                EXIT
              ENDIF
            ENDDO
          ENDDO
          IF (il > 1) THEN
            il = il-1
          ELSE
            il = 1
          ENDIF
        ENDIF
      ELSE
        il = 1
      ENDIF

      ! Check to see if maxwl is compatable with the wavelength range
      ! If maxwl was not given, set the upper bound to the last element.

      IF(maxwl .gt. 0.0) THEN
        IF(maxwl < MINVAL(wl_local)) THEN
          write(msg, '(A, F9.3, 3X, A, F9.3)')'maxwl = ',maxwl, 'min wl_local', &
            MINVAL(wl_local)
          call PGE_WARN(func_name, &
            "input Wlmax_k out of bound", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        ! Set the upper bound based on wl_local and maxwl

        IF(maxwl > MAXVAL(wl_local)) THEN
          ih = this%nWavel
        ELSE
          ih = this%nWavel+1
          DO i = 1, this%nXtrack
            DO k = 1, this%nWavel
              if (wl_local(k, i) > maxwl) THEN
                if (ih .eq. this%nWavel+1 .or. ih .lt. k) ih = k
                EXIT
              ENDIF
            ENDDO
          ENDDO
          IF (ih < this%nWavel) THEN
            ih = ih+1
          ELSE
            ih = this%nWavel
          ENDIF
        ENDIF
      ELSE
        ih = this%nWavel
      ENDIF

    ELSE  ! => we had a fill value

      ! Initialize il and ih

      ih = 0
      il = 0

      ! Loop through all wavelengths, and defind bounds based on minwl and maxwl

      if (minwl < 0.0) minwl = 0.0
      if (maxwl < 0.0) maxwl = 1000.0
      DO k = 1, this%nWavel
        DO i = 1, this%nXtrack
          IF (wl_local(k, i) >= minwl .AND. wl_local(k, i) <= maxwl) THEN
            IF (il .EQ. 0) il = k
            IF (ih .EQ. 0) ih = k
            IF (ih .LT. k) ih = k
          ENDIF
        ENDDO
      ENDDO
      IF (il .EQ. 0 .AND. ih .EQ. 0) THEN
      ELSE
        IF (il > 1) il = il-1
        IF (ih < this%nWavel) ih = ih+1
      ENDIF
    ENDIF

    ! Set the number of wavelengths in the span to Nwl_l

    IF (il .EQ. 0 .AND. ih .EQ. 0) THEN
      Nwl_l = 0
    ELSE
      Nwl_l = ih-il+1
    ENDIF
    RETURN
  END FUNCTION calc_wl_line
  !
  !!! 7. L1Br_getSIGpix
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more radiance data field values from
  ! !       the data block.  This function returns values for each given output
  ! !       keyword for the each wavelength within the input wavelength range.
  ! !       If no wavelength range is input, values are returned for all
  ! !       wavelengths.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !       iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !             range from 0 to (nXtrack-1) inclusive.
  ! !       Wlmin_k: input wavelength value for the band beginning (in nm) (Optional)
  ! !       Wlmax_k: input wavelength value for the band end (in nm) (Optional)
  ! !
  ! !       Note that Wlmin_k < Wlmax_k.
  ! !       If Wlmin_k is omitted, all wavelengths < Wlmax_k are returned.
  ! !       If Wlmax_k is omitted, all wavelengths > Wlmin_k are returned.
  ! !       If Wlmin_k and Wlmax_k are omitted, all wavelengths are returned.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.  The first set of keywords is for
  ! !       combined data, whereas the second set is for raw data; the two sets are
  ! !       not mutually exclusive, i.e., raw and combined data can be extracted
  ! !       in the same function call.
  ! !
  ! !       Keyword                    Corresponding L1B Data
  ! !       =======                    ======================
  ! !       Signal_k                   Radiance or Irradiance [radiance]
  ! !       SignalError_k              radiance_error
  ! !       SignalNoise_k              radiance_noise
  ! !       SpectralQuality_k          [spectral_channel_quality]
  ! !       QualityLevel_k             [quality_level]
  ! !       Wavelength_k               Wavelengths [nominal_wavelength]
  ! !       WavelengthShift_k          wavelength_shift
  ! !       Nwl_k                      None-just the number of wavelengths returned
  ! !
  ! !       If the user wants the raw L1B data, rather than combined data, that can be
  ! !       retrieved using the following keywords.
  ! !
  ! !       Keyword                     Corresponding L1B Data Field
  ! !       =======                     ============================
  ! !       WavelengthCoefficient_k     WavelengthCoefficient
  ! !       WavelengthCoefficientPrec_k WavelengthCoefficientPrecision
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !        May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getSIGpix(this, iLine, iPix, Wlmin_k, Wlmax_k, Signal_k, &
      SignalError_k, SpectralQuality_k, Wavelength_k, Nwl_k, &
      WavelengthShift_k,  SignalNoise_k, &
      WavelengthCoefficient_k, WavelengthCoefficientPrec_k, &
      WavelengthReferenceColumn_k, &
      QualityLevel_k, SmallPixelSignal_k) RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes

    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4), INTENT(IN):: iPix
    REAL (KIND = 4), OPTIONAL, INTENT(IN):: Wlmin_k, Wlmax_k    ! wavelength range
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: Nwl_k, WavelengthReferenceColumn_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT(OUT):: Signal_k, &
      SignalError_k, Wavelength_k, &
      WavelengthCoefficient_k, &
      WavelengthCoefficientPrec_k, &
      SignalNoise_k, SmallPixelSignal_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT(OUT)::  WavelengthShift_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT(OUT) :: &
      SpectralQuality_k, QualityLevel_k
    REAL (KIND = 4), DIMENSION(1:this%nWavel):: wl_local
    INTEGER (KIND = 4):: Nwl_l
    INTEGER:: i, j, k, q, l
    INTEGER (KIND = 4):: il, ih, i_foo(1)
    INTEGER (KIND = 4):: status
    REAL (KIND = 4):: minWl, maxWl
    CHARACTER (LEN = 256):: func_name

    ! Check requested indecies for consistancy

    func_name = 'L1Br_getSIGpix'
    status = check_blk_pix(this, iLine, iPix, j, i)
    IF(status .NE. OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'I/O Error','Failed retrieving L1B signal data.')
      RETURN
    ENDIF

    ! Set min and max wavelengths

    IF(PRESENT(Wlmin_k)) THEN
      minWl = Wlmin_k
    ELSE
      minWl = -1.0
    ENDIF

    IF(PRESENT(Wlmax_k)) THEN
      maxWl = Wlmax_k
    ELSE
      maxWl = -1.0
    ENDIF

    ! Calculate wavelengths from L1B data

    status = calc_wl_pix(j, i, this, minWl, maxWl, wl_local, il, ih, Nwl_l)
    IF(status .NE. OMI_S_SUCCESS) THEN
      write(msg, "(A, F9.3, A,3X, F9.3, 3(A, I4))") "minWl = ",minWl, "maxWl = ",maxWl, &
        "il = ", il, "ih = ",ih, "Nwl_l=",Nwl_l
      call PGE_WARN(func_name, "Failed calculating L1B wavelengths", msg)
      RETURN
    ENDIF

    ! ! Retrieve requested data; first do data for possible multiple wavelengths

    IF (Nwl_l > 0) THEN
      IF(PRESENT(Signal_k)) THEN
        IF(SIZE(Signal_k) < Nwl_l) THEN
          write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'size of Signal_k',SIZE(Signal_k)
          call PGE_WARN(func_name, "input Signal_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Signal_k(1:Nwl_l) = this%Radiance(il:ih, i, j)
      ENDIF

      IF(PRESENT(SmallPixelSignal_k)) THEN
        IF(SIZE(SmallPixelSignal_k) < this%nCoad) THEN
          write(msg, '(A, I3, A, *(I4))')'nCoad : ',this%ncoad, &
            'size of SmallPixelSignal_k',SIZE(SmallPixelSignal_k)
          call PGE_WARN(func_name, "input SmallPixelSignal_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SmallPixelSignal_k(:) = this%SmPixRad(:, i, j)
      ENDIF

      IF(PRESENT(SignalError_k)) THEN
        IF(SIZE(SignalError_k) < Nwl_l) THEN
          write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'size of Signal_k',SIZE(Signal_k)
          call PGE_WARN(func_name, "input SignalError_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SignalError_k(1:Nwl_l) = &
          this%RadianceError(il:ih, i, j)
      ENDIF


      IF(PRESENT(SpectralQuality_k)) THEN
        IF(SIZE(SpectralQuality_k) < Nwl_l) THEN
          write(msg, '(A, I3, 3X, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'size of SpectralQuality_k',SIZE(SpectralQuality_k)
          call PGE_WARN(func_name, &
            "input SpectralQuality_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SpectralQuality_k(1:Nwl_l) = this%SpecQual(il:ih, i, j)
      ENDIF

      IF(PRESENT(QualityLevel_k)) THEN
        IF(SIZE(QualityLevel_k) < Nwl_l) THEN
          write(msg, '(A, I3, 3X, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'size of QualityLevel_k',SIZE(QualityLevel_k)
          call PGE_WARN(func_name, &
            "input QualityLevel_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        QualityLevel_k(1:Nwl_l) = this%QualLevel(il:ih, i, j)
      ENDIF

      IF(PRESENT(Wavelength_k)) THEN
        IF(SIZE(Wavelength_k) < Nwl_l) THEN
          write(msg, "(A, I4, A, *(I4))")'Nwl_l = ',Nwl_l, 'size of Wavelength_k',&
            size(Wavelength_k)
          call PGE_WARN(func_name, &
            "input Wavelength_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Wavelength_k(1:Nwl_l) = wl_local(il:ih)
      ENDIF

      ! Here is the raw data extraction

    ENDIF  ! Nwl_l > 0

    IF(PRESENT(Nwl_k)) Nwl_k = Nwl_l

    ! The rest of the outputs do not depend on the value of Nwl_l

    IF(PRESENT(WavelengthCoefficient_k)) THEN
      IF(SIZE(WavelengthCoefficient_k) < this%nWavelCoef) THEN
        write(msg, '(A, I4, A, *(I4))')'NWavelCoef = ',this%nWavelCoef, &
          'size of WavelengthCoefficient_k = ', &
          size(WavelengthCoefficient_k)
        call PGE_WARN(func_name, &
          "input WavelengthCoefficient_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WavelengthCoefficient_k(1:this%nWavelCoef) = this%WavelenCoef(1:this%nWavelCoef, i, j)
    ENDIF

    IF(PRESENT(WavelengthCoefficientPrec_k)) THEN
      IF(SIZE(WavelengthCoefficientPrec_k) < this%nWavelCoef) THEN
        write(msg, '(A, I4, 3X, A, *(I4))') &
          ' ; nWavelCoef = ',this%nWavelCoef, &
          'Size ofWavelengthCoefficientPrec_k',&
          SIZE(WavelengthCoefficientPrec_k)
        call PGE_WARN(func_name, &
          "input WavelengthCoefficientPrec_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WavelengthCoefficientPrec_k(1:this%nWavelCoef) = &
        this%WavelenPrec(1:this%nWavelCoef, i, j)
    ENDIF

    IF(PRESENT(WavelengthReferenceColumn_k)) &
        WavelengthReferenceColumn_k = this%WavelenRefCol
    ! Here is the Small Pixel data


    RETURN
  END FUNCTION L1Br_getSIGpix
  !
  !!! 8. L1Br_getSIGline
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more radiance data field values from
  ! !       the data block.  This function returns values for each given output
  ! !       keyword for the each wavelength within the input wavelength range.
  ! !       If no wavelength range is input, values are returned for all
  ! !       wavelengths.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !       iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !             range from 0 to (nXtrack-1) inclusive.
  ! !       Wlmin_k: input wavelength value for the band beginning (in nm) (Optional)
  ! !       Wlmax_k: input wavelength value for the band end (in nm) (Optional)
  ! !
  ! !       Note that Wlmin_k < Wlmax_k.
  ! !       If Wlmin_k is omitted, all wavelengths < Wlmax_k are returned.
  ! !       If Wlmax_k is omitted, all wavelengths > Wlmin_k are returned.
  ! !       If Wlmin_k and Wlmax_k are omitted, all wavelengths are returned.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.  The first set of keywords is for
  ! !       combined data, whereas the second set is for raw data; the two sets are
  ! !       not mutually exclusive, i.e., raw and combined data can be extracted
  ! !       in the same function call.
  ! !
  ! !       Keyword                    Corresponding L1B Data
  ! !       =======                    ======================
  ! !       Signal_k                   Radiance or Irradiance [radiance]
  ! !       SignalError_k              radiance_error
  ! !       SignalNoise_k              radiance_noise
  ! !       SpectralQuality_k          [spectral_channel_quality]
  ! !       QualityLevel_k             [quality_level]
  ! !       Wavelength_k               Wavelengths [nominal_wavelength]
  ! !       WavelengthShift_k          wavelength_shift
  ! !       Nwl_k                      None-just the number of wavelengths returned
  ! !
  ! !       If the user wants the raw L1B data, rather than combined data, that can be
  ! !       retrieved using the following keywords.
  ! !
  ! !       Keyword                     Corresponding L1B Data Field
  ! !       =======                     ============================
  ! !       WavelengthCoefficient_k     WavelengthCoefficient
  ! !       WavelengthCoefficientPrec_k WavelengthCoefficientPrecision
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !        May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getSIGline(this, aiLine, Wlmin_k, Wlmax_k, Signal_k, &
      SignalError_k, SpectralQuality_k, Wavelength_k, Nwl_k, &
      WavelengthShift_k,  SignalNoise_k, &
      WavelengthCoefficient_k, WavelengthCoefficientPrec_k, &
      WavelengthReferenceColumn_k, &
      QualityLevel_k, SmallPixelSignal_k) RESULT (status)
    ! USE Szoom_Parameter_Module
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes

    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER, PARAMETER:: i1 = 1
    INTEGER (KIND = i1), PARAMETER:: global_mode = 8_i1, szoom_mode = 4_i1
    INTEGER (KIND = 4), INTENT(IN):: aiLine
    REAL (KIND = 4), OPTIONAL, INTENT(IN):: Wlmin_k, Wlmax_k    ! wavelength range
    INTEGER (KIND = 4), OPTIONAL, INTENT(OUT):: Nwl_k, & 
      WavelengthReferenceColumn_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:,:), INTENT(OUT):: Signal_k, &
      SignalError_k, Wavelength_k, &
      WavelengthCoefficient_k, &
      WavelengthCoefficientPrec_k, &
      SignalNoise_k, SmallPixelSignal_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT(OUT)::  WavelengthShift_k
    ! SmallPixelSignal_k, SmallPixelWavelength_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:,:), INTENT(OUT) :: &
      SpectralQuality_k, QualityLevel_k
    ! INTEGER (KIND = 2), OPTIONAL, INTENT(OUT):: WavelengthReferenceColumn_k

    REAL (KIND = 4),  DIMENSION(1:this%nWavel, 1:this%nXtrack):: tmp_Signal_k, &
      tmp_SignalError_k, tmp_Wavelength_k
    REAL (KIND = 4),  DIMENSION(1:this%nWavelCoef, 1:this%nXtrack) :: &
      tmp_WavelengthCoefficient_k, &
      tmp_WavelengthCoefficientPrec_k
    REAL (KIND = 4), ALLOCATABLE:: tmp_SmallPixelSignal_k(:,:), tmp_SmallPixelWavelength_k(:,:)
    INTEGER (KIND = 2),  DIMENSION(1:this%nWavel, 1:this%nXtrack) :: &
      tmp_Mantissa_k, tmp_Precision_k
    INTEGER (KIND = 1),  DIMENSION(1:this%nWavel, 1:this%nXtrack) :: &
      tmp_SpectralQuality_k, tmp_QualityLevel_k
    INTEGER (KIND = 1), DIMENSION(1:this%nWavel, 1:this%nXtrack):: tmp_Exponent_k

    REAL (KIND = 4), DIMENSION(1:this%nWavel, 1:this%nXtrack):: wl_local
    INTEGER:: i, j, k, q, l, iw
    INTEGER (KIND = 4):: Nwl_l
    INTEGER (KIND = 4):: il, ih
    INTEGER (KIND = 4):: status
    REAL (KIND = 4):: minWl, maxWl
    CHARACTER (LEN = 256):: func_name

    func_name='L1Br_getSIGline'
    status = check_blk_line(this, aiLine, j)
    IF(status .NE. OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'I/O Error','Failed retrieving L1B signal data.')
      RETURN
    ENDIF

    ! NetCDF4-Change ailine+1 to ailine-Using FORTRAN indexing(1 based, instead
    !       of 0 based(C)
    ! NetCDF4-Use nominal wavelength only
    ! wl_local = this%wavelen

    ! Set min and max wavelengths

    IF(PRESENT(Wlmin_k)) THEN
      minWl = Wlmin_k
    ELSE
      minWl = -1.0
    ENDIF

    IF(PRESENT(Wlmax_k)) THEN
      maxWl = Wlmax_k
    ELSE
      maxWl = -1.0
    ENDIF

    ! Calculate wavelengths from L1B data

    ! NC4 Reinstating wavelength calculation for netCDF4 format
    status = calc_wl_line(j, this, minWl, maxWl, wl_local, il, ih, Nwl_l)
    IF(status .NE. OMI_S_SUCCESS) THEN
      write(msg, "(A, F9.3, A,3X, F9.3, 3(A, I4, 2x))")" minWl = ", minWl, " maxWl = ",maxWl, &
        " il = ", il, " ih = ",ih, " Nwl_l=",Nwl_l
      call PGE_WARN(func_name, "Failed calculating L1B wavelengths", msg)
      RETURN
    ENDIF

    IF (Nwl_l > 0) THEN
      IF(PRESENT(Signal_k)) THEN
        IF(SIZE(Signal_k, 1) .LT. Nwl_l  .OR. &
          SIZE(Signal_k, 2) .LT. this%nXtrack) THEN
          write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'nXTrack : ',this%nXtrack, &
            'size of Signal_k',SIZE(Signal_k)
          call PGE_WARN(func_name, "input Signal_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Signal_k(1:Nwl_l, 1:this%nXtrack) =  &
          this%Radiance(il:ih, 1:this%nXtrack, j)

        !TODO-Zoom mode
      ENDIF

      IF(PRESENT(SmallPixelSignal_k)) THEN
        IF(SIZE(SmallPixelSignal_k, 1) .LT. this%ncoad  .OR. &
          SIZE(SmallPixelSignal_k, 2) .LT. this%nXtrack) THEN
          write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',this%ncoad, &
            'nXTrack : ',this%nXtrack, &
            'size of SmallPixelSignal_k',SIZE(SmallPixelSignal_k)
          call PGE_WARN(func_name, "input SmallPixelSignal_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SmallPixelSignal_k(:, 1:this%nXtrack) =  &
          this%SmPixRad(:, 1:this%nXtrack, j)
      ENDIF

      IF(PRESENT(SignalError_k)) THEN
        IF(SIZE(SignalError_k, 1) < Nwl_l .OR. &
          SIZE(SignalError_k, 2) < this%nXtrack) THEN
          write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'nXTrack : ',this%nXtrack, &
            'size of Signal_k',SIZE(Signal_k)
          call PGE_WARN(func_name, "input SignalError_k array too small", msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SignalError_k(1:Nwl_l, 1:this%nXtrack) = &
          this%RadianceError(il:ih, 1:this%nXtrack, j)

        !TODO-Zoom mode
      ENDIF

      IF(PRESENT(SpectralQuality_k)) THEN
        IF(SIZE(SpectralQuality_k, 1) < Nwl_l .OR. &
          SIZE(SpectralQuality_k, 2) < this%nXtrack) THEN
          write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'nXTrack : ',this%nXtrack, &
            'size of SpectralQuality_k',SIZE(SpectralQuality_k)
          call PGE_WARN(func_name, &
            "input SpectralQuality_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        ! corrected by MB - 3/23/2023
        !SpectralQuality_k(1:Nwl_l, 1:this%nXtrack) = this%QualLevel(il:ih, 1:this%nXtrack, j)
        SpectralQuality_k(1:Nwl_l, 1:this%nXtrack) = this%SpecQual(il:ih, 1:this%nXtrack, j)

        !TODO-Zoom mode
      ENDIF

      IF(PRESENT(QualityLevel_k)) THEN
        IF(SIZE(QualityLevel_k, 1) < Nwl_l .OR. &
          SIZE(QualityLevel_k, 2) < this%nXtrack) THEN
          write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
            'nXTrack : ',this%nXtrack, &
            'size of QualityLevel_k',SIZE(QualityLevel_k)
          call PGE_WARN(func_name, &
            "input QualityLevel_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        ! corrected by MB - 3/23/2023
        !QualityLevel_k(1:Nwl_l, 1:this%nXtrack) = this%SpecQual(il:ih, 1:this%nXtrack, j)
        QualityLevel_k(1:Nwl_l, 1:this%nXtrack) = this%QualLevel(il:ih, 1:this%nXtrack, j)

        !TODO-Zoom mode
      ENDIF

      IF(PRESENT(Wavelength_k)) THEN
        IF(SIZE(Wavelength_k, 1) < Nwl_l .OR. &
          SIZE(Wavelength_k, 2) < this%nXtrack) THEN
          write(msg, "(A, I4, A, *(I4))")'Nwl_l = ',Nwl_l, 'size of Wavelength_k',&
            size(Wavelength_k)
          call PGE_WARN(func_name, &
            "input Wavelength_k array too small",msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Wavelength_k(1:Nwl_l, 1:this%nXtrack) = wl_local(il:ih, 1:this%nXtrack)

        !TODO-Zoom mode
      ENDIF

      ! Here is the raw data
      !nc4 Mantissa_k removed from code


      IF(PRESENT(SignalNoise_k)) THEN
        IF(SIZE(SignalNoise_k, 1) < Nwl_l  .OR. &
          SIZE(SignalNoise_k, 2) < this%nXtrack) THEN
          write(msg, '(A, I4, A, *(I4))')'Nwl_l = ',Nwl_l, 'size of SignalNoise_k = ', &
            size(SignalNoise_k)
          call PGE_WARN(func_name, 'input SignalNoise_k array too small', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SignalNoise_k(1:Nwl_l, 1:this%nXtrack) = this%RadianceNoise(il:ih, 1:this%nXtrack, j)
      ENDIF
    ENDIF  ! Nwl_l > 0


    IF(PRESENT(Nwl_k)) Nwl_k = Nwl_l
    ! The rest of the outputs do not depend on the value of Nwl_l

    IF(PRESENT(WavelengthCoefficient_k)) THEN
      IF(SIZE(WavelengthCoefficient_k, 1) < this%nWavelCoef  .OR. &
        SIZE(WavelengthCoefficient_k, 2) < this%nXtrack) THEN
        write(msg, '(A, I4, A, *(I4))')'NWavelCoef = ',this%nWavelCoef, &
          'size of WavelengthCoefficient_k = ', &
          size(WavelengthCoefficient_k)
        call PGE_WARN(func_name, &
          "input WavelengthCoefficient_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WavelengthCoefficient_k(1:this%nWavelCoef, 1:this%nXtrack) = &
        this%WavelenCoef(1:this%nWavelCoef, 1:this%nXtrack, j)

      !TODO-Zoom mode
    ENDIF

    IF(PRESENT(WavelengthShift_k)) THEN
      IF(SIZE(WavelengthShift_k, 1) < this%nXtrack) THEN
        call PGE_WARN(func_name, &
          "input WavelengthShift_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WavelengthShift_k( 1:this%nXtrack) = &
        this%WavelenShift(1:this%nXtrack, j)

      !TODO Do we need zoom mode for this parameter?
    ENDIF

    IF(PRESENT(WavelengthCoefficientPrec_k)) THEN
      IF(SIZE(WavelengthCoefficientPrec_k, 1) < this%nWavelCoef  .OR. &
        SIZE(WavelengthCoefficientPrec_k, 2) < this%nXtrack) THEN
        write(msg, '(A, I4, A, I4, A, *(I4))')'; nXtrack = ',this%nXtrack, &
          ' ; nWavelCoef = ',this%nWavelCoef, &
          'Size ofWavelengthCoefficientPrec_k',&
          SIZE(WavelengthCoefficientPrec_k)
        call PGE_WARN(func_name, &
          "input WavelengthCoefficientPrec_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
      WavelengthCoefficientPrec_k(1:this%nWavelCoef, 1:this%nXtrack) = &
        this%WavelenPrec(1:this%nWavelCoef, 1:this%nXtrack, j)
    ENDIF

    IF(PRESENT(WavelengthReferenceColumn_k)) &
      WavelengthReferenceColumn_k = this%WavelenRefCol
    RETURN
  END FUNCTION L1Br_getSIGline
  !
  !!! 9. L1Br_getSIGpixWL
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more radiance data field values from
  ! !       the data block.  This function returns values for each given output
  ! !       keyword for the each wavelength specified in the input wavelength list.
  ! !       This function only returns data from a single pixel in the L1B file.
  ! !       If the exact wavelength is not found, the data is interpolated via a
  ! !       simple linear interpolation to find the desired approximate value, which
  ! !       is then returned.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !       iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (nXtrack-1) inclusive.
  ! !       Wavelength_k:  list of input wavelengths
  ! !       Nwl_k is optional.  If present, then it indicates the number of valid
  ! !              wavelengths in Wavelength_k.  If not, then all the values in the
  ! !              Wavelength_k are assumed to be valid.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.
  ! !
  ! !       Keyword                    Corresponding L1B Data
  ! !       =======                    ======================
  ! !       Signal_k                   Radiance or Irradiance or Signal
  ! !       SignalError_k              Error in Radiance or Irradiance or
  ! !       SpectralQuality_k          [spectral_channel_quality]
  ! !       QualityLevel_k             [quality_level]
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !         May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getSIGpixWL(this, iLine, iPix, Wavelength_k, Nwl_k, Signal_k, &
      SignalError_k, SpectralQuality_k, QualityLevel_k) RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    !TODO Add use_nominal option to use nominal wavelengths or wavelength
    !       coefficients
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4), INTENT(IN):: iLine
    INTEGER (KIND = 4), INTENT(IN):: iPix
    REAL (KIND = 4), DIMENSION(:), INTENT(IN):: Wavelength_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(IN):: Nwl_k
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT(OUT):: Signal_k, SignalError_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT(OUT):: SpectralQuality_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT(OUT):: QualityLevel_k
    REAL (KIND = 4), DIMENSION(1:this%nWavel):: wl_local
    INTEGER (KIND = 4):: Nwl_l
    INTEGER:: i, j, k, q
    INTEGER:: fac
    INTEGER:: iw, last_good
    INTEGER (KIND = 4):: il, ih, i_foo(1)
    REAL (KIND = 4):: rad_il, rad_ih, frac, loc_max, loc_min
    INTEGER (KIND = 4):: status
    CHARACTER (LEN = 256):: func_name

    func_name = 'L1Br_getSIGpixWL'
    !Use only nominal wavelengths ONLY
    is_nominal = .TRUE.
    status = check_blk_pix(this, iLine, iPix, j, i)
    IF(status .NE. OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'I/O Error','Failed retrieving L1B signal data.')
      RETURN
    ENDIF

    loc_max = -1.0
    loc_min = -1.0
    ! is_nominal = .TRUE.

    ! DO k = 1, this%nWavel
    !   wl_local(k) = this%Wavelen(k, i)
    !   IF (wl_local(k) .gt. 0) THEN
    !     if (loc_min .lt. 0 .or. loc_min .gt. wl_local(k)) loc_min = wl_local(k)
    !     if (loc_max .lt. 0 .or. loc_max .lt. wl_local(k)) loc_max = wl_local(k)
    !   ENDIF
    ! ENDDO
    DO k = 1, this%nWavel
      wl_local(k) = 1.0
      DO q = 1, this%nWavelCoef
        IF(this%WavelenCoef(q,i,j) .LE. rfill) wl_local(k) = rfill
      ENDDO
      !NC4  WavelenRefCol is scalar in col4 Netcdf L1B
      fac = k-1-this%WavelenRefCol
      IF (wl_local(k) .gt. 0) THEN

        wl_local(k) = fac*this%WavelenCoef(this%nWavelCoef,i,j)
        DO q = this%nWavelCoef -1, 2, -1
          wl_local(k) = fac*(wl_local(k) + this%WavelenCoef(q,i,j))
        ENDDO
        wl_local(k) = wl_local(k) + this%WavelenCoef(1,i,j)
        if (loc_min .lt. 0 .or. loc_min .gt. wl_local(k)) loc_min = wl_local(k)
        if (loc_max .lt. 0 .or. loc_max .lt. wl_local(k)) loc_max = wl_local(k)
      ENDIF
    ENDDO

    IF(PRESENT(Nwl_k)) THEN
      Nwl_l = Nwl_k
      IF(SIZE(Wavelength_k) < Nwl_l) THEN
        write(msg, "(A, I4, A, *(I4))")'Nwl_l = ',Nwl_l, 'size of Wavelength_k',&
          size(Wavelength_k)
        call PGE_WARN(func_name, &
          "input Wavelength_k array too small",msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ELSE
      Nwl_l = SIZE(Wavelength_k)
    ENDIF

    IF(PRESENT(Signal_k)) THEN
      IF(SIZE(Signal_k) < Nwl_l) THEN
        write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
          'size of Signal_k',SIZE(Signal_k)
        call PGE_WARN(func_name, "input Signal_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(SignalError_k)) THEN
      IF(SIZE(SignalError_k) < Nwl_l) THEN
        write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
          'size of Signal_k',SIZE(SignalError_k)
        call PGE_WARN(func_name, "input SignalError_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(SpectralQuality_k)) THEN
      IF(SIZE(SpectralQuality_k) < Nwl_l) THEN
        write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
          'size of SpectralQuality_k',size(SpectralQuality_k)
        call PGE_WARN(func_name, &
          "input SpectralQuality_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(QualityLevel_k)) THEN
      IF(SIZE(QualityLevel_k) < Nwl_l) THEN
        write(msg, '(A, I3, A, *(I4))')'Nwl_l : ',Nwl_l, &
          'size of QualityLevel_k',size(QualityLevel_k)
        call PGE_WARN(func_name, &
          "input QualityLevel_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    DO iw = 1, Nwl_l
      IF(Wavelength_k(iw) > loc_max  .OR. &
        Wavelength_k(iw) < loc_min) THEN
        IF(PRESENT(Signal_k)) Signal_k(iw) =  0
        IF(PRESENT(SignalError_k)) &
          SignalError_k(iw) = 0
        IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw) = INT(0, 2)
        IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw) = INT(0, 2)
        CYCLE
      ENDIF

      il = 0
      ih = 0
      last_good = 0
      DO k = 1, this%nWavel
        if (wl_local(k) .gt. Wavelength_k(iw) .and. il .eq. 0) then
          il = last_good
          ih = k
          exit
        else if (wl_local(k) .eq. Wavelength_k(iw)) then
          il = k
          ih = k
          exit
        else if (wl_local(k) .gt. rfill) then
          last_good = k
        end if
      ENDDO

      ! Interpolate

      if (il .eq. 0 .or. ih .eq. 0) then
        IF(PRESENT(Signal_k)) Signal_k(iw) = rfill
        IF(PRESENT(SignalError_k)) SignalError_k(iw) = rfill
        IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw) = &
          int8fill
        IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw) = &
          int8fill
      else IF(il .EQ. ih) THEN
        IF(PRESENT(Signal_k)) Signal_k(iw) = this%Radiance(il, i, j)
        IF(PRESENT(SignalError_k)) SignalError_k(iw) = &
          this%RadianceError(il, i, j)
        IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw) = &
          this%SpecQual(il, i, j)
        IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw) = &
          this%QualLevel(il, i, j)
      ELSE
        IF(PRESENT(Signal_k)) THEN
          frac = (Wavelength_k(iw) - wl_local(il))/ &
            (wl_local(ih) - wl_local(il))
          rad_il = this%Radiance(il, i, j)
          rad_ih = this%Radiance(ih, i, j)
          Signal_k(iw) = rad_il*(1.0+frac*(rad_ih/rad_il-1.0))
          if (rad_il .eq. 0.0) Signal_k(iw) = rfill
        ENDIF

        IF(PRESENT(SignalError_k)) THEN
          frac = (Wavelength_k(iw) - wl_local(il))/ &
            (wl_local(ih) - wl_local(il))
          rad_il = this%RadianceError(il, i, j)
          rad_ih = this%RadianceError(ih, i, j)
          SignalError_k(iw) = rad_il*(1.0+frac*(rad_ih/rad_il-1.0))
        ENDIF

        IF(PRESENT(SpectralQuality_k)) THEN
          SpectralQuality_k(iw) = this%SpecQual(il, i, j)
        ENDIF

        IF(PRESENT(QualityLevel_k)) THEN
          QualityLevel_k(iw) = this%QualLevel(il, i, j)
        ENDIF
      ENDIF
    ENDDO
    RETURN
  END FUNCTION L1Br_getSIGpixWL
  !
  !!! 10. L1Br_getSIGlineWL
  ! !
  ! !    Functionality:
  ! !
  ! !       This function gets one or more radiance data field values from
  ! !       the data block.  This function returns values for each given output
  ! !       keyword for the each wavelength specified in the input wavelength list.
  ! !       This function returns data from an entire line of the L1B file.
  ! !       If the exact wavelength is not found, the data is interpolated via a
  ! !       simple linear interpolation to find the desired approximate value, which
  ! !       is then returned.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !       iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (NumTimes-1) inclusive.
  ! !       iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !              range from 0 to (nXtrack-1) inclusive.
  ! !       Wavelength_k:  list of input wavelengths
  ! !       Nwl_k is optional.  If present, then it indicates the number of valid
  ! !              wavelengths in Wavelength_k.  If not, then all the values in the
  ! !              Wavelength_k are assumed to be valid.
  ! !
  ! !    Outputs:
  ! !
  ! !       The following are keyword arguments.  Only those present in the argument
  ! !       list will be set by the function.
  ! !
  ! !       Keyword                    Corresponding L1B Data
  ! !       =======                    ======================
  ! !       Signal_k                   Radiance or Irradiance or Signal
  ! !       SignalError_k              Error in Radiance or Irradiance or
  ! !       SpectralQuality_k          [spectral_channel_quality]
  ! !       QualityLevel_k             [quality_level]
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !         May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_getSIGlineWL(this, iLine, Wavelength_k, Nwl_k, Signal_k, &
      SignalError_k, SpectralQuality_k, QualityLevel_k) RESULT (status)
    !TODO Add use_nominal option to use nominal wavelengths or wavelength
    !       coefficients
    ! USE Szoom_Parameter_Module
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER, PARAMETER:: i1 = 1
    INTEGER (KIND = i1), PARAMETER:: global_mode = 8_i1, szoom_mode = 4_i1
    INTEGER (KIND = 4), INTENT(IN):: iLine
    REAL (KIND = 4), DIMENSION(:), INTENT(IN):: Wavelength_k
    INTEGER (KIND = 4), OPTIONAL, INTENT(IN):: Nwl_k
    ! INTEGER (KIND = 4), OPTIONAL, INTENT(IN):: use_nominal
    REAL (KIND = 4), OPTIONAL, DIMENSION(:,:), INTENT(OUT):: Signal_k, SignalError_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:,:), INTENT(OUT):: SpectralQuality_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:,:), INTENT(OUT):: QualityLevel_k
    REAL (KIND = 4), DIMENSION(1:this%nWavel, 1:this%nXtrack):: wl_local
    INTEGER (KIND = 4):: Nwl_l
    INTEGER:: i, j, k, q
    INTEGER:: fac
    INTEGER:: iw
    INTEGER (KIND = 4):: il, ih, last_good
    REAL (KIND = 4):: rad_il, rad_ih, frac
    REAL (KIND = 4), DIMENSION(1:this%nXtrack):: loc_min, loc_max
    INTEGER (KIND = 4):: status

    REAL (KIND = 4), DIMENSION(1:this%nWavel, 1:this%nXtrack)  :: tmp_Signal_k
    REAL (KIND = 4), DIMENSION(1:this%nWavel, 1:this%nXtrack)  :: tmp_SignalError_k
    INTEGER (KIND = 1), DIMENSION(1:this%nWavel, 1:this%nXtrack):: tmp_SpectralQuality_k
    CHARACTER (LEN = 256):: func_name

    func_name = 'L1Br_getSIGlineWL'
    status = check_blk_line(this, iLine, j)
    IF(status .NE. OMI_S_SUCCESS) THEN
      call PGE_WARN(func_name, 'I/O Error','Failed retrieving L1B signal data.')
      RETURN
    ENDIF

    loc_min(1:this%nXtrack) = -1.0
    loc_max (1:this%nXtrack)= -1.0
    is_nominal = .TRUE.

    ! DONOT Use nominal wavelengths
    !if(is_nominal) then
    !  DO i = 1, this%nXtrack
    !    DO k = 1, this%nWavel
    !      wl_local(k, i) = this%Wavelen(k, i)
    !      IF (wl_local(k, i) .gt. 0) THEN
    !        if (loc_min(i) .lt. 0 .or. loc_min(i) .gt. wl_local(k, i)) &
    !          loc_min(i) = wl_local(k, i)
    !        if (loc_max(i) .lt. 0 .or. loc_max(i) .lt. wl_local(k, i)) &
    !          loc_max(i) = wl_local(k, i)
    !      ENDIF  ! wl_local(k, i) .gt. 0
    !    ENDDO  ! k
    !  ENDDO  ! i
    !else  ! is_nominal
    !  ! Use wavelength coefficients TODO-test or remove the following code
    !  DO i = 1, this%nXtrack
    !    DO k = 1, this%nWavel
    !      wl_local(k, i) = 1.0
    !      DO q = 1, this%nWavelCoef
    !        IF(this%WavelenCoef(q, i, j) >  1.0E30) wl_local(k, i) = rfill
    !        IF(this%WavelenCoef(q, i, j) < -1.0E28) wl_local(k, i) = rfill
    !      ENDDO  ! q
    !      IF (wl_local(k, i) .gt. 0) THEN

    !        ! fac = k-1-this%WavelenRefCol(j)
    !        fac = k-1-this%WavelenRefCol
    !        wl_local(k, i) = fac*this%WavelenCoef(this%nWavelCoef, i, j)
    !        DO q = this%nWavelCoef-1, 2, -1
    !          wl_local(k, i) = fac*(wl_local(k, i) + this%WavelenCoef(q, i, j))
    !        ENDDO  ! q
    !        wl_local(k, i) = wl_local(k, i) + this%WavelenCoef(1, i, j)
    !        if (loc_min(i) .lt. 0 .or. loc_min(i) .gt. wl_local(k, i)) &
    !          loc_min(i) = wl_local(k, i)
    !        if (loc_max(i) .lt. 0 .or. loc_max(i) .lt. wl_local(k, i)) &
    !          loc_max(i) = wl_local(k, i)
    !      ENDIF  ! wl_local(k, i) .gt. 0
    !    ENDDO  ! k
    !  ENDDO  ! i
    !endif  ! is_nominal
    DO i = 1, this%nXtrack
      DO k = 1, this%nWavel
        wl_local(k,i) = 1.0
        DO q = 1, this%nWavelCoef
          IF(this%WavelenCoef(q,i,j) >  1.0E30) wl_local(k,i) = rfill
          IF(this%WavelenCoef(q,i,j) < -1.0E28) wl_local(k,i) = rfill
        ENDDO
        IF (wl_local(k,i) .gt. 0) THEN

          fac = k-1-this%WavelenRefCol
          wl_local(k,i) = fac*this%WavelenCoef(this%nWavelCoef,i,j)
          DO q = this%nWavelCoef -1, 2, -1
            wl_local(k,i) = fac*(wl_local(k,i) + this%WavelenCoef(q,i,j))
          ENDDO
          wl_local(k,i) = wl_local(k,i) + this%WavelenCoef(1,i,j)
          if (loc_min(i) .lt. 0 .or. loc_min(i) .gt. wl_local(k,i)) &
            loc_min(i) = wl_local(k,i)
          if (loc_max(i) .lt. 0 .or. loc_max(i) .lt. wl_local(k,i)) &
            loc_max(i) = wl_local(k,i)
        ENDIF
      ENDDO
    ENDDO

    IF(PRESENT(Nwl_k)) THEN
      Nwl_l = Nwl_k
      IF(SIZE(Wavelength_k) < Nwl_l) THEN
        write(msg, "(A, I4, A, *(I4))")'Nwl_l = ',Nwl_l, 'size of Wavelength_k',&
          size(Wavelength_k)
        call PGE_WARN(func_name, &
          "input Wavelength_k array too small",msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ELSE
      Nwl_l = SIZE(Wavelength_k)
    ENDIF

    IF(PRESENT(Signal_k)) THEN
      IF(SIZE(Signal_k, 1) < Nwl_l .OR. &
        SIZE(Signal_k, 2) < this%nXtrack) THEN
        write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
          ', nXtrack :', this%nXtrack, &
          ', size of Signal_k : ',SIZE(Signal_k)
        call PGE_WARN(func_name, "input Signal_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(SignalError_k)) THEN
      IF(SIZE(SignalError_k, 1) < Nwl_l .OR. &
        SIZE(SignalError_k, 2) <  this%nXtrack) THEN
        write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
          ', nXtrack :', this%nXtrack, &
          ', size of SignalError_k : ',SIZE(SignalError_k)
        call PGE_WARN(func_name, "input SignalError_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(SpectralQuality_k)) THEN
      IF(SIZE(SpectralQuality_k, 1) < Nwl_l .OR. &
        SIZE(SpectralQuality_k, 2) < this%nXtrack) THEN
        write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
          ', nXTrack : ',this%nXtrack, &
          ', size of SpectralQuality_k',size(SpectralQuality_k)
        call PGE_WARN(func_name, &
          "input SpectralQuality_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    IF(PRESENT(QualityLevel_k)) THEN
      IF(SIZE(QualityLevel_k, 1) < Nwl_l .OR. &
        SIZE(QualityLevel_k, 2) < this%nXtrack) THEN
        write(msg, '(A, I3, A, I4, A, *(I4))')'Nwl_l : ',Nwl_l, &
          ', nXTrack : ',this%nXtrack, &
          ', size of QualityLevel_k',size(QualityLevel_k)
        call PGE_WARN(func_name, &
          "input QualityLevel_k array too small", msg)
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    DO iw = 1, Nwl_l
      DO i = 1, this%nXtrack
        IF(Wavelength_k(iw) > loc_max(i)  .OR. &
          Wavelength_k(iw) < loc_min(i)) THEN
          IF(PRESENT(Signal_k)) Signal_k(iw, i) =  0
          IF(PRESENT(SignalError_k)) &
            SignalError_k(iw, i) = 0
          IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw, i) &
            = INT(0, 2)
          IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw, i) &
            = INT(0, 2)
        ELSE
          il = 0
          ih = 0
          last_good = 0
          DO k = 1, this%nWavel
            if (wl_local(k, i) .gt. Wavelength_k(iw) .and. il .eq. 0) then
              il = last_good
              ih = k
              exit
            else if (wl_local(k, i) .eq. Wavelength_k(iw)) then
              il = k
              ih = k
              exit
              !                 else if (wl_local(k, i) .ne. rfill) then
            else if (wl_local(k, i) .GT. rfill) then
              last_good = k
            end if
          ENDDO

          ! Interpolate

          IF (il .eq. 0 .or. ih .eq. 0) THEN
            IF(PRESENT(Signal_k)) Signal_k(iw, i) = rfill
            IF(PRESENT(SignalError_k)) SignalError_k(iw, i) =rfill
            IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw, i) = &
              int8fill
            IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw, i) = &
              int8fill
          ELSE IF(il .EQ. ih) THEN
            IF(PRESENT(Signal_k)) Signal_k(iw, i) = &
              this%Radiance(il, i, j)
            IF(PRESENT(SignalError_k)) SignalError_k(iw, i) =&
              this%RadianceError(il, i, j)
            IF(PRESENT(SpectralQuality_k)) SpectralQuality_k(iw, i) = &
              this%SpecQual(il, i, j)
            IF(PRESENT(QualityLevel_k)) QualityLevel_k(iw, i) = &
              this%QualLevel(il, i, j)
          ELSE
            IF(PRESENT(Signal_k)) THEN
              frac = (Wavelength_k(iw) - wl_local(il, i))/ &
                (wl_local(ih, i)   - wl_local(il, i))
              rad_il = this%Radiance(il, i, j)
              rad_ih = this%Radiance(ih, i, j)
              Signal_k(iw, i) = rad_il*(1.0+frac*(rad_ih/rad_il-1.0))
            ENDIF

            IF(PRESENT(SignalError_k)) THEN
              rad_il = this%RadianceError(il, i, j)
              rad_ih = this%RadianceError(ih, i, j)
              SignalError_k(iw, i) = MAX(rad_il*1.0, rad_ih*1.0)
            ENDIF

            IF(PRESENT(SpectralQuality_k)) THEN
              SpectralQuality_k(iw, i) = this%SpecQual(il, i, j)

              IF(PRESENT(QualityLevel_k)) THEN
                QualityLevel_k(iw, i) = this%QualLevel(il, i, j)
              ENDIF
            ENDIF
          ENDIF
        ENDIF
      ENDDO
    ENDDO

    ! Zoom mode here
    RETURN
  END FUNCTION L1Br_getSIGlineWL
  !
  !!! 11: L1Br_getDATAFIELDline
  ! !
  ! !    Functionality:
  ! !
  ! !       This function will return the data from a single line of any specified dataset
  ! !       in the L1B file.  This function exists in case any modifications are made to the
  ! !       L1B file, or in case a PGE requires the use of some of the more obscure datasets
  ! !       which are not accommodated by the rest of the L1B Reader code.  In particular,
  ! !       this includes a few of the calibration datasets.  This function is much less
  ! !       efficient than other L1B Reader functions, so it is advised that it is only
  ! !       used for datasets not otherwise included in the L1B Reader functions.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       filename      the L1B filename
  ! !       swathname     the L1B swathname
  ! !       fieldname     the L1B dataset name
  ! !       Line          the L1B line number to be retrieved
  ! !
  ! !    Outputs:
  ! !
  ! !       buf           data space to hold L1B values
  ! !       status        the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source
  !         May 2020        Ramaswamy T     Modify interface for netCDF4 format
  ! !
  !!!
  !      FUNCTION L1Br_getDATAFIELDline(filename, swathname, fieldname, Line, numtype, buf) &
  !                RESULT(status)
  !        USE hdfeos4_parameters
  !        INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
  !        INTEGER (KIND = 4), INTENT(IN):: Line, numtype
  !        CHARACTER (LEN = *), INTENT(IN):: filename, swathname, fieldname
  !	REAL (KIND = 4), INTENT(OUT):: buf
  !	CHARACTER (LEN = 256):: message
  !        INTEGER (KIND = 4):: status
  !        INTEGER (KIND = 4):: getl1bblk
  !        EXTERNAL              getl1bblk
  !        INTEGER (KIND = 4):: rank
  !        INTEGER (KIND = 4):: nL, swid, swfid, fldflg, rLine
  !        INTEGER (KIND = 4), DIMENSION(1:3):: dims
  !        CHARACTER (LEN = 256):: func_name
  !
  !	nL = 1
  !	rLine = Line+1
  !
  !        ! Open the L1B swath file
  !
  !        swfid = swopen(filename, DFACC_READ)
  !        IF(swfid < zero) THEN
  !           status = OMI_E_FAILURE
  !           ierr = OMI_SMF_setmsg(OMI_E_FILE_OPEN, filename, &
  !                 "L1Br_getDATAFIELDline", zero)
  !           RETURN
  !        ENDIF
  !
  !        ! Attach to the swath
  !
  !        swid = swattach(swfid, swathname)
  !        IF(swid < zero) THEN
  !           status = OMI_E_FAILURE
  !           ierr = OMI_SMF_setmsg(OMI_E_SWATH_ATTACH, swathname, &
  !                 "L1Br_getDATAFIELDline", zero)
  !           ierr = swclose(swfid)
  !           RETURN
  !        ENDIF
  !
  !        ! Read requested L1B datasets
  !
  !        status = getl1bblk(swid, fieldname, numtype, fldflg, Line, nL, rank, dims, buf)
  !        IF(status .NE. OMI_S_SUCCESS) THEN
  !           ierr = OMI_SMF_setmsg(OMI_E_FAILURE, "Unrecoverable error reading L1B file", &
  !                                 "L1Br_getDATAFIELDline", zero)
  !        ENDIF
  !        IF(fldflg .NE. 0) THEN
  !           status = OMI_E_FAILURE
  !	   message = "Unable to read "//fieldname
  !           ierr = OMI_SMF_setmsg(OMI_E_FAILURE, message, "L1Br_getDATAFIELDline", zero)
  !        ENDIF
  !
  !        ! Close, detach, and go home
  !
  !        ierr = swdetach(swid)
  !        ierr = swclose(swfid)
  !
  !        RETURN
  !      END FUNCTION L1Br_getDATAFIELDline
  !
  !
  !!! 7. L1Br_getDATAline
  ! !    This function gets one or more small pixel data field values from
  ! !    the data block.
  ! !    this: the block data structure
  ! !    iLine: the line number in the SmPx swath.  NOTE: this input is 0 based
  ! !           range from 0 to (nTimes-1) inclusive.
  ! !
  ! !    Data_K, Quality_k, and Wavelength_k
  ! !    are keyword arguments.  Only those present in the argument
  ! !    list will be set by the function.
  ! !
  ! !    status: the return PGS_SMF status value
  !!!
  !      FUNCTION L1Br_getDATAline( this, iLine, nPix, Data_k, &
  !                             Wavelength_k, Quality_k ) &
  !                             RESULT (status)
  !        USE Szoom_Parameter_Module
  !        INCLUDE 'PGS_OMI_1900.f'  !!this external file defines the SmPx PGS error codes
  !        TYPE (L1B_block_type), INTENT( INOUT ):: this
  !        INTEGER, PARAMETER:: i1 = 1
  !        INTEGER (KIND = i1), PARAMETER:: global_mode = 8_i1, szoom_mode = 4_i1
  !        INTEGER (KIND = 4), INTENT( IN ):: iLine
  !        REAL (KIND = 4), OPTIONAL, DIMENSION(:,:), INTENT( OUT ) :: &
  !                                         Data_k, Wavelength_k
  !        REAL (KIND = 4), DIMENSION(1:this%nXtrack, 1:this%NumTimesSmallPixel) :: &
  !                                         tmp_Data_k, tmp_Wavelength_k
  !        INTEGER (KIND = 2), OPTIONAL, DIMENSION(:), INTENT( OUT ):: Quality_k
  !        INTEGER (KIND = 2), DIMENSION(1:this%nXtrack):: tmp_Quality_k
  !        INTEGER (KIND = 2), INTENT( OUT ):: nPix
  !        INTEGER (KIND = 4):: i, j, k
  !        INTEGER:: itmp, ip
  !        INTEGER (KIND = 4):: status
  !        CHARACTER (LEN = 256):: func_name
  !
  !
  !        status = OMI_S_SUCCESS
  !        IF( .NOT. this%initialized ) THEN
  !           ierr = OMI_SMF_setmsg( OMI_E_INPUT, &
  !                                 "input block not initialized", &
  !                                 "L1Br_getDATAline", zero )
  !           status = OMI_E_FAILURE
  !           RETURN
  !        ENDIF
  !
  !        IF( iLine < 0 .OR. iLine >= this%NumTimes) THEN
  !           ierr = OMI_SMF_setmsg( OMI_E_INPUT, "iLine out of range", &
  !                                 "L1Br_getDATAline", zero )
  !           status = OMI_E_FAILURE
  !           RETURN
  !        ENDIF
  !
  !        IF( iLine < this%iLine .OR. iLine > this%eLine ) THEN
  !           status = fill_l1b_blk( this, iLine )
  !           IF( status .NE. OMI_S_SUCCESS ) THEN
  !              ierr = OMI_SMF_setmsg( OMI_E_DATA_BLOCK, "retrieve data block", &
  !                                    "L1Br_getDATAline", zero )
  !              RETURN
  !           ENDIF
  !        ENDIF
  !
  !        status = check_blk_line(this, iLine, ip)
  !        IF(status .NE. OMI_S_SUCCESS) THEN
  !           ierr = OMI_SMF_setmsg(OMI_E_GENERAL, "Failed retrieving getDATAline data.", &
  !                                 "L1Br_getDATAline", one)
  !           RETURN
  !        ENDIF
  !
  !        nPix = this%NumSmPix(iLine+1)
  !
  !
  !        ! Calculate index into small pixel arrays
  !
  !        i = 1
  !        do k = 1, iLine
  !           i = i+this%NumSmPix(k)
  !        enddo
  !        !PBD Changed added-1
  !        !j = i+nPix adds an extra value and causes bounds errors at the
  !        !end of the array
  !        j = i+nPix-1
  !
  !        IF( PRESENT( Quality_k ) ) THEN
  !           IF( SIZE( Quality_k ) < this%nXtrack ) THEN
  !              ierr = OMI_SMF_setmsg( OMI_E_INPUT, &
  !                                    "input Quality_k array too small", &
  !                                    "L1Br_getDATAline", zero )
  !              status = OMI_E_FAILURE
  !              RETURN
  !           ENDIF
  !           Quality_k(1:this%nXtrack) = &
  !                             this%PFlag(1:this%nXtrack, iLine-this%iLine+1)
  !           IF (this%ImBinFact(ip) == szoom_mode) THEN
  !              IF (this%SpaZoomIndex == 0) THEN
  !               DO itmp = 1, this%nXtrack
  !                    tmp_Quality_k(itmp) = Quality_k(itmp)
  !               ENDDO
  !               Quality_k(gfpix:zspix) = int16fill
  !               Quality_k(zspix+1:zspix+zlpix) =  tmp_Quality_k(zfpix:zlpix)
  !               Quality_k(zlpix+zspix+1:glpix) = int16fill
  !              ENDIF
  !           ENDIF
  !
  !        ENDIF
  !
  !
  !        IF( PRESENT( Data_k ) ) THEN
  !           IF( SIZE( Data_k, 2 ) < nPix  .OR. &
  !               SIZE( Data_k, 1 ) < this%nXtrack ) THEN
  !              ierr = OMI_SMF_setmsg( OMI_E_INPUT, &
  !                                    "input Data_k array too small", &
  !                                    "L1Br_getDATAline", zero )
  !              status = OMI_E_FAILURE
  !              RETURN
  !           ENDIF
  !           Data_k(1:this%nXtrack, 1:nPix) =  &
  !                             this%SmPixRad(1:this%nXtrack, i:j)
  !
  !           IF (this%ImBinFact(ip) == szoom_mode) THEN
  !              IF (this%SpaZoomIndex == 0) THEN
  !                DO itmp = 1, this%nXtrack
  !                    tmp_Data_k(itmp, 1:nPix) = Data_k(itmp, 1:nPix)
  !                ENDDO
  !                Data_k(gfpix:zspix, 1:nPix) = rfill
  !                Data_k(zspix+1:zspix+zlpix, 1:nPix) =  tmp_Data_k(zfpix:zlpix, 1:nPix)
  !                Data_k(zlpix+zspix+1:glpix, 1:nPix) = rfill
  !              ENDIF
  !           ENDIF
  !
  !        ENDIF
  !
  !        IF( PRESENT( Wavelength_k ) ) THEN
  !           IF( SIZE( Wavelength_k, 2 ) < nPix .OR. &
  !               SIZE( Wavelength_k, 1 ) < this%nXtrack ) THEN
  !              ierr = OMI_SMF_setmsg( OMI_E_INPUT, &
  !                                    "input Wavelength_k array too small",&
  !                                    "L1Br_getDATAline", zero )
  !              status = OMI_E_FAILURE
  !              RETURN
  !           ENDIF
  !           Wavelength_k(1:this%nXtrack, 1:nPix) = &
  !                                         this%SmPixWavelen(1:this%nXtrack, i:j)
  !           IF (this%ImBinFact(ip) == szoom_mode) THEN
  !              IF (this%SpaZoomIndex == 0) THEN
  !                DO itmp = 1, this%nXtrack
  !                    tmp_Wavelength_k(itmp, 1:nPix) = Wavelength_k(itmp, 1:nPix)
  !                ENDDO
  !                Wavelength_k(gfpix:zspix, 1:nPix) = rfill
  !                Wavelength_k(zspix+1:zspix+zlpix, 1:nPix) =  tmp_Wavelength_k(zfpix:zlpix, 1:nPix)
  !                Wavelength_k(zlpix+zspix+1:glpix, 1:nPix) = rfill
  !              ENDIF
  !           ENDIF
  !
  !        ENDIF
  !
  !        RETURN
  !      END FUNCTION L1Br_getDATAline
  !
  !!! 12. L1Br_close
  ! !
  ! !    Functionality:
  ! !
  ! !       This function should be called when the data block is no longer
  ! !       needed.  It deallocates all the allocated memory, and sets
  ! !       all the parameters to invalid values.
  ! !
  ! !    Calling Arguments:
  ! !
  ! !    Inputs:
  ! !
  ! !       this: the block data structure
  ! !
  ! !    Outputs:
  ! !
  ! !       status: the return PGS_SMF status value
  ! !
  ! !    Change History:
  ! !
  ! !       Date            Author          Modifications
  ! !       ====            ======          =============
  ! !       January 2005    Jeremy Warner   Original Source (derived from original
  ! !                                             L1B Reader code written by Kai Yang)
  !         May 2020        Ramaswamy T     Modify interface for collection 4 netCDF4
  !                                           format
  ! !
  !!!
  FUNCTION L1Br_close(this) RESULT(status)
    !  INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER (KIND = 4):: status, error
    CHARACTER (LEN = 256):: func_name

    status = OMI_S_SUCCESS
    func_name='L1Br_close'
    IF(.NOT. this%initialized) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    ! DEALLOCATE all the memory
    IF (ASSOCIATED(this%Time)) DEALLOCATE(this%Time)
    IF (ASSOCIATED(this%SecInDay)) DEALLOCATE(this%SecInDay)
    IF (ASSOCIATED(this%ScLat)) DEALLOCATE(this%ScLat)
    IF (ASSOCIATED(this%ScLon)) DEALLOCATE(this%ScLon)
    IF (ASSOCIATED(this%ScAlt)) DEALLOCATE(this%ScAlt)
    NULLIFY(this%Time, this%SecInDay, this%ScLat, this%ScLon, this%ScAlt)
    ! IF (ASSOCIATED(this%SolElevation)) DEALLOCATE(this%SolElevation)
    ! IF (ASSOCIATED(this%SolElevMin)) DEALLOCATE(this%SolElevMin)
    ! IF (ASSOCIATED(this%SolElevMax)) DEALLOCATE(this%SolElevMax)
    ! NULLIFY(this%SolElevation, this%SolElevMin, this%SolElevMax)
    ! IF (ASSOCIATED(this%SolAzimuth)) DEALLOCATE(this%SolAzimuth)
    ! IF (ASSOCIATED(this%SolAziMin)) DEALLOCATE(this%SolAziMin)
    ! IF (ASSOCIATED(this%SolAziMax)) DEALLOCATE(this%SolAziMax)
    IF (ASSOCIATED(this%Lon)) DEALLOCATE(this%Lon)
    IF (ASSOCIATED(this%Lat)) DEALLOCATE(this%Lat)
    IF (ASSOCIATED(this%LonBounds)) DEALLOCATE(this%LonBounds)
    IF (ASSOCIATED(this%LatBounds)) DEALLOCATE(this%LatBounds)
    ! NULLIFY(this%SolAzimuth, this%SolAziMin, this%SolAziMax, this%Lon, this%Lat)
    IF (ASSOCIATED(this%SolZenAng)) DEALLOCATE(this%SolZenAng)
    IF (ASSOCIATED(this%SolAziAng)) DEALLOCATE(this%SolAziAng)
    IF (ASSOCIATED(this%ViewZenAng)) DEALLOCATE(this%ViewZenAng)
    IF (ASSOCIATED(this%ViewAziAng)) DEALLOCATE(this%ViewAziAng)
    NULLIFY(this%SolZenAng, this%SolAziAng, this%ViewZenAng, this%ViewAziAng)
    IF (ASSOCIATED(this%TerrainHeight)) DEALLOCATE(this%TerrainHeight)
    IF (ASSOCIATED(this%WaterFraction)) DEALLOCATE(this%WaterFraction)
    IF (ASSOCIATED(this%GPQFlag)) DEALLOCATE(this%GPQFlag)
    IF (ASSOCIATED(this%XTQFlag)) DEALLOCATE(this%XTQFlag)
    NULLIFY(this%TerrainHeight, this%GPQFlag, this%XTQFlag)
    ! NULLIFY(this%TerrainHeight, this%GPQFlag)
    IF (ASSOCIATED(this%Radiance)) DEALLOCATE(this%Radiance)
    IF (ASSOCIATED(this%RadPrecision)) DEALLOCATE(this%RadPrecision)
    IF (ASSOCIATED(this%PFlag)) DEALLOCATE(this%PFlag)
    ! NULLIFY(this%Radiance, this%RadPrecision, this%SpecQual, this%PFlag)
    NULLIFY(this%Radiance, this%RadPrecision, this%PFlag)
    IF (ASSOCIATED(this%RadianceError)) DEALLOCATE(this%RadianceError)
    IF (ASSOCIATED(this%SmPixRad)) DEALLOCATE(this%SmPixRad)
    IF (ASSOCIATED(this%SmPixWavelen)) DEALLOCATE(this%SmPixWavelen)
    NULLIFY(this%RadianceError, this%SmPixRad, this%SmPixWavelen)
    IF (ASSOCIATED(this%WavelenCoef)) DEALLOCATE(this%WavelenCoef)
    IF (ASSOCIATED(this%WavelenPrec)) DEALLOCATE(this%WavelenPrec)
    ! IF (ASSOCIATED(this%WavelenRefCol)) DEALLOCATE(this%WavelenRefCol)
    ! NULLIFY(this%WavelenCoef, this%WavelenPrec, this%WavelenRefCol)
    NULLIFY(this%WavelenCoef, this%WavelenPrec)
    IF (ASSOCIATED(this%Config)) DEALLOCATE(this%Config)
    IF (ASSOCIATED(this%MeasClass)) DEALLOCATE(this%MeasClass)
    IF (ASSOCIATED(this%NumSmPixCol)) DEALLOCATE(this%NumSmPixCol)
    NULLIFY(this%Config, this%MeasClass, this%NumSmPixCol)
    IF (ASSOCIATED(this%ExposureType)) DEALLOCATE(this%ExposureType)
    IF (ASSOCIATED(this%ImBinFact)) DEALLOCATE(this%ImBinFact)
    NULLIFY(this%ExposureType, this%ImBinFact)
    IF (ASSOCIATED(this%GC1)) DEALLOCATE(this%GC1)
    IF (ASSOCIATED(this%GC2)) DEALLOCATE(this%GC2)
    IF (ASSOCIATED(this%GC3)) DEALLOCATE(this%GC3)
    IF (ASSOCIATED(this%GC4)) DEALLOCATE(this%GC4)
    IF (ASSOCIATED(this%DSGC)) DEALLOCATE(this%DSGC)
    NULLIFY(this%GC1, this%GC2, this%GC3, this%GC4, this%DSGC)
    IF (ASSOCIATED(this%LSLABF)) DEALLOCATE(this%LSLABF)
    IF (ASSOCIATED(this%USLABF)) DEALLOCATE(this%USLABF)
    IF (ASSOCIATED(this%LDABF)) DEALLOCATE(this%LDABF)
    IF (ASSOCIATED(this%UDABF)) DEALLOCATE(this%UDABF)
    NULLIFY(this%LSLABF, this%USLABF, this%LDABF, this%UDABF)
    IF (ASSOCIATED(this%MQFlag)) DEALLOCATE(this%MQFlag)
    IF (ASSOCIATED(this%SpecQual)) DEALLOCATE(this%SpecQual)
    IF (ASSOCIATED(this%QualLevel)) DEALLOCATE(this%QualLevel)
    IF (ASSOCIATED(this%LandWater)) DEALLOCATE(this%LandWater)
    IF (ASSOCIATED(this%CalSet)) DEALLOCATE(this%CalSet)
    IF (ASSOCIATED(this%SmPixCol)) DEALLOCATE(this%SmPixCol)
    NULLIFY(this%MQFlag, this%CalSet, this%SmPixCol)
    NULLIFY(this%QualLevel, this%SpecQual)
    IF (ASSOCIATED(this%GSC1)) DEALLOCATE(this%GSC1)
    IF (ASSOCIATED(this%GSC2)) DEALLOCATE(this%GSC2)
    IF (ASSOCIATED(this%GSC3)) DEALLOCATE(this%GSC3)
    NULLIFY(this%GSC1, this%GSC2, this%GSC3)
    IF (ASSOCIATED(this%SR1)) DEALLOCATE(this%SR1)
    IF (ASSOCIATED(this%SR2)) DEALLOCATE(this%SR2)
    IF (ASSOCIATED(this%SR3)) DEALLOCATE(this%SR3)
    IF (ASSOCIATED(this%SR4)) DEALLOCATE(this%SR4)
    NULLIFY(this%SR1, this%SR2, this%SR3, this%SR4)
    IF (ASSOCIATED(this%BinImgRows)) DEALLOCATE(this%BinImgRows)
    IF (ASSOCIATED(this%StopColumn)) DEALLOCATE(this%StopColumn)
    IF (ASSOCIATED(this%MasterClkPer)) DEALLOCATE(this%MasterClkPer)
    NULLIFY(this%BinImgRows, this%StopColumn, this%MasterClkPer)
    IF (ASSOCIATED(this%ExpTime)) DEALLOCATE(this%ExpTime)
    IF (ASSOCIATED(this%ReadTime)) DEALLOCATE(this%ReadTime)
    IF (ASSOCIATED(this%DetcTemp)) DEALLOCATE(this%DetcTemp)
    NULLIFY(this%ExpTime, this%ReadTime, this%DetcTemp)
    IF (ASSOCIATED(this%OptBenchTemp)) DEALLOCATE(this%OptBenchTemp)
    NULLIFY(this%OptBenchTemp)

    this%iLine              = -1
    this%nLine              = -1
    this%eLine              = -1
    this%NumTimes           = -1
    this%NumTimesSmallPixel = -1
    this%nCoad              = -1 
    this%nXtrack            = -1
    this%nWavel             = -1
    this%nWavelCoef         = -1
    this%filename           = ""
    this%bandname           = ""
    this%modename           = ""
    this%initialized        = .FALSE.

    ! close netcdf file MB 3/22/23
    error = nf90_close(this%id_nc)

    RETURN

  END FUNCTION L1Br_close
  !
  !!! 6. L1Br_getRADpix
  ! !    This function gets one or more radiance data field values from
  ! !    the data block.
  ! !    this: the block data structure
  ! !    iLine: the line number in the L1B swath.  NOTE: this input is 0 based
  ! !           range from 0 to (NumTimes-1) inclusive.
  ! !    iPix: the pixel number in the L1B swath.  NOTE: this input is 0 based
  ! !          range from 0 to (nXtrack-1) inclusive.
  ! !
  ! !    Wlmin_k: input wavelength value for the band beginning (in nm)
  ! !    Wlmax_k: input wavelength value for the band end (in nm)
  ! !    (These two inputs are optional.  Note that Wlmin_k < Wlmax_k.)
  ! !
  ! !    Radiance_k, RadiancePrecision_k, SpectralQuality_k, Wavelength_k,
  ! !    QualityLevel_k and Nwl_k are keyword arguments.  Only those present
  ! !    in the argument list will be set by the the function.
  ! !
  ! !    status: the return PGS_SMF status value
  !!!
  FUNCTION L1Br_getRADpix( this, iLine, iPix, Wlmin_k, Wlmax_k, &
      Radiance_k, QualityLevel_k, &
      SpectralQuality_k, Wavelength_k, &
      Nwl_k ) &
      RESULT (status)
    ! INCLUDE 'PGS_OMI_1900.f'  !defines the L1B PGS error codes
    TYPE (L1B_block_type), INTENT( INOUT ):: this
    INTEGER (KIND = 4), INTENT( IN ):: iLine, iPix
    INTEGER (KIND = 4), OPTIONAL, INTENT( OUT ):: Nwl_k
    REAL (KIND = 4), OPTIONAL, INTENT( IN ):: Wlmin_k, &
      Wlmax_k    ! wavelength range
    REAL (KIND = 4), OPTIONAL, DIMENSION(:), INTENT( OUT ):: Radiance_k, &
      Wavelength_k
    INTEGER (KIND = 1), OPTIONAL, DIMENSION(:), INTENT( OUT ) :: &
      SpectralQuality_k, QualityLevel_k
    REAL (KIND = 4), DIMENSION(1:this%nWavel):: wl_local
    INTEGER (KIND = 4):: Nwl_l
    INTEGER:: i, j, k, q
    INTEGER (KIND = 4):: il, ih, i_foo(1)
    INTEGER (KIND = 4):: status, fflag
    REAL (KIND = 4):: minWl, maxWl
    CHARACTER (LEN = 256):: func_name

    func_name='L1Br_getRADpix'
    fflag = 0
    status = OMI_S_SUCCESS
    IF( .NOT. this%initialized ) THEN
      call PGE_WARN(func_name, 'Initialization Error','input block not initialized')
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF( iLine < 1 .OR. iLine > this%NumTimes ) THEN
      write(msg, '(A, I5, A, I5)')'iLine=',iLine, 'NumTimes=',this%NumTimes
      call PGE_WARN(func_name, 'iLine out of range', TRIM(msg))
      status = OMI_E_FAILURE
      RETURN
    ENDIF

    IF( iPix < 1 .OR. iPix > this%nXtrack ) THEN
      write(msg, '(A, I5, A, I5)')'iPix=',iPix, 'nXtrack=',this%nXtrack
      call PGE_WARN(func_name, 'iPix out of range', TRIM(msg))
      RETURN
    ENDIF

    IF( iLine < this%iLine .OR. iLine > this%eLine ) THEN
      status = fill_l1b_blk( this, iLine )
      IF( status .NE. OMI_S_SUCCESS ) THEN
        call PGE_WARN(func_name, 'I/O Error','retrieve data block')
        status = OMI_E_FAILURE
        RETURN
      ENDIF
    ENDIF

    ! not needed as we are using FORTRAN-based reader, instead of C
    ! i = iPix+1
    ! j = iLine-this%iLine+1
    i = iPix
    j = iLine-this%iLine+1
    fflag = 0
    ! Use nominal wavelengths instead of wavelength coefficients
    wl_local = this%Wavelen(1:this%nWavel, i)
    if (any(wl_local < 0.0)) fflag = 1

    IF (fflag .EQ. 0) THEN
      IF(PRESENT( Wlmin_k ) .AND. PRESENT(  Wlmax_k )) THEN
        IF( (Wlmin_k-Wlmax_k) > 0.0 ) THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'Wlmin_k = ',Wlmin_k, ' Wlmax_k = ',Wlmax_k
          call PGE_WARN(func_name, 'input Wlmin_k greater than Wlmax_k', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
      ENDIF

      IF( PRESENT( Wlmin_k )) THEN
        IF( Wlmin_k > MAXVAL( wl_local ) )THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'Wlmin_k = ',Wlmin_k, &
            ' max. wl_local = ', MAXVAL(wl_local)
          call PGE_WARN(func_name, 'input Wlmin_k out of bound', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        IF( Wlmin_k < MINVAL(wl_local) ) THEN
          il = 1
        ELSE
          i_foo = MAXLOC( wl_local, MASK = wl_local-Wlmin_k <= 0.0 )
          il    = i_foo( 1 )
        ENDIF
      ELSE
        il = 1
      ENDIF

      IF( PRESENT( Wlmax_k )) THEN
        IF( Wlmax_k < MINVAL( wl_local ) ) THEN
          write(msg, '(A, F9.3, A,3X, F9.3)')'Wlmax_k = ',Wlmax_k, &
            'min wl_local = ', MINVAL( wl_local )
          call PGE_WARN(func_name, 'input Wlmax_k out of bound', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF

        IF( Wlmax_k > MAXVAL( wl_local ) ) THEN
          ih = this%nWavel
        ELSE
          i_foo = MINLOC( wl_local, MASK = wl_local-Wlmax_k >= 0.0 )
          ih    = i_foo( 1 )
        ENDIF
      ELSE
        ih = this%nWavel
      ENDIF

      Nwl_l = ih-il+1
    ELSE  ! fflag != 0
      IF(PRESENT( Wlmin_k )) THEN
        minWl = Wlmin_k
      ELSE
        minWl = 0.0
      ENDIF
      IF(PRESENT( Wlmax_k )) THEN
        maxWl = Wlmax_k
      ELSE
        maxWl = 1000000.0
      ENDIF
      ih = 0
      il = 0
      DO k = 1, this%nWavel
        IF (wl_local(k) >= minWl .AND. wl_local(k) <= maxWl) THEN
          IF (il .EQ. 0) il = k
          IF (ih .EQ. 0) ih = k
          IF (ih .LT. k) ih = k
        ENDIF
      ENDDO
      IF (il .EQ. 0 .AND. ih .EQ. 0) THEN
        Nwl_l = 0
      ELSE
        IF (il > 1) il = il-1
        IF (ih < this%nWavel) ih = ih+1
        Nwl_l = ih-il+1
      ENDIF
    ENDIF

    IF (Nwl_l > 0) THEN
      IF( PRESENT( Radiance_k ) ) THEN
        IF( SIZE( Radiance_k ) < Nwl_l ) THEN
          write(msg, '(A, *(I3), A, I3)')'Size of Radiance_k :',&
            SIZE( Radiance_k), 'Nwl_l = ',Nwl_l
          call PGE_WARN(func_name, 'input Radiance_k array too small', &
            msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Radiance_k(1:Nwl_l) = this%Radiance(il:ih, i, j)

        ! DO q = il, ih
        !   IF(this%RadMantissa(q, i, j) .eq. -32767) Radiance_k(q-il+1) = rfill
        ! ENDDO

      ENDIF

      IF( PRESENT( SpectralQuality_k ) ) THEN
        IF( SIZE( SpectralQuality_k ) < Nwl_l ) THEN
          write(msg, '(A, *(I3), A, I3)')'Size of SpectralQuality_k :',&
            SIZE(SpectralQuality_k), 'Nwl_l = ',Nwl_l
          call PGE_WARN(func_name, 'input SpectralQuality_k array too small', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        SpectralQuality_k(1:Nwl_l) = this%SpecQual(il:ih, i, j)
      ENDIF

      IF( PRESENT( QualityLevel_k ) ) THEN
        IF( SIZE( QualityLevel_k ) < Nwl_l ) THEN
          write(msg, '(A, *(I3), A, I3)')'Size of QualityLevel_k :',&
            SIZE(QualityLevel_k), 'Nwl_l = ',Nwl_l
          call PGE_WARN(func_name, 'input QualityLevel_k array too small', msg)
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        QualityLevel_k(1:Nwl_l) = this%QualLevel(il:ih, i, j)
      ENDIF

      IF( PRESENT( Wavelength_k ) ) THEN
        IF( SIZE( Wavelength_k ) < Nwl_l ) THEN
          write(msg, '(A, *(I3), A, I3)')'Size of Wavelength_k :', &
            SIZE(Wavelength_k), 'Nwl_l = ',Nwl_l
          status = OMI_E_FAILURE
          RETURN
        ENDIF
        Wavelength_k(1:Nwl_l) = wl_local(il:ih)
      ENDIF
    ENDIF

    IF( PRESENT( Nwl_k ) ) Nwl_k = Nwl_l

    RETURN
  END FUNCTION L1Br_getRADpix


  FUNCTION L1Br_getGlobalAttr(this, year, month, day, &
      OrbitNumber, &
      EquatorCrossingLongitude, &
      EquatorCrossingDateTime, &
      time_coverage_start, &
      time_coverage_end, &
      RangeBegDateTime, &
      RangeEndDateTime, &
      ! adding processor_version to globalattributes - MB 10/22/23
      processor_version) RESULT (status)
    implicit none
    TYPE (L1B_block_type), INTENT(INOUT):: this
    INTEGER(KIND = 4), INTENT(OUT), optional:: year, month, day
    CHARACTER(LEN = 255), INTENT(OUT), optional :: &
      OrbitNumber, RangeBegDateTime, &
      EquatorCrossingLongitude, &
      EquatorCrossingDateTime, &
      time_coverage_start, &
      time_coverage_end, &
      RangeEndDateTime, &
      processor_version ! , ShortName
    INTEGER (KIND = 4):: status
    character(len = 255):: temp_char
    integer(kind = 4):: tyear, tmonth, tday, thour, tminute, tsec
    integer(kind = 4):: meta_id, ecs_id, inv_met_id

    status = OMI_E_FAILURE
    if (present(time_coverage_start)) then
      status = nc4read_global_attr(this%id_nc, "time_coverage_start", temp_char)
      time_coverage_start = TRIM(temp_char)
    endif

    if (present(time_coverage_end)) then
      temp_char = ''
      status = nc4read_global_attr(this%id_nc, "time_coverage_end", temp_char)
      time_coverage_end = TRIM(temp_char)
    endif
    if (present(OrbitNumber)) then
      temp_char = ''
      status = nc4read_global_attr(this%id_nc, "orbit", temp_char)
      OrbitNumber = TRIM(temp_char)
    endif

    ! get processor version - MB 10/22/23
    if (present(processor_version)) then
      temp_char = ''
      status = nc4read_global_attr(this%id_nc, "processor_version", temp_char)
      processor_version = TRIM(temp_char)
    endif

    ! Get ID for all groups
    status = nf90_inq_grp_ncid(this%id_nc, "METADATA", meta_id)
    msg = 'Error in inquiring METADATA group'
    call check_nf90(status, msg)

    status = nf90_inq_grp_ncid(meta_id, "ECS_METADATA", ecs_id)
    msg = 'Error in inquiring ECS_METADATA group'
    call check_nf90(status, msg)

    status = nf90_inq_grp_ncid(ecs_id, "Inventory_Metadata", inv_met_id)
    msg = 'Error in inquiring METADATA group'
    call check_nf90(status, msg)

    if (present(RangeBegDateTime) .or. present(year) .or. present(day) .or. &
      present(month)) then
      temp_char = ''
      status = nc4read_global_attr(inv_met_id, "RangeBeginningDateTime", temp_char)
      status = get_date_time(TRIM(temp_char), &
        tyear, tmonth, tday, thour, tminute, tsec)
      if(present(year)) year = tyear
      if(present(month)) month = tmonth
      if(present(day)) day = tday
    endif

    if (present(RangeBegDateTime))  RangeBegDateTime = TRIM(temp_char)
    if (present(RangeEndDateTime)) then
      temp_char = ''
      status = nc4read_global_attr(inv_met_id, "RangeEndingDateTime", temp_char)
      RangeEndDateTime = TRIM(temp_char)
    endif
    if (present(EquatorCrossingDateTime)) then
      temp_char = ''
      status = nc4read_global_attr(inv_met_id, "EquatorCrossingDateTime", temp_char)
      EquatorCrossingDateTime = TRIM(temp_char)
    endif
    if (present(EquatorCrossingLongitude)) then
      temp_char = ''
      status = nc4read_global_attr(inv_met_id, "EquatorCrossingLongitude", temp_char)
      EquatorCrossingLongitude = TRIM(temp_char)
    endif

    status = OMI_S_SUCCESS
    END FUNCTION

    FUNCTION get_date_time(date_str, year, month, day, hour, minute, sec) &
                RESULT(status)
      implicit none
      character(len=*), intent(in) :: date_str
      integer(kind=4), intent(out) :: year, month, day
      integer(kind=4), intent(out) :: hour, minute, sec
      integer(kind=4) :: status
      integer(kind=4) :: ii,jj,pos1,pos2
      year = -999
      month = -999
      day = -999
      status = OMI_E_FAILURE
      read(date_str(1:4), '(I4)' )Year
      read(date_str(6:7), '(I2)' )Month
      read(date_str(9:10), '(I2)' )Day 
      read(date_str(12:13), '(I2)' )Hour
      read(date_str(15:16), '(I2)' )Minute
      read(date_str(18:19), '(I2)' )Sec 
      status = OMI_S_SUCCESS
      
    END FUNCTION

END MODULE L1B_Reader_class
