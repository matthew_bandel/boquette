#-------------------------------------------------------------------------------

APP Name:      OMI_L1B_Reader

APP Version:   4.1.10

APP Type:      MODULE

Description:   OMI L1B file reader.

Software Developer: B. Das (Adnet), J. Warner (SSAI)

Dependencies:

 - CI:            nc4_fort_utils
   Version:       0.0.8

Compilers:

 - CI:            cc
   Version:       3.4.6

 - CI:            pgf90
   Version:       6.0-4

 - CI:            ifort
   Version:       10.1

Environment:

 - CI:            env_setup
   Version:       0.9.5

 - CI:            PGSTK
   Version:       5.2.12v1.00-patch

Operating System:

 - CI:            Linux
   Version:       2.6.9

 - CI:            make
   Version:       3.80

#-------------------------------------------------------------------------------
