### OMTO3Qualifier

Produces QAQC Plots for OMTO3 data

### Usage

Run from the src directory as follows, where $sink is the address of the folder for dumping the data, and $year, $month, $day, $orbit are the year, month, and day and optional orbit number of the requested OMTO3 records.  If $orbit is left blank, the full day will be processed.  Plots will be saved in $sink/plots, and json records of outliers will be saved in $sink/records.

```buildoutcfg
OMTO3Qualifier/src $ python3 omto3_qualifier.py $sink $year $month $day $orbit
```

To retrieve a record, run from the src directory as follows, where $sink is the same directory as above, $orbit is the requested orbit number, $latitude is the approximate latitude for the outlier, and $keyword is an optional keyword.  Records will be retrieved within 10 degrees of the given latitude.  Only those records labeled by the requested keyword will be retrieved.

```buildoutcfg
OMTO3Qualifier/src $ python3 omto3_qualifier.py --ask $sink $orbit $latitude $keyword
```
