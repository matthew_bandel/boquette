#!/usr/bin/env python3

# omto3_qualifier.py for the OMTO3Qualifier class to build omi climatology

# import local classes
from hydras import Hydra

# import system
import sys
import os

# import numpy functions
import numpy

# import pretty printing
import pprint

# import counter
from collections import Counter

# import datetime
import datetime

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# imoprt cartopy
import cartopy

# import PIL for image manipulation
import PIL
from PyPDF2 import PdfFileReader, PdfFileWriter


# class Qualifier to do OMOCNUV analysis
class Qualifier(Hydra):
    """Qualifier class to perform QAQC on OMTO3 files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
        """

        # initialize the base Core instance
        Hydra.__init__(self)

        # set sink
        self.sink = sink

        # set yaml
        self.yam = {}
        self._configure()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < OMTO3Qualifier instance at {} >'.format(self.sink)

        return representation

    def _configure(self, codes='codes_V8.yaml'):
        """Load in quality flag yaml.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.yam
        """

        # load in yaml
        yam = self._acquire(codes)

        # set yam
        self.yam = yam

        return None

    def ask(self, orbit, latitude, label=None, number=None):
        """Ask about the details of an event.

        Arguments:
            sink: directory into which to dump report
            orbit: int, orbit number
            latitude: float, latitude
            label: str, key word of error label
            number: int, number of entries
            folder: folder for report

        Returns:
            None
        """

        # make folder
        self._make('{}/reports'.format(self.sink))

        # set default number
        number = number or 20

        # grab the file
        names = self._see('{}/records'.format(self.sink))
        names = [name for name in names if str(orbit) in name]

        # grab the records
        records = self._load(names[0])
        records = list(records.values())

        # if latitude is not all
        if latitude != 'all':

            # try to
            try:

                # get subset +/- 10 latitude
                records = [record for record in records if abs(record['Latitude'] - float(latitude)) < 10]

            # unless V9
            except KeyError:

                # in which case, try with lower case
                records = [record for record in records if abs(record['latitude'] - float(latitude)) < 10]

            # convert latitude to string
            latitude = self._orient(float(latitude))

        # if error label given
        label = label or 'all'
        if label != 'all':

            # only get those with error or extreme in key
            records = [record for record in records if label in record['error'] or label in record['extreme']]

        # number is marked as all
        if number == 'all':

            # set to len of records
            number = len(records)

        # sort by sigmas
        records.sort(key=lambda record: record['sigmas'], reverse=True)
        records = records[:number]

        # try to
        try:

            # create destination for file
            date = records[0]['date']
            formats = (self.sink, orbit, date, latitude, label)
            destination = '{}/reports/qaqc_records_o{}_{}_{}_{}.txt'.format(*formats)

            # pprint to file
            self._look(records, 10, destination=destination)

            # grab file as text
            text = self._know(destination)

            # replace single quotes with doubles and nan with NaN
            text = [line.replace("'", '"').replace('nan', 'NaN') for line in text]
            self._jot(text, destination)

            # print to screen after reverse
            records.reverse()
            for index, record in enumerate(records[-number:]):

                # print
                self._print('')
                self._print('record {} of {}'.format(index, len(records[-number:])))
                self._look(record, 3)

        # unless no record found
        except IndexError:

            # print alert
            self._print('no records found!')

        return None

    def compile(self):
        """Compile all records into one.

        Arguments:
            None

        Returns:
            None
        """

        # make compilation folder
        self._make('{}/compilation'.format(self.sink))

        # begin compilation
        compilation = {}
        destination = '{}/compilation/compilation.json'.format(self.sink)

        # grab the file
        names = self._see('{}/records'.format(self.sink))

        # for each name
        for index, name in enumerate(names):

            # grab the records
            records = self._load(name)

            # get orbit from first record
            keys = list(records.keys())
            orbit = records[keys[0]]['orbit']

            # add to compilation
            compilation[orbit] = records

            # every 50 records
            if (index % 1000 == 0):

                # save
                self._dump(compilation, destination)

        # final save
        self._dump(compilation, destination)

        return None

    def detail(self, items, fields, index, waves):
        """Create a detail dictionary for each array at the index.

        Arguments:
            items: list of str, array tuples
            fields: dictionary of abbreviation to full name mappinng
            index: int, the index of the sample
            waves: list of float, the wavelengths

        Returns:
            dict
        """

        # begin details
        details = {}

        # construct details
        for name, array in items:

            # check for 1-D array
            if len(array.shape) == 1:

                # make float of entry
                details[fields.get(name, name)] = float(array[index])

            # check for 2-D array
            if len(array.shape) > 1:

                # make float of all entries
                entries = [float(entry) for entry in array[index]]

                # create wavelength tuples
                tuples = [(self._round(wave, 1), entry) for wave, entry in zip(waves, entries)]

                # make float of all entries
                details[fields.get(name, name)] = tuples

            # check for quality flags
            if name == 'quality':

                # make float of all entries
                bits = self._bite(array[index])

                # separate into first and second bite
                first = [bit for bit in bits if bit < 3]
                second = [bit for bit in bits if bit > 2]

                # construct list
                combination = self._unite(*first)
                information = [bits]
                information += [self.yam['quality']['combo'][combination]]
                information += [self.yam['quality']['bit'][bit] for bit in second]

                # add list of information
                details[fields.get(name, name)] = information

            # check for ground flags
            if name == 'ground':

                # make float of all entries
                bits = self._bite(array[index])

                # costruct information
                information = [bits]
                information += [self.yam['ground'][bit] for bit in bits]

                # add information
                details[fields.get(name, name)] = information

            # check for algorithm flag
            if name == 'algorithm':

                # make float of all entries
                algorithm = int(array[index])

                # costruct information
                information = [algorithm]
                information += [self.yam['algorithm'][algorithm]]

                # add information
                details[fields.get(name, name)] = information

            # check for measurement flag
            if name == 'measurement':

                # make float of all entries
                measure = self._bite((array[index]))

                # costruct information
                information = [measure]
                information += [self.yam['measurement'][bit] for bit in measure]

                # add information
                details[fields.get(name, name)] = information

        return details

    def paginate(self, paths, destination):
        """Accumate a day's worth of plots into a single pdf.

        Arguments:
            paths: list of .png file names
            destination: str, filename

        Returns:
            None
        """

        # convert pngs to temporary pdf files
        ghosts = [path.replace('.png', '.pdf') for path in paths]

        # open all images and convert to arrays
        images = [PIL.Image.open(path).convert('RGBA') for path in paths]
        arrays = [numpy.array(image) for image in images]

        # save all arrays to temporary pdfs
        [PIL.Image.fromarray(array).convert('RGB').save(ghost) for array, ghost in zip(arrays, ghosts)]

        # initialize writer
        writer = PdfFileWriter()

        # go through ghosts
        for ghost in ghosts:

            # read in ghost
            reader = PdfFileReader(ghost)
            for page in range(reader.getNumPages()):

                # add to writer
                writer.addPage(reader.getPage(page))

        # Write out the merged PDF
        with open(destination, 'wb') as pointer:

            # write file
            writer.write(pointer)

        # remove ghost files
        for ghost in ghosts:

            # remove
            os.remove(ghost)

        return None

    def qualify(self, year, month, day, orbit=None, archive=96100, sigmas=4):
        """Produce qa qc plots for a day of OMTO3 orbits.

        Arguments:
            year: int, the year
            month: int, the month
            day: int, the day
            orbit: int, particular orbit
            archive: int, the archive set
            sigmas: float, number of stdev window

        Returns:
            None
        """

        # make folders for plots and json records
        self._make('{}/plots'.format(self.sink))
        self._make('{}/records'.format(self.sink))
        self._make('{}/pages'.format(self.sink))

        # make year and month files
        self._make('{}/pages/{}'.format(self.sink, year))
        self._make('{}/pages/{}/{}'.format(self.sink, year, self._pad(month)))

        # create hydra for folder of omto3 orbits
        formats = (archive, year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/{}/OMTO3/{}/{}/{}'.format(*formats))

        # gather paths
        paths = [path for path in hydra.paths if not path.endswith('.met')]

        # if given an orbit
        if orbit:

            # limit to this path
            paths = [path for path in paths if str(orbit) in path]

        # set up fields for data extraction ( V8 )
        fields = {'ozone': 'ColumnAmountO3', 'reflectivity': 'Reflectivity331', 'reflectivityii': 'Reflectivity360'}
        fields.update({'quality': 'QualityFlags', 'latitude': 'Latitude', 'algorithm': 'AlgorithmFlags'})
        fields.update({'longitude': 'Longitude', 'zenith': 'SolarZenithAngle'})
        fields.update({'ozonei': 'StepOneO3', 'ozoneii': 'StepTwoO3', 'cloud': 'CloudPressure'})
        fields.update({'ground': 'GroundPixelQualityFlags', 'reflectance': 'NValue', 'sulfur': 'SO2index'})
        fields.update({'residual': 'Residual', 'residuali': 'ResidualStep1', 'residualii': 'ResidualStep2'})
        fields.update({'measurement': 'MeasurementQualityFlags'})
        fields.update({'terrain': 'TerrainPressure'})

        # set units
        units = {'ozone': 'ozone ( DU )', 'reflectivity': 'reflectivity', 'reflectivityii': 'reflectivity'}
        units.update({'ozonei': 'ozone ( DU )', 'ozoneii': 'ozone ( DU )'})

        # set quality flag error codes
        errors = {'good_sample': 0, 'glint_corrected': 1, 'sza_over_84': 2}
        errors.update({'residual_360_threshold': 3, 'residual_unused_4_sigma': 4})
        errors.update({'soi_4_sigma': 5, 'nonconvergence': 6, 'abs_residual_over_16': 7})

        # set quality flag sigma limits for plotting
        sigma = {'good_sample': sigmas, 'glint_corrected': 0, 'sza_over_84': 0}
        sigma.update({'residual_360_threshold': 0, 'residual_unused_4_sigma': 0})
        sigma.update({'soi_4_sigma': 0, 'nonconvergence': 0, 'abs_residual_over_16': 0})

        # set hexadecimal color codes
        hexes = {'green': '#008000', 'yellow': '#ffff00', 'orange': '#ffa500', 'blue': '#0000ff'}
        hexes.update({'cyan': '#00ffff', 'magenta': '#ff00ff', 'orchid': '#da70d6', 'darkblue': '#00008b'})
        hexes.update({'black': '#000000', 'red': '#ff0000', 'crimson': '#dc143c', 'orangered': '#ff4500'})
        hexes.update({'pink': '#ffc0cb', 'violet': '#ee82ee', 'turquoise': '#40e0d0'})
        hexes.update({'lime': '#00ff00', 'chartreuse': '#7fff00', 'coral': '#ff7f50'})
        hexes.update({'gold': '#ffd700', 'goldenrod': '#daa520', 'lightblue': '#add8e6'})
        hexes.update({'purple': '#800080', 'skyblue': '#87ceeb', 'yellowgreen': '#9acd32'})

        # set palette depending on error code
        palette = {'good_sample': hexes['green'], 'glint_corrected': hexes['lime'], 'sza_over_84': hexes['yellowgreen']}
        palette.update({'residual_360_threshold': hexes['blue'], 'residual_unused_4_sigma': hexes['cyan']})
        palette.update({'soi_4_sigma': hexes['magenta'], 'nonconvergence': hexes['orange']})
        palette.update({'abs_residual_over_16': hexes['black']})
        palette.update({'center': hexes['black'], 'margin': hexes['black']})
        palette.update({'good_r331_r360_<0': hexes['skyblue'], 'good_r331_r360_>105': hexes['orange']})
        palette.update({'good_ozone_<100': hexes['cyan'], 'good_ozone_>600': hexes['magenta']})
        palette.update({'good_ozone_fill': hexes['lime']})
        palette.update({'ozone_r331_r360_nan': hexes['black']})
        palette.update({'good_ozone_>4sigma': hexes['green']})

        # set high reflectivity for alerts and wavelengths name
        high = 'r360'
        wavelengths = 'Wavelength'

        # check for version 9
        if paths[0].endswith('.nc'):

            # configure with V9
            self._configure('codes_V9.yaml')

            # set up secondary fields for data extraction ( V9 )
            fields = {'ozone': 'ColumnAmountO3', 'reflectivity': 'Reflectivity331', 'reflectivityii': 'Reflectivity354'}
            fields.update({'quality': 'QualityFlags', 'latitude': 'latitude', 'algorithm': 'AlgorithmFlags'})
            fields.update({'longitude': 'longitude', 'zenith': 'solar_zenith_angle'})
            fields.update({'ozonei': 'StepOneO3', 'ozoneii': 'StepTwoO3', 'cloud': 'CloudPressure'})
            fields.update({'ground': 'ground_pixel_quality', 'reflectance': 'NValue'})
            fields.update({'residual': 'Residual', 'residuali': 'ResidualStep1', 'residualii': 'ResidualStep2'})
            fields.update({'measurement': 'measurement_quality'})
            fields.update({'terrain': 'TerrainPressure'})

            # # set quality flag error codes
            # errors = {'good_sample': 0, 'glint_possible': 1, 'aerosol_over_threshold': 2}
            # errors.update({'residual_331_threshold': 3, 'nonconvergence': 4})
            # errors.update({'matrix_inversion': 5, 'abs_residual_over_16': 6})
            #
            # # set quality flag sigma limits for plotting
            # sigma = {'good_sample': sigmas, 'glint_possible': 0, 'aerosol_over_threshold': 0}
            # sigma.update({'residual_331_threshold': 0, 'nonconvergence': 0})
            # sigma.update({'matrix_inversion': 0, 'abs_residual_over_16': 0})

            # set quality flag error codes
            errors = {'good_sample': 0, 'glint_possible': 1, 'sza_over_88': 2}
            errors.update({'nonconvergence': 6})

            # set quality flag sigma limits for plotting
            sigma = {'good_sample': sigmas, 'glint_possible': 0, 'sza_over_88': 0}
            sigma.update({'nonconvergence': 0})

            # set palette depending on error code
            palette = {'good_sample': hexes['green'], 'glint_possible': hexes['lime']}
            palette.update({'sza_over_88': hexes['yellowgreen']})
            palette.update({'residual_331_threshold': hexes['blue'], 'nonconvergence': hexes['cyan']})
            palette.update({'matrix_inversion': hexes['magenta'], 'abs_residual_over_16': hexes['orange']})
            # palette.update({'abs_residual_over_16': hexes['black']})
            palette.update({'center': hexes['black'], 'margin': hexes['black']})
            palette.update({'good_r331_r354_<0': hexes['skyblue'], 'good_r331_r354_>105': hexes['orange']})
            palette.update({'good_ozone_<100': hexes['cyan'], 'good_ozone_>600': hexes['magenta']})
            palette.update({'good_ozone_fill': hexes['lime']})
            palette.update({'ozone_r331_r354_nan': hexes['black']})
            palette.update({'good_ozone_>4sigma': hexes['green']})

            # # set palette depending on error code
            # palette = {'good_sample': hexes['green'], 'glint_possible': hexes['lime']}
            # palette.update({'aerosol_over_threshold': hexes['yellowgreen']})
            # palette.update({'residual_331_threshold': hexes['blue'], 'nonconvergence': hexes['cyan']})
            # palette.update({'matrix_inversion': hexes['magenta'], 'abs_residual_over_16': hexes['orange']})
            # # palette.update({'abs_residual_over_16': hexes['black']})
            # palette.update({'center': hexes['black'], 'margin': hexes['black']})
            # palette.update({'good_r331_r354_<0': hexes['skyblue'], 'good_r331_r354_>105': hexes['orange']})
            # palette.update({'good_ozone_<100': hexes['cyan'], 'good_ozone_>600': hexes['magenta']})
            # palette.update({'good_ozone_fill': hexes['lime']})
            # palette.update({'ozone_r331_r354_nan': hexes['black']})
            # palette.update({'good_ozone_>4sigma': hexes['green']})

            # set high reflectivity and wavelengths name
            high = 'r354'
            wavelengths = 'wavelength'

        # for each metric, step one first so records reflect final
        metrics = ('ozonei', 'ozoneii', 'ozone')
        stubs = ('StepOneO3', 'StepTwoO3', 'ColumnO3')
        for metric, stub in zip(metrics, stubs):

            # begin pages
            pages = []

            # for each path
            for path in paths:

                # get details
                orbit = self._stage(path)['orbit']
                date = self._stage(path)['day']

                # begin outlier record
                record = {}

                # ingest and grab data
                hydra.ingest(path)
                original = {field: hydra.grab(search) for field, search in fields.items()}

                # grab wavelengths
                waves = hydra.grab(wavelengths, 0)

                # expand measurement quality
                measure = original['measurement']
                original['measurement'] = numpy.array([measure] * 60).transpose(1, 0)

                # add row and column arrays to originals
                shape = original['latitude'].shape
                scans = numpy.array([numpy.linspace(0, shape[0] - 1, shape[0])] * shape[1]).transpose(1, 0)
                rows = numpy.array([numpy.linspace(0, shape[1] - 1, shape[1])] * shape[0])
                original['scanline'] = scans.astype(int)
                original['row'] = rows.astype(int)

                # weed out row anomaly, nans, and fill values
                mask = ((original['quality'] & 64) == 0) & (abs(original[metric]) < 1e20)
                mask = mask & (numpy.isfinite(original[metric]))
                data = {field: array[mask] for field, array in original.items()}

                # create ascending and descending masks
                quality = data['quality'] & 7
                ascending = {error: (quality < 8) & (quality == code) for error, code in errors.items()}
                descending = {error: (quality >= 8) & (quality == code) for error, code in errors.items()}

                # if version is V9
                if path.endswith('.nc'):

                    # # create ascending and descending masks
                    # quality = data['quality'] & 15
                    # direction = data['quality'] & 16
                    # ascending = {error: (direction == 0) & (quality == code) for error, code in errors.items()}
                    # descending = {error: (direction == 16) & (quality == code) for error, code in errors.items()}

                    # create ascending and descending masks
                    quality = data['quality'] & 7
                    direction = data['quality'] & 8
                    ascending = {error: (direction == 0) & (quality == code) for error, code in errors.items()}
                    descending = {error: (direction == 8) & (quality == code) for error, code in errors.items()}

                # set mask for row anomaly
                anomaly = ((original['quality'] & 64) == 0)

                # set quality for extremes
                qualityii = original['quality'] & 15

                # set alarm conditions for ozone and reflectivity out or range events
                alarms = {}
                condition = (original['reflectivity'] < 0) | (original['reflectivityii'] < 0)
                condition = condition & (abs(original[metric]) < 1e20)
                alarms.update({'r331_{}_<0'.format(high): condition})
                condition = (original['reflectivity'] > 105) | (original['reflectivityii'] > 105)
                alarms.update({'r331_{}_>105'.format(high): condition})
                alarms.update({'ozone_<100': (original[metric] < 100) & (abs(original[metric]) < 1e20)})
                alarms.update({'ozone_>600': (original[metric] > 600) & (abs(original[metric]) < 1e20)})
                alarms.update({'ozone_fill': (abs(original[metric]) > 1e20) & ((original['algorithm'] % 10) > 0)})

                # begin extremes with good ozone samples > 4 sigmas
                extremes = {}
                extremes.update({'good_ozone_>4sigma': numpy.zeros(original[metric].shape).astype(bool)})

                # add good and flagged versions of alarms to extremes
                finite = numpy.isfinite(original[metric])
                # extremes.update({'good_{}'.format(name): mask & (qualityii < 2) for name, mask in alarms.items()})
                extremes.update({'good_{}'.format(name): mask & finite for name, mask in alarms.items()})

                # set nan masks and add to extremes
                infinite = numpy.zeros(original[metric].shape).astype(bool)
                infinite = infinite | numpy.logical_not(numpy.isfinite(original['reflectivity']))
                infinite = infinite | numpy.logical_not(numpy.isfinite(original['reflectivityii']))
                infinite = infinite | numpy.logical_not(numpy.isfinite(original[metric]))
                extremes['ozone_r331_{}_nan'.format(high)] = infinite

                # set statistical mode to plot absolute amounts or relative to mean and / or stdev
                modes = {'absolute': lambda array, mean: array}
                modes.update({'relative': lambda array, mean: array - mean})
                brackets = {'absolute': lambda mean, deviation: deviation}
                brackets.update({'relative': lambda mean, deviation: deviation})
                bases = {'absolute': lambda mean: mean, 'relative': lambda mean: 0, 'ratio': lambda mean: 1}
                limits = {'absolute': (100, 650), 'relative': (-100, 100), 'ratio': (0.5, 1.5)}
                heights = {'absolute': 640, 'relative': 95, 'ratio': 95}

                # set plot widths according to statistical mode
                widths = {'absolute': 8, 'relative': 4, 'ratio': 4, 'extreme': 8, 'legend': 4}

                # for each field of ozone
                for field in (metric,):

                    # set dependent axis labels
                    dependents = {'absolute': units[field], 'relative': '', 'ratio': ''}

                    # set titles
                    formats = (fields[field], date, orbit, archive, sigmas)
                    titles = {'absolute': '{}, {}, o{}, AS{}, +/- {} sigma'.format(*formats)}
                    titles.update({'relative': '+/- {} sigma'.format(sigmas)})
                    titles.update({'ratio': '+/- {} sigma / mean'.format(sigmas)})

                    # begin list of plot destinations
                    destinations = []

                    # begin set of counts for adding to legend
                    counts = {}

                    # for each statisitcal mode
                    for mode in modes.keys():

                        # create a destination file name
                        formats = (self.sink, fields[field], date, orbit, mode)
                        destination = '{}/plots/OMTO3_QAQC_{}_{}_o{}_{}.png'.format(*formats)
                        destinations.append(destination)

                        # begin plot
                        pyplot.clf()
                        pyplot.figure(figsize=(widths[mode], 5))
                        pyplot.margins(0.3, 1.0)
                        pyplot.title(titles[mode])
                        pyplot.xlabel('latitude')
                        pyplot.ylabel(dependents[mode])
                        pyplot.xlim(-90, 90)
                        pyplot.ylim(*limits[mode])

                        # set marker size
                        size = 4

                        # create labels, masks, and colors
                        masques = list(descending.values()) + list(ascending.values())
                        labels = ['descending_{}'.format(error) for error in errors.keys()]
                        labels += ['ascending_{}'.format(error) for error in errors.keys()]
                        colors = [palette[error] for error in errors.keys()] * 2
                        markers = ['o'] * len(errors) + ['D'] * len(errors)

                        # revise abscissa and ordinate for good ascending data only
                        abscissa = data['latitude'][ascending['good_sample']]
                        ordinate = data[field][ascending['good_sample']]

                        # create orbital zonal means
                        half = 5
                        width = 10
                        points = [90 - (width + index * width - half) for index in range(18)]
                        arrays = [ordinate[(abscissa >= point - half) & (abscissa <= point + half)] for point in points]
                        means = [array.mean() for array in arrays]
                        deviations = [array.std() for array in arrays]

                        # for each mean
                        for point, mean, deviation in zip(points, means, deviations):

                            # for each error code
                            for masque, color, marker, label in zip(masques, colors, markers, labels):

                                # grab masked abscissa and ordinate
                                abscissaii = data['latitude'][masque]
                                ordinateii = data[field][masque]

                                # generate latitude zone from abscissa
                                zone = (abscissaii >= point - half) & (abscissaii <= point + half)

                                # shift according to mode
                                shift = modes[mode](ordinateii[zone], mean)

                                # plot data
                                parameters = {'color': color, 'label': label, 'markersize': size, 'marker': marker}
                                parameters.update({'linestyle': "None"})
                                pyplot.plot(abscissaii[zone], shift, **parameters)

                                # add to counts
                                counts[label] = counts.get(label, 0) + ordinateii[zone].shape[0]

                                # if in absolute mode
                                if mode == 'absolute':

                                    # get set of points greater than four sigma
                                    base = '_'.join(label.split('_')[1:])
                                    scores = abs((ordinateii[zone] - mean) / deviation)
                                    outside = (scores > sigma[base])

                                    # get info about outsiders
                                    outsiders = {name: array[masque][zone][outside] for name, array in data.items()}

                                    # for each index
                                    for index, score in enumerate(scores[outside]):

                                        # if good smaples
                                        if 'ascending_good' in label:

                                            # recover pixel
                                            scan = outsiders['scanline'][index]
                                            row = outsiders['row'][index]

                                            # add index to extremes mask
                                            extremes['good_ozone_>4sigma'][scan, row] = True

                                        # get pixel
                                        pixel = (int(outsiders['scanline'][index]), int(outsiders['row'][index]))

                                        # add to record
                                        items = outsiders.items()

                                        # add other details, default extreme to ''
                                        details = self.detail(items, fields, index, waves)
                                        details.update({'sigmas': float(score), 'error': label})
                                        details.update({'date': date, 'orbit': orbit})
                                        details.update({'extreme': ''})
                                        record.update({str(pixel): details})

                            # set horizontal extent of sigma margin lines
                            horizontal = [point - half, point + half]

                            # for each margin
                            margins = (sigmas, 0, -sigmas)
                            lines = ('margin', 'center', 'margin')
                            for margin, line in zip(margins, lines):

                                # plot the line
                                vertical = [bases[mode](mean) + margin * brackets[mode](mean, deviation)] * 2
                                parameters = {'color': palette[line], 'linestyle': 'solid'}
                                pyplot.plot(horizontal, vertical, **parameters)

                        # for each extreme mask
                        for name, mask in extremes.items():

                            # create latitude and longitude
                            longitude = original['longitude'][mask & anomaly]
                            latitude = original['latitude'][mask & anomaly]

                            # replace longitude with height near the top of the plot
                            height = numpy.where(longitude, heights[mode], heights[mode])

                            # plot the marker
                            size = 7
                            marker = 'x'
                            parameters = {'color': palette[name], 'label': name, 'markersize': size}
                            parameters.update({'marker': marker, 'linestyle': "None"})
                            pyplot.plot(latitude, height, **parameters)

                            # add to counts
                            counts[name] = counts.get(name, 0) + height.shape[0]

                            # get info about out of range outsiders
                            outsiders = {name: array[mask & anomaly] for name, array in original.items()}

                            # for each index
                            for index, _ in enumerate(outsiders['scanline']):

                                # get pixel
                                pixel = (int(outsiders['scanline'][index]), int(outsiders['row'][index]))

                                # add to record
                                items = outsiders.items()

                                # # only add to details if not already added for 4 sigma
                                # if name != 'good_ozone_>4sigma':

                                # grab prior record if there is one
                                prior = record.get(str(pixel), {})

                                # add other details
                                details = self.detail(items, fields, index, waves)
                                details.update({'sigmas': prior.get('sigmas', 0), 'extreme': name})
                                details.update({'date': date, 'orbit': orbit})
                                details.update({'error': prior.get('error', '')})
                                record.update({str(pixel): details})

                        # save figure and clear
                        self._print('saving {}...'.format(destination))
                        pyplot.savefig(destination)
                        pyplot.clf()

                    # create destination for out of range map plot
                    formats = (self.sink, fields[field], date, orbit)
                    destination = '{}/plots/OMTO3_QAQC_{}_{}_o{}_extreme.png'.format(*formats)
                    destinations.append(destination)

                    # begin map
                    pyplot.clf()
                    pyplot.figure(figsize=(widths['extreme'], 5))
                    pyplot.margins(0.5, 1.0)

                    # set cartopy axis with coastlines
                    axis = pyplot.axes(projection=cartopy.crs.PlateCarree())
                    axis.coastlines()
                    _ = axis.gridlines(draw_labels=True)
                    axis.set_aspect('auto')
                    axis.set_global()

                    # for each extreme mask
                    for name, mask in extremes.items():

                        # create latitude and longitude
                        longitude = original['longitude'][mask & anomaly]
                        latitude = original['latitude'][mask & anomaly]

                        # plot the marker
                        size = 7
                        marker = 'x'
                        parameters = {'color': palette[name], 'label': name, 'markersize': size}
                        parameters.update({'marker': marker, 'linestyle': "None"})
                        pyplot.plot(longitude, latitude, **parameters)

                    # save figure and clear
                    self._print('saving {}...'.format(destination))
                    pyplot.savefig(destination)
                    pyplot.clf()

                    # for each legend
                    for legend, reservoir in zip(('legend',), (ascending,)):

                        # set destination for plotting legend
                        formats = (self.sink, fields[field], date, orbit, legend)
                        destination = '{}/plots/OMTO3_QAQC_{}_{}_o{}_{}.png'.format(*formats)
                        destinations.append(destination)

                        # begin legend plot
                        pyplot.clf()
                        pyplot.figure(figsize=(widths['legend'], 5))
                        pyplot.margins(0.2, 0.2)
                        pyplot.xlim(-1, 1)
                        pyplot.ylim(-1, 1)
                        pyplot.gca().get_xaxis().set_visible(False)
                        pyplot.gca().get_yaxis().set_visible(False)

                        # for each entry
                        for name in reservoir.keys():

                            # construct label, dividing by 2 because of the two statitical modes
                            count = counts['ascending_{}'.format(name)] + counts['descending_{}'.format(name)]
                            label = name.replace('_', ' ') + ' ( {} )'.format(int(count / len(modes.keys())))
                            parameters = {'color': palette[name], 'label': label, 'marker': 'D'}
                            parameters.update({'linestyle': "None"})
                            pyplot.plot([100, 100], [100, 100], **parameters)

                        # for each entry
                        for name in extremes.keys():

                            # add out of range to legend
                            count = int(counts[name] / len(modes.keys()))
                            label = name.replace('_', ' ') + ' ( {} )'.format(count)
                            parameters = {'color': palette[name], 'label': label, 'marker': 'X'}
                            parameters.update({'linestyle': "None"})
                            pyplot.plot([100, 100], [100, 100], **parameters)

                        # add legend
                        pyplot.legend(loc='upper left', prop={'size': 11})

                        # save figure and clear
                        self._print('saving {}...'.format(destination))
                        pyplot.savefig(destination)
                        pyplot.clf()

                    # create destination for combined plots
                    destinationii = destinations[0].replace('absolute', 'quartet')

                    # for each plot
                    arrays = []
                    for destination in destinations:

                        # create an array from the image
                        image = PIL.Image.open(destination)
                        array = numpy.array(image)
                        arrays.append(array)

                    # combine arrays into top and bottom
                    top = numpy.hstack([arrays[0], arrays[1]])
                    bottom = numpy.hstack([arrays[3], arrays[2]])

                    # combine top and bottom
                    quartet = numpy.vstack([top, bottom])

                    # save to destination
                    self._print('saving {}...'.format(destinationii))
                    PIL.Image.fromarray(quartet).save(destinationii)

                    # append destiation to pages
                    pages.append(destinationii)

                    # for each singlet
                    for destination in destinations:

                        # erase
                        self._clean(destination, force=True)

                # store json record
                destination = '{}/records/OMTO3_QAQC_Points_{}.json'.format(self.sink, self._pad(orbit, 6))
                self._dump(record, destination)

            # create pdf from pages
            formats = [self.sink, year, self._pad(month), stub, archive]
            formats += [year, self._pad(month), self._pad(day)]
            destinationiii = '{}/pages/{}/{}/OMTO3_QAQC_{}_{}_{}m{}{}.pdf'.format(*formats)
            self._print('stitching pngs into {}...'.format(destinationiii))
            self.paginate(pages, destinationiii)

        return

    def sample(self, year, percent=3, divisions=4):
        """Sample omto3 records at random days in the year.

        Arguments:
            year: int, year
            percent: int, percent of data
            divisions: int, number of equal divisions

        Returns:
            None
        """

        # get percentage of days
        percentage = (percent / 100) * 365

        # divivde by divisions
        days = int(numpy.ceil(percentage / divisions))

        # make day blocks
        blocks = [int(index * 365 / divisions) for index in range(5)]
        brackets = [(one, two) for one, two in zip(blocks[:-1], blocks[1:])]

        # for each division
        for bracket in brackets:

            # and each day
            for day in range(days):

                # get random day
                random = int(bracket[0] + numpy.ceil(numpy.random.rand() * (bracket[1] - bracket[0])))

                # determine date
                date = datetime.datetime(year=year, month=1, day=1) + datetime.timedelta(days=random)

                # run qaqc
                self.qualify(date.year, date.month, date.day)

        return None

    def summarize(self, label, association, number=None):
        """Ask about the details of an event.

        Arguments:
            sink: directory into which to dump report
            label: error label
            association: associated attributes

        Returns:
            None
        """

        # make folder
        self._make('{}/reports'.format(self.sink))

        # grab the file
        names = self._see('{}/records'.format(self.sink))

        # set default number and subset
        number = int(number or len(names))
        names = names[:number]

        # begin compilation
        counts = 0
        collection = {}

        # for each name
        for index, name in enumerate(names):

            # print aler
            self._print('scanning {} of {}...'.format(index, len(names)))

            # grab the records
            records = self._load(name)
            records = list(records.values())

            # only get those with error or extreme in key
            records = [record for record in records if label in record['error'] or label in record['extreme']]

            # add to counts
            counts += len(records)

            # add to collection
            counter = Counter([str(record.get(association, 'not recorded')) for record in records])

            # graba all keys, new and old
            keys = list(counter.keys()) + list(collection.keys())
            collection = {key: collection.get(key, 0) + counter.get(key, 0) for key in keys}

            # # create counter
            # collection.update(counter)

        # begin report
        report = [self._print('QAQC summary, {}, {}:'.format(label, association))]

        # print summary
        report.append(self._print(label))
        report.append(self._print('{} orbits'.format(len(names))))
        report.append(self._print('{} occurences'.format(counts)))
        report.append(self._print('{} occurences / orbit'.format(round(counts / len(names), 5))))
        report.append(self._print(''))

        # for the association
        # collection = [record.get(association, 'not recorded') for record in records]
        counter = list(collection.items())
        counter.sort(key=lambda pair: pair[1], reverse=True)
        for quantity, count in counter:

            # print to screen
            report.append(self._print('{}: {}'.format(count, quantity)))

        # create destination for file
        formats = (self.sink, label, association)
        destination = '{}/reports/qaqc_summary_{}_{}.txt'.format(*formats)
        self._jot(report, destination)

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('--')]
    arguments = [argument for argument in arguments if not argument.startswith('--')]

    # pad arguments
    number = 6
    arguments += [''] * number
    arguments = arguments[:number]

    # chedk for ask option
    if '--ask' in options:

        # unpack arguments
        sink, orbit, latitude, word, number, _ = arguments

        # create qualifier instance
        qualifier = Qualifier(sink)

        # run qualifier
        qualifier.ask(orbit, latitude, word, number)

    # chedk for summarize option
    elif '--summarize' in options:

        # unpack arguments
        sink, label, association, number, _, _ = arguments

        # create qualifier instance
        qualifier = Qualifier(sink)

        # run qualifier
        qualifier.summarize(label, association, number)

    # chedk for summarize option
    elif '--compile' in options:

        # unpack arguments
        sink, _, _, _, _, _ = arguments

        # create qualifier instance
        qualifier = Qualifier(sink)

        # run qualifier
        qualifier.compile()

    # otherwise
    else:

        # unpack arguments
        sink, year, month, day, archive, orbit = arguments

        # create qualifier instance
        qualifier = Qualifier(sink)

        # run qualifier
        qualifier.qualify(year, month, day, orbit, archive)
