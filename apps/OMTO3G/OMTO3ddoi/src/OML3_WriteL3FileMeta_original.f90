!===============================================================================
!
! SUBROUTINE OML3_WriteL3FileMeta
!
! High Level Overview:
!   This subroutine is called by OML3_Main to write the file-level Metadata to
!   the OMI L3 product file.
!
! All Twelve Arguments Are Inputs:
!   l3_fileid          - L3 file ID.
!   l2_norb            - Number of L2 orbits in each L2G file.
!   l2_orb             - Array of L2 orbit numbers.
!   l2_orbper          - Array of L2 orbital periods.
!   l3_month           - Month of L3 file.
!   l3_day             - Day of L3 file.
!   l3_year            - Year of L3 file.
!   l3_dyofyr          - Day of year of L3 file.
!   l3_TAI93at0z       - TAI93 at 0z of L3 file.
!   l3_pgevers         - L3 PGE version.
!   l3_tstart          - TAI93 at start of L3 file.
!   l3_tend            - TAI93 at end of L3 file.
!
! OML3 Subroutine Called:
!   OML3_EndInFailure  - Ends OML3 execution in failure.
!
! PGS Toolkit Function Called:
!   pgs_td_taitoutc    - Converts TAI93 to UTC ascii string.
!
! HDF-EOS 5 Function Called:
!   he5_ehwrglattr     - Writes global attribute to file.
!
! Author(s):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Initial Version:
!   September 19, 2008
!
! Revision History:
!
!===============================================================================
 
SUBROUTINE OML3_WriteL3FileMeta(l3_fileid, &
                                l2_norb, l2_orb, l2_orbper, &
                                l3_month, l3_day, l3_year, l3_dyofyr, &
                                l3_TAI93at0z, l3_pgevers, &
                                l3_tstart, l3_tend)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'PGS_TD_3.f'               ! Some Toolkit parameters.
  INCLUDE 'hdfeos5.inc'              ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l3_fileid          ! L3 file ID.
  INTEGER (KIND=4), &
    DIMENSION(3), &
    INTENT(IN) :: l2_norb            ! Number of L2 orbits in each L2G file.
  INTEGER (KIND=4), &
    DIMENSION(20,3), &
    INTENT(IN) :: l2_orb             ! Array of L2 orbit numbers.
  REAL (KIND=8), &
    DIMENSION(20,3), &
    INTENT(IN) :: l2_orbper          ! Array of L2 orbital periods.
  INTEGER (KIND=4), &
    INTENT(IN) :: l3_month           ! Month of L3 file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l3_day             ! Day of L3 file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l3_year            ! Year of L3 file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l3_dyofyr          ! Day of year of L3 file.
  REAL (KIND=8), &
    INTENT(IN) :: l3_TAI93at0z       ! TAI93 at 0z of L3 file.
  CHARACTER (LEN=20), &
    INTENT(IN) :: l3_pgevers         ! L3 PGE version.
  REAL (KIND=8), &
    INTENT(IN) :: l3_tstart          ! TAI93 at start of L3 file.
  REAL (KIND=8), &
    INTENT(IN) :: l3_tend            ! TAI93 at end of L3 file.
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=8) :: count          ! Number of values in global attribute.
  INTEGER (KIND=4) :: iorbit         ! Orbit counter.
  INTEGER (KIND=4) :: iorbit2        ! Second orbit counter.
  INTEGER (KIND=4), &
    DIMENSION(60) :: l2_ovect        ! Vector of L2 orbit numbers.
  REAL (KIND=8), &
    DIMENSION(60) :: l2_opvect       ! Vector of orbital periods of L2 granules.
  INTEGER (KIND=4) :: count2         ! Orbit count from previous L2G file(s).
  CHARACTER (LEN=256) :: dummy       ! Dummy string variable.
  CHARACTER (LEN=28) :: l3_startUTC  ! UTC at start of L3 file.
  CHARACTER (LEN=28) :: l3_endUTC    ! UTC at end of L3 file.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat            ! Function call status.
  CHARACTER (LEN=256) :: stat_msg          ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML3_WriteL3FileMeta.f90"  ! Code file reference.
 
!===============================================================================
! Declaration of PGS Toolkit function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: pgs_td_taitoutc      ! Converts TAI93 to UTC ascii string.
 
!===============================================================================
! Declaration of HDF-EOS 5 function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_ehwrglatt        ! Writes global attribute to file.
 
!===============================================================================
! Create orbit number and orbital period vectors.
!===============================================================================
 
!===============================================================================
! Consider orbits in first L2G file.
!===============================================================================
  count = 0
 
  IF (l2_norb(1) .GT. 0) THEN
 
    DO iorbit = 1, l2_norb(1)
      count = count + 1
      l2_ovect(count) = l2_orb(iorbit,1)
      l2_opvect(count) = l2_orbper(iorbit,1)
    END DO
 
  END IF ! End of "IF (l2_norb(1) .GT. 0)" test.
 
!===============================================================================
! Consider orbits in second L2G file.
!===============================================================================
  IF (l2_norb(2) .GT. 0) THEN
 
    count2 = count
 
    IF (count2 == 0) THEN
      DO iorbit = 1, l2_norb(2)
        count = count + 1
        l2_ovect(count) = l2_orb(iorbit,2)
        l2_opvect(count) = l2_orbper(iorbit,2)
      END DO
    END IF
 
    IF (count2 .GT. 0) THEN
      Orbit_LoopA: DO iorbit = 1, l2_norb(2)
        DO iorbit2 = 1, count
          IF(l2_ovect(iorbit2) == l2_orb(iorbit,2)) CYCLE Orbit_LoopA
        END DO
        count = count + 1
        l2_ovect(count) = l2_orb(iorbit,2)
        l2_opvect(count) = l2_orbper(iorbit,2)
      END DO Orbit_LoopA
    END IF
 
  END IF ! End of "IF (l2_norb(2) .GT. 0)" test.
 
!===============================================================================
! Consider orbits in third L2G file.
!===============================================================================
  IF (l2_norb(3) .GT. 0) THEN
 
    count2 = count
 
    IF (count2 == 0) THEN
      DO iorbit = 1, l2_norb(3)
        count = count + 1
        l2_ovect(count) = l2_orb(iorbit,3)
        l2_opvect(count) = l2_orbper(iorbit,3)
      END DO
    END IF
 
    IF (count2 .GT. 0) THEN
      Orbit_LoopB: DO iorbit = 1, l2_norb(3)
        DO iorbit2 = 1, count
          IF(l2_ovect(iorbit2) == l2_orb(iorbit,3)) CYCLE Orbit_LoopB
        END DO
        count = count + 1
        l2_ovect(count) = l2_orb(iorbit,3)
        l2_opvect(count) = l2_orbper(iorbit,3)
      END DO Orbit_LoopB
    END IF
 
  END IF ! End of "IF (l2_norb(3) .GT. 0)" test.
 
  IF (count == 0) GO TO 1
 
!===============================================================================
! Write file-level Metadata to L3 file:
!===============================================================================
 
!===============================================================================
! Write L2 orbit numbers to L3 file.
!===============================================================================
  call_stat = he5_ehwrglatt(l3_fileid, "OrbitNumber", &
                            HE5T_NATIVE_INT, count, &
                            l2_ovect)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write L2 orbital periods to L3 file.
!===============================================================================
  call_stat = he5_ehwrglatt(l3_fileid, "OrbitPeriod", &
                            HE5T_NATIVE_DOUBLE, count, &
                            l2_opvect)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write instrument name to L3 file.
!===============================================================================
  dummy = "OMI"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l3_fileid, "InstrumentName", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write process level to L3 file.
!===============================================================================
  dummy = "3"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l3_fileid, "ProcessLevel", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write granule month to L3 file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l3_fileid, "GranuleMonth", &
                            HE5T_NATIVE_INT, count, &
                            l3_month)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write granule day to L3 file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l3_fileid, "GranuleDay", &
                            HE5T_NATIVE_INT, count, &
                            l3_day)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write granule year to L3 file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l3_fileid, "GranuleYear", &
                            HE5T_NATIVE_INT, count, &
                            l3_year)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write granule day of year to L3 file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l3_fileid, "GranuleDayOfYear", &
                            HE5T_NATIVE_INT, count, &
                            l3_dyofyr)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write TAI93 at 0z of granule to L3 file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l3_fileid, "TAI93At0zOfGranule", &
                            HE5T_NATIVE_DOUBLE, count, &
                            l3_TAI93at0z)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write PGE version to L3 file.
!===============================================================================
  count = LEN(TRIM(l3_pgevers))
  call_stat = he5_ehwrglatt(l3_fileid, "PGEVersion", &
                            HE5T_NATIVE_CHAR, count, &
                            l3_pgevers)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Convert start TAI93 of granule to UTC.
!===============================================================================
  call_stat = pgs_td_taitoutc(l3_tstart, l3_startUTC)
  IF (call_stat .NE. 0) GO TO 7
 
!===============================================================================
! Write start UTC of granule to L3 file.
!===============================================================================
  count = LEN(TRIM(l3_startUTC))
  call_stat = he5_ehwrglatt(l3_fileid, "StartUTC", &
                            HE5T_NATIVE_CHAR, count, &
                            l3_startUTC)
  IF (call_stat < 0) GO TO 4

!===============================================================================
! Convert end TAI93 of granule to UTC.
!===============================================================================
  call_stat = pgs_td_taitoutc(l3_tend, l3_endUTC)
  IF (call_stat .NE. 0) GO TO 7
 
!===============================================================================
! Write end UTC of granule to L3 file.
!===============================================================================
  count = LEN(TRIM(l3_endUTC))
  call_stat = he5_ehwrglatt(l3_fileid, "EndUTC", &
                            HE5T_NATIVE_CHAR, count, &
                            l3_endUTC)
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Write period of granule to L3 file.
!===============================================================================
  dummy = "Daily"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l3_fileid, "Period", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 4
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN

!===============================================================================
! This part of subroutine is reached only if L2G input files do not contain any
! L2 orbits, and so end execution with fatal error.
!===============================================================================
1 WRITE(stat_msg, 2) call_stat
2 FORMAT("Fatal error:  L2G input files do not contain any L2 orbits!")
  CALL OML3_EndInFailure(stat_msg, code_ref)

!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_ehwrglatt, and so end execution with fatal error.
!===============================================================================
4 WRITE(stat_msg, 5) call_stat
5 FORMAT("Fatal error while writing file-level Metadatum:", &
         "  he5_ehwrglatt call status =",i10,"!")
  CALL OML3_EndInFailure(stat_msg, code_ref)

!===============================================================================
! This part of subroutine is reached only if function call status is not equal
! to zero for a call to pgs_td_taitoutc, and so end execution with fatal error.
!===============================================================================
7 WRITE(stat_msg, 8) call_stat
8 FORMAT("Fatal error while converting TAI93 to UTC:", &
         "  pgs_td_taitoutc call status =",i10,"!")
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML3_WriteL3FileMeta
