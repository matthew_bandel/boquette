!===============================================================================
!
! README for OMTO3e (OMI Daily L3e for OMTO3)
!
! Peter J.T. Leonard (ADNET) - 06/02/09
!
! OMTO3e High Level Overview:
!   This is the main program for the OMI (Ozone Monitoring Instrument) OMTO3e
!   Product Generation Executive (PGE).  The OMTO3e PGE creates the OMTO3e data
!   product, which is the daily 0.25-degree by 0.25-degree Level 3e (L3e) total
!   column ozone product of the U.S. OMI Science Team.  The "e" at the end of
!   "OMTO3e" represents "expanded".
!
!   The OMTO3e PGE creates a (Total Ozone Mapping Spectrometer) TOMS-like daily
!   L3e gridded data product file from (as many as) three consecutive OMTO3G
!   daily Level 2G (L2G) gridded data product files, where each OMTO3G file
!   contains 24 consecutive UTC hours of OMTO3 orbital Level 2 (L2) swath data
!   subsetted onto a 0.25-degree by 0.25-degree grid in longitude and latitude.
!
!   A TOMS L3 day is defined as the ensemble of all L2 ground pixels with pixel
!   centers that have the same local calendar date on the ground.  There are two
!   reasons behind such a definition.  First, a TOMS L3 day provides complete
!   coverage of Earth, since every point on Earth (outside of polar night)
!   experiences daylight on each calendar date (in comparison, 24 consecutive
!   UTC hours of OMI observations do not completely cover Earth).  Second, the
!   TOMS L3 day puts the discontinuity (i.e., where the L2 observations within a
!   given day differ by almost 24 hours) at +/-180 degrees longitude, and, thus,
!   the discontinuity can be placed undistractingly along the extreme left and
!   right edges of several commonly used map projections.
!
!   The calendar date of the TOMS L3 day is the calendar date at Greenwich
!   midway through the TOMS L3 day, and is specified via the L3 day of year
!   parameter in the PCF (Process Control File) of the OMTO3e PGE.  Note
!   that some of the L2 observations at the beginning of a TOMS L3 day will
!   correspond to the previous calendar date at Greenwich, and some of the L2
!   observations at the end of a TOMS L3 day will correspond to the next
!   calendar date at Greenwich.  Consequently, data from three consecutive OMI
!   L2G files are required to fully populate the L3 grid at all longitudes for
!   any given TOMS L3 day.
!
!   The OMTO3e PGE was developed for Dr. Mark R. Schoeberl (NASA/GSFC), and is
!   based upon the TOMS Level 3 Gridded Software.  The latter was developed over
!   a period of many years by several people:  W. Byerly, D. Cao, E. Celarier,
!   Q. Choung, S. Huang, B. Irby, D. Lee, L. Liu, R. McPeters, L. Moy, M. Peng,
!   L. Phung, B. Raines, C. Seftor, and, especially, C. Wellemeyer.
!
! Adopted OMTO3e Grid:
!   The adopted L3 grid is a 0.25-degree by 0.25-degree grid in longitude and
!   latitude.  The dimensions of the grid are 1440 by 720.  The center of the
!   first grid cell is located at longitude -179.875 and latitude -89.875.  The
!   center of the final grid cell is located at longitude 179.875 and latitude
!   89.875.  The center of the grid itself is located at longitude 0.0 and
!   latitude 0.0, and corresponds to the corners of four grid cells.
!
!   The grid and format of the OMTO3e ASCII product files are consistent with
!   NASA document number NASA/TM-2000-209896 entitled "Total Ozone Mapping
!   Spectrometer (TOMS) Level-3 Data Products User's Guide" by R. McPeters,
!   P.K. Bhartia, A. Krueger, J. Herman, C. Wellemeyer, C. Seftor, W. Byerly
!   and E.A. Celarier.
!
!   The adopted grid for the OMTO3e HDF-EOS 5 product files is consistent with
!   KNMI document number SD-OMIE-KNMI-443 entitled "Definition of OMI Grids
!   for Level 3 and Level 4 Data Products" by J.P. Veefkind, J.F. De Hahn,
!   P.F. Levelt and R. Noordhoek.
!
!   The format of the OMTO3e HDF-EOS 5 product files is consistent with
!   Version 1.7 of the document entitled "A File Format for Satellite
!   Atmospheric Chemistry Data" by C. Craig, P. Veefkind, P. Leonard,
!   P. Wagner, C. Vuu and D. Shepard.
!
! OMTO3e Gridding Algorithm:
!   Each grid cell in the L3e product contains the data for the L2 observation
!   that overlaps with the L3 grid cell which has the shortest path length
!   [path length = 1/cos(solar zenith angle) + 1/cos(viewing zenith angle)].
!
!   The overlap between an L2 observation and an L3 grid cell is determined
!   in a manner consistent with the document entitled "Total Ozone Mapping
!   Spectrometer (TOMS) Level-3 Data Products User's Guide" mentioned above.
!
!   An L2 observation can be mapped onto more than one L3 grid cell, if the L2
!   observation overlaps with and has the shortest path length for more than one
!   L3 grid cell.
!
!   The L2 observations are not averaged or weighted in any way in the L3e
!   product.
!
!   The L3e product currently excludes L2 data collected in spatial and spectral
!   zoom modes.
!
!   Before the L2 observation with the shortest path length is selected, each of
!   the L2 observations that overlap with each L3 grid cell is considered, and
!   compared with several exclusion criteria.  These criteria are summarized
!   here in sequence.
!
!   Let l3_tnoon be the time at noon UTC for the TOMS L3 day, and let l2g_time
!   be the L2 observation time.
!
!   A1) As a rough first cut, L2 observations made outside of the 48-hour time
!       interval centered at l3_tnoon are excluded.  Thus, L2 observations with
!
!       l2g_time <  l3_tnoon - (24 hours - 15 minutes)
!
!       or
!
!       l2g_time >= l3_tnoon + (24 hours - 15 minutes)
!
!       are excluded.
!
!   At any given moment, all points on Earth between the longitude of midnight
!   and the dateline that are on the same side of the dateline have the same
!   calendar date.  The calendar dates on opposite sides of the dateline differ
!   by one day, except at the instant when the longitude of midnight and the
!   dateline coincide, in which case the date is the same everywhere on Earth.
!
!   Let l2_lom be the longitude of midnight at l2g_time, and let l2g_lon be the
!   longitude at the center of the L2 observation.  The dateline is assumed to
!   lie strictly at a longitude of +/-180 degrees for the sake of simplicity,
!   which ignores the zigs and zags of the actual dateline.
!
!   A2) L2 observations with local calendar dates on the ground that correspond
!       to the day before the TOMS L3 day are excluded.  This has been
!       implemented as L2 observations with
!
!       l2g_time <  l3_tnoon - 15 minutes
!
!       and
!
!       -180 degrees <= l2g_lon < l2_lom
!
!       are excluded.
!
!   A3) L2 observations with local calendar dates on the ground that correspond
!       to the day after the TOMS L3 day are excluded.  This has been
!       implemented as L2 observations with
!
!       l2g_time >= l3_tnoon + 15 minutes
!
!       and
!
!       l2_lom <= l2g_lon < 180 degrees
!
!       are excluded.
!
!   Let bit5 be bit 5 (the 6th bit) of the "ground pixel quality flag" of the L2
!   observation.  This is the solar eclipse possibility flag.
!
!   A4) L2 observations with the solar eclipse possibility flag set are excluded.
!       Thus, L2 observations with
!
!       bit5 /= 0
!
!       are excluded.
!
!   Let bit6 be bit 6 (the 7th bit) of "quality flags" of the L2 observation.
!   This is the row anomaly flag.
!
!   A5) L2 observations with the row anomaly flag set are excluded.  Thus, L2
!       observations with
!
!       bit6 /= 0
!
!       are excluded.
!
!   After this point there are significant differences in how L2 observations
!   are excluded from 1) the L3 grids for the total column amount ozone and
!   radiative cloud fraction, and 2) the L3 grid for the UV aerosol index.
!
! OMTO3e Gridding Algorithm for Total Column Ozone and Radiative Cloud Fraction:
!   There is one criterian in addition to A1 through A5 (above) for excluding
!   L2 observations from the L3 grids for the total column amount ozone and
!   radiative cloud fraction.
!
!   Let first4bitsA be bits 0 through 3 (the first four bits) of the "quality
!   flag" of the L2 observation, which has the following values:
!     0 - good sample
!     1 - glint contamination (corrected)
!     2 - sza > 84 (degree)
!     3 - 360 residual > threshold
!     4 - residual at unused ozone wavelength > 4 sigma
!     5 - SOI > 4 sigma (SO2 present)
!     6 - non-convergence
!     7 - abs(residual) > 16.0 (fatal)
!     Add 8 for descending data.
!
!   B6) L2 observations gathered on the ascending part of the orbit that are not
!       either a "good sample" or "glint contamination corrected" are excluded,
!       as are all observations gathered on the descending part of the orbit.
!       Thus, L2 observations with both
!
!       first4bitsA /= 0
!
!       and
!
!       first4bitsA /= 1
!
!       are excluded.
!
! OMTO3e Gridding Algorithm for UV Aerosol Index:
!   There are five criteria in addition to A1 through A5 (above) for excluding
!   L2 observations from the L3 grid for the UV aerosol index.
!
!   C6) L2 observations gathered on the descending part of the orbit or with the
!       "non-convergence" flag set are excluded.  Thus, L2 observations with
!
!       first4bitsA >= 6
!
!       are excluded.
!
!   C7) L2 observations with a solar zenith angle greater than or equal to 70.0
!       degrees are excluded.  Thus, L2 observations with
!
!       l2g_sza >= 70.0
!
!       are excluded.
!
!   Let path_index be the TOMS L3 "path index" of an L2 observation.  This is
!   somewhat arbitrarily defined to be 1.0 / cos(l2g_sza) + 2.0 / cos(l2g_vza),
!   where l2g_sza and l2g_vza are the solar zenith and viewing zenith angles of
!   the observation, respectively.
!
!   C8) L2 observations with a path index greater than or equal to 7.0 are
!       excluded.  Thus, L2 observations with
!
!       path_index >= 7.0
!
!       are excluded.
!
!   Let first4bitsB be bits 0 through 3 (the first four bits) of the "ground
!   pixel quality flag" of the L2 observation, which has the following values:
!     0    - shallow ocean
!     1    - land
!     2    - shallow inland water
!     3    - ocean coastline/lake shoreline
!     4    - ephemeral (intermittent) water
!     5    - deep inland water
!     6    - continental shelf ocean
!     7    - deep ocean
!     8-14 - not used
!     15   - error flag for land/water
!
!   (Please note that first4bitsA is completely different from first4bitsB.  The
!   former refers to the "quality flag" of the L2 observation, while the latter
!   refers to the "ground pixel quality flag" of the L2 observation.)
!
!   Let glint_angle be the "glint angle" of an L2 observation.  This is equal to
!   the inverse cosine of
!   ( cos(l2g_sza) * cos(l2g_vza) + sin(l2g_sza) * sin(l2g_vza) * cos(l2g_raa) )
!   where l2g_raa is the relative azimuth angle of the observation.
!
!   C9) L2 observations with water at the ground pixel center and a glint angle
!       less than or equal to than 20.0 degrees are excluded.  Thus, L2
!       observations with both
!
!       first4bitsB /= 1
!
!       and
!
!       glint_angle <= 20.0
!
!       are excluded.
!
!   Let l2g_uvai be the UV aerosol index for an L2 observation, and let mv_uvai
!   be the missing value for the UV aerosol index.
!
!   C10) L2 observations with a value of the UV aerosol index equal to the
!        missing value (to within one part in a thousand) are excluded.  Thus,
!        L2 observations with
!
!        ABS( (l2g_uvai - mv_uvai) / mv_uvai ) <= 0.001
!
!        are excluded.
!
!   C11) Values of the UV aerosol index less than 0.5 are excluded.  Thus, L2
!        observations with
!
!        l2g_uvai < 0.5
!
!        are excluded.
!
!===============================================================================
