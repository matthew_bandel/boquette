Shortname:    OMTO3e
Longname:     OMI/Aura Ozone (O3) Total Column Daily L3 Global 0.25deg Lat/Lon Grid
PFS Version:  1.1.0
Date:         24 May 2023
Author(s):    Peter Leonard (ADNET)

PGE Version:               1.1.0
Lead Algorithm Scientist:  Richard D. McPeters (NASA/GSFC)
Lead Algorithm Developer:  Peter Leonard (ADNET)
Lead PGE Developer:        Peter Leonard (ADNET)
PGE Developer(s):          Peter Leonard (ADNET)

Description: >
  
 This document specifies the format of the Ozone Monitoring Instrument (OMI)
 OMTO3e product, which is the daily Level 3e (L3e) gridded data product that
 corresponds to the OMTO3 product.  The latter is the total column ozone orbital
 Level 2 (L2) swath data product of the U.S. OMI Science Team (Reference 1).
 The "e" at the end of "OMTO3e" represents "expanded".
  
 The adopted L3e grid is a 0.25-degree by 0.25-degree grid in longitude and
 latitude.  The dimensions of this grid are 1440 by 720.
 The grid cell at coordinates (1, 1)
 is centered at (longitude = -179.875 , latitude = -89.875),
 and the grid cell at coordinates (1440, 720)
 is centered at (longitude =  179.875 , latitude =  89.875).
 The center of the grid
 is located at  (longitude =    0.000 , latitude =   0.000),
 and corresponds to the corners of four grid cells.
  
 The adopted L3e grid is consistent with the document entitled "Definition
 of OMI Grids for Level 3 and Level 4 Data Products" by J.P. Veefkind et al.
 (Reference 2).
 
 Each grid cell in the L3e product contains the data for the L2 observation
 that overlaps with the L3e grid cell which has the shortest path length
 [path length = 1/cos(solar zenith angle) + 1/cos(viewing zenith angle)].
 
 The overlap between an L2 observation and an L3e grid cell is determined
 in a manner consistent with the document entitled "Total Ozone Mapping
 Spectrometer (TOMS) Level-3 Data Products User's Guide" by R. McPeters et
 al. (Reference 5).
  
 An L2 observation can be mapped onto more than one L3e grid cell, if the L2
 observation overlaps with and has the shortest path length for more than one
 L3e grid cell.
 
 The L2 data are not averaged or weighted in any way in the L3e product.
  
 The L3e product currently excludes L2 data collected in spatial and spectral
 zoom modes.
  
 The L3e product is stored as one HDF-EOS 5 grid file, and has a size of 4 MB.
  
 The format of the L3 product files is consistent with the document entitled
 "A File Format for Satellite Atmospheric Chemistry Data" by C. Craig et al.
 (Reference 3).
  
Global Metadata:
 
 - Metadata Name:     EndUTC
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    UTC at the end of the L3e granule in "YYYY-MM-DDT23:59:59.999999Z" format.
 
 - Metadata Name:     GranuleDay
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     31
   Data Source:       PGE
   Description:       The day of the month at the start of the L3e granule.
 
 - Metadata Name:     GranuleDayOfYear
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     366
   Data Source:       PGE
   Description:       The day of the year at the start of the L3e granule.

 - Metadata Name:     GranuleMonth
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     12
   Data Source:       PGE
   Description:       The month of the year at the start of the L3e granule.

 - Metadata Name:     GranuleYear
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     2000
   Maximum Value:     2099
   Data Source:       PGE
   Description:       The (four-digit) year at the start of the L3e granule.

 - Metadata Name:     HDFEOSVersion
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       HE
   Description: >
    The version of HDF-EOS 5 used in production.  Example is "HDFEOS_5.1.9".

 - Metadata Name:     InstrumentName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI
   Data Source:       PGE
   Description:       Actual is "OMI" (see Section 6.1 of Reference 3).
 
 - Metadata Name:     OrbitNumber
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,60
   Minimum Value:     1
   Maximum Value:     999999
   Data Source:       L2G
   Description:       The OMI orbit number for each L2 input granule.
 
 - Metadata Name:     OrbitPeriod
   Mandatory:         T
   Data Type:         HE5T_NATIVE_DOUBLE
   Number of Values:  1,60
   Minimum Value:     5000.0
   Maximum Value:     7000.0
   Data Source:       L2G
   Description:       The Aura orbital period for each L2 input granule.
 
 - Metadata Name:     Period
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Daily,Weekly,Monthly
   Data Source:       PGE
   Description:       The duration of the L3e granule.  Actual is "Daily".

 - Metadata Name:     PGEVersion
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PCF
   Description:       Example is "0.9.38" (see Appendix K of Reference 4).

 - Metadata Name:     ProcessLevel
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            3e
   Data Source:       PGE
   Description:       Actual is "3e".
 
 - Metadata Name:     StartUTC
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    UTC at the start of the L3e granule in "YYYY-MM-DDT00:00:00.000000Z" format.

 - Metadata Name:     TAI93At0zOfGranule
   Mandatory:         T
   Data Type:         HE5T_NATIVE_DOUBLE
   Number of Values:  1
   Minimum Value:     0.0
   Maximum Value:     1.0e+30
   Data Source:       PGE
   Description: >
    The TAI93 time at 0z of the L3e granule (see Section 6.1 of Reference 3).
 
Grid Metadata:
 
 - Metadata Name:     GCTPProjectionCode
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     99
   Data Source:       PGE
   Description: >
    The GCTP projection code of the L3e grid.  Actual is 0, which corresponds
    to the geographic projection.
 
 - Metadata Name:     GridName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI Column Amount O3
   Data Source:       PGE
   Description:       Actual is "OMI Column Amount O3".
 
 - Metadata Name:     GridOrigin
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Center
   Data Source:       PGE
   Description: >
    The origin of the L3e grid.  Actual is "Center" (see Section 6.2 of
    Reference 3).
 
 - Metadata Name:     GridSpacing
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Spacing of L3e grid (in degrees).  Actual is "(0.25,0.25)".
 
 - Metadata Name:     GridSpacingUnit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            deg
   Data Source:       PGE
   Description: >
    Unit for GridSpacing.  Actual is "deg".
 
 - Metadata Name:     GridSpan
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Span of L3e grid (in degrees).  Actual is "(-180,180,-90,90)".
 
 - Metadata Name:     GridSpanUnit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            deg
   Data Source:       PGE
   Description: >
    Unit for GridSpan.  Actual is "deg".
 
 - Metadata Name:     NumberOfLatitudesInGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     720
   Data Source:       PGE
   Description:       The number of latitude bins in the L3e grid.
 
 - Metadata Name:     NumberOfLongitudesInGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     1440
   Data Source:       PGE
   Description:       The number of longitude bins in the L3e grid.
 
 - Metadata Name:     Projection
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Geographic
   Data Source:       PGE
   Description: >
    The map projection of the L3e grid.  Actual is "Geographic" (see
    Section 6.2 of Reference 3).
 
Grid Dimensions:
 
 - Dimension Name:    XDim
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     1440
   Data Source:       PGE
   Description: >
    The longitudes dimension of the L3e grid.  There are currently 1440
    0.25-degree-wide bins between longitudes -180.0 and 180.0 degrees.
 
 - Dimension Name:    YDim
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     720
   Data Source:       PGE
   Description: >
    The latitudes dimension of the L3e grid.  There are currently 720
    0.25-degree-wide bins between latitudes -90.0 and 90.0 degrees.

Geolocation Fields:
 
 - Field Name:               SolarZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2G
   Title:                    Solar Zenith Angle
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The solar zenith angle (in degrees) on the ground at the center for the
    L2 candidate scene with the shortest path length in each L3e grid cell.
 
 - Field Name:               ViewingZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            70.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2G
   Title:                    Viewing Zenith Angle
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The viewing zenith angle (in degrees) on the ground at the center for the
    L2 candidate scene with the shortest path length in each L3e grid cell.
 
Data Fields:
 
 - Field Name:               ColumnAmountO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               YDim,XDim
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              L2G
   Title:                    Best Total Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The best total ozone solution (in DU) for the L2 candidate scene with
    the shortest path length in each L3e grid cell.
 
 - Field Name:               RadiativeCloudFraction
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2G
   Title:                    Radiative Cloud Fraction = fc*lc331/lm331
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The radiative cloud fraction (= fc * lc331 / lm331) for the L2 candidate
    scene with the shortest path length in each L3e grid cell.

Core Metadata: >
 
  None.

Product Specific Attributes: >
 
  None.

Archived Metadata: >
 
  None.
 
References: >
 
  1. "OMTO3 README File"
     (January 30, 2008)
     (http://disc.sci.gsfc.nasa.gov/Aura/OMI/omto3_v003.shtml)
 
  2. "Definition of OMI Grids for Level 3 and Level 4 Data Products"
     (OMI-Grids_L3L4, SD-OMIE-KNMI-443, 25 January 2005)

  3. "A File Format for Satellite Atmospheric Chemistry Data"
     (OMI-AURA-DATA-GUIDE, Version 1.7, 12 May 2008)

  4. "OMI Science Software Delivery Guide for Version 0.9"
     (OMI-SSDG-0.9.10, Version 0.9.10, 22 June 2005)
 
  5. "Total Ozone Mapping Spectrometer (TOMS) Level-3 Data Products User's Guide"
     (NASA/TM-2000-209896, July 2000)
