!===============================================================================
!
! PROGRAM OMTO3e (OMI Daily L3e for OMTO3)
!
! OMTO3e High Level Overview:
!   This is the main program for the OMI (Ozone Monitoring Instrument) OMTO3e
!   Product Generation Executive (PGE).  The OMTO3e PGE creates the OMTO3e data
!   product, which is the daily 0.25-degree by 0.25-degree Level 3e (L3e) total
!   column ozone product of the U.S. OMI Science Team.  The "e" at the end of
!   "OMTO3e" represents "expanded".
!
!   The OMTO3e PGE creates a (Total Ozone Mapping Spectrometer) TOMS-like daily
!   L3e gridded data product file from (as many as) three consecutive OMTO3G
!   daily Level 2G (L2G) gridded data product files, where each OMTO3G file
!   contains 24 consecutive UTC hours of OMTO3 orbital Level 2 (L2) swath data
!   subsetted onto a 0.25-degree by 0.25-degree grid in longitude and latitude.
!
!   A TOMS L3 day is defined as the ensemble of all L2 ground pixels with pixel
!   centers that have the same local calendar date on the ground.  There are two
!   reasons behind such a definition.  First, a TOMS L3 day provides complete
!   coverage of Earth, since every point on Earth (outside of polar night)
!   experiences daylight on each calendar date (in comparison, 24 consecutive
!   UTC hours of OMI observations do not completely cover Earth).  Second, the
!   TOMS L3 day puts the discontinuity (i.e., where the L2 observations within a
!   given day differ by almost 24 hours) at +/-180 degrees longitude, and, thus,
!   the discontinuity can be placed undistractingly along the extreme left and
!   right edges of several commonly used map projections.
!
!   The calendar date of the TOMS L3 day is the calendar date at Greenwich
!   midway through the TOMS L3 day, and is specified via the L3 day of year
!   parameter in the PCF (Process Control File) of the OMTO3e PGE.  Note
!   that some of the L2 observations at the beginning of a TOMS L3 day will
!   correspond to the previous calendar date at Greenwich, and some of the L2
!   observations at the end of a TOMS L3 day will correspond to the next
!   calendar date at Greenwich.  Consequently, data from three consecutive OMI
!   L2G files are required to fully populate the L3 grid at all longitudes for
!   any given TOMS L3 day.
!
!   The OMTO3e PGE was developed for Dr. Mark R. Schoeberl (NASA/GSFC), and is
!   based upon the TOMS Level 3 Gridded Software.  The latter was developed over
!   a period of many years by several people:  W. Byerly, D. Cao, E. Celarier,
!   Q. Choung, S. Huang, B. Irby, D. Lee, L. Liu, R. McPeters, L. Moy, M. Peng,
!   L. Phung, B. Raines, C. Seftor, and, especially, C. Wellemeyer.
!
! Adopted OMTO3e Grid:
!   The adopted L3 grid is a 0.25-degree by 0.25-degree grid in longitude and
!   latitude.  The dimensions of the grid are 1440 by 720.  The center of the
!   first grid cell is located at longitude -179.875 and latitude -89.875.  The
!   center of the final grid cell is located at longitude 179.875 and latitude
!   89.875.  The center of the grid itself is located at longitude 0.0 and
!   latitude 0.0, and corresponds to the corners of four grid cells.
!
!   The grid and format of the OMTO3e ASCII product files are consistent with
!   NASA document number NASA/TM-2000-209896 entitled "Total Ozone Mapping
!   Spectrometer (TOMS) Level-3 Data Products User's Guide" by R. McPeters,
!   P.K. Bhartia, A. Krueger, J. Herman, C. Wellemeyer, C. Seftor, W. Byerly
!   and E.A. Celarier.
!
!   The adopted grid for the OMTO3e HDF-EOS 5 product files is consistent with
!   KNMI document number SD-OMIE-KNMI-443 entitled "Definition of OMI Grids
!   for Level 3 and Level 4 Data Products" by J.P. Veefkind, J.F. De Hahn,
!   P.F. Levelt and R. Noordhoek.
!
!   The format of the OMTO3e HDF-EOS 5 product files is consistent with
!   Version 1.7 of the document entitled "A File Format for Satellite
!   Atmospheric Chemistry Data" by C. Craig, P. Veefkind, P. Leonard,
!   P. Wagner, C. Vuu and D. Shepard.
!
! OMTO3e Gridding Algorithm:
!   Each grid cell in the L3e product contains the data for the L2 observation
!   that overlaps with the L3 grid cell which has the shortest path length
!   [path length = 1/cos(solar zenith angle) + 1/cos(viewing zenith angle)].
!
!   The overlap between an L2 observation and an L3 grid cell is determined
!   in a manner consistent with the document entitled "Total Ozone Mapping
!   Spectrometer (TOMS) Level-3 Data Products User's Guide" mentioned above.
!
!   An L2 observation can be mapped onto more than one L3 grid cell, if the L2
!   observation overlaps with and has the shortest path length for more than one
!   L3 grid cell.
!
!   The L2 observations are not averaged or weighted in any way in the L3e
!   product.
!
!   The L3e product currently excludes L2 data collected in spatial and spectral
!   zoom modes.
!
!   Before the L2 observation with the shortest path length is selected, each of
!   the L2 observations that overlap with each L3 grid cell is considered, and
!   compared with several exclusion criteria.  These criteria are summarized
!   here in sequence.
!
!   Let l3_tnoon be the time at noon UTC for the TOMS L3 day, and let l2g_time
!   be the L2 observation time.
!
!   A1) As a rough first cut, L2 observations made outside of the 48-hour time
!       interval centered at l3_tnoon are excluded.  Thus, L2 observations with
!
!       l2g_time <  l3_tnoon - (24 hours - 15 minutes)
!
!       or
!
!       l2g_time >= l3_tnoon + (24 hours - 15 minutes)
!
!       are excluded.
!
!   At any given moment, all points on Earth between the longitude of midnight
!   and the dateline that are on the same side of the dateline have the same
!   calendar date.  The calendar dates on opposite sides of the dateline differ
!   by one day, except at the instant when the longitude of midnight and the
!   dateline coincide, in which case the date is the same everywhere on Earth.
!
!   Let l2_lom be the longitude of midnight at l2g_time, and let l2g_lon be the
!   longitude at the center of the L2 observation.  The dateline is assumed to
!   lie strictly at a longitude of +/-180 degrees for the sake of simplicity,
!   which ignores the zigs and zags of the actual dateline.
!
!   A2) L2 observations with local calendar dates on the ground that correspond
!       to the day before the TOMS L3 day are excluded.  This has been
!       implemented as L2 observations with
!
!       l2g_time <  l3_tnoon - 15 minutes
!
!       and
!
!       -180 degrees <= l2g_lon < l2_lom
!
!       are excluded.
!
!   A3) L2 observations with local calendar dates on the ground that correspond
!       to the day after the TOMS L3 day are excluded.  This has been
!       implemented as L2 observations with
!
!       l2g_time >= l3_tnoon + 15 minutes
!
!       and
!
!       l2_lom <= l2g_lon < 180 degrees
!
!       are excluded.
!
!   Let bit5 be bit 5 (the 6th bit) of the "ground pixel quality flag" of the L2
!   observation.  This is the solar eclipse possibility flag.
!
!   A4) L2 observations with the solar eclipse possibility flag set are excluded.
!       Thus, L2 observations with
!
!       bit5 /= 0
!
!       are excluded.
!
!   Let bit6 be bit 6 (the 7th bit) of "quality flags" of the L2 observation.
!   This is the row anomaly flag.
!
!   A5) L2 observations with the row anomaly flag set are excluded.  Thus, L2
!       observations with
!
!       bit6 /= 0
!
!       are excluded.
!
!   After this point there are significant differences in how L2 observations
!   are excluded from 1) the L3 grids for the total column amount ozone and
!   radiative cloud fraction, and 2) the L3 grid for the UV aerosol index.
!
! OMTO3e Gridding Algorithm for Total Column Ozone and Radiative Cloud Fraction:
!   There is one criterian in addition to A1 through A5 (above) for excluding
!   L2 observations from the L3 grids for the total column amount ozone and
!   radiative cloud fraction.
!
!   Let first4bitsA be bits 0 through 3 (the first four bits) of the "quality
!   flag" of the L2 observation, which has the following values:
!     0 - good sample
!     1 - glint contamination (corrected)
!     2 - sza > 84 (degree)
!     3 - 360 residual > threshold
!     4 - residual at unused ozone wavelength > 4 sigma
!     5 - SOI > 4 sigma (SO2 present)
!     6 - non-convergence
!     7 - abs(residual) > 16.0 (fatal)
!     Add 8 for descending data.
!
!   B6) L2 observations gathered on the ascending part of the orbit that are not
!       either a "good sample" or "glint contamination corrected" are excluded,
!       as are all observations gathered on the descending part of the orbit.
!       Thus, L2 observations with both
!
!       first4bitsA /= 0
!
!       and
!
!       first4bitsA /= 1
!
!       are excluded.
!
! OMTO3e Gridding Algorithm for UV Aerosol Index:
!   There are five criteria in addition to A1 through A5 (above) for excluding
!   L2 observations from the L3 grid for the UV aerosol index.
!
!   C6) L2 observations gathered on the descending part of the orbit or with the
!       "non-convergence" flag set are excluded.  Thus, L2 observations with
!
!       first4bitsA >= 6
!
!       are excluded.
!
!   C7) L2 observations with a solar zenith angle greater than or equal to 70.0
!       degrees are excluded.  Thus, L2 observations with
!
!       l2g_sza >= 70.0
!
!       are excluded.
!
!   Let path_index be the TOMS L3 "path index" of an L2 observation.  This is
!   somewhat arbitrarily defined to be 1.0 / cos(l2g_sza) + 2.0 / cos(l2g_vza),
!   where l2g_sza and l2g_vza are the solar zenith and viewing zenith angles of
!   the observation, respectively.
!
!   C8) L2 observations with a path index greater than or equal to 7.0 are
!       excluded.  Thus, L2 observations with
!
!       path_index >= 7.0
!
!       are excluded.
!
!   Let first4bitsB be bits 0 through 3 (the first four bits) of the "ground
!   pixel quality flag" of the L2 observation, which has the following values:
!     0    - shallow ocean
!     1    - land
!     2    - shallow inland water
!     3    - ocean coastline/lake shoreline
!     4    - ephemeral (intermittent) water
!     5    - deep inland water
!     6    - continental shelf ocean
!     7    - deep ocean
!     8-14 - not used
!     15   - error flag for land/water
!
!   (Please note that first4bitsA is completely different from first4bitsB.  The
!   former refers to the "quality flag" of the L2 observation, while the latter
!   refers to the "ground pixel quality flag" of the L2 observation.)
!
!   Let glint_angle be the "glint angle" of an L2 observation.  This is equal to
!   the inverse cosine of
!   ( cos(l2g_sza) * cos(l2g_vza) + sin(l2g_sza) * sin(l2g_vza) * cos(l2g_raa) )
!   where l2g_raa is the relative azimuth angle of the observation.
!
!   C9) L2 observations with water at the ground pixel center and a glint angle
!       less than or equal to than 20.0 degrees are excluded.  Thus, L2
!       observations with both
!
!       first4bitsB /= 1
!
!       and
!
!       glint_angle <= 20.0
!
!       are excluded.
!
!   Let l2g_uvai be the UV aerosol index for an L2 observation, and let mv_uvai
!   be the missing value for the UV aerosol index.
!
!   C10) L2 observations with a value of the UV aerosol index equal to the
!        missing value (to within one part in a thousand) are excluded.  Thus,
!        L2 observations with
!
!        ABS( (l2g_uvai - mv_uvai) / mv_uvai ) <= 0.001
!
!        are excluded.
!
!   C11) Values of the UV aerosol index less than 0.5 are excluded.  Thus, L2
!        observations with
!
!        l2g_uvai < 0.5
!
!        are excluded.
!
! OML3 Subroutines Called:
!   OML3_ReadPCF            - Reads OMTO3e PCF.
!   OML3_EndInFailure       - Ends OMTO3e execution in failure.
!
!   OML3_OpenL2GFile        - Opens L2G file and attaches to grid in file.
!   OML3_ReadL2GECSMeta     - Reads ECS Metadata from L2G file.`
!   OML3_ConvertECT         - Converts eq. cross. data to solar and TAI93 times.
!   OML3_ReadL2GFileMeta    - Reads file-level Metadata from L2G file.
!   OML3_ReadL2GGridMeta    - Reads grid-level Metadata from L2G file.
!   OML3_CheckL2GInputs     - Checks if L2G files correspond to consecutive days.
!   OML3_ReadL2GFieldInfo   - Reads names of grid fields from L2G file.
!   OML3_ReadL2GFieldMeta   - Reads grid-field Metadata from L2G file.
!   OML3_ReadL2GGridFields  - Reads fields from grid in L2G file.
!   OML3_MidnightLong       - Calculates long. of midnight for L2 cand. scene.
!   OML3_CloseL2GFile       - Detaches from grid in L2G file and closes file.
!
!   OML3_WriteL3FilesASCII  - Writes out ASCII TOMS-like L3 files.
!
!   OML3_OpenL3File         - Creates L3 file and creates grid within file.
!   OML3_WriteL3FileMeta    - Writes file-level Metadata to L3 file.
!   OML3_WriteL3GridMeta    - Writes grid-level Metadata to L3 file.
!   OML3_WriteL3GridFields  - Writes fields to grid in L3 file.
!   OML3_WriteL3FieldMeta   - Writes grid-field Metadata to L3 file.
!   OML3_CloseL3File        - Detaches from grid in L3 file and closes file.
!
! External Function Called:
!   OMI_SMF_setmsg          - OMI messaging function.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   April 13, 2006
!
! Revision History:
!   April 14, 2006 - Replaced L2G_File_Loop2 and L2G_File_Loop3 double loop
!                    structure with 2G_File_Loop2 single loop structure.
!   April 17, 2006 - Commented out glint-angle filtering of aerosol index.
!   April 19, 2006 - Corrected screening indices l2g_ilon_beg and l2g_ilat_beg.
!   April 20, 2006 - Reintroduced glint-angle filtering of aerosol index.
!   May 1, 2006 - Added calls to OML3_OpenL3File, OML3_WriteL3FileMeta,
!                 OML3_WriteL3GridMeta, OML3_WriteL3GridFields,
!                 OML3_WriteL3FieldMeta and OML3_CloseL3File to create
!                 HDF-EOS 5 L3 file.
!   May 5, 2006 - Improved comments in file header.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History (Continued):
!   June 20, 2007 - Increased number of characters in l3_pgevers to 20.
!                 - Improved comments in file header.
!   November 27, 2007 - Added to comments in file header.
!   December 6, 2007 - Added to comments in file header.
!   December 12, 2007 - Replaced reflectivity at 331 nm field with radiative
!                       cloud fraction field.
!   December 14, 2007 - Added solar zenith angle & viewing zenith angle fields
!                       to HE5 output file.
!   March 21, 2008 - Expanded temporal range searched.
!   May 13, 2008 - Improved comments in file header.
!   July 18, 2008 - Added logic to exclude observations with scene number 
!                   in the range from 38 to 43 (1-based), with line number
!                   of 1100 or greater, made on May 1, 2008 or later.
!   August 29, 2008 - Removed "line 1100" filter for May 2008 anomaly.
!                   - Added logic to exclude observations with scene number 
!                     54 or 55 (1-based) made on June 1, 2007 or later.
!   February 11, 2009 - Added logic to exclude observations with scene number
!                       in the range from 29 to 45 (1-based) made on January 24,
!                       2009 or later.
!   February 19, 2009 - Extended blocking of observations with scene number in
!                       the range from 36 to 45 (1-based) back to Dec. 1, 2008.
!   April 2, 2009 - Removed all cross-track position blocking.
!   June 2, 2009 - Removed all traces of hard-wired blocked-pixel ranges.
!                - Added logic to exclude obs. with bit 6 of l2g_qflg set.
!   March 16, 2017 - Added checks for NaN values.
!
!===============================================================================

PROGRAM OMTO3e

!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE

!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'PGS_SMF.f'       ! Declarations for Toolkit Fortran SMF functions.
  INCLUDE 'PGS_OMI_1900.f'  ! Declarations for general OMI messages.
 
!===============================================================================
! L2G file quantities.
!===============================================================================
  CHARACTER (LEN=8) :: l2g_prodid     ! L2G product ID.
  INTEGER (KIND=4) :: l2g_nfile       ! Number of L2G files.
  INTEGER (KIND=4) :: l2g_ifile       ! L2G file counter (1 to l2g_nfile).
  CHARACTER (LEN=256), &
    DIMENSION(3) :: l2g_fname         ! L2G file names.
  INTEGER (KIND=4) :: l2g_fileid      ! L2G file ID.
 
!===============================================================================
! L2G grid quantities.
!===============================================================================
  INTEGER (KIND=4) :: l2g_gridid      ! L2G grid ID.
  INTEGER (KIND=4) :: l2g_nlon        ! Number of longitudes in L2G grid.
  INTEGER (KIND=4) :: l2g_nlat        ! Number of latitudes in L2G grid.
  INTEGER (KIND=4) :: l2g_ncand       ! Number of candidates in L2G grid.
  INTEGER (KIND=4) :: l2g_nlon_prev   ! Number of longitudes in previous grid.
  INTEGER (KIND=4) :: l2g_nlat_prev   ! Number of latitudes in previous grid.
  INTEGER (KIND=4) :: l2g_ncand_prev  ! Number of candidates in previous grid.
  INTEGER (KIND=4) :: l2g_ilon        ! L2G longitude counter.
  INTEGER (KIND=4) :: l2g_ilat        ! L2G latitude counter.
  INTEGER (KIND=4) :: l2g_icand       ! L2G candidate counter.
  INTEGER (KIND=4) :: l2g_ilon_beg    ! L2G longitude beginning index.
  INTEGER (KIND=4) :: l2g_ilon_end    ! L2G longitude ending index.
  INTEGER (KIND=4) :: l2g_ilat_beg    ! L2G latitude beginning index.
  INTEGER (KIND=4) :: l2g_ilat_end    ! L2G latitude ending index.

!===============================================================================
! L2 orbital quantities.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(3) :: l2_norb        ! Number of L2 orbits in each L2G file (from
                                   ! ECS Core Metadata).
  INTEGER (KIND=4), &
    DIMENSION(20,3) :: l2_orb      ! Array of L2 orbit numbers (from ECS Core
                                   ! Metadata).
  INTEGER (KIND=4), &
    DIMENSION(20,3) :: l2_orbit    ! Array of L2 orbit numbers (from HDF-EOS
                                   ! file-level Metadata).
  REAL (KIND=8), &
    DIMENSION(20,3) :: l2_orbper   ! Array of L2 orbital periods (from HDF-EOS
                                   ! file-level Metadata).
  INTEGER (KIND=4) l2_iorb         ! L2 orbit counter.
  CHARACTER (LEN=10), &
    DIMENSION(20,3) :: l2_ecd      ! Equator crossing dates of L2 orbits.
  CHARACTER (LEN=16), &
    DIMENSION(20,3) :: l2_ect      ! Equator crossing times of L2 orbits.
  REAL (KIND=8), &
    DIMENSION(20,3) :: l2_ecl      ! Equator crossing longitudes of L2 orbits.
  INTEGER (KIND=4), &
    DIMENSION(20,3) :: l2_ech      ! Eq. cr. mean solar hours of L2 orbits.
  INTEGER (KIND=4), &
    DIMENSION(20,3) :: l2_ecm      ! Eq. cr. mean solar minutes of L2 orbits.
  INTEGER (KIND=4), &
    DIMENSION(20,3) :: l2_ecs      ! Eq. cr. mean solar seconds of L2 orbits.
  REAL (KIND=8), &
    DIMENSION(20,3) :: l2_ecTAI93  ! Equator crossing TAI93s of L2 orbits.
 
!===============================================================================
! L2G temporal quantities.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(3) :: l2g_month      ! Month of L2G file.
  INTEGER (KIND=4), &
    DIMENSION(3) :: l2g_day        ! Day of L2G file.
  INTEGER (KIND=4), &
    DIMENSION(3) :: l2g_year       ! Year of L2G file.
  INTEGER (KIND=4), &
    DIMENSION(3) :: l2g_dyofyr     ! Day of year of L2G file.
  REAL (KIND=8), &
    DIMENSION(3) :: l2g_TAI93at0z  ! TAI93 at 0z of L2G file.
  CHARACTER (LEN=28), &
    DIMENSION(3) :: l2g_startUTC   ! UTC at start of L2G file.
  CHARACTER (LEN=28), &
    DIMENSION(3) :: l2g_endUTC     ! UTC at end of L2G file.
  INTEGER (KIND=4) :: l2g_idyofyr  ! Index of L2G file for L3 day of year.
  REAL (KIND=8) :: l2_lom          ! Longitude of midnight for L2 cand. scene.
 
!===============================================================================
! L2G field names and Metadata (note that UFD = Unique Field Definition).
!===============================================================================
  INTEGER (KIND=4) :: gfi1u_nfield    ! Number of uns. 1-B int. fields.
  INTEGER (KIND=4) :: gfi2_nfield     ! Number of 2-B int. fields.
  INTEGER (KIND=4) :: gfi2u_nfield    ! Number of uns. 2-B int. fields.
  INTEGER (KIND=4) :: gfi4_nfield     ! Number of 4-B int. fields.
  INTEGER (KIND=4) :: gfr4_nfield     ! Number of 4-B real fields.
  INTEGER (KIND=4) :: gfr8_nfield     ! Number of 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_names     ! Names of uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_names      ! Names of 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_names     ! Names of uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_names      ! Names of 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_names      ! Names of 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_names      ! Names of 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_units     ! Units for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_units      ! Units for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_units     ! Units for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_units      ! Units for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_units      ! Units for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_units      ! Units for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_title     ! Titles for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_title      ! Titles for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_title     ! Titles for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_title      ! Titles for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_title      ! Titles for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_title      ! Titles for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_ufdef     ! UFDs for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_ufdef      ! UFDs for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_ufdef     ! UFDs for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_ufdef      ! UFDs for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_ufdef      ! UFDs for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_ufdef      ! UFDs for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi1u_sfactor   ! Scale factors for uns. 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2_sfactor    ! Scale factors for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2u_sfactor   ! Scale factors for uns. 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi4_sfactor    ! Scale factors for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr4_sfactor    ! Scale factors for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_sfactor    ! Scale factors for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi1u_offset    ! Offsets for uns. 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2_offset     ! Offsets for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2u_offset    ! Offsets for uns. 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi4_offset     ! Offsets for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr4_offset     ! Offsets for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_offset     ! Offsets for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100,2) :: gfi1u_vrange  ! Valid ranges for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2) :: gfi2_vrange   ! Valid ranges for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2) :: gfi2u_vrange  ! Valid ranges for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100,2) :: gfi4_vrange   ! Valid ranges for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100,2) :: gfr4_vrange   ! Valid ranges for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100,2) :: gfr8_vrange   ! Valid ranges for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100) :: gfi1u_mvalue    ! Missing values for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100) :: gfi2_mvalue     ! Missing values for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100) :: gfi2u_mvalue    ! Missing values for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100) :: gfi4_mvalue     ! Missing values for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100) :: gfr4_mvalue     ! Missing values for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_mvalue     ! Missing values for 8-B real fields.
 
!===============================================================================
! L2G grid fields.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(:,:), &
    ALLOCATABLE :: l2g_ncs            ! Number of candidate L2 scenes.
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_gpqf           ! Ground pixel quality flags of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_lat            ! Latitudes of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_line           ! Line numbers of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_lon            ! Longitudes of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_orbit          ! Orbit numbers of L2 scenes in L2G grid.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_pathl          ! Path lengths of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_raa            ! Relative azimuth angles of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_scene          ! Scene numbers of L2 scenes in L2G grid.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_sza            ! Solar zenith angles of L2 scenes.
  REAL (KIND=8), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_time           ! Observation times of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_vza            ! Viewing zenith angles of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_cao3           ! Column amounts O3 of L2 scenes.
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_qflg           ! Quality flags of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_rcf            ! Radiative cloud fractions of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_uvai           ! UV aerosol indices of L2 scenes.
 
!===============================================================================
! L3 file quantities.
!===============================================================================
  CHARACTER (LEN=20) :: l3_pgevers    ! L3 PGE version.
  INTEGER (KIND=4) :: l3_fileid       ! L3 HDF-EOS 5 file ID.
  CHARACTER (LEN=256), &
    DIMENSION(4) :: l3_fname          ! L3 file names.  Elements 1 to 3 are for
                                      ! TOMS-like ASCII files, and element 4 is
                                      ! for HDF-EOS 5 file.
 
!===============================================================================
! L3 temporal quantities.
!===============================================================================
  INTEGER (KIND=4) :: l3_dyofyr  ! Day of year of L3 file.
  INTEGER (KIND=4) :: l3_month   ! Month of year of L3 file.
  INTEGER (KIND=4) :: l3_day     ! Day of month of L3 file.
  INTEGER (KIND=4) :: l3_year    ! Year of L3 file.
  REAL (KIND=8) :: l3_tnoon      ! Time (TAI93) at noon of L3 day.
  REAL (KIND=8) :: l3_tstart     ! Time (TAI93) at start of considered data.
  REAL (KIND=8) :: l3_tend       ! Time (TAI93) at end of considered data.
 
!===============================================================================
! L3 grid quantities.
!===============================================================================
  INTEGER (KIND=8), &
    PARAMETER :: l3_nlon = 1440             ! Number of longitudes in L3 grid.
  INTEGER (KIND=8), &
    PARAMETER :: l3_nlat = 720              ! Number of latitudes in L3 grid.
  INTEGER (KIND=4) :: l3_ilon               ! L3 longitude counter.
  INTEGER (KIND=4) :: l3_ilat               ! L3 latitude counter.
  INTEGER (KIND=4) :: l3_gridid             ! L3 grid ID.

!===============================================================================
! L3 grid fields.
!===============================================================================
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_plmin  ! Minimum path length of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_plminuv  ! Minimum path length of L2 scenes
                                              ! for UV aerosol index.
  INTEGER (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_ncs    ! No. of cand. L2 scenes in L3 grid
                                            ! for O3 and radative cloud fraction
                                            ! calculations.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_sza    ! Solar zenith angles in L3 grid
                                            ! based on mimimum path length.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_vza    ! Viewing zenith angles in L3 grid
                                            ! based on mimimum path length.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_cao3   ! Column amounts O3 in L3 grid based
                                            ! on mimimum path length.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_rcf    ! Radiative cloud fractions in L3
                                            ! grid based on mimimum path length.
  INTEGER (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_ncsuv  ! No. of cand. L2 scenes in L3 grid
                                            ! for UV aerosol index calculations.
  REAL (KIND=4), &
    DIMENSION(l3_nlon,l3_nlat) :: l3_uvai   ! UV aerosol indices in L3 grid
                                            ! based on mimimum path length.
 
!===============================================================================
! Test variables.
!===============================================================================
  REAL (KIND=4) :: mv_uvai         ! Missing value for UV aerosol index.
  REAL (KIND=8) :: tocs            ! Time of candidate scene.
  INTEGER (KIND=2) :: bit5         ! Bit 5 (6th bit) of 2-byte integer.
  INTEGER (KIND=2) :: bit6         ! Bit 6 (7th bit) of 2-byte integer.
  INTEGER (KIND=2) :: first4bitsA  ! First 4 bits of 2-byte integer.
  INTEGER (KIND=2) :: first4bitsB  ! First 4 bits of 2-byte integer.
  REAL (KIND=4) :: path_index      ! Path index of L2 scene.
  REAL (KIND=4) :: path_length     ! Path length of L2 scene.
  REAL (KIND=4) :: glint_angle     ! Glint angle for L2 scene.
  REAL (KIND=4) :: test_dummy      ! Test dummy for missing value.

!===============================================================================
! Some variables to help with classic TOMS-like tessellation.
!===============================================================================
  REAL (KIND=4), &
    DIMENSION(60) :: fovkm      ! Cross track size of FOV (in km) for all 60
                                ! swath positions.
  INTEGER (KIND=4) :: hw_lon    ! Maximum half width in longitude (in L3 grid
                                ! cells) of L2 scene.
  REAL (KIND=4) :: hwd_lon      ! Half width in longitude (in deg) of L2 scene.
  REAL (KIND=4) :: l2s_lon_beg  ! Beginning longitude of L2 scene.
  REAL (KIND=4) :: l2s_lon_end  ! End longitude of L2 scene.
  REAL (KIND=4) :: l3_lon_beg   ! Beginning longitude of L3 grid cell.
  REAL (KIND=4) :: l3_lon_end   ! End longitude of L3 grid cell.
  REAL (KIND=4) :: l3_lat       ! Latitude of L3 grid cell.
  REAL (KIND=4) :: l2s_lat_beg  ! Beginning latitude of L2 scene.
  REAL (KIND=4) :: l2s_lat_end  ! End latitude of L2 scene.
  REAL (KIND=4) :: l3_lat_beg   ! Beginning latitude of L3 grid cell.
  REAL (KIND=4) :: l3_lat_end   ! End latitude of L3 grid cell.

!===============================================================================
! Miscellaneous.
!===============================================================================
  REAL (KIND=4) :: cfdtr        ! Conversion factor for degrees to radians.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat    ! Function call status.
  CHARACTER (LEN=256) :: stat_msg  ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OMTO3e.f90"        ! Code file reference.
  INTEGER (KIND=4), &
    EXTERNAL :: OMI_SMF_setmsg     ! OMI messaging function.

!===============================================================================
! Calculate conversion factor for degrees to radians.
!===============================================================================
  cfdtr = ACOS(-1.0)/180.0
 
!===============================================================================
! Set cross track size of FOV (in km) for all 60 swath positions.
!===============================================================================
  fovkm( 1) = 179.0
  fovkm( 2) = 144.0
  fovkm( 3) = 118.0
  fovkm( 4) = 101.0
  fovkm( 5) =  89.0
  fovkm( 6) =  78.0
  fovkm( 7) =  71.0
  fovkm( 8) =  64.0
  fovkm( 9) =  58.0
  fovkm(10) =  54.0
  fovkm(11) =  50.0
  fovkm(12) =  47.0
  fovkm(13) =  44.0
  fovkm(14) =  41.0
  fovkm(15) =  39.0
  fovkm(16) =  38.0
  fovkm(17) =  36.0
  fovkm(18) =  35.0
  fovkm(19) =  34.0
  fovkm(20) =  33.0
  fovkm(21) =  32.0
  fovkm(22) =  31.0
  fovkm(23) =  31.0
  fovkm(24) =  30.0
  fovkm(25) =  29.0
  fovkm(26) =  29.0
  fovkm(27) =  29.0
  fovkm(28) =  29.0
  fovkm(29) =  28.0
  fovkm(30) =  28.0
  fovkm(31) =  28.0
  fovkm(32) =  28.0
  fovkm(33) =  29.0
  fovkm(34) =  29.0
  fovkm(35) =  29.0
  fovkm(36) =  29.0
  fovkm(37) =  30.0
  fovkm(38) =  31.0
  fovkm(39) =  31.0
  fovkm(40) =  32.0
  fovkm(41) =  33.0
  fovkm(42) =  34.0
  fovkm(43) =  35.0
  fovkm(44) =  36.0
  fovkm(45) =  38.0
  fovkm(46) =  39.0
  fovkm(47) =  41.0
  fovkm(48) =  44.0
  fovkm(49) =  47.0
  fovkm(50) =  50.0
  fovkm(51) =  54.0
  fovkm(52) =  58.0
  fovkm(53) =  64.0
  fovkm(54) =  71.0
  fovkm(55) =  78.0
  fovkm(56) =  89.0
  fovkm(57) = 101.0
  fovkm(58) = 118.0
  fovkm(59) = 144.0
  fovkm(60) = 179.0
 
!===============================================================================
! Read OML3 PCF.
!===============================================================================
  CALL OML3_ReadPCF(l2g_prodid, l2g_nfile, l2g_fname, &
                    l3_pgevers, l3_dyofyr, l3_fname)
 
!===============================================================================
! Loop through L2G files to obtain file- and grid-level Metadata.
!===============================================================================
  L2G_File_Loop1: DO l2g_ifile = 1, l2g_nfile
 
!===============================================================================
! Open L2G file and grid.
!===============================================================================
    CALL OML3_OpenL2GFile(l2g_prodid, l2g_fname(l2g_ifile), &
                          l2g_fileid, l2g_gridid)
 
!===============================================================================
! Read file-level Metadata from L2G file.
!===============================================================================
    l2_orbit(1:20,l2g_ifile) = 0
    l2_orbper(1:20,l2g_ifile) = 0.0D0
    CALL OML3_ReadL2GFileMeta(l2g_fileid, &
                              l2_orbit(1:20,l2g_ifile), &
                              l2_orbper(1:20,l2g_ifile), &
                              l2g_month(l2g_ifile), l2g_day(l2g_ifile), &
                              l2g_year(l2g_ifile), l2g_dyofyr(l2g_ifile), &
                              l2g_TAI93at0z(l2g_ifile), &
                              l2g_startUTC(l2g_ifile), l2g_endUTC(l2g_ifile))
 
!===============================================================================
! Read ECS Metadata from L2G file.
!===============================================================================
    CALL OML3_ReadL2GECSMeta(l2g_ifile, &
                             l2_norb, l2_orb, l2_ecd, l2_ect, l2_ecl)
 
!===============================================================================
! Test if file-level Metadata orbit numbers do not match ECS Metadata orbit
! numbers.  If true, then end execution with fatal error.
!===============================================================================
    DO l2_iorb = 1, l2_norb(l2g_ifile)
 
      IF ( l2_orbit(l2_iorb,l2g_ifile) /= l2_orb(l2_iorb,l2g_ifile) ) THEN
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
        WRITE(stat_msg, '(A)') &
          "Fatal error due to file-level and ECS orbit number mismatch!"
        CALL OML3_EndInFailure(stat_msg, code_ref)
 
      END IF
      ! End of "IF ( l2_orbit(l2_iorb,l2g_ifile) /=" test.
 
    END DO
    ! End of "DO l2_iorb = 1, l2_norb(l2g_ifile)" loop.
 
!===============================================================================
! Read grid-level Metadata from L2G file.
!===============================================================================
    CALL OML3_ReadL2GGridMeta(l2g_gridid, l2g_nlon, l2g_nlat, l2g_ncand)
 
!===============================================================================
! Test if l2g_ifile is greater than 1.  If true, then test if dimensions of L2G
! grid have changed.
!===============================================================================
    IF (l2g_ifile > 1) THEN
 
!===============================================================================
! Test if dimensions of L2G grid have changed.  If true, then end execution with
! fatal error.
!===============================================================================
      IF (l2g_nlon  /= l2g_nlon_prev .OR. &
          l2g_nlat  /= l2g_nlat_prev .OR. &
          l2g_ncand /= l2g_ncand_prev) THEN
 
!===============================================================================
! Close L2G grid and file.
!===============================================================================
          CALL OML3_CloseL2GFile(l2g_fileid, l2g_gridid)

!===============================================================================
! End execution with fatal error.
!===============================================================================
          WRITE(stat_msg, '(A)') &
            "Fatal error due to change in dimensions of L2G grid!"
          CALL OML3_EndInFailure(stat_msg, code_ref)
 
      END IF
      ! End of "(l2g_nlon /= l2g_nlon_prev .OR." test.
 
    END IF
    ! End of "IF (l2g_ifile > 1)" test.
 
!===============================================================================
! Copy new L2G grid dimensions to "previous" dimensions.
!===============================================================================
    l2g_nlon_prev = l2g_nlon
    l2g_nlat_prev = l2g_nlat
    l2g_ncand_prev = l2g_ncand
 
!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML3_CloseL2GFile(l2g_fileid, l2g_gridid)
 
  END DO L2G_File_Loop1
  ! End of "DO l2g_ifile = 1, l2g_nfile" loop.
 
!===============================================================================
! Check whether L2G input files correspond to consecutive days.
!===============================================================================
  CALL OML3_CheckL2GInputs(l2g_nfile, &
                           l2g_month, l2g_day, l2g_year, l2g_dyofyr)
 
!===============================================================================
! Calculate some L2G and L3 temporal quantities.
!===============================================================================
  l2g_idyofyr = -1
  l3_month = -1
  l3_day = -1
  l3_year = -1
  DO l2g_ifile = 1, l2g_nfile
    IF (l2g_dyofyr(l2g_ifile) == l3_dyofyr) THEN
      l2g_idyofyr = l2g_ifile
      l3_month = l2g_month(l2g_ifile)
      l3_day = l2g_day(l2g_ifile)
      l3_year = l2g_year(l2g_ifile)
    END IF
  END DO
 
!===============================================================================
! Test if index of L2G file that corresponds to L3 day of year is equal to -1.
! If true, then end execution with fatal error.
!===============================================================================
  IF (l2g_idyofyr == -1) THEN
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
    WRITE(stat_msg, '(A)') "Fatal error:  ", &
      "None of the input L2G files correspond to L3 day of year!"
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_idyofyr == -1)" test.
 
!===============================================================================
! Calculate TAI93 time of UTC noon for OMI L3 day, and calculate TAI93 start and
! end times of data to be considered for OMI L3 day.
!===============================================================================
  l3_tnoon  = l2g_TAI93at0z(l2g_idyofyr) + 43200.0D0
  l3_tstart = l3_tnoon - 85500.0D0
  l3_tend   = l3_tnoon + 85500.0D0
 
!===============================================================================
! Convert equator crossing data for each L2 orbit into equator crossing mean
! solar and TAI93 times.
!===============================================================================
  CALL OML3_ConvertECT(l2g_nfile, l2_norb, &
                       l2_orb, l2_ecd, l2_ect, l2_ecl, &
                       l2_ech, l2_ecm, l2_ecs, l2_ecTAI93)
 
!===============================================================================
! Initialize L3 grid fields.
!===============================================================================
  l3_plmin(1:l3_nlon,1:l3_nlat) = 100.0
  l3_plminuv(1:l3_nlon,1:l3_nlat) = 100.0
  l3_sza(1:l3_nlon,1:l3_nlat) = -100.0
  l3_vza(1:l3_nlon,1:l3_nlat) = -100.0
  l3_cao3(1:l3_nlon,1:l3_nlat) = 0.0
  l3_rcf(1:l3_nlon,1:l3_nlat) = -100.0
  l3_uvai(1:l3_nlon,1:l3_nlat) = -100.0
 
!===============================================================================
! Loop through L2G files to read grid-field Metadata and grid fields for path
! index calculations.
!===============================================================================
  L2G_File_Loop2: DO l2g_ifile = 1, l2g_nfile
 
!===============================================================================
! Open L2G file and grid.
!===============================================================================
    CALL OML3_OpenL2GFile(l2g_prodid, l2g_fname(l2g_ifile), &
                          l2g_fileid, l2g_gridid)
 
!===============================================================================
! Read grid-field Metadata from L2G file.
!===============================================================================
    IF (l2g_ifile == 1) THEN
 
      CALL OML3_ReadL2GFieldInfo(l2g_gridid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names)
 
      CALL OML3_ReadL2GFieldMeta(l2g_gridid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names, &
                                 gfi1u_units, gfi1u_title, gfi1u_ufdef, &
                                 gfi1u_sfactor, gfi1u_offset, &
                                 gfi1u_vrange, gfi1u_mvalue, &
                                 gfi2_units, gfi2_title, gfi2_ufdef, &
                                 gfi2_sfactor, gfi2_offset, &
                                 gfi2_vrange, gfi2_mvalue, &
                                 gfi2u_units, gfi2u_title, gfi2u_ufdef, &
                                 gfi2u_sfactor, gfi2u_offset, &
                                 gfi2u_vrange, gfi2u_mvalue, &
                                 gfi4_units, gfi4_title, gfi4_ufdef, &
                                 gfi4_sfactor, gfi4_offset, &
                                 gfi4_vrange, gfi4_mvalue, &
                                 gfr4_units, gfr4_title, gfr4_ufdef, &
                                 gfr4_sfactor, gfr4_offset, &
                                 gfr4_vrange, gfr4_mvalue, &
                                 gfr8_units, gfr8_title, gfr8_ufdef, &
                                 gfr8_sfactor, gfr8_offset, &
                                 gfr8_vrange, gfr8_mvalue)

!===============================================================================
! Set missing value for UV aerosol index.
!===============================================================================
      mv_uvai = gfr4_mvalue(9)
 
    END IF
    ! End of "IF (l2g_ifile == 1)" test.

!===============================================================================
! Allocate memory for L2G fields.
!===============================================================================
    ALLOCATE(l2g_ncs(l2g_nlon,l2g_nlat))
    ALLOCATE(l2g_gpqf(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_lat(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_line(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_lon(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_orbit(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_pathl(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_raa(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_scene(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_sza(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_time(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_vza(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_cao3(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_qflg(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_rcf(l2g_nlon,l2g_nlat,l2g_ncand))
    ALLOCATE(l2g_uvai(l2g_nlon,l2g_nlat,l2g_ncand))
 
!===============================================================================
! Initialize L2G fields.
!===============================================================================
    l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
    l2g_gpqf(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2u_mvalue(1)
    l2g_lat(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
    l2g_line(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(1)
    l2g_lon(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(2)
    l2g_orbit(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(3)
    l2g_pathl(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(3)
    l2g_raa(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(4)
    l2g_scene(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(4)
    l2g_sza(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(5)
    l2g_time(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr8_mvalue(1)
    l2g_vza(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(6)
    l2g_cao3(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(7)
    l2g_qflg(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2u_mvalue(2)
    l2g_rcf(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(8)
    l2g_uvai(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(9)
 
!===============================================================================
! Read grid fields from L2G file.
!===============================================================================
    CALL OML3_ReadL2GGridFields(l2g_gridid, &
                                l2g_nlon, l2g_nlat, l2g_ncand, &
                                l2g_ncs, l2g_gpqf, l2g_lat, &
                                l2g_line, l2g_lon, l2g_orbit, &
                                l2g_pathl, l2g_raa, l2g_scene, &
                                l2g_sza, l2g_time, l2g_vza, &
                                l2g_cao3, l2g_qflg, l2g_rcf, &
                                l2g_uvai)
 
!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML3_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! Loop through L3 longitudes.
    !===============================================================================

    DO l3_ilon = 1, l3_nlon
 
!===============================================================================
! Loop through L3 latitudes.
!===============================================================================
       DO l3_ilat = 1, l3_nlat

 
!===============================================================================
! Guess at ranges for corresponding L2G indicies.
!===============================================================================
        l3_lat = 0.25 * FLOAT(l3_ilat - 1) - 89.875
        hw_lon = INT( 3.25 / COS(cfdtr * l3_lat) ) + 1
        l2g_ilon_beg = l3_ilon - hw_lon
        IF (l2g_ilon_beg <=   0) l2g_ilon_beg =    1
        l2g_ilon_end = l2g_ilon_beg + 2 * hw_lon
        IF (l2g_ilon_end > 1440) l2g_ilon_end = 1440
        l2g_ilat_beg = l3_ilat - 1
        IF (l2g_ilat_beg <=   0) l2g_ilat_beg =    1
        l2g_ilat_end = l2g_ilat_beg + 2
        IF (l2g_ilat_end >  720) l2g_ilat_end =  720
 
!===============================================================================
! Loop through L2G longitudes.
!===============================================================================
        L2G_Lon_Loop2: DO l2g_ilon = l2g_ilon_beg, l2g_ilon_end
 
!===============================================================================
! Loop through L2G latitudes.
!===============================================================================
          L2G_Lat_Loop2: DO l2g_ilat = l2g_ilat_beg, l2g_ilat_end
 
!===============================================================================
! Test if number of candidate L2 scenes in L2G grid cell is zero.  If true,
! then cycle to next L2G latitude.
!===============================================================================
            IF (l2g_ncs(l2g_ilon,l2g_ilat) == 0) CYCLE L2G_Lat_Loop2
 
!===============================================================================
! Loop through candidate scenes in L2G grid cell.
!===============================================================================
            L2G_Cand_Loop2: DO l2g_icand = 1, l2g_ncs(l2g_ilon,l2g_ilat)

!===============================================================================
! Test if time of candidate scene lies outside L3 range under consideration.  If
! true, then cycle to next candidate L2 scene.
!===============================================================================
              tocs = l2g_time(l2g_ilon,l2g_ilat,l2g_icand)
              IF (tocs <  l3_tstart .OR. &
                  tocs >= l3_tend) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Calculate longitude of midnight for L2 candidate scene.
!===============================================================================
              CALL OML3_MidnightLong(l2g_ifile, l2_norb, l2_orb, &
                                     l2_ecl, l2_ech, l2_ecm, &
                                     l2_ecs, l2_ecTAI93, &
                                     l2_orbper, &
                                     l2g_orbit(l2g_ilon,l2g_ilat,l2g_icand), &
                                     tocs, l2_lom)
 
!===============================================================================
! Test if date of L2 candidate scene is different from date of L3 day under
! consideration.  If true, then cycle to next candidate L2 scene.
!===============================================================================
 
!===============================================================================
! In morning of L3 day under consideration, test if longitude of L2 candidate
! scene is greater than or equal to -180.0 degrees and is less than longitude of
! midnight.  If true, then cycle to next candidate L2 scene.
!===============================================================================
              IF (tocs <  (l3_tnoon - 900.0D0)) THEN
                IF (l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) >= -180.0 .AND. &
                    l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) <   l2_lom) THEN
                  CYCLE L2G_Cand_Loop2
                END IF
              END IF
 
!===============================================================================
! In evening of L3 day under consideration, test if longitude of L2 candidate
! scene is greater than or equal to longitude of midnight and is less than 180.0
! degrees.  If true, then cycle to next candidate L2 scene.
!===============================================================================
              IF (tocs >= (l3_tnoon + 900.0D0)) THEN
                IF (l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) >=  l2_lom .AND. &
                    l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) <   180.0) THEN
                  CYCLE L2G_Cand_Loop2
                END IF
              END IF
 
!===============================================================================
! Test if L2G ground pixel quality flag is equal to fill value.  If true, then
! end execution with fatal error.
!===============================================================================
              IF (l2g_gpqf(l2g_ilon,l2g_ilat,l2g_icand) == -1) THEN
                WRITE(stat_msg, '(A)') "Error:  L2G ground ", &
                  "pixel quality flag is equal to fill value!"
                CALL OML3_EndInFailure(stat_msg, code_ref)
              END IF
 
!===============================================================================
! Test if L2G quality flag is equal to fill value.  If true, then end execution
! with fatal error.
!===============================================================================
              IF (l2g_qflg(l2g_ilon,l2g_ilat,l2g_icand) == -1) THEN
                WRITE(stat_msg, '(A)') "Error:  ", &
                  "L2G quality flag is equal to fill value!"
                CALL OML3_EndInFailure(stat_msg, code_ref)
              END IF
 
!===============================================================================
! Extract bit 5 (6th bit) of l2g_gpqf for candidate L2 scene.
!===============================================================================
              bit5 = IBITS(l2g_gpqf(l2g_ilon,l2g_ilat,l2g_icand), 5, 1)
 
!===============================================================================
! Test if bit 5 (solar eclipse possibility flag) of l2g_gpqf is set.  If true,
! then cycle to next candidate L2 scene.
!===============================================================================
              IF (bit5 /= 0) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Extract bit 6 (7th bit) of l2g_qflg for candidate L2 scene.
!===============================================================================
              bit6 = IBITS(l2g_qflg(l2g_ilon,l2g_ilat,l2g_icand), 6, 1)
 
!===============================================================================
! Test if bit 6 (row anomaly flag) of l2g_qflg is set.  If true, then cycle to
! next candidate L2 scene.
!===============================================================================
              IF (bit6 /= 0) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Test for spatial overlap of L2 candidate scene with L3 grid cell.
!===============================================================================
              hwd_lon = 0.004545 * &
                        fovkm(l2g_scene(l2g_ilon,l2g_ilat,l2g_icand)) / &
                        COS(cfdtr * l2g_lat(l2g_ilon,l2g_ilat,l2g_icand))
              l2s_lon_beg = l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) - hwd_lon
              !Adding rounding in to compare better with pre-ubuntu version
              l2s_lon_end = anint((l2s_lon_beg + 2 * hwd_lon)*10.**4)/10.0**4
              l3_lon_beg = 0.25 * FLOAT(l3_ilon - 1) - 180.0
              l3_lon_end = l3_lon_beg + 0.25


              IF (l2s_lon_beg > l3_lon_end) CYCLE L2G_Cand_Loop2
              IF (l2s_lon_end <  l3_lon_beg) CYCLE L2G_Cand_Loop2
 
              l2s_lat_beg = l2g_lat(l2g_ilon,l2g_ilat,l2g_icand) - 0.065
              !Adding rounding in to compare better with pre-ubuntu version
              l2s_lat_end = anint((l2s_lat_beg + 0.130)*10.**4)/10.0**4
              l3_lat_beg = 0.25 * FLOAT(l3_ilat - 1) - 90.0
              l3_lat_end = l3_lat_beg + 0.25

              IF (l2s_lat_beg > l3_lat_end) CYCLE L2G_Cand_Loop2
              IF (l2s_lat_end <  l3_lat_beg) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Calculate path index and length for candidate L2 scene.
!===============================================================================
              path_index = &
                1.0 / COS(cfdtr * l2g_sza(l2g_ilon,l2g_ilat,l2g_icand)) + &
                2.0 / COS(cfdtr * l2g_vza(l2g_ilon,l2g_ilat,l2g_icand))
              path_length = l2g_pathl(l2g_ilon,l2g_ilat,l2g_icand)
 
!===============================================================================
! Extract first four bits of l2g_qflg for candidate L2 scene.
!===============================================================================
              first4bitsA = IBITS(l2g_qflg(l2g_ilon,l2g_ilat,l2g_icand), 0, 4)
 
!===============================================================================
! Test if first four bits of l2g_qflg are not equal to 0 or 1.  If true, then
! skip column amount O3 and radiative cloud fraction for candidate L2 scene.
!===============================================================================
              IF (first4bitsA /= 0 .AND. &
                  first4bitsA /= 1) GO TO 10
 
!===============================================================================
! As final check, test if column amount O3 for candidate L2 scene is less than
! zero, which should never happen.  If true, then cycle to next cand. L2 scene.
!===============================================================================
              IF (l2g_cao3(l2g_ilon,l2g_ilat,l2g_icand) < 0.0) GO TO 10
              test_dummy = l2g_cao3(l2g_ilon,l2g_ilat,l2g_icand)
              IF (test_dummy .NE. test_dummy) GO TO 10
              IF ((test_dummy + 1.0) .EQ. test_dummy) GO TO 10
 
!===============================================================================
! Test if path length for candidate L2 scene is smaller than minimum path length
! for L3 grid cell.  If true, then update minimum path length, column amount O3
! and radiative cloud fraction for L3 grid cell.
!===============================================================================
              IF (path_length < l3_plmin(l3_ilon,l3_ilat)) THEN
                l3_plmin(l3_ilon,l3_ilat) = path_length
                l3_sza(l3_ilon,l3_ilat) = &
                  l2g_sza(l2g_ilon,l2g_ilat,l2g_icand)
                l3_vza(l3_ilon,l3_ilat) = &
                  l2g_vza(l2g_ilon,l2g_ilat,l2g_icand)
                l3_cao3(l3_ilon,l3_ilat) = &
                  l2g_cao3(l2g_ilon,l2g_ilat,l2g_icand)
                l3_rcf(l3_ilon,l3_ilat) = &
                  l2g_rcf(l2g_ilon,l2g_ilat,l2g_icand)
              END IF
 
!===============================================================================
! Test if first four bits of l2g_qflg are greater than or equal to 6.  If true,
! then cycle to next candidate L2 scene.
!===============================================================================
10            IF (first4bitsA >= 6) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Test if solar zenith angle is are greater than or equal to 70.0 degrees.  If
! true, then cycle to next candidate L2 scene.
!===============================================================================
              IF (l2g_sza(l2g_ilon,l2g_ilat,l2g_icand) >= 70.0) &
                CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Test if path index is greater than or equal to 7.0.  If true, then cycle to
! next candidate L2 scene.
!===============================================================================
              IF (path_index >= 7.0) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Extract first four bits of l2g_gpqf for candidate L2 scene.
!===============================================================================
              first4bitsB = IBITS(l2g_gpqf(l2g_ilon,l2g_ilat,l2g_icand), 0, 4)
 
!===============================================================================
! Calculate glint angle for candidate L2 scene.
!===============================================================================
              glint_angle = &
                ACOS(COS(cfdtr * l2g_sza(l2g_ilon,l2g_ilat,l2g_icand)) * &
                     COS(cfdtr * l2g_vza(l2g_ilon,l2g_ilat,l2g_icand)) + &
                     SIN(cfdtr * l2g_sza(l2g_ilon,l2g_ilat,l2g_icand)) * &
                     SIN(cfdtr * l2g_vza(l2g_ilon,l2g_ilat,l2g_icand)) * &
                     COS(cfdtr * l2g_raa(l2g_ilon,l2g_ilat,l2g_icand))) / cfdtr
 
!===============================================================================
! Test if first four bits of l2g_gpqf are not equal to 1 (i.e., not land) AND if
! glint angle is less than or equal to 20.0 degrees.  If both are true, then
! cycle to next candidate L2 scene.
!===============================================================================
              IF (first4bitsB /= 1 .AND. glint_angle <= 20.0) &
                CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Test if UV aerosol index for candidate L2 scene is equal to missing value.  If
! true, then cycle to next candidate L2 scene.
!===============================================================================
              test_dummy = ABS((l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand) - &
                                mv_uvai) / mv_uvai)
              IF (test_dummy <= 0.001) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! As final check, test if UV aerosol index for candidate L2 scene is less than
! -100.0, which should never happen.  If true, then cycle to next cand. L2 scene.
!===============================================================================
              IF (l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand) < -100.0) &
                                       CYCLE L2G_Cand_Loop2
              test_dummy = l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand)
              IF (test_dummy .NE. test_dummy) CYCLE L2G_Cand_Loop2
              IF ((test_dummy + 1.0) .EQ. test_dummy) CYCLE L2G_Cand_Loop2
 
!===============================================================================
! Test if path length for candidate L2 scene is smaller than minimum path length
! for L3 grid cell.  If true, then update minimum path length and UV aerosol
! index for L3 grid cell.
!===============================================================================
              IF (path_length < l3_plminuv(l3_ilon,l3_ilat)) THEN
                l3_plminuv(l3_ilon,l3_ilat) = path_length
                l3_uvai(l3_ilon,l3_ilat) = &
                  l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand)
              END IF
 
            END DO L2G_Cand_Loop2
            ! End of "DO l2g_icand = 1, l2g_ncs(l2g_ilon,l2g_ilat)" loop.
 
          END DO L2G_Lat_Loop2
          ! End of "DO l2g_ilat = l2g_ilat_beg, l2g_ilat_end" loop.
 
        END DO L2G_Lon_Loop2
        ! End of "DO l2g_ilon = l2g_ilon_beg, l2g_ilon_end" loop.
 
      END DO
      ! End of "DO l3_ilat = 1, l3_nlat" loop.
 
    END DO
    ! End of "DO l3_ilon = 1, l3_nlon" loop.
 
!===============================================================================
! Deallocate memory used for L2G fields.
!===============================================================================
    DEALLOCATE(l2g_ncs)
    DEALLOCATE(l2g_gpqf)
    DEALLOCATE(l2g_lat)
    DEALLOCATE(l2g_line)
    DEALLOCATE(l2g_lon)
    DEALLOCATE(l2g_orbit)
    DEALLOCATE(l2g_pathl)
    DEALLOCATE(l2g_raa)
    DEALLOCATE(l2g_scene)
    DEALLOCATE(l2g_sza)
    DEALLOCATE(l2g_time)
    DEALLOCATE(l2g_vza)
    DEALLOCATE(l2g_cao3)
    DEALLOCATE(l2g_qflg)
    DEALLOCATE(l2g_rcf)
    DEALLOCATE(l2g_uvai)
 
  END DO L2G_File_Loop2
  ! End of "DO l2g_ifile = 1, l2g_nfile" loop.
 
!===============================================================================
! Write out ASCII TOMS-like L3 files.
!===============================================================================
  CALL OML3_WriteL3FilesASCII(l3_fname(1:3), l3_dyofyr, &
                              l3_month, l3_day, l3_year, &
                              l3_nlon, l3_nlat, &
                              l3_ncs, l3_cao3, l3_rcf, &
                              l3_ncsuv, l3_uvai, &
                              l2g_nfile, l2g_dyofyr, &
                              l2_norb, l2_ech, l2_ecm, l2_ecs)
 
!===============================================================================
! Write out HDF-EOS 5 L3 file:
!===============================================================================
 
!===============================================================================
! Create L3 file, and create grid structure within file.
!===============================================================================
  CALL OML3_OpenL3File(l2g_prodid, l3_fname(4), &
                       l3_nlon, l3_nlat, &
                       l3_fileid, l3_gridid)
 
!===============================================================================
! Write file-level Metadata to L3 file.
!===============================================================================
  CALL OML3_WriteL3FileMeta(l3_fileid, &
                            l2_norb, l2_orb, l2_orbper, &
                            l3_month, l3_day, l3_year, l3_dyofyr, &
                            l2g_TAI93at0z(l2g_idyofyr), l3_pgevers, &
                            l3_tstart, l3_tend)
 
!===============================================================================
! Write grid-level Metadata to L3 file.
!===============================================================================
  CALL OML3_WriteL3GridMeta(l3_gridid, l3_nlon, l3_nlat)
 
!===============================================================================
! Write grid fields to L3 file.
!===============================================================================
  CALL OML3_WriteL3GridFields(l3_gridid, l3_nlon, l3_nlat, &
                              gfr4_mvalue(5), l3_sza, &
                              gfr4_mvalue(6), l3_vza, &
                              gfr4_mvalue(7), l3_cao3, &
                              gfr4_mvalue(8), l3_rcf, &
                              gfr4_mvalue(9), l3_uvai)
 
!===============================================================================
! Write grid-field Metadata to L3 file.
!===============================================================================
  CALL OML3_WriteL3FieldMeta(l3_gridid, &
                             gfr4_nfield, gfr4_names, &
                             gfr4_units, gfr4_title, gfr4_ufdef, &
                             gfr4_sfactor, gfr4_offset, &
                             gfr4_vrange, gfr4_mvalue)
 
!===============================================================================
! Detach from grid structure in L3 file and close file.
!===============================================================================
  CALL OML3_CloseL3File(l3_fileid, l3_gridid)
 
!===============================================================================
! End OML3 execution normally with non-fatal error(s) and exit code = 0.
!===============================================================================
  WRITE(stat_msg, '(A)') &
    "OML3 execution ends normally with non-fatal error(s) and exit code = 0."
  call_stat = OMI_SMF_setmsg(OMI_S_SUCCESS, stat_msg, code_ref, 0)
  CALL EXIT(0)

END PROGRAM OMTO3e
