!===============================================================================
!
! SUBROUTINE OML3_OpenL3File
!
! High Level Overview:
!   This subroutine is called by OML3_Main to create an OMI L3 product file, and
!   create a grid structure within the file.
!
! The First Four Arguments Are Inputs:
!   l2g_prodid          - L2G product ID.
!   l3_fname            - L3 file name.
!   l3_nlon             - Number of longitudes in L3 grid.
!   l3_nlat             - Number of latitudes in L3 grid.
!
! The Final Two Arguments Are Outputs:
!   l3_fileid           - L3 file ID.
!   l3_gridid           - L3 grid ID.
!
! OML3 Subroutine Called:
!   OML3_EndInFailure   - Ends OML3 execution in failure.
!
! HDF-EOS 5 Functions Called:
!   he5_gdopen          - Opens HE5 grid file.
!   he5_gdcreate        - Creates new grid in HE5 file.
!   he5_gddefpreg       - Defines pixel registration for grid.
!   he5_gddefdim        - Defines dimension for grid.
!   he5_gddeforigin     - Defines origin of grid.
!   he5_gddefproj       - Defines projection of grid.
!
! Author(s):
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: Peter_Leonard@ssaihq.com
!   Phone: (301) 867-2190
!
! Initial Version:
!   April 28, 2006
!
! Revision History:
!
!===============================================================================
 
SUBROUTINE OML3_OpenL3File(l2g_prodid, l3_fname, &
                           l3_nlon, l3_nlat, &
                           l3_fileid, l3_gridid)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'               ! Declarations for HDF-EOS 5.

!===============================================================================
! Declarations of input arguments.
!===============================================================================
  CHARACTER (LEN=8), &
    INTENT(IN) :: l2g_prodid          ! L2G product ID.
  CHARACTER (LEN=256), &
    INTENT(IN) :: l3_fname            ! L3 file name.
  INTEGER (KIND=8), &
    INTENT(IN) :: l3_nlon             ! Number of longitudes in L3 grid.
  INTEGER (KIND=8), &
    INTENT(IN) :: l3_nlat             ! Number of latitudes in L3 grid.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: l3_fileid          ! L3 file ID.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l3_gridid          ! L3 grid ID.
 
!===============================================================================
! Declarations of local arguments.
!===============================================================================
  CHARACTER (LEN=256) :: l3_gname     ! L3 grid name.
  REAL (KIND=8), &
    DIMENSION(2) :: upp_lft_pt        ! Upper left point for he5_gdcreate.
  REAL (KIND=8), &
    DIMENSION(2) :: low_rgt_pt        ! Lower right point for he5_gdcreate.
  INTEGER (KIND=4) :: dummy1          ! Dummy argument 1 for he5_gddefproj.
  INTEGER (KIND=4) :: dummy2          ! Dummy argument 2 for he5_gddefproj.
  REAL (KIND=8) :: dummy3             ! Dummy argument 3 for he5_gddefproj.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat       ! Function call status.
  CHARACTER (LEN=256) :: stat_msg     ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML3_OpenL3File.f90"  ! Code file reference.
 
!===============================================================================
! Declarations of HDF-EOS 5 function calls.
!===============================================================================
  INTEGER (KIND=4), EXTERNAL :: he5_gdopen       ! Opens HE5 grid file.
  INTEGER (KIND=4), EXTERNAL :: he5_gdcreate     ! Creates new grid in HE5 file.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefpreg    ! Defines pixel reg. for grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefdim     ! Defines dimension for grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddeforigin  ! Defines origin of grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefproj    ! Defines projection of grid.
 
!===============================================================================
! Open L3 grid file.
!===============================================================================
  l3_fileid = he5_gdopen(l3_fname, HE5F_ACC_TRUNC)
 
!===============================================================================
! Test if L3 file ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l3_fileid < 0) THEN
 
    WRITE(stat_msg, 5) l3_fileid
5   FORMAT("Fatal error:  L3 file ID =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l3_fileid < 0)" test.
 
!===============================================================================
! Determine name of L3 grid.
!===============================================================================
  IF (l2g_prodid == "OMTO3G") l3_gname = "OMI Column Amount O3"
 
!===============================================================================
! Set extreme upper left and lower right points of grid (format is such that
! -180000000.0d0 = -180 degrees 000 minutes 000.0d0 seconds, etc...).
!===============================================================================

  ! correcting grid pointers for OMI north to south grids - MB 06/20/24
  upp_lft_pt(1) = -180000000.0d0
  upp_lft_pt(2) =  -90000000.0d0
  low_rgt_pt(1) =  180000000.0d0
  low_rgt_pt(2) =   90000000.0d0
!  upp_lft_pt(1) = -180000000.0d0
!  upp_lft_pt(2) =   90000000.0d0
!  low_rgt_pt(1) =  180000000.0d0
!  low_rgt_pt(2) =  -90000000.0d0
 
!===============================================================================
! Create L3 grid in L3 file.
!===============================================================================
  l3_gridid = he5_gdcreate(l3_fileid, l3_gname, &
                           l3_nlon, l3_nlat, &
                           upp_lft_pt, low_rgt_pt)
 
!===============================================================================
! Test if L3 grid ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l3_gridid < 0) THEN
 
    WRITE(stat_msg, 10) l3_gridid
10  FORMAT("Fatal error:  L3 grid ID =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l3_gridid < 0)" test.
 
!===============================================================================
! Define pixel registration in L3 grid to be pixel centers.
!===============================================================================
  call_stat = he5_gddefpreg(l3_gridid, HE5_HDFE_CENTER)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 15) call_stat
15  FORMAT("Fatal error:  he5_gddefpreg call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define longitude dimension in L3 grid.
!===============================================================================
  call_stat = he5_gddefdim(l3_gridid, "XDim", l3_nlon)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 20) call_stat
20  FORMAT("Fatal error:  longitude he5_gddefdim call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define latitude dimension in L3 grid.
!===============================================================================
  call_stat = he5_gddefdim(l3_gridid, "YDim", l3_nlat)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 25) call_stat
25  FORMAT("Fatal error:  latitude he5_gddefdim call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define origin of L3 grid.
!===============================================================================
  ! removing grid origin setting to keep it from metadata - MB 08/02/2024
!  call_stat = he5_gddeforigin(l3_gridid, HE5_HDFE_GD_LL)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 30) call_stat
30  FORMAT("Fatal error:  he5_gddeforigin call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define projection of L3 grid.
!===============================================================================
  call_stat = he5_gddefproj(l3_gridid, HE5_GCTP_GEO, dummy1, dummy2, dummy3)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 35) call_stat
35  FORMAT("Fatal error:  he5_gddefproj call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML3_OpenL3File
