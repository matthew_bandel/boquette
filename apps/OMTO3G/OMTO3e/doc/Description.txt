APP Name:    OMTO3e
APP Type:    OMI
APP Version: 1.1.1
 

Long Name: OMI/Aura Ozone (O3) Total Column Daily L3 Global 0.25deg Lat/Lon Grid
 

Lead Algorithm Scientist:   R.D. McPeters (NASA/GSFC)
Other Algorithm Scientists: P. Leonard (ADNET)
App Developer:              P. Leonard (ADNET)
Support Contact:            P. Leonard (ADNET)


Description: >
 
 Creates TOMS-like daily L3 files from (one to) three consecutive OMTO3G
 (daily L2G) files.


Structure: >
 
 Run OMTO3e on (one to) three consecutive OMTO3G (daily L2G) files.


Operational Scenario: >
 
 Run OMTO3e on (one to) three consecutive OMTO3G (daily L2G) files.


Period: Days=1


Output Files:

 - LUN:      320011
   ESDT:     OMTO3etoz
   Filename: OMI-Aura_L3-OMTO3etoz_<Date>_v<ECSCollection>-<ProductionTime>.txt
   Desc:     TOMS-like L3 daily total column amount ozone file.

 - LUN:      320012
   ESDT:     OMTO3ercf
   Filename: OMI-Aura_L3-OMTO3ercf_<Date>_v<ECSCollection>-<ProductionTime>.txt
   Desc:     TOMS-like L3 daily radiative cloud fraction file.

 - LUN:      320014
   ESDT:     OMTO3e
   Filename: OMI-Aura_L3-OMTO3e_<Date>_v<ECSCollection>-<ProductionTime>.he5
   Desc:     L3 daily total column ozone, radiative cloud fraction and UV aerosol index file.
 
 
Support Output Files:
#  None


Static Input Files:
#  None


Dynamic Input Files:
 
 - LUN:  10301
   ESDT: LEAPSECT
   Rule: Required
   Desc: Leap Seconds - ECS internal format for Toolkit
 
 - LUN:  300011
   ESDT: OMTO3G
   Rule: Required
   Desc: OMI/Aura Corrected Ozone (O3) Total Column Daily L2 Global 0.25deg Lat/Lon Grid
 

Runtime Parameters:

 - LUN:   10220
   Param: Toolkit version string
   Value: 'SCF  TK5.2.13'

 - LUN:   200011
   Param: L2GprodID
   Value: '"OMTO3G"'

 - LUN:   200100
   Param: SMF Verbosity Threshold
   Value: 2

 - LUN:   200105
   Param: PGEVERSION
   Value: '"<PGEVersion>"'

 - LUN:   200109
   Param: Day
   Value: '<Day>'
   Desc:  Julian day of year.


Production Rules:
 
 - Rule:       Common
   ESDT:       LEAPSECT

 - Rule:       PGEKeyDailyParams

 - Rule:       L3Match3Days
   ESDT:       OMTO3G
   Max_files:  3
   IngestWait: '72 hours'
 
 
Dependencies:
 
 - CI: OMI_SMF_Wrapper
   Version: 1.0.1
