Shortname:    OMTO3G
Longname:     OMI/Aura Ozone (O3) Total Column Daily L2 Global 0.25deg Lat/Lon Grid
PFS Version:  1.0.5
Date:         19 June 2010
Author(s):    Peter Leonard (ADNET)

PGE Version:               1.0.5
Lead Algorithm Scientist:  Pawan K. Bhartia (NASA/GSFC)
Lead Algorithm Developer:  Peter Leonard (ADNET)
Lead PGE Developer:        Peter Leonard (ADNET)
PGE Developer(s):          Peter Leonard (ADNET)

Description: >
  
 This document specifies the format of the Ozone Monitoring Instrument (OMI)
 OMTO3G product, which is the daily Level 2G (L2G) gridded data product that
 corresponds to the OMTO3 product.  The latter is the U.S. OMI Science Team's
 total column ozone orbital Level 2 (L2) swath data product (Reference 1).
  
 The L2G product contains 24 UTC hours of L2 product subsetted onto a
 longitude-latitude grid.
  
 An OMI L2G day is defined to be the 24 hours that lie between UTC times of
 0 hours, 0 minutes, 0 seconds and 23 hours, 59 minutes, 59.999999 seconds.
  
 The L2G product contains the data for all L2 "scenes" that
 1) have observation times that lie within the L2G day in question,
 2) have centers that lie within the L2G grid cell in question, and
 3) are "good".
  
 A "good" OMTO3 L2 scene is defined as one that has
 i)  a solar zenith angle that is less than or equal to 88.0 degrees, and
 ii) an ozone column amount that is not equal to the missing value.
  
 The adopted L2G grid is a 0.25-degree by 0.25-degree grid in longitude and
 latitude.  The dimensions of this grid are 1440 by 720.  The origin of the
 grid is at lower left.  That is, the grid cell at coordinates (1, 1)
 is centered at (longitude = -179.875 , latitude = -89.875),
 and the grid cell at coordinates (1440, 720)
 is centered at (longitude =  179.875 , latitude =  89.875).
  
 The adopted L2G grid is consistent with the document entitled "Definition
 of OMI Grids for Level 3 and Level 4 Data Products" by J.P. Veefkind et al.
 (Reference 2).
  
 The L2G product currently excludes L2 data collected in spatial and spectral
 zoom modes.
  
 Each "good" OMTO3 L2 scene is mapped onto only one L2G grid cell.
  
 The number of L2 scenes that are mapped onto a given L2G grid cell can range
 from 0 to 12, and the corresponding data are stored in an additional dimension
 of the grid.
  
 The L2 data are not averaged or weighted in any way in the L2G product.
  
 The product is stored as one HDF-EOS 5 grid file, and has a size of 150 MB.
  
 The format of the L2G product files is consistent with the document entitled
 "HDF-EOS Aura File Format Guidelines" by C. Craig et al. (Reference 3).
  
Global Metadata:
 
 - Metadata Name:     EndUTC
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    UTC at the end of the L2G granule in "YYYY-MM-DDT23:59:59.999999Z" format.
 
 - Metadata Name:     FirstLineInOrbit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,16
   Minimum Value:     1
   Maximum Value:     1700
   Data Source:       PGE
   Description: >
    The first line number in each L2 orbit that contributes to the L2G granule.
 
 - Metadata Name:     GranuleDay
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     31
   Data Source:       PGE
   Description:       The day of the month at the start of the L2G granule.
 
 - Metadata Name:     GranuleDayOfYear
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     366
   Data Source:       PGE
   Description:       The day of the year at the start of the L2G granule.

 - Metadata Name:     GranuleMonth
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     12
   Data Source:       PGE
   Description:       The month of the year at the start of the L2G granule.

 - Metadata Name:     GranuleYear
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     2000
   Maximum Value:     2099
   Data Source:       PGE
   Description:       The (four-digit) year at the start of the L2G granule.

 - Metadata Name:     HDFEOSVersion
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       HE
   Description: >
    The version of HDF-EOS 5 used in production.  Example is "HDFEOS_5.1.8".

 - Metadata Name:     InstrumentName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI
   Data Source:       PGE
   Description:       Actual is "OMI" (see Section 6.1 of Reference 3).
 
 - Metadata Name:     LastLineInOrbit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,16
   Minimum Value:     1
   Maximum Value:     1700
   Data Source:       PGE
   Description: >
    The last line number in each L2 orbit that contributes to the L2G granule.
 
 - Metadata Name:     NumberOfLinesMissingGeolocation
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,16
   Minimum Value:     1
   Maximum Value:     1700
   Data Source:       PGE
   Description: >
    The number of lines in each L2 orbit that are missing geolocation (a.k.a.
    number of "bad" lines in each L2 file).
 
 - Metadata Name:     OrbitNumber
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,16
   Minimum Value:     1
   Maximum Value:     999999
   Data Source:       L2
   Description:       The OMI orbit number for each L2 input granule.
 
 - Metadata Name:     OrbitPeriod
   Mandatory:         T
   Data Type:         HE5T_NATIVE_DOUBLE
   Number of Values:  1,16
   Minimum Value:     5000.0
   Maximum Value:     7000.0
   Data Source:       PGE
   Description:       The Aura orbital period for each L2 input granule.
 
 - Metadata Name:     Period
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Daily,Weekly,Monthly
   Data Source:       PGE
   Description:       The duration of the L2G granule.  Actual is "Daily".

 - Metadata Name:     PGEVERSION
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PCF
   Description:       Example is "1.0.5" (see Appendix K of Reference 4).

 - Metadata Name:     ProcessLevel
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            2G
   Data Source:       PGE
   Description:       Actual is "2G".

 - Metadata Name:     QAPercentMissingData
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1,16
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       L2
   Description: >
    The percent of Level 1B calibrated radiance data that is missing from each
    L2 input granule.
 
 - Metadata Name:     StartUTC
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    UTC at the start of the L2G granule in "YYYY-MM-DDT00:00:00.000000Z" format.

 - Metadata Name:     TAI93At0zOfGranule
   Mandatory:         T
   Data Type:         HE5T_NATIVE_DOUBLE
   Number of Values:  1
   Minimum Value:     0.0
   Maximum Value:     1.0e+30
   Data Source:       PGE
   Description: >
    The TAI93 time at 0z of the L2G granule (see Section 6.1 of Reference 3).
 
Grid Metadata:
 
 - Metadata Name:     GCTPProjectionCode
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     99
   Data Source:       PGE
   Description: >
    The GCTP projection code of the L2G grid.  Actual is 0, which corresponds
    to the geographic projection.
 
 - Metadata Name:     GridName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI Column Amount O3
   Data Source:       PGE
   Description:       Actual is "OMI Column Amount O3".
 
 - Metadata Name:     GridOrigin
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Center
   Data Source:       PGE
   Description: >
    The origin of the L2G grid.  Actual is "Center" (see Section 6.2 of
    Reference 3).
 
 - Metadata Name:     GridSpacing
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Spacing of L2G grid (in degrees).  Actual is "(0.25,0.25)".
 
 - Metadata Name:     GridSpacingUnit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            deg
   Data Source:       PGE
   Description: >
    Unit for GridSpacing.  Actual is "deg".
 
 - Metadata Name:     GridSpan
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Span of L2G grid (in degrees).  Actual is "(-180,180,-90,90)".
 
 - Metadata Name:     GridSpanUnit
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            deg
   Data Source:       PGE
   Description: >
    Unit for GridSpan.  Actual is "deg".
 
 - Metadata Name:     MaximumNumberOfCandidatesPerGridCell
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     15
   Data Source:       PGE
   Description: >
    The maximum number of L2 scenes per cell in the L2G grid (this can be as
    large as 12).
 
 - Metadata Name:     MinimumNumberOfCandidatesPerGridCell
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     15
   Data Source:       PGE
   Description: >
    The minimum number of L2 scenes per cell in the L2G grid (this is typically
    0, because empty L2G grid cells are quite common).
 
 - Metadata Name:     NumberOfEmptyGridCells
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1036800
   Data Source:       PGE
   Description: >
    The number of cells in the L2G grid that do not contain any L2 scenes.
 
 - Metadata Name:     NumberOfDuplicateScenesAcceptedIntoGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1500000
   Data Source:       PGE
   Description: >
    The number of L2 scenes accepted into L2G grid cells that already contain
    one or more L2 scenes.
 
 - Metadata Name:     NumberOfGridCells
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     1036800
   Data Source:       PGE
   Description:       The total number of cells in the L2G grid.
 
 - Metadata Name:     NumberOfLatitudesInGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     720
   Data Source:       PGE
   Description:       The number of latitude bins in the L2G grid.
 
 - Metadata Name:     NumberOfLongitudesInGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     1440
   Data Source:       PGE
   Description:       The number of longitude bins in the L2G grid.
 
 - Metadata Name:     NumberOfMultiplyPopulatedGridCells
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1036800
   Data Source:       PGE
   Description: >
    The number of cells in the L2G grid that contain two or more L2 scenes.
 
 - Metadata Name:     NumberOfPopulatedGridCells
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1036800
   Data Source:       PGE
   Description: >
    The number of cells in the L2G grid that contain one or more L2 scenes.
 
 - Metadata Name:     NumberOfScenesAcceptedIntoGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1500000
   Data Source:       PGE
   Description:       The number of L2 scenes accepted into the L2G grid.
 
 - Metadata Name:     NumberOfScenesConsideredForGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1500000
   Data Source:       PGE
   Description:       The number of L2 scenes considered for the L2G grid.
 
 - Metadata Name:     NumberOfScenesRejectedFromGrid
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1500000
   Data Source:       PGE
   Description:       The number of L2 scenes rejected from the L2G grid.
 
 - Metadata Name:     Projection
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Geographic
   Data Source:       PGE
   Description: >
    The map projection of the L2G grid.  Actual is "Geographic" (see
    Section 6.2 of Reference 3).
 
Grid Dimensions:
 
 - Dimension Name:    nCandidate
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     15
   Data Source:       PGE
   Description: >
    The L2-candidate-scenes dimension of the L2G grid.  The size of this
    dimension is currently set at 15.
 
 - Dimension Name:    nLayers
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     7
   Data Source:       PGE
   Description: >
    The ozone-profile-layers dimension of the L2G grid.  Only the first seven
    layers of OMTO3 are reproduced in OMTO3G.

 - Dimension Name:    nWavel
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     12
   Data Source:       PGE
   Description: >
    The wavelengths dimension of the L2G grid.  There are currently twelve
    wavelengths in both OMTO3 and OMTO3G.
 
 - Dimension Name:    XDim
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     1440
   Data Source:       PGE
   Description: >
    The longitudes dimension of the L2G grid.  There are currently 1440
    0.25-degree-wide bins between longitudes -180.0 and 180.0 degrees.
 
 - Dimension Name:    YDim
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     720
   Data Source:       PGE
   Description: >
    The latitudes dimension of the L2G grid.  There are currently 720
    0.25-degree-wide bins between latitudes -90.0 and 90.0 degrees.

Geolocation Fields:
 
 - Field Name:               GroundPixelQualityFlags
   Data Type:                HE5T_NATIVE_UINT16
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            65534
   Missing Value:            65535
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Ground Pixel Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The ground pixel quality flags for each L2 candidate scene in each L2G grid
    cell:
 
    Bits 0 to 3 together contain the land/water flags:
      0    - shallow ocean
      1    - land
      2    - shallow inland water
      3    - ocean coastline/lake shoreline
      4    - ephemeral (intermittent) water
      5    - deep inland water
      6    - continental shelf ocean
      7    - deep ocean
      8-14 - not used
      15   - error flag for land/water
 
    Bits 4 to 6 are flags that are set to 0 for FALSE, or 1 for TRUE:
      Bit 4 - sun glint possibility flag
      Bit 5 - solar eclipse possibility flag
      Bit 6 - geolocation error flag
 
    Bit 7 is reserved for future use (currently set to 0).
 
    Bits 8 to 14 together contain the snow/ice flags (based on NISE):
      0       - snow-free land
      1-100   - sea ice concentration (percent)
      101     - permanent ice (Greenland, Antarctica)
      102     - not used
      103     - dry snow
      104     - ocean (NISE-255)
      105-123 - reserved for future use
      124     - mixed pixels at coastline (NISE-252)
      125     - suspect ice value (NISE-253)
      126     - corners undefined (NISE-254)
      127     - error
 
    Bit 15 - NISE nearest neighbor filling flag.
    (See Section 6.2 of Reference 5 for more details.)
 
 - Field Name:               Latitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -90.0
   Maximum Value:            90.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2
   Title:                    Geodetic Latitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The geodetic latitude (in degrees) on the ground at the center of each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               LineNumber
   Data Type:                HE5T_NATIVE_INT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            1
   Maximum Value:            1700
   Missing Value:            -2000000000
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Line Number of Candidate Scene
   Unique Field Definition:  OMI-Specific
   Description: >
    The line number for each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               Longitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2
   Title:                    Geodetic Longitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The geodetic longitude (in degrees) on the ground at the center of each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               NumberOfCandidateScenes
   Data Type:                HE5T_NATIVE_INT
   Dimensions:               YDim,XDim
   Minimum Value:            0
   Maximum Value:            15
   Missing Value:            0
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Number of Candidate Scenes
   Unique Field Definition:  OMI-Specific
   Description: >
    The number of L2 candidate scenes in each L2G grid cell.
 
 - Field Name:               OrbitNumber
   Data Type:                HE5T_NATIVE_INT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            1
   Maximum Value:            999999
   Missing Value:            -2000000000
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Orbit Number of Candidate Scene
   Unique Field Definition:  OMI-Specific
   Description: >
    The orbit number for each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               PathLength
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            2.0
   Maximum Value:            100.0
   Missing Value:            1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Path Length
   Unique Field Definition:  OMI-Specific
   Description: >
    The path length [= sec(solar zenith angle) + sec(viewing zenith angle)] for
    each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               RelativeAzimuthAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg(EastofNorth)
   Data Source:              L2
   Title:                    Relative Azimuth Angle (sun + 180 - view)
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The relative (sun + 180 - view) azimuth angle (in degrees) on the ground at
    the center of each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               SceneNumber
   Data Type:                HE5T_NATIVE_INT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            1
   Maximum Value:            60
   Missing Value:            -2000000000
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Scene Number of Candidate Scene
   Unique Field Definition:  OMI-Specific
   Description: >
    The cross-track ground-pixel number for each L2 candidate scene in each L2G
    grid cell.
 
 - Field Name:               SecondsInDay
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            86401.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    s
   Data Source:              L2
   Title:                    Seconds after UTC midnight
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The time after UTC midnight (in seconds) for each L2 candidate scene in each
    L2G grid cell.
 
 - Field Name:               SolarZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2
   Title:                    Solar Zenith Angle
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The solar zenith angle (in degrees) on the ground at the center of each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               TerrainHeight
   Data Type:                HE5T_NATIVE_INT16
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -200
   Maximum Value:            10000
   Missing Value:            -32767
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    m
   Data Source:              L2
   Title:                    Terrain Height
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The terrain height (in meters) at the center of each L2 candidate scene in
    each L2G grid cell.

 - Field Name:               Time
   Data Type:                HE5T_NATIVE_DOUBLE
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -5.0e+09
   Maximum Value:            1.0e+10
   Missing Value:            -1.2676506002282294e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    s
   Data Source:              L2
   Title:                    Time at Start of Scan (TAI93)
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The TAI93 time (in seconds) for each L2 candidate scene in each L2G grid
    cell.

 - Field Name:               ViewingZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            70.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L2
   Title:                    Viewing Zenith Angle
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The viewing zenith angle (in degrees) on the ground at the center of each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               XTrackQualityFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Cross Track Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The cross track quality flags assigned to each pixel in  
    OMI L1B data. Flags indicate detection of the OMI row 
    anomaly and if the effect has been corrected. 
 
    Bits 0 to 2 together indicate row anomaly status: 
      0 - Not affected
      1 - Affected, Not corrected, do not use
      2 - Slightly affected, not corrected, use with caution
      3 - Affected, corrected, use with caution
      4 - Affected, corrected, use pixel
      5 - Not used
      6 - Not used
      7 - Error during anomaly detection processing
    Bit 3 - Reserved for future use.
    Bit 4 - Possibly affected by wavelength shift
    Bit 5 - Possibly affected by blockage
    Bit 6 - Possibly affected by stray sunlight
    Bit 7 - Possibly affected by stray earthshine
 
Data Fields:
 
 - Field Name:               AlgorithmFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            13
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Algorithm Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The algorithm flags for each L2 candidate scene in each L2G grid cell:
      0 - skipped
      1 - standard
      2 - adjusted for profile shape
      3 - based on C-pair (331 and 360 nm)
      Add 10 for snow/ice.
 
 - Field Name:               APrioriLayerO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,nLayers,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            125.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              L2
   Title:                    A Priori Ozone Profile
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The a priori layer ozone values (in DU) for each L2 candidate scene in each
    L2G grid cell.
 
 - Field Name:               CloudPressure
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            1013.25
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    hPa
   Data Source:              L2
   Title:                    Effective Cloud Pressure
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The effective cloud pressure (in hPa) for each L2 candidate scene in each
    L2G grid cell.
 
 - Field Name:               ColumnAmountO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              L2
   Title:                    Best Total Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The best total ozone solution (in DU) for each L2 candidate scene in each
    L2G grid cell.
 
 - Field Name:               InstrumentConfigurationId
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            200
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Unique Field Definition:  OMI-Specific
   Title:                    Instrument Configuration ID
   Description: >
    Instrument Configuration ID for each L2 candidate scene in each L2G grid
    cell.
 
 - Field Name:               LayerEfficiency
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,nLayers,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            10.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Algorithmic Layer Efficiency
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The algorithmic layer efficiencies for each L2 candidate scene in each L2G
    grid cell.
 
 - Field Name:               MeasurementQualityFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Measurement Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The measurement quality flags for each L2 candidate scene in each L2G grid
    cell (Bit value is 0 for not set and 1 for set):
      Bit 0 - Measurement Missing Flag:  All ground pixels have L1B
              PixelQualityFlags bit 0 set.
      Bit 1 - Measurement Error Flag:  Any of L1B MeasurementQualityFlags bit 0,
              1 or 3 are set for radiance or solar product used.
      Bit 2 - Measurement Warning Flag:  Any of L1B MeasurementQualityFlags
              bit 2, 4, 5, 8, 9 are set for radiance or solar product used.
      Bit 3 - Rebinned Measurement Flag:  L1B radiance MeasurementQualityFlags
              bit 7 is set.
      Bit 4 - SAA Flag:  L1B MeasurementQualityFlags bit 10 is set for radiance
              or solar product used.
      Bit 5 - Spacecraft Maneuver Flag:  L1B MeasurementQualityFlags bit 11 is
              set for radiance or solar product used.
      Bit 6 - Instrument Setting Error Flag:  Values for Earth and solar
              InstrumentConfigurationId are not compatible.
      Bit 7 - To be defined (currently set to 0).
 
 - Field Name:               NumberSmallPixelColumns
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            5
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Number of Small Pixel Columns
   Unique Field Definition:  OMI-Specific
   Description: >
    The number of small-pixel-column co-additions for each L2 candidate scene in
    each L2G grid cell.  This is either 0 (for no or invalid small pixel column
    selected) or the number of co-additions.
 
 - Field Name:               O3BelowCloud
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            100.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              L2
   Title:                    Ozone Below Fractional Cloud
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The ozone amount below fractional cloud (in DU) for each L2 candidate scene
    in each L2G grid cell.
 
 - Field Name:               QualityFlags
   Data Type:                HE5T_NATIVE_UINT16
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            65534
   Missing Value:            65535
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The quality flags for each L2 candidate scene in each L2G grid cell:
 
    Bits 0 to 3 together contain several output error flags:
      0 - good sample
      1 - glint contamination (corrected)
      2 - sza > 84 (degree)
      3 - 360 residual > threshold
      4 - residual at unused ozone wavelength > 4 sigma
      5 - SOI > 4 sigma (SO2 present)
      6 - non-convergence
      7 - abs(residual) > 16.0 (fatal)
      Add 8 for descending data.
 
    Bits 4 to 5 are reserved for future use (currently set to 0).
 
    Bit 6 - set to 0 when data are unaffected by OMI instrument row anomaly, 
            set to 1 when data are affected.
 
    Bit 7 - set to 0 when OMI cloud (OMCLDRR or OMCLDO2) pressure is used,
            set to 1 when climatological cloud pressure is used. 
 
    Bits 8 to 15 are flags that are set to 0 for FALSE (good value), or
    1 for TRUE (bad value):
      Bit  8 - geolocation error (anomalous FOV Earth location)
      Bit  9 - sza > 88 (degree)
      Bit 10 - missing input radiance
      Bit 11 - error input radiance
      Bit 12 - warning input radiance
      Bit 13 - missing input irradiance
      Bit 14 - error input irradiance
      Bit 15 - warning input irradiance
 
 - Field Name:               RadiativeCloudFraction
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Radiative Cloud Fraction = fc*lc331/lm331
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The radiative cloud fraction (= fc * lc331 / lm331) for each L2 candidate
    scene in each L2G grid cell.
 
 - Field Name:               Reflectivity331
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -15.0
   Maximum Value:            115.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    "%"
   Data Source:              L2
   Title:                    Effective Surface Reflectivity at 331 nm
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The effective surface reflectivity at 331 nm (in percent) for each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               Reflectivity360
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -15.0
   Maximum Value:            115.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    "%"
   Data Source:              L2
   Title:                    Effective Surface Reflectivity at 360 nm
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The effective surface reflectivity at 360 nm (in percent) for each L2
    candidate scene in each L2G grid cell.
 
 - Field Name:               Residual
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,nWavel,YDim,XDim
   Minimum Value:            -32.0
   Maximum Value:            32.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    N-Value Residual
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The N-value residuals for each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               SmallPixelColumn
   Data Type:                HE5T_NATIVE_INT16
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0
   Maximum Value:            1000
   Missing Value:            -32767
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    Small Pixel Column
   Unique Field Definition:  OMI-Specific
   Description: >
    The small-pixel-column number for each L2 candidate scene in each L2G grid
    cell.  This is the column number on the CCD for which the pixels are
    additionally transmitted without co-addition.
 
 - Field Name:               SO2index
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -300.0
   Maximum Value:            300.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    SO2 Index
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The SO2 index for each L2 candidate scene in each L2G grid cell.
 
 - Field Name:               StepTwoO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              L2
   Title:                    Step 2 Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The step 2 ozone solution (in DU) for each L2 candidate scene in each L2G
    grid cell.
 
 - Field Name:               TerrainPressure
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            0.0
   Maximum Value:            1013.25
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    hPa
   Data Source:              L2
   Title:                    Terrain Pressure
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The terrain pressure (in hPa) at the center of each L2 candidate scene in
    each L2G grid cell.

 - Field Name:               UVAerosolIndex
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nCandidate,YDim,XDim
   Minimum Value:            -30.0
   Maximum Value:            30.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L2
   Title:                    UV Aerosol Index
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The UV aerosol index for each L2 candidate scene in each L2G grid cell.

 - Field Name:               Wavelength
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel
   Minimum Value:            300.0
   Maximum Value:            400.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    nm
   Data Source:              L2
   Title:                    Wavelength
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The wavelengths (in nm) used in the OMTO3 algorithm.

Core Metadata:

 - Metadata Name:     AssociatedInstrumentShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            OMI
   Data Source:       MCF
   Description:       Actual is "OMI".

 - Metadata Name:     AssociatedPlatformShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            Aura
   Data Source:       MCF
   Description:       Actual is "Aura".

 - Metadata Name:     AssociatedSensorShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            CCD Ultra Violet,CCD Visible
   Data Source:       MCF
   Description:       Actual is "CCD Ultra Violet".

 - Metadata Name:     AutomaticQualityFlag
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            Passed,Suspect,Failed
   Data Source:       PGE
   Description:       Actual is "Failed".
 
 - Metadata Name:     AutomaticQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Actual is "An automatic quality investigation has not yet been devised."
 
 - Metadata Name:     DayNightFlag
   Mandatory:         T
   Data Type:         VA5
   Number of Values:  1
   Valids:            Day,Night,Both
   Data Source:       MCF
   Description:       Actual is "Day".
 
 - Metadata Name:     EastBoundingCoordinate
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     -180.0
   Maximum Value:     180.0
   Data Source:       PGE
   Description: >
    The terrestrial longitude (in degrees) of the easternmost data in the L2G
    granule, which is typically 180.0 degrees.

 - Metadata Name:     EquatorCrossingDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1,16
   Data Source:       L2
   Description: >
    The date of the ascending equator crossing for each L2 input granule.

 - Metadata Name:     EquatorCrossingLongitude
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1,16
   Minimum Value:     -180.0
   Maximum Value:     180.0
   Data Source:       L2
   Description: >
    The terrestrial longitude of the ascending equator crossing for each L2
    input granule.

 - Metadata Name:     EquatorCrossingTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1,16
   Data Source:       L2
   Description: >
    The time of the ascending equator crossing for each L2 input granule.

 - Metadata Name:     InputPointer
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1,16
   Data Source:       PCF
   Description: >
    A list of the L2 input granules.  Example is
    ("OMI-Aura_L2-OMTO3_2005m0121t2301-o02777_v002-2005m0625t035912.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0039-o02778_v002-2005m0625t035923.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0218-o02779_v002-2005m0625t035926.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0357-o02780_v002-2005m0625t035846.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0536-o02781_v002-2005m0625t035925.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0715-o02782_v002-2005m0625t035935.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t0854-o02783_v002-2005m0625t035929.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1033-o02784_v002-2005m0625t035923.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1212-o02785_v002-2005m0625t035921.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1351-o02786_v002-2005m0625t035949.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1529-o02787_v002-2005m0625t035933.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1708-o02788_v002-2005m0625t035927.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t1847-o02789_v002-2005m0625t035956.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t2026-o02790_v002-2005m0625t035923.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t2205-o02791_v002-2005m0625t035939.he5",
     "OMI-Aura_L2-OMTO3_2005m0122t2344-o02792_v002-2005m0625t035954.he5").

 - Metadata Name:     LocalGranuleID
   Mandatory:         T
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Example is "OMI-Aura_L2G-OMTO3G_2005m0122_v002-2005m1111t023553.he5"
    (see Appendix E of Reference 4).

 - Metadata Name:     LocalityValue
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Data Source:       MCF
   Description:       Actual is "Global".

 - Metadata Name:     LOCALVERSIONID
   Mandatory:         T
   Data Type:         VA60
   Number of Values:  1
   Data Source:       PCF
   Description: >
    MD5 fingerprint of the HDF product file.  Example valids are
    "RFC1321 MD5 = not yet calculated" and "RFC1321 MD5 = [0-9,a-f]{32}".
 
 - Metadata Name:     NorthBoundingCoordinate
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     -90.0
   Maximum Value:     90.0
   Data Source:       PGE
   Description: >
    The terrestrial latitude (in degrees) of the northernmost data in the L2G
    granule, which typically lies in the range from 65.0 to 90.0 degrees.

 - Metadata Name:     OperationalQualityFlag
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       PGE
   Description: >
    Actual is "Passed".

 - Metadata Name:     OperationalQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Actual is "This granule passed operational tests that were administered
    by the OMI SIPS.  QA metadata was extracted and the file was successfully
    read using standard HDF-EOS utilities.".

 - Metadata Name:     OrbitNumber
   Mandatory:         T
   Data Type:         I
   Number of Values:  1,16
   Minimum Value:     1
   Maximum Value:     999999
   Data Source:       L2
   Description:       The OMI orbit number for each L2 input granule.

 - Metadata Name:     ParameterName
   Mandatory:         T
   Data Type:         VA40
   Number of Values:  1
   Valids:            Total Column Ozone Gridded
   Data Source:       PGE
   Description: >
    The measured science parameter expressed in the L2G granule.  Actual is
    "Total Column Ozone Gridded".

 - Metadata Name:     PGEVERSION
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Data Source:       PCF
   Description:       Example is "1.0.5" (see Appendix K of Reference 4).

 - Metadata Name:     ProductionDateTime
   Mandatory:         T
   Data Type:         DT
   Number of Values:  1
   Data Source:       TK
   Description:       The date and time of the Level 2G processing.

 - Metadata Name:     QAPercentMissingData
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    An average for the entire L2G granule of the percent of missing Level 1B
    calibrated radiance data.

 - Metadata Name:     RangeBeginningDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1
   Data Source:       PGE
   Description:       The year, month and day when the L2G granule begins.

 - Metadata Name:     RangeBeginningTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1
   Data Source:       PGE
   Description: >
    The hour, minute, second and fraction of a second when the L2G granule
    begins.

 - Metadata Name:     RangeEndingDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1
   Data Source:       PGE
   Description:       The year, month and day when the L2G granule ends.

 - Metadata Name:     RangeEndingTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1
   Data Source:       PGE
   Description: >
    The hour, minute, second and fraction of a second when the L2G granule ends.

 - Metadata Name:     REPROCESSINGACTUAL
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    processed 1 time,processed 2 times,processed 3 times,processed 4 times
   Data Source:       PCF
   Description: >
    An indication of what reprocessing has been performed on the L2G granule.
 
 - Metadata Name:     ReprocessingPlanned
   Mandatory:         T
   Data Type:         VA40
   Number of Values:  1
   Valids: >
    no further update anticipated,further update is anticipated,
    further update anticipated using enhanced PGE
   Data Source:       DP
   Description:       Actual is "further update is anticipated".

 - Metadata Name:     ScienceQualityFlag
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       DP
   Description:       Actual is "Not Investigated".

 - Metadata Name:     ScienceQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       DP
   Description: >
    Actual is "An updated science quality flag and explanation is put in the
    product .met file when a granule has been evaluated.  The flag value in
    this file, Not Investigated, is an automatic default that is put into
    every granule during production.".

 - Metadata Name:     ShortName
   Mandatory:         T
   Data Type:         VA8
   Number of Values:  1
   Valids:            OMTO3G
   Data Source:       MCF
   Description:       Actual is "OMTO3G".

 - Metadata Name:     SizeMBECSDataGranule
   Mandatory:         F
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     0.0
   Maximum Value:     10000.0
   Data Source:       DSS
   Description: >
    The volume of data (in MB) contained in the L2G granule (this Metadata will
    not appear in the L2G granule).
 
 - Metadata Name:     SouthBoundingCoordinate
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     -90.0
   Maximum Value:     90.0
   Data Source:       PGE
   Description: >
    The terrestrial latitude (in degrees) of the southernmost data in the L2G
    granule, which typically lies in the range from -90.0 to -65.0 degrees.

 - Metadata Name:     VERSIONID
   Mandatory:         T
   Data Type:         SI
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     999
   Data Source:       PCF
   Description:       Example is 3.
 
 - Metadata Name:     WestBoundingCoordinate
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     -180.0
   Maximum Value:     180.0
   Data Source:       PGE
   Description: >
    The terrestrial longitude (in degrees) of the westernmost data in the L2G
    granule, which is typically -180.0 degrees.

Product Specific Attributes:

 - Metadata Name:     ExpeditedData
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       PGE
   Description:       The indicator for expedited Level 0 data.

 - Metadata Name:     ExposureTimes
   Mandatory:         T
   Data Type:         F
   Number of Values:  1,256
   Minimum Value:     0.0
   Maximum Value:     2000.0
   Data Source:       PGE
   Description: >
    An array containing the exposure times in seconds used for the measurements.

 - Metadata Name:     MasterClockPeriods
   Mandatory:         T
   Data Type:         F
   Number of Values:  1,128
   Minimum Value:     0.0
   Maximum Value:     10.0
   Data Source:       PGE
   Description: >
    An array containing the master clock periods in seconds used for the
    measurements.

 - Metadata Name:     NrMeasurements
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     30000
   Data Source:       PGE
   Description: >
    The number of measurements used to create the L2G granule.

 - Metadata Name:     NrSpatialZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     0
   Data Source:       PGE
   Description: >
    The number of measurements in spatial zoom mode.  Actual is 0, because
    zoom measurements are excluded from the L2G granule.

 - Metadata Name:     NrSpectralZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     0
   Data Source:       PGE
   Description: >
    The number of measurements in spectral zoom mode.  Actual is 0, because
    zoom measurements are excluded from the L2G granule.

 - Metadata Name:     NrZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     0
   Data Source:       PGE
   Description: >
    The number of measurements in zoom modes.  Actual is 0, because zoom
    measurements are excluded from the L2G granule.

 - Metadata Name:     SolarEclipse
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       PGE
   Description: >
    The indicator that during part of the measurements a solar eclipse occurred.

 - Metadata Name:     SouthAtlanticAnomalyCrossing
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       PGE
   Description: >
    The indicator that during part of the measurements the spacecraft was
    in the South Atlantic Anomaly.

 - Metadata Name:     SpacecraftManeuverFlag
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE,UNKNOWN
   Data Source:       PGE
   Description: >
    The indicator that during part of the measurements the spacecraft was
    performing a maneuver.

Archived Metadata:

 - Metadata Name:     ESDTDescriptorRevision
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Data Source:       MCF
   Description: >
    The version of the ESDT descriptor file as determined by ECS.

 - Metadata Name:     LongName
   Mandatory:         T
   Data Type:         VA80
   Number of Values:  1
   Valids: >
    OMI/Aura Ozone (O3) Total Column Daily L2 Global 0.25deg Lat/Lon Grid
   Data Source:       MCF
   Description: >
    Actual is
    "OMI/Aura Ozone (O3) Total Column Daily L2 Global 0.25deg Lat/Lon Grid".
 
References: >
 
  1. "OMTO3 README File"
     (30 January 2008)
     (http://disc.sci.gsfc.nasa.gov/Aura/data-holdings/OMI/omto3_v003.shtml)

  1. "OMI Algorithm Theoretical Basis Document, Volume II, OMI Ozone Products"
     (OMI-ATBD-VOL2, ATBD-OMI-02, Version 2.0, August 2002)
 
  2. "Definition of OMI Grids for Level 3 and Level 4 Data Products"
     (OMI-Grids_L3L4, SD-OMIE-KNMI-443, 25 January 2005)

  3. "HDF-EOS Aura File Format Guidelines"
     (OMI-AURA-DATA-GUIDE, Version 2.12, 24 October 2006)

  4. "OMI Science Software Delivery Guide for Version 0.9"
     (OMI-SSDG-0.9.10, Version 0.9.10, 22 June 2005)

  5. "OMI GDPS Input/Output Data Specification (IODS) Volume 2"
     (OMI-GDPS-IODS-2, SD-OMIE-7200-DS-467, 8 November 2004)
 
  6. "OMTO3G ECS Metadata Requirements"
     (OMI-OMTO3G_Metadata_RD, Version 0.9.30, 12 August 2005)

  7. "Release 6A Implementation Earth Science Data Model for the ECS Project"
     (420-TP-022-002, June 2001)
     (http://edhs1.gsfc.nasa.gov/waisdata/rel6/html/tp4202202.html and
      http://edhs1.gsfc.nasa.gov/waisdata/rel6/html/tp42022_adds.html)
