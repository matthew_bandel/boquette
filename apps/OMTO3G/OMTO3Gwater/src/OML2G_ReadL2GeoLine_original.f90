!===============================================================================
!
! SUBROUTINE OML2G_ReadL2GeoLine
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to read one line of geolocation from
!   a swath in an OMI L2 product file.
!
! The First Five Arguments Are Inputs:
!   l2_swathid          - L2 swath ID.
!   l2_ifile            - File number for L2 swath.
!   l2_iline            - Line number in L2 swath.
!   l2_nline            - Number of lines in L2 swath.
!   l2_nscene           - Number of scenes in L2 line.
!
! The Final Fifteen Arguments Are Outputs:
!   l2_gpqf             - Ground pixel quality flags for L2 line.
!   l2_lat              - Latitude for L2 line.
!   l2_lon              - Longitude for L2 line.
!   l2_raa              - Relative azimuth angle for L2 line.
!   l2_secday           - Seconds of day for L2 line.
!   l2_saa              - Solar azimuth angle for L2 line.
!   l2_sza              - Solar zenith angle for L2 line.
!   l2_scalt            - Spacecraft altitude for L2 line.
!   l2_sclat            - Spacecraft latitude for L2 line.
!   l2_sclon            - Spacecraft longitude for L2 line.
!   l2_thgt             - Terrain height for L2 line.
!   l2_tline            - Time (TAI93) for L2 line.
!   l2_vaa              - Viewing azimuth angle for L2 line.
!   l2_vza              - Viewing zenith angle for L2 line.
!   l2_xtqf             - Cross-track quality flags for L2 line.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure  - Ends OML2G execution in failure.
! 
! HDF-EOS 5 Function Called:
!   he5_swrdfld         - Reads data from swath field.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: Peter_Leonard@ssaihq.com
!   Phone: (301) 867-2190
!
! Initial Version:
!   July 27, 2005
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   March 27, 2007 - Corrected block initialization problem.
!   June 19, 2010 - Added XTrackQualityFlags field.
!   November 22, 2012 - Changed l2_xtqf_blk to KIND=1.
!
!===============================================================================
 
SUBROUTINE OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'       ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_swathid  ! L2 swath ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_ifile    ! File number for L2 swath.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_iline    ! Line number in L2 swath.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_nline    ! Number of lines in L2 swath.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_nscene   ! Number of scenes in L2 line.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=2), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_gpqf    ! Ground pixel quality flags at scene centers.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_lat     ! Latitude at scene centers.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_lon     ! Longitude at scene centers.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_raa     ! Relative azimuth angle at scene centers.
  REAL (KIND=4), &
    INTENT(OUT) :: l2_secday  ! Seconds of day for L2 line.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_saa     ! Solar azimuth angle at scene centers.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_sza     ! Solar zenith angle at scene centers.
  REAL (KIND=4), &
    INTENT(OUT) :: l2_scalt   ! Spacecraft altitude for L2 line.
  REAL (KIND=4), &
    INTENT(OUT) :: l2_sclat   ! Spacecraft latitude for L2 line.
  REAL (KIND=4), &
    INTENT(OUT) :: l2_sclon   ! Spacecraft longitude for L2 line.
  INTEGER (KIND=2), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_thgt    ! Terrain height at scene centers.
  REAL (KIND=8), &
    INTENT(OUT) :: l2_tline   ! Time (TAI93) for L2 line.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_vaa     ! Viewing azimuth angle at scene centers.
  REAL (KIND=4), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_vza     ! Viewing zenith angle at scene centers.
  INTEGER (KIND=1), &
    DIMENSION(60), &
    INTENT(OUT) :: l2_xtqf    ! Cross-track quality flags at scene centers.
 
!===============================================================================
! Declarations of local variables for he5_swrdfld function.
!===============================================================================
  INTEGER (KIND=8) :: start1          ! 1-D start array for he5_swrdfld.
  INTEGER (KIND=8) :: stride1         ! 1-D stride array for he5_swrdfld.
  INTEGER (KIND=8) :: edge1           ! 1-D edge array for he5_swrdfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: start2            ! 2-D start array for he5_swrdfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: stride2           ! 2-D stride array for he5_swrdfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: edge2             ! 2-D edge array for he5_swrdfld.
 
!===============================================================================
! Declarations of local variables for data blocks.
!===============================================================================
  INTEGER (KIND=4) :: l2_iline_blk    ! Line number in data block.
  INTEGER (KIND=2), &
    DIMENSION(60,100), save :: l2_gpqf_blk  ! Ground pixel quality flags data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_lat_blk   ! Latitude data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_lon_blk   ! Longitude data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_raa_blk   ! Relative azimuth angle data block.
  REAL (KIND=4), &
    DIMENSION(100), save :: l2_secday_blk   ! Seconds of day data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_saa_blk   ! Solar azimuth angle data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_sza_blk   ! Solar zenith angle data block.
  REAL (KIND=4), &
    DIMENSION(100), save :: l2_scalt_blk    ! Spacecraft altitude data block.
  REAL (KIND=4), &
    DIMENSION(100), save :: l2_sclat_blk    ! Spacecraft latitude data block.
  REAL (KIND=4), &
    DIMENSION(100), save :: l2_sclon_blk    ! Spacecraft longitude data block.
  INTEGER (KIND=2), &
    DIMENSION(60,100), save :: l2_thgt_blk  ! Terrain height data block.
  REAL (KIND=8), &
    DIMENSION(100), save :: l2_tline_blk    ! Time (TAI93) data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_vaa_blk   ! Viewing azimuth angle data block.
  REAL (KIND=4), &
    DIMENSION(60,100), save :: l2_vza_blk   ! Viewing zenith angle data block.
  INTEGER (KIND=1), &
    DIMENSION(60,100), save :: l2_xtqf_blk  ! Cross-track quality flags data block.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat           ! Function call status.
  CHARACTER (LEN=256) :: stat_msg         ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_ReadL2GeoLine.f90"  ! Code file reference.
 
!===============================================================================
! Declaration of HDF-EOS 5 function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_swrdfld               ! Reads data from swath field.
 
!===============================================================================
! Initialize geolocation fields.
!===============================================================================
  l2_gpqf(1:l2_nscene) = 65000
  l2_lat(1:l2_nscene) = -1.0e+30
  l2_lon(1:l2_nscene) = -1.0e+30
  l2_raa(1:l2_nscene) = -1.0e+30
  l2_secday = -1.0e+30
  l2_saa(1:l2_nscene) = -1.0e+30
  l2_sza(1:l2_nscene) = -1.0e+30
  l2_scalt = -1.0e+30
  l2_sclat = -1.0e+30
  l2_sclon = -1.0e+30
  l2_thgt(1:l2_nscene) = -32000
  l2_tline = -1.0d+30
  l2_vaa(1:l2_nscene) = -1.0e+30
  l2_vza(1:l2_nscene) = -1.0e+30
  l2_xtqf(1:l2_nscene) = 255
 
!===============================================================================
! Initialize start1, stride1 and edge1 arrays for 1-D geolocation fields.
!===============================================================================
  start1 = 100 * int((l2_iline - 1)/100)
  stride1 = 1
  edge1 = min(100, (l2_nline - start1))
 
!===============================================================================
! Initialize start2, stride2 and edge2 arrays for 2-D geolocation fields.
!===============================================================================
  start2(1) = 0
  start2(2) = start1
  stride2(1) = 1
  stride2(2) = 1
  edge2(1) = l2_nscene
  edge2(2) = edge1
 
!===============================================================================
! Calculate relevant line number in data block.
!===============================================================================
  l2_iline_blk = l2_iline - start1
 
!===============================================================================
! If relevant line number in data block is one (i.e., first line of block),
! then read one block of each geolocation field from L2 swath.
!===============================================================================
  IF (l2_iline_blk == 1) THEN
 
    l2_gpqf_blk(1:l2_nscene,1:100) = 65000
    call_stat = he5_swrdfld(l2_swathid, "GroundPixelQualityFlags", &
                            start2, stride2, edge2, l2_gpqf_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_lat_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "Latitude", &
                            start2, stride2, edge2, l2_lat_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_lon_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "Longitude", &
                            start2, stride2, edge2, l2_lon_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_raa_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "RelativeAzimuthAngle", &
                            start2, stride2, edge2, l2_raa_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_secday_blk(1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SecondsInDay", &
                            start1, stride1, edge1, l2_secday_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_saa_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SolarAzimuthAngle", &
                            start2, stride2, edge2, l2_saa_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_sza_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SolarZenithAngle", &
                            start2, stride2, edge2, l2_sza_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_scalt_blk(1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SpacecraftAltitude", &
                            start1, stride1, edge1, l2_scalt_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_sclat_blk(1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SpacecraftLatitude", &
                            start1, stride1, edge1, l2_sclat_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_sclon_blk(1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "SpacecraftLongitude", &
                            start1, stride1, edge1, l2_sclon_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_thgt_blk(1:l2_nscene,1:100) = -32000
    call_stat = he5_swrdfld(l2_swathid, "TerrainHeight", &
                            start2, stride2, edge2, l2_thgt_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_tline_blk(1:100) = -1.0d+30
    call_stat = he5_swrdfld(l2_swathid, "Time", &
                            start1, stride1, edge1, l2_tline_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_vaa_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "ViewingAzimuthAngle", &
                            start2, stride2, edge2, l2_vaa_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_vza_blk(1:l2_nscene,1:100) = -1.0e+30
    call_stat = he5_swrdfld(l2_swathid, "ViewingZenithAngle", &
                            start2, stride2, edge2, l2_vza_blk)
    IF (call_stat < 0) GO TO 2
 
    l2_xtqf_blk(1:l2_nscene,1:100) = 255
    call_stat = he5_swrdfld(l2_swathid, "XTrackQualityFlags", &
                            start2, stride2, edge2, l2_xtqf_blk)
    IF (call_stat < 0) GO TO 2
 
  END IF
  ! End of "IF (l2_iline_blk == 1)" test.
 
!===============================================================================
! Obtain geolocation for relevant line from data blocks.
!===============================================================================
  l2_gpqf(1:l2_nscene) = l2_gpqf_blk(1:l2_nscene,l2_iline_blk)
  l2_lat(1:l2_nscene) = l2_lat_blk(1:l2_nscene,l2_iline_blk)
  l2_lon(1:l2_nscene) = l2_lon_blk(1:l2_nscene,l2_iline_blk)
  l2_raa(1:l2_nscene) = l2_raa_blk(1:l2_nscene,l2_iline_blk)
  l2_secday = l2_secday_blk(l2_iline_blk)
  l2_saa(1:l2_nscene) = l2_saa_blk(1:l2_nscene,l2_iline_blk)
  l2_sza(1:l2_nscene) = l2_sza_blk(1:l2_nscene,l2_iline_blk)
  l2_scalt = l2_scalt_blk(l2_iline_blk)
  l2_sclat = l2_sclat_blk(l2_iline_blk)
  l2_sclon = l2_sclon_blk(l2_iline_blk)
  l2_thgt(1:l2_nscene) = l2_thgt_blk(1:l2_nscene,l2_iline_blk)
  l2_tline = l2_tline_blk(l2_iline_blk)
  l2_vaa(1:l2_nscene) = l2_vaa_blk(1:l2_nscene,l2_iline_blk)
  l2_vza(1:l2_nscene) = l2_vza_blk(1:l2_nscene,l2_iline_blk)
  l2_xtqf(1:l2_nscene) = l2_xtqf_blk(1:l2_nscene,l2_iline_blk)
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_swrdfld, and so end execution with fatal error.
!===============================================================================
 
2 WRITE(stat_msg, 5) l2_iline, l2_ifile, call_stat
5 FORMAT("Fatal error while reading geolocation for line",i5, &
         " of file",i3,":  Function call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML2G_ReadL2GeoLine
