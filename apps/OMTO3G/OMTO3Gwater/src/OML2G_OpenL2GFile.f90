!===============================================================================
!
! SUBROUTINE OML2G_OpenL2GFile
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to create an OMI L2G product file,
!   and create a grid structure within the file.
!
! The First Seven Arguments Are Inputs:
!   l2_prodid              - L2 product ID.
!   l2g_fname              - L2G file name.
!   l2g_nlon               - Number of longitudes in L2G grid.
!   l2g_nlat               - Number of latitudes in L2G grid.
!   l2g_nlayers            - Number of layers in L2G grid.
!   l2g_nwavel             - Number of wavelengths in L2G grid.
!   l2g_ncand              - Number of candidates in L2G grid.
!
! The Final Two Arguments Are Outputs:
!   l2g_fileid             - L2G file ID.
!   l2g_gridid             - L2G grid ID.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure     - Ends OML2G execution in failure.
!
! HDF-EOS 5 Functions Called:
!   he5_gdopen             - Opens HE5 grid file.
!   he5_gdcreate           - Creates new grid in HE5 file.
!   he5_gddefpreg          - Defines pixel registration for grid.
!   he5_gddefdim           - Defines dimension for grid.
!   he5_gddeforigin        - Defines origin of grid.
!   he5_gddefproj          - Defines projection of grid.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 22, 2005
!
! Revision History:
!   September 29, 2005 - Renamed nLongitude to XDim and nLatitude to YDim.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   March 27, 2007 - Updated list of L2G grid names.
!   October 18, 2007 - Included OMAERO in l2_prodid list.
!   November 22, 2012 - Revised upp_lft_pt and low_rgt_pt.
!
!===============================================================================
 
SUBROUTINE OML2G_OpenL2GFile(l2_prodid, l2g_fname, l2g_nlon, l2g_nlat, &
                             l2g_nlayers, l2g_nwavel, l2g_ncand, &
                             l2g_fileid, l2g_gridid)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================

  ! adding C_LONG binding to imitate OMNO2G coding details - MB 08/01/24
  !  IMPLICIT NONE
  USE ISO_C_BINDING, ONLY: C_LONG
  IMPLICIT NONE

!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'                 ! Declarations for HDF-EOS 5.

!===============================================================================
! Declarations of input arguments.
!===============================================================================
  CHARACTER (LEN=8), &
    INTENT(IN) :: l2_prodid             ! L2 product ID.
  CHARACTER (LEN=256), &
    INTENT(IN) :: l2g_fname             ! L2G file name.

  ! switching to C_LONG type to imitate OMNO2G grid pattern - MB 08/01/24
!  INTEGER (KIND=8), &
!    INTENT(IN) :: l2g_nlon              ! Number of longitudes in L2G grid.
!  INTEGER (KIND=8), &
!    INTENT(IN) :: l2g_nlat              ! Number of latitudes in L2G grid.
!  INTEGER (KIND=8), &
!    INTENT(IN) :: l2g_nlayers           ! Number of layers in L2G grid.
!  INTEGER (KIND=8), &
!    INTENT(IN) :: l2g_nwavel            ! Number of wavelengths in L2G grid.
!  INTEGER (KIND=8), &
!    INTENT(IN) :: l2g_ncand             ! Number of candidates in grid.
  INTEGER (KIND=C_LONG), &
    INTENT(IN) :: l2g_nlon              ! Number of longitudes in L2G grid.
  INTEGER (KIND=C_LONG), &
    INTENT(IN) :: l2g_nlat              ! Number of latitudes in L2G grid.
  INTEGER (KIND=C_LONG), &
    INTENT(IN) :: l2g_nlayers           ! Number of layers in L2G grid.
  INTEGER (KIND=C_LONG), &
    INTENT(IN) :: l2g_nwavel            ! Number of wavelengths in L2G grid.
  INTEGER (KIND=C_LONG), &
    INTENT(IN) :: l2g_ncand             ! Number of candidates in grid.


!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2g_fileid           ! L2G file ID.

  ! changing to C_LONG type to imitate OMNO2G grid pattern - MB 08/01/24
!  INTEGER (KIND=4), &
  INTEGER (KIND=C_LONG), &
    INTENT(OUT) :: l2g_gridid           ! L2G grid ID.
 
!===============================================================================
! Declarations of local arguments.
!===============================================================================
  CHARACTER (LEN=256) :: l2g_gname      ! L2G grid name.
  REAL (KIND=8), &
    DIMENSION(2) :: upp_lft_pt          ! Upper left point for he5_gdcreate.
  REAL (KIND=8), &
    DIMENSION(2) :: low_rgt_pt          ! Lower right point for he5_gdcreate.
  INTEGER (KIND=4) :: dummy1            ! Dummy argument 1 for he5_gddefproj.
  INTEGER (KIND=4) :: dummy2            ! Dummy argument 2 for he5_gddefproj.
  REAL (KIND=8) :: dummy3               ! Dummy argument 3 for he5_gddefproj.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat         ! Function call status.
  CHARACTER (LEN=256) :: stat_msg       ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_OpenL2GFile.f90"  ! Code file reference.
 
!===============================================================================
! Declarations of HDF-EOS 5 function calls.
!===============================================================================
  INTEGER (KIND=4), EXTERNAL :: he5_gdopen       ! Opens HE5 grid file.
  INTEGER (KIND=4), EXTERNAL :: he5_gdcreate     ! Creates new grid in HE5 file.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefpreg    ! Defines pixel reg. for grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefdim     ! Defines dimension for grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddeforigin  ! Defines origin of grid.
  INTEGER (KIND=4), EXTERNAL :: he5_gddefproj    ! Defines projection of grid.
 
!===============================================================================
! Open L2G grid file.
!===============================================================================
  l2g_fileid = he5_gdopen(l2g_fname, HE5F_ACC_TRUNC)
 
!===============================================================================
! Test if L2G file ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l2g_fileid < 0) THEN
 
    WRITE(stat_msg, 5) l2g_fileid
5   FORMAT("Fatal error:  L2G file ID =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_fileid < 0)" test.
 
!===============================================================================
! Determine name of L2G grid.
!===============================================================================
  IF (l2_prodid == "OMAERO")  l2g_gname = "ColumnAmountAerosol"
  IF (l2_prodid == "OMAERUV") l2g_gname = "Aerosol NearUV Swath"
  IF (l2_prodid == "OMCLDO2") l2g_gname = "CloudFractionAndPressure"
  IF (l2_prodid == "OMCLDRR") l2g_gname = "Cloud Product"
  IF (l2_prodid == "OMDOAO3") l2g_gname = "ColumnAmountO3"
  IF (l2_prodid == "OMNO2")   l2g_gname = "ColumnAmountNO2"
  IF (l2_prodid == "OMSO2")   l2g_gname = "OMI Total Column Amount SO2"
  IF (l2_prodid == "OMTO3")   l2g_gname = "OMI Column Amount O3"
 
!===============================================================================
! Set extreme upper left and lower right points of grid (format is such that
! -180000000.0d0 = -180 degrees 000 minutes 000.0d0 seconds, etc...).
!===============================================================================
  upp_lft_pt(1) = -180000000.0d0
  upp_lft_pt(2) =  -90000000.0d0
  low_rgt_pt(1) =  180000000.0d0
  low_rgt_pt(2) =   90000000.0d0
 
!===============================================================================
! Create L2G grid in L2G file.
!===============================================================================
  l2g_gridid = he5_gdcreate(l2g_fileid, l2g_gname, &
                            l2g_nlon, l2g_nlat, &
                            upp_lft_pt, low_rgt_pt)
 
!===============================================================================
! Test if L2G grid ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l2g_gridid < 0) THEN
 
    WRITE(stat_msg, 10) l2g_gridid
10  FORMAT("Fatal error:  L2G grid ID =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_gridid < 0)" test.
 
!===============================================================================
! Define pixel registration in L2G grid to be pixel centers.
!===============================================================================
  call_stat = he5_gddefpreg(l2g_gridid, HE5_HDFE_CENTER)

!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 15) call_stat
15  FORMAT("Fatal error:  he5_gddefpreg call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define longitude dimension in L2G grid.
!===============================================================================
  call_stat = he5_gddefdim(l2g_gridid, "XDim", l2g_nlon)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 20) call_stat
20  FORMAT("Fatal error:  longitude he5_gddefdim call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define latitude dimension in L2G grid.
!===============================================================================
  call_stat = he5_gddefdim(l2g_gridid, "YDim", l2g_nlat)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 25) call_stat
25  FORMAT("Fatal error:  latitude he5_gddefdim call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define layers dimension in L2G grid.
!===============================================================================
  call_stat = he5_gddefdim(l2g_gridid, "nLayers", l2g_nlayers)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 30) call_stat
30  FORMAT("Fatal error:  layers he5_gddefdim call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define wavelength dimension in L2G grid.
!===============================================================================
  call_stat = he5_gddefdim(l2g_gridid, "nWavel", l2g_nwavel)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 35) call_stat
35  FORMAT("Fatal error:  wavelength he5_gddefdim call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define candidate dimension in L2G grid.
!===============================================================================
  call_stat = he5_gddefdim(l2g_gridid, "nCandidate", l2g_ncand)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 40) call_stat
40  FORMAT("Fatal error:  candidate he5_gddefdim call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define origin of L2G grid.
!===============================================================================

  ! removing grid origin in imitation of OMNO2G grid pattern - MB 08/01/24
!  call_stat = he5_gddeforigin(l2g_gridid, HE5_HDFE_GD_LL)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 45) call_stat
45  FORMAT("Fatal error:  he5_gddeforigin call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Define projection of L2G grid.
!===============================================================================

  call_stat = he5_gddefproj(l2g_gridid, HE5_GCTP_GEO, dummy1, dummy2, dummy3)

!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 50) call_stat
50  FORMAT("Fatal error:  he5_gddefproj call status =",i10,"!")
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML2G_OpenL2GFile
