!===============================================================================
!
! SUBROUTINE OML2G_WriteL2GGeoGrid
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to write the geolocation fields to a
!   grid in an L2G product file.
!
! All Twenty Five Arguments Are Inputs:
!   l2g_gridid          - L2G grid ID.
!   l2g_nlon            - Number of longitudes in grid.
!   l2g_nlat            - Number of latitudes in grid.
!   l2g_ncand           - Number of candidates in grid.
!   gfi1u_mvalue        - Missing values for uns. 1-B int. fields.
!   gfi2_mvalue         - Missing values for 2-B int. fields.
!   gfi2u_mvalue        - Missing values for uns. 2-B int. fields.
!   gfi4_mvalue         - Missing values for 4-B int. fields.
!   gfr4_mvalue         - Missing values for 4-B real fields.
!   gfr8_mvalue         - Missing values for 8-B real fields.
!   l2g_gpqf            - Ground pixel quality flags of good scenes.
!   l2g_lat             - Latitudes of good scenes.
!   l2g_line            - Line numbers of good scenes.
!   l2g_lon             - Longitudes of good scenes.
!   l2g_ncs             - Number of candidate scenes.
!   l2g_orbit           - Orbit numbers of good scenes.
!   l2g_pathl           - Path lengths of good scenes.
!   l2g_raa             - Relative azimuth angles of good scenes.
!   l2g_scene           - Scene numbers of good scenes.
!   l2g_secday          - Seconds of day of good scenes.
!   l2g_sza             - Solar zenith angles of good scenes.
!   l2g_thgt            - Terrain heights of good scenes.
!   l2g_time            - Observation times of good scenes.
!   l2g_vza             - View zenith angles of good scenes.
!   l2g_xtqf            - Cross-track quality flags of good scenes.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure  - Ends OML2G execution in failure.
!
! HDF-EOS 5 Functions Called:
!   he5_gddefcomtle     - Sets compression parameters.
!   he5_gdsetfill       - Set fill value for grid field.
!   he5_gddeffld        - Defines field in grid.
!   he5_gdwrfld         - Writes field to grid.
!
! Author(s):
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: Peter_Leonard@ssaihq.com
!   Phone: (301) 867-2190
!
! Initial Version:
!   July 27, 2005
!
! Revision History:
!   August 26, 2005 - Reversed dimension order of 2-D and 3-D fields.
!   September 2, 2005 - Added missing values to arguments to set fill values.
!   September 29, 2005 - Renamed nLongitude to XDim and nLatitude to YDim, and
!                        reversed order of dimensions.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History (Continued):
!   June 19, 2010 - Added XTrackQualityFlags field.
!
!===============================================================================
 
SUBROUTINE OML2G_WriteL2GGeoGrid(l2g_gridid, &
                                 l2g_nlon, l2g_nlat, l2g_ncand, &
                                 gfi1u_mvalue, gfi2_mvalue, gfi2u_mvalue, &
                                 gfi4_mvalue, gfr4_mvalue, gfr8_mvalue, &
                                 l2g_gpqf, l2g_lat, l2g_line, l2g_lon, &
                                 l2g_ncs, l2g_orbit, l2g_pathl, l2g_raa, &
                                 l2g_scene, l2g_secday, l2g_sza, l2g_thgt, &
                                 l2g_time, l2g_vza, l2g_xtqf)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'             ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_gridid        ! L2G grid ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlon          ! Number of longitudes in grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlat          ! Number of latitudes in grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_ncand         ! Number of candidates in grid.
  INTEGER (KIND=1), &
    INTENT(IN) :: gfi1u_mvalue      ! Missing values for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    INTENT(IN) :: gfi2_mvalue       ! Missing values for 2-B int. fields.
  INTEGER (KIND=2), &
    INTENT(IN) :: gfi2u_mvalue      ! Missing values for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfi4_mvalue       ! Missing values for 4-B int. fields.
  REAL (KIND=4), &
    INTENT(IN) :: gfr4_mvalue       ! Missing values for 4-B real fields.
  REAL (KIND=8), &
    INTENT(IN) :: gfr8_mvalue       ! Missing values for 8-B real fields.
  INTEGER (KIND=2), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand),&
    INTENT(IN) :: l2g_gpqf          ! Ground pixel quality flags of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_lat           ! Latitudes of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_line          ! Line numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_lon           ! Longitudes of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat), &
    INTENT(IN) :: l2g_ncs           ! Number of candidate scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_orbit         ! Orbit numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_pathl         ! Path lengths of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_raa           ! Relative azimuth angles of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_scene         ! Scene numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_secday        ! Seconds of day of good scenes.
!  REAL (KIND=4), &
!    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
!    INTENT(IN) :: l2g_saa          ! Solar azimuth angles of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_sza           ! Solar zenith angles of good scenes.
!  REAL (KIND=4), &
!    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
!    INTENT(IN) :: l2g_scalt        ! Spacecraft altitudes for good scenes.
!  REAL (KIND=4), &
!    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
!    INTENT(IN) :: l2g_sclat        ! Spacecraft latitudes for good scenes.
!  REAL (KIND=4), &
!    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
!    INTENT(IN) :: l2g_sclon        ! Spacecraft longitudes for good scenes.
  INTEGER (KIND=2), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_thgt          ! Terrain heights of good scenes.
  REAL (KIND=8), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_time          ! Observation times of good scenes.
!  REAL (KIND=4), &
!    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
!    INTENT(IN) :: l2g_vaa          ! Viewing azimuth angles of good scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_vza           ! View zenith angles of good scenes.
  INTEGER (KIND=1), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(IN) :: l2g_xtqf          ! Cross-track quality of good scenes.
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=4) :: degree_of_comp2  ! Degree of 2-D compression.
  INTEGER (KIND=4) :: degree_of_comp3  ! Degree of 3-D compression.
  INTEGER (KIND=4) :: tile_rank2       ! Rank of 2-D compression tile.
  INTEGER (KIND=4) :: tile_rank3       ! Rank of 3-D compression tile.
  INTEGER (KIND=8), &
    DIMENSION(2) :: tile_dims2       ! Dimensions of 2-D compression tile.
  INTEGER (KIND=8), &
    DIMENSION(3) :: tile_dims3       ! Dimensions of 3-D compression tile.
  INTEGER (KIND=8), &
    DIMENSION(2) :: start2           ! 2-D start array for he5_gdwrfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: start3           ! 3-D start array for he5_gdwrfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: stride2          ! 2-D stride array for he5_gdwrfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: stride3          ! 3-D stride array for he5_gdwrfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: edge2            ! 2-D edge array for he5_gdwrfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: edge3            ! 3-D edge array for he5_gdwrfld.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat             ! Function call status.
  CHARACTER (LEN=256) :: stat_msg           ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_WriteL2GGeoGrid.f90"  ! Code file reference.
 
!===============================================================================
! Declarations of HDF-EOS 5 function calls.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gddefcomtle             ! Sets compression parameters.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdsetfill               ! Set fill value for grid field.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gddeffld                ! Defines field in grid.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdwrfld                 ! Writes grid to field.
 
!===============================================================================
! Set compression parameters for 2-D geolocation fields.
!===============================================================================
  degree_of_comp2 = 5
  tile_rank2 = 2
  tile_dims2(1) = l2g_nlon/8
  tile_dims2(2) = l2g_nlat/4
 
!===============================================================================
! Set compression parameters for 3-D geolocation fields.
!===============================================================================
  degree_of_comp3 = 5
  tile_rank3 = 3
  tile_dims3(1) = l2g_nlon/8
  tile_dims3(2) = l2g_nlat/4
  tile_dims3(3) = 1

!===============================================================================
! Set start2, stride2 and edge2 arrays for 2-D geolocation fields.
!===============================================================================
  start2(1) = 0
  start2(2) = 0
  stride2(1) = 1
  stride2(2) = 1
  edge2(1) = l2g_nlon
  edge2(2) = l2g_nlat

!===============================================================================
! Set start3, stride3 and edge3 arrays for 3-D geolocation fields.
!===============================================================================
  start3(1) = 0
  start3(2) = 0
  start3(3) = 0
  stride3(1) = 1
  stride3(2) = 1
  stride3(3) = 1
  edge3(1) = l2g_nlon
  edge3(2) = l2g_nlat
  edge3(3) = l2g_ncand
 
!===============================================================================
! Define geolocation fields, prepare fields for compression, and write fields to
! grid:
!===============================================================================
 
!===============================================================================
! Ground pixel quality flags of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "GroundPixelQualityFlags", &
                            HE5T_NATIVE_UINT16, gfi2u_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "GroundPixelQualityFlags", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_UINT16, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "GroundPixelQualityFlags", &
                          start3, stride3, edge3, l2g_gpqf)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Latitudes of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "Latitude", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "Latitude", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "Latitude", &
                          start3, stride3, edge3, l2g_lat)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Line numbers of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "LineNumber", &
                            HE5T_NATIVE_INT, gfi4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "LineNumber", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_INT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "LineNumber", &
                          start3, stride3, edge3, l2g_line)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Longitudes of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "Longitude", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "Longitude", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "Longitude", &
                          start3, stride3, edge3, l2g_lon)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Number of candidate scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp2, tile_rank2, tile_dims2)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "NumberOfCandidateScenes", &
                            HE5T_NATIVE_INT, 0)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "NumberOfCandidateScenes", &
                           "XDim,YDim", " ", &
                           HE5T_NATIVE_INT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "NumberOfCandidateScenes", &
                          start2, stride2, edge2, l2g_ncs)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Orbit numbers of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "OrbitNumber", &
                            HE5T_NATIVE_INT, gfi4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "OrbitNumber", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_INT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "OrbitNumber", &
                          start3, stride3, edge3, l2g_orbit)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Path lengths of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "PathLength", &
                            HE5T_NATIVE_FLOAT, -1.0*gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "PathLength", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "PathLength", &
                          start3, stride3, edge3, l2g_pathl)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Relative azimuth angles of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "RelativeAzimuthAngle", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "RelativeAzimuthAngle", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "RelativeAzimuthAngle", &
                          start3, stride3, edge3, l2g_raa)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Scene numbers of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "SceneNumber", &
                            HE5T_NATIVE_INT, gfi4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "SceneNumber", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_INT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "SceneNumber", &
                          start3, stride3, edge3, l2g_scene)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Seconds of day of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "SecondsInDay", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "SecondsInDay", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "SecondsInDay", &
                          start3, stride3, edge3, l2g_secday)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Solar azimuth angles of good scenes.
!===============================================================================
!  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
!                              degree_of_comp3, tile_rank3, tile_dims3)
!  IF (call_stat < 0) GO TO 1
!  call_stat = he5_gdsetfill(l2g_gridid, "SolarAzimuthAngle", &
!                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
!  IF (call_stat < 0) GO TO 3
!  call_stat = he5_gddeffld(l2g_gridid, "SolarAzimuthAngle", &
!                           "XDim,YDim,nCandidate", " ", &
!                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
!  IF (call_stat < 0) GO TO 5
!  call_stat = he5_gdwrfld(l2g_gridid, "SolarAzimuthAngle", &
!                          start3, stride3, edge3, l2g_saa)
!  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Solar zenith angles of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "SolarZenithAngle", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "SolarZenithAngle", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "SolarZenithAngle", &
                          start3, stride3, edge3, l2g_sza)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Spacecraft altitudes for good scenes.
!===============================================================================
!  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
!                              degree_of_comp3, tile_rank3, tile_dims3)
!  IF (call_stat < 0) GO TO 1
!  call_stat = he5_gdsetfill(l2g_gridid, "SpacecraftAltitude", &
!                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
!  IF (call_stat < 0) GO TO 3
!  call_stat = he5_gddeffld(l2g_gridid, "SpacecraftAltitude", &
!                           "XDim,YDim,nCandidate", " ", &
!                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
!  IF (call_stat < 0) GO TO 5
!  call_stat = he5_gdwrfld(l2g_gridid, "SpacecraftAltitude", &
!                          start3, stride3, edge3, l2g_scalt)
!  IF (call_stat < 0) GO TO 7
! 
!===============================================================================
! Spacecraft latitudes for scenes.
!===============================================================================
!  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
!                              degree_of_comp3, tile_rank3, tile_dims3)
!  IF (call_stat < 0) GO TO 1
!  call_stat = he5_gdsetfill(l2g_gridid, "SpacecraftLatitude", &
!                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
!  IF (call_stat < 0) GO TO 3
!  call_stat = he5_gddeffld(l2g_gridid, "SpacecraftLatitude", &
!                           "XDim,YDim,nCandidate", " ", &
!                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
!  IF (call_stat < 0) GO TO 5
!  call_stat = he5_gdwrfld(l2g_gridid, "SpacecraftLatitude", &
!                          start3, stride3, edge3, l2g_sclat)
!  IF (call_stat < 0) GO TO 7
! 
!===============================================================================
! Spacecraft longitudes for good scenes.
!===============================================================================
!  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
!                              degree_of_comp3, tile_rank3, tile_dims3)
!  IF (call_stat < 0) GO TO 1
!  call_stat = he5_gdsetfill(l2g_gridid, "SpacecraftLongitude", &
!                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
!  IF (call_stat < 0) GO TO 3
!  call_stat = he5_gddeffld(l2g_gridid, "SpacecraftLongitude", &
!                           "XDim,YDim,nCandidate", " ", &
!                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
!  IF (call_stat < 0) GO TO 5
!  call_stat = he5_gdwrfld(l2g_gridid, "SpacecraftLongitude", &
!                          start3, stride3, edge3, l2g_sclon)
!  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Terrain heights of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "TerrainHeight", &
                            HE5T_NATIVE_INT16, gfi2_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "TerrainHeight", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_INT16, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "TerrainHeight", &
                          start3, stride3, edge3, l2g_thgt)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Observation times of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "Time", &
                            HE5T_NATIVE_DOUBLE, gfr8_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "Time", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_DOUBLE, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "Time", &
                          start3, stride3, edge3, l2g_time)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Viewing azimuth angles of good scenes.
!===============================================================================
!  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
!                              degree_of_comp3, tile_rank3, tile_dims3)
!  IF (call_stat < 0) GO TO 1
!  call_stat = he5_gdsetfill(l2g_gridid, "ViewingAzimuthAngle", &
!                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
!  IF (call_stat < 0) GO TO 3
!  call_stat = he5_gddeffld(l2g_gridid, "ViewingAzimuthAngle", &
!                           "XDim,YDim,nCandidate", " ", &
!                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
!  IF (call_stat < 0) GO TO 5
!  call_stat = he5_gdwrfld(l2g_gridid, "ViewingAzimuthAngle", &
!                          start3, stride3, edge3, l2g_vaa)
!  IF (call_stat < 0) GO TO 7
! 
!===============================================================================
! View zenith angles of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "ViewingZenithAngle", &
                            HE5T_NATIVE_FLOAT, gfr4_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "ViewingZenithAngle", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_FLOAT, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "ViewingZenithAngle", &
                          start3, stride3, edge3, l2g_vza)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Cross-track quality flags of good scenes.
!===============================================================================
  call_stat = he5_gddefcomtle(l2g_gridid, HE5_HDFE_COMP_DEFLATE, &
                              degree_of_comp3, tile_rank3, tile_dims3)
  IF (call_stat < 0) GO TO 1
  call_stat = he5_gdsetfill(l2g_gridid, "XTrackQualityFlags", &
                            HE5T_NATIVE_UINT8, gfi1u_mvalue)
  IF (call_stat < 0) GO TO 3
  call_stat = he5_gddeffld(l2g_gridid, "XTrackQualityFlags", &
                           "XDim,YDim,nCandidate", " ", &
                           HE5T_NATIVE_UINT8, HE5_HDFE_NOMERGE)
  IF (call_stat < 0) GO TO 5
  call_stat = he5_gdwrfld(l2g_gridid, "XTrackQualityFlags", &
                          start3, stride3, edge3, l2g_xtqf)
  IF (call_stat < 0) GO TO 7
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gddefcomtle, and so end execution with fatal error.
!===============================================================================
1 WRITE(stat_msg, 2) call_stat
2 FORMAT("Fatal error:  he5_gddefcomtle call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gdsetfill, and so end execution with fatal error.
!===============================================================================
3 WRITE(stat_msg, 4) call_stat
4 FORMAT("Fatal error:  he5_gdsetfill call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gddeffld, and so end execution with fatal error.
!===============================================================================
5 WRITE(stat_msg, 6) call_stat
6 FORMAT("Fatal error:  he5_gddeffld call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gdwrfld, and so end execution with fatal error.
!===============================================================================
7 WRITE(stat_msg, 8) call_stat
8 FORMAT("Fatal error:  he5_gdwrfld call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML2G_WriteL2GGeoGrid
