!===============================================================================
!
! SUBROUTINE OML2G_ReadL2FieldInfo
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to read the names of the swath
!   fields from an OMI L2 product file.
!
! The First Argument Is An Input:
!   l2_swathid          - L2 swath ID.
!
! The Final Twelve Arguments Are Outputs:
!   gfi1u_nfield        - Number of unsigned 1-byte integer fields.
!   gfi1u_names         - Names of unsigned 1-byte integer fields.
!   gfi2_nfield         - Number of 2-byte integer fields.
!   gfi2_names          - Names of 2-byte integer fields.
!   gfi2u_nfield        - Number of unsigned 2-byte integer fields.
!   gfi2u_names         - Names of unsigned 2-byte integer fields.
!   gfi4_nfield         - Number of 4-byte integer fields.
!   gfi4_names          - Names of 4-byte integer fields.
!   gfr4_nfield         - Number of 4-byte real fields.
!   gfr4_names          - Names of 4-byte real fields.
!   gfr8_nfield         - Number of 8-byte real fields.
!   gfr8_names          - Names of 8-byte real fields.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 26, 2005
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   November 13, 2007 - Renamed CloudTopPressure field to CloudPressure.
!                     - Added RadiativeCloudFraction field.
!   June 19, 2010 - Added XTrackQualityFlags field.
!
!===============================================================================
 
SUBROUTINE OML2G_ReadL2FieldInfo(l2_swathid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'                     ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input argument.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_swathid     ! L2 swath ID.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi1u_nfield  ! Number of unsigned 1-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi2_nfield   ! Number of 2-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi2u_nfield  ! Number of unsigned 2-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi4_nfield   ! Number of 4-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfr4_nfield   ! Number of 4-byte real fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfr8_nfield   ! Number of 8-byte real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_names   ! Names of unsigned 1-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_names    ! Names of 2-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_names   ! Names of unsigned 2-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_names    ! Names of 4-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_names    ! Names of 4-byte real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_names    ! Names of 8-byte real fields.
 
!===============================================================================
! Initialize names of fields.
!===============================================================================
  gfi1u_names(1:100) = " "
  gfi2_names(1:100) = " "
  gfi2u_names(1:100) = " "
  gfi4_names(1:100) = " "
  gfr4_names(1:100) = " "
  gfr8_names(1:100) = " "
 
!===============================================================================
! Read names of swath fields from L2 file (names are not currently read from L2
! file, but instead are hard-wired below):
!===============================================================================
  gfi1u_nfield = 5
  gfi1u_names(1) = "XTrackQualityFlags"
  gfi1u_names(2) = "AlgorithmFlags"
  gfi1u_names(3) = "InstrumentConfigurationId"
  gfi1u_names(4) = "MeasurementQualityFlags"
  gfi1u_names(5) = "NumberSmallPixelColumns"
 
  gfi2_nfield = 2
  gfi2_names(1)  = "SmallPixelColumn"
  gfi2_names(2)  = "TerrainHeight"
 
  gfi2u_nfield = 2
  gfi2u_names(1) = "GroundPixelQualityFlags"
  gfi2u_names(2) = "QualityFlags"
 
  gfi4_nfield = 0
 
  gfr4_nfield = 20
  gfr4_names(1)  = "Latitude"
  gfr4_names(2)  = "Longitude"
  gfr4_names(3)  = "RelativeAzimuthAngle"
  gfr4_names(4)  = "SecondsInDay"
  gfr4_names(5)  = "SolarZenithAngle"
  gfr4_names(6)  = "ViewingZenithAngle"
  gfr4_names(7)  = "APrioriLayerO3"
  gfr4_names(8)  = "CloudPressure"
  gfr4_names(9)  = "ColumnAmountO3"
  gfr4_names(10) = "LayerEfficiency"
  gfr4_names(11) = "O3BelowCloud"
  gfr4_names(12) = "RadiativeCloudFraction"
  gfr4_names(13) = "Reflectivity331"
  gfr4_names(14) = "Reflectivity360"
  gfr4_names(15) = "Residual"
  gfr4_names(16) = "SO2index"
  gfr4_names(17) = "StepTwoO3"
  gfr4_names(18) = "TerrainPressure"
  gfr4_names(19) = "UVAerosolIndex"
  gfr4_names(20) = "Wavelength"
 
  gfr8_nfield = 1
  gfr8_names(1)  = "Time"
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML2G_ReadL2FieldInfo
