Shortname:    OMTO3d
Longname:     OMI/Aura Ozone (O3) Total Column Daily L3 Global 1x1deg Lat/Lon Grid
PFS Version:  1.0.12
Date:         12 September 2024
Author(s):    Peter Leonard (ADNET)

PGE Version:               1.0.12
Lead Algorithm Scientist:  Richard D. McPeters (NASA/GSFC)
Lead Algorithm Developer:  Peter Leonard (ADNET)
Lead PGE Developer:        Peter Leonard (ADNET)
PGE Developer(s):          Peter Leonard (ADNET)

Description: >

    The grid and data format of the OMTO3d product files are consistent with
    NASA document number NASA/TM-2000-209896 entitled "Total Ozone Mapping
    Spectrometer (TOMS) Level-3 Data Products User's Guide" by R. McPeters,
    P.K. Bhartia, A. Krueger, J. Herman, C. Wellemeyer, C. Seftor, W. Byerly
    and E.A. Celarier.

    Starting with Version 0.9.31 of the OMTO3d product, the grid cell size
    has been changed to 1.0-deg by 1.0-deg, as requested by R. McPeters.

    Starting with Version 1.0.7 of the OMTO3d product, each Level 3 grid
    cell with a value of the UV aerosol index that is less than the cutoff
    value of 0.5 is flagged with a Fill Value of 888 in the ASCII output,
    as requested by R. McPeters.


 - Field Name:               ColumnAmountO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    Best Total Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The best total ozone solution (in DU) associated with the grid box.


 - Field Name:               RadiativeCloudFraction
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Radiative Cloud Fraction
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The radiative cloud fraction at the center of the grid box.


 - Field Name:               SolarZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Solar Zenith Angle
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The solar zenith angle (in deg) at the center of the grid box.


 - Field Name:               UVAerosolIndex
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -30.0
   Maximum Value:            30.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    UV Aerosol Index
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The UV aerosol index associated with the grid box.


 - Field Name:               ViewingZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            70.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Viewing Zenith Angle
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The viewing zenith angle (in deg) at the center of the grid box.
