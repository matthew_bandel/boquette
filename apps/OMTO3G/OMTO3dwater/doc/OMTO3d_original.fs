The grid and data format of the OMTO3d product files are consistent with
NASA document number NASA/TM-2000-209896 entitled "Total Ozone Mapping
Spectrometer (TOMS) Level-3 Data Products User's Guide" by R. McPeters,
P.K. Bhartia, A. Krueger, J. Herman, C. Wellemeyer, C. Seftor, W. Byerly
and E.A. Celarier.
  
Starting with Version 0.9.31 of the OMTO3d product, the grid cell size
has been changed to 1.0-deg by 1.0-deg, as requested by R. McPeters.
  
Starting with Version 1.0.7 of the OMTO3d product, each Level 3 grid
cell with a value of the UV aerosol index that is less than the cutoff
value of 0.5 is flagged with a Fill Value of 888 in the ASCII output,
as requested by R. McPeters.
