!===============================================================================
!
! SUBROUTINE OML3_OpenL2GFile
!
! High Level Overview:
!   This subroutine is called by OML3_Main to open an OMI L2G product file, and
!   attach to the grid structure within the file.
!
! The First Two Arguments Are Inputs:
!   l2g_prodid         - L2G product ID.
!   l2g_fname          - L2G file name.
!
! The Final Two Arguments Are Outputs:
!   l2g_fileid         - L2G file ID.
!   l2g_gridid         - L2G grid ID.
!
! OML3 Subroutine Called:
!   OML3_EndInFailure  - Ends OML3 execution in failure.
!
! HDF-EOS 5 Functions Called:
!   he5_gdopen         - Opens HE5 grid file.
!   he5_gdattach       - Attaches to grid in HE5 file.
!   he5_gdpreginfo     - Gets pixel registration code for grid.
!   he5_gdorginfo      - Gets origin code for grid.
!   he5_gdprojinfo     - Gets projection code for grid.
!
! Author(s):
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: Peter_Leonard@ssaihq.com
!   Phone: (301) 867-2190
!
! Initial Version:
!   September 27, 2005
!
! Revision History:
!
!===============================================================================
 
SUBROUTINE OML3_OpenL2GFile(l2g_prodid, l2g_fname, &
                            l2g_fileid, l2g_gridid)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'                ! Declarations for HDF-EOS 5.

!===============================================================================
! Declarations of input arguments.
!===============================================================================
  CHARACTER (LEN=8), &
    INTENT(IN) :: l2g_prodid           ! L2G product ID.
  CHARACTER (LEN=256), &
    INTENT(IN) :: l2g_fname            ! L2G file name.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2g_fileid          ! L2G file ID.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2g_gridid          ! L2G grid ID.
 
!===============================================================================
! Declarations of local arguments.
!===============================================================================
  CHARACTER (LEN=256) :: l2g_gname     ! L2G grid name.
  INTEGER (KIND=4) :: dummy            ! Dummy argument for he5_gd* functions.
  INTEGER (KIND=4) :: dummy1           ! Dummy argument 1 for he5_gdprojinfo.
  INTEGER (KIND=4) :: dummy2           ! Dummy argument 2 for he5_gdprojinfo.
  REAL (KIND=8) :: dummy3              ! Dummy argument 3 for he5_gdprojinfo.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat        ! Function call status.
  CHARACTER (LEN=256) :: stat_msg      ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML3_OpenL2GFile.f90"  ! Code file reference.
 
!===============================================================================
! Declarations of HDF-EOS 5 function calls.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdopen             ! Opens HE5 grid file.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdattach           ! Attaches to grid in HE5 file.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdpreginfo         ! Gets pixel registration code for grid.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdorginfo          ! Gets origin code for grid.
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdprojinfo         ! Gets projection code for grid.
 
!===============================================================================
! Open L2G grid file.
!===============================================================================
  l2g_fileid = he5_gdopen(l2g_fname, HE5F_ACC_RDONLY)
 
!===============================================================================
! Test if L2G file ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l2g_fileid < 0) THEN
 
    WRITE(stat_msg, 5) l2g_fileid
5   FORMAT("Fatal error:  L2G file ID =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_fileid < 0)" test.
 
!===============================================================================
! Determine name of L2G grid.
!===============================================================================
  IF (l2g_prodid == "OMTO3G") l2g_gname = "OMI Column Amount O3"
 
!===============================================================================
! Attach to L2G grid.
!===============================================================================
  l2g_gridid = he5_gdattach(l2g_fileid, l2g_gname)
 
!===============================================================================
! Test if L2G grid ID is less than zero.  If true, then end execution with fatal
! error.
!===============================================================================
  IF (l2g_gridid < 0) THEN
 
    WRITE(stat_msg, 10) l2g_gridid
10  FORMAT("Fatal error:  L2G grid ID =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_gridid < 0)" test.
 
!===============================================================================
! Get pixel registration code for L2G grid.
!===============================================================================
  call_stat = he5_gdpreginfo(l2g_gridid, dummy)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 15) call_stat
15  FORMAT("Fatal error:  he5_gdpreginfo call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Test if L2G pixel registration code is not equal to HE5_HDFE_CENTER.  If true,
! then end execution with fatal error.
!===============================================================================
  IF (dummy .NE. HE5_HDFE_CENTER) THEN
 
    WRITE(stat_msg, 20) dummy
20  FORMAT("Fatal error:  L2G pixel registration code is incorrect =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (dummy .NE. HE5_HDFE_CENTER)" test.
 
!===============================================================================
! Get origin code for L2G grid.
!===============================================================================
  call_stat = he5_gdorginfo(l2g_gridid, dummy)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 25) call_stat
25  FORMAT("Fatal error:  he5_gdorginfo call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Test if L2G origin code is not equal to HE5_HDFE_GD_LL.  If true, then end
! execution with fatal error.
!===============================================================================
  IF (dummy .NE. HE5_HDFE_GD_LL) THEN
 
    WRITE(stat_msg, 30) dummy
30  FORMAT("Fatal error:  L2G origin code is incorrect =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End if "IF (dummy .NE. HE5_HDFE_GD_LL)" test.
 
!===============================================================================
! Get projection code for L2G grid.
!===============================================================================
  call_stat = he5_gdprojinfo(l2g_gridid, dummy, dummy1, dummy2, dummy3)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 35) call_stat
35  FORMAT("Fatal error:  he5_gdprojinfo call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Test if L2G projection code is not equal to HE5_GCTP_GEO.  If true, then end
! execution with fatal error.
!===============================================================================
  IF (dummy .NE. HE5_GCTP_GEO) THEN
 
    WRITE(stat_msg, 40) dummy
40  FORMAT("Fatal error:  L2G projection code is incorrect =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (dummy .NE. HE5_GCTP_GEO)" test.
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML3_OpenL2GFile
