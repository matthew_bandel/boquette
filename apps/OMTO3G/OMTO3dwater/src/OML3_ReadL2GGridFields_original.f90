!===============================================================================
!
! SUBROUTINE OML3_ReadL2GGridFields
!
! High Level Overview:
!   This subroutine is called by OML3_Main to read some geolocation and data
!   fields from a grid in an OMI L2G product file.
!
! The First Four Arguments Are Inputs:
!   l2g_gridid         - L2G grid ID.
!   l2g_nlon           - Number of longitudes in L2G grid.
!   l2g_nlat           - Number of latitudes in L2G grid.
!   l2g_ncand          - Number of candidates in L2G grid.
!
! The Final Fifteen Arguments Are Outputs:
!   l2g_ncs            - Number of candidate L2 scenes in L2G grid.
!   l2g_gpqf           - Ground pixel quality flags of L2 scenes in L2G grid.
!   l2g_lat            - Latitudes of L2 scenes in L2G grid.
!   l2g_line           - Line numbers of L2 scenes in L2G grid.
!   l2g_lon            - Longitudes of L2 scenes in L2G grid.
!   l2g_orbit          - Orbit numbers of L2 scenes in L2G grid.
!   l2g_raa            - Relative azimuth angles of L2 scenes in L2G grid.
!   l2g_scene          - Scene numbers of L2 scenes in L2G grid.
!   l2g_sza            - Solar zenith angles of L2 scenes in L2G grid.
!   l2g_time           - Observation times of L2 scenes in L2G grid.
!   l2g_vza            - Viewing zenith angles of L2 scenes in L2G grid.
!   l2g_cao3           - Column amounts O3 of L2 scenes in L2G grid.
!   l2g_qflg           - Quality flags of L2 scenes in L2G grid.
!   l2g_rcf            - Radiative cloud fractions of L2 scenes in L2G grid.
!   l2g_uvai           - UV aerosol indices of L2 scenes in L2G grid.
!
! OML3 Subroutine Called:
!   OML3_EndInFailure  - Ends OML3 execution in failure.
!
! HDF-EOS 5 Functions Called:
!   he5_gdrdfld        - Reads field from grid.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   December 21, 2005
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   January 8, 2008 - Replaced reflectivity at 331 nm field with radiative
!                     cloud fraction field.
!   July 18, 2008 - Added read of LineNumber and SceneNumber fields.
!
!===============================================================================
 
SUBROUTINE OML3_ReadL2GGridFields(l2g_gridid, &
                                  l2g_nlon, l2g_nlat, l2g_ncand, &
                                  l2g_ncs, l2g_gpqf, l2g_lat, &
                                  l2g_line, l2g_lon, l2g_orbit, &
                                  l2g_raa, l2g_scene, l2g_sza, &
                                  l2g_time, l2g_vza, l2g_cao3, &
                                  l2g_qflg, l2g_rcf, l2g_uvai)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
!  IMPLICIT NONE
 
!===============================================================================
! Include files.
  !===============================================================================

  INCLUDE 'hdfeos5.inc'               ! Declarations for HDF-EOS 5.   

 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_gridid          ! L2G grid ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlon            ! Number of longitudes in grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlat            ! Number of latitudes in grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_ncand           ! Number of candidates in grid.
 
!===============================================================================
! Declarations of ouput arguments.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat), &
    INTENT(OUT) :: l2g_ncs            ! Number of candidate L2 scenes.
  INTEGER (KIND=2), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_gpqf           ! Ground pixel quality flags of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_lat            ! Latitudes of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_line           ! Line numbers of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_lon            ! Longitudes of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_orbit          ! Orbit numbers of L2 scenes in L2G grid.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_raa            ! Relative azimuth angles of L2 scenes.
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_scene          ! Scene numbers of L2 scenes in L2G grid.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_sza            ! Solar zenith angles of L2 scenes.
  REAL (KIND=8), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_time           ! Observation times of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_vza            ! Viewing zenith angles of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_cao3           ! Column amounts O3 of L2 scenes.
  INTEGER (KIND=2), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_qflg           ! Quality flags of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_rcf            ! Radiative cloud fractions of L2 scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat,l2g_ncand), &
    INTENT(OUT) :: l2g_uvai           ! UV aerosol indices of L2 scenes.
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=8), &
    DIMENSION(2) :: start2            ! 2-D start array for he5_gdrdfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: start3            ! 3-D start array for he5_gdrdfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: stride2           ! 2-D stride array for he5_gdrdfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: stride3           ! 3-D stride array for he5_gdrdfld.
  INTEGER (KIND=8), &
    DIMENSION(2) :: edge2             ! 2-D edge array for he5_gdrdfld.
  INTEGER (KIND=8), &
    DIMENSION(3) :: edge3             ! 3-D edge array for he5_gdrdfld.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat              ! Function call status.
  CHARACTER (LEN=256) :: stat_msg            ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML3_ReadL2GGridFields.f90"  ! Code file reference.
 
!===============================================================================
! Declarations of HDF-EOS 5 function calls.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdrdfld                  ! Reads field from grid.
 
!===============================================================================
! Set start2, stride2 and edge2 arrays for 2-D geolocation and data fields.
!===============================================================================
  start2(1) = 0
  start2(2) = 0
  stride2(1) = 1
  stride2(2) = 1
  edge2(1) = l2g_nlon
  edge2(2) = l2g_nlat

!===============================================================================
! Set start3, stride3 and edge3 arrays for 3-D geolocation and data fields.
!===============================================================================
  start3(1) = 0
  start3(2) = 0
  start3(3) = 0
  stride3(1) = 1
  stride3(2) = 1
  stride3(3) = 1
  edge3(1) = l2g_nlon
  edge3(2) = l2g_nlat
  edge3(3) = l2g_ncand
 
!===============================================================================
! Read number of candidate L2 scenes from L2G grid.
  !===============================================================================


  call_stat = he5_gdrdfld(l2g_gridid, "NumberOfCandidateScenes", &
       start2, stride2, edge2, l2g_ncs)

!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 2) call_stat
2   FORMAT("Fatal error:  NumberOfCandScenes he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read ground pixel quality flags of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "GroundPixelQualityFlags", &
                          start3, stride3, edge3, l2g_gpqf)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 4) call_stat
4   FORMAT("Fatal error:  GroundPixelQualityFlags he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read latitudes of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "Latitude", &
                          start3, stride3, edge3, l2g_lat)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 6) call_stat
6  FORMAT("Fatal error:  Latitude he5_gdrdfld call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read line numbers of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "LineNumber", &
                          start3, stride3, edge3, l2g_line)

!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN

    WRITE(stat_msg, 8) call_stat
8   FORMAT("Fatal error:  LineNumber he5_gdrdfld call status =", &
      i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)

  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read longitudes of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "Longitude", &
                          start3, stride3, edge3, l2g_lon)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 10) call_stat
10 FORMAT("Fatal error:  Longitude he5_gdrdfld call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read orbit numbers of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "OrbitNumber", &
                          start3, stride3, edge3, l2g_orbit)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 12) call_stat
12  FORMAT("Fatal error:  OrbitNumber he5_gdrdfld call status =", &
      i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read relative azimuth angles of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "RelativeAzimuthAngle", &
                          start3, stride3, edge3, l2g_raa)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 14) call_stat
14  FORMAT("Fatal error:  RelativeAzimuthAngle he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read scene numbers of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "SceneNumber", &
                          start3, stride3, edge3, l2g_scene)

!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN

    WRITE(stat_msg, 16) call_stat
16  FORMAT("Fatal error:  SceneNumber he5_gdrdfld call status =", &
      i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)

  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read solar zenith angles of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "SolarZenithAngle", &
                          start3, stride3, edge3, l2g_sza)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 18) call_stat
18  FORMAT("Fatal error:  SolarZenithAngle he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read observation times of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "Time", &
                          start3, stride3, edge3, l2g_time)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 20) call_stat
20  FORMAT("Fatal error:  Time he5_gdrdfld call status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read viewing zenith angles of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "ViewingZenithAngle", &
                          start3, stride3, edge3, l2g_vza)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 25) call_stat
25  FORMAT("Fatal error:  ViewingZenithAngle he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read column amounts O3 of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "ColumnAmountO3", &
                          start3, stride3, edge3, l2g_cao3)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 30) call_stat
30  FORMAT("Fatal error:  ColumnAmountO3 he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read quality flags of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "QualityFlags", &
                          start3, stride3, edge3, l2g_qflg)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 35) call_stat
35  FORMAT("Fatal error:  QualityFlags he5_gdrdfld call ", &
           "status =", i10, "!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read radiative cloud fractions of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "RadiativeCloudFraction", &
                          start3, stride3, edge3, l2g_rcf)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 40) call_stat
40  FORMAT("Fatal error:  RadiativeCloudFraction he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Read UV aerosol indices of L2 scenes from L2G grid.
!===============================================================================
  call_stat = he5_gdrdfld(l2g_gridid, "UVAerosolIndex", &
                          start3, stride3, edge3, l2g_uvai)
 
!===============================================================================
! Test if function call status is less than zero.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (call_stat < 0) THEN
 
    WRITE(stat_msg, 45) call_stat
45  FORMAT("Fatal error:  UVAerosolIndex he5_gdrdfld call ", &
           "status =",i10,"!")
    CALL OML3_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (call_stat < 0)" test.
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML3_ReadL2GGridFields
