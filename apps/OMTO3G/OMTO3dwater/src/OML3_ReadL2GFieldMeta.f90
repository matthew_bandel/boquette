!===============================================================================
!
! SUBROUTINE OML3_ReadL2GFieldMeta
!
! High Level Overview:
!   This subroutine is called by OML3_Main to read the grid-field Metadata from
!   an OMI L2G product file.
!
! The First Thirteen Arguments Are Inputs:
!   l2g_gridid         - L2G grid ID.
!   gfi1u_nfield       - Number of uns. 1-B int. fields.
!   gfi1u_names        - Names of uns. 1-B int. fields.
!   gfi2_nfield        - Number of 2-B int. fields.
!   gfi2_names         - Names of 2-B int. fields.
!   gfi2u_nfield       - Number of uns. 2-B int. fields.
!   gfi2u_names        - Names of uns. 2-B int. fields.
!   gfi4_nfield        - Number of 4-B int. fields.
!   gfi4_names         - Names of 4-B int. fields.
!   gfr4_nfield        - Number of 4-B real fields.
!   gfr4_names         - Names of 4-B real fields.
!   gfr8_nfield        - Number of 8-B real fields.
!   gfr8_names         - Names of 8-B real fields.
!
! The Final Forty Two Arguments Are Outputs:
!   gfi1u_units        - Units for uns. 1-B int. fields.
!   gfi1u_title        - Titles for uns. 1-B int. fields.
!   gfi1u_ufdef        - UFDs for uns. 1-B int. fields.
!   gfi1u_sfactor      - Scale factors for uns. 1-B int. fields.
!   gfi1u_offset       - Offsets for uns. 1-B int. fields.
!   gfi1u_vrange       - Valid ranges for uns. 1-B int. fields.
!   gfi1u_mvalue       - Missing values for uns. 1-B int. fields.
!   gfi2_units         - Units for 2-B int. fields.
!   gfi2_title         - Titles for 2-B int. fields.
!   gfi2_ufdef         - UFDs for 2-B int. fields.
!   gfi2_sfactor       - Scale factors for 2-B int. fields.
!   gfi2_offset        - Offsets for 2-B int. fields.
!   gfi2_vrange        - Valid ranges for 2-B int. fields.
!   gfi2_mvalue        - Missing values for 2-B int. fields.
!   gfi2u_units        - Units for uns. 2-B int. fields.
!   gfi2u_title        - Titles for uns. 2-B int. fields.
!   gfi2u_ufdef        - UFDs for uns. 2-B int. fields.
!   gfi2u_sfactor      - Scale factors for uns. 2-B int. fields.
!   gfi2u_offset       - Offsets for uns. 2-B int. fields.
!   gfi2u_vrange       - Valid ranges for uns. 2-B int. fields.
!   gfi2u_mvalue       - Missing values for uns. 2-B int. fields.
!   gfi4_units         - Units for 4-B int. fields.
!   gfi4_title         - Titles for 4-B int. fields.
!   gfi4_ufdef         - UFDs for 4-B int. fields.
!   gfi4_sfactor       - Scale factors for 4-B int. fields.
!   gfi4_offset        - Offsets for 4-B int. fields.
!   gfi4_vrange        - Valid ranges for 4-B int. fields.
!   gfi4_mvalue        - Missing values for 4-B int. fields.
!   gfr4_units         - Units for 4-B real fields.
!   gfr4_title         - Titles for 4-B real fields.
!   gfr4_ufdef         - UFDs for 4-B real fields.
!   gfr4_sfactor       - Scale factors for 4-B real fields.
!   gfr4_offset        - Offsets for 4-B real fields.
!   gfr4_vrange        - Valid ranges for 4-B real fields.
!   gfr4_mvalue        - Missing values for 4-B real fields.
!   gfr8_units         - Units for 8-B real fields.
!   gfr8_title         - Titles for 8-B real fields.
!   gfr8_ufdef         - UFDs for 8-B real fields.
!   gfr8_sfactor       - Scale factors for 8-B real fields.
!   gfr8_offset        - Offsets for 8-B real fields.
!   gfr8_vrange        - Valid ranges for 8-B real fields.
!   gfr8_mvalue        - Missing values for 8-B real fields.
!
! OML3 Subroutine Called:
!   OML3_EndInFailure  - Ends OML3 execution in failure.
!
! HDF-EOS 5 Function Called:
!   he5_gdrdlattr      - Reads local attribute for grid field.
!
! Author(s):
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: Peter_Leonard@ssaihq.com
!   Phone: (301) 867-2190
!
! Initial Version:
!   September 16, 2005
!
! Revision History:
!
!===============================================================================
 
SUBROUTINE OML3_ReadL2GFieldMeta(l2g_gridid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names, &
                                 gfi1u_units, gfi1u_title, gfi1u_ufdef, &
                                 gfi1u_sfactor, gfi1u_offset, &
                                 gfi1u_vrange, gfi1u_mvalue, &
                                 gfi2_units, gfi2_title, gfi2_ufdef, &
                                 gfi2_sfactor, gfi2_offset, &
                                 gfi2_vrange, gfi2_mvalue, &
                                 gfi2u_units, gfi2u_title, gfi2u_ufdef, &
                                 gfi2u_sfactor, gfi2u_offset, &
                                 gfi2u_vrange, gfi2u_mvalue, &
                                 gfi4_units, gfi4_title, gfi4_ufdef, &
                                 gfi4_sfactor, gfi4_offset, &
                                 gfi4_vrange, gfi4_mvalue, &
                                 gfr4_units, gfr4_title, gfr4_ufdef, &
                                 gfr4_sfactor, gfr4_offset, &
                                 gfr4_vrange, gfr4_mvalue, &
                                 gfr8_units, gfr8_title, gfr8_ufdef, &
                                 gfr8_sfactor, gfr8_offset, &
                                 gfr8_vrange, gfr8_mvalue)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'           ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_gridid      ! L2G grid ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfi1u_nfield    ! Number of uns. 1-B int. fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfi2_nfield     ! Number of 2-B int. fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfi2u_nfield    ! Number of uns. 2-B int. fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfi4_nfield     ! Number of 4-B int. fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfr4_nfield     ! Number of 4-B real fields.
  INTEGER (KIND=4), &
    INTENT(IN) :: gfr8_nfield     ! Number of 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfi1u_names     ! Names of uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfi2_names      ! Names of 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfi2u_names     ! Names of uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfi4_names      ! Names of 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfr4_names      ! Names of 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(IN) :: gfr8_names      ! Names of 8-B real fields.
 
!===============================================================================
! Declarations of output arguments (note that UFD = Unique Field Definition).
!===============================================================================
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_units    ! Units for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_units     ! Units for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_units    ! Units for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_units     ! Units for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_units     ! Units for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_units     ! Units for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_title    ! Titles for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_title     ! Titles for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_title    ! Titles for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_title     ! Titles for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_title     ! Titles for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_title     ! Titles for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_ufdef    ! UFDs for uns. 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_ufdef     ! UFDs for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_ufdef    ! UFDs for uns. 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_ufdef     ! UFDs for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_ufdef     ! UFDs for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_ufdef     ! UFDs for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_sfactor  ! Scale factors for uns. 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_sfactor   ! Scale factors for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_sfactor  ! Scale factors for uns. 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_sfactor   ! Scale factors for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_sfactor   ! Scale factors for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_sfactor   ! Scale factors for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_offset   ! Offsets for uns. 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_offset    ! Offsets for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_offset   ! Offsets for uns. 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_offset    ! Offsets for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_offset    ! Offsets for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_offset    ! Offsets for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfi1u_vrange   ! Valid ranges for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfi2_vrange    ! Valid ranges for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfi2u_vrange   ! Valid ranges for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfi4_vrange    ! Valid ranges for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfr4_vrange    ! Valid ranges for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100,2), &
    INTENT(OUT) :: gfr8_vrange    ! Valid ranges for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_mvalue   ! Missing values for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_mvalue    ! Missing values for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_mvalue   ! Missing values for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_mvalue    ! Missing values for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_mvalue    ! Missing values for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_mvalue    ! Missing values for 8-B real fields.

!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=4) :: ifield      ! Grid field counter.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat             ! Function call status.
  CHARACTER (LEN=256) :: stat_msg           ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML3_ReadL2GFieldMeta.f90"  ! Code file reference.
 
!===============================================================================
! Declaration of HDF-EOS 5 function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdrdlattr     ! Reads local attribute for grid field.
 
!===============================================================================
! Initialize grid-field Metadata.
!===============================================================================
  gfi1u_units(1:100) = " "
  gfi2_units(1:100) = " "
  gfi2u_units(1:100) = " "
  gfi4_units(1:100) = " "
  gfr4_units(1:100) = " "
  gfr8_units(1:100) = " "
  gfi1u_title(1:100) = " "
  gfi2_title(1:100) = " "
  gfi2u_title(1:100) = " "
  gfi4_title(1:100) = " "
  gfr4_title(1:100) = " "
  gfr8_title(1:100) = " "
  gfi1u_ufdef(1:100) = " "
  gfi2_ufdef(1:100) = " "
  gfi2u_ufdef(1:100) = " "
  gfi4_ufdef(1:100) = " "
  gfr4_ufdef(1:100) = " "
  gfr8_ufdef(1:100) = " "
  gfi1u_sfactor(1:100) = -1.0d+30
  gfi2_sfactor(1:100) = -1.0d+30
  gfi2u_sfactor(1:100) = -1.0d+30
  gfi4_sfactor(1:100) = -1.0d+30
  gfr4_sfactor(1:100) = -1.0d+30
  gfr8_sfactor(1:100) = -1.0d+30
  gfi1u_offset(1:100) = -1.0d+30
  gfi2_offset(1:100) = -1.0d+30
  gfi2u_offset(1:100) = -1.0d+30
  gfi4_offset(1:100) = -1.0d+30
  gfr4_offset(1:100) = -1.0d+30
  gfr8_offset(1:100) = -1.0d+30
  gfi1u_vrange(1:100,1:2) = -1 ! 255
  gfi2_vrange(1:100,1:2) = -32767
  gfi2u_vrange(1:100,1:2) = -1 ! 65535
  gfi4_vrange(1:100,1:2) = -2000000000
  gfr4_vrange(1:100,1:2) = -1.0e+30
  gfr8_vrange(1:100,1:2) = -1.0d+30
  gfi1u_mvalue(1:100) = -1 ! 255
  gfi2_mvalue(1:100) = -32767
  gfi2u_mvalue(1:100) = -1 ! 65535
  gfi4_mvalue(1:100) = -2000000000
  gfr4_mvalue(1:100) = -1.0e+30
  gfr8_mvalue(1:100) = -1.0d+30
 
!===============================================================================
! Read grid-field Metadata from L2G file:
!===============================================================================
 
!===============================================================================
! Read grid-field Metadata for unsigned 1-B integer fields.
!===============================================================================
  DO ifield = 1, gfi1u_nfield

    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "Units", gfi1u_units(ifield))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "Title", gfi1u_title(ifield))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "UniqueFieldDefinition", gfi1u_ufdef(ifield))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "ScaleFactor", gfi1u_sfactor(ifield))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "Offset", gfi1u_offset(ifield))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "ValidRange", gfi1u_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 1
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi1u_names(ifield), &
                              "MissingValue", gfi1u_mvalue(ifield))
    IF (call_stat < 0) GO TO 1

  END DO
 
!===============================================================================
! Read grid-field Metadata for 2-B integer fields.
!===============================================================================
  DO ifield = 1, gfi2_nfield
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "Units", gfi2_units(ifield))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "Title", gfi2_title(ifield))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "UniqueFieldDefinition", gfi2_ufdef(ifield))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "ScaleFactor", gfi2_sfactor(ifield))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "Offset", gfi2_offset(ifield))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "ValidRange", gfi2_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 3
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2_names(ifield), &
                              "MissingValue", gfi2_mvalue(ifield))
    IF (call_stat < 0) GO TO 3
 
  END DO
 
!===============================================================================
! Read grid-field Metadata for unsigned 2-B integer fields.
!===============================================================================
  DO ifield = 1, gfi2u_nfield
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "Units", gfi2u_units(ifield))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "Title", gfi2u_title(ifield))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "UniqueFieldDefinition", gfi2u_ufdef(ifield))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "ScaleFactor", gfi2u_sfactor(ifield))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "Offset", gfi2u_offset(ifield))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "ValidRange", gfi2u_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 4
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi2u_names(ifield), &
                              "MissingValue", gfi2u_mvalue(ifield))
    IF (call_stat < 0) GO TO 4
 
  END DO
 
!===============================================================================
! Read grid-field Metadata for 4-B integer fields.
!===============================================================================
  DO ifield = 1, gfi4_nfield
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "Units", gfi4_units(ifield))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "Title", gfi4_title(ifield))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "UniqueFieldDefinition", gfi4_ufdef(ifield))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "ScaleFactor", gfi4_sfactor(ifield))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "Offset", gfi4_offset(ifield))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "ValidRange", gfi4_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 5
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfi4_names(ifield), &
                              "MissingValue", gfi4_mvalue(ifield))
    IF (call_stat < 0) GO TO 5
 
  END DO
 
!===============================================================================
! Read grid-field Metadata for 4-B real fields.
!===============================================================================
  DO ifield = 1, gfr4_nfield
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "Units", gfr4_units(ifield))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "Title", gfr4_title(ifield))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "UniqueFieldDefinition", gfr4_ufdef(ifield))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "ScaleFactor", gfr4_sfactor(ifield))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "Offset", gfr4_offset(ifield))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "ValidRange", gfr4_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 6
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr4_names(ifield), &
                              "MissingValue", gfr4_mvalue(ifield))
    IF (call_stat < 0) GO TO 6
 
  END DO
 
!===============================================================================
! Read grid-field Metadata for 8-B real fields.
!===============================================================================
  DO ifield = 1, gfr8_nfield
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "Units", gfr8_units(ifield))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "Title", gfr8_title(ifield))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "UniqueFieldDefinition", gfr8_ufdef(ifield))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "ScaleFactor", gfr8_sfactor(ifield))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "Offset", gfr8_offset(ifield))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "ValidRange", gfr8_vrange(ifield,1:2))
    IF (call_stat < 0) GO TO 7
 
    call_stat = he5_gdrdlattr(l2g_gridid, gfr8_names(ifield), &
                              "MissingValue", gfr8_mvalue(ifield))
    IF (call_stat < 0) GO TO 7
 
  END DO
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gdrdlattr for one of the various data types, and so end
! execution with fatal error.
!===============================================================================
1 WRITE(stat_msg, 2) gfi1u_names(ifield),call_stat
2 FORMAT("Fatal error while reading grid-field Metadatum for ",a9, &
         ":  he5_gdrdlattr call status =",i10,"!")
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
3 WRITE(stat_msg, 2) gfi2_names(ifield),call_stat
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
4 WRITE(stat_msg, 2) gfi2u_names(ifield),call_stat
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
5 WRITE(stat_msg, 2) gfi4_names(ifield),call_stat
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
6 WRITE(stat_msg, 2) gfr4_names(ifield),call_stat
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
7 WRITE(stat_msg, 2) gfr8_names(ifield),call_stat
  CALL OML3_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML3_ReadL2GFieldMeta
