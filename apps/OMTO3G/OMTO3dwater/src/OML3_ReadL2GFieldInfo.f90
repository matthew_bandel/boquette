!===============================================================================
!
! SUBROUTINE OML3_ReadL2GFieldInfo
!
! High Level Overview:
!   This subroutine is called by OML3_Main to read the names of the grid fields
!   from an OMI L2G product file.
!
! The First Argument Is An Input:
!   l2g_gridid          - L2G grid ID.
!
! The Final Twelve Arguments Are Outputs:
!   gfi1u_nfield        - Number of unsigned 1-byte integer fields.
!   gfi1u_names         - Names of unsigned 1-byte integer fields.
!   gfi2_nfield         - Number of 2-byte integer fields.
!   gfi2_names          - Names of 2-byte integer fields.
!   gfi2u_nfield        - Number of unsigned 2-byte integer fields.
!   gfi2u_names         - Names of unsigned 2-byte integer fields.
!   gfi4_nfield         - Number of 4-byte integer fields.
!   gfi4_names          - Names of 4-byte integer fields.
!   gfr4_nfield         - Number of 4-byte real fields.
!   gfr4_names          - Names of 4-byte real fields.
!   gfr8_nfield         - Number of 8-byte real fields.
!   gfr8_names          - Names of 8-byte real fields.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   December 21, 2005
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   January 8, 2008 - Replaced reflectivity at 331 nm field with radiative
!                     cloud fraction field.
!   July 18, 2008 - Added LineNumber and SceneNumber fields.
!
!===============================================================================
 
SUBROUTINE OML3_ReadL2GFieldInfo(l2g_gridid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'          ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input argument.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_gridid     ! L2G grid ID.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi1u_nfield  ! Number of unsigned 1-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi2_nfield   ! Number of 2-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi2u_nfield  ! Number of unsigned 2-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfi4_nfield   ! Number of 4-byte integer fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfr4_nfield   ! Number of 4-byte real fields.
  INTEGER (KIND=4), &
    INTENT(OUT) :: gfr8_nfield   ! Number of 8-byte real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi1u_names   ! Names of unsigned 1-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2_names    ! Names of 2-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi2u_names   ! Names of unsigned 2-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfi4_names    ! Names of 4-byte integer fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr4_names    ! Names of 4-byte real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100), &
    INTENT(OUT) :: gfr8_names    ! Names of 8-byte real fields.
 
!===============================================================================
! Initialize names of fields.
!===============================================================================
  gfi1u_names(1:100) = " "
  gfi2_names(1:100) = " "
  gfi2u_names(1:100) = " "
  gfi4_names(1:100) = " "
  gfr4_names(1:100) = " "
  gfr8_names(1:100) = " "
 
!===============================================================================
! Read names of grid fields from L2G file (names are not currently read from L2G
! file, but instead are hard-wired below):
!===============================================================================

  ! add in water fraction and land classificiation - MB 07/25/24
!  gfi1u_nfield = 0
  gfi1u_nfield = 2
  gfi1u_names(1) = "WaterFraction"
  gfi1u_names(2) = "LandWaterClassification"

  gfi2_nfield = 0
 
  gfi2u_nfield = 2
  gfi2u_names(1) = "GroundPixelQualityFlags"
  gfi2u_names(2) = "QualityFlags"
 
  gfi4_nfield = 4
  gfi4_names(1)  = "LineNumber"
  gfi4_names(2)  = "NumberOfCandidateScenes"
  gfi4_names(3)  = "OrbitNumber"
  gfi4_names(4)  = "SceneNumber"
 
  gfr4_nfield = 8
  gfr4_names(1)  = "Latitude"
  gfr4_names(2)  = "Longitude"
  gfr4_names(3)  = "RelativeAzimuthAngle"
  gfr4_names(4)  = "SolarZenithAngle"
  gfr4_names(5)  = "ViewingZenithAngle"
  gfr4_names(6)  = "ColumnAmountO3"
  gfr4_names(7)  = "RadiativeCloudFraction"
  gfr4_names(8)  = "UVAerosolIndex"
 
  gfr8_nfield = 1
  gfr8_names(1)  = "Time"
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
END SUBROUTINE OML3_ReadL2GFieldInfo
