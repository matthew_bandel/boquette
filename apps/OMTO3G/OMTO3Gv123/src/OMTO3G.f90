!===============================================================================
!
! PROGRAM OMTO3G (OML2G for OMTO3)
!
! OML2G High Level Overview:
!   This is the main program for the OML2G Product Generation Executive (PGE).
!
!   The OML2G PGE creates an Ozone Monitoring Instrument (OMI) daily Level 2G
!   (L2G) gridded data product file from (as many as) 16 OMI orbital Level 2
!   (L2) swath data product files.
!
!   Each OMI L2G product file contains 24 UTC hours of OMI L2 data from a single
!   OMI L2 product subsetted onto a longitude-latitude grid.
!
!   An OMI L2G day is defined to be the 24 hours that lie between UTC times of
!   0 hours, 0 minutes, 0 seconds and 23 hours, 59 minutes, 59.999999 seconds.
!
!   The OMI L2G product currently excludes L2 data collected in spatial and
!   spectral zoom modes.
!
!   The format of the OMI L2G product files is consistent with the document
!   entitled "HDF-EOS Aura File Format Guidelines" by C. Craig et al.
!
! OML2G Algorithm:
!   The OML2G PGE populates each cell in the L2G grid with data for all L2
!   "scenes" that
!   1) have observation times that lie within the L2G day in question,
!   2) have centers that lie within the L2G grid cell in question, and
!   3) are "good".
!
!   The OML2G PGE makes use of different definitions of "good" to populate
!   the L2G grids that correspond to different L2 products.
!
!   In the case of the OMI L2G product named OMTO3G, which is L2G for the L2
!   product named OMTO3, the definition of a "good" scene is one that has
!   i)  a solar zenith angle that is less than or equal to 88.0 degrees, and
!   ii) a column amount O3 that is not equal to the missing value.
!
!   The OMI L2 data that are reproduced in the OMI L2G product are not averaged
!   or weighted in any way by the OML2G PGE.
!
! Adopted L2G Grid:
!   The adopted L2G grid is a 0.25-degree by 0.25-degree grid in longitude and
!   latitude.  The dimensions of this grid are 1440 by 720.  The origin of the
!   grid is at lower left.  That is, the grid cell at coordinates (1, 1)
!   is centered at (longitude = -179.875 , latitude = -89.875),
!   and the grid cell at coordinates (1440, 720)
!   is centered at (longitude =  179.875 , latitude =  89.875).
!
!   Each "good" L2 scene is mapped onto only one L2G grid cell.
!
!   The number of L2 scenes that are mapped onto a given L2G grid cell can range
!   from 0 to 12.  These data are stored in an additional dimension of the grid.
!
!   The adopted L2G grid is consistent with the document entitled "Definition
!   of OMI Grids for Level 3 and Level 4 Data Products" by J.P. Veefkind et al.
!
! L2 Product IDs:
!   The L2 Product ID (stored in the variable named "l2_prodid") for a given
!   OML2G run is simply the name of the corresponding L2 product.  Here are the
!   supported L2 products:
!     OMAERO   - Aerosol Optical Thickness & Single Scattering
!     OMAERUV  - Aerosol Extinction & Absorption Optical Depth
!     OMCLDO2  - Cloud Pressure and Fraction (O2-O2 Absorption)
!     OMCLDRR  - Cloud Pressure and Fraction (Raman Scattering)
!     OMDOAO3  - Ozone (O3) DOAS Total Column
!     OMHCHO   - Formaldehyde (HCHO) Total Column
!     OMNO2    - Nitrogen Dioxide (NO2) Total & Tropospheric Column
!     OMSO2    - Sulphur Dioxide (SO2) Total Column
!     OMTO3    - Ozone (O3) Total Column
!
!   The following L2 products may be supported in the future:
!     OMBRO    - Bromine Monoxide (BrO) Total Column
!     OMOCLO   - Chlorine Dioxide (OClO) Slant Column
!     OMSAO3   - Ozone (O3) Slant Column
!
! OML2G Subroutines Called:
!   OML2G_ReadPCF            - Reads OML2G PCF.
!   OML2G_FindL2GDay         - Finds L2G day of year and other temporal items.
!   OML2G_EndInFailure       - Ends OML2G execution in failure.
!
!   OML2G_OpenL2File         - Opens L2 file and swath.
!   OML2G_ReadL2ECSMeta      - Reads ECS Metadata from L2 file.
!   OML2G_ReadL2SwathMeta    - Reads swath-level Metadata from L2 file.
!   OML2G_ReadL2FieldInfo    - Reads names of swath fields from L2 file.
!   OML2G_ReadL2FieldMeta    - Reads swath-field Metadata from L2 file.
!   OML2G_ReadL2GeoLine      - Reads one line of geolocation from L2 file.
!   OML2G_ReadL2DatLine      - Reads one line of data-field data from L2 file.
!   OML2G_CloseL2File        - Closes L2 swath and file.
!
!   OML2G_OpenL2GFile        - Opens L2G file and creates grid.
!   OML2G_WriteL2GFileMeta   - Writes file-level Metadata to L2G file.
!   OML2G_WriteL2GGridMeta   - Writes grid-level Metadata to L2G file.
!   OML2G_WriteL2GGeoGrid    - Writes geolocation fields to L2G file.
!   OML2G_WriteL2GDatGrid    - Writes data fields to L2G file.
!   OML2G_WriteL2GAplGrid    - Writes 4-D a priori layer O3 field to L2G file.
!   OML2G_WriteL2GLefGrid    - Writes 4-D layer efficiencies field to L2G file.
!   OML2G_WriteL2GResGrid    - Writes 4-D residuals field to L2G file.
!   OML2G_WriteL2GFieldMeta  - Writes grid-field Metadata to L2G file.
!   OML2G_CloseL2GFile       - Closes L2G grid and file.
!   OML2G_WriteL2GECSMeta    - Writes ECS Metadata to L2G files.
!
! External Function Called:
!   OMI_SMF_setmsg           - OMI messaging function.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 27, 2005
!
! Revision History:
!   August 17, 2005 - Increased value of l2g_nwavel parameter from 10 to 12.
!                   - Increased first dimension of l2_res from 10 to 12.
!                   - Increased dimension of l2_wav from 10 to 12.
!                   - Increased value of l2g_ncand parameter from 10 to 13.
!                   - Added logic to end execution in error if l2g_ncgcmax is
!                     greater than l2g_ncand.
!                   - Added OML2G_WriteL2GAplGrid, OML2G_WriteL2GLefGrid and
!                     OML2G_WriteL2GResGrid calls to write 4-D data fields
!                     to L2G file.
!                   - Added allocation and deallocation statements for 3-D and
!                     4-D arrays to address problem with memory limit.
!                   - Corrected setting of MissingValue Metadata.
!   August 18, 2005 - Decreased value of l2g_ncand parameter from 13 to 12 to
!                     reduce memory requirements to make PGE fit on ominions.
!   August 19, 2005 - Separated processing of l2g_aplo3 from other L2G fields.
!                   - Separated processing of l2g_leff from other L2G fields.
!                   - Separated processing of l2g_res from other L2G fields.
!                   - Increased value of l2g_ncand parameter from 12 to 15
!                     following above improvements in memory usage.
!   August 26, 2005 - Reversed dimension order of 2-D, 3-D and 4-D fields.
!   September 2, 2005 - Removed l2g_onrange and l2g_oprange, and instead write
!                       all non-fill elements of l2_orbit and l2_orbper to L2G
!                       file to comply with HDF-EOS Aura File Format Guidlines.
!                     - Added missing value arguments to OML2G_WriteL2G*Grid
!                       subroutine calls to set FillValue Metadata.
!   September 20, 2005 - Separated processing of 3-D geolocation fields from
!                        processing of 3-D data fields.
!   September 29, 2005 - Reversed order of dimensions (again).
!   September 30, 2005 - Increased accuracy of angles to three decimal places.
!   November 10, 2005 - Included l2_qapmd array in OML2G_WriteL2GGridMeta call.
!   November 10, 2005 - Moved l2_qapmd array to OML2G_WriteL2GFileMeta call.
!   March 28, 2006 - Made revisions to file header and other comments.
!   May 24, 2006 - Included l2_prodid in call to OML2G_WriteL2GECSMeta.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A100
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History (Continued):
!   March 21, 2007 - Added screening to exclude Zoom-Mode orbits.
!                  - Check each line of each L2 file for bad geolocation.
!                  - Count no. of lines in each L2 file with bad geolocation.
!                  - Report each line with bad geolocation in PGE log file.
!   March 22, 2007 - Added calculation of L2 orbital line ranges (l2_lrange).
!   March 23, 2007 - Increased number of characters in l2g_pgevers to 20.
!   March 27, 2007 - Use DNINT for rounding floating point field values.
!   October 18, 2007 - Several minor improvements.
!   November 13, 2007 - Renamed CloudFraction field to RadiativeCloudFraction.
!                     - Renamed CloudTopPressure field to CloudPressure.
!   June 19, 2010 - Added XTrackQualityFlags field.
!   July 13, 2011 - Added test for consistency of l2g_dyofyr .
!
!===============================================================================
 
PROGRAM OMTO3G
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'PGS_SMF.f'       ! Declarations for Toolkit Fortran SMF functions.
  INCLUDE 'PGS_OMI_1900.f'  ! Declarations for general OMI messages.
 
!===============================================================================
! L2 file quantities.
!===============================================================================
  CHARACTER (LEN=8) :: l2_prodid  ! L2 Product ID.
  INTEGER (KIND=4) :: l2_nfile    ! Number of L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_fname     ! List of L2 file names.
  INTEGER (KIND=4) :: l2_ifirst   ! First L2 file entirely in most frequent day.
  INTEGER (KIND=4) :: l2_fileid   ! L2 file ID.
  INTEGER (KIND=4) :: l2_swathid  ! L2 swath ID.
 
!===============================================================================
! L2 file Metadata.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_orbit   ! List of L2 orbit numbers.
  INTEGER (KIND=4), &
    DIMENSION(20,2) :: l2_lrange  ! L2 orbital line ranges.
  CHARACTER (LEN=10), &
    DIMENSION(20) :: l2_ecd     ! Equator crossing dates of L2 files.
  CHARACTER (LEN=16), &
    DIMENSION(20) :: l2_ect     ! Equator crossing times of L2 files.
  REAL (KIND=8), &
    DIMENSION(20) :: l2_ecl     ! Equator crossing longitudes of L2 files.
  CHARACTER (LEN=10), &
    DIMENSION(20) :: l2_rbd     ! Range beginning dates of L2 files.
  CHARACTER (LEN=16), &
    DIMENSION(20) :: l2_rbt     ! Range beginning times of L2 files.
  CHARACTER (LEN=10),&
    DIMENSION(20) :: l2_red     ! Range ending dates of L2 files.
  CHARACTER (LEN=16), &
    DIMENSION(20) :: l2_ret     ! Range ending times of L2 files.
  REAL (KIND=8), &
    DIMENSION(20) :: l2_orbper  ! Orbital periods of L2 granules.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_qapmd   ! QA percent missing data of L2 files.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nm      ! Numbers of measurements in L2 files.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nz      ! Numbers of zoom measurements in L2 files.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nspatz  ! Numbers of spatial zoom meas. in L2 files.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nspecz  ! Numbers of spectral zoom meas. in L2 files.
  CHARACTER (LEN=5), &
    DIMENSION(20) :: l2_ed      ! Expedited data indicators for L2 files.
  CHARACTER (LEN=5), &
    DIMENSION(20) :: l2_saac    ! SAA crossing indicators for L2 files.
  CHARACTER (LEN=5), &
    DIMENSION(20) :: l2_sm      ! Spacecraft maneuver indicators for L2 files.
  CHARACTER (LEN=5), &
    DIMENSION(20) :: l2_se      ! Solar eclipse indicators for L2 files.
  REAL (KIND=8), &
    DIMENSION(6,20) :: l2_mcp   ! Master clock periods for L2 files.
  REAL (KIND=8), &
    DIMENSION(6,20) :: l2_et    ! Exposure times for L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_cps     ! Cloud pressure sources for L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_tps     ! Terrain pressure sources for L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_ts      ! Temperature sources for L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_sis     ! Snow ice sources for L2 files.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_apops   ! A priori ozone profile sources for L2 files.
 
!===============================================================================
! L2 counters and local variables.
!===============================================================================
  INTEGER (KIND=4) :: l2_ifile    ! L2 file counter (1 to l2_nfile).
  INTEGER (KIND=4) :: l2_ifield   ! L2 swath field counter.
  INTEGER (KIND=4) :: l2_iline    ! L2 line counter (1 to l2_nline).
  INTEGER (KIND=4) :: l2_iscene   ! L2 scene counter (1 to l2_nscene).
  REAL(KIND=4) :: l2_minsza       ! Minimum solar zenith angle in line of data.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nlbad     ! L2 "bad" line counter.
  INTEGER (KIND=4), &
    DIMENSION(20) :: l2_nlgood    ! L2 "good" line counter.
  REAL (KIND=4) :: l2_dummy       ! L2 dummy test variable.
  REAL (KIND=4) :: l2_pathl       ! Path length of scene.
 
!===============================================================================
! L2 swath Metadata.
!===============================================================================
  INTEGER (KIND=4) :: l2_nline    ! Number of lines in L2 swath.
  INTEGER (KIND=4) :: l2_nscene   ! Number of scenes in L2 line.
  INTEGER (KIND=4) :: l2_nlayers  ! Number of layers in L2 swath.
  INTEGER (KIND=4) :: l2_nwavel   ! Number of wavelengths in L2 swath.
 
!===============================================================================
! L2 field names and Metadata (note that UFD = Unique Field Definition).
!===============================================================================
  INTEGER (KIND=4) :: gfi1u_nfield    ! Number of unsigned 1-B int. fields.
  INTEGER (KIND=4) :: gfi2_nfield     ! Number of 2-B int. fields.
  INTEGER (KIND=4) :: gfi2u_nfield    ! Number of unsigned 2-B int. fields.
  INTEGER (KIND=4) :: gfi4_nfield     ! Number of 2-B int. fields.
  INTEGER (KIND=4) :: gfr4_nfield     ! Number of 4-B real fields.
  INTEGER (KIND=4) :: gfr8_nfield     ! Number of 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_names     ! Names of unsigned 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_names      ! Names of 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_names     ! Names of unsigned 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_names      ! Names of 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_names      ! Names of 4-B real field.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_names      ! Names of 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_units     ! Units for unsigned 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_units      ! Units for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_units     ! Units for unsigned 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_units      ! Units for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_units      ! Units for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_units      ! Units for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_title     ! Titles for unsigned 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_title      ! Titles for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_title     ! Titles for unsigned 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_title      ! Titles for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_title      ! Titles for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_title      ! Titles for 8-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi1u_ufdef     ! UFDs for unsigned 1-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2_ufdef      ! UFDs for 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi2u_ufdef     ! UFDs for unsigned 2-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfi4_ufdef      ! UFDs for 4-B int. fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr4_ufdef      ! UFDs for 4-B real fields.
  CHARACTER (LEN=256), &
    DIMENSION(100) :: gfr8_ufdef      ! UFDs for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi1u_sfactor   ! Scale factors for uns. 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2_sfactor    ! Scale factors for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2u_sfactor   ! Scale factors for uns. 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi4_sfactor    ! Scale factors for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr4_sfactor    ! Scale factors for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_sfactor    ! Scale factors for 8-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi1u_offset    ! Offsets for unsigned 1-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2_offset     ! Offsets for 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi2u_offset    ! Offsets for unsigned 2-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfi4_offset     ! Offsets for 4-B int. fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr4_offset     ! Offsets for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_offset     ! Offsets for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100,2) :: gfi1u_vrange  ! Valid ranges for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2) :: gfi2_vrange   ! Valid ranges for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100,2) :: gfi2u_vrange  ! Valid ranges for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100,2) :: gfi4_vrange   ! Valid ranges for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100,2) :: gfr4_vrange   ! Valid ranges for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100,2) :: gfr8_vrange   ! Valid ranges for 8-B real fields.
  INTEGER (KIND=1), &
    DIMENSION(100) :: gfi1u_mvalue    ! Missing values for uns. 1-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100) :: gfi2_mvalue     ! Missing values for 2-B int. fields.
  INTEGER (KIND=2), &
    DIMENSION(100) :: gfi2u_mvalue    ! Missing values for uns. 2-B int. fields.
  INTEGER (KIND=4), &
    DIMENSION(100) :: gfi4_mvalue     ! Missing values for 4-B int. fields.
  REAL (KIND=4), &
    DIMENSION(100) :: gfr4_mvalue     ! Missing values for 4-B real fields.
  REAL (KIND=8), &
    DIMENSION(100) :: gfr8_mvalue     ! Missing values for 8-B real fields.
 
!===============================================================================
! Missing and actual values for "key" field, and some other missing values.
!===============================================================================
  REAL (KIND=4) :: key_mvalue    ! Missing value for key field.
  REAL (KIND=4) :: key_fvalue    ! Actual value for key field.
  REAL (KIND=4) :: aplo3_mvalue  ! Missing value for a priori layer O3 field.
  REAL (KIND=4) :: leff_mvalue   ! Missing value for layer efficiencies field.
  REAL (KIND=4) :: res_mvalue    ! Missing value for residuals field.
 
!===============================================================================
! L2 geolocation fields.
!===============================================================================
  INTEGER (KIND=2), &
    DIMENSION(60) :: l2_gpqf  ! Ground pixel quality flags at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_lat   ! Latitude at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_lon   ! Longitude at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_raa   ! Relative azimuth angle at scene centers.
  REAL (KIND=4) :: l2_secday  ! Seconds of day for L2 line.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_saa   ! Solar azimuth angle at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_sza   ! Solar zenith angle at scene centers.
  REAL (KIND=4) :: l2_scalt   ! Spacecraft altitude for L2 line.
  REAL (KIND=4) :: l2_sclat   ! Spacecraft latitude for L2 line.
  REAL (KIND=4) :: l2_sclon   ! Spacecraft longitude for L2 line.
  INTEGER (KIND=2), &
    DIMENSION(60) :: l2_thgt  ! Terrain height at scene centers.
  REAL (KIND=8) :: l2_tline   ! Time (TAI93) for L2 line.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_vaa   ! Viewing azimuth angle at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_vza   ! Viewing zenith angle at scene centers.
  INTEGER (KIND=1), &
    DIMENSION(60) :: l2_xtqf  ! Cross-track quality flags at scene centers.
 
!===============================================================================
! L2 data fields.
!===============================================================================
  INTEGER (KIND=1), &
    DIMENSION(60) :: l2_aflg      ! Algorithm flags at scene centers.
  REAL (KIND=4), &
    DIMENSION(11,60) :: l2_aplo3  ! A priori layer O3 at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_cp        ! Cloud pressures at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_cao3      ! Column amounts O3 at scene centers.
  INTEGER (KIND=1) :: l2_icid     ! Instrument configuration IDs for line.
  REAL (KIND=4), &
    DIMENSION(11,60) :: l2_leff   ! Layer efficiencies at scene centers.
  INTEGER (KIND=1) :: l2_mqflg    ! Measurement quality flags for line.
  INTEGER (KIND=1) :: l2_nspc     ! Number of small pixel columns for line.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_o3bc      ! O3 below cloud at scene centers.
  INTEGER (KIND=2), &
    DIMENSION(60) :: l2_qflg      ! Quality flags at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_rcf       ! Radiative cloud fractions at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_r331      ! Reflectivities at 331 nm at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_r360      ! Reflectivities at 360 nm at scene centers.
  REAL (KIND=4), &
    DIMENSION(12,60) :: l2_res    ! Residuals at scene centers.
  INTEGER (KIND=2) :: l2_spc      ! Small pixel column for line.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_so2i      ! SO2 indices at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_s1o3      ! Step one O3 at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_s2o3      ! Step two O3 at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_tp        ! Terrain pressures at scene centers.
  REAL (KIND=4), &
    DIMENSION(60) :: l2_uvai      ! UV aerosol indices at scene centers.
  REAL (KIND=4), &
    DIMENSION(12) :: l2_wav       ! Wavelengths.
 
!===============================================================================
! L2G file quantities.
!===============================================================================
  CHARACTER (LEN=20) :: l2g_pgevers ! L2G PGE version.
  CHARACTER (LEN=256) :: l2g_fname  ! L2G file name.
  INTEGER (KIND=4) :: l2g_fileid    ! L2G file ID.
 
!===============================================================================
! L2G temporal quantities.
!===============================================================================
  INTEGER (KIND=4) :: l2g_month     ! Month of L2G file.
  INTEGER (KIND=4) :: l2g_day       ! Day of L2G file.
  INTEGER (KIND=4) :: l2g_year      ! Year of L2G file.
  INTEGER (KIND=4) :: l2g_dyofyr2   ! Day of year of L2G file from PCF.
  INTEGER (KIND=4) :: l2g_dyofyr    ! Day of year of L2G file calculated.
  REAL (KIND=8) :: l2g_TAI93at0z    ! TAI93 at 0z of L2G file.
  REAL (KIND=8) :: l2g_tstart       ! Time (TAI93) at start of L2G day.
  REAL (KIND=8) :: l2g_tend         ! Time (TAI93) at end of L2G day.
  CHARACTER (LEN=10) :: l2g_rbd     ! Range beginning date of L2G file.
  CHARACTER (LEN=16) :: l2g_rbt     ! Range beginning time of L2G file.
  CHARACTER (LEN=10) :: l2g_red     ! Range ending date of L2G file.
  CHARACTER (LEN=16) :: l2g_ret     ! Range ending time of L2G file.
 
!===============================================================================
! L2G grid quantities.
!===============================================================================
  INTEGER (KIND=8), &
    PARAMETER :: l2g_nlon = 1440    ! Number of longitudes in L2G grid.
  INTEGER (KIND=8), &
    PARAMETER :: l2g_nlat = 720     ! Number of latitudes in L2G grid.
  INTEGER (KIND=8), &
    PARAMETER :: l2g_nlayers = 7    ! Number of layers in L2G grid.
  INTEGER (KIND=8), &
    PARAMETER :: l2g_nwavel = 12    ! Number of wavelengths in L2G grid.
  INTEGER (KIND=8), &
    PARAMETER :: l2g_ncand = 15     ! Number of candidates in grid.
  INTEGER (KIND=4) :: l2g_ilon      ! Longitude counter for L2G grid.
  INTEGER (KIND=4) :: l2g_ilat      ! Latitude counter for L2G grid.
  INTEGER (KIND=4) :: l2g_icand     ! Candidate number for L2G grid.
  INTEGER (KIND=4) :: l2g_gridid    ! L2G grid ID.
 
!===============================================================================
! L2G statistical Metadata.
!===============================================================================
  INTEGER (KIND=4) :: l2g_nscon     ! Number of L2 scenes considered.
  INTEGER (KIND=4) :: l2g_nsacc     ! Number of L2 scenes accepted.
  INTEGER (KIND=4) :: l2g_nsdup     ! Number of L2 duplicate scenes accepted.
  INTEGER (KIND=4) :: l2g_nsrej     ! Number of L2 scenes rejected.
  INTEGER (KIND=4) :: l2g_npgc      ! Number of populated L2G grid cells.
  INTEGER (KIND=4) :: l2g_nmpgc     ! Number of multiply populated grid cells.
  INTEGER (KIND=4) :: l2g_ngc       ! Number of L2G grid cells.
  INTEGER (KIND=4) :: l2g_negc      ! Number of empty L2G grid cells.
  INTEGER (KIND=4) :: l2g_ncgcmin   ! Minimum number of candidates.
  INTEGER (KIND=4) :: l2g_ncgcmax   ! Maximum number of candidates.
 
!===============================================================================
! L2G 2-D geolocation fields.
!===============================================================================
  INTEGER (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat) :: l2g_ncs      ! Number of candidate scenes.
  REAL (KIND=4), &
    DIMENSION(l2g_nlon,l2g_nlat) :: l2g_pathl2d  ! Path length of best scene.
 
!===============================================================================
! L2G 3-D geolocation fields.
!===============================================================================
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_gpqf      ! Ground pixel quality flags of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_lat       ! Latitudes of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_line      ! Line numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_lon       ! Longitudes of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_orbit     ! Orbit numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_pathl3d   ! Path lengths of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_raa       ! Relative azimuth angles of good scenes.
  INTEGER (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_scene     ! Scene numbers of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_secday    ! Seconds of day of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_sza       ! Solar zenith angles of good scenes.
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_thgt      ! Terrain heights of good scenes.
  REAL (KIND=8), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_time      ! Observation times of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_vza       ! View zenith angles of good scenes.
  INTEGER (KIND=1), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_xtqf      ! Cross-track quality flags at scene centers.
 
!===============================================================================
! L2G 1-D data field.
!===============================================================================
  REAL (KIND=4), &
    DIMENSION(l2g_nwavel) :: l2g_wav  ! Wavelength.
 
!===============================================================================
! L2G 3-D data fields.
!===============================================================================
  INTEGER (KIND=1), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_aflg      ! Algorithm flags of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_cp        ! Cloud pressures of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_cao3      ! Column amount O3 of good scenes.
  INTEGER (KIND=1), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_icid      ! Instrument configuration IDs of good scenes.
  INTEGER (KIND=1), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_mqflg     ! Measurement quality flags of good scenes.
  INTEGER (KIND=1), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_nspc      ! Number of small pixel columns of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_o3bc      ! O3 below cloud of good scenes.
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_qflg      ! Quality flags of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_rcf       ! Radiative cloud fractions of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_r331      ! Reflectivities at 331 nm of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_r360      ! Reflectivities at 360 nm of good scenes.
  INTEGER (KIND=2), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_spc       ! Small pixel column of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_so2i      ! SO2 indices of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_s2o3      ! Step two O3 of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_tp        ! Terrain pressure of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:), &
    ALLOCATABLE :: l2g_uvai      ! UV aerosol indices of good scenes.
 
!===============================================================================
! L2G 4-D data fields.
!===============================================================================
  REAL (KIND=4), &
    DIMENSION(:,:,:,:), &
    ALLOCATABLE :: l2g_aplo3     ! A priori layer O3 of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:,:), &
    ALLOCATABLE :: l2g_leff      ! Layer efficiencies of good scenes.
  REAL (KIND=4), &
    DIMENSION(:,:,:,:), &
    ALLOCATABLE :: l2g_res       ! Residuals of good scenes.
 
!===============================================================================
! Miscellaneous.
!===============================================================================
  REAL (KIND=4) :: cfdtr         ! Conversion factor for degrees to radians.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat    ! Function call status.
  CHARACTER (LEN=256) :: stat_msg  ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OMTO3G.f90"        ! Code file reference.
  INTEGER (KIND=4), &
    EXTERNAL :: OMI_SMF_setmsg     ! OMI messaging function.
 
!===============================================================================
! Calculate conversion factor for degrees to radians.
!===============================================================================
  cfdtr = ACOS(-1.0)/180.0
 
!===============================================================================
! Read OML2G PCF.
!===============================================================================
  CALL OML2G_ReadPCF(l2_prodid, l2_nfile, l2_fname, &
                     l2g_pgevers, l2g_dyofyr2, l2g_fname)
 
!===============================================================================
! Find L2G day of year and other L2G temporal quantities.
!===============================================================================
  CALL OML2G_FindL2GDay(l2_nfile, l2_fname, l2_prodid, l2_ifirst, &
                        l2g_month, l2g_day, l2g_year, l2g_dyofyr, &
                        l2g_TAI93at0z, l2g_tstart, l2g_tend, &
                        l2g_rbd, l2g_rbt, l2g_red, l2g_ret)
 
!===============================================================================
! Test if l2g_dyofyr2 is not equal to l2g_dyofyr.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (l2g_dyofyr2 .NE. l2g_dyofyr) THEN
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
    WRITE(stat_msg, '(A)') &
     "Fatal error due to inconsistent l2g_dyofyr!"
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_dyofyr2 .NE. l2g_dyofyr)" test.
 
!===============================================================================
! Initialize some L2G statistical quantities.
!===============================================================================
  l2g_nscon = 0
  l2g_nsacc = 0
  l2g_nsdup = 0
  l2g_nsrej = 0
  l2g_npgc = 0
  l2g_nmpgc = 0
  l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
  l2g_pathl2d(1:l2g_nlon,1:l2g_nlat) = 1.0e+30
 
!===============================================================================
! Initialize L2 orbit numbers.
!===============================================================================
  l2_orbit(1:20) = -1
  l2_lrange(1:20,1) =  200000000
  l2_lrange(1:20,2) = -200000000
 
!===============================================================================
! Process L2G geolocation fields first:
!===============================================================================
 
!===============================================================================
! Allocate memory for L2G geolocation fields.
!===============================================================================
  ALLOCATE(l2g_gpqf(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_lat(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_line(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_lon(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_orbit(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_pathl3d(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_raa(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_scene(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_secday(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_sza(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_thgt(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_time(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_vza(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_xtqf(l2g_nlon,l2g_nlat,l2g_ncand))
 
!===============================================================================
! Loop through L2 files.
!===============================================================================
  File_Loop: DO l2_ifile = 1, l2_nfile
 
    l2_nlbad(l2_ifile) = 0
    l2_nlgood(l2_ifile) = 0
 
!===============================================================================
! Open L2 file and swath.
!===============================================================================
    CALL OML2G_OpenL2File(l2_fname(l2_ifile), l2_prodid, l2_fileid, l2_swathid)
 
!===============================================================================
! Read ECS Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2ECSMeta(l2_ifile, &
                             l2_orbit(l2_ifile), l2_ecd(l2_ifile), &
                             l2_ect(l2_ifile), l2_ecl(l2_ifile), &
                             l2_rbd(l2_ifile), l2_rbt(l2_ifile), &
                             l2_red(l2_ifile), l2_ret(l2_ifile), &
                             l2_orbper(l2_ifile), l2_qapmd(l2_ifile), &
                             l2_nm(l2_ifile), l2_nz(l2_ifile), &
                             l2_nspatz(l2_ifile), l2_nspecz(l2_ifile), &
                             l2_ed(l2_ifile), l2_saac(l2_ifile), &
                             l2_sm(l2_ifile), l2_se(l2_ifile), &
                             l2_mcp(1:6,l2_ifile), l2_et(1:6,l2_ifile), &
                             l2_cps(l2_ifile), l2_tps(l2_ifile), &
                             l2_ts(l2_ifile), l2_sis(l2_ifile), &
                             l2_apops(l2_ifile))
 
!===============================================================================
! Read swath-level Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2SwathMeta(l2_swathid, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel)
 
!===============================================================================
! Test sizes of l2_nlayers and l2_nwavel dimensions.
!===============================================================================
    IF (l2_ifile == 1) THEN
 
!===============================================================================
! Test if l2g_nlayers is greater than l2_nlayers.  If true, then end execution
! with fatal error.  (Want l2g_nlayers to be less than or equal to l2_nlayers.)
!===============================================================================
      IF (l2g_nlayers > l2_nlayers) THEN

!===============================================================================
! Close L2 swath and file.
!===============================================================================
        CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
        WRITE(stat_msg, '(A)') &
          "Fatal error due to l2g_nlayers being greater than l2_nlayers!"
        CALL OML2G_EndInFailure(stat_msg, code_ref)
 
      END IF
      ! End of "IF (l2g_nlayers > l2_nlayers)" test.
 
!===============================================================================
! Test if l2g_nwavel is not equal to l2_nwavel.  If true, then end execution
! with fatal error.
!===============================================================================
      IF (l2g_nwavel /= l2_nwavel) THEN

!===============================================================================
! Close L2 swath and file.
!===============================================================================
        CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
        WRITE(stat_msg, '(A)') &
          "Fatal error due to l2g_nwavel not being equal to l2_nwavel!"
        CALL OML2G_EndInFailure(stat_msg, code_ref)
 
      END IF
      ! End of "IF (l2g_nwavel /= l2_nwavel)" test.
 
    END IF
    ! End of "IF (l2_ifile == 1)" test .
 
!===============================================================================
! Test if number of L2 scenes in swath is not equal to 60.  If true, then end
! execution with fatal error.
!===============================================================================
    IF (l2_nscene /= 60) THEN

!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
      WRITE(stat_msg, '(A)') &
        "Fatal error due to l2_nscene not being equal to 60!"
      CALL OML2G_EndInFailure(stat_msg, code_ref)

    END IF
    ! End of "IF (l2_nscene /= 60)" test.
 
!===============================================================================
! Read swath-field Metadata from L2 file.
!===============================================================================
    IF (l2_ifile == 1) THEN
 
      CALL OML2G_ReadL2FieldInfo(l2_swathid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names)
 
      CALL OML2G_ReadL2FieldMeta(l2_swathid, &
                                 gfi1u_nfield, gfi1u_names, &
                                 gfi2_nfield, gfi2_names, &
                                 gfi2u_nfield, gfi2u_names, &
                                 gfi4_nfield, gfi4_names, &
                                 gfr4_nfield, gfr4_names, &
                                 gfr8_nfield, gfr8_names, &
                                 gfi1u_units, gfi1u_title, gfi1u_ufdef, &
                                 gfi1u_sfactor, gfi1u_offset, &
                                 gfi1u_vrange, gfi1u_mvalue, &
                                 gfi2_units, gfi2_title, gfi2_ufdef, &
                                 gfi2_sfactor, gfi2_offset, &
                                 gfi2_vrange, gfi2_mvalue, &
                                 gfi2u_units, gfi2u_title, gfi2u_ufdef, &
                                 gfi2u_sfactor, gfi2u_offset, &
                                 gfi2u_vrange, gfi2u_mvalue, &
                                 gfi4_units, gfi4_title, gfi4_ufdef, &
                                 gfi4_sfactor, gfi4_offset, &
                                 gfi4_vrange, gfi4_mvalue, &
                                 gfr4_units, gfr4_title, gfr4_ufdef, &
                                 gfr4_sfactor, gfr4_offset, &
                                 gfr4_vrange, gfr4_mvalue, &
                                 gfr8_units, gfr8_title, gfr8_ufdef, &
                                 gfr8_sfactor, gfr8_offset, &
                                 gfr8_vrange, gfr8_mvalue)
 
!===============================================================================
! Initialize L2G geolocation fields with missing values.
!===============================================================================
      l2g_gpqf(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2u_mvalue(1)
      l2g_lat(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_line(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(1)
      l2g_lon(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_orbit(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(1)
      l2g_pathl3d(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = -1.0 * gfr4_mvalue(1)
      l2g_raa(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_scene(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi4_mvalue(1)
      l2g_secday(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_sza(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_thgt(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2_mvalue(1)
      l2g_time(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr8_mvalue(1)
      l2g_vza(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
      l2g_xtqf(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi1u_mvalue(1)
 
!===============================================================================
! Initialize L2G wavelength field with missing values.
!===============================================================================
      l2g_wav(1:l2g_nwavel) = gfr4_mvalue(1)
 
!===============================================================================
! Obtain key missing value.
!===============================================================================
      key_mvalue = 0.0
      IF (TRIM(l2_prodid) == "OMTO3") THEN
 
        DO l2_ifield = 1, gfr4_nfield
 
          IF (TRIM(gfr4_names(l2_ifield)) == "ColumnAmountO3") THEN
            key_mvalue = gfr4_mvalue(l2_ifield)
          END IF
 
        END DO
        ! End of "DO l2_ifield = 1, gfr4_nfield" loop.
 
      END IF
      ! End of "IF (TRIM(l2_prodid) == "OMTO3")" test.
 
!===============================================================================
! Test if key missing value has not been obtained.  If true, then end OML2G
! execution in error.
!===============================================================================
      IF (key_mvalue > -0.001) THEN

!===============================================================================
! Close L2 swath and file.
!===============================================================================
        CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! End OML2G execution in error.
!===============================================================================
        WRITE(stat_msg, '(A)') &
	  "Fatal error:  key missing value was not obtained!"
        CALL OML2G_EndInFailure(stat_msg, code_ref)
 
      END IF
      ! End of "IF (key_mvalue > -0.001)" test.

    END IF
    ! End of "IF (l2_ifile == 1)" test.
 
!===============================================================================
! Test if L2 file contains zoom data.  If true, then cycle to next L2 file.
!===============================================================================
    IF (l2_nz(l2_ifile)     /= 0 .OR. &
        l2_nspatz(l2_ifile) /= 0 .OR. &
        l2_nspecz(l2_ifile) /= 0 ) THEN
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! Cycle to next L2 file.
!===============================================================================
      CYCLE File_Loop
 
    END IF
    ! End of "IF (l2_nz(l2_ifile) /= 0 .OR." test.
 
!===============================================================================
! Loop through lines of data in L2 swath.
!===============================================================================
    Line_Loop: DO l2_iline = 1, l2_nline
 
!===============================================================================
! Read one line of geolocation from L2 file.
!===============================================================================
      CALL OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Read one line of data-field data from L2 file.
!===============================================================================
      CALL OML2G_ReadL2DatLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel, &
                               l2_aflg, l2_aplo3, l2_cp, l2_cao3, &
                               l2_icid, l2_leff, l2_mqflg, l2_nspc, &
                               l2_o3bc, l2_qflg, l2_rcf, l2_r331, &
                               l2_r360, l2_res, l2_spc, l2_so2i, &
                               l2_s1o3, l2_s2o3, l2_tp, l2_uvai, l2_wav)
 
!===============================================================================
! Copy L2 wavelengths to L2G wavelengths only once per L2G run.  Hopefully
! these will not change during L2G day.
!===============================================================================
      IF (l2_ifile == l2_ifirst .AND. l2_iline == 1) THEN
        l2g_wav(1:l2g_nwavel) = &
              0.01d0 * DNINT(100.0d0 * l2_wav(1:l2g_nwavel))
      END IF
 
!===============================================================================
! Test if time of line of data lies outside L2G day under consideration.  If
! true, then cycle to next line of data.
!===============================================================================
      IF (l2_tline < l2g_tstart .OR. l2_tline >= l2g_tend) CYCLE Line_Loop
 
!===============================================================================
! Find minimum solar zenith angle in line of data.
!===============================================================================
      l2_minsza = MINVAL(l2_sza)
 
!===============================================================================
! Test if minimum solar zenith angle in line of data is less than 0.0 degrees.
! If true, then cycle to next line of data.
!===============================================================================
      IF (l2_minsza < 0.0) THEN
 
!===============================================================================
! Increment number of "geolocation bad" lines by one, because minimum solar
! zenith angle in line is less than 0.0 degrees.
!===============================================================================
        l2_nlbad(l2_ifile) = l2_nlbad(l2_ifile) + 1
 
!===============================================================================
! Note bad geolocation for line in PGE log file.
!===============================================================================
        WRITE(stat_msg, 95) l2_iline, l2_ifile
95      FORMAT("Bad geolocation for line",I5, " of file",I3,"!")
        call_stat = OMI_SMF_setmsg(OMI_W_GENERAL, stat_msg, code_ref, 1)
 
!===============================================================================
! Cycle to next line.
!===============================================================================
        CYCLE Line_Loop
 
      END IF
      ! End of "IF (l2_minsza < 0.0)" test.
 
!===============================================================================
! Increment number of "good" lines by one, because time of line of data lies
! within L2G day under consideration.
!===============================================================================
      l2_nlgood(l2_ifile) = l2_nlgood(l2_ifile) + 1
 
!===============================================================================
! Loop through scenes in line.
!===============================================================================
      Scene_Loop: DO l2_iscene = 1, l2_nscene
 
!===============================================================================
! Increment number of scenes considered by one.
!===============================================================================
        l2g_nscon = l2g_nscon + 1
 
!===============================================================================
! Test if solar zenith angle for scene is greater than 88.0 degrees.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (l2_sza(l2_iscene) > 88.0) THEN
 
!===============================================================================
! Increment number of scenes rejected by one.
!===============================================================================
          l2g_nsrej = l2g_nsrej + 1
 
!===============================================================================
! Cycle to next scene.
!===============================================================================
          CYCLE Scene_Loop
 
        END IF
        ! End of "IF (l2_sza(l2_iscene) > 88.0)" test.
 
!===============================================================================
! Test if value for key field for scene is equal to key missing value.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (TRIM(l2_prodid) == "OMTO3") key_fvalue = l2_cao3(l2_iscene)
        l2_dummy = ABS((key_fvalue - key_mvalue) / key_mvalue)
 
        IF (l2_dummy < 0.001) THEN
 
!===============================================================================
! Increment number of scenes rejected by one.
!===============================================================================
          l2g_nsrej = l2g_nsrej + 1
 
!===============================================================================
! Cycle to next scene.
!===============================================================================
          CYCLE Scene_Loop
 
        END IF
        ! End of "IF (l2_dummy < 0.001)" test.
 
!===============================================================================
! Increment number of scenes accepted by one.
!===============================================================================
        l2g_nsacc = l2g_nsacc + 1
 
!===============================================================================
! Test L2 orbital line range, and, if necessary, adjust.
!===============================================================================
        IF (l2_iline < l2_lrange(l2_ifile,1)) l2_lrange(l2_ifile,1) = l2_iline
        IF (l2_iline > l2_lrange(l2_ifile,2)) l2_lrange(l2_ifile,2) = l2_iline
 
!===============================================================================
! Calculate indices of L2G grid cell that contains center of candidate scene.
! Dimensions of 0.25-degree by 0.25-degree L2G grid are 1440 by 720.  L2G grid
! cell with (l2g_ilon=1, l2g_ilat=1) is centered at (lon=-179.875, lat=-89.875).
! L2G grid cell with (l2g_ilon=1440, l2g_ilat=720) is centered at (lon=179.875,
! lat=89.875).
!===============================================================================
        IF (l2_lon(l2_iscene) < 0.0) l2g_ilon = &
                       720 + INT(4.0*l2_lon(l2_iscene) + 0.000001)
        IF (l2_lon(l2_iscene) >= 0.0) l2g_ilon = &
                       721 + INT(4.0*l2_lon(l2_iscene) - 0.000001)
        IF (l2_lat(l2_iscene) < 0.0) l2g_ilat = &
                       360 + INT(4.0*l2_lat(l2_iscene) + 0.000001)
        IF (l2_lat(l2_iscene) >= 0.0) l2g_ilat = &
                       361 + INT(4.0*l2_lat(l2_iscene) - 0.000001)
 
!===============================================================================
! Increment number of candidate scenes for L2G grid cell by one.
!===============================================================================
        l2g_ncs(l2g_ilon,l2g_ilat) = l2g_ncs(l2g_ilon,l2g_ilat) + 1
 
!===============================================================================
! Test if candidate scene is second in L2G grid cell.  If true, then increment
! number of "multiply" populated L2G grid cells by one.
!===============================================================================
        l2g_icand = l2g_ncs(l2g_ilon,l2g_ilat)
        IF (l2g_icand == 2) l2g_nmpgc = l2g_nmpgc + 1
 
!===============================================================================
! Calculate path length for candidate scene.
!===============================================================================
        l2_pathl = 1.0/COS(cfdtr*l2_sza(l2_iscene)) + &
                   1.0/COS(cfdtr*l2_vza(l2_iscene))
 
!===============================================================================
! Copy quantities for L2 scene to corresponding L2G grid cell.
!===============================================================================
        l2g_gpqf(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_gpqf(l2_iscene)
        l2g_lat(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_lat(l2_iscene))
        l2g_line(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_iline
        l2g_lon(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_lon(l2_iscene))
        l2g_orbit(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_orbit(l2_ifile)
        l2g_pathl3d(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_pathl)
        l2g_raa(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_raa(l2_iscene))
        l2g_scene(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_iscene
        l2g_secday(l2g_ilon,l2g_ilat,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_secday)
        l2g_sza(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_sza(l2_iscene))
        l2g_thgt(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_thgt(l2_iscene)
        l2g_time(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_tline
        l2g_vza(l2g_ilon,l2g_ilat,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_vza(l2_iscene))
        l2g_xtqf(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_xtqf(l2_iscene)
 
!===============================================================================
! Test if path length of scene in L2G grid is equal to missing value.  If true,
! then increment number of populated L2G grid cells by one.
!===============================================================================
        IF (l2g_pathl2d(l2g_ilon,l2g_ilat) > 1.0e+29) THEN
 
!===============================================================================
! Increment number of populated L2G grid cells by one.
!===============================================================================
          l2g_npgc = l2g_npgc + 1
 
!===============================================================================
! Update path length for L2G grid cell with that of candidate scene.
!===============================================================================
          l2g_pathl2d(l2g_ilon,l2g_ilat) = l2_pathl
 
!===============================================================================
! "ELSE" means current smallest path length is not equal to missing value.
!===============================================================================
        ELSE
 
!===============================================================================
! Increment number of "duplicate" scenes by one.
!===============================================================================
          l2g_nsdup = l2g_nsdup + 1
 
!===============================================================================
! Test if path length of candidate scene is less than that of current best path
! length for L2G grid cell.  If true, then update path length for L2G grid cell
! with that of candidate scene.
!===============================================================================
          IF (l2_pathl < l2g_pathl2d(l2g_ilon,l2g_ilat)) &
                         l2g_pathl2d(l2g_ilon,l2g_ilat) = l2_pathl
 
        END IF
        ! End of "IF (l2g_pathl2d(l2g_ilon,l2g_ilat) > 1.0e+29)" test.
 
      END DO Scene_Loop
      ! End of "DO l2_iscene = 1, l2_nscene" loop.
 
    END DO Line_Loop
    ! End of "DO l2_iline = 1, l2_nline" loop.
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
    CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
  END DO File_Loop
  ! End of "DO l2_ifile = 1, l2_nfile" loop.
 
!===============================================================================
! Open L2G file, and create grid within file.
!===============================================================================
  CALL OML2G_OpenL2GFile(l2_prodid, l2g_fname, l2g_nlon, l2g_nlat, &
                         l2g_nlayers, l2g_nwavel, l2g_ncand, &
                         l2g_fileid, l2g_gridid)
 
!===============================================================================
! Write file-level Metadata to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GFileMeta(l2g_fileid, &
                              l2_nfile, l2_orbit, l2_lrange, &
                              l2_nlbad, l2_orbper, l2g_month, &
                              l2g_day, l2g_year, l2g_dyofyr, &
                              l2g_TAI93at0z, l2g_pgevers, &
                              l2g_tstart, l2g_tend, l2_qapmd)
 
!===============================================================================
! Write grid-level Metadata to L2G file.
!===============================================================================
  l2g_ngc = l2g_nlon * l2g_nlat
  l2g_negc = l2g_ngc - l2g_npgc
  l2g_ncgcmin = MINVAL(l2g_ncs)
  l2g_ncgcmax = MAXVAL(l2g_ncs)
  CALL OML2G_WriteL2GGridMeta(l2g_gridid, &
                              l2g_nlon, l2g_nlat, l2g_ngc, &
                              l2g_negc, l2g_npgc, l2g_nmpgc, &
                              l2g_ncgcmin, l2g_ncgcmax, l2g_nscon, &
                              l2g_nsacc, l2g_nsdup, l2g_nsrej)
 
!===============================================================================
! Test if l2g_ncgcmax is greater than l2g_ncand.  If true, then end execution
! with fatal error.
!===============================================================================
  IF (l2g_ncgcmax > l2g_ncand) THEN

!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML2G_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! End execution with fatal error.
!===============================================================================
    WRITE(stat_msg, '(A)') &
      "Fatal error due to l2g_ncgcmax being greater than l2g_ncand!"
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (l2g_ncgcmax > l2g_ncand)" test.
 
!===============================================================================
! Write geolocation fields to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GGeoGrid(l2g_gridid, &
                             l2g_nlon, l2g_nlat, l2g_ncand, &
                             gfi1u_mvalue(1), gfi2_mvalue(1), gfi2u_mvalue(1), &
                             gfi4_mvalue(1), gfr4_mvalue(1), gfr8_mvalue(1), &
                             l2g_gpqf, l2g_lat, l2g_line, l2g_lon, &
                             l2g_ncs, l2g_orbit, l2g_pathl3d, l2g_raa, &
                             l2g_scene, l2g_secday, l2g_sza, l2g_thgt, &
                             l2g_time, l2g_vza, l2g_xtqf)
 
!===============================================================================
! Deallocate memory used for geolocation fields.
!===============================================================================
  DEALLOCATE(l2g_gpqf)
  DEALLOCATE(l2g_lat)
  DEALLOCATE(l2g_line)
  DEALLOCATE(l2g_lon)
  DEALLOCATE(l2g_orbit)
  DEALLOCATE(l2g_pathl3d)
  DEALLOCATE(l2g_raa)
  DEALLOCATE(l2g_scene)
  DEALLOCATE(l2g_secday)
  DEALLOCATE(l2g_sza)
  DEALLOCATE(l2g_thgt)
  DEALLOCATE(l2g_time)
  DEALLOCATE(l2g_vza)
  DEALLOCATE(l2g_xtqf)
 
!===============================================================================
! Process 3-D L2G data fields:
!===============================================================================
 
!===============================================================================
! Allocate memory for 3-D L2G data fields.
!===============================================================================
  ALLOCATE(l2g_aflg(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_cp(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_cao3(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_icid(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_mqflg(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_nspc(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_o3bc(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_qflg(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_rcf(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_r331(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_r360(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_spc(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_so2i(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_s2o3(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_tp(l2g_nlon,l2g_nlat,l2g_ncand))
  ALLOCATE(l2g_uvai(l2g_nlon,l2g_nlat,l2g_ncand))
 
!===============================================================================
! Initialize 3-D data fields with missing values.
!===============================================================================
  l2g_aflg(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi1u_mvalue(1)
  l2g_cp(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_cao3(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_icid(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi1u_mvalue(1)
  l2g_mqflg(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi1u_mvalue(1)
  l2g_nspc(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi1u_mvalue(1)
  l2g_o3bc(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_qflg(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2u_mvalue(1)
  l2g_rcf(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_r331(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_r360(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_spc(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfi2_mvalue(1)
  l2g_so2i(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_s2o3(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_tp(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
  l2g_uvai(1:l2g_nlon,1:l2g_nlat,1:l2g_ncand) = gfr4_mvalue(1)
 
!===============================================================================
! Initialize 2-D number of candidate scenes field to zero.
!===============================================================================
  l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
 
!===============================================================================
! Loop through L2 files.
!===============================================================================
  File_Loop2: DO l2_ifile = 1, l2_nfile
 
!===============================================================================
! Open L2 file and swath.
!===============================================================================
    CALL OML2G_OpenL2File(l2_fname(l2_ifile), l2_prodid, l2_fileid, l2_swathid)
 
!===============================================================================
! Read swath-level Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2SwathMeta(l2_swathid, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel)
 
!===============================================================================
! Test if L2 file contains zoom data.  If true, then cycle to next L2 file.
!===============================================================================
    IF (l2_nz(l2_ifile)     /= 0 .OR. &
        l2_nspatz(l2_ifile) /= 0 .OR. &
        l2_nspecz(l2_ifile) /= 0 ) THEN
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! Cycle to next L2 file.
!===============================================================================
      CYCLE File_Loop2
 
    END IF
    ! End of "IF (l2_nz(l2_ifile) /= 0 .OR." test.
 
!===============================================================================
! Loop through lines of data in L2 swath.
!===============================================================================
    Line_Loop2: DO l2_iline = 1, l2_nline
 
!===============================================================================
! Read one line of geolocation from L2 file.
!===============================================================================
      CALL OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Read one line of data-field data from L2 file.
!===============================================================================
      CALL OML2G_ReadL2DatLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel, &
                               l2_aflg, l2_aplo3, l2_cp, l2_cao3, &
                               l2_icid, l2_leff, l2_mqflg, l2_nspc, &
                               l2_o3bc, l2_qflg, l2_rcf, l2_r331, &
                               l2_r360, l2_res, l2_spc, l2_so2i, &
                               l2_s1o3, l2_s2o3, l2_tp, l2_uvai, l2_wav)
 
!===============================================================================
! Test if time of line of data lies outside L2G day under consideration.  If
! true, then cycle to next line of data.
!===============================================================================
      IF (l2_tline < l2g_tstart .OR. l2_tline >= l2g_tend) CYCLE Line_Loop2
 
!===============================================================================
! Find minimum solar zenith angle in line of data.
!===============================================================================
      l2_minsza = MINVAL(l2_sza)
 
!===============================================================================
! Test if minimum solar zenith angle in line of data is less than 0.0 degrees.
! If true, then cycle to next line of data.
!===============================================================================
      IF (l2_minsza < 0.0) CYCLE Line_Loop2
 
!===============================================================================
! Loop through scenes in line.
!===============================================================================
      Scene_Loop2: DO l2_iscene = 1, l2_nscene
 
!===============================================================================
! Test if solar zenith angle for scene is greater than 88.0 degrees.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (l2_sza(l2_iscene) > 88.0) CYCLE Scene_Loop2
 
!===============================================================================
! Test if value for key field for scene is equal to key missing value.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (TRIM(l2_prodid) == "OMTO3") key_fvalue = l2_cao3(l2_iscene)
        l2_dummy = ABS((key_fvalue - key_mvalue) / key_mvalue)
        IF (l2_dummy < 0.001) CYCLE Scene_Loop2
 
!===============================================================================
! Calculate indices of L2G grid cell that contains center of candidate scene.
! Dimensions of 0.25-degree by 0.25-degree L2G grid are 1440 by 720.  L2G grid
! cell with (l2g_ilon=1, l2g_ilat=1) is centered at (lon=-179.875, lat=-89.875).
! L2G grid cell with (l2g_ilon=1440, l2g_ilat=720) is centered at (lon=179.875,
! lat=89.875).
!===============================================================================
        IF (l2_lon(l2_iscene) < 0.0) l2g_ilon = &
                       720 + INT(4.0*l2_lon(l2_iscene) + 0.000001)
        IF (l2_lon(l2_iscene) >= 0.0) l2g_ilon = &
                       721 + INT(4.0*l2_lon(l2_iscene) - 0.000001)
        IF (l2_lat(l2_iscene) < 0.0) l2g_ilat = &
                       360 + INT(4.0*l2_lat(l2_iscene) + 0.000001)
        IF (l2_lat(l2_iscene) >= 0.0) l2g_ilat = &
                       361 + INT(4.0*l2_lat(l2_iscene) - 0.000001)
 
!===============================================================================
! Increment number of candidate scenes for L2G grid cell by one.
!===============================================================================
        l2g_ncs(l2g_ilon,l2g_ilat) = l2g_ncs(l2g_ilon,l2g_ilat) + 1
        l2g_icand = l2g_ncs(l2g_ilon,l2g_ilat)
 
!===============================================================================
! Copy quantities for L2 scene to corresponding L2G grid cell.
!===============================================================================
        l2g_aflg(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_aflg(l2_iscene)
        l2g_cp(l2g_ilon,l2g_ilat,l2g_icand) = &
              0.1d0 * DNINT(10.0d0 * l2_cp(l2_iscene))
        l2g_cao3(l2g_ilon,l2g_ilat,l2g_icand) = &
              0.1d0 * DNINT(10.0d0 * l2_cao3(l2_iscene))
        l2g_icid(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_icid
        l2g_mqflg(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_mqflg
        l2g_nspc(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_nspc
        l2g_o3bc(l2g_ilon,l2g_ilat,l2g_icand) = &
              0.1d0 * DNINT(10.0d0 * l2_o3bc(l2_iscene))
        l2g_qflg(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_qflg(l2_iscene)
        l2g_rcf(l2g_ilon,l2g_ilat,l2g_icand) = &
        0.0001d0 * DNINT(10000.0d0 * l2_rcf(l2_iscene))
        l2g_r331(l2g_ilon,l2g_ilat,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_r331(l2_iscene))
        l2g_r360(l2g_ilon,l2g_ilat,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_r360(l2_iscene))
        l2g_spc(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_spc
        l2g_s2o3(l2g_ilon,l2g_ilat,l2g_icand) = &
              0.1d0 * DNINT(10.0d0 * l2_s2o3(l2_iscene))
        l2g_tp(l2g_ilon,l2g_ilat,l2g_icand) = &
              0.1d0 * DNINT(10.0d0 * l2_tp(l2_iscene))
 
!===============================================================================
! Copy SO2 and UV aerosol indices for L2 scene to corresponding L2G grid cell,
! but be wary of missing values, which can occur for these indices, even if key
! field is good.
!===============================================================================
        IF (l2_so2i(l2_iscene) .LT. -1.0E+30) THEN
          l2g_so2i(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_so2i(l2_iscene)
        ELSE
          l2g_so2i(l2g_ilon,l2g_ilat,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_so2i(l2_iscene))
        END IF
 
        IF (l2_uvai(l2_iscene) .LT. -1.0E+30) THEN
          l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand) = &
                                     l2_uvai(l2_iscene)
        ELSE
          l2g_uvai(l2g_ilon,l2g_ilat,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_uvai(l2_iscene))
        END IF
 
      END DO Scene_Loop2
      ! End of "DO l2_iscene = 1, l2_nscene" loop.
 
    END DO Line_Loop2
    ! End of "DO l2_iline = 1, l2_nline" loop.
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
    CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
  END DO File_Loop2
  ! End of "DO l2_ifile = 1, l2_nfile" loop.
 
!===============================================================================
! Write 1-D and 3-D data fields to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GDatGrid(l2g_gridid, &
                             l2g_nlon, l2g_nlat, l2g_nwavel, l2g_ncand, &
                             gfi1u_mvalue(1), gfi2_mvalue(1), gfi2u_mvalue(1), &
                             gfi4_mvalue(1), gfr4_mvalue(1), gfr8_mvalue(1), &
                             l2g_aflg, l2g_cp, l2g_cao3, l2g_icid, &
                             l2g_mqflg, l2g_nspc, l2g_o3bc, l2g_qflg, &
                             l2g_rcf, l2g_r331, l2g_r360, l2g_spc, &
                             l2g_so2i, l2g_s2o3, l2g_tp, l2g_uvai, l2g_wav)
 
!===============================================================================
! Deallocate memory used for 3-D data fields.
!===============================================================================
  DEALLOCATE(l2g_aflg)
  DEALLOCATE(l2g_cp)
  DEALLOCATE(l2g_cao3)
  DEALLOCATE(l2g_icid)
  DEALLOCATE(l2g_mqflg)
  DEALLOCATE(l2g_nspc)
  DEALLOCATE(l2g_o3bc)
  DEALLOCATE(l2g_qflg)
  DEALLOCATE(l2g_rcf)
  DEALLOCATE(l2g_r331)
  DEALLOCATE(l2g_r360)
  DEALLOCATE(l2g_spc)
  DEALLOCATE(l2g_so2i)
  DEALLOCATE(l2g_s2o3)
  DEALLOCATE(l2g_tp)
  DEALLOCATE(l2g_uvai)
 
!===============================================================================
! Process 4-D a priori layer O3 field separately from other fields:
!===============================================================================
 
!===============================================================================
! Allocate memory for 4-D a priori layer O3 field.
!===============================================================================
  ALLOCATE(l2g_aplo3(l2g_nlon,l2g_nlat,l2g_nlayers,l2g_ncand))
 
!===============================================================================
! Obtain missing value for 4-D a priori layer O3 field.
!===============================================================================
  aplo3_mvalue = 0.0
  IF (TRIM(l2_prodid) == "OMTO3") THEN

    DO l2_ifield = 1, gfr4_nfield
 
      IF (TRIM(gfr4_names(l2_ifield)) == "APrioriLayerO3") THEN
        aplo3_mvalue = gfr4_mvalue(l2_ifield)
      END IF
 
    END DO
    ! End of "DO l2_ifield = 1, gfr4_nfield" loop.
 
  END IF
  ! End of "IF (TRIM(l2_prodid) == "OMTO3")" test.
 
!===============================================================================
! Test if a priori layer O3 missing value has not been obtained.  If true, then
! end OML2G execution in error.
!===============================================================================
  IF (aplo3_mvalue > -0.001) THEN

!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML2G_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! End OML2G execution in error.
!===============================================================================
    WRITE(stat_msg, '(A)') &
      "Fatal error:  a priori layer O3 missing value was not obtained!"
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (aplo3_mvalue > -0.001)" test.
 
!===============================================================================
! Initialize 4-D a priori layer O3 field with missing value.
!===============================================================================
  l2g_aplo3(1:l2g_nlon,1:l2g_nlat,1:l2g_nlayers,1:l2g_ncand) = aplo3_mvalue
 
!===============================================================================
! Initialize 2-D number of candidate scenes field to zero.
!===============================================================================
  l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
 
!===============================================================================
! Loop through L2 files.
!===============================================================================
  File_Loop3: DO l2_ifile = 1, l2_nfile
 
!===============================================================================
! Open L2 file and swath.
!===============================================================================
    CALL OML2G_OpenL2File(l2_fname(l2_ifile), l2_prodid, l2_fileid, l2_swathid)
 
!===============================================================================
! Read swath-level Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2SwathMeta(l2_swathid, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel)
 
!===============================================================================
! Test if L2 file contains zoom data.  If true, then cycle to next L2 file.
!===============================================================================
    IF (l2_nz(l2_ifile)     /= 0 .OR. &
        l2_nspatz(l2_ifile) /= 0 .OR. &
        l2_nspecz(l2_ifile) /= 0 ) THEN
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! Cycle to next L2 file.
!===============================================================================
      CYCLE File_Loop3
 
    END IF
    ! End of "IF (l2_nz(l2_ifile) /= 0 .OR." test.
 
!===============================================================================
! Loop through lines of data in L2 swath.
!===============================================================================
    Line_Loop3: DO l2_iline = 1, l2_nline
 
!===============================================================================
! Read one line of geolocation from L2 file.
!===============================================================================
      CALL OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Read one line of data-field data from L2 file.
!===============================================================================
      CALL OML2G_ReadL2DatLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel, &
                               l2_aflg, l2_aplo3, l2_cp, l2_cao3, &
                               l2_icid, l2_leff, l2_mqflg, l2_nspc, &
                               l2_o3bc, l2_qflg, l2_rcf, l2_r331, &
                               l2_r360, l2_res, l2_spc, l2_so2i, &
                               l2_s1o3, l2_s2o3, l2_tp, l2_uvai, l2_wav)
 
!===============================================================================
! Test if time of line of data lies outside L2G day under consideration.  If
! true, then cycle to next line of data.
!===============================================================================
      IF (l2_tline < l2g_tstart .OR. l2_tline >= l2g_tend) CYCLE Line_Loop3
 
!===============================================================================
! Find minimum solar zenith angle in line of data.
!===============================================================================
      l2_minsza = MINVAL(l2_sza)
 
!===============================================================================
! Test if minimum solar zenith angle in line of data is less than 0.0 degrees.
! If true, then cycle to next line of data.
!===============================================================================
      IF (l2_minsza < 0.0) CYCLE Line_Loop3
 
!===============================================================================
! Loop through scenes in line.
!===============================================================================
      Scene_Loop3: DO l2_iscene = 1, l2_nscene
 
!===============================================================================
! Test if solar zenith angle for scene is greater than 88.0 degrees.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (l2_sza(l2_iscene) > 88.0) CYCLE Scene_Loop3
 
!===============================================================================
! Test if value for key field for scene is equal to key missing value.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (TRIM(l2_prodid) == "OMTO3") key_fvalue = l2_cao3(l2_iscene)
        l2_dummy = ABS((key_fvalue - key_mvalue) / key_mvalue)
        IF (l2_dummy < 0.001) CYCLE Scene_Loop3
 
!===============================================================================
! Calculate indices of L2G grid cell that contains center of candidate scene.
! Dimensions of 0.25-degree by 0.25-degree L2G grid are 1440 by 720.  L2G grid
! cell with (l2g_ilon=1, l2g_ilat=1) is centered at (lon=-179.875, lat=-89.875).
! L2G grid cell with (l2g_ilon=1440, l2g_ilat=720) is centered at (lon=179.875,
! lat=89.875).
!===============================================================================
        IF (l2_lon(l2_iscene) < 0.0) l2g_ilon = &
                       720 + INT(4.0*l2_lon(l2_iscene) + 0.000001)
        IF (l2_lon(l2_iscene) >= 0.0) l2g_ilon = &
                       721 + INT(4.0*l2_lon(l2_iscene) - 0.000001)
        IF (l2_lat(l2_iscene) < 0.0) l2g_ilat = &
                       360 + INT(4.0*l2_lat(l2_iscene) + 0.000001)
        IF (l2_lat(l2_iscene) >= 0.0) l2g_ilat = &
                       361 + INT(4.0*l2_lat(l2_iscene) - 0.000001)
 
!===============================================================================
! Increment number of candidate scenes for L2G grid cell by one.
!===============================================================================
        l2g_ncs(l2g_ilon,l2g_ilat) = l2g_ncs(l2g_ilon,l2g_ilat) + 1
        l2g_icand = l2g_ncs(l2g_ilon,l2g_ilat)
 
!===============================================================================
! Copy a priori layer O3 for L2 scene to corresponding L2G grid cell.
!===============================================================================
        l2g_aplo3(l2g_ilon,l2g_ilat,1:l2g_nlayers,l2g_icand) = &
            0.01d0 * DNINT(100.0d0 * l2_aplo3(1:l2g_nlayers,l2_iscene))
 
      END DO Scene_Loop3
      ! End of "DO l2_iscene = 1, l2_nscene" loop.
 
    END DO Line_Loop3
    ! End of "DO l2_iline = 1, l2_nline" loop.
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
    CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
  END DO File_Loop3
  ! End of "DO l2_ifile = 1, l2_nfile" loop.
 
!===============================================================================
! Write 4-D a priori layer O3 field to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GAplGrid(l2g_gridid, &
                             l2g_nlon, l2g_nlat, &
                             l2g_nlayers, l2g_ncand, &
                             aplo3_mvalue, l2g_aplo3)
 
!===============================================================================
! Deallocate memory used for 4-D a priori layer O3 field.
!===============================================================================
  DEALLOCATE(l2g_aplo3)
 
!===============================================================================
! Process 4-D layer efficiencies field separately from other fields:
!===============================================================================
 
!===============================================================================
! Allocate memory for 4-D layer efficiencies field.
!===============================================================================
  ALLOCATE(l2g_leff(l2g_nlon,l2g_nlat,l2g_nlayers,l2g_ncand))
 
!===============================================================================
! Obtain missing value for 4-D layer efficiencies field.
!===============================================================================
  leff_mvalue = 0.0
  IF (TRIM(l2_prodid) == "OMTO3") THEN

    DO l2_ifield = 1, gfr4_nfield
 
      IF (TRIM(gfr4_names(l2_ifield)) == "LayerEfficiency") THEN
        leff_mvalue = gfr4_mvalue(l2_ifield)
      END IF
 
    END DO
    ! End of "DO l2_ifield = 1, gfr4_nfield" loop.
 
  END IF
  ! End of "IF (TRIM(l2_prodid) == "OMTO3")" test.
 
!===============================================================================
! Test if layer efficiencies missing value has not been obtained.  If true,
! then end OML2G execution in error.
!===============================================================================
  IF (leff_mvalue > -0.001) THEN

!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML2G_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! End OML2G execution in error.
!===============================================================================
    WRITE(stat_msg, '(A)') &
      "Fatal error:  layer efficiencies missing value was not obtained!"
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (leff_mvalue > -0.001)" test.
 
!===============================================================================
! Initialize 4-D layer efficiencies field with missing value.
!===============================================================================
  l2g_leff(1:l2g_nlon,1:l2g_nlat,1:l2g_nlayers,1:l2g_ncand) = leff_mvalue
 
!===============================================================================
! Initialize 2-D number of candidate scenes field to zero.
!===============================================================================
  l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
 
!===============================================================================
! Loop through L2 files.
!===============================================================================
  File_Loop4: DO l2_ifile = 1, l2_nfile
 
!===============================================================================
! Open L2 file and swath.
!===============================================================================
    CALL OML2G_OpenL2File(l2_fname(l2_ifile), l2_prodid, l2_fileid, l2_swathid)
 
!===============================================================================
! Read swath-level Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2SwathMeta(l2_swathid, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel)
 
!===============================================================================
! Test if L2 file contains zoom data.  If true, then cycle to next L2 file.
!===============================================================================
    IF (l2_nz(l2_ifile)     /= 0 .OR. &
        l2_nspatz(l2_ifile) /= 0 .OR. &
        l2_nspecz(l2_ifile) /= 0 ) THEN
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! Cycle to next L2 file.
!===============================================================================
      CYCLE File_Loop4
 
    END IF
    ! End of "IF (l2_nz(l2_ifile) /= 0 .OR." test.
 
!===============================================================================
! Loop through lines of data in L2 swath.
!===============================================================================
    Line_Loop4: DO l2_iline = 1, l2_nline
 
!===============================================================================
! Read one line of geolocation from L2 file.
!===============================================================================
      CALL OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Read one line of data-field data from L2 file.
!===============================================================================
      CALL OML2G_ReadL2DatLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel, &
                               l2_aflg, l2_aplo3, l2_cp, l2_cao3, &
                               l2_icid, l2_leff, l2_mqflg, l2_nspc, &
                               l2_o3bc, l2_qflg, l2_rcf, l2_r331, &
                               l2_r360, l2_res, l2_spc, l2_so2i, &
                               l2_s1o3, l2_s2o3, l2_tp, l2_uvai, l2_wav)
 
!===============================================================================
! Test if time of line of data lies outside L2G day under consideration.  If
! true, then cycle to next line of data.
!===============================================================================
      IF (l2_tline < l2g_tstart .OR. l2_tline >= l2g_tend) CYCLE Line_Loop4
 
!===============================================================================
! Find minimum solar zenith angle in line of data.
!===============================================================================
      l2_minsza = MINVAL(l2_sza)
 
!===============================================================================
! Test if minimum solar zenith angle in line of data is less than 0.0 degrees.
! If true, then cycle to next line of data.
!===============================================================================
      IF (l2_minsza < 0.0) CYCLE Line_Loop4
 
!===============================================================================
! Loop through scenes in line.
!===============================================================================
      Scene_Loop4: DO l2_iscene = 1, l2_nscene
 
!===============================================================================
! Test if solar zenith angle for scene is greater than 88.0 degrees.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (l2_sza(l2_iscene) > 88.0) CYCLE Scene_Loop4
 
!===============================================================================
! Test if value for key field for scene is equal to key missing value.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (TRIM(l2_prodid) == "OMTO3") key_fvalue = l2_cao3(l2_iscene)
        l2_dummy = ABS((key_fvalue - key_mvalue) / key_mvalue)
        IF (l2_dummy < 0.001) CYCLE Scene_Loop4
 
!===============================================================================
! Calculate indices of L2G grid cell that contains center of candidate scene.
! Dimensions of 0.25-degree by 0.25-degree L2G grid are 1440 by 720.  L2G grid
! cell with (l2g_ilon=1, l2g_ilat=1) is centered at (lon=-179.875, lat=-89.875).
! L2G grid cell with (l2g_ilon=1440, l2g_ilat=720) is centered at (lon=179.875,
! lat=89.875).
!===============================================================================
        IF (l2_lon(l2_iscene) < 0.0) l2g_ilon = &
                       720 + INT(4.0*l2_lon(l2_iscene) + 0.000001)
        IF (l2_lon(l2_iscene) >= 0.0) l2g_ilon = &
                       721 + INT(4.0*l2_lon(l2_iscene) - 0.000001)
        IF (l2_lat(l2_iscene) < 0.0) l2g_ilat = &
                       360 + INT(4.0*l2_lat(l2_iscene) + 0.000001)
        IF (l2_lat(l2_iscene) >= 0.0) l2g_ilat = &
                       361 + INT(4.0*l2_lat(l2_iscene) - 0.000001)
 
!===============================================================================
! Increment number of candidate scenes for L2G grid cell by one.
!===============================================================================
        l2g_ncs(l2g_ilon,l2g_ilat) = l2g_ncs(l2g_ilon,l2g_ilat) + 1
        l2g_icand = l2g_ncs(l2g_ilon,l2g_ilat)
 
!===============================================================================
! Copy layer efficiencies for L2 scene to corresponding L2G grid cell.
!===============================================================================
        l2g_leff(l2g_ilon,l2g_ilat,1:l2g_nlayers,l2g_icand) = &
        0.0001d0 * DNINT(10000.0d0 * l2_leff(1:l2g_nlayers,l2_iscene))
 
      END DO Scene_Loop4
      ! End of "DO l2_iscene = 1, l2_nscene" loop.
 
    END DO Line_Loop4
    ! End of "DO l2_iline = 1, l2_nline" loop.
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
    CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
  END DO File_Loop4
  ! End of "DO l2_ifile = 1, l2_nfile" loop.
 
!===============================================================================
! Write 4-D layer efficiencies field to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GLefGrid(l2g_gridid, &
                             l2g_nlon, l2g_nlat, &
                             l2g_nlayers, l2g_ncand, &
                             leff_mvalue, l2g_leff)
 
!===============================================================================
! Deallocate memory used for 4-D layer efficiencies O3 field.
!===============================================================================
  DEALLOCATE(l2g_leff)
 
!===============================================================================
! Process 4-D residuals field separately from other fields:
!===============================================================================
 
!===============================================================================
! Allocate memory for 4-D residuals field.
!===============================================================================
  ALLOCATE(l2g_res(l2g_nlon,l2g_nlat,l2g_nwavel,l2g_ncand))
 
!===============================================================================
! Obtain missing value for 4-D residuals field.
!===============================================================================
  res_mvalue = 0.0
  IF (TRIM(l2_prodid) == "OMTO3") THEN

    DO l2_ifield = 1, gfr4_nfield
 
      IF (TRIM(gfr4_names(l2_ifield)) == "Residual") THEN
        res_mvalue = gfr4_mvalue(l2_ifield)
      END IF
 
    END DO
    ! End of "DO l2_ifield = 1, gfr4_nfield" loop.
 
  END IF
  ! End of "IF (TRIM(l2_prodid) == "OMTO3")" test.
 
!===============================================================================
! Test if residuals missing value has not been obtained.  If true, then end
! OML2G execution in error.
!===============================================================================
  IF (res_mvalue > -0.001) THEN

!===============================================================================
! Close L2G grid and file.
!===============================================================================
    CALL OML2G_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! End OML2G execution in error.
!===============================================================================
    WRITE(stat_msg, '(A)') &
      "Fatal error:  residuals missing value was not obtained!"
    CALL OML2G_EndInFailure(stat_msg, code_ref)
 
  END IF
  ! End of "IF (res_mvalue > -0.001)" test.
 
!===============================================================================
! Initialize 4-D residuals field with missing value.
!===============================================================================
  l2g_res(1:l2g_nlon,1:l2g_nlat,1:l2g_nwavel,1:l2g_ncand) = res_mvalue
 
!===============================================================================
! Initialize 2-D number of candidate scenes field to zero.
!===============================================================================
  l2g_ncs(1:l2g_nlon,1:l2g_nlat) = 0
 
!===============================================================================
! Loop through L2 files.
!===============================================================================
  File_Loop5: DO l2_ifile = 1, l2_nfile
 
!===============================================================================
! Open L2 file and swath.
!===============================================================================
    CALL OML2G_OpenL2File(l2_fname(l2_ifile), l2_prodid, l2_fileid, l2_swathid)
 
!===============================================================================
! Read swath-level Metadata from L2 file.
!===============================================================================
    CALL OML2G_ReadL2SwathMeta(l2_swathid, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel)
 
!===============================================================================
! Test if L2 file contains zoom data.  If true, then cycle to next L2 file.
!===============================================================================
    IF (l2_nz(l2_ifile)     /= 0 .OR. &
        l2_nspatz(l2_ifile) /= 0 .OR. &
        l2_nspecz(l2_ifile) /= 0 ) THEN
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
      CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
!===============================================================================
! Cycle to next L2 file.
!===============================================================================
      CYCLE File_Loop5
 
    END IF
    ! End of "IF (l2_nz(l2_ifile) /= 0 .OR." test.
 
!===============================================================================
! Loop through lines of data in L2 swath.
!===============================================================================
    Line_Loop5: DO l2_iline = 1, l2_nline
 
!===============================================================================
! Read one line of geolocation from L2 file.
!===============================================================================
      CALL OML2G_ReadL2GeoLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, &
                               l2_gpqf, l2_lat, l2_lon, l2_raa, l2_secday, &
                               l2_saa, l2_sza, l2_scalt, l2_sclat, l2_sclon, &
                               l2_thgt, l2_tline, l2_vaa, l2_vza, l2_xtqf)
 
!===============================================================================
! Read one line of data-field data from L2 file.
!===============================================================================
      CALL OML2G_ReadL2DatLine(l2_swathid, &
                               l2_ifile, l2_iline, &
                               l2_nline, l2_nscene, l2_nlayers, l2_nwavel, &
                               l2_aflg, l2_aplo3, l2_cp, l2_cao3, &
                               l2_icid, l2_leff, l2_mqflg, l2_nspc, &
                               l2_o3bc, l2_qflg, l2_rcf, l2_r331, &
                               l2_r360, l2_res, l2_spc, l2_so2i, &
                               l2_s1o3, l2_s2o3, l2_tp, l2_uvai, l2_wav)
 
!===============================================================================
! Test if time of line of data lies outside L2G day under consideration.  If
! true, then cycle to next line of data.
!===============================================================================
      IF (l2_tline < l2g_tstart .OR. l2_tline >= l2g_tend) CYCLE Line_Loop5
 
!===============================================================================
! Find minimum solar zenith angle in line of data.
!===============================================================================
      l2_minsza = MINVAL(l2_sza)
 
!===============================================================================
! Test if minimum solar zenith angle in line of data is less than 0.0 degrees.
! If true, then cycle to next line of data.
!===============================================================================
      IF (l2_minsza < 0.0) CYCLE Line_Loop5
 
!===============================================================================
! Loop through scenes in line.
!===============================================================================
      Scene_Loop5: DO l2_iscene = 1, l2_nscene
 
!===============================================================================
! Test if solar zenith angle for scene is greater than 88.0 degrees.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (l2_sza(l2_iscene) > 88.0) CYCLE Scene_Loop5
 
!===============================================================================
! Test if value for key field for scene is equal to key missing value.  If true,
! then reject current scene, and cycle to next scene.
!===============================================================================
        IF (TRIM(l2_prodid) == "OMTO3") key_fvalue = l2_cao3(l2_iscene)
        l2_dummy = ABS((key_fvalue - key_mvalue) / key_mvalue)
        IF (l2_dummy < 0.001) CYCLE Scene_Loop5
 
!===============================================================================
! Calculate indices of L2G grid cell that contains center of candidate scene.
! Dimensions of 0.25-degree by 0.25-degree L2G grid are 1440 by 720.  L2G grid
! cell with (l2g_ilon=1, l2g_ilat=1) is centered at (lon=-179.875, lat=-89.875).
! L2G grid cell with (l2g_ilon=1440, l2g_ilat=720) is centered at (lon=179.875,
! lat=89.875).
!===============================================================================
        IF (l2_lon(l2_iscene) < 0.0) l2g_ilon = &
                       720 + INT(4.0*l2_lon(l2_iscene) + 0.000001)
        IF (l2_lon(l2_iscene) >= 0.0) l2g_ilon = &
                       721 + INT(4.0*l2_lon(l2_iscene) - 0.000001)
        IF (l2_lat(l2_iscene) < 0.0) l2g_ilat = &
                       360 + INT(4.0*l2_lat(l2_iscene) + 0.000001)
        IF (l2_lat(l2_iscene) >= 0.0) l2g_ilat = &
                       361 + INT(4.0*l2_lat(l2_iscene) - 0.000001)
 
!===============================================================================
! Increment number of candidate scenes for L2G grid cell by one.
!===============================================================================
        l2g_ncs(l2g_ilon,l2g_ilat) = l2g_ncs(l2g_ilon,l2g_ilat) + 1
        l2g_icand = l2g_ncs(l2g_ilon,l2g_ilat)
 
!===============================================================================
! Copy residuals for L2 scene to corresponding L2G grid cell.
!===============================================================================
        l2g_res(l2g_ilon,l2g_ilat,1:l2g_nwavel,l2g_icand) = &
          0.001d0 * DNINT(1000.0d0 * l2_res(1:l2g_nwavel,l2_iscene))
 
      END DO Scene_Loop5
      ! End of "DO l2_iscene = 1, l2_nscene" loop.
 
    END DO Line_Loop5
    ! End of "DO l2_iline = 1, l2_nline" loop.
 
!===============================================================================
! Close L2 swath and file.
!===============================================================================
    CALL OML2G_CloseL2File(l2_fileid, l2_swathid)
 
  END DO File_Loop5
  ! End of "DO l2_ifile = 1, l2_nfile" loop.
 
!===============================================================================
! Write 4-D residuals field to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GResGrid(l2g_gridid, &
                             l2g_nlon, l2g_nlat, &
                             l2g_nwavel, l2g_ncand, &
                             res_mvalue, l2g_res)
 
!===============================================================================
! Deallocate memory used for 4-D residuals field.
!===============================================================================
  DEALLOCATE(l2g_res)
 
!===============================================================================
! Write grid-field Metadata to L2G file.
!===============================================================================
  CALL OML2G_WriteL2GFieldMeta(l2g_gridid, &
                               gfi1u_nfield, gfi1u_names, &
                               gfi2_nfield, gfi2_names, &
                               gfi2u_nfield, gfi2u_names, &
                               gfi4_nfield, gfi4_names, &
                               gfr4_nfield, gfr4_names, &
                               gfr8_nfield, gfr8_names, &
                               gfi1u_units, gfi1u_title, gfi1u_ufdef, &
                               gfi1u_sfactor, gfi1u_offset, &
                               gfi1u_vrange, gfi1u_mvalue, &
                               gfi2_units, gfi2_title, gfi2_ufdef, &
                               gfi2_sfactor, gfi2_offset, &
                               gfi2_vrange, gfi2_mvalue, &
                               gfi2u_units, gfi2u_title, gfi2u_ufdef, &
                               gfi2u_sfactor, gfi2u_offset, &
                               gfi2u_vrange, gfi2u_mvalue, &
                               gfi4_units, gfi4_title, gfi4_ufdef, &
                               gfi4_sfactor, gfi4_offset, &
                               gfi4_vrange, gfi4_mvalue, &
                               gfr4_units, gfr4_title, gfr4_ufdef, &
                               gfr4_sfactor, gfr4_offset, &
                               gfr4_vrange, gfr4_mvalue, &
                               gfr8_units, gfr8_title, gfr8_ufdef, &
                               gfr8_sfactor, gfr8_offset, &
                               gfr8_vrange, gfr8_mvalue)
 
!===============================================================================
! Close L2G grid and file.
!===============================================================================
  CALL OML2G_CloseL2GFile(l2g_fileid, l2g_gridid)
 
!===============================================================================
! Write ECS Metadata to L2G (HDF-EOS 5 and .met) files.
!===============================================================================
  CALL OML2G_WriteL2GECSMeta(l2g_fname, l2_prodid, &
                             l2_nfile, l2_fname, l2_ifirst, &
                             l2g_year, l2g_month, l2g_day, &
                             l2_qapmd, l2_nm, l2_nlgood, &
                             l2_orbit, l2_ecd, l2_ect, l2_ecl, &
                             l2g_rbd, l2g_rbt, l2g_red, l2g_ret, &
                             l2_ed, l2_saac, l2_sm, l2_se, &
                             l2_mcp, l2_et)
 
!===============================================================================
! End OML2G execution normally with non-fatal error(s) and exit code = 0.
!===============================================================================
  WRITE(stat_msg, '(A)') &
    "OML2G execution ends normally with non-fatal error(s) and exit code = 0."
  call_stat = OMI_SMF_setmsg(OMI_S_SUCCESS, stat_msg, code_ref, 0)
  CALL EXIT(0)
 
END PROGRAM OMTO3G
