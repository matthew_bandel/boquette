!===============================================================================
!
! SUBROUTINE OML2G_ReadL2ECSMeta
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to read the ECS Metadata from an OMI
!   L2 product file.
!
! The First Argument Is An Input:
!   l2_ifile             - L2 file counter.
!
! The Final Twenty Five Arguments Are Outputs:
!   l2_orbit             - L2 orbit number.
!   l2_ecd               - Equator crossing date of L2 file.
!   l2_ect               - Equator crossing time of L2 file.
!   l2_ecl               - Equator crossing longitude of L2 file.
!   l2_rbd               - Range beginning date of L2 file.
!   l2_rbt               - Range beginning time of L2 file.
!   l2_red               - Range ending date of L2 file.
!   l2_ret               - Range ending time of L2 file.
!   l2_orbper            - Orbital period of L2 granule.
!   l2_qapmd             - QA percent missing data of L2 file.
!   l2_nm                - Number of measurements in L2 file.
!   l2_nz                - Number of zoom measurements in L2 file.
!   l2_nspatz            - Number of spatial zoom measurements in L2 file.
!   l2_nspecz            - Number of spectral zoom measurements in L2 file.
!   l2_ed                - Expedited data indicator for L2 file.
!   l2_saac              - South Atlantic Anomaly crossing indicator for L2 file.
!   l2_sm                - Spacecraft maneuver indicator for L2 file.
!   l2_se                - Solar eclipse indicator for L2 file.
!   l2_mcp               - Master clock periods for L2 file.
!   l2_et                - Exposure times for L2 file.
!   l2_cps               - Cloud pressure source for L2 file.
!   l2_tps               - Terrain pressure source for L2 file.
!   l2_ts                - Temperature source for L2 file.
!   l2_sis               - Snow ice source for L2 file.
!   l2_apops             - A priori ozone profile source for L2 file.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure   - Ends OML2G execution in failure.
!
! PGS Toolkit Functions Called:
!   pgs_met_getpcattr_i  - Reads integer Metadatum from LUN-specified file.
!   pgs_met_getpcattr_s  - Reads string Metadatum from LUN-specified file.
!   pgs_met_getpcattr_d  - Reads floating point Metadatum from LUN-specified file.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 26, 2005
!
! Revision History:
!   April 26, 2006 - Made handling of master clock periods and exposure times
!                    more robust.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A1C1
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History:
!   March 27, 2007 - Added INTENT(IN) to l2_ifile declaration.
!
!===============================================================================
 
SUBROUTINE OML2G_ReadL2ECSMeta(l2_ifile, &
                               l2_orbit, l2_ecd, &
                               l2_ect, l2_ecl, &
                               l2_rbd, l2_rbt, &
                               l2_red, l2_ret, &
                               l2_orbper, l2_qapmd, &
                               l2_nm, l2_nz, &
                               l2_nspatz, l2_nspecz, &
                               l2_ed, l2_saac, &
                               l2_sm, l2_se, &
                               l2_mcp, l2_et, &
                               l2_cps, l2_tps, &
                               l2_ts, l2_sis, &
                               l2_apops)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include file.
!===============================================================================
  INCLUDE 'PGS_MET_13.f'      ! Some Toolkit parameters.
 
!===============================================================================
! Declaration of input argument.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_ifile    ! L2 file counter.
 
!===============================================================================
! Declarations of output arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_orbit   ! L2 orbit number.
  CHARACTER (LEN=10), &
    INTENT(OUT) :: l2_ecd     ! Equator crossing date of L2 file.
  CHARACTER (LEN=16), &
    INTENT(OUT) :: l2_ect     ! Equator crossing time of L2 file.
  REAL (KIND=8), &
    INTENT(OUT) :: l2_ecl     ! Equator crossing longitude of L2 file.
  CHARACTER (LEN=10), &
    INTENT(OUT) :: l2_rbd     ! Range beginning date of L2 file.
  CHARACTER (LEN=16), &
    INTENT(OUT) :: l2_rbt     ! Range beginning time of L2 file.
  CHARACTER (LEN=10),&
    INTENT(OUT) :: l2_red     ! Range ending date of L2 file.
  CHARACTER (LEN=16), &
    INTENT(OUT) :: l2_ret     ! Range ending time of L2 file.
  REAL (KIND=8), &
    INTENT(OUT) :: l2_orbper  ! Orbital period of L2 granule.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_qapmd   ! QA percent missing data of L2 file.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_nm      ! Number of measurements in L2 file.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_nz      ! Number of zoom measurements in L2 file.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_nspatz  ! Number of spatial zoom measurements in L2 file.
  INTEGER (KIND=4), &
    INTENT(OUT) :: l2_nspecz  ! Number of spectral zoom measurements in L2 file.
  CHARACTER (LEN=5), &
    INTENT(OUT) :: l2_ed      ! Expedited data indicator for L2 file.
  CHARACTER (LEN=5), &
    INTENT(OUT) :: l2_saac    ! South Atlantic Anomaly crossing indicator for L2 file.
  CHARACTER (LEN=5), &
    INTENT(OUT) :: l2_sm      ! Spacecraft maneuver indicator for L2 file.
  CHARACTER (LEN=5), &
    INTENT(OUT) :: l2_se      ! Solar eclipse indicator for L2 file.
  REAL (KIND=8), &
    DIMENSION(6), &
    INTENT(OUT) :: l2_mcp     ! Master clock periods for L2 file.
  REAL (KIND=8), &
    DIMENSION(6), &
    INTENT(OUT) :: l2_et      ! Exposure times for L2 file.
  CHARACTER (LEN=256), &
    INTENT(OUT) :: l2_cps     ! Cloud pressure source for L2 file.
  CHARACTER (LEN=256), &
    INTENT(OUT) :: l2_tps     ! Terrain pressure source for L2 file.
  CHARACTER (LEN=256), &
    INTENT(OUT) :: l2_ts      ! Temperature source for L2 file.
  CHARACTER (LEN=256), &
    INTENT(OUT) :: l2_sis     ! Snow ice source for L2 file.
  CHARACTER (LEN=256), &
    INTENT(OUT) :: l2_apops   ! A priori ozone profile source for L2 file.
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=4) :: l2_filelun  ! LUN for L2 file.
  INTEGER (KIND=4) :: i4hour      ! Hour of day.
  INTEGER (KIND=4) :: i4min       ! Minute hour.
  REAL (KIND=8) :: r8sec          ! Second of minute.
  REAL (KIND=8) :: dummy1         ! Dummy variable for range beginning time.
  REAL (KIND=8) :: dummy2         ! Dummy variable related to range ending time.
  CHARACTER (LEN=6) :: dummy6     ! Dummy variable for reading in integer PSAs.
  CHARACTER (LEN=5) :: dummy5     ! Dummy variable for reading indicator PSAs.
  CHARACTER (LEN=8), &
    DIMENSION(6) :: dummy8x6      ! Dummy variable for clock and exposure times.
  INTEGER (KIND=4) :: itemporal   ! Clock and exposure time (temporal) counter.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat           ! Function call status.
  CHARACTER (LEN=256) :: stat_msg         ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_ReadL2ECSMeta.f90"  ! Code file reference.

!===============================================================================
! Declaration of PGS Toolkit function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: pgs_met_getpcattr_i       ! Reads integer ECS Metadatum.
  INTEGER (KIND=4), &
    EXTERNAL :: pgs_met_getpcattr_s       ! Reads string ECS Metadatum.
  INTEGER (KIND=4), &
    EXTERNAL :: pgs_met_getpcattr_d       ! Reads floating point ECS Metadatum.
 
!===============================================================================
! Initialize ECS Metadatum to be read from L2 file.
!===============================================================================
  l2_orbit = -1
  l2_ecd = " "
  l2_ect = " "
  l2_ecl = -1.0d0
  l2_rbd = " "
  l2_rbt = " "
  l2_red = " "
  l2_ret = " "
  l2_qapmd = -1
  l2_nm = -1
  l2_nz = -1
  l2_nspatz = -1
  l2_nspecz = -1
  l2_ed = " "
  l2_saac = " "
  l2_sm = " "
  l2_se = " "
  l2_mcp(1:6) = -1.0d0
  l2_et(1:6) = -1.0d0
  l2_cps = " "
  l2_tps = " "
  l2_ts = " "
  l2_sis = " "
  l2_apops = " "
 
!===============================================================================
! Read ECS Metadata from L2 file:
!===============================================================================
 
!===============================================================================
! Read orbit number from L2 file.
!===============================================================================
  l2_filelun = 300011
  call_stat = pgs_met_getpcattr_i(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "ORBITNUMBER.1", l2_orbit)
  IF (call_stat .NE. 0) GO TO 2
 
!===============================================================================
! Read equator crossing date from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "EQUATORCROSSINGDATE.1", l2_ecd)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read equator crossing time from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "EQUATORCROSSINGTIME.1", l2_ect)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read equator crossing longitude from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_d(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "EQUATORCROSSINGLONGITUDE.1", l2_ecl)
  IF (call_stat .NE. 0) GO TO 8
 
!===============================================================================
! Read range beginning date from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "RANGEBEGINNINGDATE", l2_rbd)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read range beginning time from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "RANGEBEGINNINGTIME", l2_rbt)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read range ending date from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "RANGEENDINGDATE", l2_red)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read range ending time from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "RANGEENDINGTIME", l2_ret)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Calculate orbital period of L2 granule.
!===============================================================================
  READ(l2_rbt,'(I2,1X,I2,1X,F9.6)') i4hour,i4min,r8sec
  dummy1 = ((i4hour * 60) + i4min)*60 + r8sec
  READ(l2_ret,'(I2,1X,I2,1X,F9.6)') i4hour,i4min,r8sec
  dummy2 = ((i4hour * 60) + i4min)*60 + r8sec
  IF (dummy1 .GT. dummy2) dummy2 = dummy2 + 86400.0d0
  l2_orbper = dummy2 - dummy1

  write(6, *) 'l2_rbt, l2_ret, dummy1, dummy2, l2_orbper:'
  write(6, *) l2_rbt, l2_ret, dummy1, dummy2, l2_orbper


!===============================================================================
! Read QA percent missing data from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_i(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "QAPERCENTMISSINGDATA.1", l2_qapmd)
  IF (call_stat .NE. 0) GO TO 2
 
!===============================================================================
! Read number of measurements from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "NrMeasurements", dummy6)
  IF (call_stat .NE. 0) GO TO 5
  READ(dummy6,'(I6)') l2_nm
 
!===============================================================================
! Read number of zoom measurements from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "NrZoom", dummy6)
  IF (call_stat .NE. 0) GO TO 5
  READ(dummy6,'(I6)') l2_nz
 
!===============================================================================
! Read number of spatial zoom measurements from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "NrSpatialZoom", dummy6)
  IF (call_stat .NE. 0) GO TO 5
  READ(dummy6,'(I6)') l2_nspatz
 
!===============================================================================
! Read number of spectral zoom measurements from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "NrSpectralZoom", dummy6)
  IF (call_stat .NE. 0) GO TO 5
  READ(dummy6,'(I6)') l2_nspecz
 
!===============================================================================
! Read expedited data indicator from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "ExpeditedData", dummy5)
  IF (call_stat .NE. 0) GO TO 5
  l2_ed = TRIM(dummy5)
 
!===============================================================================
! Read South Atlantic Anomaly crossing indicator from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "SouthAtlanticAnomalyCrossing", dummy5)
  IF (call_stat .NE. 0) GO TO 5
  l2_saac = TRIM(dummy5)
 
!===============================================================================
! Read spacecraft maneuver indicator from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "SpacecraftManeuverFlag", dummy5)
  IF (call_stat .NE. 0) GO TO 5
  l2_sm = TRIM(dummy5)
 
!===============================================================================
! Read solar eclipse indicator from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "SolarEclipse", dummy5)
  IF (call_stat .NE. 0) GO TO 5
  l2_se = TRIM(dummy5)
 
!===============================================================================
! Read master clock periods from L2 file.
!===============================================================================
  dummy8x6(1:6) = ' '
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "MasterClockPeriods", dummy8x6)
  IF (call_stat .NE. 0) GO TO 5
  DO itemporal = 1, 6
    IF (dummy8x6(itemporal) .NE. ' ') &
      READ(dummy8x6(itemporal),'(F8.6)') l2_mcp(itemporal)
  END DO
 
!===============================================================================
! Read exposure times from L2 file.
!===============================================================================
  dummy8x6(1:6) = ' '
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "CoreMetadata", &
                                  "ExposureTimes", dummy8x6)
  IF (call_stat .NE. 0) GO TO 5
  DO itemporal = 1, 6
    IF (dummy8x6(itemporal) .NE. ' ') &
      READ(dummy8x6(itemporal),'(F8.6)') l2_et(itemporal)
  END DO
 
!===============================================================================
! Read cloud pressure source from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "ArchivedMetadata", &
                                  "CLOUDPRESSURESOURCE", l2_cps)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read terrain pressure source from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "ArchivedMetadata", &
                                  "TERRAINPRESSURESOURCE", l2_tps)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read temperature source from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "ArchivedMetadata", &
                                  "TEMPERATURESOURCE", l2_ts)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read snow ice source from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "ArchivedMetadata", &
                                  "SNOWICESOURCE", l2_sis)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Read a priori ozone profile source from L2 file.
!===============================================================================
  call_stat = pgs_met_getpcattr_s(l2_filelun, l2_ifile, "ArchivedMetadata", &
                                  "APRIORIOZONEPROFILESOURCE", l2_apops)
  IF (call_stat .NE. 0) GO TO 5
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN

!===============================================================================
! This part of subroutine is reached only if function call status is not equal to
! zero for a call to pgs_met_getpcattr_i, and so end execution with fatal error.
!===============================================================================
2 WRITE(stat_msg, 3) call_stat
3 FORMAT("Fatal error while reading ECS Metadatum:", &
         "  pgs_met_getpcattr_i call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
!===============================================================================
! This part of subroutine is reached only if function call status is not equal to
! zero for a call to pgs_met_getpcattr_s, and so end execution with fatal error.
!===============================================================================
5 WRITE(stat_msg, 6) call_stat
6 FORMAT("Fatal error while reading ECS Metadatum:", &
         "  pgs_met_getpcattr_s call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
!===============================================================================
! This part of subroutine is reached only if function call status is not equal to
! zero for a call to pgs_met_getpcattr_d, and so end execution with fatal error.
!===============================================================================
8 WRITE(stat_msg, 9) call_stat
9 FORMAT("Fatal error while reading ECS Metadatum:", &
         "  pgs_met_getpcattr_d call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML2G_ReadL2ECSMeta
