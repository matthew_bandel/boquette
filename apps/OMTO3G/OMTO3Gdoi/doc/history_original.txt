--------------------------------------------------------------------------------
history.txt   summary of differences between versions of OMTO3G

Information from this file is used by managers while deciding when to place a
version of a PGE into production.

This file shows a summary of the following for each version of a PGE or
shared code:

  (a) What changed
  (b) Why it changed
  (c) How the output product will be affected
  (d) The approximate date of the change

This file is not an exhaustive list of all changes.  Instead, it only highlights
the most important features that make each version unique.
--------------------------------------------------------------------------------

1.2.3                                                                 5/11/2023
--------------------------------------------------------------------------------
Adaptation to six digit orbit numbers in file, mainly concerning a fixed file name length
of 65 characters, which now must accommodate 66 character file names.  The main change is to key off
"OMI-Aura" to find the start of the filename from the full path, rather than relying on a fixed 65 character
file name.  Trimming of the individual filenames seems to be handled by the external function
pgs_met_setmultiattr_s.

-MB

OML2G_WriteL2GECSMeta:

line 195:
  !extending file name length to 256 characters to accomodate longer file names - MB 5/10/23
!  CHARACTER (LEN=65), &
!    DIMENSION(20) :: l2_fname65    ! L2 file names without directory path.
  CHARACTER (LEN=256), &
    DIMENSION(20) :: l2_fname256    ! L2 file names without directory path.

line 448:
    ! adjust for six digit orbits by keying off OMI_Aura for path - MB 5/11/23
!    dirpathlen = LEN_TRIM(l2_fname(l2_ifile)) - 65
    dirpathlen = index(l2_fname(l2_ifile), "OMI-Aura_L2") - 1


line 454:
    ! only perform if dirpathlen is greater than 1 - MB 5/11/23
!    DO ccount = 1, dirpathlen
!      READ(dummy256,'(1X,A)') dummy256
!    END DO
    if (dirpathlen.gt.1) then
      DO ccount = 1, dirpathlen
        READ(dummy256,'(1X,A)') dummy256
      END DO
    end if

line 465:
    ! adjust for six digit orbits and handle negative directory path lengths - MB 5/11/23
!    READ(dummy256,'(A65)') l2_fname65(l2_ifile)
!
!    IF (dirpathlen == 0) READ(l2_fname(l2_ifile),'(A65)') l2_fname65(l2_ifile)

    READ(dummy256,'(A256)') l2_fname256(l2_ifile)

    IF (dirpathlen.lt.1) READ(l2_fname(l2_ifile),'(A256)') l2_fname256(l2_ifile)

line 481:
  ! adjust for six digit orbits - MB 5/10/23
!  call_stat = pgs_met_setmultiattr_s(groups(inventory), "InputPointer", &
!                                     l2_nfile, l2_fname65)

  call_stat = pgs_met_setmultiattr_s(groups(inventory), "InputPointer", &
                                     l2_nfile, l2_fname256)







1.2.2                                                                 5/10/2023
--------------------------------------------------------------------------------
1.Romoved utd directory as it was interferring with delivery


1.2.1                                                                 5/10/2023
--------------------------------------------------------------------------------
multiple changes to handle ubuntu 64 bit processor

see
https://hdfeos.org/examples/fort_he5_swath.php

1.Multiple changes to start, stride, and edge parameters
2.Multiple arrays given "save" indicator to maintain stability
3.Updating leap seconds file

Note: XtrackQuality is now using 0 as indicator for good data, whereas before it was using 255


1.2.0                                                                 11/22/2012
--------------------------------------------------------------------------------
1.Changed l2_xtqf_blk to KIND=1 in OML2G_ReadL2GeoLine.
2.Revised upp_lft_pt and low_rgt_pt in OML2G_OpenL2GFile.
3.Changed Unit Test date to 2006-01-01.
 
1.1.0                                                                 07/13/2011
--------------------------------------------------------------------------------
1.Added LUN 200109 for l2g_dyofyr to Description.txt file.
2.Added read of LUN 200109 (l2g_dyofyr) from PCF in OML2G_ReadPCF.
3.Added test for consistency of l2g_dyofyr in OMTO3G main.
 
1.0.5                                                                 06/19/2010
--------------------------------------------------------------------------------
Added XTrackQualityFlags field via changes to OML2G_ReadL2FieldInfo,
OML2G_ReadL2GeoLine, OML2G_WriteL2GGeoGrid and OMTO3G main.
 
1.0.3.5                                                               06/03/2008
--------------------------------------------------------------------------------
Revised OMTO3G.fs (product format specification file).
 
1.0.3.3                                                               08/13/2008
--------------------------------------------------------------------------------
Another test delivery for testing AppDeliver on omiveh.
 
1.0.3.2                                                               08/13/2008
--------------------------------------------------------------------------------
Test delivery for testing AppDeliver on omiveh.
 
1.0.3.1                                                               12/18/2007
--------------------------------------------------------------------------------
Corrected OMTO3G.fs (product format specification file).
 
1.0.3                                                                 11/13/2007
--------------------------------------------------------------------------------
1.To match fields in Version 1.1.0 of OMTO3, renamed CloudTopPressure field to
  CloudPressure, and also added RadiativeCloudFraction field (via changes to
  OML2G_ReadL2FieldInfo, OML2G_ReadL2DatLine, OML2G_WriteL2GDatGrid and OMTO3G
  main).
2.Updated OMTO3G.fs (product format specification file) accordingly.
3.Changed Unit Test date to April 5, 2005.
 
1.0.2.1                                                               10/30/2007
--------------------------------------------------------------------------------
Removed OMTO3G.pl script.
 
1.0.2                                                                 10/18/2007
--------------------------------------------------------------------------------
Several minor modifications mainly in OMTO3G main.
 
1.0.1                                                                 10/08/2007
--------------------------------------------------------------------------------
Included Description.txt file, and re-delivered as an App.
 
1.0.0.1                                                               04/05/2007
--------------------------------------------------------------------------------
Modified Metadata in OMTO3G.fs (product format specification file) to match
recent revisions to OMTO3G product file.
 
1.0.0                                                                 03/29/2007
--------------------------------------------------------------------------------
Revised version of OMTO3G delivered for production.
 
0.9.38.8                                                              03/27/2007
--------------------------------------------------------------------------------
Use DNINT for rounding floating point field values in OMTO3G main.
 
0.9.38.7                                                              03/27/2007
--------------------------------------------------------------------------------
Minor fixes in OML2G_OpenL2File, OML2G_OpenL2GFile, OML2G_ReadL2ECSMeta,
OML2G_ReadL2GeoLine.f90 and OML2G_WriteL2GFieldMeta.
 
0.9.38.6                                                              03/23/2007
--------------------------------------------------------------------------------
Increased number of characters in l2g_pgevers to 20 in OML2G_ReadPCF,
OML2G_WriteL2GFileMeta and OMTO3G main.
 
0.9.38.5                                                              03/22/2007
--------------------------------------------------------------------------------
Added fix in OML2G_FindL2GDay to obtain correct l2_ifirst for rare case.
 
0.9.38.4                                                              03/22/2007
--------------------------------------------------------------------------------
Added GridSpacing, GridSpacingUnit, GridSpan and GridSpanUnit to grid-level
Metadata in OML2G_WriteL2GGridMeta.
 
0.9.38.3                                                              03/22/2007
--------------------------------------------------------------------------------
Added FirstLineInOrbit and LastLineInOrbit to file-level Metadata via revisions
in OML2G_WriteL2GFileMeta and OMTO3G main.
 
0.9.38.2                                                              03/21/2007
--------------------------------------------------------------------------------
1.Check each line of each L2 file for bad geolocation in OMTO3G main.
2.Count number of lines in each L2 file with bad geolocation in OMTO3G main.
3.Report each line with bad geolocation in PGE log file via OMTO3G main.
4.Write NumberOfLinesMissingGeolocation Metadatum via OML2G_WriteL2GFileMeta.
 
0.9.38.1                                                              03/21/2007
--------------------------------------------------------------------------------
Added screening in OMTO3G main to exclude Zoom-Mode orbits.
 
0.9.38                                                                03/21/2007
--------------------------------------------------------------------------------
Baseline delivery with Unit Test data.
 
0.9.37.7                                                              08/24/2006
--------------------------------------------------------------------------------
GetOMIDailyOrbitCounts production rule added to PGE_Description.txt to prevent
PGE from waiting for OMTO3 global-zoom data.
 
0.9.37.6                                                              05/25/2006
--------------------------------------------------------------------------------
Corrected typos in OMTO3G.fs and OML2G_WriteL2GECSMeta.
 
0.9.37.5                                                              05/24/2006
--------------------------------------------------------------------------------
Renamed Projection to GCTPProjectionCode, and added GridName, GridOrigin and
(new) Projection to OMTO3G.fs.
 
0.9.37.4                                                              05/24/2006
--------------------------------------------------------------------------------
1.Use l2_prodid to set ParameterName in OML2G_WriteL2GECSMeta.
2.Included l2_prodid in call to OML2G_WriteL2GECSMeta from OMTO3G main.
 
0.9.37.3                                                              05/24/2006
--------------------------------------------------------------------------------
Renamed integer Metadatum named Projection to GCTPProjectionCode, and added
string Metadata named Projection and GridOrigin to OML2G_WriteL2GGridMeta.
 
0.9.37.2                                                              04/26/2006
--------------------------------------------------------------------------------
1. Made handling of master clock periods and exposure times more robust in
   OML2G_ReadL2ECSMeta.
2. Changed upper limit of ValidRange of OrbitNumber to 999999 in
   OML2G_WriteL2GFieldMeta.
3. Revised OMTO3G.fs to fix problems described in Bug 3469.
 
0.9.37.1                                                              03/28/2006
--------------------------------------------------------------------------------
Added "&" character to fix Bug in OML2G_WriteL2GECSMeta.
 
0.9.37                                                                03/28/2006
--------------------------------------------------------------------------------
1.Initial version of OMTO3G.fs (product format specification file).
2.Initial version of history.txt (PGE history file - this file).
3.Made revisions to file header and other comments in OMTO3G main.
4.Included "UNKNOWN" as possible value of L2G spacecraft maneuver indicator in
  OML2G_WriteL2GECSMeta.
5.Changed low end of valid range for NumberOfCandidateScenes from 1 to 0 in
  OML2G_WriteL2GFieldMeta.
 
0.9.36.5                                                              02/03/2006
--------------------------------------------------------------------------------
Improvements related to Version ID in OML2G_WriteL2GECSMeta.
 
0.9.36.4                                                              02/03/2006
--------------------------------------------------------------------------------
Added error handling for Version ID in OML2G_WriteL2GECSMeta.
 
0.9.36.3                                                              02/03/2006
--------------------------------------------------------------------------------
Modified LocalGranuleID to handle ECS Collection Version IDs greater than 9 in
OML2G_WriteL2GECSMeta.
 
0.9.36.2                                                              02/03/2006
--------------------------------------------------------------------------------
Removed Value of VersionID from OMTO3G.MCF.
 
0.9.36.1                                                              02/02/2006
--------------------------------------------------------------------------------
1.Included leap years 2004 and 2024 in day sum calculation in OML2G_FindL2GDay.
2.Changed Data_Location of VersionID from MCF to PCF in OMTO3G.MCF.
 
0.9.36                                                                11/10/2005
--------------------------------------------------------------------------------
1.Removed l2_qapmd array from L2G grid-level Metadata in OML2G_WriteL2GGridMeta.
2.Included l2_qapmd array in L2G file-level Metadata in OML2G_WriteL2GFileMeta.
3.Moved l2_qapmd array to OML2G_WriteL2GFileMeta call in OMTO3G main.
 
0.9.35                                                                11/10/2005
--------------------------------------------------------------------------------
1.Increased size of l2_dayhist array from 20 to 366 in OML2G_FindL2GDay.
2.Included l2_qapmd array in L2G grid-level Metadata in OML2G_WriteL2GGridMeta.
3.Included l2_qapmd array in OML2G_WriteL2GGridMeta call in OMTO3G main.
4.Changed input data from 07/20/05 to 01/22/05 in OMTO3G.pcf .
 
0.9.31                                                                10/07/2005
--------------------------------------------------------------------------------
Changed NUM_VAL from 16 to 1 for InputPointer in OMTO3G.MCF.
 
0.9.30                                                                10/05/2005
--------------------------------------------------------------------------------
Removed directory path from beginning of each L2 file name before setting
InputPointer Metadatum in OML2G_WriteL2GECSMeta.
 
0.9.29                                                                09/30/2005
--------------------------------------------------------------------------------
Increased accuracy of angles to three decimal places in OMTO3G main.
 
0.9.28                                                                09/29/2005
--------------------------------------------------------------------------------
1.Renamed nLongitude to XDim and nLatitude to YDim in OML2G_OpenL2GFile,
  OML2G_WriteL2GAplGrid, OML2G_WriteL2GDatGrid, OML2G_WriteL2GGeoGrid,
  OML2G_WriteL2GLefGrid, OML2G_WriteL2GResGrid and OMTO3G main.
2.Reversed order of dimensions (again) in OML2G_WriteL2GAplGrid,
  OML2G_WriteL2GDatGrid, OML2G_WriteL2GGeoGrid, OML2G_WriteL2GLefGrid,
  OML2G_WriteL2GResGrid and OMTO3G main.
3.Added comments in OML2G_WriteL2GFieldMeta.
4.Improved wording of comments in OML2G_WriteL2GFileMeta.
 
0.9.27.1                                                              09/20/2005
--------------------------------------------------------------------------------
Separated processing of 3-D geolocation fields from processing of 3-D data
fields in OMTO3G main.
 
0.9.27                                                                09/02/2005
--------------------------------------------------------------------------------
1.Added missing values to arguments of OML2G_WriteL2GAplGrid,
  OML2G_WriteL2GDatGrid, OML2G_WriteL2GGeoGrid, OML2G_WriteL2GLefGrid and
  OML2G_WriteL2GResGrid to set grid fill values.
2.Reversed dimension order of 2-D, 3-D and 4-D fields in OML2G_WriteL2GAplGrid,
  OML2G_WriteL2GDatGrid, OML2G_WriteL2GGeoGrid, OML2G_WriteL2GLefGrid,
  OML2G_WriteL2GResGrid and OMTO3G main.
3.Removed l2g_onrange and l2g_oprange;  instead write all non-fill elements of
  l2_orbit and l2_orbper to L2G file-level Metadata via OML2G_WriteL2GFileMeta
  and OMTO3G main to comply with HDF-EOS Aura File Format Guidelines.
 
0.9.26                                                                08/19/2005
--------------------------------------------------------------------------------
1.Included leap years in l2_daysum calculation in OML2G_FindL2GDay.
2.Decreased l2g_tend by 0.000001 s in OML2G_FindL2GDay.
3.Separated processing of l2g_aplo3, l2g_leff and l2g_leff from each other and
  from processing of other L2G fields in OMTO3G main.
4.Increased value of l2g_ncand parameter from 12 to 15 in OMTO3G main following
  improvements in processing of 4-D data fields.
 
0.9.25.1                                                              08/18/2005
--------------------------------------------------------------------------------
Decreased value of l2g_ncand parameter from 13 to 12 in OMTO3G main to reduce
memory requirements of PGE.
 
0.9.25                                                                08/12/2005
--------------------------------------------------------------------------------
1.Increased wavelength dimension of l2_res and l2_wav from 10 to 12 in
  OML2G_ReadL2DatLine and OMTO3G main.
2.Increased value of l2g_ncand parameter from 10 to 13 in OMTO3G main.
3.Added logic to end execution of PGE in error if l2g_ncgcmax is greater than
  l2g_ncand in OMTO3G main.
4.Removed 4-D data fields (l2g_aplo3, l2g_leff and l2g_res) from
  OML2G_WriteL2GDatGrid.
5.Initial versions of OML2G_WriteL2GAplGrid, OML2G_WriteL2GLefGrid and
  OML2G_WriteL2GResGrid (to write 4-D data fields to L2G file).
6.Added calls to OML2G_WriteL2GAplGrid, OML2G_WriteL2GLefGrid and
  OML2G_WriteL2GResGrid in OMTO3G main.
7.Added allocation and deallocation statements for 3-D and 4-D arrays in OMTO3G
  main to improve memory usage.
8.Corrected setting of MissingValue Metadata in OML2G_WriteL2GFieldMeta and
  OMTO3G main.
9.Changed input data from 03/02/05 to 07/20/05 in OMTO3G.pcf .
 
0.9.20.1                                                              07/28/2005
--------------------------------------------------------------------------------
Initial version of OMTO3G.pl .
 
0.9.20                                                                07/22/2005
--------------------------------------------------------------------------------
Initial delivery of PGE.
