!===============================================================================
!
! SUBROUTINE OML2G_WriteL2GFileMeta
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to write the file-level Metadata to
!   the OMI L2G product file.
!
! All Fifteen Arguments Are Inputs:
!   l2g_fileid          - L2G file ID.
!   l2_nfile            - Number of L2 files.
!   l2_orbit            - List of L2 orbit numbers.
!   l2_lrange           - L2 orbital line ranges.
!   l2_nlbad            - Numbers of "bad" lines in L2 files.
!   l2_orbper           - Orbital periods of L2 granules.
!   l2g_month           - Month of L2G file.
!   l2g_day             - Day of L2G file.
!   l2g_year            - Year of L2G file.
!   l2g_dyofyr          - Day of year of L2G file.
!   l2g_TAI93at0z       - TAI93 at 0z of L2G file.
!   l2g_pgevers         - L2G PGE version.
!   l2g_tstart          - TAI93 at start of L2G file.
!   l2g_tend            - TAI93 at end of L2G file.
!   l2_qapmd            - QA percent missing data of L2 files.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure  - Ends OML2G execution in failure.
!
! PGS Toolkit Function Called:
!   pgs_td_taitoutc     - Converts TAI93 to UTC ascii string.
!
! HDF-EOS 5 Function Called:
!   he5_ehwrglattr      - Writes global attribute to file.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 27, 2005
!
! Revision History:
!   September 2, 2005 - Removed l2g_onrange and l2g_oprange, and instead write
!                       all non-fill elements of l2_orbit and l2_orbper to L2G
!                       file to comply with HDF-EOS Aura File Format Guidlines.
!   September 29, 2005 - Tweaked wording of some comments.
!   November 10, 2005 - Included l2_qapmd array in L2G file-level Metadata.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A1C1
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History (Continued):
!   March 21, 2007 - Added NumberOfLinesMissingGeolocation Metadatum.
!   March 22, 2007 - Added FirstLineInOrbit and LastLineInOrbit Metadata.
!   March 23, 2007 - Increased number of characters in l2g_pgevers to 20.
!
!===============================================================================
 
SUBROUTINE OML2G_WriteL2GFileMeta(l2g_fileid, &
                                  l2_nfile, l2_orbit, l2_lrange, &
                                  l2_nlbad, l2_orbper, l2g_month, &
                                  l2g_day, l2g_year, l2g_dyofyr, &
                                  l2g_TAI93at0z, l2g_pgevers, &
                                  l2g_tstart, l2g_tend, l2_qapmd)

!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'PGS_TD_3.f'                ! Some Toolkit parameters.
  INCLUDE 'hdfeos5.inc'               ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_fileid          ! L2G file ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2_nfile            ! Number of L2 files.
  INTEGER (KIND=4), &
    DIMENSION(20), &
    INTENT(IN) :: l2_orbit            ! List of L2 orbit numbers.
  INTEGER (KIND=4), &
    DIMENSION(20,2), &
    INTENT(INOUT) :: l2_lrange        ! L2 orbital line ranges.
  INTEGER (KIND=4), &
    DIMENSION(20), &
    INTENT(IN) :: l2_nlbad            ! Numbers of "bad" lines in L2 files.
  REAL (KIND=8), &
    DIMENSION(20), &
    INTENT(IN) :: l2_orbper           ! Orbital periods of L2 granules.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_month           ! Month of L2G file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_day             ! Day of L2G file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_year            ! Year of L2G file.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_dyofyr          ! Day of year of L2G file.
  REAL (KIND=8), &
    INTENT(IN) :: l2g_TAI93at0z       ! TAI93 at 0z of L2G file.
  CHARACTER (LEN=20), &
    INTENT(IN) :: l2g_pgevers         ! L2G PGE version.
  REAL (KIND=8), &
    INTENT(IN) :: l2g_tstart          ! TAI93 at start of L2G file.
  REAL (KIND=8), &
    INTENT(IN) :: l2g_tend            ! TAI93 at end of L2G file.
  INTEGER (KIND=4), &
    DIMENSION(20), &
    INTENT(IN) :: l2_qapmd            ! QA percent missing data of L2 files.
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=8) :: count           ! Number of values in global attribute.
  INTEGER (KIND=4) :: l2_ifile        ! L2 file counter (1 to l2_nfile).
  CHARACTER (LEN=256) :: dummy        ! Dummy string variable.
  CHARACTER (LEN=28) :: l2g_startUTC  ! UTC at start of L2G file.
  CHARACTER (LEN=28) :: l2g_endUTC    ! UTC at end of L2G file.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat                ! Function call status.
  CHARACTER (LEN=256) :: stat_msg              ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_WriteL2GFileMeta.f90"    ! Code file reference.
 
!===============================================================================
! Declaration of PGS Toolkit function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: pgs_td_taitoutc       ! Converts TAI93 to UTC ascii string.
 
!===============================================================================
! Declaration of HDF-EOS 5 function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_ehwrglatt         ! Writes global attribute to file.
 
!===============================================================================
! Write file-level Metadata to L2G file:
!===============================================================================
 
!===============================================================================
! Write orbit numbers to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, "OrbitNumber", &
                            HE5T_NATIVE_INT, count, &
                            l2_orbit)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Test fill value for orbital first line numbers, and, if necessary, adjust.
!===============================================================================
  DO l2_ifile = 1, l2_nfile
    IF (l2_lrange(l2_ifile,1) == 200000000) l2_lrange(l2_ifile,1) = -200000000
  END DO
 
!===============================================================================
! Write orbital first line numbers to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, "FirstLineInOrbit", &
                            HE5T_NATIVE_INT, count, &
                            l2_lrange(1:l2_nfile,1))
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write orbital last line numbers to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, "LastLineInOrbit", &
                            HE5T_NATIVE_INT, count, &
                            l2_lrange(1:l2_nfile,2))
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write of "bad" lines to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, &
                            "NumberOfLinesMissingGeolocation", &
                            HE5T_NATIVE_INT, count, &
                            l2_nlbad)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write orbital periods to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, "OrbitPeriod", &
                            HE5T_NATIVE_DOUBLE, count, &
                            l2_orbper)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write instrument name to L2G file.
!===============================================================================
  dummy = "OMI"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l2g_fileid, "InstrumentName", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write process level to L2G file.
!===============================================================================
  dummy = "2G"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l2g_fileid, "ProcessLevel", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write granule month to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l2g_fileid, "GranuleMonth", &
                            HE5T_NATIVE_INT, count, &
                            l2g_month)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write granule day to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l2g_fileid, "GranuleDay", &
                            HE5T_NATIVE_INT, count, &
                            l2g_day)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write granule year to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l2g_fileid, "GranuleYear", &
                            HE5T_NATIVE_INT, count, &
                            l2g_year)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write granule day of year to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l2g_fileid, "GranuleDayOfYear", &
                            HE5T_NATIVE_INT, count, &
                            l2g_dyofyr)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write TAI93 at 0z of granule to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_ehwrglatt(l2g_fileid, "TAI93At0zOfGranule", &
                            HE5T_NATIVE_DOUBLE, count, &
                            l2g_TAI93at0z)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write PGE version to L2G file.
!===============================================================================
  count = LEN(TRIM(l2g_pgevers))
  call_stat = he5_ehwrglatt(l2g_fileid, "PGEVersion", &
                            HE5T_NATIVE_CHAR, count, &
                            l2g_pgevers)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Convert start TAI93 of granule to UTC.
!===============================================================================
  call_stat = pgs_td_taitoutc(l2g_tstart, l2g_startUTC)
  IF (call_stat .NE. 0) GO TO 6
 
!===============================================================================
! Write start UTC of granule to L2G file.
!===============================================================================
  count = LEN(TRIM(l2g_startUTC))
  call_stat = he5_ehwrglatt(l2g_fileid, "StartUTC", &
                            HE5T_NATIVE_CHAR, count, &
                            l2g_startUTC)
  IF (call_stat < 0) GO TO 2

!===============================================================================
! Convert end TAI93 of granule to UTC.
!===============================================================================
  call_stat = pgs_td_taitoutc(l2g_tend, l2g_endUTC)
  IF (call_stat .NE. 0) GO TO 6
 
!===============================================================================
! Write end UTC of granule to L2G file.
!===============================================================================
  count = LEN(TRIM(l2g_endUTC))
  call_stat = he5_ehwrglatt(l2g_fileid, "EndUTC", &
                            HE5T_NATIVE_CHAR, count, &
                            l2g_endUTC)
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write period of granule to L2G file.
!===============================================================================
  dummy = "Daily"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l2g_fileid, "Period", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 2
 
!===============================================================================
! Write QA percent missing data of L2 files to L2G file.
!===============================================================================
  count = l2_nfile
  call_stat = he5_ehwrglatt(l2g_fileid, &
                            "QAPercentMissingData", &
                            HE5T_NATIVE_INT, count, &
                            l2_qapmd)
  IF (call_stat < 0) GO TO 2


  ! write doi - MB 06/20/24
  dummy = "10.5067/AURA/OMI/DATA2425"
  count = LEN(TRIM(dummy))
  call_stat = he5_ehwrglatt(l2g_fileid, "identifier_product_doi", &
                            HE5T_NATIVE_CHAR, count, &
                            TRIM(dummy))
  IF (call_stat < 0) GO TO 2

!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN

!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_ehwrglatt, and so end execution with fatal error.
!===============================================================================
2 WRITE(stat_msg, 5) call_stat
5 FORMAT("Fatal error while writing file-level Metadatum:", &
         "  he5_ehwrglatt call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)

!===============================================================================
! This part of subroutine is reached only if function call status is not equal
! to zero for a call to pgs_td_taitoutc, and so end execution with fatal error.
!===============================================================================
6 WRITE(stat_msg, 8) call_stat
8 FORMAT("Fatal error while converting TAI93 to UTC:", &
         "  pgs_td_taitoutc call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML2G_WriteL2GFileMeta
