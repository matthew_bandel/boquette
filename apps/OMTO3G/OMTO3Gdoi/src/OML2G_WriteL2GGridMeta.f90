!===============================================================================
!
! SUBROUTINE OML2G_WriteL2GGridMeta
!
! High Level Overview:
!   This subroutine is called by OML2G_Main to write the grid-level Metadata to
!   the L2G product file.
!
! All Thirteen Arguments Are Inputs:
!   l2g_gridid          - L2G grid ID.
!   l2g_nlon            - Number of longitudes in L2G grid.
!   l2g_nlat            - Number of latitudes in L2G grid.
!   l2g_ngc             - Number of grid cells in L2G grid.
!   l2g_negc            - Number of empty grid cells in L2G grid.
!   l2g_npgc            - Number of populated grid cells in L2G grid.
!   l2g_nmpgc           - Number of multiply populated grid cells in L2G grid.
!   l2g_ncgcmin         - Minimum number of candidates per grid cell.
!   l2g_ncgcmax         - Maximum number of candidates per grid cell.
!   l2g_nscon           - Number of scenes considered for L2G grid.
!   l2g_nsacc           - Number of scenes accepted into L2G grid.
!   l2g_nsdup           - Number of duplicate scenes accepted into L2G grid.
!   l2g_nsrej           - Number of scenes rejected from L2G grid.
!
! OML2G Subroutine Called:
!   OML2G_EndInFailure  - Ends OML2G execution in failure.
!
! HDF-EOS 5 Function Called:
!   he5_gdwrattr        - Writes attribute to grid.
!
! Author:
!   Peter J.T. Leonard
!   Science Systems and Applications, Inc.
!   10210 Greenbelt Road, Suite 400
!   Lanham, Maryland 20706
!   U.S.A.
!
! Initial Version:
!   July 26, 2005
!
! Revision History:
!   November 10, 2005 - Included l2_qapmd array in L2G grid-level Metadata.
!   November 10, 2005 - Moved l2_qapmd array to L2G file-level Metadata.
!   May 24, 2006 - Renamed integer Metadatum Projection to GCTPProjectionCode,
!                  and added string Metadata named Projection and GridOrigin.
!
! Author (Updated):
!   Peter J.T. Leonard
!   ADNET Systems, Inc.
!   7515 Mission Drive, Suite A1C1
!   Lanham, Maryland 20706
!   U.S.A.
!
!   e-mail: pleonard@sesda2.com
!   Phone: (301) 352-4659
!
! Revision History (Continued):
!   March 22, 2007 - Added GridSpacing, GridSpacingUnit, GridSpan and
!                    GridSpanUnit to grid-level Metadata.
!
!===============================================================================
 
SUBROUTINE OML2G_WriteL2GGridMeta(l2g_gridid, &
                                  l2g_nlon, l2g_nlat, l2g_ngc, &
                                  l2g_negc, l2g_npgc, l2g_nmpgc, &
                                  l2g_ncgcmin, l2g_ncgcmax, l2g_nscon, &
                                  l2g_nsacc, l2g_nsdup, l2g_nsrej)
 
!===============================================================================
! Explicitly declare and describe all variables.
!===============================================================================
  IMPLICIT NONE
 
!===============================================================================
! Include files.
!===============================================================================
  INCLUDE 'hdfeos5.inc'         ! Declarations for HDF-EOS 5.
 
!===============================================================================
! Declarations of input arguments.
!===============================================================================
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_gridid    ! L2G grid ID.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlon      ! Number of longitudes in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nlat      ! Number of latitudes in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_ngc       ! Number of grid cells in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_negc      ! Number of empty grid cells in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_npgc      ! Number of populated grid cells in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nmpgc     ! Number of multiply populated cells in L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_ncgcmin   ! Minimum number of candidates per grid cell.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_ncgcmax   ! Maximum number of candidates per grid cell.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nscon     ! Number of scenes considered for L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nsacc     ! Number of scenes accepted into L2G grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nsdup     ! Number of duplicate scenes accepted into grid.
  INTEGER (KIND=4), &
    INTENT(IN) :: l2g_nsrej     ! Number of scenes rejected from L2G grid,
 
!===============================================================================
! Declarations of local variables.
!===============================================================================
  INTEGER (KIND=4) :: l2g_proj  ! GCTP projection used for grid.
  INTEGER (KIND=4) :: count     ! Number of values in grid-level attribute.
  CHARACTER (LEN=256) :: dummy  ! Dummy string variable.
 
!===============================================================================
! Messaging.
!===============================================================================
  INTEGER (KIND=4) :: call_stat              ! Function call status.
  CHARACTER (LEN=256) :: stat_msg            ! Status message.
  CHARACTER (LEN=256) :: &
    code_ref = "OML2G_WriteL2GGridMeta.f90"  ! Code file reference.
 
!===============================================================================
! Declaration of HDF-EOS 5 function call.
!===============================================================================
  INTEGER (KIND=4), &
    EXTERNAL :: he5_gdwrattr    ! Writes attribute to grid.
 
!===============================================================================
! Write grid-level attributes to L2G file:
!===============================================================================
!===============================================================================
! Write GCTP projection code to L2G file.
!===============================================================================
  l2g_proj = HE5_GCTP_GEO
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GCTPProjectionCode", &
                           HE5T_NATIVE_INT, count, l2g_proj)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write projection to L2G file.
!===============================================================================
  dummy = "Geographic"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "Projection", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write grid origin to L2G file.
!===============================================================================
  dummy = "Center"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GridOrigin", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write grid spacing to L2G file.
!===============================================================================
  dummy = "(0.25,0.25)"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GridSpacing", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write grid spacing unit to L2G file.
!===============================================================================
  dummy = "deg"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GridSpacingUnit", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write grid span to L2G file.
!===============================================================================
  dummy = "(-180,180,-90,90)"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GridSpan", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write grid span unit to L2G file.
!===============================================================================
  dummy = "deg"
  count = LEN(TRIM(dummy))
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "GridSpanUnit", &
                           HE5T_NATIVE_CHAR, count, TRIM(dummy))
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of longitudes in grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfLongitudesInGrid", &
                           HE5T_NATIVE_INT, count, l2g_nlon)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of latitudes in grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfLatitudesInGrid", &
                           HE5T_NATIVE_INT, count, l2g_nlat)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of grid cells to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfGridCells", &
                           HE5T_NATIVE_INT, count, l2g_ngc)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of empty grid cells to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfEmptyGridCells", &
                           HE5T_NATIVE_INT, count, l2g_negc)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of populated grid cells to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfPopulatedGridCells", &
                           HE5T_NATIVE_INT, count, l2g_npgc)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of multiply populated grid cells to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfMultiplyPopulatedGridCells", &
                           HE5T_NATIVE_INT, count, l2g_nmpgc)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write minimum number of candidates per grid cell to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "MinimumNumberOfCandidatesPerGridCell", &
                           HE5T_NATIVE_INT, count, l2g_ncgcmin)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write maximum number of candidates per grid cell to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "MaximumNumberOfCandidatesPerGridCell", &
                           HE5T_NATIVE_INT, count, l2g_ncgcmax)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of scenes considered for grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfScenesConsideredForGrid", &
                           HE5T_NATIVE_INT, count, l2g_nscon)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of scenes accepted into grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfScenesAcceptedIntoGrid", &
                           HE5T_NATIVE_INT, count, l2g_nsacc)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of duplicate scenes accepted into grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfDuplicateScenesAcceptedIntoGrid", &
                           HE5T_NATIVE_INT, count, l2g_nsdup)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Write number of scenes rejected from grid to L2G file.
!===============================================================================
  count = 1
  call_stat = he5_gdwrattr(l2g_gridid, &
                           "NumberOfScenesRejectedFromGrid", &
                           HE5T_NATIVE_INT, count, l2g_nsrej)
  IF (call_stat < 0) GO TO 5
 
!===============================================================================
! Return to calling code.
!===============================================================================
  RETURN
 
!===============================================================================
! This part of subroutine is reached only if function call status is less than
! zero for a call to he5_gdwrattr, and so end execution with fatal error.
!===============================================================================
5 WRITE(stat_msg, 8) call_stat
8 FORMAT("Fatal error while writing grid-level Metadatum:", &
         "  he5_gdwrattr call status =",i10,"!")
  CALL OML2G_EndInFailure(stat_msg, code_ref)
 
END SUBROUTINE OML2G_WriteL2GGridMeta
