!!****************************************************************************
!!F90
!
!!Description:
!
! MODULE O3T_radgeo_class
! 
! This module contains the allocatable arrays used to store in GEOlocation
! inforamtion and Earth view RADiance information retrived from the input
! L1B file. 
!
!!Revision History:
! Initial version 03/26/2002  Kai Yang/UMBC
!
!!Team-unique Header:
! This software was developed by the OMI Science Team Support
! Group for the National Aeronautics and Space Administration, Goddard
! Space Flight Center, under NASA Task 916-003-1
!
!!References and Credits
! Written by 
! Kai Yang 
! University of Maryland Baltimore County
! email: Kai.Yang-1@nasa.gov
! 
!!Design Notes
!
!!END
!!****************************************************************************
MODULE O3T_L1B_class
    USE PGS_PC_class ! define PGSd_PC_FILE_PATH_MAX, and pgs_pc functions 
                     ! include PGS_SMF.f define PGS_SMF_MAX_MSG_SIZE
    ! Use netCDF4 L1B Reader, instead of HE4 reader
    ! USE L1B_class    ! L1B_class contains module to read OMI L1B
                     ! geolocation, radiance, and irradiance parameters
    USE L1B_Reader_class
    USE L1B_radirr_class

    ! using adapted version
!    USE O3T_radgeo_class
    USE ISO_C_BINDING, ONLY: C_LONG

    ! adding supplementary geoang class = MB 9/18/23
    use L1B_geoang_class
    use L1B_radirr_class
    use O3T_radgeo_class

    IMPLICIT NONE
    TYPE (L1B_block_type) :: L1B_blk, L1B_blk_VIS

    ! defining geoang_blk - MB 9/18/23
    TYPE (L1B_geoang_type) :: geoang_blk
    TYPE (L1B_radirr_type) :: radiance_blk

    INTEGER (KIND=4), PARAMETER, PRIVATE :: zero = 0, one = 1, two = 2
    INTEGER (KIND=4), PARAMETER, PRIVATE :: three = 3, four =4, five = 5
    INTEGER, PARAMETER, PRIVATE ::   OMI_S_SUCCESS = 15565312
    INTEGER, PARAMETER, PRIVATE ::   OMI_E_FAILURE = 15568385

    PUBLIC  :: O3T_L1B_initRAD
    PUBLIC  :: O3T_L1B_freeRAD
    ! removing VIS references - MB 9/15/23
!    PUBLIC  :: O3T_L1B_initRAD_VIS
!    PUBLIC  :: O3T_L1B_freeRAD_VIS


    CONTAINS
       FUNCTION O3T_L1B_initRAD( L1B_filename, L1B_bandname, L1B_modename, wl_com ) &
                RESULT( status )
         USE OMI_LUN_set      ! define Logical Unit for Input and Output
         USE OMI_SMF_class    ! include PGE specific messages and OMI_SMF_setmsg
         CHARACTER( LEN = * ), INTENT(IN) :: L1B_filename, L1B_bandname, &
                L1B_modename
         REAL (KIND = 4), DIMENSION(:), INTENT(IN), OPTIONAL :: wl_com
         CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE  ) :: msg
         INTEGER (KIND=4) :: iLine, line
         INTEGER (KIND=4) :: version
         INTEGER (KIND=4) :: status, ierr
         integer (kind=4) :: reference

         !! clean up everything before proceed
         ! CALL O3T_L1B_freeRAD
         ! EarthSunDistance = L1Bga_EarthSunDistance( L1B_filename,L1B_swathname )
         ! making block size 1000 MB 8/29/23
         status = L1Br_open( L1B_blk, L1B_filename, L1B_bandname, L1B_modename, nL=1000 )
         ! status = L1Bga_open( geo_blk, L1B_filename, L1B_bandname )
         IF( status /= OMI_S_SUCCESS ) THEN
            WRITE( msg,'(A)' ) "L1Bga_open "// TRIM(L1B_bandname) //&
                               TRIM(L1B_modename) // &
                               " in file " // TRIM(L1B_filename) // " failed."
            ierr = OMI_SMF_setmsg( OZT_E_INPUT, msg, "O3T_L1B_initRAD", zero )
            status = OZT_E_FAILURE
            RETURN 
         ENDIF

         ! obtain sizes of dimensions defined in swath
         status = L1Br_getSWdims( L1B_blk, nTimes_rad, nXtrack_rad, &
                                   nTimesSmallPixel_rad, &
                                   nWavel_rad, nWavelCoef_rad )
         IF( status /= OMI_S_SUCCESS ) THEN
            ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
                                "L1Br_getSWdims failed.", "O3T_L1B_initRAD", zero )
            status = OZT_E_FAILURE
            RETURN
         ELSE
            WRITE( msg,'(A,4I4)' ) "(nTimes, nXtract, nWavel, nWavelCoef) =", &
                         nTimes_rad, nXtrack_rad, nWavel_rad, nWavelCoef_rad
            ierr = OMI_SMF_setmsg( OMI_S_SUCCESS, msg, "O3T_L1B_initRAD", four )
         ENDIF


         ! moving allocationn block - MB 9/18/23
          ! allocate memory for arrays
         ALLOCATE( latitude( nXtrack_rad ), &
                   longitude( nXtrack_rad ), &
                   szenith( nXtrack_rad ), &
                   sazimuth( nXtrack_rad ), &
                   vzenith( nXtrack_rad ), &
                   vazimuth( nXtrack_rad ), &
                   phiArray( nXtrack_rad ), &
                   ptArray( nXtrack_rad ), &
                   pcArray( nXtrack_rad ), &
!                   rcfArrayCloud( nXtrack_rad), &
!                   ptArrayCloud( nXtrack_rad), &
                   PclimQ( nXtrack_rad ), &
                   snowIceArray( nXtrack_rad ), &
                   height( nXtrack_rad ), &
                   geoflg( nXtrack_rad ), &
                   trackflg( nXtrack_rad ), &
                   anomflg( nXtrack_rad ), &
                   radiance( nWavel_rad, nXtrack_rad ), &
                   radPrecision( nWavel_rad, nXtrack_rad ), &
                   radQAflags( nWavel_rad, nXtrack_rad ), &
                   radWavelength( nWavel_rad, nXtrack_rad ), &
                   radCoefficient( nWavelCoef_rad, nXtrack_rad ), &
                   ! addig small pixel radiance - MB 03/28/24
                    small_pixel( nTimesSmallPixel_rad, nXtrack_rad ), &
!                   l1b_spec_qual( nWavel_rad, nXtrack_rad ), &
!                   l1b_qual_level( nWavel_rad, nXtrack_rad ), &
!                   rad_water_fraction( nXtrack_rad ), &
                   STAT=ierr )

         ! obtain earthsundistance for irradiance file
         status =  L1Br_getGEOline(L1B_blk, 1, &
           EarthSunDistance_k=EarthSunDistance)
         IF( status /= OMI_S_SUCCESS ) THEN
           ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
             "Obtaining SunEarthCorrection failed.","O3T_L1B_initRAD", zero )
            status = OZT_E_FAILURE
            RETURN
          ENDIF

         ! allocate geoang
         status = Allocate_geoang(geoang_blk, nTimes_rad, nXtrack_rad)

         ! allocate radiance
         status = Allocate_radiance(radiance_blk, nTimes_rad, nXtrack_rad, nWavel_rad, nWavelCoef_rad, &
                nTimesSmallPixel_rad)

         ! for each time step
         do line = 1, nTimes_rad

             ! get measurement quality flag
!             status = L1Br_getDATA(L1B_blk, line, MeasurementQualityFlags_k=mqa_rad, &
!                    InstrumentConfigurationId_k=instId_rad)
             ! get measurement quality flag, exposure, master clock period - MB 03/28/24
             status = L1Br_getDATA(L1B_blk, line, MeasurementQualityFlags_k=mqa_rad, &
                    InstrumentConfigurationId_k=instId_rad, ExposureTime_k=exposure, &
                    MasterClockPeriod_k=clock)

             ! get geo coordinates - MB 9/19/23
             status =  L1Br_getGEOline(L1B_blk, line, &
               EarthSunDistance_k=EarthSunDistance, Longitude_k=longitude, &
                     Latitude_k=latitude, SolarZenithAngle_k=szenith, SolarAzimuthAngle_k=sazimuth, &
                    ViewingZenithAngle_k=vzenith, ViewingAzimuthAngle_k=vazimuth, &
                    SpacecraftLatitude_k=scLat, SpacecraftLongitude_k=scLon, SpacecraftAltitude_k=scHgt, &
                    Time_k=time, SecondsInDay_k=sInD, TerrainHeight_k=height, &
                    GroundPixelQualityFlags_k=geoflg, XTrackQualityFlags_k=trackflg)
             IF( status /= OMI_S_SUCCESS ) THEN
               ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
                 "Obtaining SunEarthCorrection failed.","O3T_L1B_initRAD", zero )
                status = OZT_E_FAILURE
                RETURN
              ENDIF

             ! transfer to geoang - MB 9/18/23
             status = Transfer_geoang(geoang_blk, line, latitude, longitude, szenith, sazimuth, &
                vzenith, vazimuth, scLat, scLon, scHgt, time, sInD, height, geoflg, trackflg) ;

             ! get radiance data - MB 9/19/23
             status =  L1Br_getSIGline(L1B_blk, line, SpectralQuality_k=radQAflags, &
                    Signal_k=radiance, WavelengthCoefficient_k=radCoefficient, &
                     WavelengthReferenceColumn_k=reference, SignalError_k=radPrecision)
             IF( status /= OMI_S_SUCCESS ) THEN
               ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
                 "Obtaining SunEarthCorrection failed.","O3T_L1B_initRAD", zero )
                status = OZT_E_FAILURE
                RETURN
              ENDIF

             ! set dummy value for small pixel column - MB 03/28/24
             small_pixel_column = 0

             ! transfer to radiance - MB 9/19/23
!             status = Transfer_radiance(radiance_blk, line, mqa_rad, radiance, radWavelength, &
!                radCoefficient, reference, radprecision, radQAflags, instId_rad) ;
             ! transfer to radiance, extended arguments - MB 03/28/24
             status = Transfer_radiance(radiance_blk, line, mqa_rad, radiance, radWavelength, &
                radCoefficient, reference, radprecision, radQAflags, instId_rad, exposure, &
                clock, small_pixel_column) ;


         enddo ! line loop


         IF( PRESENT( wl_com ) ) THEN
            nWavel_rad = SIZE( wl_com )
            WRITE( msg,'(A,I3)' ) "Common array of fixed wavelength used, " //&
                               "nWavel_rad = ", nWavel_rad
            ierr = OMI_SMF_setmsg( OZT_W_GENERAL, msg, "O3T_L1B_initRAD", four )
         ENDIF

         IF( ierr /= zero ) THEN
            ierr = OMI_SMF_setmsg( OZT_E_MEM_ALLOC, &
                                  "radiance and geo allocation failure", &
                                  "O3T_L1B_initRAD", zero )
            status = OZT_E_FAILURE
            RETURN
         ENDIF
       END FUNCTION O3T_L1B_initRAD

       SUBROUTINE O3T_L1B_freeRAD
         INTEGER (KIND=4) :: status
         IF( ALLOCATED( latitude      ) ) DEALLOCATE( latitude      )
         IF( ALLOCATED( longitude     ) ) DEALLOCATE( longitude     )
         IF( ALLOCATED( szenith       ) ) DEALLOCATE( szenith       )
         IF( ALLOCATED( sazimuth      ) ) DEALLOCATE( sazimuth      )
         IF( ALLOCATED( vzenith       ) ) DEALLOCATE( vzenith       )
         IF( ALLOCATED( vazimuth      ) ) DEALLOCATE( vazimuth      )
         IF( ALLOCATED( phiArray      ) ) DEALLOCATE( phiArray      )
         IF( ALLOCATED( ptArray       ) ) DEALLOCATE( ptArray       )
         IF( ALLOCATED( pcArray       ) ) DEALLOCATE( pcArray       )
!         IF( ALLOCATED( ptArrayCloud  ) ) DEALLOCATE( ptArrayCloud  )
!         IF( ALLOCATED( rcfArrayCloud ) ) DEALLOCATE( rcfArrayCloud )
         IF( ALLOCATED( PclimQ        ) ) DEALLOCATE( PclimQ        )
         IF( ALLOCATED( snowIceArray  ) ) DEALLOCATE( snowIceArray  )
         IF( ALLOCATED( height        ) ) DEALLOCATE( height        )
         IF( ALLOCATED( geoflg        ) ) DEALLOCATE( geoflg        )
         IF( ALLOCATED( anomflg       ) ) DEALLOCATE( anomflg       )
         IF( ALLOCATED( radiance      ) ) DEALLOCATE( radiance      )
         IF( ALLOCATED( radPrecision  ) ) DEALLOCATE( radPrecision  )
         IF( ALLOCATED( radWavelength ) ) DEALLOCATE( radWavelength )
         IF( ALLOCATED( radQAflags    ) ) DEALLOCATE( radQAflags    )
!         IF( ALLOCATED( l1b_spec_qual ) ) DEALLOCATE( l1b_spec_qual )
!         IF( ALLOCATED( l1b_qual_level ) ) DEALLOCATE( l1b_qual_level )
!         IF( ALLOCATED( rad_water_fraction ) ) DEALLOCATE( rad_water_fraction )


         ! close data block structure
         ! status = L1Bga_close( geo_blk )
         status = L1Br_close( L1B_blk )
       END SUBROUTINE O3T_L1B_freeRAD

       !////////////////////////////////////////////////////////////////////////
       !//
       !// [CHANGE-JYL 9/9/2013]: added subroutine to handle VIS
       !//
       !////////////////////////////////////////////////////////////////////////

!       FUNCTION O3T_L1B_initRAD_VIS( L1B_filename, L1B_bandname, L1B_modename) &
!                RESULT( status )
!         USE OMI_LUN_set      ! define Logical Unit for Input and Output
!         USE OMI_SMF_class    ! include PGE specific messages and OMI_SMF_setmsg
!         CHARACTER( LEN = * ), INTENT(IN) :: L1B_filename, L1B_bandname, &
!                L1B_modename
!!//      REAL (KIND = 4), DIMENSION(:), INTENT(IN), OPTIONAL :: wl_com
!         CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE  ) :: msg
!         INTEGER (KIND=4) :: iLine
!         INTEGER (KIND=4) :: version
!         INTEGER (KIND=4) :: status, ierr
!
!         !! clean up everything before proceed
!         ! CALL O3T_L1B_freeRAD_VIS
!
!         ! EarthSunDistance_VIS = L1Bga_EarthSunDistance( L1B_filename, &
!         !                                                L1B_bandname )
!         status = L1Br_open( L1B_blk_VIS, L1B_filename, L1B_bandname, &
!                              L1B_modename, nL=1000)
!         IF( status /= OMI_S_SUCCESS ) THEN
!            WRITE( msg,'(A)' ) "L1Br_open "// TRIM(L1B_bandname) //&
!                               TRIM(L1B_modename) // &
!                               " in file " // TRIM(L1B_filename) // " failed."
!            ierr = OMI_SMF_setmsg( OZT_E_INPUT, msg, "O3T_L1B_initRAD_VIS", zero )
!            status = OZT_E_FAILURE
!            RETURN
!         ENDIF
!
!
!
!         ! obtain sizes of dimensions defined in swath
!         status = L1Br_getSWdims( L1B_blk_VIS, &
!                                   nTimes_rad_VIS, nXtrack_rad_VIS, &
!                                   nTimesSmallPixel_rad_VIS, &
!                                   nWavel_rad_VIS, nWavelCoef_rad_VIS)
!         IF( status /= OMI_S_SUCCESS ) THEN
!            ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
!                      "L1Bri_getSWdims failed.", "O3T_L1B_initRAD_VIS", zero )
!            status = OZT_E_FAILURE
!            RETURN
!         ELSE
!            WRITE( msg,'(A,4I4)' ) "(nTimes, nXtract, nWavel, nWavelCoef) =", &
!                         nTimes_rad, nXtrack_rad, nWavel_rad, nWavelCoef_rad
!            ierr = OMI_SMF_setmsg( OMI_S_SUCCESS, msg, "O3T_L1B_initRAD_VIS", four )
!         ENDIF
!
!         ! obtain earthsundistance for irradiance file
!         status =  L1Br_getGEOline(L1B_blk_VIS, 1, &
!           EarthSunDistance_k=EarthSunDistance_VIS)
!         IF( status /= OMI_S_SUCCESS ) THEN
!           ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
!             "Obtaining SunEarthCorrection failed.","O3T_L1B_initRAD", zero )
!            status = OZT_E_FAILURE
!            RETURN
!          ENDIF
!
!!//      IF( PRESENT( wl_com ) ) THEN
!!//         nWavel_rad = SIZE( wl_com )
!!//         WRITE( msg,'(A,I3)' ) "Common array of fixed wavelength used, " //&
!!//                            "nWavel_rad = ", nWavel_rad
!!//         ierr = OMI_SMF_setmsg( OZT_W_GENERAL, msg, "O3T_L1B_initRAD", four )
!!//      ENDIF
!
!
!         ! allocate memory for arrays. Some may not be needed, but that is
!         ! okay. No harm done.
!
!         ALLOCATE( latitude_VIS( nXtrack_rad_VIS ),  &
!                   longitude_VIS( nXtrack_rad_VIS ), &
!                   szenith_VIS( nXtrack_rad_VIS ),   &
!                   sazimuth_VIS( nXtrack_rad_VIS ),  &
!                   vzenith_VIS( nXtrack_rad_VIS ),   &
!                   vazimuth_VIS( nXtrack_rad_VIS ),  &
!                   phiArray_VIS( nXtrack_rad_VIS ),  &
!                   ptArray_VIS( nXtrack_rad_VIS ),   &
!                   pcArray_VIS( nXtrack_rad_VIS ),   &
!                   PclimQ_VIS( nXtrack_rad_VIS ),    &
!                   snowIceArray_VIS( nXtrack_rad_VIS ), &
!                   height_VIS( nXtrack_rad_VIS ),  &
!                   geoflg_VIS( nXtrack_rad_VIS ),  &
!                   anomflg_VIS( nXtrack_rad_VIS ), &
!                   radiance_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ),      &
!                   radPrecision_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ),  &
!                   radQAflags_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ),    &
!                   radWavelength_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ), &
!                   l1b_spec_qual_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ), &
!                   l1b_qual_level_VIS( nWavel_rad_VIS, nXtrack_rad_VIS ), &
!                   rad_water_fraction_VIS( nXtrack_rad ), &
!                   STAT=ierr )
!
!         IF( ierr /= zero ) THEN
!            ierr = OMI_SMF_setmsg( OZT_E_MEM_ALLOC, &
!                                  "radiance and geo allocation failure", &
!                                  "O3T_L1B_initRAD_VIS", zero )
!            status = OZT_E_FAILURE
!            RETURN
!         ENDIF
!       END FUNCTION O3T_L1B_initRAD_VIS


!       SUBROUTINE O3T_L1B_freeRAD_VIS
!         INTEGER (KIND=4) :: status
!         IF( ALLOCATED( latitude_VIS      ) ) DEALLOCATE( latitude_VIS  )
!         IF( ALLOCATED( longitude_VIS     ) ) DEALLOCATE( longitude_VIS )
!         IF( ALLOCATED( szenith_VIS       ) ) DEALLOCATE( szenith_VIS   )
!         IF( ALLOCATED( sazimuth_VIS      ) ) DEALLOCATE( sazimuth_VIS  )
!         IF( ALLOCATED( vzenith_VIS       ) ) DEALLOCATE( vzenith_VIS   )
!         IF( ALLOCATED( vazimuth_VIS      ) ) DEALLOCATE( vazimuth_VIS  )
!         IF( ALLOCATED( phiArray_VIS      ) ) DEALLOCATE( phiArray_VIS  )
!         IF( ALLOCATED( ptArray_VIS       ) ) DEALLOCATE( ptArray_VIS   )
!         IF( ALLOCATED( pcArray_VIS       ) ) DEALLOCATE( pcArray_VIS   )
!         IF( ALLOCATED( PclimQ_VIS        ) ) DEALLOCATE( PclimQ_VIS    )
!         IF( ALLOCATED( snowIceArray_VIS  ) ) DEALLOCATE( snowIceArray_VIS )
!         IF( ALLOCATED( height_VIS        ) ) DEALLOCATE( height_VIS    )
!         IF( ALLOCATED( geoflg_VIS        ) ) DEALLOCATE( geoflg_VIS    )
!         IF( ALLOCATED( anomflg_VIS       ) ) DEALLOCATE( anomflg_VIS   )
!         IF( ALLOCATED( radiance_VIS      ) ) DEALLOCATE( radiance_VIS  )
!         IF( ALLOCATED( radPrecision_VIS  ) ) DEALLOCATE( radPrecision_VIS  )
!         IF( ALLOCATED( radWavelength_VIS ) ) DEALLOCATE( radWavelength_VIS )
!         IF( ALLOCATED( radQAflags_VIS    ) ) DEALLOCATE( radQAflags_VIS    )
!         IF( ALLOCATED( l1b_spec_qual_VIS ) ) DEALLOCATE( l1b_spec_qual_VIS )
!         IF( ALLOCATED( l1b_qual_level_VIS ) ) DEALLOCATE( l1b_qual_level_VIS )
!         IF( ALLOCATED( rad_water_fraction_VIS ) ) DEALLOCATE( rad_water_fraction_VIS )
!
!         ! close data block structure
!         ! status = L1Bga_close( geo_blk_VIS )
!         status = L1Br_close( L1B_blk_VIS )
!       END SUBROUTINE O3T_L1B_freeRAD_VIS


END MODULE O3T_L1B_class
