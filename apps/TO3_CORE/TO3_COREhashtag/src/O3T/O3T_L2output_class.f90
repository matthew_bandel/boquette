! 
! Functions to allocate memory for storage of L2 results and transfer data to output 
! block. Set fill values if there are no L2 results. 
!
! last modified: Aug 1, 2019  Dave Haffner (SSAI)
! purpose:  update field names
!
! initial version: March 26, 2002  Kai Yang (UMBC)
!
!!****************************************************************************
MODULE O3T_L2output_class
    USE OMI_SMF_class ! include PGE specific messages and OMI_SMF_setmsg
    USE O3T_radgeo_class
    USE OMI_L2writer_class
    USE O3T_omto3_fs
    USE O3T_pixel_class
    USE ISO_C_BINDING, ONLY: C_LONG

    IMPLICIT NONE

    INTEGER (KIND=4), PARAMETER, PRIVATE :: zero = 0, one = 1, two = 2
    INTEGER (KIND=4), PARAMETER, PRIVATE :: three = 3, four =4, five = 5

    REAL    (KIND = 4), DIMENSION(:), ALLOCATABLE :: res_stp1, &
                                                     res_stp2, res_stp3, &
                                                     dndomega_t, dNdT, &
                                                     dndr

    REAL    (KIND = 4), DIMENSION(:), ALLOCATABLE, TARGET :: xnvalm

    INTEGER (KIND = 2), DIMENSION(:,:), ALLOCATABLE :: radQAflags_com, &
                                                       irrQAflags_com
    REAL    (KIND = 4), DIMENSION(:,:), ALLOCATABLE :: radPrecision_com, &
                                                       irrPrecision_com

    PUBLIC :: O3T_initL2out
    PUBLIC :: O3T_L2setGeoLine
    PUBLIC :: O3T_L2setDataPix
    PUBLIC :: O3T_L2fillDataPix
    PUBLIC :: O3T_L2setMqaLine
    PUBLIC :: O3T_freeL2out

    CONTAINS
       FUNCTION O3T_initL2out( wl_com ) RESULT( status )
         REAL (KIND = 4), DIMENSION(:), INTENT(IN), OPTIONAL :: wl_com
         CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE  ) :: msg
         INTEGER (KIND=4) :: status, ierr
         INTEGER (KIND=4) :: nwl_com

         status = OZT_S_SUCCESS
         IF( PRESENT( wl_com ) ) THEN
            nwl_com = SIZE( wl_com )
         ELSE
            nwl_com = nWavel_rad
         ENDIF

         CALL O3T_freeL2out
         ALLOCATE( radQAflags_com( nwl_com, nXtrack_rad ), &
                   irrQAflags_com( nwl_com, nXtrack_rad ), &
                   radPrecision_com( nwl_com, nXtrack_rad ), &
                   irrPrecision_com( nwl_com, nXtrack_rad ), &
                   xnvalm( nwl_com ), &
                   res_stp1( nwl_com ), &
                   res_stp2( nwl_com ), &
                   res_stp3( nwl_com ), &
                   dndomega_t( nwl_com ), &
                   dNdT( nwl_com ), &
                   dndr( nwl_com ), &
                   STAT=ierr )
        IF( ierr /= zero ) THEN
            ierr = OMI_SMF_setmsg( OZT_E_MEM_ALLOC, &
                                  "L2out allocation failure", &
                                  "O3T_initL2out", zero )
            status = OZT_E_FAILURE
            RETURN
         ENDIF
         RETURN
 
       END FUNCTION O3T_initL2out

       SUBROUTINE O3T_freeL2out
         IF( ALLOCATED( radQAflags_com   ) ) DEALLOCATE( radQAflags_com   )
         IF( ALLOCATED( irrQAflags_com   ) ) DEALLOCATE( irrQAflags_com   )
         IF( ALLOCATED( radPrecision_com ) ) DEALLOCATE( radPrecision_com )
         IF( ALLOCATED( irrPrecision_com ) ) DEALLOCATE( irrPrecision_com )
         IF( ALLOCATED( xnvalm           ) ) DEALLOCATE( xnvalm           )
         IF( ALLOCATED( res_stp1         ) ) DEALLOCATE( res_stp1         )
         IF( ALLOCATED( res_stp2         ) ) DEALLOCATE( res_stp2         )
         IF( ALLOCATED( res_stp3         ) ) DEALLOCATE( res_stp3         )
         IF( ALLOCATED( dndomega_t       ) ) DEALLOCATE( dndomega_t       )
         IF( ALLOCATED( dNdT             ) ) DEALLOCATE( dNdT             )
         IF( ALLOCATED( dndr             ) ) DEALLOCATE( dndr             )
       END SUBROUTINE O3T_freeL2out

       SUBROUTINE O3T_L2setGeoLine( iT, geoblk, datablk ) 
         TYPE (L2_generic_type), INTENT( INOUT ) :: geoblk, datablk
         INTEGER (KIND=4), INTENT(IN) :: iT
         INTEGER (KIND=4) :: ig, id, Ls, Le, bsize
         REAL (KIND=4), DIMENSION(nXtrack_rad) :: R4Array

         DO ig = 1, geoblk%nFields
           bsize = geoblk%lineSize(ig)
           Ls    = geoblk%accuBlkSize(ig-1) + (iT-1)*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1       !! fortran index scheme

           IF(      ig == 1 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( geoflg,    I1, bsize )
           ELSE IF( ig == 2 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( latitude,  I1, bsize )
           ELSE IF( ig == 3 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( longitude, I1, bsize )
           ELSE IF( ig == 4 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( szenith,   I1, bsize )
           ELSE IF( ig == 5 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( sazimuth,  I1, bsize )
           ELSE IF( ig == 6 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( vzenith,   I1, bsize )
           ELSE IF( ig == 7 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( vazimuth,  I1, bsize )
           ELSE IF( ig == 8 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( phiArray,  I1, bsize )
           ELSE IF( ig == 9 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( height,    I1, bsize )
           ELSE IF( ig == 10 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( time,      I1, bsize )
           ELSE IF( ig == 11 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( sInD,      I1, bsize )
           ELSE IF( ig == 12 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( scLat,     I1, bsize )
           ELSE IF( ig == 13 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( scLon,     I1, bsize )
           ELSE IF( ig == 14 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( scHgt,     I1, bsize )
           ELSE IF( ig == 15 ) THEN
              geoblk%data( Ls:Le ) = TRANSFER( anomflg,   I1, bsize)
           ENDIF
         ENDDO

         ! adding scene pressure to output - MB 02/04/24
!         DO id = 1, 2
         DO id = 1, 3
           bsize = datablk%lineSize(id)
           Ls    = datablk%accuBlkSize(id-1) + (iT-1)*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1       !! fortran index scheme
           IF( id == 1 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( pcArray,  I1, bsize )
           ELSE IF( id == 2 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( ptArray,  I1, bsize )
           ELSE IF( id == 3 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( psArray,  I1, bsize )
           ENDIF
         ENDDO

       END SUBROUTINE O3T_L2setGeoLine
  

       SUBROUTINE O3T_L2setDataPix( iT, iX, nWavel, nLayers, nWavelRet, &
                                    algflg, QAflags, &
                                    ler1, ler2, ler3, fc, stp1oz, domega_temp, stp2oz, err3, &
                                    stp3oz, dfs, aprftm, dndx, x2, w2, xa, sx, x3, w3, &
                                    err2, &
                                    cldoz, &
                                    Gc, &
                                    Sret, noise,  &
                                    datablk )

         TYPE (L2_generic_type), INTENT(INOUT) :: datablk
         INTEGER (KIND=4), INTENT(IN) :: iT, iX, nWavel, nLayers, nWavelRet
         REAL (KIND=4), DIMENSION(:), INTENT(IN) ::  x2, x3, w2, w3, xa, aprftm
         REAL (KIND=4), DIMENSION(nWavelRet), INTENT(IN) :: noise
         REAL (KIND=4), DIMENSION(:,:), INTENT(IN) :: sx, Sret
         REAL (KIND=4), DIMENSION(:,:), INTENT(IN) :: dndx
         REAL (KIND=4), DIMENSION(:), INTENT(IN) :: Gc
         INTEGER (KIND=1), INTENT(IN) :: algflg
         INTEGER (KIND=2), INTENT(IN) :: QAflags

         REAL (KIND=4), INTENT(IN) :: stp1oz, domega_temp, stp2oz, err3, dfs, ler1, ler2, ler3, stp3oz
         REAL (KIND=4), INTENT(IN) :: fc
         REAL (KIND=4), INTENT(IN) :: err2, cldoz
         REAL (KIND=4) :: measurement_noise
         INTEGER (KIND=4) :: id, is, ie, Ls, Le, bsize, ilyr
         INTEGER (KIND=4) :: LL
         INTEGER (KIND=1) :: I1temp
         INTEGER (KIND=2) :: I2temp
         REAL    (KIND=4) :: R4temp
         REAL    (KIND=4), DIMENSION( nWavelRet ) :: R4wlretArray
         REAL    (KIND=4), DIMENSION( nWavel  ) :: R4wlArray 
         REAL    (KIND=4), DIMENSION( nLayers ) :: R4lyrArray
         REAL(KIND=4), DIMENSION(nWavelRet, NLayers) :: R4wlRetLyrArray
         INTEGER (KIND=4) :: i
         REAL (KIND=4), DIMENSION( nLayers ) :: sx_diag, ret_cov_diag
         INTEGER (KIND=4) :: iwl_fc1, iwl_fc2

         ! added ScenePressure to position three, so all indices here get advanced by 1
!         DO id = 3, 30
         DO id = 4, 31
           LL    = (iT-1)*nXtrack_rad + (iX-1)     !! 0 based index
           bsize = datablk%pixSize(id)
           Ls    = datablk%accuBlkSize(id-1) + LL*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1       !! fortran index scheme
           IF(      id == 4 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( algflg,        I1, bsize )
           ELSE IF( id == 5 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( QAflags,       I1, bsize )
           ELSE IF( id == 6 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( dfs,   I1, bsize )
           ELSE IF( id == 7 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( err3, I1, bsize ) 
           ELSE IF( id == 8 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( fc, I1, bsize ) 
           ELSE IF( id == 9 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( stp3oz, I1, bsize)
           ELSE IF( id == 10 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( cldoz, I1, bsize ) 
           ELSE IF( id == 11) THEN
              datablk%data( Ls:Le ) = TRANSFER( ler1, I1, bsize )
           ELSE IF( id == 12 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( ler2, I1, bsize )
           ELSE IF( id == 13 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( ler3, I1, bsize )              
           ELSE IF( id == 14 ) THEN
              measurement_noise = SQRT(SUM((noise*Gc)**2))  
              datablk%data( Ls:Le ) = TRANSFER( measurement_noise, I1, bsize )
           ELSE IF( id == 15 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( stp1oz, I1, bsize )
           ELSE IF( id == 16 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( stp2oz, I1, bsize )
           ELSE IF( id == 17 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( err2, I1, bsize )
           ELSE IF( id == 18 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( dndr, I1, bsize )
           ELSE IF( id == 19 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( xnvalm*100., I1, bsize )
           ELSE IF( id == 20 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( res_stp1, I1, bsize )
           ELSE IF( id == 21 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( res_stp2, I1, bsize )
           ELSE IF( id == 22 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( dndomega_t, I1, bsize )
           ELSE IF( id == 23 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( dndt, I1, bsize )
           ELSE IF( id == 24 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( xa,     I1, bsize )
           ELSE IF( id == 25 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( w3,        I1, bsize )
           ELSE IF( id == 26 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( aprftm,        I1, bsize )
           ELSE IF( id == 27 ) THEN
              sx_diag = (/ ( sx(i,i), i=1,11 ) /)
              datablk%data( Ls:Le ) = TRANSFER( sx_diag, I1, bsize )
           ELSE IF( id == 28 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( x2,        I1, bsize )
           ELSE IF( id == 29 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( w2,        I1, bsize )
           ELSE IF( id == 30 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( x3,        I1, bsize )
           ELSE IF( id == 31 ) THEN
              ret_cov_diag = (/ ( Sret(i,i), i=1,11 ) /)
              datablk%data( Ls:Le ) = TRANSFER( ret_cov_diag, I1, bsize )
           ELSE IF( id == 32 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( Gc,     I1, bsize )
           ENDIF
         ENDDO

       END SUBROUTINE O3T_L2setDataPix

       SUBROUTINE O3T_L2fillDataPix( iT, iX, nWavel, nLayers, nWavelRet, &
                                     algflg, QAflags, datablk )
         TYPE (L2_generic_type) :: datablk
         INTEGER (KIND=4), INTENT(IN) :: iT, iX, nWavel, nLayers
         INTEGER (KIND=4), INTENT(IN) :: nWavelRet
         INTEGER (KIND=1), INTENT(IN) :: algflg
         INTEGER (KIND=2), INTENT(IN) :: QAflags
         REAL    (KIND=4), DIMENSION(nWavel) :: R4wlArray 
         REAL    (KIND=4), DIMENSION(nLayers) :: R4lyrArray
         REAL    (KIND=4), DIMENSION(nWavelRet) :: R4wlRetArray
         REAL    (KIND=4), DIMENSION(nWavelRet, NLayers) :: R4wlRetLyrArray
         INTEGER (KIND=4) :: id, is, ie, Ls, Le, bsize
         INTEGER (KIND=4) :: LL
         INTEGER (KIND=1) :: I1temp
         INTEGER (KIND=2) :: I2temp
         REAL    (KIND=4) :: R4temp

         ! advance by 1 due to scene pressure addition - MB 02/04/24
!         is = 3
!         ie = 15
         is = 4
         ie = 16
         DO id = is, ie
           LL    = (iT-1)*nXtrack_rad + (iX-1) !! 0 based index
           bsize = datablk%pixSize(id)
           Ls    = datablk%accuBlkSize(id-1) + LL*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1  !! 1 based index
           IF(      id == 4 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( algflg,  I1, bsize )
           ELSE IF( id == 5 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( QAflags, I1, bsize )
           ELSE IF( id == 6 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( 0, I1, bsize )
           ELSE 
              R4temp = fill_float32
              datablk%data( Ls:Le ) = TRANSFER( R4temp,  I1, bsize )
           ENDIF
         ENDDO

         ! advance indices below by 1 due to scene pressure - MB 02/04/24
!         is = 16
!         ie = 21
         is = 17
         ie = 22
         R4wlArray(:) = fill_float32
         DO id = is, ie   !! 3-D datafields: nWavel,nXtrack,nTimes
           LL    = (iT-1)*nXtrack_rad + (iX-1) !! 0 based index
           bsize = datablk%pixSize(id)
           Ls    = datablk%accuBlkSize(id-1) + LL*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1  !! 1 based index
           IF( id == is+1 ) THEN
              datablk%data( Ls:Le ) = TRANSFER( R4wlArray, I1, bsize )
           ELSE
              datablk%data( Ls:Le ) = TRANSFER( R4wlArray,  I1, bsize )
           ENDIF
         ENDDO 

         ! advance indices below by 1 due to scene pressure - MB 02/04/24
!         is = 22
!         ie = 29
         is = 23
         ie = 30
         R4lyrArray(:) = fill_float32
         DO id = is, ie   !! 3-D datafields: nLayers,nXtrack,nTimes
           LL    = (iT-1)*nXtrack_rad + (iX-1) !! 0 based index
           bsize = datablk%pixSize(id)
           Ls    = datablk%accuBlkSize(id-1) + LL*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1  !! 1 based index 
           datablk%data( Ls:Le ) = TRANSFER( R4lyrArray,  I1, bsize )
         ENDDO 

         ! advance indices below by 1 due to scene pressure - MB 02/04/24
!         is = 30
!         ie = 30
         is = 31
         ie = 31
         R4wlRetArray(:) = fill_float32
         DO id = is, ie   !! 3-D datafields: nWavelRet,nXtrack,nTimes
           LL    = (iT-1)*nXtrack_rad + (iX-1) !! 0 based index
           bsize = datablk%pixSize(id)
           Ls    = datablk%accuBlkSize(id-1) + LL*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1  !! 1 index scheme
           datablk%data( Ls:Le ) = TRANSFER( R4wlRetArray,  I1, bsize )
         ENDDO

       END SUBROUTINE O3T_L2fillDataPix

       SUBROUTINE O3T_L2setMqaLine( iT, mqaL2, datablk ) 
         TYPE (L2_generic_type), INTENT( INOUT ) :: datablk
         INTEGER (KIND=1), INTENT(IN) :: mqaL2
         INTEGER (KIND=4), INTENT(IN) :: iT
         INTEGER (KIND=4) :: id, Ls, Le, bsize
         ! advance indices by 1 due to scenepressure addition - MB 02/04/24
!         DO id = 31, 31
         DO id = 32, 32
           bsize = datablk%lineSize(id)
           Ls    = datablk%accuBlkSize(id-1) + (iT-1)*bsize
           Le    = Ls + bsize
           Ls    = Ls + 1       !! fortran index scheme
           datablk%data( Ls:Le ) = TRANSFER( mqaL2,  I1, bsize )
         ENDDO       
         
       END SUBROUTINE O3T_L2setMqaLine
END MODULE O3T_L2output_class
