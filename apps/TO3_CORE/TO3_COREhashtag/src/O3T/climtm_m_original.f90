MODULE climtm_m
    USE O3T_stnprof_class
    USE OMI_LUN_set
    USE PGS_PC_class
    IMPLICIT NONE
    INTEGER (KIND = 4), PARAMETER, PRIVATE :: zero = 0
    INTEGER (KIND = 4), PARAMETER, PRIVATE :: ntoz = 10
    INTEGER (KIND = 4), PARAMETER, PRIVATE :: nlat_ctrs = 18
    
    REAL (KIND=4), DIMENSION(ntoz) :: toz
    REAL (KIND=4), DIMENSION(nlat_ctrs) :: lat_ctrs=(/-85.0,-75.0,-65.0,-55.0,&
    -45.0,-35.0,-25.0,-15.0,-5.0,5.0,15.0,25.0,35.0,45.0,55.0,65.0,75.0,85.0/)
    REAL (KIND=4), DIMENSION(nlat_ctrs) :: rlats
    REAL (KIND=4), DIMENSION(NLYR,ntoz,nlat_ctrs,13) :: tzaprf
    REAL (KIND=4), DIMENSION(NLYR,nlat_ctrs,12) :: climtm
    
   CONTAINS

     FUNCTION climtm_read( ) RESULT( status )
       INCLUDE 'PGS_IO.f'
       INCLUDE 'PGS_IO_1.f'
       INTEGER (KIND=4), EXTERNAL :: pgs_io_gen_openf, pgs_io_gen_closef
       INTEGER (KIND=4) :: file_version, record_length, &
                           climoz_handle, climtm_handle
       INTEGER (KIND=4) :: status, ierr, ios
       INTEGER (KIND=4) :: kmonth, jlat, ioz, ilyr, mnth
       CHARACTER (LEN =255) :: msg

       record_length= 0
 
       file_version = 1
       status = PGS_IO_Gen_OpenF( TM_CLIM_LUN, PGSd_IO_Gen_RSeqFrm, &
                                  record_length, climtm_handle, &
                                  file_version )
       IF( status /= PGS_S_SUCCESS ) THEN
          ierr = OMI_SMF_setmsg( status,  "open clim tm file for read failed", &
                                 "climtm_read", zero )
          status = OZT_E_FAILURE
          RETURN
       ELSE
          READ (climtm_handle,'(11F7.1)', IOSTAT = ios ) climtm

          IF( ios /= zero ) THEN
             status = OZT_E_FAILURE
             WRITE(msg,*) 'error: reading climtm file '
             ierr = OMI_SMF_setmsg( OZT_E_INPUT, msg, "climtm_read", zero )
             RETURN
          ENDIF
     
          status = pgs_io_gen_closef( climtm_handle )
       ENDIF
       status = OZT_S_SUCCESS
     END FUNCTION climtm_read

     FUNCTION climtm_prf( latitude, jday, aprftm_k ) &
                               RESULT( status )
       REAL (KIND=4), INTENT(IN) :: latitude
       INTEGER (KIND=4), INTENT(IN) :: jday
       REAL (KIND=4), DIMENSION(:), INTENT(OUT) :: aprftm_k
       INTEGER (KIND=4) :: status, ierr, ios
       CHARACTER (LEN =255) :: msg
       INTEGER :: l1, l2, m1, m2
       REAL (KIND=4) :: fracl, xmon, fracm

       !
       ! -- compute indeces for bracketing months and latitudes
       !
       l1 = (latitude + 85.0) / 10.0 + 1.0
       IF(l1  <=  0) l1 = 1
       IF(l1  >= 18) l1 = 17
       l2 = l1 + 1
       fracl = (latitude - lat_ctrs(l1)) / (lat_ctrs(l2) - lat_ctrs(l1))
 
       xmon = (jday + 15.25) / 30.5
       m1 = xmon
       m2 = m1 + 1
       fracm = xmon - m1
       if(m1 == 0) m1 = 12
       if(m2 == 13) m2 = 1

       IF( SIZE(aprftm_k) < NLYR ) THEN
          status = OZT_E_FAILURE
          ierr = OMI_SMF_setmsg( OZT_E_INPUT, &
                                 "input array aprftm size too small", &
                                 "climtm_prf", zero )
          RETURN
       ENDIF
       aprftm_k=(1.0-fracl)*(1.0-fracm)*climtm(:,l1,m1) + &
                     fracl *(1.0-fracm)*climtm(:,l2,m1) + &
                     fracl *     fracm *climtm(:,l2,m2) + &
                (1.0-fracl)*     fracm *climtm(:,l1,m2)

       status = OZT_S_SUCCESS
     END FUNCTION climtm_prf
   END MODULE climtm_m
