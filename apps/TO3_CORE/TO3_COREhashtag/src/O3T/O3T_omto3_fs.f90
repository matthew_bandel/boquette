! This module contains the definitions for the output of OMTO3. All information 
! about the geolocation and data output fields are defined as user-defined types
! are defined here.
!
! revision history:
!
!  last modified: July 20, 2019, Dave Haffner / SSAI
!  purpose: modify fields for version 9
!
!  initial version: March 26, 2002, Kai Yang / UMBC
!
! /* TODO */
! 
! Add fields: 
!   Temperature
!   MeasurementNoise
!   CrossSection
!  
module o3t_omto3_fs
    use he5_class
    implicit none

! Geolocation Fields
    TYPE (DFHE5_T) :: gf_GroundPixelQualityFlags  =                       &
           DFHE5_T( 0.0, 65534.0D0, 1.0D0, 0.0D0,                         &
                    "GroundPixelQualityFlags",                            &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Ground Pixel Quality Flags",                         & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_UINT16, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_Latitude =                                       &
           DFHE5_T( -90.0, 90.0, 1.0D0, 0.0D0,                            &
                    "Latitude ",                                          &
                    "nXtrack,nTimes",                                     & 
                    "degrees_north",                                      &
                    "Geodetic Latitude",                                  &
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_Longitude =                                      &
           DFHE5_T( -180.0, 180.0, 1.0D0, 0.0D0,                          & 
                    "Longitude",                                          &
                    "nXtrack,nTimes",                                     & 
                    "degrees_east",                                       &
                    "Geodetic Longitude",                                 &
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SolarAzimuthAngle =                              &
           DFHE5_T( -180.0, 180.0, 1.0D0, 0.0D0,                          & 
                    "SolarAzimuthAngle",                                  &
                    "nXtrack,nTimes",                                     & 
                    "degrees",                                            &
                    "Solar Azimuth Angle",                                &
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_ViewingAzimuthAngle =                            &
           DFHE5_T( -180.0, 180.0, 1.0D0, 0.0D0,                          & 
                    "ViewingAzimuthAngle",                                &
                    "nXtrack,nTimes",                                     & 
                    "degrees",                                            &
                    "Viewing Azimuth Angle",                              &
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_RelativeAzimuthAngle =                           &
           DFHE5_T( -180.0, 180.0, 1.0D0, 0.0D0,                          & 
                    "RelativeAzimuthAngle",                               &
                    "nXtrack,nTimes",                                     & 
                    "degrees",                                            &
                    "Relative Azimuth Angle (Solar - View + 180)",        &
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SolarZenithAngle =                               &
           DFHE5_T( 0.0, 180.0, 1.0D0, 0.0D0,                             &
                    "SolarZenithAngle",                                   &
                    "nXtrack,nTimes",                                     & 
                    "degrees",                                            &
                    "Solar Zenith Angle",                                 &
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_ViewingZenithAngle =                             &
           DFHE5_T( 0.0, 90.0, 1.0D0, 0.0D0,                              &
                    "ViewingZenithAngle",                                 &
                    "nXtrack,nTimes",                                     & 
                    "degrees",                                            &
                    "Viewing Zenith Angle",                               &
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_TerrainHeight  =                                 &
           DFHE5_T( -200.0, 10000.0, 1.0D0, 0.0D0,                        & 
                    "TerrainHeight",                                      &
                    "nXtrack,nTimes",                                     &
                    "meters",                                             &
                    "Terrain Height",                                     & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_INT16, 2, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: gf_Time  =                                          &
           DFHE5_T( -5.0D09, 1.0D10, 1.0D0, 0.0D0,                        & 
                    "Time",                                               &
                    "nTimes",                                             &
                    "seconds since 1993-01-01 00:00:00Z",                 &
                    "Time at start of scan (TAI93)",                      & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_DOUBLE, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SecondsInDay =                                   &
           DFHE5_T( 0.0, 86400.0, 1.0D0, 0.0D0,                           & 
                    "SecondsInDay",                                       &
                    "nTimes",                                             &
                    "seconds",                                            &
                    "Seconds after midnight UTC",                         & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SpacecraftLatitude =                             &
           DFHE5_T( -90.0, 90.0, 1.0D0, 0.0D0,                            & 
                    "SpacecraftLatitude",                                 &
                    "nTimes",                                             &
                    "degrees_north",                                      &
                    "Spacecraft Latitude",                                & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SpacecraftLongitude =                            &
           DFHE5_T( -180.0, 180.0, 1.0D0, 0.0D0,                          & 
                    "SpacecraftLongitude",                                &
                    "nTimes",                                             &
                    "degrees_east",                                       &
                    "Spacecraft Longitude",                               & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_SpacecraftAltitude =                             &
           DFHE5_T( 4.0D05, 9.0D05, 1.0D0, 0.0D0,                         & 
                    "SpacecraftAltitude",                                 &
                    "nTimes",                                             &
                    "meters",                                             &
                    "Spacecraft Altitude",                                & 
                    "TOMS-Aura-Shared",                                   & 
                     -1, HE5T_NATIVE_FLOAT, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: gf_XTrackQualityFlags  =                            &
           DFHE5_T( 0, 254, 1.0D0, 0.0D0,                                 &
                    "XTrackQualityFlags",                                 &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Cross-track Quality Flags",                          & 
                    "OMI-Specific",                                    & 
                     -1, HE5T_NATIVE_UINT8, 2, (/ -1, -1, -1 /))

! Data Fields
    TYPE (DFHE5_T) :: df_MeasurementQualityFlags  =                       &
           DFHE5_T( 0, 254, 1.0D0, 0.0D0,                                 & 
                    "MeasurementQualityFlags",                            &
                    "nTimes",                                             &
                    "NoUnits",                                            &
                    "Measurement Quality Flags",                          & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_UINT8, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_NumberSmallPixelColumns  =                       &
           DFHE5_T( 0, 5, 1.0D0, 0.0D0,                                   & 
                    "NumberSmallPixelColumns",                            &
                    "nTimes",                                             &
                    "NoUnits",                                            &
                    "Number of Small Pixel Columns",                            & 
                    "OMI-Specific",                                       & 
                     -1, HE5T_NATIVE_UINT8, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_SmallPixelColumn  =                              &
           DFHE5_T( 0, 1000, 1.0D0, 0.0D0,                                & 
                    "SmallPixelColumn",                                   &
                    "nTimes",                                             &
                    "NoUnits",                                            &
                    "Small Pixel Column",                                 &
                    "OMI-Specific",                                       & 
                     -1, HE5T_NATIVE_INT16, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_InstrumentConfigurationId =                      &
           DFHE5_T( 0, 200, 1.0D0, 0.0D0,                                 & 
                    "InstrumentConfigurationId",                          &
                    "nTimes",                                             &
                    "NoUnits",                                            &
                    "Instrument Configuration ID",                        &
                    "OMI-Specific",                                       &                           
                     -1, HE5T_NATIVE_UINT8, 1, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_Wavelength  =                                    &
           DFHE5_T( 300.0, 400.0, 1.0D0, 0.0D0,                           & 
                    "Wavelength",                                         &
                    "nWavel",                                             &
                    "nm",                                                 &
                    "Wavelength",                                         & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 1, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_TerrainPressure  =                               &
           DFHE5_T( 0.0, 1100.00, 1.0D0, 0.0D0,                           & 
                    "TerrainPressure",                                    &
                    "nXtrack,nTimes",                                     &
                    "hPa",                                                &
                    "Terrain Pressure",                                   & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    ! adeding ScenePressure - MB 02/04/24
    TYPE (DFHE5_T) :: df_ScenePressure  =                                 &
           DFHE5_T( 0.0, 1100.00, 1.0D0, 0.0D0,                           &
                    "ScenePressure",                                      &
                    "nXtrack,nTimes",                                     &
                    "hPa",                                                &
                    "Scene Pressure",                                     &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_AlgorithmFlags =                                 &
           DFHE5_T( 0.0, 12.0, 1.0D0, 0.0D0,                              & 
                    "AlgorithmFlags",                                     &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Algorithm Flags",                                    & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_UINT8, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_APrioriLayerO3 =                                 &
           DFHE5_T( 0.0, 200.0, 1.0D0, 0.0D0,                             & 
                    "APrioriLayerO3",                                     &
                    "nLayers,nXtrack,nTimes",                             &
                    "DU",                                                 &
                    "A Priori Layer Amount O3",                           & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_CloudFraction    =                               &
           DFHE5_T( 0.0, 1.0, 1.0D0, 0.0D0,                               & 
                    "CloudFraction",                                      &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Cloud Fraction",                                     & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_CloudPressure  =                                 &
           DFHE5_T( 0.0, 1100.00, 1.0D0, 0.0D0,                           & 
                    "CloudPressure",                                      &
                    "nXtrack,nTimes",                                     &
                    "hPa",                                                &
                    "Optical Centroid Cloud Pressure",                    & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_ColumnAmountO3    =                              &
           DFHE5_T( 50.0, 700.0, 1.0D0, 0.0D0,                            & 
                    "ColumnAmountO3",                                     &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Best Total Ozone Solution",                          & 
                    "TOMS-OMI-Shared",                                    &  
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_dNdR =                                           &
           DFHE5_T( -200.0, 0.0, 1.0D0, 0.0D0,                            & 
                    "dNdR",                                               &
                    "nWavel,nXtrack,nTimes",                              &
                    "NoUnits",                                            &
                    "N-Value Sensitivity to Reflectivity",              & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_QualityFlags   =                                 &
           DFHE5_T( 0.0, 65534.0, 1.0D0, 0.0D0,                           & 
                    "QualityFlags",                                       &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Quality Flags",                                      & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_UINT16, 2, (/ -1, -1, -1 /))                   
                     
    TYPE (DFHE5_T) :: df_ColumnWeightingFunction =                        &
           DFHE5_T( -3.0, 3.0, 1.0D0, 0.0D0,                              & 
                    "ColumnWeightingKernel",                              &
                    "nLayers,nXtrack,nTimes",                             &
                    "NoUnits",                                            &                              
                    "Column Weighting Kernel",                            & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_NValue =                                         &
           DFHE5_T( 0.0, 400.0, 1.0D0, 0.0D0,                             & 
                    "NValue",                                             &
                    "nWavel,nXtrack,nTimes",                              &
                    "NoUnits",                                            &
                    "N-Value",                                            & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_OzoneUnderCloud =                                &
           DFHE5_T( 0.0, 30.0, 1.0D0, 0.0D0,                             & 
                    "O3UnderCloud",                                    &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Ozone Under Cloud",                                  & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_Reflectivity331 =                                 &
           DFHE5_T( -0.1, 1.2, 1.0D0, 0.0D0,                               & 
                    "Reflectivity331",                                     &
                    "nXtrack,nTimes",                                      &
                    "NoUnits",                                             &
                    "Lambert Equivalent Reflectivity at 331 nm",           &
                    "TOMS-OMI-Shared",                                     & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_Reflectivity340 =                                 &
           DFHE5_T( -0.1, 1.2, 1.0D0, 0.0D0,                               & 
                    "Reflectivity340",                                     &
                    "nXtrack,nTimes",                                      &
                    "NoUnits",                                             &
                    "Lambert Equivalent Reflectivity at 340 nm",           &
                    "TOMS-OMI-Shared",                                     & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))
    
    TYPE (DFHE5_T) :: df_Reflectivity354 =                                 &
           DFHE5_T( -0.1, 1.2, 1.0D0, 0.0D0,                               & 
                    "Reflectivity354",                                     &
                    "nXtrack,nTimes",                                      &
                    "NoUnits",                                             &
                    "Lambert Equivalent Reflectivity at 354 nm",           &
                    "TOMS-OMI-Shared",                                     & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_ResidualStep1 =                                  &
           DFHE5_T( -32.0, 32.0, 1.0D0, 0.0D0,                            & 
                    "ResidualStep1",                                      &
                    "nWavel,nXtrack,nTimes",                              &
                    "NoUnits",                                            &
                    "Step One Residual",                                  & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_ResidualStep2 =                                  &
           DFHE5_T( -32.0, 32.0, 1.0D0, 0.0D0,                            & 
                    "ResidualStep2",                                      &
                    "nWavel,nXtrack,nTimes",                              &
                    "NoUnits",                                            &
                    "Step Two Residual",                                  & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_Sensitivity =                                    &
           DFHE5_T( -1.0, 1.0, 1.0D0, 0.0D0,                               & 
                    "Sensitivity",                                        &
                    "nWavel,nXtrack,nTimes",                              &
                    "1/DU",                                               &
                    "N-Value Sensitivity to Total Column O3",          & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_dNdT =                                           &
           DFHE5_T( 0.0, 1.0, 1.0D0, 0.0D0,                               & 
                    "dNdT",                                               &
                    "nWavel,nXtrack,nTimes",                              &
                    "1/K",                                                &
                    "N-Value Sensitivity to Ozone Weighted Temperature",& 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))
                     
    TYPE (DFHE5_T) :: df_StepOneO3 =                                      &
           DFHE5_T( 50.0, 700.0, 1.0D0, 0.0D0,                            & 
                    "StepOneO3",                                          &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Step One Total O3 Solution",                         & 
                    "TOMS-OMI-Shared",                                    &  
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_StepTwoO3 =                                      &
           DFHE5_T( 50.0, 700.0, 1.0D0, 0.0D0,                            & 
                    "StepTwoO3",                                          &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Step Two Total O3 Solution",                              & 
                    "TOMS-OMI-Shared",                                    &  
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_NValueAdjustment =                               &
           DFHE5_T( -5.0, 5.0, 1.0D0, 0.0D0,                            & 
                    "NValueAdjustments",                                  &
                    "nWavel,nXtrack",                                     &
                    "NoUnits",                                            &
                    "N-Value Adjustments",                                & 
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_ProfileShapeError =                              &
           DFHE5_T( 0.0, 30.0, 1.0D0, 0.0D0,                              &
                    "ProfileShapeError",                                  &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Profile Shape Error",                                &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_StepTwoProfileShapeError =                       &
           DFHE5_T( 0.0, 30.0, 1.0D0, 0.0D0,                              &
                    "StepTwoProfileShapeError",                           &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Step Two Profile Shape Error",              &
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_DegreesFreedomforSignal =                        &
           DFHE5_T( 0.0, 4.0, 1.0D0, 0.0D0,                               &
                    "DegreesFreedomforSignal",                            &
                    "nXtrack,nTimes",                                     &
                    "NoUnits",                                            &
                    "Degrees of Freedom for Signal",                      &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_MeasurementNoise =                               &
           DFHE5_T( 0.0, 30.0, 1.0D0, 0.0D0,                              &
                    "MeasurementNoise",                                   &
                    "nXtrack,nTimes",                                     &
                    "DU",                                                 &
                    "Measurement Noise",                                  &
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 2, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_APrioriLayerO3Covariance =                       &
           DFHE5_T( 0.0, 1000.0, 1.0D0, 0.0D0,                            &
                    "APrioriLayerO3Covariance",                           &
                    "nLayers,nXtrack,nTimes",                             &
                    "DU^2",                                                &
                    "A Priori Layer Amount O3 Covariance",                &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_LayerTemperature =                               &
           DFHE5_T( 100.0, 300.0, 1.0D0, 0.0D0,                           &
                    "LayerTemperature",                                   &
                    "nLayers,nXtrack,nTimes",                             &
                    "K",                                                  &
                    "Layer Temperature",                                  &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_StepTwoLayerO3 =                                 &
           DFHE5_T( 0.0, 200.0, 1.0D0, 0.0D0,                             &
                    "StepTwoLayerAmountO3",                                     &
                    "nLayers,nXtrack,nTimes",                             &
                    "DU",                                                 &
                    "Step Two Layer Amount O3",                   &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_StepTwoColumnWeightingFunction =                 &
           DFHE5_T( -3.0, 3.0, 1.0D0, 0.0D0,                              &
                    "StepTwoColumnWeightingKernel",                       &
                    "nLayers,nXtrack,nTimes",                             &
                    "NoUnits",                                            &
                    "Step Two Column Weighting Kernel",                   &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_LayerAmountO3 =                                  &
           DFHE5_T( 0.0, 200.0, 1.0D0, 0.0D0,                             &
                    "LayerAmountO3",                                      &
                    "nLayers,nXtrack,nTimes",                             &
                    "DU",                                                 &
                    "Layer Amount O3",                                    &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_LayerAmountO3Covariance =                        &
           DFHE5_T( 0.0, 1000.0, 1.0D0, 0.0D0,                            &
                    "LayerAmountO3Covariance",                            &
                    "nLayers,nXtrack,nTimes",                             &
                    "DU^2",                                               &
                    "Layer Amount O3 Covariance",   &
                    "TOMS-OMI-Shared",                                    &
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

    TYPE (DFHE5_T) :: df_ColumnGain =                                     &
           DFHE5_T( -25.0, 25.0, 1.0D0, 0.0D0,                            &
                    "ColumnGain",                                         &
                    "nWavelRet,nXtrack,nTimes",                           &
                    "DU",                                                 &
                    "Column Gain",                                        &
                    "TOMS-OMI-Shared",                                    & 
                     -1, HE5T_NATIVE_FLOAT, 3, (/ -1, -1, -1 /))

end module o3t_omto3_fs
