module apriori_m
  use omi_smf_class
  implicit none
  private
  real(kind=4), dimension(11,18,12), public :: xavg
  real(kind=4), dimension(11,11,18,12), public :: xcov
  public :: read_apriori, get_apriori
contains

  subroutine read_apriori()
    use pgs_pc_class
    use omi_lun_set
    implicit none

    ! parameters
    integer(kind=4), parameter :: lun=20
    integer(kind=4), parameter :: zero=0

    ! local
    integer(kind=4) :: iostat
    character(len=256) :: hdr, fn_o3ap, msg
    integer(kind=4) :: i, j
    integer(kind=4) :: version, ierr, status

    ! read ozone a priori
    version = 1
    status = PGS_PC_GetReference( O3_APRIORI_LUN, version, fn_o3ap )
    IF( status /= PGS_S_SUCCESS ) THEN
       WRITE( msg,'(A)' ) "get O3 a priori filename failed, PGE aborting, exit code = 1."
       ierr = OMI_SMF_setmsg( status, msg, "O3T_main", zero )
       status = OZT_E_FAILURE
       RETURN
    ENDIF

    open(lun,iostat=iostat,file=fn_o3ap)
    do i=1,4 
       read(lun,'(a)') hdr
    enddo
    read(lun,*) xavg
    read(lun,'(a)') hdr
    read(lun,*) xcov
    
    ! force covaraince to zero beyond one element off-diagonal 
    do i=1,11
       do j=1,11
          if(abs(i-j) > 1) xcov(i,j,:,:) = 0.0
       enddo
    enddo

    close(lun)
  end subroutine read_apriori

  FUNCTION get_apriori(xlat_in, iday, xa, sx) RESULT(status)
    character(len=256) :: msg
    integer(kind=4) :: status, ierr, ios
    integer(kind=4), intent(in) :: iday
    integer :: l1, l2, m1, m2, i
    real(kind=4) :: fracl, xmon, fracm
    real(kind=4), intent(in) :: xlat_in
    real(kind=4) :: xlat
    real(kind=4), dimension(:), intent(out) :: xa    
    real(kind=4), dimension(:,:), intent(out) :: sx
    real(kind=4), dimension(18), parameter :: cntrs = &
         (/-85.0,-75.0,-65.0,-55.0,-45.0,-35.0,-25.0,-15.0, -5.0,&
             5.0, 15.0, 25.0, 35.0, 45.0, 55.0, 65.0, 75.0, 85.0/)
!
!     -- compute indeces for bracketing latitudes and months
!
    xlat = xlat_in
    l1 = (xlat + 85.0) / 10.0 + 1.0
    if(l1.le.0) l1 = 1
    if(l1.ge.18) l1 = 17
    if(xlat.lt. -85.0) xlat=-85.0
    if(xlat.gt.  85.0) xlat= 85.0

    l2 = l1 + 1
    fracl = (xlat - cntrs(l1)) / (cntrs(l2) - cntrs(l1))
!
    xmon = (iday + 15.25) / 30.5
    m1 = xmon
    m2 = m1 + 1
    fracm = xmon - m1
    if(m1.eq.0) m1 = 12
    if(m2.eq.13) m2 = 1
!
    xa=(1.0-fracl)*(1.0-fracm)*xavg(:,l1,m1) + &
            fracl *(1.0-fracm)*xavg(:,l2,m1) + &
            fracl *     fracm *xavg(:,l2,m2) + &
       (1.0-fracl)*     fracm *xavg(:,l1,m2)
!
    sx=(1.0-fracl)*(1.0-fracm)*xcov(:,:,l1,m1) + &
            fracl *(1.0-fracm)*xcov(:,:,l2,m1) + &
            fracl *     fracm *xcov(:,:,l2,m2) + &
       (1.0-fracl)*     fracm *xcov(:,:,l1,m2)
!
    status = OZT_S_SUCCESS
!  
  end FUNCTION get_apriori
end module apriori_m
