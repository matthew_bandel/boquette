module m_write_global_attributes

    ! declare public
    public

    ! declare contains
    contains

    ! write the OMTO3 global attributes
    subroutine write_global_attributes(destination, ultraviolet, irradiance, &
            southernmost, northernmost, westernmost, easternmost, seconds, track, &
            snow, cloud, status)

        ! declare modules
        use netcdf
        use PGS_PC_class
        use HDF5
        use H5A
        use m_write_string_attribute
        use m_write_double_attribute
        use m_write_integer_attribute

        ! declare implicits
        implicit none

        ! declare arguments
        character (len=200), intent(in) :: destination, ultraviolet, irradiance
        character (len=*), intent(in) :: snow, cloud
        real(kind=8), intent(in) :: southernmost, northernmost, westernmost, easternmost, seconds
        integer(kind=4), intent(in) :: track
        integer(kind=4), intent(out) :: status

        ! declare variables
        integer (KIND=4), external :: day_of_year
        integer(kind=4) :: file, fileii, metadata, metadataii, inventory
        ! adding groups - MB 04/01/2024
        integer(kind=4) :: root, additional, group
        integer(kind=4) :: year, month, day, julian, orbit
        real(kind=8) :: equator
        character (len=2048) :: information
        character(100) :: production, history, version

        ! current datetime parameters
        character(8) :: date
        character(10) :: time
        character(19) :: zone

        ! open library and file
        call h5open_f(status)
        call h5fopen_f(trim(destination), H5F_ACC_RDWR_F, file, status)

        ! get the additional attributes group
        call h5gopen_f(file, "HDFEOS", root, status)
        call h5gopen_f(root, "ADDITIONAL", additional, status)
        call h5gopen_f(additional, "FILE_ATTRIBUTES", group, status)

        write(6, *) 'group status: ', status

        ! Get the current date and time
        call date_and_time(date, time, zone)

        ! add production time
        production = date(1:4)//"-"//date(5:6)//"-"//date(7:8)//"T"//time(1:2)//":"//time(3:4)//":"//time(5:6)
        call write_string_attribute(group, "ProductionDateTime", trim(production), status)
        write(6, *) 'production: ', trim(production), status

        ! copy toolkit version from control
        status = PGS_PC_GetConfigData(10220, information)
!        status = GetConfigString("E", "Runtime Parameters Toolkit version string", information)
        call write_string_attribute(group, "Toolkit version string", trim(information), status)
        write(6, *) 'toolkit: ', trim(information), status

       ! copy verbosity threshold
        status = PGS_PC_GetConfigData(200100, information)
!        status = GetConfigString("E", "Runtime Parameters SMF Verbosity Threshold", information)
        call write_string_attribute(group, "SMF Verbosity Threshold", trim(information), status)
        write(6, *) 'verbosity: ', trim(information), status

        ! copy PGE version
        status = PGS_PC_GetConfigData(200105, version)
!        status = GetConfigString("E", "Runtime Parameters PGEVERSION", information)
        call write_string_attribute(group, "PGEVersion", trim(version), status)
        write(6, *) 'pge version: ', trim(version), status

        ! copy processing center
        status = PGS_PC_GetConfigData(200110, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessingCenter", information)
        call write_string_attribute(group, "ProcessingCenter", trim(information), status)
        write(6, *) 'processing center: ', trim(information), status

        ! copy processing host
        status = PGS_PC_GetConfigData(200115, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessingHost", information)
        call write_string_attribute(group, "ProcessingHost", trim(information), status)
        write(6, *) 'processing host: ', trim(information)

        ! copy reprocessing actual
        status = PGS_PC_GetConfigData(200135, information)
!        status = GetConfigString("E", "Runtime Parameters REPROCESSINGACTUAL", information)
        call write_string_attribute(group, "ReprocessingActual", trim(information), status)
        write(6, *) 'processing actual: ', trim(information)

        ! copy processing level
        status = PGS_PC_GetConfigData(200170, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessLevel", information)
        call write_string_attribute(group, "ProcessingLevel", trim(information), status)
        write(6, *) 'processing level: ', trim(information)

        ! copy instrument name into instrument name and short name
        status = PGS_PC_GetConfigData(200175, information)
!        status = GetConfigString("E", "Runtime Parameters InstrumentName", information)
        call write_string_attribute(group, "InstrumentName", trim(information), status)
        call write_string_attribute(group, "InstrumentShortName", trim(information), status)
        write(6, *) 'instrument name: ', trim(information)

        ! copy operation mode
        status = PGS_PC_GetConfigData(200180, information)
!        status = GetConfigString("E", "Runtime Parameters OPERATIONMODE", information)
        call write_string_attribute(group, "OperationMode", trim(information), status)
        write(6, *) 'operation mode: ', trim(information)

        ! copy author affiliation
        status = PGS_PC_GetConfigData(200185, information)
!        status = GetConfigString("E", "Runtime Parameters AuthorAffiliation", information)
        call write_string_attribute(group, "AuthorAffiliation", trim(information), status)
        write(6, *) 'author affiliation: ', trim(information)

        ! copy author name
        status = PGS_PC_GetConfigData(200190, information)
!        status = GetConfigString("E", "Runtime Parameters AuthorName", information)
        call write_string_attribute(group, "AuthorName", trim(information), status)
        write(6, *) 'author name: ', trim(information)

        ! copy local version id
        status = PGS_PC_GetConfigData(200195, information)
!        status = GetConfigString("E", "Runtime Parameters LOCALVERSIONID", information)
        call write_string_attribute(group, "LocalVersionID", trim(information), status)
        write(6, *) 'local version: ', trim(information)

        ! copy orbit number
        status = PGS_PC_GetConfigData(200200, information)
!        status = GetConfigString("E", "Runtime Parameters OrbitNumber", information)
        read(information, *) orbit
        call write_integer_attribute(group, "OrbitNumber", orbit, status)
        call write_integer_attribute(group, "orbit", orbit, status)
        write(6, *) 'orbit number: ', orbit

        ! copy swath name
        status = PGS_PC_GetConfigData(200210, information)
!        status = GetConfigString("E", "Runtime Parameters SwathName", information)
        call write_string_attribute(group, "SwathName", trim(information), status)
        write(6, *) 'swath name: ', trim(information)

        ! copy cloud pressure source, passed in from main
        call write_string_attribute(group, "CloudPressureSource", trim(cloud), status)
        write(6, *) 'cloud pressure source: ', trim(cloud)

        ! copy terrain pressure source
        status = PGS_PC_GetConfigData(400091, information)
!        status = GetConfigString("E", "Runtime Parameters TERRAINPRESSURESOURCE", information)
        call write_string_attribute(group, "TerrainPressureSource", trim(information), status)
        write(6, *) 'terrain pressure source: ', trim(information)

        ! copy temperature source
        status = PGS_PC_GetConfigData(400092, information)
!        status = GetConfigString("E", "Runtime Parameters TEMPERATURESOURCE", information)
        call write_string_attribute(group, "TemperatureSource", trim(information), status)
        write(6, *) 'temperature source: ', trim(information)

        ! copy snow ice source passed in from main
        call write_string_attribute(group, "SnowIceSource", trim(snow), status)
        write(6, *) 'snow ice source: ', trim(snow)

        ! copy apriori source
        status = PGS_PC_GetConfigData(400094, information)
!        status = GetConfigString("E", "Runtime Parameters APRIORIOZONEPROFILESOURCE", information)
        call write_string_attribute(group, "AprioriOzoneProfileSource", trim(information), status)
        write(6, *) 'a priori source: ', trim(information)

        ! copy version id
        status = PGS_PC_GetConfigData(123456, information)
!        status = GetConfigString("E", "Runtime Parameters VERSIONID", information)
        call write_string_attribute(group, "VersionID", trim(information), status)
        write(6, *) 'version: ', trim(information)

!        ! copy aersol limit
!        status = PGS_PC_GetConfigData(400096, information)
!!        status = GetConfigString("E", "Runtime Parameters Aerosol Limit", information)
!        call write_string_attribute(group, "Aerosol Limit", trim(information), status)
!        write(6, *) 'aerosol limit: ', trim(information)
!
!        ! copy residual limit
!        status = PGS_PC_GetConfigData(400097, information)
!!        status = GetConfigString("E", "Runtime Parameters Residual Limit", information)
!        call write_string_attribute(group, "Residual Limit", trim(information), status)
!        write(6, *) 'residual limit: ', trim(information)

        ! add history
        history = "OZONE PEATE "//trim(production)//", APPVersion="//trim(version)
        call write_string_attribute(group, "history", trim(history), status)
        write(6, *) 'history: ', history

        ! open ultraviolet file
        status = nf90_open(trim(ultraviolet), nf90_nowrite, fileii)

        ! copy attriubtes from L1B
        status = nf90_get_att(fileii, NF90_GLOBAL, "Conventions", information)
        call write_string_attribute(group, "Conventions", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "institution", information)
        call write_string_attribute(group, "Institutioin", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "processor_version", information)
        call write_string_attribute(group, "processor_version", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "summary", information)
        call write_string_attribute(group, "summary", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_reference", information)
        call write_string_attribute(group, "time_reference", trim(information), status)

        ! add time coverage beginning parameterw
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_start", information)
        call write_string_attribute(group, "time_coverage_start", trim(information), status)
        call write_string_attribute(group, "RangeBeginningDate", trim(information(1:10)), status)
        call write_string_attribute(group, "RangeBeginningTime", trim(information(12:20)), status)

        ! add time coverage end based parameeters
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_end", information)
        call write_string_attribute(group, "time_coverage_end", trim(information), status)
        call write_string_attribute(group, "RangeEndingDate", trim(information(1:10)), status)
        call write_string_attribute(group, "RangeEndingTime", trim(information(12:20)), status)

        ! get day of year
        read(information(1:4), *) year
        read(information(6:7), *) month
        read(information(9:10), *) day
        julian = day_of_year(year, month, day)

        ! set granule attributes
        call write_integer_attribute(group, "GranuleYear", year, status)
        call write_integer_attribute(group, "GranuleMonth", month, status)
        call write_integer_attribute(group, "GranuleDay", day, status)
        call write_integer_attribute(group, "GranuleDayOfYear", julian, status)

        ! get equator crossing time
        status = nf90_inq_grp_ncid(fileii, "METADATA", metadata)
        status = nf90_inq_grp_ncid(metadata, "ECS_METADATA", metadataii)
        status = nf90_inq_grp_ncid(metadataii, "Inventory_Metadata", inventory)
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingDateTime", information)
        call write_string_attribute(group, "EquatorCrossingDateTime", trim(information), status)
        call write_string_attribute(group, "EquatorCrossingDate", trim(information(1:10)), status)
        call write_string_attribute(group, "EquatorCrossingTime", trim(information(12:20)), status)

        ! get equator crossing longitude
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingLongitude", information)
        read(information, *) equator
        call write_double_attribute(group, "EquatorCrossingLongitude", equator, status)

        ! add input pointer
        information = trim(ultraviolet)//","//trim(irradiance)
        call write_string_attribute(group, "InputPointer", trim(information), status)
        write(6, *) 'input pointer: ', trim(information)

        ! add coordinates
        call write_double_attribute(group, "NorthBoundingCoordinate", northernmost, status)
        call write_double_attribute(group, "SouthBoundingCoordinate", southernmost, status)
        call write_double_attribute(group, "WestBoundingCoordinate", westernmost, status)
        call write_double_attribute(group, "EastBoundingCoordinate", easternmost, status)

        ! add time, adding an additional amount of seconds because L1B is referenced to 2010-01-01 instead of 1993
        call write_double_attribute(group, "TAI93At0zOfGranule", seconds + 536457600, status)
        call write_integer_attribute(group, "NumTimes", track, status)

        ! add destination name to granule ids
        call write_string_attribute(group, "GranuleID", trim(destination), status)
        call write_string_attribute(group, "LocalGranuleID", trim(destination), status)

        ! add other hard coded attributes
        call write_string_attribute(group, "comment", " ", status)
        call write_string_attribute(group, "DataSetQuality", "Excellent data quality", status)
        call write_string_attribute(group, "DayNightFlag", "Day", status)
        call write_string_attribute(group, "Format", "HDF5", status)
        call write_string_attribute(group, "FOVResolution", "13kmx24km", status)
        call write_string_attribute(group, "IdentifierProductDOI", " ", status)
        call write_string_attribute(group, "IdentifierProductDOIAuthority", "https://doi.org/", status)
        call write_string_attribute(group, "institution", "NASA/GSFC", status)
        call write_string_attribute(group, "LocalityValue", "Global", status)
        call write_string_attribute(group, "LongName", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km", status)
        call write_string_attribute(group, "ParameterName", "OMI Ground Pixels UV1, UV2 and VIS", status)
        call write_string_attribute(group, "PGEName", "OMTO3", status)
        call write_string_attribute(group, "PlatformShortName", "Aura", status)
        call write_string_attribute(group, "ProductType", "L2 Swath", status)
        call write_string_attribute(group, "references", " ", status)
        call write_string_attribute(group, "source", "Aura OMI", status)
        call write_string_attribute(group, "title", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km", status)
        call write_string_attribute(group, "SensorShortName", "CCD Ultraviolet, CCD Visible", status)
        call write_string_attribute(group, "ShortName", "OMTO3", status)

        ! close file and library
        call h5gclose_f(group, status)
        call h5gclose_f(additional, status)
        call h5gclose_f(root, status)
        call h5fclose_f(file, status)
        call h5close_f(status)

        ! close L1B file
        status = nf90_close(fileii)

        return

    end subroutine write_global_attributes
end module m_write_global_attributes
