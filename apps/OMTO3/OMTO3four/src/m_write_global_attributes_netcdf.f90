module m_write_global_attributes

    ! declare public
    public

    ! declare contains
    contains

    ! write the OMTO3 global attributes
    subroutine write_global_attributes(destination, ultraviolet, irradiance, &
            southernmost, northernmost, westernmost, easternmost, seconds, track, &
            snow, cloud, status)

        ! declare modules
        use netcdf
        use PGS_PC_class

        ! declare implicits
        implicit none

        ! adding for control files - MB 01/18/24
!        #include "Messages.inc"
        include "GetConfig.inc"

        ! declare arguments
        character (len=200), intent(in) :: destination, ultraviolet, irradiance
        character (len=*), intent(in) :: snow, cloud
        real(kind=4), intent(in) :: southernmost, northernmost, westernmost, easternmost, seconds
        integer(kind=4), intent(in) :: track
        integer(kind=4), intent(out) :: status

        ! declare variables
        integer (KIND=4), external :: day_of_year
        integer(kind=4) :: file, fileii, metadata, metadataii, inventory
        integer(kind=4) :: year, month, day, julian, orbit
        real(kind=4) :: equator
        character (len=2048) :: information
        character(100) :: production, history, version

        ! current datetime parameters
        character(8) :: date
        character(10) :: time
        character(19) :: zone

        write(6, *) 'destination: ', trim(destination)

        ! open file
        status = nf90_open(trim(destination), NF90_WRITE, file)

        write(6, *) 'open status: ', status

        ! Get the current date and time
        call date_and_time(date, time, zone)

        write(6, *) 'date: ', date
        write(6, *) 'time: ', time
        write(6, *) 'zone: ', zone

        ! add production time
        production = date(1:4)//"-"//date(5:6)//"-"//date(7:8)//"T"//time(1:2)//":"//time(3:4)//":"//time(5:6)
        status = nf90_put_att(file, NF90_GLOBAL, "ProductionDateTime", trim(production))

        write(6, *) 'production: ', trim(production)

        ! copy attriubtes from PCF file / control
!        status = PGS_PC_GetConfigData(10220, information)
        status = GetConfigString("E", "Runtime Parameters Toolkit version string", information)
        write(6, *) 'toolkit: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "Toolkit version string", trim(information))
!        status = PGS_PC_GetConfigData(200100, information)
        status = GetConfigString("E", "Runtime Parameters SMF Verbosity Threshold", information)
        write(6, *) 'verbosity: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "SMF Verbosity Threshold", trim(information))
!        status = PGS_PC_GetConfigData(200105, version)
        status = GetConfigString("E", "Runtime Parameters PGEVERSION", information)
        write(6, *) 'pge version: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "PGEVERSION", trim(version))
        status = nf90_put_att(file, NF90_GLOBAL, "PGEVersion", trim(version))
!        status = PGS_PC_GetConfigData(200110, information)
        status = GetConfigString("E", "Runtime Parameters ProcessingCenter", information)
        write(6, *) 'processing center: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingCenter", trim(information))
!        status = PGS_PC_GetConfigData(200115, information)
        status = GetConfigString("E", "Runtime Parameters ProcessingHost", information)
        write(6, *) 'processing host: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingHost", trim(information))
!        status = PGS_PC_GetConfigData(200135, information)
        status = GetConfigString("E", "Runtime Parameters REPROCESSINGACTUAL", information)
        write(6, *) 'processing actual: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "REPROCESSINGACTUAL", trim(information))
!        status = PGS_PC_GetConfigData(200170, information)
        status = GetConfigString("E", "Runtime Parameters ProcessLevel", information)
        write(6, *) 'processing level: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessLevel", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingLevel", trim(information))
!        status = PGS_PC_GetConfigData(200175, information)
        status = GetConfigString("E", "Runtime Parameters InstrumentName", information)
        write(6, *) 'instrument name: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "InstrumentName", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "InstrumentShortName", trim(information))
!        status = PGS_PC_GetConfigData(200180, information)
        status = GetConfigString("E", "Runtime Parameters OPERATIONMODE", information)
        write(6, *) 'operation mode: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "OPERATIONMODE", trim(information))
!        status = PGS_PC_GetConfigData(200185, information)
        status = GetConfigString("E", "Runtime Parameters AuthorAffiliation", information)
        write(6, *) 'author affiliation: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "AuthorAffiliation", trim(information))
!        status = PGS_PC_GetConfigData(200190, information)
        status = GetConfigString("E", "Runtime Parameters AuthorName", information)
        write(6, *) 'author name: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "AuthorName", trim(information))
!        status = PGS_PC_GetConfigData(200195, information)
        status = GetConfigString("E", "Runtime Parameters LOCALVERSIONID", information)
        write(6, *) 'local version: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "LOCALVERSIONID", trim(information))
!        status = PGS_PC_GetConfigData(200200, information)
        status = GetConfigString("E", "Runtime Parameters OrbitNumber", information)
        write(6, *) 'orbit number: ', trim(information)
        read(information, *) orbit
        status = nf90_put_att(file, NF90_GLOBAL, "OrbitNumber", orbit)
        status = nf90_put_att(file, NF90_GLOBAL, "orbit", orbit)
!        status = PGS_PC_GetConfigData(200210, information)
        status = GetConfigString("E", "Runtime Parameters SwathName", information)
        write(6, *) 'swath name: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "SwathName", trim(information))
!        status = PGS_PC_GetConfigData(400090, information)
!        status = GetConfigString("E", "Runtime Parameters CLOUDPRESSURESOURCE", information)
        write(6, *) 'cloud pressure source: ', trim(cloud)
        status = nf90_put_att(file, NF90_GLOBAL, "CLOUDPRESSURESOURCE", trim(cloud))
!        status = PGS_PC_GetConfigData(400091, information)
        status = GetConfigString("E", "Runtime Parameters TERRAINPRESSURESOURCE", information)
        write(6, *) 'terrain pressure source: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "TERRAINPRESSURESOURCE", trim(information))
!        status = PGS_PC_GetConfigData(400092, information)
        status = GetConfigString("E", "Runtime Parameters TEMPERATURESOURCE", information)
        write(6, *) 'temperature source: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "TEMPERATURESOURCE", trim(information))
!        status = PGS_PC_GetConfigData(400093, information)
!        status = GetConfigString("E", "Runtime Parameters SNOWICESOURCE", information)
!        write(6, *) 'snow ice source: ', trim(information)
!        status = nf90_put_att(file, NF90_GLOBAL, "SNOWICESOURCE", trim(information))
!        status = PGS_PC_GetConfigData(400094, information)
        status = GetConfigString("E", "Runtime Parameters APRIORIOZONEPROFILESOURCE", information)
        write(6, *) 'a priori source: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "APRIORIOZONEPROFILESOURCE", trim(information))
!        status = PGS_PC_GetConfigData(123456, information)
        status = GetConfigString("E", "Runtime Parameters VERSIONID", information)
        write(6, *) 'version: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "VERSIONID", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "VersionID", trim(information))
!        status = PGS_PC_GetConfigData(400096, information)
        status = GetConfigString("E", "Runtime Parameters Aerosol Limit", information)
        write(6, *) 'aerosol limit: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "Aerosol Limit", trim(information))
!        status = PGS_PC_GetConfigData(400097, information)
        status = GetConfigString("E", "Runtime Parameters Residual Limit", information)
        write(6, *) 'residual limit: ', trim(information)
        status = nf90_put_att(file, NF90_GLOBAL, "Residual Limit", trim(information))

        ! add attributes gathered from control in main program
!        write(6, *) 'temperature source: ', trim(temperature)
!        status = nf90_put_att(file, NF90_GLOBAL, "TEMPERATURESOURCE", trim(temperature))
        write(6, *) 'snow ice source: ', trim(snow)
        status = nf90_put_att(file, NF90_GLOBAL, "SNOWICESOURCE", trim(snow))

        ! add history
        history = "OZONE PEATE "//trim(production)//", APPVersion="//trim(version)
        status = nf90_put_att(file, NF90_GLOBAL, "history", trim(history))
        write(6, *) 'history: ', history

        ! open ultraviolet file
        status = nf90_open(trim(ultraviolet), nf90_nowrite, fileii)

        ! copy attriubtes
        status = nf90_get_att(fileii, NF90_GLOBAL, "Conventions", information)
        status = nf90_put_att(file, NF90_GLOBAL, "Conventions", trim(information))
!        status = nf90_get_att(fileii, NF90_GLOBAL, "institution", information)
!        status = nf90_put_att(file, NF90_GLOBAL, "institution", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "processor_version", information)
        status = nf90_put_att(file, NF90_GLOBAL, "processor_version", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "summary", information)
        status = nf90_put_att(file, NF90_GLOBAL, "summary", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_reference", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_reference", trim(information))

        ! add time coverage beginning parameterw
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_start", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_coverage_start", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeBeginningDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeBeginningTime", trim(information(12:20)))

        ! add time coverage end based parameeters
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_end", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_coverage_end", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeEndingDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeEndingTime", trim(information(12:20)))

        ! get day of year
        read(information(1:4), *) year
        read(information(6:7), *) month
        read(information(9:10), *) day
        julian = day_of_year(year, month, day)

        ! set granule attributes
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleYear", year)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleMonth", month)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleDay", day)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleDayOfYear", julian)

        ! get equator longitude and crossig
        status = nf90_inq_grp_ncid(fileii, "METADATA", metadata)
        status = nf90_inq_grp_ncid(metadata, "ECS_METADATA", metadataii)
        status = nf90_inq_grp_ncid(metadataii, "Inventory_Metadata", inventory)
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingDateTime", information)
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingDateTime", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingTime", trim(information(12:20)))
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingLongitude", information)
        read(information, *) equator
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingLongitude", equator)

        ! add InputPointer
        information = trim(ultraviolet)//","//trim(irradiance)
        status = nf90_put_att(file, NF90_GLOBAL, "InputPointer", trim(information))

        ! add coordinates
        status = nf90_put_att(file, NF90_GLOBAL, "NorthBoundingCoordinate", northernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "SouthBoundingCoordinate", southernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "WestBoundingCoordinate", westernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "EastBoundingCoordinate", easternmost)

        ! add time, adding an additional amount of seconds because L1B is referenced to 2010-01-01 instead of 1993
        status = nf90_put_att(file, NF90_GLOBAL, "TAI93At0zOfGranule", seconds + 536457600)
        status = nf90_put_att(file, NF90_GLOBAL, "NumTimes", track)

        ! add other hard coded attributes
        status = nf90_put_att(file, NF90_GLOBAL, "comment", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "DataSetQuality", "Excellent data quality")
        status = nf90_put_att(file, NF90_GLOBAL, "DayNightFlag", "Day")
        status = nf90_put_att(file, NF90_GLOBAL, "Format", "netCDF-4")
        status = nf90_put_att(file, NF90_GLOBAL, "FOVResolution", "13kmx24km")
        status = nf90_put_att(file, NF90_GLOBAL, "IdentifierProductDOI", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "IdentifierProductDOIAuthority", "https://doi.org/")
        status = nf90_put_att(file, NF90_GLOBAL, "institution", "NASA/GSFC")
        status = nf90_put_att(file, NF90_GLOBAL, "LocalityValue", "Global")
        status = nf90_put_att(file, NF90_GLOBAL, "LongName", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km")
        status = nf90_put_att(file, NF90_GLOBAL, "ParameterName", "OMI Ground Pixels UV1, UV2 and VIS")
        status = nf90_put_att(file, NF90_GLOBAL, "PGEName", "OMTO3")
        status = nf90_put_att(file, NF90_GLOBAL, "PlatformShortName", "Aura")
        status = nf90_put_att(file, NF90_GLOBAL, "ProductType", "L2 Swath")
        status = nf90_put_att(file, NF90_GLOBAL, "references", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "source", "Aura OMI")
        status = nf90_put_att(file, NF90_GLOBAL, "title", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km")
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleID", trim(destination))
        status = nf90_put_att(file, NF90_GLOBAL, "LocalGranuleID", trim(destination))
        status = nf90_put_att(file, NF90_GLOBAL, "SensorShortName", "CCD Ultraviolet, CCD Visible")
        status = nf90_put_att(file, NF90_GLOBAL, "ShortName", "OMTO3")

        ! close files
        status = nf90_close(file)
        status = nf90_close(fileii)

        return

    end subroutine write_global_attributes
end module m_write_global_attributes
