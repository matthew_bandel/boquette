module dateparse
  implicit none
  
  private
  public :: get_date, get_time

contains

  subroutine parse_iso_dt(datetime, date, time)
    character(len=20), intent(in) :: datetime
    character(len=10), intent(out) :: date
    character(len=8)  :: time
    integer :: year, month, day, hour, minute, second

    ! Extract year, month, day, hour, minute, and second from the datetime string
    read(datetime, "(i4,1x,i2,1x,i2,1x,i2,1x,i2,1x,i2)") year, month, day, hour, minute, second

    ! Convert year, month, and day to date string
    write(date, '(i4.4, "-", i2.2, "-", i2.2)') year, month, day

    ! Convert hour, minute, and second to time string
    write(time, '(i2.2, ":", i2.2, ":", i2.2)') hour, minute, second
  end subroutine parse_iso_dt

  function get_date(datetime) result(date)
    character(len=20), intent(in) :: datetime
    character(len=10) :: date
    character(len=8) :: time

    call parse_iso_dt(datetime, date, time)
  end function get_date

  function get_time(datetime) result(time)
    character(len=20), intent(in) :: datetime
    character(len=10) :: date
    character(len=8)  :: time

    call parse_iso_dt(datetime, date, time)
  end function get_time

end module dateparse
