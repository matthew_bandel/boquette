module m_write_integer_attribute

    ! declare public
    public

    ! declare contains
    contains

    ! write the OMTO3 global attributes
    subroutine write_integer_attribute(file, name, data, status)

        ! declare modules
        use HDF5
        use H5A

        ! declare implicits
        implicit none

        ! declare arguments
        integer(kind=4), intent(in) :: file
        character(len=*), intent(in) :: name
        integer(kind=4), intent(in) :: data
        integer(kind=4), intent(out) :: status

        ! declare variables
        integer (kind=4) :: attribute, space, datatype
        integer (kind=8), dimension(1) :: dimensions

        ! Create a scalar dataspace for the attribute
        dimensions(1) = 1
        call h5screate_simple_f(1, dimensions, space, status)

        ! Create the datatype
        call h5tcopy_f(H5T_NATIVE_INTEGER, datatype, status)
!        call h5tset_size_f(datatype, int(len(data), 8), status)

        ! Create a global attribute in the file
        call h5acreate_f(file, name, datatype, space, attribute, status)

        ! Write data to the attribute
        call h5awrite_f(attribute, datatype, data, dimensions, status)

        return

    end subroutine write_integer_attribute
end module m_write_integer_attribute
