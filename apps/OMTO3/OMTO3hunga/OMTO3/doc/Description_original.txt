#-------------------------------------------------------------------------------
APP Name:     OMTO3

APP Version:  2.5.12.1

APP Type:     OMI

Dependencies:
 
 - CI:        TO3_CORE
   Version:   2.5.8.1

 - CI:        OMI_L1B_Reader
   Version:   4.1.11

 - CI:        OMI_ANC_Reader
   Version:   0.0.4

Long Name:    OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km

Lead Algorithm Scientist:    P.K. Bhartia 
Other Algorithm Scientists:  ['Charlie Wellemeyer','David Haffner']
Software Developer:          ['David Haffner', 'Kai Yang', 'Matt Bandel']
Support Contact:             David Haffner

Description:  >

 TOMS algorithm for OMI to derive total column ozone and other parameters

Period:       Orbits=1

Output Files:

 - LUN:       449010
   ESDT:      OMTO3
   Filename:  OMI-Aura_L2-OMTO3_<StartTime!%Ym%m%dt%H%M%S>-o<OrbitNumber%06d>-<ProductionTime>.h5
   Desc:      OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km

Dynamic Input Files:

 - LUN:       1100
   ESDT:      OML1BRUG
   Desc:      OMI/Aura Level 1B UV Global Geolocated Earthshine Radiances

 - LUN:       10301
   ESDT:      LEAPSECT
   Desc:      Leap Seconds - ECS internal format for Toolkit

 - LUN:       500001
   ESDT:      OMCLDRR
   Desc:      OMI/Aura L2 Cloud Pressure and Fraction

 - LUN:       500110
   ESDT:      OMUANC
   Desc:      OMI/Aura Primary Ancillary Data Geo-Colocated to UV2

Static Input Files:

 - LUN:       1104
   Filename:  OMI-Aura_L1-OML1BIRR_2004m1231t1248-o002465_v0401-2022m0615t1814.nc
   Desc:      OMI/Aura Level 1B Averaged Solar Irradiances

 - LUN:       400100
   Filename:  BACKUP_IRR/OMI-Aura_L1-OML1BIRR_2004m1231t1248-o002465_v0401-2022m0615t1814.nc
   Desc:      Backup OMI/Aura Level 1B Averaged Solar Irradiances

 - LUN:       400121
   Filename:  CLIM/OZON_CLIM.txt
   Desc:      Ozone Climatology file

 - LUN:       400122
   Filename:  CLIM/TEMP_CLIM.txt
   Desc:      Temperature Climatology file

 - LUN:       400150
   Filename:  V8ANC/terrainPressure0303.he4
   Desc:      Terrain Pressure file

 - LUN:       400151
   Filename:  V8ANC/cloud_pressure.he4
   Desc:      Cloud Top Pressure Climatology file

 - LUN:       400152
   Filename:  V8ANC/v8snowice.he4
   Desc:      Snow and Ice Climatology file

 - LUN:       400400
   Filename:  LUT/NVAL.SO2_w12_RRS.h5
   Desc:      N-value table

 - LUN:       400401
   Filename:  LUT/DNDX.SO2_w12.h5
   Desc:      Layer Ozone Sensitivity Tables

 - LUN:       410100
   Filename:  OMTO3.MCF
   Desc:      Metadata Configuration File

 - LUN:       420001
   Filename:  OMI-Aura_L2-NVALC_TO3_v00051.he4
   Desc:      Orbital N-value correction file

 - LUN:       420002
   Filename:  OMNVVT_OMTO3_v00051.txt
   Desc:      Orbital N-value lookup table

Runtime Parameters:

 - LUN:       10220
   Param:     Toolkit version string
   Value:     'SCF  TK5.2.15'

 - LUN:       200100
   Param:     SMF Verbosity Threshold
   Value:     2

 - LUN:       200105
   Param:     PGEVERSION
   Value:     '"<PGEVersion>"'

 - LUN:       200110
   Param:     ProcessingCenter
   Value:     '"<ProcessingCenter>"'

 - LUN:       200115
   Param:     ProcessingHost
   Value:     '"<ProcessingHost>"'

 - LUN:       200135
   Param:     REPROCESSINGACTUAL
   Value:     '"<ReprocessingActual>"'

 - LUN:       200170
   Param:     ProcessLevel
   Value:     '"2"'

 - LUN:       200175
   Param:     InstrumentName
   Value:     '"OMI"'

 - LUN:       200180
   Param:     OPERATIONMODE
   Value:     '"Test"'

 - LUN:       200185
   Param:     AuthorAffiliation
   Value:     '"NASA/GSFC"'

 - LUN:       200190
   Param:     AuthorName
   Value:     '"U.S. OMI Science Team"'

 - LUN:       200195
   Param:     LOCALVERSIONID
   Value:     '"RFC1321 MD5 = not yet calculated"'

 - LUN:       200200
   Param:     OrbitNumber
   Value:     '<OrbitNumber>'

 - LUN:       200210
   Param:     SwathName
   Value:     '"OMI Column Amount O3"'

 - LUN:       400010
   Param:     wl_min
   Value:     '308.0'

 - LUN:       400020
   Param:     wl_mix
   Value:     '312.6'

 - LUN:       400030
   Param:     wl_oz
   Value:     '317.5'

 - LUN:       400040
   Param:     wl_reflLow
   Value:     '331.3'

 - LUN:       400050
   Param:     wl_reflHigh
   Value:     '360.1'

 - LUN:       400060
   Param:     wl_max
   Value:     '370.0'

 - LUN:       400090
   Param:     CLOUDPRESSURESOURCE
   Value:     '"OMCLDRR"'

 - LUN:       400091
   Param:     TERRAINPRESSURESOURCE
   Value:     '"ETOPO-5"'

 - LUN:       400092
   Param:     TEMPERATURESOURCE
   Value:     '"Climatology"'

 - LUN:       400093
   Param:     SNOWICESOURCE
   Value:     '"Climatology"'

 - LUN:       400094
   Param:     APRIORIOZONEPROFILESOURCE
   Value:     '"Climatology"'

 - LUN:       123456
   Param:     VERSIONID
   Value:     "<ECSCollection>"
   Desc:      ECS Collection Number.

 - Param:     AppShortName
   Value:     OMTO3

 - Param:     AuthorName
   Value:     David Haffner

 - Param:     Processing Center
   Value:     OMIDAP

 - Param:     Instrument
   Value:     OMI

 - Param:     Satellite
   Value:     Aura

 - Param:     Source
   Value:     OMI-Aura

 - Param:     OrbitNumber
   Value:     '<OrbitNumber>'

 - Param:     StartTime
   Value:     '<StartTime>'

 - Param:     EndTime
   Value:     '<EndTime>'

 - Param:     Year
   Value:     '<Year>'

 - Param:     Day
   Value:     '<Day>'

 - Param:     OMI_collection
   Value:     4

 - Param:     MaximumLevelofDetail
   Value:     6
   
Production Rules:

 - Rule:      GetOrbitParams

 - Rule:      OrbitMatch
   ESDT:      OML1BRUG
   Min_files: 1
   InstrumentConfig: '0, 1, 2, 7'
   
 - Rule:      OrbitMatch
   ESDT:      OMCLDRR

 - Rule:      OrbitMatch
   ESDT:      OMUANC

 - Rule:      Common
   ESDT:      LEAPSECT

Compilers:

 - CI:        gcc
   Version:   9.4.0

Environment:

 - CI:        env_setup
   Version:   2.0.4

 - CI:        HDF-EOS4
   Version:   2.19

 - CI:        HDF5
   Version:   5-1.8.19

 - CI:        HDF-EOS5
   Version:   5-1.15

 - CI:        PGSTK
   Version:   5.2.20v1.00

Operating System:

 - CI:        Linux
   Version:   5.4.0

 - CI:        make
   Version:   4.2.1

#-------------------------------------------------------------------------------
