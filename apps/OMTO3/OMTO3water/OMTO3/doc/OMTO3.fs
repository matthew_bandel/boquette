Shortname:    OMTO3
Longname:     OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km
PFS Version:  2.5.13
Date:         21 Nov 2024
Author(s):    Kai Yang (UMBC), Charles Wellemeyer (SSAI), David Haffner (SSAI), Peter Leonard (SSAI)

PGE Version:                  2.5.13
Lead Algorithm Scientist:     Pawan K. Bhartia (NASA/GSFC)
Lead Algorithm Developer(s):  Charlie Wellemeyer (SSAI), Kai Yang (UMBC), David Haffner (SSAI)
Lead PGE Developer:           David Haffner (SSAI), Kai Yang (UMBC)
PGE Developer:                Shifang Luo (SSAI)

Description: >

 This document specifies the product format for the OMTO3 Level 2 PGE,
 which uses the V8 TOMS algorithm to estimate total column ozone from OMI
 UV-2 measurements (Reference 1).  The product is stored as one HDF-EOS 5
 swath file for each granule (i.e., one orbit) of OMI Level 1B data, and
 has a size range of 5 to 500 Mb.

Global Metadata:

 - Metadata Name:     AuthorAffiliation
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            NASA/GSFC
   Data Source:       PCF
   Description:       Example is "NASA/GSFC".

 - Metadata Name:     AuthorName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            U.S. OMI Science Team
   Data Source:       PCF
   Description:       Example is "U.S. OMI Science Team".

 - Metadata Name:     GranuleDay
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     31
   Data Source:       PGE
   Description:       The day of the month at the start of the granule.

 - Metadata Name:     GranuleMonth
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     12
   Data Source:       PGE
   Description:       The month at the start of the granule.

 - Metadata Name:     GranuleYear
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     2000
   Maximum Value:     2099
   Data Source:       PGE
   Description:       The (four-digit) year at the start of the granule.

 - Metadata Name:     HDFEOSVersion
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       HE
   Description:       Example is "HDFEOS_5.1.5".

 - Metadata Name:     InputVersions
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PGE
   Description: >
    A list of every ESDT (including version) whose product was used as input
    for the processing.

 - Metadata Name:     InstrumentName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI
   Data Source:       PCF
   Description:       Actual is "OMI" (see Section 6.1 of Reference 2).

 - Metadata Name:     OrbitData
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            DEFINITIVE,PREDICTED
   Data Source:       L1B
   Description: >
    Indicates whether orbit data used by the Level 1B processor is definitive
    or predicted.

 - Metadata Name:     PGEVERSION
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PCF
   Description:       Example is "1.1.0" (see Appendix K of Reference 3).

 - Metadata Name:     ProcessingCenter
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMIDAPS,TLCF
   Data Source:       PCF
   Description:       Example is "OMIDAPS".

 - Metadata Name:     ProcessingHost
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The output from executing the Unix "uname -a" command on the processing
    machine.

 - Metadata Name:     ProcessLevel
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            2
   Data Source:       PCF
   Description:       Actual is "2".

 - Metadata Name:     TAI93At0zOfGranule
   Mandatory:         T
   Data Type:         HE5T_NATIVE_DOUBLE
   Number of Values:  1
   Minimum Value:     0.0
   Maximum Value:     1.0e+30
   Data Source:       PGE
   Description: >
    The TAI93 time at 0z of the granule (see Section 6.1 of Reference 2).

Swath Metadata:

 - Metadata Name:     EarthSunDistance
   Mandatory:         T
   Data Type:         HE5T_NATIVE_FLOAT
   Number of Values:  1
   Minimum Value:     1.47e+11
   Maximum Value:     1.53e+11
   Data Source:       L1B
   Description: >
    The Earth-sun distance (in m) at the time of the radiance measurement.

 - Metadata Name:     NumTimes
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of "scan" lines in the swath.

 - Metadata Name:     NumTimesSmallPixel
   Mandatory:         T
   Data Type:         HE5T_NATIVE_INT
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of small pixel "scan" lines in the swath.

 - Metadata Name:     SwathName
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            OMI Column Amount O3
   Data Source:       PGE
   Description:       Actual is "OMI Column Amount O3".

 - Metadata Name:     VerticalCoordinate
   Mandatory:         T
   Data Type:         HE5T_NATIVE_CHAR
   Number of Values:  1
   Valids:            Total Column
   Data Source:       PGE
   Description:       Actual is "Total Column" (see Section 6.2 of Reference 2).

Swath Dimensions:

 - Dimension Name:    nLayers
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     11
   Data Source:       PGE
   Description:       The number of layers in the ozone profile per ground pixel.

 - Dimension Name:    nTimes
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of "scan" lines in the swath.

 - Dimension Name:    nTimesSmallPixel
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of small pixel "scan" lines in the swath.

 - Dimension Name:    nWavel
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     500
   Data Source:       PGE
   Description:       The number of wavelengths per ground pixel.

 - Dimension Name:    nXtrack
   Data Type:         HE5T_NATIVE_INT
   Dimension Type:    FIXED
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     60
   Data Source:       L1B
   Description:       The number of ground pixels per "scan" line.

Geolocation Fields:

 - Field Name:               GroundPixelQualityFlags
   Data Type:                HE5T_NATIVE_UINT16
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Title:                    Ground Pixel Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >

   Bits 0 to 7 are ground pixel quality flags, same as in the L1B data, that are set to 0 for FALSE, or 1 for TRUE:

      Bit 0 - solar eclipse flag
      Bit 1 - sun glint possibility flag
      Bit 2 - descending side of orbit flag
      Bit 3 - night side of orbit flag
      Bit 4 - geometric boundary ( for instance dateline ) crossing flag
      Bit 5 - not used
      Bit 6 - not used
      Bit 7 - geolocation error

 - Field Name:               LandWaterClassification
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Title:                    Land Water Classification
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >

   Values 0 to 7 are land / water classification indicators, from the L1B data:

      0 - shallow ocean
      1 - land
      2 - ocean coastline lake shoreline
      3 - shallow inland water
      4 - intermittent water
      5 - deep inland water
      6 - continental shelf ocean
      7 - deep ocean

 - Field Name:               Latitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -90.0
   Maximum Value:            90.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Geodetic Latitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The geodetic latitude (in deg) at the center of the ground pixel.

 - Field Name:               Longitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Geodetic Longitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The geodetic longitude (in deg) at the center of the ground pixel.

 - Field Name:               RelativeAzimuthAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg(EastofNorth)
   Data Source:              L1B
   Title:                    Relative Azimuth Angle (sun + 180 - view)
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The relative (sun + 180 - view) azimuth angle (in deg) at the center of
    the ground pixel.

 - Field Name:               SecondsInDay
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nTimes
   Minimum Value:            0.0
   Maximum Value:            86401.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    s
   Data Source:              L1B
   Title:                    Seconds after UTC midnight
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The time (in s) after UTC midnight at the start of the "scan".

 - Field Name:               SolarAzimuthAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg(EastofNorth)
   Data Source:              L1B
   Title:                    Solar Azimuth Angle
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The solar azimuth angle (in deg) at the center of the ground pixel.

 - Field Name:               SolarZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Solar Zenith Angle
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The solar zenith angle (in deg) at the center of the ground pixel.

 - Field Name:               SpacecraftAltitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nTimes
   Minimum Value:            4.0e+05
   Maximum Value:            9.0e+05
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    m
   Data Source:              L1B
   Title:                    Spacecraft Altitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description:              Height above WGS84 ellipsoid.

 - Field Name:               SpacecraftLatitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nTimes
   Minimum Value:            -90.0
   Maximum Value:            90.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Spacecraft Latitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description:              Geodetic latitude above WGS84 ellipsoid.

 - Field Name:               SpacecraftLongitude
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nTimes
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Spacecraft Longitude
   Unique Field Definition:  TOMS-Aura-Shared
   Description:              Geodetic longitude above WGS84 ellipsoid.

 - Field Name:               TerrainHeight
   Data Type:                HE5T_NATIVE_INT16
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -200
   Maximum Value:            10000
   Missing Value:            -32767
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    m
   Data Source:              L1B
   Title:                    Terrain Height
   Unique Field Definition:  TOMS-Aura-Shared
   Description: >
    The terrain height (in m) at the center of the ground pixel (from the
    OMI Level 1B file).

 - Field Name:               Time
   Data Type:                HE5T_NATIVE_DOUBLE
   Dimensions:               nTimes
   Minimum Value:            -5.0e+09
   Maximum Value:            1.0e+10
   Missing Value:            -1.2676506002282294e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    s
   Data Source:              L1B
   Title:                    Time at Start of Scan (TAI93)
   Unique Field Definition:  TOMS-Aura-Shared
   Description:              The TAI93 time (in s) at the start of the "scan".

 - Field Name:               ViewingAzimuthAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -180.0
   Maximum Value:            180.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg(EastofNorth)
   Data Source:              L1B
   Title:                    Viewing Azimuth Angle
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The viewing azimuth angle (in deg) at the center of the ground pixel.

 - Field Name:               ViewingZenithAngle
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            70.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    deg
   Data Source:              L1B
   Title:                    Viewing Zenith Angle
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The viewing zenith angle (in deg) at the center of the ground pixel.

 - Field Name:               XTrackQualityFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Cross Track Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The cross track quality flags assigned to each pixel in  
    OMI L1B data. Flags indicate detection of the OMI row 
    anomaly by analysis of the L1B data. 

    Bits 0 to 2 together indicate row anomaly status: 
      0 - Not affected
      1 - Affected, Not corrected, do not use
      2 - Slightly affected, not corrected, use with caution
      3 - Affected, corrected, use with caution
      4 - Affected, corrected, use pixel
      5 - Not used
      6 - Not used
      7 - Error during anomaly detection processing
    Bit 3 - Reserved for future use.
    Bit 4 - Possibly affected by wavelength shift
    Bit 5 - Possibly affected by blockage
    Bit 6 - Possibly affected by stray sunlight
    Bit 7 - Possibly affected by stray earthshine

 - Field Name:               WaterFraction
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            254
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Title:                    Water Fraction
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >

   The approximate percent of ground pixel covered by water

Data Fields:

 - Field Name:               AlgorithmFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            13
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Algorithm Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The algorithm flag associated with the ground pixel:
      0 - skipped
      1 - standard
      2 - adjusted for profile shape
      3 - based on C-pair (331 and 360 nm)
      Add 10 for snow/ice.
 
 - Field Name:               APrioriLayerO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nLayers,nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            125.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    A Priori Ozone Profile
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The apriori layer ozone values. 

 - Field Name:               CalibrationAdjustment
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack
   Minimum Value:            -10.0
   Maximum Value:             10.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Calibration Adjustment
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              Calibration Adjustment expressed as N-values

 - Field Name:               RadiativeCloudFraction
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Radiative Cloud Fraction
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The radiative cloud fraction.

 - Field Name:               fc
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    MLER Cloud Fraction
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The MLER model parameter: effective cloud fraction.

 - Field Name:               CloudPressure
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1013.25
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    hPa
   Data Source:              PGE
   Title:                    Effective Cloud Pressure
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The cloud top pressure (in hPa) associated with the ground pixel.

 - Field Name:               ColumnAmountO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    Best Total Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The best total ozone solution (in DU) associated with the ground pixel.
 
 - Field Name:               dN_dR
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            -200.0
   Maximum Value:            0.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Reflectivity Sensitivity Ratio, dN/dR
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The reflectivity sensitivity ratio, dN/dR.

 - Field Name:               dN_dT
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Ozone Weighted Temperature Sensitivity Ratio, dN/dT
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The ozone weighted temperature sensitivity ratio, dN/dT.

 - Field Name:               InstrumentConfigurationId
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nTimes
   Minimum Value:            0
   Maximum Value:            200
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Unique Field Definition:  OMI-Specific
   Title:                    Instrument Configuration ID
   Description: >
    Instrument Configuration ID as defined by OMIS operations.

 - Field Name:               LayerEfficiency
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nLayers,nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            10.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Algorithmic Layer Efficiency
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The algorithmic layer efficiency.

 - Field Name:               MeasurementQualityFlags
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nTimes
   Minimum Value:            0
   Maximum Value:            65534
   Missing Value:            65535
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Measurement Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >

    The measurement quality flags are the same as in the L1B data, associated with each "scan" line (Bit value
    is 0 for not set and 1 for set):

       Bit 0  - proc_skipped, one or more processing steps were skipped
       Bit 1  - quality_warning, instrument settings or L1b correction parameters had unexpected values
       Bit 2  - thermal_instability, instrument was outside its nominal ( stable ) temperature
       Bit 3  - alteng, engineering data was not available and engineering data from an adjacent measurement was used
       Bit 4  - saa, measurement was obtained while in South Atlantic Anomaly
       Bit 5  - spacecraft_manoeuvre, measurement was obtained during spacecraft manoeuvre
       Bit 6  - shadow_umbra, spacecraft is in the umbral shadow of the Earth
       Bit 7  - shadow_penumbra, spacecraft is in the penumbral shadow of the Earth
       Bit 8  - irr_out_of_range, measurement outside nominal elevation / azimuth range
       Bit 9  - sub_group, measurement was flagged as sub-group by subgroup algorithm
       Bit 10 - msmtcomb, multiple measurements were combined into a single measurement
       Bit 11 - unused
       Bit 12 - coaderr, co-adder error flag
       Bit 13 - coadov, coaddition overflow possibility warning
       Bit 14 - test, instrument test mode
       Bit 15 - alt_seq, alternating sequencing readout flag

 - Field Name:               NumberSmallPixelColumns
   Data Type:                HE5T_NATIVE_UINT8
   Dimensions:               nTimes
   Minimum Value:            0
   Maximum Value:            5
   Missing Value:            255
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Title:                    Number of Small Pixel Columns
   Unique Field Definition:  OMI-Specific
   Description: >
    NumberSmallPixelColumns is either 0 (for no or invalid small pixel column
    selected) or equal to the number of co-additions.

 - Field Name:               NValue
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            600.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Measured N-Value
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The measured N-value.

 - Field Name:               O3BelowCloud
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            100.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    Ozone Below Fractional Cloud
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The ozone below fractional cloud (DU).

 - Field Name:               QualityFlags
   Data Type:                HE5T_NATIVE_UINT16
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            65534
   Missing Value:            65535
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Quality Flags
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    Bits 0 to 2 together contain several output error flags:
      0 - good sample
      1 - glint contamination (corrected)
      2 - sza > 84 (degree)
      3 - 360 residual > threshold
      4 - residual at unused ozone wavelength > 4 sigma
      5 - SOI > 4 sigma (SO2 present)
      6 - non-convergence
      7 - abs(residual) > 16.0 (fatal)

    Bit 3 - set to 0 for ascending data,
            set to 1 for descending data.

    Bits 4 to 5 are reserved for future use (currently set to 0).

    Bit 6 - set to 0 when row anomlay error has not been detected,
            set to 1 when row anomaly error has been found.           

    Bit 7 - set to 0 when OMI cloud (OMCLDRR or OMCLDO2) pressure is used,
            set to 1 when climatolgical cloud pressure is used. 

    Bits 8 to 15 are flags that are set to 0 for FALSE (good value), or
    1 for TRUE (bad value):
      Bit  8 - geolocation error (anomalous FOV Earth location)
      Bit  9 - sza > 88 (degree)
      Bit 10 - missing input radiance
      Bit 11 - error input radiance
      Bit 12 - warning input radiance
      Bit 13 - missing input irradiance
      Bit 14 - error input irradiance
      Bit 15 - warning input irradiance

 - Field Name:               RadianceBadPixelFlagAccepted
   Data Type:                HE5T_NATIVE_UINT16
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0
   Maximum Value:            65534
   Missing Value:            65535
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Radiance Bad Pixel Flag Accepted
   Unique Field Definition:  OMI-Specific
   Description: >
    Indicates the wavelength(s) in the algorithm for which the field radiance PixelQualityFlags 
    was set to BAD_PIXEL (Reference 4, p. 34) in the L1b input. Bits are indexed starting 
    at zero for the first wavelength in the "Wavelength" data field.  In all cases, the 
    algorithm proceededs with retrieval despite occurence of BAD_PIXEL flags on radiance pixels.

 - Field Name:               Reflectivity331
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -15.0
   Maximum Value:            115.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    "%"
   Data Source:              PGE
   Title:                    Effective Surface Reflectivity at 331 nm
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The effective surface reflectivity at 331 nm (percent).

 - Field Name:               Reflectivity360
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -15.0
   Maximum Value:            115.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    "%"
   Data Source:              PGE
   Title:                    Effective Surface Reflectivity at 360 nm
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The effective surface reflectivity at 360 nm (percent).

 - Field Name:               Residual
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            -32.0
   Maximum Value:            32.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    N-Value Residual
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The N-value residual.

 - Field Name:               ResidualStep1
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            -32.0
   Maximum Value:            32.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Step 1 N-Value Residual
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The step 1 N-value residual.

 - Field Name:               ResidualStep2
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            -32.0
   Maximum Value:            32.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Step 2 N-Value Residual
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The step 2 N-value residual.

 - Field Name:               Sensitivity
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel,nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    Ozone Sensitivity Ratio, dN/dOmega
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The ozone sensitivity ratio, dN/dOmega.

 - Field Name:               SmallPixelColumn
   Data Type:                HE5T_NATIVE_INT16
   Dimensions:               nTimes
   Minimum Value:            0
   Maximum Value:            1000
   Missing Value:            -32767
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              L1B
   Title:                    Small Pixel Column
   Unique Field Definition:  OMI-Specific
   Description: >
    Column number on the CCD for which the pixels are additionally
    transmitted without co-addition.

 - Field Name:               SO2index
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -300.0
   Maximum Value:            300.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    SO2 Index
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The SO2 index associated with the ground pixel.

 - Field Name:               StepOneO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    Step 1 Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The step 1 ozone solution (in DU) associated with the ground pixel.

 - Field Name:               StepTwoO3
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            50.0
   Maximum Value:            700.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    DU
   Data Source:              PGE
   Title:                    Step 2 Ozone Solution
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The step 2 ozone solution (in DU) associated with the ground pixel.

 - Field Name:               TerrainPressure
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            0.0
   Maximum Value:            1013.25
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    hPa
   Data Source:              PGE
   Title:                    Terrain Pressure
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The terrain pressure at the center of the ground pixel (from the V8 TOMS
    algorithm).

 - Field Name:               UVAerosolIndex
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nXtrack,nTimes
   Minimum Value:            -30.0
   Maximum Value:            30.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    NoUnits
   Data Source:              PGE
   Title:                    UV Aerosol Index
   Unique Field Definition:  TOMS-OMI-Shared
   Description: >
    The UV aerosol index associated with the ground pixel.

 - Field Name:               Wavelength
   Data Type:                HE5T_NATIVE_FLOAT
   Dimensions:               nWavel
   Minimum Value:            300.0
   Maximum Value:            400.0
   Missing Value:            -1.2676506e+30
   Offset:                   0.0
   Scale Factor:             1.0
   Units:                    nm
   Data Source:              PGE
   Title:                    Wavelength
   Unique Field Definition:  TOMS-OMI-Shared
   Description:              The wavelength used in the OMTO3 algorithm.

Core Metadata:

 - Metadata Name:     AssociatedInstrumentShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            OMI
   Data Source:       MCF
   Description:       Actual is "OMI".

 - Metadata Name:     AssociatedPlatformShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            Aura
   Data Source:       MCF
   Description:       Actual is "Aura".

 - Metadata Name:     AssociatedSensorShortName
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            CCD Ultra Violet,CCD Visible
   Data Source:       MCF
   Description:       Actual is "CCD Ultra Violet".

 - Metadata Name:     AutomaticQualityFlag
   Mandatory:         T
   Data Type:         VA64
   Number of Values:  1
   Valids:            Passed,Suspect,Failed
   Data Source:       PGE
   Description: >
    A granule-level quality flag that applies generally to the granule and
    specifically to the parameters at the granule level.
 
 - Metadata Name:     AutomaticQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       PGE
   Description: >
    The AutomaticQualityFlag is set to
    1) "Passed" if QAPercentHighQualityData >= 90%,
    2) "Suspect" if QAPercentHightQualityData >= 60% or if the input L1B file
        does not have its AutomaticQualityFlag set to "Passed",
    and
    3) "Failed" if QAPercentHighQualityData < 60%.

 - Metadata Name:     DayNightFlag
   Mandatory:         T
   Data Type:         VA5
   Number of Values:  1
   Valids:            Day,Night,Both
   Data Source:       MCF
   Description:       Actual is "Day".

 - Metadata Name:     EquatorCrossingDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1
   Data Source:       L1B
   Description:       The date of the ascending equator crossing in the granule.

 - Metadata Name:     EquatorCrossingLongitude
   Mandatory:         T
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     -180.0
   Maximum Value:     180.0
   Data Source:       L1B
   Description: >
    The terrestrial longitude of the ascending equator crossing in the granule.

 - Metadata Name:     EquatorCrossingTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1
   Data Source:       L1B
   Description:       The time of the ascending equator crossing in the granule.

 - Metadata Name:     InputPointer
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  0,10
   Data Source:       PGE
   Description: >
    Example is
    ("OMI-Aura_L1-OML1BRUG_2002m0630t2354-o21434_v001-2003m0327t181402.he4",
    "OMI-Aura_L1-OML1BIRR_2002m0630t2354-o21434_v001-2003m0327t181812.he4",
    "OZON_CLIM.txt", "TEMP_CLIM.txt", "terrain_pressure.he4",
    "cloud_pressure.he4", "NVAL_LUT_omiTOMS_RRS.h5", "DNDX_LUT_omiTOMS.h5").

 - Metadata Name:     LocalGranuleID
   Mandatory:         T
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PGE
   Description: >
    Example is
    "OMI-Aura_L1-OMTO3_2002m0630t2354-o21434_v001-2003m0515t181917.he5"
    (see Appendix E of Reference 3).

 - Metadata Name:     LOCALVERSIONID
   Mandatory:         T
   Data Type:         VA60
   Number of Values:  1
   Data Source:       PCF
   Description: >
    MD5 fingerprint of the HDF product file.  Example valids are
    "RFC1321 MD5 = not yet calculated" and "RFC1321 MD5 = [0-9,a-f]{32}".

 - Metadata Name:     OperationalQualityFlag
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       PGE
   Description: >
    A granule-level quality flag that applies generally to the granule and
    specifically to the parameters at the granule level.

 - Metadata Name:     OperationalQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       PGE
   Description: >
    The criteria for setting the OperationalQualityFlag should be stated here
    (this Metadata will not appear in the granule).

 - Metadata Name:     OPERATIONMODE
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids:            Normal,Test
   Data Source:       PCF
   Description:       Example is "Normal" (not implemented yet).

 - Metadata Name:     OrbitNumber
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     1
   Maximum Value:     999999
   Data Source:       L1B
   Description:       The OMI orbit number.

 - Metadata Name:     ParameterName
   Mandatory:         T
   Data Type:         VA40
   Number of Values:  1
   Valids:            Total Column Ozone
   Data Source:       PGE
   Description:       The measured science parameter expressed in the granule.

 - Metadata Name:     PGEVERSION
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Data Source:       PCF
   Description:       Example is "0.9.33" (see Appendix K of Reference 3).

 - Metadata Name:     ProductionDateTime
   Mandatory:         T
   Data Type:         DT
   Number of Values:  1
   Data Source:       TK
   Description:       The date and time of the Level 2 processing.

 - Metadata Name:     QAPercentMissingData
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    The percent of missing data from Level 1B calibrated radiance needed by
    OMTO3 PGE data per granule. 

 - Metadata Name:     RangeBeginningDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1
   Data Source:       L1B
   Description:       The year, month and day when the granule began.

 - Metadata Name:     RangeBeginningTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1
   Data Source:       L1B
   Description: >
    The hour, minute, second and fraction of a second when the granule began.

 - Metadata Name:     RangeEndingDate
   Mandatory:         T
   Data Type:         D
   Number of Values:  1
   Data Source:       L1B
   Description:       The year, month and day when the granule ended.

 - Metadata Name:     RangeEndingTime
   Mandatory:         T
   Data Type:         T
   Number of Values:  1
   Data Source:       L1B
   Description: >
    The hour, minute, second and fraction of a second when the granule ended.

 - Metadata Name:     REPROCESSINGACTUAL
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    processed 1 time,processed 2 times,processed 3 times,processed 4 times
   Data Source:       PCF
   Description: >
    An indication of what reprocessing has been performed on the granule.
 
 - Metadata Name:     ReprocessingPlanned
   Mandatory:         T
   Data Type:         VA45
   Number of Values:  1
   Valids: >
    no further update anticipated,further update is anticipated,
    further update anticipated using enhanced PGE
   Data Source:       DP
   Description:       Actual is "further update is anticipated".

 - Metadata Name:     ScienceQualityFlag
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       DP
   Description:       Actual is "Not Investigated".

 - Metadata Name:     ScienceQualityFlagExplanation
   Mandatory:         T
   Data Type:         VA255
   Number of Values:  1
   Data Source:       DP
   Description: >
    An explanation of the criteria used to set the science quality flag should
    go here.

 - Metadata Name:     ShortName
   Mandatory:         T
   Data Type:         VA8
   Number of Values:  1
   Valids:            OMTO3
   Data Source:       MCF
   Description:       Actual is "OMTO3".

 - Metadata Name:     SizeMBECSDataGranule
   Mandatory:         F
   Data Type:         LF
   Number of Values:  1
   Minimum Value:     0.0
   Maximum Value:     10000.0
   Data Source:       DSS
   Description: >
    The volume of data contained in the granule in Mb (this Metadata will not
    appear in the granule).

 - Metadata Name:     VersionID
   Mandatory:         T
   Data Type:         SI
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     999
   Data Source:       MCF
   Description:       Actual is 1 for test and pre-launch.

Product Specific Attributes:

 - Metadata Name:     EndBlockNr
   Mandatory:         T
   Data Type:         SI
   Number of Values:  1,500
   Minimum Value:     1
   Maximum Value:     50
   Data Source:       L1B
   Description:       The number of the NOSE end block along the track.

 - Metadata Name:     ExpeditedData
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       L1B
   Description:       The indicator for expedited Level 0 data.

 - Metadata Name:     ExposureTimes
   Mandatory:         T
   Data Type:         F
   Number of Values:  1,256
   Minimum Value:     0.0
   Maximum Value:     2000.0
   Data Source:       L1B
   Description: >
    An array containing the exposure times in seconds used for the measurements.

 - Metadata Name:     InstrumentConfigurationIDs
   Mandatory:         T
   Data Type:         SI
   Number of Values:  1,256
   Minimum Value:     0
   Maximum Value:     255
   Data Source:       L1B
   Description: >
    An array containing the instrument configuration identifiers used for the
    measurements.

 - Metadata Name:     MasterClockPeriods
   Mandatory:         T
   Data Type:         F
   Number of Values:  1,128
   Minimum Value:     0.0
   Maximum Value:     10.0
   Data Source:       L1B
   Description: >
    An array containing the master clock periods in seconds used for the
    measurements.

 - Metadata Name:     NrMeasurements
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description: >
    The number of measurements in the granule (per output product).

 - Metadata Name:     NrSpatialZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of measurements in spatial zoom mode.

 - Metadata Name:     NrSpectralZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of measurements in spectral zoom mode.

 - Metadata Name:     NrZoom
   Mandatory:         T
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     9999
   Data Source:       L1B
   Description:       The number of measurements in zoom modes.

 - Metadata Name:     PathNr
   Mandatory:         T
   Data Type:         I
   Number of Values:  1,500
   Minimum Value:     1
   Maximum Value:     466
   Data Source:       L1B
   Description:       Number of the NOSE path within the repeat cycle.

 - Metadata Name:     SolarEclipse
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       L1B
   Description: >
    The indicator that during part of the measurements a solar eclipse occurred.

 - Metadata Name:     SouthAtlanticAnomalyCrossing
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE
   Data Source:       L1B
   Description: >
    The indicator that during part of the measurements the spacecraft was in
    the South Atlantic Anomaly.

 - Metadata Name:     SpacecraftManeuverFlag
   Mandatory:         T
   Data Type:         VA10
   Number of Values:  1
   Valids:            TRUE,FALSE,UNKNOWN
   Data Source:       L1B
   Description: >
    The indicator that during part of the measurements the spacecraft was
    performing a maneuver.

 - Metadata Name:     StartBlockNr
   Mandatory:         T
   Data Type:         SI
   Number of Values:  1,500
   Minimum Value:     1
   Maximum Value:     50
   Data Source:       L1B
   Description:       Number of the NOSE start block along the track.

Archived Metadata:

 - Metadata Name:     AlgorithmPath1QualityFlagCounts
   Mandatory:         F
   Data Type:         I
   Number of Values:  16
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples that are processed through algorithm path 1
    that have QualityFlags set in the respective categories.

 - Metadata Name:     AlgorithmPath2QualityFlagCounts
   Mandatory:         F
   Data Type:         I
   Number of Values:  16
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples that are processed through algorithm path 2
    that have QualityFlags set in the respective categories.

 - Metadata Name:     AlgorithmPath3QualityFlagCounts
   Mandatory:         F
   Data Type:         I
   Number of Values:  16
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples that are processed through algorithm path 3
    that have QualityFlags in the respective categories.

 - Metadata Name:     APrioriOzoneProfileSource
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The source of apriori ozone profile used in derived total ozone.  Example
    is "Climatology".

 - Metadata Name:     BackupSolarProductUsed
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1
   Data Source:       PGE
   Description: >
    Set to 1 if the backup Solar product is used instead of the normal solar
    product.

 - Metadata Name:     CloudPressureSource
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The source of cloud top pressure used in derived total ozone.  Examples
    are "Climtology", "OMCLDRR Cloud Pressure" and "OMCLDO2 Cloud Pressure".

 - Metadata Name:     ESDTDescriptorRevision
   Mandatory:         T
   Data Type:         VA20
   Number of Values:  1
   Data Source:       MCF
   Description: >
    The version of the ESDT descriptor file as determined by ECS.

 - Metadata Name:     InstrumentConfigurationIDsNonCompatible
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Valids:            TRUE
   Data Source:       L1B
   Description: >
    Set to TRUE if NumberOfInstrumentSettingsError is greater than 0.
    Otherwise it would not appear in the Archived Metadata.

 - Metadata Name:     IrradianceSceinceQualityFlag
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       L1B
   Description: >
    Set to the value for the radiance product ScienceQualityFlag attribute.

 - Metadata Name:     LongName
   Mandatory:         T
   Data Type:         VA80
   Number of Values:  1
   Valids:            OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km
   Data Source:       MCF
   Description: >
    Actual is "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km"
    (see Section 7.0 of Reference 2).

 - Metadata Name:     NumberOfBadInputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of bad samples out of NumberOfInputSamples.  The error
    flag is set for these samples.

 - Metadata Name:     NumberOfGlintCorrectedSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of glint corrected samples in the ascending part of the
    orbit.  QualityFlags is set to 1 for these samples.

 - Metadata Name:     NumberOfGoodInputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of good samples out of NumberOfInputSamples in the
    ascending part of the orbit.  No error, warning or geolocation flags
    are set for these samples.

 - Metadata Name:     NumberOfGoodOutputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of good samples in the ascending part of the orbit.
    QualityFlags is set to 0 for these samples.

 - Metadata Name:     NumberOfInputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE 
   Description: >
    The total number of samples read from the Level 1B file for processing.
    In general this is equal to nXtrack * nTimes .

 - Metadata Name:     NumberOfInputWarningSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of warning samples out of NumberOfInputSamples.  The
    warning flag is set for these samples.

 - Metadata Name:     NumberOfLargeSZAInputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples out of NumberOfGoodInputSamples with a Solar
    Zenith Angle (SZA) greater or equal to 84.0 degrees in the ascending part
    of the orbit.

 - Metadata Name:     NumberOfMissingInputSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of missing samples out of NumberOfInputSamples.

 - Metadata Name:     NumberOfSkippedSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description:       The total number of samples that are skipped.

 - Metadata Name:     NumberOfStep1InvalidSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples with errors detected in step one processing.

 - Metadata Name:     NumberOfStep2InvalidSamples
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100000
   Data Source:       PGE
   Description: >
    The total number of samples with errors detected in step two processing.

 - Metadata Name:     QAPctInstrumentSettingsError
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 5 is set.

 - Metadata Name:     QAPctIrradianceError
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 14 is set.

 - Metadata Name:     QAPctIrradianceMissing
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 13 is set.

 - Metadata Name:     QAPctIrradianceWarning
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 15 is set.

 - Metadata Name:     QAPctMeasurementError
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 1 is set.

 - Metadata Name:     QAPctMeasurementManeuver
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 5 is set.

 - Metadata Name:     QAPctMeasurementMissing
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 0 is set.

 - Metadata Name:     QAPctMeasurementRebinned
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 3 is set.

 - Metadata Name:     QAPctMeasurementSAA
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 4 is set.

 - Metadata Name:     QAPctMeasurementWarning
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of "scan" lines for which L2 MeasurementQualityFlags bit 2 is set.

 - Metadata Name:     QAPctRadianceError
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 11 is set.

 - Metadata Name:     QAPctRadianceMissing
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 10 is set.

 - Metadata Name:     QAPctRadianceWarning
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    Percent of ground pixels for which L2 QualityFlags bit 12 is set.

 - Metadata Name:     QAPercentHighQualityData 
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     100
   Data Source:       PGE
   Description: >
    QAPercentHighQualityData = 100 *
    (NumberOfGoodOutputSamples + NumberOfGlintCorrectedSamples) /
    (NumberOfGoodInputSamples - NumberOfLargeSZAInputSamples).

 - Metadata Name:     RadianceScienceQualityFlag
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Valids: >
    Passed,Failed,Being Investigated,Not Investigated,Inferred Passed,
    Inferred Failed,Suspect
   Data Source:       L1B
   Description: >
    Set to the value for the radiance product ScienceQualityFlag metadata
    attribute.

 - Metadata Name:     SnowIceSource
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The source of Snow/Ice coverage used in derived total ozone.  Example is
    "NISE from OML1BRUG".

 - Metadata Name:     SolarIrradianceWarning
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1
   Data Source:       PGE
   Description:       Set to 1 if NumberOfIrradianceWarning is larger than 0.

 - Metadata Name:     SolarProductMissing
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1
   Data Source:       PGE
   Description: >
    Set to 1 if Normal Solar product could not be opened, or when there is
    fatal error in reading the solar irradiance file.

 - Metadata Name:     SolarProductOutOfDate
   Mandatory:         F
   Data Type:         I
   Number of Values:  1
   Minimum Value:     0
   Maximum Value:     1
   Data Source:       PGE
   Description: >
    Set to 1 if the time difference between the used Solar product and the
    radiance file is larger than the threshold. 

 - Metadata Name:     TemperatureSource
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The source of temperature profile used in derived total ozone.  Example
    is "Climatology".

 - Metadata Name:     TerrainPressureSource
   Mandatory:         F
   Data Type:         VA80
   Number of Values:  1
   Data Source:       PCF
   Description: >
    The source of terrain pressure used in derived total ozone.  Example is
    "Climatology".
 
 - Metadata Name:     ZonalLatitudeMaximum
   Mandatory:         F
   Data Type:         D
   Number of Values:  5
   Minimum Value:     -90.0
   Maximum Value:     90.0
   Data Source:       PGE
   Description:       The maximum latitude of each zone.

 - Metadata Name:     ZonalLatitudeMinimum
   Mandatory:         F
   Data Type:         D
   Number of Values:  5
   Minimum Value:     -90.0
   Maximum Value:     90.0
   Data Source:       PGE
   Description:       The minimum latitude of each zone.

 - Metadata Name:     ZonalOzoneMaximum
   Mandatory:         F
   Data Type:         D
   Number of Values:  5
   Minimum Value:     0.0
   Maximum Value:     700.0
   Data Source:       PGE
   Description:       The maximum ozone (in DU) of each zone.

 - Metadata Name:     ZonalOzoneMinimum
   Mandatory:         F
   Data Type:         D
   Number of Values:  5
   Minimum Value:     0.0
   Maximum Value:     700.0
   Data Source:       PGE
   Description:       The minimum ozone (in DU) of each zone.

References: >

  1. "OMI Algorithm Theoretical Basis Document, Volume II, OMI Ozone Products"
     (OMI-ATBD-VOL2, ATBD-OMI-02, Version 2.0, August 2002)

  2. "HDF-EOS Aura File Format Guidelines"
     (OMI-AURA-DATA-GUIDE, Version 1.3, 16 October 2003)

  3. "OMI Science Software Delivery Guide for Version 0.9"
     (OMI-SSDG-0.9.8, Version 0.9.8, 14 August 2003)

  4. "Input output data specification for the collection 4 L01b data processing of the Ozone Monitoring Instrument"
     (AURA-OMI-KNMI-L01B-0005-SD 24148, 10 June 2022)

  5. "Release 6A Implementation Earth Science Data Model for the ECS Project"
     (420-TP-022-002, June 2001)

  6. "OMI L2 - L4 Metadata Reference Guide"
     (3 July 2002)
