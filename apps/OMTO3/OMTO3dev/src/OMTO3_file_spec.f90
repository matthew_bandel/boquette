module OMTO3_file_spec
  !! This module contains subroutines for writing col4 OMTO3 netCDF4 file.

    !TODO-RAM : Add a subroutine that does range-check and NaN check
    !     before it dumps arrays into netCDF file
  use nc4_cf_writer
  use L2_output_params
  use nc4utils
  implicit none

  ! type, public:: nc4_Var_T 
  !   character(len = 128):: name = nc4_dummy_string
  !   integer(kind = 4):: parent  = -1
  !   integer(kind = 4):: id = -1
  !   integer(kind = 4):: datatype = -1
  !   integer:: rank = 0
  !   integer(kind = 4), dimension(MAXRANK):: dims = (/-1, -1, -1, -1, -1, -1, -1/)
  !   character(len = 128):: standard_name = nc4_dummy_string
  !   character(len = 128):: coordinates = nc4_dummy_string
  !   character(len = 128):: long_name = nc4_dummy_string
  !   character(len = 128):: content_type = nc4_dummy_string
  !   real(DP), dimension(2):: valid_range = (/ 0, 0 /)
  !   real(DP):: fill_value = -999.0
  ! end type 

  private
  type(nc4_Var_T):: ColumnAmountO3, StepTwoLayerAmountO3, StepOneO3, &
    StepTwoO3, StepTwoProfileShapeError, StepTwoColumnWeightingKernel, &
    APrioriLayerO3, CloudFraction, CloudPressure, ProfileShapeError, &
    DegreesFreedomforSignal, LayerAmountO3, LayerAmountO3Covariance, &
    LayerTemperature, dNdR, dNdT, TerrainPressure, Sensitivity, &
    ResidualStep1, ResidualStep2, Reflectivity340, Reflectivity380, &
    NValue, NValueAdjustments, MeasurementNoise, O3UnderCloud, &
    APrioriLayerO3Covariance, ColumnWeightingKernel, QualityFlags, &
    ColumnGain, TerrainHeight, AlgorithmFlags, relative_azimuth_angle, &
    wavelength, wavelength_retrieval, scanline, ground_pixel, layer, &
    corner

  integer(kind = 4):: nc4_file_id   !> netCDF4 file ID
  integer(kind = 4):: geo_group_id  !> GEODATA group ID
  integer(kind = 4)::  sci_group_id !> SCIDATA group ID
  ! Dimension IDs
  integer(kind = 4)::  dim_scanline !> Along-track dimension ID
  integer(kind = 4)::  dim_ground_pixel !> X-track dimension ID
  integer(kind = 4)::  dim_Wavelength !> Wavelength dimension ID
  integer(kind = 4)::  dim_Wavelength_ret !> Wavelength dimension ID
  ! adding spectral_channel to dims - MB 4/11/23
  integer(kind = 4)::  dim_spectral_channel !> Wavelength dimension ID

  ! n_wavelength_retrieval
  integer(kind = 4)::  dim_layer !> Layer dimension ID
  integer(kind = 4)::  dim_corner !> Corner dimension ID
  logical:: initialized = .FALSE.

  ! dimension names and dimension ids
  ! increaasing dimension ids to 7 for spectral_channel - MB 4/11/23
  !integer(kind=4), parameter :: ndim_ids = 6
  integer(kind=4), parameter :: ndim_ids = 7
  character(len=255) :: dim_names(ndim_ids)
  integer(kind=4) :: dim_ids(ndim_ids)
  integer(kind=4) :: ii
  integer(kind=4), dimension(:), allocatable :: tmp_1d_arr

  public :: OMTO3_file_init
  public :: OMTO3_create_variables
  public :: write_values
  public :: Replace_Text
  public :: get_L2_nc_filename
contains

  subroutine OMTO3_file_init(level2_file, level1_file, nLines, &
      nXTrack, nWavel, nLayers, wl_com, nPixels)
    !! This subroutine opens the netCDF4 file for writing.
    !! It also creates groups and dimensions.
    implicit none  
    character(len=*), intent(in):: level2_file !> Level-2 netCDF4 filename
    character(len=*), intent(in):: level1_file !> Level-1 netCDF4 filename
    integer(kind = 4), intent(in):: nLines !> Number of Along-tracks
    integer(kind = 4), intent(in):: nXTrack !> Number of X-tracks
    integer(kind = 4), intent(in):: nWavel !> Number of Wavelengths
    integer(kind = 4), intent(in):: nLayers !> Number of Layers
    ! adding nPixels for spectral channel dimension of quality level - MB 4/11/23
    integer(kind = 4), intent(in):: nPixels !> Number of spectral pixels
    real(kind=4) :: wl_com(nWavel)
    integer(kind = 4):: status
    integer(kind=4):: l1_file_id, l1_geo_id, temp1_id, temp2_id, ii
    character(len=255) :: copy_variables_name(13) 
    ! copy_variables_name(1:13) = &
    ! (/ "latitude","longitude","latitude_bounds","longitude_bounds", &
    !   "satellite_altitude", "satellite_latitude", "satellite_longitude", &
    !   "satellite_orbit_phase", "viewing_zenith_angle", "viewing_azimuth_angle", &
    !   "solar_zenith_angle", "solar_azimuth_angle", "surface_altitude" /)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++
    ! Create file.
    call check(nf90_create(TRIM(level2_file), NF90_NETCDF4, nc4_file_id), &
    ! call check(nf90_create(level2_file, NF90_CLOBBER, nc4_file_id), &
      'Unable to create file', fatal = 1)
    ! Create groups.
    call check( nf90_def_grp(nc4_file_id, "GEODATA", geo_group_id), &
      'Unable to create GeolocationData', fatal = 1)
    call check( nf90_def_grp(nc4_file_id, "SCIDATA", sci_group_id), &
      'Unable to create ScienceData', fatal = 1)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++
    ! Define the dimensions.
    call check(nf90_def_dim(nc4_file_id, "scanline", nLines, dim_scanline), &
      'Unable to create DimAlongTrack')
    call check(nf90_def_dim(nc4_file_id, "ground_pixel", nXTrack, dim_ground_pixel), &
      'Unable to create DimCrossTrack')
    call check(nf90_def_dim(nc4_file_id, "ncorner", 4, dim_corner), &
      'Unable to create DimCorners')
    call check(nf90_def_dim(nc4_file_id, "wavelength", nWavel, dim_Wavelength), &
      'Unable to create Wavelength')
    call check(nf90_def_dim(nc4_file_id, "wavelength_retrieval", 4, dim_Wavelength_ret), &
      'Unable to create n_wavelength_retrieval')
    call check(nf90_def_dim(nc4_file_id, "layer", nLayers, dim_layer), &
      'Unable to create DimLayers')
    ! add spectral channel dimension to output - MB 4/11/23
    call check(nf90_def_dim(nc4_file_id, "spectral_channel", nPixels, dim_spectral_channel), &
      'Unable to create spectral_channel')


    ! PRINT * , wl_com
    ! Load dimensions into arrays
    dim_ids(1) = dim_scanline
    dim_names(1) = "scanline"
    dim_ids(2) = dim_ground_pixel
    dim_names(2) = "ground_pixel"
    dim_ids(3) = dim_corner
    dim_names(3) = "ncorner"
    dim_ids(4) = dim_Wavelength
    dim_names(4) = "wavelength"
    dim_ids(5) = dim_layer
    dim_names(5) = "layer"
    dim_ids(6) = dim_Wavelength_ret
    dim_names(6) = "wavelength_retrieval"
    ! adding dim id and name for spectral_channel - MB 4/11/23
    dim_ids(7) = dim_spectral_channel
    dim_names(7) = "spectral_channel"

    ! RAM - Try copying variables
    ! Open L1B file.
    call check( nf90_open(level1_file, NF90_NOWRITE, l1_file_id), &
      'Unable to open Level-1B file')
    call check( nf90_inq_ncid(l1_file_id, "BAND2_RADIANCE", temp1_id),&
      'Unable to find "BAND2_RADIANCE" in Level-1B file')
    call check( nf90_inq_ncid(temp1_id, "STANDARD_MODE", temp2_id),&
      'Unable to find "STANDARD_MODE" in Level-1B file')
    call check( nf90_inq_ncid(temp2_id, "GEODATA", l1_geo_id),&
      'Unable to find "GEODATA" in Level-1B file')
  
    ! do ii = 1, size(copy_variables_name)
    !   status = copy_variable(l1_geo_id, TRIM(copy_variables_name(ii)), &
    !      geo_group_id, ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
   ! enddo
    status = copy_variable(l1_geo_id, "latitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "longitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "latitude_bounds", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "longitude_bounds", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "satellite_altitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "satellite_latitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "satellite_longitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "satellite_orbit_phase", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "viewing_zenith_angle", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "viewing_azimuth_angle", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "solar_zenith_angle", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "solar_azimuth_angle", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(l1_geo_id, "surface_altitude", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(temp2_id, "time_TAI93", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(temp2_id, "ground_pixel_quality", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(temp2_id, "measurement_quality", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)
    status = copy_variable(temp2_id, "quality_level", geo_group_id, &
      ndim_ids, dim_names, dim_ids, copy_attrs=.TRUE.)


    initialized = .TRUE.
  end subroutine

  subroutine OMTO3_create_variables(nLines, nXTrack, nLayers)
    !! This subroutine defines the variables, and writes field-level attribtes.
    implicit none
    character(len = 255):: msg
    integer(kind = 4), intent(in):: nLines !> Number of Along-tracks
    integer(kind = 4), intent(in):: nXTrack !> Number of X-tracks
    integer(kind = 4), intent(in):: nLayers !> Number of Layers
    integer(kind = 4):: status
    msg =' Error in creating dataset : '
    relative_azimuth_angle%name = "relative_azimuth_angle"
    relative_azimuth_angle%units = "degree"
    relative_azimuth_angle%rank = 2
    relative_azimuth_angle%coordinates = "longitude latitude"
    relative_azimuth_angle%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    relative_azimuth_angle%parent = geo_group_id
    relative_azimuth_angle%datatype = NF90_REAL4
    relative_azimuth_angle%valid_min = -180.0
    relative_azimuth_angle%valid_max = 180.0
    relative_azimuth_angle%fill_value = NF90_FILL_REAL4
    relative_azimuth_angle%long_name = "relative azimuth angle"
    relative_azimuth_angle%comment = "Relative azimuth angle at the ground pixel location on the WGS84 reference ellipsoid. Angle is calculated as 180.0 + solar_azimuth_angle - viewing_azimuth_angle, shifted into [-180, 180] when necessary"
    status = create_variable(relative_azimuth_angle)

    scanline%name = "scanline"
    scanline%units = "1"
    scanline%rank = 1
    scanline%dims(1:1) = (/ dim_scanline/)
    scanline%parent = nc4_file_id
    scanline%datatype = NF90_INT4
    scanline%valid_min = 0
    scanline%valid_max = nLines
    scanline%fill_value = NF90_FILL_INT
    scanline%long_name = "scanline dimension"
    status = create_variable(scanline)
    allocate(tmp_1d_arr(nLines))
    do ii = 1, nLines
      tmp_1d_arr(ii) =ii
    enddo
    call check(nf90_put_var(scanline%parent, scanline%id, tmp_1d_arr), TRIM(msg)//scanline%name)
    deallocate(tmp_1d_arr)

    ground_pixel%name = "ground_pixel"
    ground_pixel%units = "1"
    ground_pixel%rank = 1
    ground_pixel%dims(1:1) = (/ dim_ground_pixel/)
    ground_pixel%parent = nc4_file_id
    ground_pixel%datatype = NF90_INT4
    ground_pixel%valid_min = 0
    ground_pixel%valid_max = nXTrack
    ground_pixel%fill_value = NF90_FILL_INT
    ground_pixel%long_name = "ground_pixel dimension"
    status = create_variable(ground_pixel)
    allocate(tmp_1d_arr(nXTrack))
    do ii = 1, nXTrack
      tmp_1d_arr(ii) =ii
    enddo
    call check(nf90_put_var(ground_pixel%parent, ground_pixel%id, tmp_1d_arr), TRIM(msg)//ground_pixel%name)
    deallocate(tmp_1d_arr)

    layer%name = "layer"
    layer%units = "1"
    layer%rank = 1
    layer%dims(1:1) = (/ dim_layer/)
    layer%parent = nc4_file_id
    layer%datatype = NF90_INT4
    layer%valid_min = 0
    layer%valid_max = nXTrack
    layer%fill_value = NF90_FILL_INT
    layer%long_name = "layer dimension"
    status = create_variable(layer)
    allocate(tmp_1d_arr(nLayers))
    do ii = 1, nLayers
      tmp_1d_arr(ii) =ii
    enddo
    call check(nf90_put_var(layer%parent, layer%id, tmp_1d_arr), TRIM(msg)//layer%name)
    deallocate(tmp_1d_arr)

    corner%name = "ncorner"
    corner%units = "1"
    corner%rank = 1
    corner%dims(1:1) = (/ dim_corner/)
    corner%parent = nc4_file_id
    corner%datatype = NF90_INT4
    corner%valid_min = 0
    corner%valid_max = 4
    corner%fill_value = NF90_FILL_INT
    corner%long_name = "corner dimension"
    status = create_variable(corner)
    allocate(tmp_1d_arr(4))
    tmp_1d_arr(:) = (/ 1,2,3,4 /)
    call check(nf90_put_var(corner%parent, corner%id, tmp_1d_arr), TRIM(msg)//corner%name)
    deallocate(tmp_1d_arr)

    wavelength%name = "wavelength"
    wavelength%units = "nm"
    wavelength%rank = 1
    wavelength%dims(1:1) = (/ dim_Wavelength/)
    wavelength%parent = nc4_file_id
    wavelength%datatype = NF90_REAL4
    wavelength%valid_min = 300.0
    wavelength%valid_max = 400.0
    wavelength%fill_value = fill_value_rk4
    wavelength%long_name = "Wavelength"
    status = create_variable(wavelength)

    wavelength_retrieval%name = "wavelength_retrieval"
    wavelength_retrieval%units = "nm"
    wavelength_retrieval%rank = 1
    wavelength_retrieval%dims(1:1) = (/ dim_Wavelength_ret/)
    wavelength_retrieval%parent = nc4_file_id
    wavelength_retrieval%datatype = NF90_REAL4
    wavelength_retrieval%valid_min = 300.0
    wavelength_retrieval%valid_max = 400.0
    wavelength_retrieval%fill_value = fill_value_rk4
    wavelength_retrieval%long_name = "Retrieved Wavelengths"
    status = create_variable(wavelength_retrieval)

    ColumnAmountO3%name = "ColumnAmountO3"
    ColumnAmountO3%units = "DU"
    ColumnAmountO3%rank = 2
    ColumnAmountO3%coordinates = "longitude latitude"
    ColumnAmountO3%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    ColumnAmountO3%parent = sci_group_id
    ColumnAmountO3%datatype = NF90_REAL4
    ColumnAmountO3%valid_min = 50.0
    ColumnAmountO3%valid_max = 700.0
    ColumnAmountO3%fill_value = fill_value_rk4
    ColumnAmountO3%long_name = "Best Total Ozone Solution"
    status = create_variable(ColumnAmountO3)

    APrioriLayerO3%name = 'APrioriLayerO3'
    APrioriLayerO3%units = "DU"
    APrioriLayerO3%rank = 3
    APrioriLayerO3%coordinates = "longitude latitude layer"
    APrioriLayerO3%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    APrioriLayerO3%parent = sci_group_id
    APrioriLayerO3%datatype = NF90_REAL4
    APrioriLayerO3%valid_min = 0.0
    APrioriLayerO3%valid_max = 200.0
    APrioriLayerO3%fill_value = fill_value_rk4
    APrioriLayerO3%long_name = "A Priori Layer Amount O3"
    status = create_variable(APrioriLayerO3)

    APrioriLayerO3Covariance%name = 'APrioriLayerO3Covariance'
    APrioriLayerO3Covariance%units = "DU^2"
    APrioriLayerO3Covariance%rank = 3
    APrioriLayerO3Covariance%coordinates = "longitude latitude layer"
    APrioriLayerO3Covariance%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    APrioriLayerO3Covariance%parent = sci_group_id
    APrioriLayerO3Covariance%datatype = NF90_REAL4
    APrioriLayerO3Covariance%valid_min = 0.0
    APrioriLayerO3Covariance%valid_max = 1000.0
    APrioriLayerO3Covariance%fill_value = fill_value_rk4
    APrioriLayerO3Covariance%long_name = "A Priori Layer Amount O3 Covariance"
    status = create_variable(APrioriLayerO3Covariance)

    CloudFraction%name = 'CloudFraction'
    CloudFraction%units = "1"
    CloudFraction%rank = 2
    CloudFraction%coordinates = "longitude latitude"
    CloudFraction%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    CloudFraction%parent = sci_group_id
    CloudFraction%datatype = NF90_REAL4
    CloudFraction%valid_min = -1000.0
    CloudFraction%valid_max = 1000.0
    CloudFraction%fill_value = fill_value_rk4
    CloudFraction%long_name = "Cloud Fraction at 340 nm"
    status = create_variable(CloudFraction)

    CloudPressure%name = 'CloudPressure'
    CloudPressure%units = "hPa"
    CloudPressure%rank = 2
    CloudPressure%coordinates = "longitude latitude"
    CloudPressure%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    CloudPressure%parent = sci_group_id
    CloudPressure%datatype = NF90_REAL4
    CloudPressure%valid_min = 0.0
    CloudPressure%valid_max = 1100.0
    CloudPressure%fill_value = fill_value_rk4
    CloudPressure%long_name = "Optical Centroid Cloud Pressure"
    status = create_variable(CloudPressure)

    ColumnWeightingKernel%name = 'ColumnWeightingKernel'
    ColumnWeightingKernel%units = "1"
    ColumnWeightingKernel%rank = 3
    ColumnWeightingKernel%coordinates = "longitude latitude layer"
    ColumnWeightingKernel%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    ColumnWeightingKernel%parent = sci_group_id
    ColumnWeightingKernel%datatype = NF90_REAL4
    ColumnWeightingKernel%valid_min = -3.0
    ColumnWeightingKernel%valid_max = 3.0
    ColumnWeightingKernel%fill_value = fill_value_rk4
    ColumnWeightingKernel%long_name = "Column Weighting Kernel"
    status = create_variable(ColumnWeightingKernel)

    DegreesFreedomforSignal%name = 'DegreesFreedomforSignal'
    DegreesFreedomforSignal%units = "1"
    DegreesFreedomforSignal%rank = 2
    DegreesFreedomforSignal%coordinates = "longitude latitude"
    DegreesFreedomforSignal%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    DegreesFreedomforSignal%parent = sci_group_id
    DegreesFreedomforSignal%datatype = NF90_REAL4
    DegreesFreedomforSignal%valid_min = 0.0
    DegreesFreedomforSignal%valid_max = 4.0
    DegreesFreedomforSignal%fill_value = fill_value_rk4
    DegreesFreedomforSignal%long_name = "Degrees of Freedom for Signal"
    status = create_variable(DegreesFreedomforSignal)

    LayerAmountO3%name = 'LayerAmountO3'
    LayerAmountO3%units = "DU"
    LayerAmountO3%rank = 3
    LayerAmountO3%coordinates = "longitude latitude layer"
    LayerAmountO3%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    LayerAmountO3%parent = sci_group_id
    LayerAmountO3%datatype = NF90_REAL4
    LayerAmountO3%valid_min = 0.0
    LayerAmountO3%valid_max = 200.0
    LayerAmountO3%fill_value = fill_value_rk4
    LayerAmountO3%long_name = "Layer Amount O3"
    status = create_variable(LayerAmountO3)

    LayerAmountO3Covariance%name = 'LayerAmountO3Covariance'
    LayerAmountO3Covariance%units = "DU^2"
    LayerAmountO3Covariance%rank = 3
    LayerAmountO3Covariance%coordinates = "longitude latitude layer"
    LayerAmountO3Covariance%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    LayerAmountO3Covariance%parent = sci_group_id
    LayerAmountO3Covariance%datatype = NF90_REAL4
    LayerAmountO3Covariance%valid_min = 0.0
    LayerAmountO3Covariance%valid_max = 1000.0
    LayerAmountO3Covariance%fill_value = fill_value_rk4
    LayerAmountO3Covariance%long_name = "Layer Amount O3 Covariance"
    status = create_variable(LayerAmountO3Covariance)

    LayerTemperature%name = 'LayerTemperature'
    LayerTemperature%units = "Kelvin"
    LayerTemperature%rank = 3
    LayerTemperature%coordinates = "longitude latitude layer"
    LayerTemperature%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    LayerTemperature%parent = sci_group_id
    LayerTemperature%datatype = NF90_REAL4
    LayerTemperature%valid_min = 100.0
    LayerTemperature%valid_max = 300.0
    LayerTemperature%fill_value = fill_value_rk4
    LayerTemperature%long_name = "Layer Temperature"
    LayerTemperature%standard_name = "air_temperature"
    status = create_variable(LayerTemperature)

    MeasurementNoise%name = 'MeasurementNoise'
    MeasurementNoise%units = "DU"
    MeasurementNoise%rank = 2
    MeasurementNoise%coordinates = "longitude latitude"
    MeasurementNoise%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    MeasurementNoise%parent = sci_group_id
    MeasurementNoise%datatype = NF90_REAL4
    MeasurementNoise%valid_min = 0.0
    MeasurementNoise%valid_max = 30.0
    MeasurementNoise%fill_value = fill_value_rk4
    MeasurementNoise%long_name = "Measurement Noise"
    status = create_variable(MeasurementNoise)

    NValueAdjustments%name = 'NValueAdjustments'
    NValueAdjustments%units = "1"
    NValueAdjustments%rank = 2
    NValueAdjustments%dims(1:2) = (/ dim_Wavelength, dim_ground_pixel /)
    NValueAdjustments%parent = sci_group_id
    NValueAdjustments%datatype = NF90_REAL4
    NValueAdjustments%valid_min = -5.0
    NValueAdjustments%valid_max = 5.0
    NValueAdjustments%fill_value = fill_value_rk4
    NValueAdjustments%long_name = "N-Value Adjustment"
    status = create_variable(NValueAdjustments)

    NValue%name = 'NValue'
    NValue%units = "1"
    NValue%rank = 3
    NValue%coordinates = "longitude latitude wavelength"
    NValue%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    NValue%parent = sci_group_id
    NValue%datatype = NF90_REAL4
    NValue%valid_min = 0.0
    NValue%valid_max = 400.0
    NValue%fill_value = fill_value_rk4
    NValue%long_name = "N-Value"
    status = create_variable(NValue)

    O3UnderCloud%name = 'O3UnderCloud'
    O3UnderCloud%units = "DU"
    O3UnderCloud%rank = 2
    O3UnderCloud%coordinates = "longitude latitude"
    O3UnderCloud%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    O3UnderCloud%parent = sci_group_id
    O3UnderCloud%datatype = NF90_REAL4
    O3UnderCloud%valid_min = 0.0
    O3UnderCloud%valid_max = 30.0
    O3UnderCloud%fill_value = fill_value_rk4
    O3UnderCloud%long_name = "Ozone Under Cloud"
    status = create_variable(O3UnderCloud)

    ProfileShapeError%name = 'ProfileShapeError'
    ProfileShapeError%units = "DU"
    ProfileShapeError%rank = 2
    ProfileShapeError%coordinates = "longitude latitude"
    ProfileShapeError%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    ProfileShapeError%parent = sci_group_id
    ProfileShapeError%datatype = NF90_REAL4
    ProfileShapeError%valid_min = 0.0
    ProfileShapeError%valid_max = 30.0
    ProfileShapeError%fill_value = fill_value_rk4
    ProfileShapeError%long_name = "Profile Shape Error"
    status = create_variable(ProfileShapeError)

    QualityFlags%name = 'QualityFlags'
    QualityFlags%units = "1"
    QualityFlags%rank = 2
    QualityFlags%coordinates = "longitude latitude"
    QualityFlags%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    QualityFlags%parent = sci_group_id
    QualityFlags%datatype = NF90_USHORT
    QualityFlags%valid_min = 0
    QualityFlags%valid_max = 65534
    QualityFlags%fill_value = fill_value_uik2
    QualityFlags%long_name = "Quality Flags"
    status = create_variable(QualityFlags)

    AlgorithmFlags%name = 'AlgorithmFlags'
    AlgorithmFlags%units = "1"
    AlgorithmFlags%rank = 2
    AlgorithmFlags%coordinates = "longitude latitude"
    AlgorithmFlags%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    AlgorithmFlags%parent = sci_group_id
    AlgorithmFlags%datatype = NF90_UBYTE
    AlgorithmFlags%valid_min = 0
    AlgorithmFlags%valid_max = 254
    AlgorithmFlags%fill_value = fill_value_uik1
    AlgorithmFlags%long_name = "Algorithm Flags"
    status = create_variable(AlgorithmFlags)

    Reflectivity340%name = 'Reflectivity340'
    Reflectivity340%units = "1"
    Reflectivity340%rank = 2
    Reflectivity340%coordinates = "longitude latitude"
    Reflectivity340%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    Reflectivity340%parent = sci_group_id
    Reflectivity340%datatype = NF90_REAL4
    Reflectivity340%valid_min = -0.1
    Reflectivity340%valid_max = 1.2
    Reflectivity340%fill_value = fill_value_rk4
    Reflectivity340%long_name = "Lambert Equivalent Reflectivity at 340 nm"
    status = create_variable(Reflectivity340)

    Reflectivity380%name = 'Reflectivity380'
    Reflectivity380%units = "1"
    Reflectivity380%rank = 2
    Reflectivity380%coordinates = "longitude latitude"
    Reflectivity380%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    Reflectivity380%parent = sci_group_id
    Reflectivity380%datatype = NF90_REAL4
    Reflectivity380%valid_min = -0.1
    Reflectivity380%valid_max = 1.2
    Reflectivity380%fill_value = fill_value_rk4
    Reflectivity380%long_name = "Lambert Equivalent Reflectivity at 380 nm"
    status = create_variable(Reflectivity380)

    ResidualStep1%name = 'ResidualStep1'
    ResidualStep1%units = "1"
    ResidualStep1%rank = 3
    ResidualStep1%coordinates = "longitude latitude wavelength"
    ResidualStep1%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    ResidualStep1%parent = sci_group_id
    ResidualStep1%datatype = NF90_REAL4
    ResidualStep1%valid_min = -32.0
    ResidualStep1%valid_max = 32.0
    ResidualStep1%fill_value = fill_value_rk4
    ResidualStep1%long_name = "Step One Residual"
    status = create_variable(ResidualStep1)

    ResidualStep2%name = 'ResidualStep2'
    ResidualStep2%units = "1"
    ResidualStep2%rank = 3
    ResidualStep2%coordinates = "longitude latitude wavelength"
    ResidualStep2%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    ResidualStep2%parent = sci_group_id
    ResidualStep2%datatype = NF90_REAL4
    ResidualStep2%valid_min = -32.0
    ResidualStep2%valid_max = 32.0
    ResidualStep2%fill_value = fill_value_rk4
    ResidualStep2%long_name = "Step Two Residual"
    status = create_variable(ResidualStep2)

    Sensitivity%name = 'Sensitivity'
    Sensitivity%units = "1/DU"
    Sensitivity%rank = 3
    Sensitivity%coordinates = "longitude latitude wavelength"
    Sensitivity%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    Sensitivity%parent = sci_group_id
    Sensitivity%datatype = NF90_REAL4
    Sensitivity%valid_min = -1.0
    Sensitivity%valid_max = 1.0
    Sensitivity%fill_value = fill_value_rk4
    Sensitivity%long_name = "N-Value Sensitivity to Total Column O3"
    status = create_variable(Sensitivity)

    StepOneO3%name = 'StepOneO3'
    StepOneO3%units = "DU"
    StepOneO3%rank = 2
    StepOneO3%coordinates = "longitude latitude"
    StepOneO3%dims(1:2) = (/ dim_ground_pixel, dim_scanline /)
    StepOneO3%parent = sci_group_id
    StepOneO3%datatype = NF90_REAL4
    StepOneO3%valid_min = 50.0
    StepOneO3%valid_max = 700.0
    StepOneO3%fill_value = fill_value_rk4
    StepOneO3%long_name = "Step One Total O3 Solution"
    status = create_variable(StepOneO3)

    StepTwoColumnWeightingKernel%name = 'StepTwoColumnWeightingKernel'
    StepTwoColumnWeightingKernel%units = "1"
    StepTwoColumnWeightingKernel%rank = 3
    StepTwoColumnWeightingKernel%coordinates = "longitude latitude layer"
    StepTwoColumnWeightingKernel%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    StepTwoColumnWeightingKernel%parent = sci_group_id
    StepTwoColumnWeightingKernel%datatype = NF90_REAL4
    StepTwoColumnWeightingKernel%valid_min = -3.0
    StepTwoColumnWeightingKernel%valid_max = 3.0
    StepTwoColumnWeightingKernel%fill_value = fill_value_rk4
    StepTwoColumnWeightingKernel%long_name = "Step Two Column Weighting Kernel"
    status = create_variable(StepTwoColumnWeightingKernel)

    StepTwoLayerAmountO3%name = 'StepTwoLayerAmountO3'
    StepTwoLayerAmountO3%units = "DU"
    StepTwoLayerAmountO3%rank = 3
    StepTwoLayerAmountO3%coordinates = "longitude latitude layer"
    StepTwoLayerAmountO3%dims(1:3) = (/ dim_layer, dim_ground_pixel, dim_scanline/)
    StepTwoLayerAmountO3%parent = sci_group_id
    StepTwoLayerAmountO3%datatype = NF90_REAL4
    StepTwoLayerAmountO3%valid_min = 0.0
    StepTwoLayerAmountO3%valid_max = 200.0
    StepTwoLayerAmountO3%fill_value = fill_value_rk4
    StepTwoLayerAmountO3%long_name = "Step Two Layer Amount O3"
    status = create_variable(StepTwoLayerAmountO3)

    StepTwoO3%name = 'StepTwoO3'
    StepTwoO3%units = "DU"
    StepTwoO3%rank = 2
    StepTwoO3%coordinates = "longitude latitude"
    StepTwoO3%dims(1:2) = (/dim_ground_pixel, dim_scanline /)
    StepTwoO3%parent = sci_group_id
    StepTwoO3%datatype = NF90_REAL4
    StepTwoO3%valid_min = 50.0
    StepTwoO3%valid_max = 700.0
    StepTwoO3%fill_value = fill_value_rk4
    StepTwoO3%long_name = "Step Two Total O3 Solution"
    status = create_variable(StepTwoO3)

    StepTwoProfileShapeError%name = 'StepTwoProfileShapeError'
    StepTwoProfileShapeError%units = "DU"
    StepTwoProfileShapeError%rank = 2
    StepTwoProfileShapeError%coordinates = "longitude latitude"
    StepTwoProfileShapeError%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    StepTwoProfileShapeError%parent = sci_group_id
    StepTwoProfileShapeError%datatype = NF90_REAL4
    StepTwoProfileShapeError%valid_min = 0.0
    StepTwoProfileShapeError%valid_max = 30.0
    StepTwoProfileShapeError%fill_value = fill_value_rk4
    StepTwoProfileShapeError%long_name = "Step Two Profile Shape Error"
    status = create_variable(StepTwoProfileShapeError)

    TerrainPressure%name = 'TerrainPressure'
    TerrainPressure%units = "hPa"
    TerrainPressure%rank = 2
    TerrainPressure%coordinates = "longitude latitude"
    TerrainPressure%dims(1:2) = (/ dim_ground_pixel, dim_scanline/)
    TerrainPressure%parent = sci_group_id
    TerrainPressure%datatype = NF90_REAL4
    TerrainPressure%valid_min = 0.0
    TerrainPressure%valid_max = 1100.0
    TerrainPressure%fill_value = fill_value_rk4
    TerrainPressure%long_name = "Terrain Pressure"
    status = create_variable(TerrainPressure)

    dNdR%name = 'dNdR'
    dNdR%units = "1"
    dNdR%rank = 3
    dNdR%coordinates = "longitude latitude wavelength"
    dNdR%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    dNdR%parent = sci_group_id
    dNdR%datatype = NF90_REAL4
    dNdR%valid_min = -200.0
    dNdR%valid_max = 0.0
    dNdR%fill_value = fill_value_rk4
    dNdR%long_name = "N-Value Sensitivity to Reflectivity at 340"
    status = create_variable(dNdR)

    dNdT%name = 'dNdT'
    dNdT%units = "1/Kelvin"
    dNdT%rank = 3
    dNdT%coordinates = "longitude latitude wavelength"
    dNdT%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    dNdT%parent = sci_group_id
    dNdT%datatype = NF90_REAL4
    dNdT%valid_min = 0.0
    dNdT%valid_max = 1.0
    dNdT%fill_value = fill_value_rk4
    dNdT%long_name = "N-Value Sensitivity to Ozone Weighted Temperature"
    status = create_variable(dNdT)

    ColumnGain%name = 'ColumnGain'
    ColumnGain%units = "1/Kelvin"
    ColumnGain%rank = 3
    ColumnGain%coordinates = "longitude latitude wavelength"
    ColumnGain%dims(1:3) = (/ dim_Wavelength, dim_ground_pixel, dim_scanline/)
    ColumnGain%parent = sci_group_id
    ColumnGain%datatype = NF90_REAL4
    ColumnGain%valid_min = 0.0
    ColumnGain%valid_max = 1.0
    ColumnGain%fill_value = fill_value_rk4
    ColumnGain%long_name = "N-Value Sensitivity to Ozone Weighted Temperature"
    status = create_variable(ColumnGain)
  end subroutine

  subroutine write_values(wl_com, iwl_oe)
    !! Write data (array values) into netCDF4 variables.
    !! These variables are already created by "nc4_create_variables"
    implicit none
    character(len = 255):: msg
    integer(kind = 4):: status
    real(kind=4), dimension(:), intent(in) :: wl_com
    integer(kind=4), dimension(:), intent(in) :: iwl_oe
    msg =' Error in writing dataset : '

    !TODO-RAM : Add a subroutine that does range-check, NaN check and then
    !     dumps arrays into netCDF file
      call check(nf90_put_var(wavelength%parent, wavelength%id, wl_com), TRIM(msg)//wavelength%name)
      call check(nf90_put_var(wavelength_retrieval%parent, wavelength_retrieval%id, wl_com(iwl_oe)), TRIM(msg)//wavelength_retrieval%name)
      call check(nf90_put_var(AlgorithmFlags%parent, AlgorithmFlags%id, L2_AlgorithmFlags), TRIM(msg)//AlgorithmFlags%name)
      call check(nf90_put_var(relative_azimuth_angle%parent, relative_azimuth_angle%id, L2_RelativeAzimuthAngle), TRIM(msg)//relative_azimuth_angle%name)
      call check(nf90_put_var(QualityFlags%parent, QualityFlags%id, L2_QualityFlags), TRIM(msg)//QualityFlags%name)
      call check(nf90_put_var(CloudFraction%parent, CloudFraction%id, L2_CloudFraction), TRIM(msg)//CloudFraction%name)
      call check(nf90_put_var(CloudPressure%parent, CloudPressure%id, L2_CloudPressure), TRIM(msg)//CloudPressure%name)
      call check(nf90_put_var(TerrainPressure%parent, TerrainPressure%id, L2_TerrainPressure), TRIM(msg)//TerrainPressure%name)
      ! call check(nf90_put_var(TerrainHeight%parent, TerrainHeight%id, L2_TerrainHeight), TRIM(msg)//TerrainHeight%name)
      call check(nf90_put_var(NValueAdjustments%parent, NValueAdjustments%id, L2_NValueAdjustments), TRIM(msg)//NValueAdjustments%name)
      call check(nf90_put_var(Reflectivity340%parent, Reflectivity340%id, L2_Reflectivity340), TRIM(msg)//Reflectivity340%name)
      call check(nf90_put_var(Reflectivity380%parent, Reflectivity380%id, L2_Reflectivity380), TRIM(msg)//Reflectivity380%name)
      call check(nf90_put_var(StepOneO3%parent, StepOneO3%id, L2_StepOneO3), TRIM(msg)//StepOneO3%name)
      call check(nf90_put_var(StepTwoO3%parent, StepTwoO3%id, L2_StepTwoO3), TRIM(msg)//StepTwoO3%name)
      call check(nf90_put_var(ColumnAmountO3%parent, ColumnAmountO3%id, L2_ColumnAmountO3), TRIM(msg)//ColumnAmountO3%name)
      call check(nf90_put_var(DegreesFreedomforSignal%parent, DegreesFreedomforSignal%id, L2_DegreesFreedomforSignal), TRIM(msg)//DegreesFreedomforSignal%name)
      call check(nf90_put_var(O3UnderCloud%parent, O3UnderCloud%id, L2_O3UnderCloud), TRIM(msg)//O3UnderCloud%name)
      call check(nf90_put_var(ProfileShapeError%parent, ProfileShapeError%id, L2_ProfileShapeError), TRIM(msg)//ProfileShapeError%name)
      call check(nf90_put_var(MeasurementNoise%parent, MeasurementNoise%id, L2_MeasurementNoise), TRIM(msg)//MeasurementNoise%name)
      call check(nf90_put_var(StepTwoProfileShapeError%parent, StepTwoProfileShapeError%id, L2_StepTwoProfileShapeError), TRIM(msg)//StepTwoProfileShapeError%name)
      call check(nf90_put_var(NValue%parent, NValue%id, L2_NValue), TRIM(msg)//NValue%name)
      call check(nf90_put_var(AprioriLayerO3%parent, AprioriLayerO3%id, L2_AprioriLayerO3), TRIM(msg)//AprioriLayerO3%name)
      call check(nf90_put_var(AprioriLayerO3Covariance%parent, AprioriLayerO3Covariance%id, L2_AprioriLayerO3Cov), TRIM(msg)//AprioriLayerO3Covariance%name)
      call check(nf90_put_var(ResidualStep1%parent, ResidualStep1%id, L2_ResidualStep1), TRIM(msg)//ResidualStep1%name)
      call check(nf90_put_var(ResidualStep2%parent, ResidualStep2%id, L2_ResidualStep2), TRIM(msg)//ResidualStep2%name)
      call check(nf90_put_var(dNdR%parent, dNdR%id, L2_dNdR), TRIM(msg)//dNdR%name)
      call check(nf90_put_var(dNdT%parent, dNdT%id, L2_dNdT), TRIM(msg)//dNdT%name)
      call check(nf90_put_var(Sensitivity%parent, Sensitivity%id, L2_Sensitivity), TRIM(msg)//Sensitivity%name)
      call check(nf90_put_var(StepTwoColumnWeightingKernel%parent, StepTwoColumnWeightingKernel%id, L2_StepTwoColumnWeightingKernel), TRIM(msg)//StepTwoColumnWeightingKernel%name)
      call check(nf90_put_var(StepTwoLayerAmountO3%parent, StepTwoLayerAmountO3%id, L2_StepTwoLayerAmountO3), TRIM(msg)//StepTwoLayerAmountO3%name)
      call check(nf90_put_var(LayerAmountO3%parent, LayerAmountO3%id, L2_LayerAmountO3), TRIM(msg)//LayerAmountO3%name)
      call check(nf90_put_var(LayerTemperature%parent, LayerTemperature%id, L2_LayerTemperature), TRIM(msg)//LayerTemperature%name)
      call check(nf90_put_var(LayerAmountO3Covariance%parent, LayerAmountO3Covariance%id, L2_LayerAmountO3Covariance), TRIM(msg)//LayerAmountO3Covariance%name)
      call check(nf90_put_var(ColumnWeightingKernel%parent, ColumnWeightingKernel%id, L2_ColumnWeightingKernel), TRIM(msg)//ColumnWeightingKernel%name)
      call check(nf90_put_var(ColumnGain%parent, ColumnGain%id, L2_ColumnGain), TRIM(msg)//ColumnGain%name)
      call check(nf90_close(nc4_file_id), 'Unable to close netCDF-4 file')
    end subroutine

    FUNCTION Replace_Text (s,text,rep)  RESULT(outs)
      CHARACTER(*)        :: s,text,rep
      CHARACTER(LEN(s)+100) :: outs,outs1     ! provide outs with extra 100 char len
      INTEGER             :: i, nt, nr

      outs1 = TRIM(s) ; nt = LEN_TRIM(text) ; nr = LEN_TRIM(rep)
      DO
        i = INDEX(outs,text(:nt)) ; IF (i == 0) EXIT
        outs1 = outs1(:i-1) // rep(:nr) // outs1(i+nt:)
      END DO
      outs = TRIM(outs1)
    END FUNCTION Replace_Text

    FUNCTION get_L2_nc_filename( L2_file_LUN, L2_filename) RESULT( status )
      use PGS_PC_class
      USE OMI_SMF_class
      implicit none
      integer(kind=4), intent(in) :: L2_file_LUN
      character(len=*), intent(out) :: L2_filename
      CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE  ) :: msg
      INTEGER (KIND=4) :: version
      INTEGER (KIND=4) :: status, ierr, zero
      zero = 0

      ! Get the L2 file name from the PCF.

      version = 1
      status = PGS_PC_getreference( L2_file_LUN, version, L2_filename )
      L2_filename = TRIM(L2_filename)

      IF( status /= PGS_S_SUCCESS ) THEN
        WRITE( msg,'(A,I8)' ) "get filename from PCF file at LUN =", &
          L2_file_LUN
        ierr = OMI_SMF_setmsg( OZT_E_INPUT, msg, "get_L2_nc_filename", zero )
        status = OZT_E_FAILURE
        RETURN
      ENDIF
    end function

  end module
