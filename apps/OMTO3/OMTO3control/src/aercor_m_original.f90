MODULE aercor_m
  USE PGS_PC_class
  USE OMI_SMF_class
  USE OMI_LUN_set
  IMPLICIT NONE
  PUBLIC :: get_aercor
  CHARACTER (LEN=256) :: aercor_fn
  INTEGER(KIND = 4), PARAMETER :: nwl = 12
  REAL(KIND=4), DIMENSION(nwl) :: aercor_fac, aercor_wave
CONTAINS
  
  FUNCTION GET_AERCOR(aercor_wave,aercor_fac) RESULT(status)
    INTEGER(KIND=4) :: status, ierr, version
    CHARACTER(LEN=128) :: aercor_fn, msg
    INTEGER(KIND=4) :: i
    REAL(KIND=4), DIMENSION(nwl) :: aercor_fac, aercor_wave
    REAL(KIND=4), DIMENSION(2,nwl) :: buffer
    INTEGER(KIND=4), PARAMETER :: zero = 0
    
    version = 1
    status = PGS_PC_getReference(AERCOR_LUN,version, aercor_fn)
    IF( status /= PGS_S_SUCCESS ) THEN
       WRITE( msg,'(A)' ) " get aerosol correction filename failed"
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "get_aercor", zero )
       CALL EXIT(1)
    ENDIF

    open(88,file=aercor_fn,status='old')
    read(88,*) buffer
    close(88)
    aercor_wave=buffer(1,:)
    aercor_fac=buffer(2,:)

  END FUNCTION GET_AERCOR

END MODULE aercor_m
