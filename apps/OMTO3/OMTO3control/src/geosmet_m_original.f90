! TODO: return status
! TODO: handle close of h5
module geosmet_m
USE HDF5
USE ISO_C_BINDING
USE OMI_LUN_set
USE PGS_PC_class
USE OMI_SMF_class

implicit none

INCLUDE 'PGS_IO.f'
INCLUDE 'PGS_IO_1.f'

REAL(KIND=4),      DIMENSION(:,:,:),ALLOCATABLE :: geosmet_temp, geosmet_delp
REAL(KIND=4),      DIMENSION(:,:),ALLOCATABLE :: geosmet_lat, geosmet_lon 
REAL(KIND=8),      DIMENSION(:),ALLOCATABLE :: geosmet_time
integer(kind=4), dimension(11), parameter :: ilev_geosmet = (/44, 41, 39, 37, 33, 29, 25, 21, 18, 15, 12/)

public :: read_geosmet
public :: read_geosmet_nc4
public :: close_geosmet

CONTAINS 

SUBROUTINE read_geosmet
implicit none
  INTEGER (KIND=4) :: status, ierr, ios, version
  CHARACTER(LEN=PGSd_PC_FILE_PATH_MAX) :: geosmet_file
  integer(kind=4), parameter :: zero=0
  CHARACTER (LEN =255) :: msg
  INTEGER(HID_T) :: file_id   !File identifier
  INTEGER(HID_T) :: dspace_Temp, dset_Temp, dset_Time
  INTEGER(HID_T) :: dset_lat, dset_lon, dset_delp
  INTEGER(HSIZE_T), DIMENSION(3)   :: dims ,max_dims
  INTEGER(HSIZE_T), DIMENSION(2)   :: Dxl
  INTEGER(HSIZE_T),DIMENSION(1)   :: Dl
  INTEGER :: nLevels , nXT , nScans
  INTEGER :: error
	CHARACTER(LEN=30) :: grp_name , name_buffer
	INTEGER :: elem_type , nMembers , ii
	LOGICAL :: grp_found

  version = 1
  status = PGS_PC_getReference(GEOSMET_LUN, version, geosmet_file)
  IF (status /= PGS_S_SUCCESS) THEN
     WRITE( msg, '(A)') "GEOSMET file not specified in PCF. exiting program."
     ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
     CALL EXIT(1)
  ENDIF

  ! Initialize FORTRAN interface.
  CALL h5open_f(error)
  
  ! Open an existing file.
  CALL h5fopen_f (TRIM(geosmet_file), H5F_ACC_RDONLY_F, file_id, error)
  
  if(error < 0) then
     PRINT *,"Error Opening GEOSMET file"
  endif
  
  ! Get dataset identifiers.
  CALL h5gn_members_f(file_id, "/", nMembers, error)
  
  !Traverse the tree; check if top-level group ("tavg3_3d_asm_Nv") is present
  grp_found = .FALSE.
  grp_name = "/"
  
  do ii = 0, nMembers-1
     CALL h5gget_obj_info_idx_f(file_id, "/" , ii, name_buffer, elem_type, &
          error)
     if( TRIM(name_buffer) .EQ. 'tavg_3d_asm_Nv') THEN
        grp_found = .TRUE.
        grp_name = TRIM('/tavg_3d_asm_Nv/')
        exit
     endif
  enddo
  
  ! Get dataset identifiers.
  CALL h5dopen_f(file_id, TRIM(grp_name)//TRIM("T"),dset_Temp,error)
  CALL h5dopen_f(file_id, "time",dset_Time,error)
  CALL h5dopen_f(file_id, "lat",dset_lat,error)
  CALL h5dopen_f(file_id, "lon",dset_lon,error)
  CALL h5dopen_f(file_id, TRIM(grp_name)//TRIM("DELP"),dset_delp,error)
  
  !Get dataspace
  CALL h5dget_space_f(dset_Temp, dspace_Temp, error)
  
  !Get actual dimensions
  CALL h5sget_simple_extent_dims_f(dspace_Temp,dims,max_dims,error)
  
  nLevels=dims(1)
  nXT=dims(2)
  nScans=dims(3)
  
  Dl = nScans
  
  Dxl(1) = nXT
  Dxl(2) = nScans
  
  !Allocate arrays
  ALLOCATE(geosmet_Temp(nLevels,nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_Temp"
  ALLOCATE(geosmet_delp(nLevels,nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_delp"
  ALLOCATE(geosmet_Time(0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_Time"
  ALLOCATE(geosmet_lat(nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_lat"
  ALLOCATE(geosmet_lon(nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_lon"
  
  !Read datasets
  
  CALL h5dread_f(dset_Temp, H5T_NATIVE_REAL, geosmet_Temp, dims,  error)
  if(error /= 0) THEN
     PRINT *,"Error Reading Temperature from GEOSMET"
  endif
  
  CALL h5dread_f(dset_delp, H5T_NATIVE_REAL, geosmet_delp, dims,  error)
  if(error /= 0) THEN
    PRINT *,"Error Reading DELP from GEOSMET"
  endif
  
  CALL h5dread_f(dset_Time, H5T_NATIVE_DOUBLE, geosmet_Time, Dl,  error)
  if(error /= 0) THEN
     PRINT *,"Error Reading Time from GEOSMET"
  endif
  
  CALL h5dread_f(dset_lat, H5T_NATIVE_REAL, geosmet_lat, Dxl,  error)
  if(error /= 0) THEN
     PRINT *,"Error Reading Latitude from GEOSMET"
  endif
  
  CALL h5dread_f(dset_lon, H5T_NATIVE_REAL, geosmet_lon, Dxl,  error)
  if(error /= 0) THEN
     PRINT *,"Error Reading Longitude from GEOSMET"
  endif
  
  !close HDF5
  CALL h5sclose_f(dspace_Temp, error)
  if (error /=0) PRINT *,"Unable to close dspace_temp"
  
  CALL h5dclose_f(dset_Temp, error)
  if (error /=0) PRINT *,"Unable to close dset_temp"
  CALL h5dclose_f(dset_Time, error)
  if (error /=0) PRINT *,"Unable to close dset_Time"
  CALL h5dclose_f(dset_delp, error)
  if (error /=0) PRINT *,"Unable to close dset_delp"
  CALL h5dclose_f(dset_lat, error)
  if (error /=0) PRINT *,"Unable to close dset_lat"
  CALL h5dclose_f(dset_lon, error)
  if (error /=0) PRINT *,"Unable to close dset_lon"
  
  ! CALL h5close_f(error)
  status = OZT_S_SUCCESS
  RETURN
END SUBROUTINE read_geosmet

SUBROUTINE read_geosmet_nc4
use nc4utils
use nc4read
use netcdf
implicit none
  INTEGER (KIND=4) :: status, ierr, ios, version
  CHARACTER(LEN=PGSd_PC_FILE_PATH_MAX) :: geosmet_file
  integer(kind=4), parameter :: zero=0
  integer:: ncid
  INTEGER :: nLevels , nXT , nScans, max_dims
  INTEGER :: dims(7)
  INTEGER :: error
	CHARACTER(LEN=30) :: grp_name , name_buffer
  character(len=255) :: msg
	INTEGER :: elem_type , nMembers , ii
	LOGICAL :: grp_found
  REAL(KIND=4), DIMENSION(:,:,:,:),ALLOCATABLE :: tmp_arr_4d
  REAL(KIND=4), DIMENSION(:,:,:),ALLOCATABLE ::  tmp_arr_3d

  version = 1
  status = PGS_PC_getReference(GEOSMET_LUN, version, geosmet_file)
  IF (status /= PGS_S_SUCCESS) THEN
     WRITE( msg, '(A)') "GEOSMET file not specified in PCF. exiting program."
     ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
     CALL EXIT(1)
  ENDIF

  msg = 'Error in opening file : '//TRIM(geosmet_file)//':'
  call check_nf90( nf90_open(TRIM(geosmet_file), nf90_nowrite, ncid), msg)
  msg = 'Unable to get dimensions for variable "T" '
  call check_nc4( get_var_dims_by_name(ncid, "T",max_dims, dims), msg)
  nLevels=dims(1)
  nXT=dims(2)
  nScans=dims(3)

  !Allocate arrays
  ! Use temporary arrays to get rid of extra dimension.
  ALLOCATE(tmp_arr_4d(nLevels,nXT,nScans,1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate tmp_arr_4d"
  ALLOCATE(tmp_arr_3d(nXT,nScans,1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate tmp_arr_3d"
  ALLOCATE(geosmet_Temp(nLevels,nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_Temp"
  ALLOCATE(geosmet_delp(nLevels,nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_delp"
  ALLOCATE(geosmet_Time(0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_Time"
  ALLOCATE(geosmet_lat(nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_lat"
  ALLOCATE(geosmet_lon(nXT,0:nScans-1),stat=error)
  if (error /=0) PRINT *,"Unable to allocate Geosmet_lon"

  msg = 'Unable to read netCDF4 variable : '
  call check_nc4( nc4read_arrays(ncid, "latitude", tmp_arr_3d), msg//'latitude')
  geosmet_lat = tmp_arr_3d(:,:,1)
  call check_nc4( nc4read_arrays(ncid, "longitude", tmp_arr_3d), msg//'longitude')
  geosmet_lon = tmp_arr_3d(:,:,1)
  call check_nc4( nc4read_arrays(ncid, "T", tmp_arr_4d), msg//'T')
  geosmet_temp = tmp_arr_4d(:,:,:,1)
  call check_nc4( nc4read_arrays(ncid, "DELP", tmp_arr_4d), msg//'DELP')
  geosmet_delp = tmp_arr_4d(:,:,:,1)

  ! De-allocate temporary arrays
  if (ALLOCATED(tmp_arr_3d))  deallocate(tmp_arr_3d)
  if (ALLOCATED(tmp_arr_4d))  deallocate(tmp_arr_4d)

END SUBROUTINE

SUBROUTINE close_geosmet
  IMPLICIT NONE
  integer(kind=4) :: status
  if (ALLOCATED(geosmet_Temp))  deallocate(geosmet_Temp)
  if (ALLOCATED(geosmet_Time))  deallocate(geosmet_Time)
  if (ALLOCATED(geosmet_lat))  deallocate(geosmet_lat)
  if (ALLOCATED(geosmet_lon))  deallocate(geosmet_lon)
  if (ALLOCATED(geosmet_delp))  deallocate(geosmet_delp)
  status = OZT_S_SUCCESS
  RETURN
END SUBROUTINE close_geosmet

subroutine check_nc4(status, error_message)
  implicit none
  integer, intent(in) :: status
  character(len=*), intent(in):: error_message
  INTEGER, PARAMETER :: OMI_S_SUCCESS = 15565312
  if (status /= OMI_S_SUCCESS) then 
    PRINT *, "Error Reading OMUFPMET file"
    PRINT *, error_message
  endif
  end subroutine
end module geosmet_m
