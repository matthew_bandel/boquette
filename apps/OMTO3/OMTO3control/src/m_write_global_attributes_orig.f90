module m_write_global_attributes

    ! declare public
    public

    ! declare contains
    contains

    ! write the OMTO3 global attributes
    subroutine write_global_attributes(destination, ultraviolet, visible, irradiance, &
            southernmost, northernmost, westernmost, easternmost, seconds, track, status)

        ! declare modules
        use netcdf
        use PGS_PC_class

        ! declare implicits
        implicit none




        ! declare arguments
        character (len=200), intent(in) :: destination, ultraviolet, visible, irradiance
        real(kind=4), intent(in) :: southernmost, northernmost, westernmost, easternmost, seconds
        integer(kind=4), intent(in) :: track
        integer(kind=4), intent(out) :: status

        ! declare variables
        integer (KIND=4), external :: day_of_year
        integer(kind=4) :: file, fileii, metadata, metadataii, inventory
        integer(kind=4) :: year, month, day, julian, orbit
        real(kind=4) :: equator
        character (len=200) :: information
        character(100) :: production, history, version

        ! current datetime parameters
        character(8) :: date
        character(10) :: time
        character(19) :: zone

        ! open file
        status = nf90_open(trim(destination), NF90_WRITE, file)

        ! Get the current date and time
        call date_and_time(date, time, zone)

        write(6, *) 'date: ', date
        write(6, *) 'time: ', time
        write(6, *) 'zone: ', zone

        ! add production time
        production = date(1:4)//"-"//date(5:6)//"-"//date(7:8)//"T"//time(1:2)//":"//time(3:4)//":"//time(5:6)
        status = nf90_put_att(file, NF90_GLOBAL, "ProductionDateTime", trim(production))

        write(6, *) 'production: ', trim(production)

        ! copy attriubtes from PCF file
        status = PGS_PC_GetConfigData(10220, information)
        status = nf90_put_att(file, NF90_GLOBAL, "Toolkit version string", trim(information))
        status = PGS_PC_GetConfigData(200100, information)
        status = nf90_put_att(file, NF90_GLOBAL, "SMF Verbosity Threshold", trim(information))
        status = PGS_PC_GetConfigData(200105, version)
        status = nf90_put_att(file, NF90_GLOBAL, "PGEVERSION", trim(version))
        status = nf90_put_att(file, NF90_GLOBAL, "PGEVersion", trim(version))
        status = PGS_PC_GetConfigData(200110, information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingCenter", trim(information))
        status = PGS_PC_GetConfigData(200115, information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingHost", trim(information))
        status = PGS_PC_GetConfigData(200135, information)
        status = nf90_put_att(file, NF90_GLOBAL, "REPROCESSINGACTUAL", trim(information))
        status = PGS_PC_GetConfigData(200170, information)
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessLevel", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "ProcessingLevel", trim(information))
        status = PGS_PC_GetConfigData(200175, information)
        status = nf90_put_att(file, NF90_GLOBAL, "InstrumentName", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "InstrumentShortName", trim(information))
        status = PGS_PC_GetConfigData(200180, information)
        status = nf90_put_att(file, NF90_GLOBAL, "OPERATIONMODE", trim(information))
        status = PGS_PC_GetConfigData(200185, information)
        status = nf90_put_att(file, NF90_GLOBAL, "AuthorAffiliation", trim(information))
        status = PGS_PC_GetConfigData(200190, information)
        status = nf90_put_att(file, NF90_GLOBAL, "AuthorName", trim(information))
        status = PGS_PC_GetConfigData(200195, information)
        status = nf90_put_att(file, NF90_GLOBAL, "LOCALVERSIONID", trim(information))
        status = PGS_PC_GetConfigData(200200, information)
        read(information, *) orbit
        status = nf90_put_att(file, NF90_GLOBAL, "OrbitNumber", orbit)
        status = nf90_put_att(file, NF90_GLOBAL, "orbit", orbit)
        status = PGS_PC_GetConfigData(200210, information)
        status = nf90_put_att(file, NF90_GLOBAL, "SwathName", trim(information))
        status = PGS_PC_GetConfigData(400090, information)
        status = nf90_put_att(file, NF90_GLOBAL, "CLOUDPRESSURESOURCE", trim(information))
        status = PGS_PC_GetConfigData(400091, information)
        status = nf90_put_att(file, NF90_GLOBAL, "TERRAINPRESSURESOURCE", trim(information))
        status = PGS_PC_GetConfigData(400092, information)
        status = nf90_put_att(file, NF90_GLOBAL, "TEMPERATURESOURCE", trim(information))
        status = PGS_PC_GetConfigData(400093, information)
        status = nf90_put_att(file, NF90_GLOBAL, "SNOWICESOURCE", trim(information))
        status = PGS_PC_GetConfigData(400094, information)
        status = nf90_put_att(file, NF90_GLOBAL, "APRIORIOZONEPROFILESOURCE", trim(information))
        status = PGS_PC_GetConfigData(123456, information)
        status = nf90_put_att(file, NF90_GLOBAL, "VERSIONID", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "VersionID", trim(information))
        status = PGS_PC_GetConfigData(400096, information)
        status = nf90_put_att(file, NF90_GLOBAL, "Aerosol Limit", trim(information))
        status = PGS_PC_GetConfigData(400097, information)
        status = nf90_put_att(file, NF90_GLOBAL, "Residual Limit", trim(information))

        ! add history
        history = "OZONE PEATE "//trim(production)//", APPVersion="//trim(version)
        status = nf90_put_att(file, NF90_GLOBAL, "history", trim(history))

        write(6, *) 'history: ', history

        ! open ultraviolet file
        status = nf90_open(trim(ultraviolet), nf90_nowrite, fileii)

        ! copy attriubtes
        status = nf90_get_att(fileii, NF90_GLOBAL, "Conventions", information)
        status = nf90_put_att(file, NF90_GLOBAL, "Conventions", trim(information))
!        status = nf90_get_att(fileii, NF90_GLOBAL, "institution", information)
!        status = nf90_put_att(file, NF90_GLOBAL, "institution", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "processor_version", information)
        status = nf90_put_att(file, NF90_GLOBAL, "processor_version", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "summary", information)
        status = nf90_put_att(file, NF90_GLOBAL, "summary", trim(information))
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_reference", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_reference", trim(information))

        ! add time coverage beginning parameterw
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_start", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_coverage_start", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeBeginningDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeBeginningTime", trim(information(12:20)))

        ! add time coverage end based parameeters
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_end", information)
        status = nf90_put_att(file, NF90_GLOBAL, "time_coverage_end", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeEndingDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "RangeEndingTime", trim(information(12:20)))

        ! get day of year
        read(information(1:4), *) year
        read(information(6:7), *) month
        read(information(9:10), *) day
        julian = day_of_year(year, month, day)

        ! set granule attributes
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleYear", year)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleMonth", month)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleDay", day)
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleDayOfYear", julian)

        ! get equator longitude and crossig
        status = nf90_inq_grp_ncid(fileii, "METADATA", metadata)
        status = nf90_inq_grp_ncid(metadata, "ECS_METADATA", metadataii)
        status = nf90_inq_grp_ncid(metadataii, "Inventory_Metadata", inventory)
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingDateTime", information)
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingDateTime", trim(information))
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingDate", trim(information(1:10)))
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingTime", trim(information(12:20)))
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingLongitude", information)
        read(information, *) equator
        status = nf90_put_att(file, NF90_GLOBAL, "EquatorCrossingLongitude", equator)

        ! add InputPointer
        information = trim(ultraviolet)//","//trim(visible)//","//trim(irradiance)
        status = nf90_put_att(file, NF90_GLOBAL, "InputPointer", trim(information))

        ! add coordinates
        status = nf90_put_att(file, NF90_GLOBAL, "NorthBoundingCoordinate", northernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "SouthBoundingCoordinate", southernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "WestBoundingCoordinate", westernmost)
        status = nf90_put_att(file, NF90_GLOBAL, "EastBoundingCoordinate", easternmost)

        ! add time, adding an additional amount of seconds because L1B is referenced to 2010-01-01 instead of 1993
        status = nf90_put_att(file, NF90_GLOBAL, "TAI93At0zOfGranule", seconds + 536457600)
        status = nf90_put_att(file, NF90_GLOBAL, "NumTimes", track)

        ! add other hard coded attributes
        status = nf90_put_att(file, NF90_GLOBAL, "comment", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "DataSetQuality", "Excellent data quality")
        status = nf90_put_att(file, NF90_GLOBAL, "DayNightFlag", "Day")
        status = nf90_put_att(file, NF90_GLOBAL, "Format", "netCDF-4")
        status = nf90_put_att(file, NF90_GLOBAL, "FOVResolution", "13kmx24km")
        status = nf90_put_att(file, NF90_GLOBAL, "IdentifierProductDOI", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "IdentifierProductDOIAuthority", "https://doi.org/")
        status = nf90_put_att(file, NF90_GLOBAL, "institution", "NASA/GSFC")
        status = nf90_put_att(file, NF90_GLOBAL, "LocalityValue", "Global")
        status = nf90_put_att(file, NF90_GLOBAL, "LongName", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km")
        status = nf90_put_att(file, NF90_GLOBAL, "ParameterName", "OMI Ground Pixels UV1, UV2 and VIS")
        status = nf90_put_att(file, NF90_GLOBAL, "PGEName", "OMTO3")
        status = nf90_put_att(file, NF90_GLOBAL, "PlatformShortName", "Aura")
        status = nf90_put_att(file, NF90_GLOBAL, "ProductType", "L2 Swath")
        status = nf90_put_att(file, NF90_GLOBAL, "references", " ")
        status = nf90_put_att(file, NF90_GLOBAL, "source", "Aura OMI")
        status = nf90_put_att(file, NF90_GLOBAL, "title", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km")
        status = nf90_put_att(file, NF90_GLOBAL, "GranuleID", trim(destination))
        status = nf90_put_att(file, NF90_GLOBAL, "LocalGranuleID", trim(destination))
        status = nf90_put_att(file, NF90_GLOBAL, "SensorShortName", "CCD Ultraviolet, CCD Visible")
        status = nf90_put_att(file, NF90_GLOBAL, "ShortName", "OMTO3")

        ! close files
        status = nf90_close(file)
        status = nf90_close(fileii)

        return

    end subroutine write_global_attributes
end module m_write_global_attributes
