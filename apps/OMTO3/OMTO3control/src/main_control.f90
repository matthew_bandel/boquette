PROGRAM main
    USE O3T_irrad_class
    USE O3T_radgeo_class
    USE L1B_class
    USE O3T_nval_class, ONLY: wl_com, nwl_com, nvELS, nvRRS, & 
                        iwl_oz1, iwl_oz2, iwl_ref1, iwl_ref2, iwl_ref3, iwl_ram, &
                        O3T_nval_setup, O3T_nval_dispose, O3T_getLambdaSet
    USE O3T_dndx_class, ONLY: nwl_sub, O3T_dndx_setup, O3T_dndx_dispose
    USE O3T_cloudPres_class
    USE O3T_lpolycoef_class
    USE O3T_lpolyinterp_class
    USE O3T_pixel_class
    USE O3T_class
    USE O3T_QA_class
    USE O3T_const
    USE O3T_so2_class
    USE OMI_SMF_class   
    USE L1B_getNames_m
    USE OMI_LUN_set
    USE O3T_omto3_fs
    USE OMI_copyHE4toHE5_class
    USE O3T_L2output_class
    USE OMI_L2writer_class
    USE L1B_metaData_class
    USE L2_metaData_class
    USE L2_attr_class
    USE nvalcor_m
    USE anomflg_m
    USE climtm_m
    USE O3T_const
    USE raman_m
    USE apriori_m
    USE qc
    use geosmet_m
    use aercor_m
    use interp_spec_m
    use smooth_spec_m
    use relayer_temp_fort
    use O3T_L1B_class
    use L1B_Reader_class
    use O3T_L1B_irrad_class
    use L2_output_module
    use L2_output_params
    use OMTO3_file_spec
    use anc_reader
    USE ISO_C_BINDING, ONLY: C_LONG
    USE cor5_20231012
    ! add metadata write - MB 12/11/23
    use m_write_global_attributes
!    USE cor4_20230717
!    USE nvcorr_null
    IMPLICIT NONE

    ! adding for control files - MB 01/18/24
    #include "Messages.inc"
    #include "GetConfig.inc"

    INTEGER (KIND=4), PARAMETER :: zero = 0, one = 1, two = 2
    INTEGER (KIND=4), PARAMETER :: nLinesPerWrite = 100
    INTEGER (KIND=4), PARAMETER :: nGeoF = 15, nDatF = 32, nwlA = 4
    INTEGER (KIND=C_LONG), DIMENSION(6) :: dims
    CHARACTER (LEN=256) :: dimList
    CHARACTER (LEN=PGSd_PC_FILE_PATH_MAX) :: OMTO3_fn, OMUANC_fn, OMTO3_fn_nc
    CHARACTER (LEN=256) :: OMTO3_swathname
    INTEGER (KIND=C_LONG) :: OMTO3_fileid, OMTO3_swid
    CHARACTER (LEN=2) :: satname
    CHARACTER( LEN=32) :: shortname, shortname_VIS
    INTEGER (KIND=1) :: landsea_mask, surface_category  
    TYPE (DFHE5_T), DIMENSION(nGeoF) :: gf_omto3
    TYPE (DFHE5_T), DIMENSION(nDatF) :: df_omto3
    TYPE (DFHE5_T), DIMENSION( 1  ) :: df_omto3wl, df_caladj
    TYPE (DFHE5_T), DIMENSION( 3  ) :: df_copy
    TYPE (L2_generic_type) :: wlblk, geoblk, datablk, calblk
    REAL (KIND=4) :: wl_cutoff = 400.0 ! nm
    REAL (KIND=4) :: sza_p, vza_p, phi, pt, pc, lat, lon, hgt
    REAL (KIND=4) :: sza_p_VIS, vza_p_VIS, phi_VIS, lat_VIS, lon_VIS
    TYPE (O3T_lpoly_cden_type) :: LUT_cden_blk
    TYPE (O3T_lpoly_coef_type) :: coefs
    TYPE (geolocation_type)     :: pixGEO, pixGEO_VIS
    TYPE (surface_type)   :: pixSURF
    TYPE (L1BECSMETA_T)        :: L1BUV2coreArch, L1BVIScoreArch 
    TYPE (L1BECSMETA_T)        :: L1BIRRcoreArch
    TYPE (L2PARAM_T), DIMENSION(1) :: L2_parameters
    INTEGER (KIND=4), DIMENSION(12):: LUNinputPointer
    INTEGER (KIND=4) :: mcfLUN
    REAL (KIND=4) :: doz_limit = 5.0, guesoz, stp1oz, stp2oz, stp3oz, dr, domega_temp                                                
    real (kind=4) :: stp1oz_notmp                                                                                                    
    REAL (KIND=4) :: xtoz, xerr, dfs, ref1, ref2, slope                                                                              
    REAL (KIND=4), dimension(n_signal) :: yres1                                                                                      
    REAL (KIND=4), dimension(n_layers) :: x2, wc2, xa, wc3, x3
    real(kind=4), dimension(n_layers) :: fgprf, aprftm
    REAL (KIND=4), dimension(n_signal) :: Gc                                                                                         
    REAL (KIND=4), dimension(n_layers,n_layers) :: sx, Sret                                                                          
    ! THIS IS HARD CODED FOR 12 WAVELENGTHS -- FIX THIS!
    REAL (KIND=4), DIMENSION(12, n_layers) :: dndx                                  
    REAL (KIND=4) :: fc3, pse, cldoz  
    REAL (KIND=4)  :: latPre, latNow
    LOGICAL :: absrfl, skipit, maxitr, descendQ, nXevenQ
    LOGICAL :: stp1oz_valid, stp2oz_valid, opt_est_valid
    INTEGER (KIND=4) :: iwl_oz, iwl_refl, iplow, isnow
    INTEGER (KIND=4), DIMENSION(nwlA) :: iwlArray
    INTEGER (KIND=1) :: algflg, mqaL2 = 0, cld_errflg = 0
    INTEGER (KIND=2) :: QAflags, radBadPixflgs, errflg, errbase
    INTEGER (KIND=4) :: year, month, day, jday
    INTEGER (KIND=4) :: year_VIS, month_VIS, day_VIS, jday_VIS
    LOGICAL :: radLMissing = .FALSE., instIDmismatch = .FALSE., bit7Q = .FALSE. 
    REAL(kind=4), dimension(12) :: ref
    REAL(kind=4), dimension(12) :: ramcor, calc_fill, scale_fill, ramcor_save
    REAL(KIND=4), DIMENSION(12) :: nvadj
    REAL(kind=4) :: scl
    integer(kind=4) :: iostat  
    INTEGER (KIND=4) :: iwl, iX, iLine, iLine_s, iLine_b, iLine_e, iT, ii, nLw, iLat
    INTEGER (KIND=4) :: pixID
    INTEGER (KIND=4) :: status, ierr, i
    INTEGER (KIND=4) :: numfiles
    integer(kind=4) :: isdesc, isanom
    CHARACTER (LEN=PGSd_PC_FILE_PATH_MAX), DIMENSION(100) :: L1B_filenames
    CHARACTER( LEN=2048) :: L1B_swathlist
    CHARACTER (LEN=PGSd_PC_FILE_PATH_MAX) :: IRR_filename, UV_filename
    CHARACTER (LEN=PGSd_PC_FILE_PATH_MAX) :: VIS_filename
    CHARACTER (LEN=2048) :: IRR_swathname, UV_swathname, temp_filename
    CHARACTER (LEN=2048) :: VIS_swathname
    ! need different length for cfg, control file data
!    CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE ) :: msg, cloud_pressure_source, cfg
    CHARACTER( LEN = PGS_SMF_MAX_MSG_SIZE ) :: msg
    character (len=2048) :: cfg, aerosol_limit, residual_limit, cloud_pressure_source
    character (len=2048) :: cloud_filename, snow_filename, terrain_filename, omcldrr_filename, omufpmet_filename
    INTEGER, EXTERNAL :: OMI_pixGetCldPres
    INTEGER, EXTERNAL :: OMI_pixGetSnowIce
    INTEGER, EXTERNAL :: OMI_pixGetTerPres
    CHARACTER( LEN = PGSd_PC_FILE_PATH_MAX) :: snowice_source, temperature_source
    REAL(KIND = 4), DIMENSION(:), ALLOCATABLE :: wl_cor
    REAL(KIND = 4), DIMENSION(:,:), ALLOCATABLE :: swpcr
    INTEGER(KIND = 4), DIMENSION(:), ALLOCATABLE :: iwlSub
    INTEGER(KIND = 4) :: il, ih
    REAL(KIND = 4) :: frac
    REAL(KIND=4), PARAMETER :: EPSILON10=1.0E-4
    CHARACTER(LEN=128) :: filename
    REAL(KIND=4), DIMENSION(:), POINTER :: wl_uv2, wl_vis, xnvalm_uv2, xnvalm_vis
    INTEGER(KIND=4), DIMENSION(:), ALLOCATABLE :: channel
    INTEGER(KIND=4) :: ilo_uv2, ihi_uv2
    INTEGER(KIND=4) :: ilo_vis, ihi_vis
    INTEGER(KIND=4) :: ilo, ihi
    LOGICAL(KIND=4), ALLOCATABLE, DIMENSION(:) :: sensor_2_mask
    LOGICAL(KIND=4), ALLOCATABLE, DIMENSION(:) :: sensor_3_mask
    LOGICAL(KIND=4) :: using_sensor_2
    LOGICAL(KIND=4) :: using_sensor_3
    LOGICAL(KIND=4), DIMENSION(12) :: mask
    real(kind=4) :: psi
    real(kind=4), dimension(3) :: yresf
    integer(kind=4) :: version, j
    real(kind=4) :: rad_diag1, rad_diag2, rad_diag3
    real(kind=4) :: irr_diag1, irr_diag2, irr_diag3
    real(kind=4) :: rad_diag1_VIS, rad_diag2_VIS, rad_diag3_VIS
    real(kind=4) :: irr_diag1_VIS, irr_diag2_VIS, irr_diag3_VIS
    real(kind=4), dimension(:,:), allocatable :: radiance2, irradiance2, radiance2_VIS, irradiance2_VIS
    integer(kind=4), dimension(12) :: band_diag
    real(kind=4), dimension(11) :: t11
    integer(kind=4), dimension(4), parameter :: iwl_oe = (/ 2, 3, 6, 8 /)
    real(kind=4), dimension(4), parameter :: noise = (/ 0.3, 0.2, 0.2, 0.2 /)
    real(kind=4) :: deltar_limit, res_limit
    real(kind=4) :: band_uv2_irr, band_vis_irr
    real(kind=4) :: band_uv2_rad, band_vis_rad

    ! add coordiates to keep track of max and min latitudes and longitudes - MB 12/11/23
    real(kind=4) :: southernmost, northernmost, westernmost, easternmost, seconds

    ! OMCLDRR bad row manually excluded
    integer(kind=4), parameter :: cld_badrow = 31
    real(kind=4) :: res331_limit, res340_limit, gln340_limit
    character(len=255) :: str1, str2, str3, str4
    INTEGER, PARAMETER ::   OMI_S_SUCCESS = 15565312
    INTEGER, PARAMETER ::   OMI_E_FAILURE = 15568385
    INTEGER :: OMTO3_L2_LUN_NC = 449011
    ! load Nvalue from 92005 l2 file
    ! real(kind=4) :: l2_nval(12,60,1494)
    logical :: do_pscene

!    ! adding variables for control.txt - MB 01/18/24
!    character(len=CFG_KEY_LEN) :: buffer

    ! define latitude longitude most extreme coordinates for metadata - MB 12/11/13
    southernmost = 0
    northernmost = 0
    westernmost = 0
    easternmost = 0

!    ! get output from control - MB 01/18/24
!    status = GetConfigString("E", "Output Files", buffer)
!
!    write(6, *) 'output: ', status, trim(buffer)
!
!    ! get output from control - MB 01/18/24
!    status = GetConfigString("E", "Input Files OMCLDRR", buffer)
!
!    write(6, *) 'output: ', status, trim(buffer)

    ! get temperature from control - MB 01/18/24
    status = GetConfigString("E", "Runtime Parameters TEMPERATURESOURCE", temperature_source)
    if (status /= 0) then
        write(6, *) "temperature source not found, exiting."
        call exit(1)
    endif
    write(*,*) 'temperature source: ', trim(temperature_source)
!    status = PGS_PC_GetConfigData( TEMPERATURESOURCE_LUN, msg)
!    IF (status /= PGS_S_SUCCESS) THEN
!       WRITE( msg, '(A)') "temperature source not specified in PCF. exiting program."
!       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
!       CALL EXIT(1)
!    ELSE
!       READ( msg, '(A)') temperature_source
!    ENDIF

    IF (ADJUSTL(temperature_source) /= '"Climatology"' .and. &
       ADJUSTL(temperature_source) /= '"FPIT"') THEN
       WRITE( msg, '(A)') "temperature source not recognized. exiting program."
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! get snow ice from control - MB 01/18/24
    status = GetConfigString("E", "Runtime Parameters SNOWICESOURCE", snowice_source)
    if (status /= 0) then
        write(6, *) "snow ice source not found, exiting."
        call exit(1)
    endif
    write(*,*) 'snowice source: ', trim(snowice_source)
!    ! read and verify snowice input source
!    status = PGS_PC_GetConfigData( SNOWICESOURCE_LUN, msg)
!    IF (status /= PGS_S_SUCCESS) THEN
!       WRITE( msg, '(A)') "snowice source not specified in PCF. exiting program."
!       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
!       CALL EXIT(1)
!    ELSE
!       READ( msg, '(A)') snowice_source
!    ENDIF


    IF (ADJUSTL(snowice_source) /= '"Climatology"') THEN
       WRITE( msg, '(A)') "snowice source not recognized. exiting program."
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! load radiance table
    status = O3T_nval_setup( OMTO3_NVAL_LUN, nvRRS )
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "Read NVAL LUT failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    write(*,*) 'radiance table loaded.'

    ! set wavelengths
    status = O3T_getLambdaSet()

    write(*,*) 'wavelengths: ', wl_com(iwl_oe)

    ! load Jacobain table 
    status = O3T_dndx_setup( wl_cutoff)
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "Read DNDX LUT failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    write(*,*) 'jacobian table loaded.'

    ! compute common coefficient for Lagrangian interpolation
    status = O3T_lpoly_cden( LUT_cden_blk)
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "compute common interpolation coefficient failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! ! get UV2 irradiance swath list
    status = L1B_getNames( L1B_IRR_FILE_LUN, numfiles, L1B_filenames, L1B_swathlist)
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "get UV2 irradiance swath list failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! ! get UV2 irradiance swath
    ! status = L1B_selectIRR( "UV-2", USED_L1BIRR_LUN, IRR_filename, IRR_swathname, &
    !      IRR_FILE_TYPE, NORMAL_L1BIRR_MISSING, irr_error, irr_warning, irr_any)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "get UV-2 irradiance swath failed. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF

    ! ! check UV2 irradiance swath
    ! IF (irr_error > 0) THEN
    !    WRITE( msg,'(A)') "one or more MeasurementQualityflags was set in irradiance file:" // ADJUSTL(TRIM(IRR_filename)) // ". exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF

    ! get irradiance file from control - MB 01/18/24
    status = GetConfigString("E", "Input Files 1104", IRR_filename)
    if (status /= 0) then
        write(6, *) "irradiance file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'irradiance file name: ', trim(IRR_filename)
!    !col4 get filename
!    version=1
!    status = PGS_PC_getreference(L1B_IRR_FILE_LUN, version, IRR_filename)
!    IF (status /= PGS_S_SUCCESS ) then
!      PRINT *, "Unable to read IRRADIANCE FILE"
!      CALL EXIT(1)
!    ENDIF


    ! ! read the L1B irradiance file
    status = O3T_L1B_getIRR(IRR_filename, "BAND2_IRRADIANCE", "STANDARD_MODE")
    IF (status /= OMI_S_SUCCESS) THEN
       WRITE( msg,'(A)') "O3T_getIRR failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! get ancillary file from control - MB 01/18/24
    status = GetConfigString("E", "Input Files OMUANC", OMUANC_fn)
    if (status /= 0) then
        write(6, *) "ancillary file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'ancillary file name: ', trim(OMUANC_fn)
!    ! Read col4 ANC file
!    version=1
!    status = PGS_PC_getreference(500110, version, OMUANC_fn)
!    IF (status /= PGS_S_SUCCESS ) then
!      PRINT *, "Unable to read IRRADIANCE FILE"
!      CALL EXIT(1)
!    ENDIF

    call read_anc(OMUANC_fn)
    PRINT *," shape of row_anomaly", shape(row_anomaly)
    ! status = L1B_selectIRR( "VIS", USED_L1BIRR_LUN, &
    !                         IRR_filename, IRR_swathname, &
    !                         IRR_FILE_TYPE, NORMAL_L1BIRR_MISSING,  &
    !                         irr_error, irr_warning, irr_any)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "Get IRR file or VIS Swath Name failed, "// &
    !                       "program aborting. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF
    ! IF (irr_error > 0) THEN
    !    WRITE( msg,'(A)') "One or more VIS MeasurementQualityflags was "// &
    !          "set to Eorror in irradiance file:"// TRIM(IRR_filename) // &
    !          ". Alternative file should be staged. program aborting. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF

    status = O3T_L1B_getIRR_VIS(IRR_filename, "BAND3_IRRADIANCE", "STANDARD_MODE")
    IF (status /= OMI_S_SUCCESS) THEN
       WRITE( msg,'(A)') "O3T_getIRR_VIS failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! get uv radiance file from control - MB 01/18/24
    status = GetConfigString("E", "Input Files OML1BRUG", UV_filename)
    if (status /= 0) then
        write(6, *) "uv radiance file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'uv radiance file name: ', trim(UV_filename)

    ! get cloud pressure file name from control - MB 01/19/24
    status = GetConfigString("E", "Input Files 400151", cloud_filename)
    if (status /= 0) then
        write(6, *) "cloud pressure file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'cloud pressure file name: ', trim(cloud_filename)

    ! get omcldrr file name from control - MB 01/19/24
    status = GetConfigString("E", "Input Files OMCLDRR", omcldrr_filename)
    if (status /= 0) then
        write(6, *) "omcldrr file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'omcldrr file name: ', trim(omcldrr_filename)

    ! get terrain pressure file name from control - MB 01/19/24
    status = GetConfigString("E", "Input Files 400150", terrain_filename)
    if (status /= 0) then
        write(6, *) "terrain pressure file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'terrain pressure file name: ', trim(terrain_filename)

    ! get omufpmet file name from control - MB 01/19/24
    status = GetConfigString("E", "Input Files OMUFPMET", omufpmet_filename)
    if (status /= 0) then
        write(6, *) "omufpmet file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'omufpmet file name: ', trim(omufpmet_filename)

    ! get snow ice file name from control - MB 01/19/24
    status = GetConfigString("E", "Input Files 400152", snow_filename)
    if (status /= 0) then
        write(6, *) "snow ice file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'snow ice file name: ', trim(snow_filename)

!    ! !! open the L1B radiance file
!    status = L1B_getNames( L1B_UV_FILE_LUN, numfiles, L1B_filenames, &
!                           L1B_swathlist)
!    IF (numfiles /= 1) THEN
!       WRITE( msg,'(A)') "Number of L1BRUG file not equal to 1, " // &
!                          "program aborting. exiting."
!       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
!       CALL EXIT(1)
!    ELSE
!       UV_filename = L1B_filenames(1)
!    ENDIF

    ! IF (INDEX( TRIM(L1B_swathlist), "Earth UV-2 Swath") == 0) THEN
    !    WRITE( msg,'(A)') "file:" // TRIM(UV_filename)//"," // &
    !                       TRIM(L1B_swathlist)//", does not have UV-2 Swath"//&
    !                       ". exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ELSE
    !    UV_swathname = "Earth UV-2 Swath"
    ! ENDIF

    status = O3T_L1B_initRAD( UV_filename, "BAND2_RADIANCE", "STANDARD_MODE")
    IF (status /= OMI_S_SUCCESS) THEN
       WRITE( msg,'(A)') "O3T_L1B_initRAD failed. exiting." 
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    !nc4 RAM - allocate level-2 arrrays. from O3T_radgeo_class
    status = initL2out(nTimes_rad, nXTrack_rad, nwl_com, nWavel_ret=4, nLayers=11)

    ! get vis radiance file from control - MB 01/18/24
    status = GetConfigString("E", "Input Files OML1BRVG", VIS_filename)
    if (status /= 0) then
        write(6, *) "vis radiance file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'vis radiance file name: ', trim(VIS_filename)
!    status = L1B_getNames( L1B_VIS_FILE_LUN, numfiles, L1B_filenames, &
!                           L1B_swathlist)
!    IF (numfiles /= 1) THEN
!       WRITE( msg,'(A)') "Number of L1BRVG file not equal to 1, " // &
!                          "program aborting. exiting."
!       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
!       CALL EXIT(1)
!    ELSE
!       VIS_filename = L1B_filenames(1)
!    ENDIF

    ! IF (INDEX( TRIM(L1B_swathlist), "Earth VIS Swath") == 0) THEN
    !    WRITE( msg,'(A)') "file:" // TRIM(VIS_filename)//"," // &
    !                       TRIM(L1B_swathlist)//", does not have VIS Swath"//&
    !                       ". exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ELSE
    !    VIS_swathname = "Earth VIS Swath"
    ! ENDIF

    ! status = O3T_L1B_initRAD_VIS( VIS_filename, VIS_swathname)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "O3T_L1B_initRAD_VIS failed. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF

    status = O3T_L1B_initRAD_VIS(VIS_filename, "BAND3_RADIANCE", &
                  "STANDARD_MODE") 
    ! write(*,*) 'earth-sun distance: ', earthsundistance, earthsundistance_vis

    ! setup storage for L2 output, number of wavlengths in output is determined by 
    ! fixed output wavelength grid. nXtrack nTimes are determined by input L1B. 
    status = O3T_initL2out( wl_com)

    write(*,'(A5,1X,12F9.2)') 'wlen:', wl_com
    write(*,*) 'wlen ref1  ref2:', wl_com(iwl_ref1), wl_com(iwl_ref2)
    write(*,*) 'wlen oz1 oz2:', wl_com(iwl_oz1), wl_com(iwl_oz2)
    write(*,*) 'wlen oe:', wl_com(iwl_oe)
    write(*,*) 'noise:', noise


    write(*,'(A5,1X,12F9.2)') 'wlen:', wl_com 
    write(*,*) 'aercor_fac', aercor_fac
    write(*,*) 'aercor_fac ref', aercor_fac( (/ iwl_ref1, iwl_ref2 /) )
    write(*,*) 'aercor_fac oe', aercor_fac( iwl_oe )
    write(*,*) 'aercor_fac stp1', aercor_fac( (/ iwl_oz1, iwl_oz2  /) )
    ! get the irradiance QA flags and precision at the fixed output wavelength grid.
    status = L1Bri_interpWL(irrWavelength, irrQAflags, wl_com, irrQAflags_com, &
         irrPrecision, irrPrecision_com)
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "re-gridding IRR QA flags and precison failed. exiting."
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! read metadata from irradiance file
    ! status = L1B_getCoreArchivedMetaData( USED_L1BIRR_LUN, L1BIRRcoreArch, year, month, day, jday)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "L1B_getCoreArchivedMetaData IRR failed. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF

    ! read metadata from UV2 radiance file
    ! status = L1B_getCoreArchivedMetaData( L1B_UV_FILE_LUN, L1BUV2coreArch, year, month, day, jday)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "L1B_getCoreArchivedMetaData UV2 failed. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF
    ! shortname = L1BUV2coreArch%ShortName

    ! ! read metadata from VIS radiance file
    ! status = L1B_getCoreArchivedMetaData( L1B_VIS_FILE_LUN, L1BVIScoreArch, &
    !      year_VIS, month_VIS, day_VIS, jday_VIS)
    ! IF (status /= OZT_S_SUCCESS) THEN
    !    WRITE( msg,'(A)') "L1B_getCoreArchivedMetaData VIS failed. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
    !    CALL EXIT(1)
    ! ENDIF
    ! shortname_VIS = L1BVIScoreArch%ShortName

    ! TODO : ADD CHECK THAT YEAR, MONTH, DAY FOR UV2 AND VIS MATCH

    ! year = 2011
    ! month = 7
    ! day = 20
    ! jday = 201
    year = 2019
    month = 8
    day =6
    jday = 218
    status = L1Br_getGlobalAttr(L1B_blk, year, month, day)
    write(*,*) year, month, day

    ! skip zoom mode.
    ! IF (INDEX(TRIM(ADJUSTL(shortname)), 'Z', back=.true.) == LEN_TRIM(shortname)) THEN
    !    WRITE( msg,'(A)') "orbit is zoom mode. exiting."
    !    ierr = OMI_SMF_setmsg( OZT_S_SUCCESS, msg, "main", zero)
    !    CALL EXIT(0)
    ! ENDIF

    ! get cloud prssure file from control, default to Climatology - MB 01/18/24
    status = GetConfigString("E", "Runtime Parameters CLOUDPRESSURESOURCE", cloud_pressure_source)
    if (status /= 0) then
        write(6, *) "Climatology assumed as cloud pressure source."
    endif
    write(*,*) 'cloud pressure source file name: ', trim(cloud_pressure_source)

    ! init cloud pressure source ( cloud pressure source as input instead of output ) - MB 01/19/24
    !
    status = O3T_initCLD( cloud_pressure_source)

!    write(*,*) 'cloud pressure source:', trim(cloud_pressure_source)
    
    ! read temperature climatology

     status = climtm_read()                                                                                                           
     IF( status /= OZT_S_SUCCESS ) THEN
        WRITE( msg,'(A)' ) "Read climatology data failed, " // &                                                                      
                           "PGE aborting, exit code = 1."                                                                             
        ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero )  
        CALL EXIT(1)
     ENDIF

     ! nc4 - Using netCDF OMUFPMET (72 layers) from AS 19994.
     ! call read_geosmet()
     call read_geosmet_nc4()

    write(*,*) 'read geos met input'

    ! get row anomaly flag
!nc4    status= get_anomflg_3(year,jday,anomflg_3)
!nc4    write(*,'(A)') 'anomaly flag:'
!nc4    do i=1,60
!nc4       write(*,'(I2,1X,36I1)') i, anomflg_3(i,:)
!nc4    enddo
    ! nc4 - Use row anomaly from OMUANC flag


    ! get aerosol correction factor
    status= get_aercor(aercor_wave, aercor_fac)

    write(*,'(A)') 'aerosol correction factor:'
    do i=1, 12 
       write(*,'(F7.2,2X,F4.2,2X,L)') aercor_wave(i), aercor_fac(i), wl_com(i)-aercor_wave(i) < 0.1
    enddo

    ! init soft-calibration correction
!    status = OmiNVCinfo()
!    ALLOCATE(wl_cor(nWvc))
!    ALLOCATE(iwlSub(nWvc))
!    ALLOCATE(swpcr(nWvc,nXtc))
    ALLOCATE(wl_cor(12))
    ALLOCATE(iwlSub(12))
    ALLOCATE(swpcr(12,60))
!    status = OmiNvalueCorr( L1BUV2coreArch%orbitNumber, wl_cor, swpcr)

    !swpcr(:,:)=0.0
    swpcr=nvcorr
    write(*,*) shape(swpcr), shape(nvcorr)
    write(*,'(12F12.3)') swpcr

    ! get satellite/instrument code ((get rid of this)
    ! satname = O3T_getSatName( L1BUV2coreArch%ShortName, OMTO3_NVAL_LUN)

    ! get output file name from control - MB 01/19/24
!     create and set up the output file
!    status = get_L2_nc_filename(OMTO3_L2_LUN_NC, OMTO3_fn_nc)
    status = GetConfigString("E", "Output Files OMTO3", OMTO3_fn_nc)
    if (status /= 0) then
        write(6, *) "output file name not found, exiting."
        call exit(1)
    endif
    write(*,*) 'output file: ', trim(OMTO3_fn_nc)

    ! set up swath in the output file
    dimList = "nWavel,nWavelRet,nXtrack,nTimes,nLayers,nTimesSmallPixel"
    dims(1:6) = (/ nwl_com,nWavelRet,nXtrack_rad,nTimes_rad,NLYR,nTimesSmallPixel_rad /)

    ! write retrieval wavelengths to output
    df_omto3wl(:) = (/ df_Wavelength /)

    ! define new block for wavelengths in output

    ! transfer wavelength data (does it need to be done this way?)

    ! srite soft-calibration adjustments to output block
    df_caladj(:) = (/ df_NValueAdjustment /)

    ! define new block for soft-calibration in output

!    ! transfer soft-calibration adjustments to output block (must be a better way)
!    calblk%data(:) = TRANSFER( swpcr(:,:),  I1, nwl_com*nxtrack*calblk%elmSize(1))


! THIS CODE CHECKS THAT SOFT CAL WAVELENGTHS ARE SAME AS ALGORTHM WAVELENGTHS

! no soft cal for now
    ! ! match soft-calibration wavelengths to table wavelengths
    ! ! notreally necessary -- this complexity. 
    ! ! TODO: can check that wavelengths are all the same more easily.
    ! DO iwl = 1, nWvc ! what is nWvc -- this section is a mess -- clean it up
    !    write(*,*) iwl
    !    call flush(6)
    !   il = iwl 
    !   il = hunt( wl_com(1:nwl_com), wl_cor(iwl), il)
    !   IF (il == 0 .OR. il == nwl_com) THEN
    !      ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
    !           "wl_cor outside wl_com range. exiting.", "main", zero)
    !      CALL EXIT(1)
    !   ELSE
    !      ih = il+1
    !      frac = (wl_cor(iwl) - wl_com(il))/ (wl_com(ih) - wl_com(il))
    !      IF (ABS( frac) < EPSILON10) THEN
    !         ih = il
    !      ELSE IF (ABS( frac -1.0) < EPSILON10) THEN
    !         il = ih
    

    !      ENDIF
    !      IF (ih /= il) THEN
    !         ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
    !              "wl_cor not mathced any in wl_com. exiting.", "main", zero)
    !         CALL EXIT(1)
    !      ENDIF
    !   ENDIF
    !   iwlSub(iwl) = ih 
    ! ENDDO

    ! hard coded, UV2 = 2, VIS = 3 ; change to read from table
    sensor_id = (/ 2,2,2,2,2,2,2,2,2,2,3,3 /)

    ! b vs. s stuff is weird. get rid of it.
    iLine_b = 0
    iLine_s = iLine_b

    ! write wavelength and soft-calibration data to file.  
    ! (this seems like a kludge to do this in main)
!    status = L2_writeBlock( calblk, iLine_s, size(swpcr))
!    CALL L2_disposeBlockW(calblk)

    ! copy these for OMI only
    ! IF (TRIM(L1BUV2coreArch%ShortName) == "OML1BRUG") THEN
    !    df_copy(:) = (/ df_NumberSmallPixelColumns,  &
    !                    df_SmallPixelColumn       ,  &
    !                    df_InstrumentConfigurationId /)
    !    status = L2_defSWdatafields( OMTO3_swid, df_copy)
    !    IF (status /= OZT_S_SUCCESS) THEN
    !       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
    !            "L2_defSWdatafields failed. exiting", "main", zero)
    !       CALL EXIT(1)
    !    ENDIF
    ! ENDIF

    ! construct geolocation fields
    gf_omto3(:) = (/ gf_GroundPixelQualityFlags, &      
                     gf_Latitude               , &
                     gf_Longitude              , &
                     gf_SolarZenithAngle       , &
                     gf_SolarAzimuthAngle      , &      ! OMI specific
                     gf_ViewingZenithAngle     , &
                     gf_ViewingAzimuthAngle    , &      ! OMI specific
                     gf_RelativeAzimuthAngle   , &
                     gf_TerrainHeight          , &
                     gf_Time                   , &      
                     gf_SecondsInDay           , &
                     gf_SpacecraftLatitude     , &
                     gf_SpacecraftLongitude    , &
                     gf_SpacecraftAltitude     , &
                     gf_XTrackQualityFlags     /)       ! OMI specific

    ! construct data fields
    df_omto3(:) = (/ df_CloudPressure,           &
                     df_TerrainPressure,         & 
                     df_AlgorithmFlags,          & 
                     df_QualityFlags,            & 
                     df_DegreesFreedomforSignal, &
                     df_ProfileShapeError,       &
                     df_CloudFraction,           &
                     df_ColumnAmountO3,          &
                     df_OzoneUnderCloud,         &
                     df_Reflectivity331,         &
                     df_Reflectivity340,         & 
                     df_Reflectivity354,         &
                     df_MeasurementNoise,        &
                     df_StepOneO3,               &
                     df_StepTwoO3,               &
                     df_StepTwoProfileShapeError,&
                     df_dNdR,                    &  
                     df_NValue,                  &
                     df_ResidualStep1,           &
                     df_ResidualStep2,           &
                     df_Sensitivity,             &
                     df_dNdT,                    &
                     df_APrioriLayerO3,          &
                     df_ColumnWeightingFunction, &
                     df_LayerTemperature,        & ! nLayer * nX * NY
                     df_APrioriLayerO3Covariance,&
                     df_StepTwoLayerO3,          &
                     df_StepTwoColumnWeightingFunction,&
                     df_LayerAmountO3,           &
                     df_LayerAmountO3Covariance, &
                     df_ColumnGain,              & ! nSignal * nX * NY
                     df_MeasurementQualityFlags /) ! OMI specific

    ! define geolocation and data fields in output file

    ! allocate memory for output data block.
    ! Kai's note: memory allocated before for wl_com is deallocated 
    ! and reallocated for the new df_omto3 data fields.


    call read_apriori() ! v9 climatology 

    write(6, *) 'getting limits...'

    ! get aerosol limit from control
    status = GetConfigString("E", "Runtime Parameters Aerosol Limit", aerosol_limit)
    if (status /= 0) then
        write(6, *) "aerosol limit not found, exiting."
        call exit(1)
    else
        read(aerosol_limit, *) res340_limit
    endif
    write(*,*) 'res340_limit: ', res340_limit
!    status = PGS_PC_GetConfigData(AER_LIMIT_ID, cfg)
!    if (status /= PGS_S_SUCCESS) then
!       write(msg,*) "Delta R limit not specified in PCF. exiting program."
!       ierr = omi_smf_setmsg(OZT_E_FAILURE, msg, "main", zero)
!       call exit(1)
!    else
!       read(cfg,*) res340_limit
!    endif

    ! get residual limit from control
    status = GetConfigString("E", "Runtime Parameters Residual Limit", residual_limit)
    if (status /= 0) then
        write(6, *) "residual limit not found, exiting."
        call exit(1)
    else
        read(residual_limit, *) res331_limit
    endif
    write(*,*) 'res331_limit: ', res331_limit
!    status = PGS_PC_GetConfigData(RES_LIMIT_ID, cfg)
!    if (status /= PGS_S_SUCCESS) then
!       write(msg,*) "Residual limit not specified in PCF. exiting program."
!       ierr = omi_smf_setmsg(OZT_E_FAILURE, msg, "main", zero)
!       call exit(1)
!    else
!       read(cfg,*) res331_limit
!    endif

!    write(*,*) 'res340 limit: ', res340_limit
!    write(*,*) 'res331 limit: ', res331_limit

    ! determine if orbit is acending or descending using latitude data from first two lines
    IF (MOD( nXtrack_rad, 2) == 0) THEN
       nXevenQ = .TRUE.
    ELSE
       nXevenQ = .FALSE.
    ENDIF

    iLine = 0
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
    status = L1Br_getGEOline( L1B_blk, iLine+1, time, & 
                  Latitude_k = latitude, Longitude_k = longitude, &
                     SolarZenithAngle_k = szenith, &
                     SolarAzimuthAngle_k=sazimuth, &
                     ViewingZenithAngle_k = vzenith, &
                     ViewingAzimuthAngle_k = vazimuth, &
                     TerrainHeight_k = height, &
                     SpacecraftLatitude_k = scLat, &
                     SpacecraftLongitude_k = scLon )
         ! szenith, sazimuth, vzenith, vazimuth,  height, geoflg)

    ! set seconds time for metadata - MB 12/11/23
    seconds = time

    IF (nXevenQ) THEN
       latPre = (latitude(nXtrack_rad/2-1) + latitude(nXtrack_rad/2))*0.5
    ELSE
       latPre = latitude((nXtrack_rad-1)/2)
    ENDIF

    iLine = 1
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
    status = L1Br_getGEOline( L1B_blk, iLine+1, time, &
                  Latitude_k = latitude, Longitude_k = longitude, &
                     SolarZenithAngle_k = szenith, &
                     SolarAzimuthAngle_k=sazimuth, &
                     ViewingZenithAngle_k = vzenith, &
                     ViewingAzimuthAngle_k = vazimuth, &
                     TerrainHeight_k = height, &
                     SpacecraftLatitude_k = scLat, &
                     SpacecraftLongitude_k = scLon )

    IF (nXevenQ) THEN
       latNow = (latitude(nXtrack_rad/2-1) + latitude(nXtrack_rad/2))*0.5
    ELSE
       latNow = latitude((nXtrack_rad-1)/2)
    ENDIF
    descendQ = O3T_descendQ( latPre, latNow) ! logical

    ! initialize metadata values
    L2_parameters(1)%NumberOfInputSamples          = nTimes_rad*nXtrack_rad
    L2_parameters(1)%NumberOfGoodInputSamples      = 0
    L2_parameters(1)%NumberOfLargeSZAInputSamples  = 0
    L2_parameters(1)%NumberOfMissingInputSamples   = 0
    L2_parameters(1)%NumberOfBadInputSamples       = 0
    L2_parameters(1)%NumberOfInputWarningSamples   = 0
    L2_parameters(1)%NumberOfGoodOutputSamples     = 0
    L2_parameters(1)%NumberOfGlintCorrectedSamples = 0
    L2_parameters(1)%NumberOfSkippedSamples        = 0
    L2_parameters(1)%NumberOfStep1InvalidSamples   = 0
    L2_parameters(1)%NumberOfStep2InvalidSamples   = 0
    L2_parameters(1)%NumberOfIrradianceMissing     = 0
    L2_parameters(1)%NumberOfIrradianceError       = 0
    L2_parameters(1)%NumberOfIrradianceWarning     = 0
    L2_parameters(1)%NumberOfRadianceMissing       = 0
    L2_parameters(1)%NumberOfRadianceError         = 0
    L2_parameters(1)%NumberOfRadianceWarning       = 0
    L2_parameters(1)%NumberOfMeasurement           = nTimes_rad
    L2_parameters(1)%NumberOfMeasurementMissing    = 0
    L2_parameters(1)%NumberOfMeasurementError      = 0
    L2_parameters(1)%NumberOfMeasurementWarning    = 0
    L2_parameters(1)%NumberOfMeasurementRebinned   = 0
    L2_parameters(1)%NumberOfMeasurementSAA        = 0
    L2_parameters(1)%NumberOfMeasurementManeuver   = 0
    L2_parameters(1)%NumberOfInstrumentSettingsError= 0
    L2_parameters(1)%ZonalOzoneMin(1:5)            =-1000.0
    L2_parameters(1)%ZonalOzoneMax(1:5)            = 1000.0
    L2_parameters(1)%ZonalLatRange(1,1:5) = (/ -90, -60, -30, 30, 60 /)
    L2_parameters(1)%ZonalLatRange(2,1:5) = (/ -60, -30,  30, 60, 90 /)

    ! channel = (/ (iwl, iwl=1, 12) /)
    ! mask = .true.
    ! mask(11:12) = .false.
    ! ilo = MINVAL(PACK(channel, mask))
    ! ihi = MAXVAL(PACK(channel, mask))

    ! write(*,*) 'UV2:', wl_com(ilo:ihi)
    
    ! mask = .not. mask
    ! ilo = MINVAL(PACK(channel, mask))
    ! ihi = MAXVAL(PACK(channel, mask))
    ! write(*,*) 'VIS:', wl_com(ilo:ihi)

    ! write(*,*) 'OE wlen: ', wl_com(iwl_oe)
    ! write(*,*) 'SQRT(Se):', noise

    !%%% process data one line at a time %%%

    iLine_s = iLine_b
    iLine_e = nTimes_rad-1
    
    ! transfer soft-calibration adjustments to output block (must be a better way)
    DO iLine = iLine_s, iLine_e

       ! progress meter
       if (mod(iline,25) .eq. 0) write(*,'(A1,1X,I3)') "#", iline/25

      ! get UV-2 and VIS radiance lines
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
      status = L1Br_getGEOline( L1B_blk, iLine+1, time, &
                  Latitude_k = latitude, Longitude_k = longitude, &
                     SolarZenithAngle_k = szenith, &
                     SolarAzimuthAngle_k=sazimuth, &
                     ViewingZenithAngle_k = vzenith, &
                     ViewingAzimuthAngle_k = vazimuth, &
                     TerrainHeight_k = height, &
                     SpacecraftLatitude_k = scLat, &
                     SpacecraftLongitude_k = scLon, &
                     GroundPixelQualityFlags_k = l1b_GPQ, &
                     XTrackQualityFlags_k = l1b_xtrack_qual)
                     ! geoflg, anomflg, sInD, cHgt)
      geoflg = 0
      anomflg = 0
      IF (status /=  OMI_S_SUCCESS) THEN
         WRITE( msg,'(A)') "L1Br_getGEOline failed. exiting."
         ierr = OMI_SMF_setmsg( OMI_E_FAILURE, msg, "main", zero)
         CALL EXIT(1)
      ENDIF
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
      status = L1Br_getGEOline( L1B_blk_VIS, iLine+1, time_VIS, &
                  Latitude_k = latitude_VIS, Longitude_k = longitude_VIS, &
                     SolarZenithAngle_k = szenith_VIS, &
                     SolarAzimuthAngle_k=sazimuth_VIS, &
                     ViewingZenithAngle_k = vzenith_VIS, &
                     ViewingAzimuthAngle_k = vazimuth_VIS, &
                     TerrainHeight_k = height_VIS, &
                     SpacecraftLatitude_k = scLat_VIS, &
                     SpacecraftLongitude_k = scLon_VIS )
                     ! scLat_VIS, scLon_VIS, scHgt_VIS)
      IF (status /=  OMI_S_SUCCESS) THEN
         WRITE( msg,'(A)') "L1Br_getGEOline on VIS failed. exiting."
         ierr = OMI_SMF_setmsg( OMI_E_FAILURE, msg, "main", zero)
         CALL EXIT(1)
      ENDIF
      DO iX = 1, nXtrack_rad
         ! compute relative azimuth angles from UV-2 and VIS geolocation
         phiArray(iX) = adjustDEG(180.0+sazimuth(iX)-vazimuth(iX))
         phiArray_VIS(iX) = adjustDEG(180.0+sazimuth_VIS(iX)-vazimuth_VIS(iX))
         ! get terrain height and snowice coverage data
         status = OMI_pixGetTerPres(latitude(iX), longitude(iX), ptArray(iX))
         status = OMI_pixGetSnowIce(latitude(iX), longitude(iX), year, month, day, snowIceArray(iX))
      ENDDO

       ! check for bounding latitudes and longitudes  -MB 12/11/23
       if (minval(latitude).lt.southernmost) then
            southernmost = minval(latitude)
       endif
       if (maxval(latitude).gt.northernmost) then
           northernmost = maxval(latitude)
       endif
       if (minval(longitude).lt.westernmost) then
           westernmost = minval(longitude)
       endif
       if (maxval(longitude).gt.easternmost) then
           easternmost = maxval(longitude)
       endif

      ! get cloud pressure information
      IF(TRIM(ADJUSTL(cloud_pressure_source)) == '"Climatology"') THEN
         DO iX = 1, nXtrack_rad
           status = OMI_pixGetCldPres(latitude(iX), longitude(iX), year, month, day, pcArray(iX))
           ! ensure cloud pressure is inside range 200.0 to 1013.25 hPa.
           PclimQ(iX) = .TRUE.
           IF(pcArray(iX) < Pc_min) THEN
              pcArray(iX) = Pc_min
           ELSE IF(pcArray(iX) > Pc_max) THEN
              pcArray(iX) = Pc_max
           ENDIF
           ! force cloud pressure <= terrain pressure.
           IF (ptArray(iX) < pcArray(iX)) pcArray(iX) = ptArray(iX)
         ENDDO
      ELSE
         CALL O3T_getOMICldPress(iLine, pcArray, rcfArrayCloud, ptArrayCloud, nXtrack_rad)
         PclimQ(1:nXtrack_rad) = .FALSE.
         DO iX = 1, nXtrack_rad
           IF (TRIM(cloud_pressure_source) == '"OMCLDO2"') THEN
              cld_errflg = IBITS( ProcessingQualityFlags(iX),5,7)
           ELSE IF (TRIM(cloud_pressure_source) == '"OMCLDRR"') THEN
              cld_errflg = IBITS( ProcessingQualityFlags(iX),0, 3) &
                         + IBITS( ProcessingQualityFlags(iX),6, 2) &
                         + IBITS( ProcessingQualityFlags(iX),14,2) 
           ENDIF

! @ TURN OFF ALL SCREENING OF OCP DATA
           IF (.false.) THEN
           
           ! if OMI cloud pressure is bad, use climatological cloud pressure instead.
           IF (cld_errflg > 0 .or. iX == cld_badrow) THEN
              status = OMI_pixGetCldPres( latitude(iX), longitude(iX), year, month, day, &
                   pcArray(iX))
              PclimQ(iX) = .TRUE.
           ENDIF
           ! force cloud pressure in range 250.0 to 1013.25 hPa.
           IF(pcArray(iX) < Pc_min) THEN
              pcArray(iX) = Pc_min
           ELSE IF(pcArray(iX) > Pc_max) THEN
              pcArray(iX) = Pc_max
           ENDIF
           ! force cloud pressure <= terrain pressure
           IF(ptArray(iX) < pcArray(iX)) pcArray(iX) = ptArray(iX)

           ENDIF
           
         ENDDO
      ENDIF
      ! counter for lines in block
      iT = MOD( iLine, nLinesPerWrite) + 1

! @ MOVE PT and PC ARRAY OUTPUT BELOW
!      ! copy all geolocation data from input file to output file
!      CALL L2setGeoLine( iLine+1)

      ! PRINT *, "shape of radiance:",shape(radiance)
      ! get one line of UV-2 radiance data
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
      status = L1Br_getSIGline(L1B_blk, iLine+1, Signal_k=radiance, &
           ! RadIrrPrecision_k=radPrecision, PixelQualityFlags_k=radQAflags, &
         SpectralQuality_k=l1b_spec_qual, &
           QualityLevel_k=l1b_qual_level, &
           Wavelength_k=radWavelength)
      IF (status /=  OMI_S_SUCCESS) THEN
         WRITE( msg,'(A)') "L1Br_getSIGlineWL failed. exiting."
         ierr = OMI_SMF_setmsg( OMI_E_FAILURE, msg, "main", zero)
         CALL EXIT(1)
      ENDIF

      ! PRINT *,  iLine+1,shape(radiance(:,iX))
      ! get one line of VIS radiance data
    ! NC4 L1B col4 uses FORTRAN(1-based index), so add 1 to iline
      status = L1Br_getSIGline(L1B_blk_VIS, iLine+1, Signal_k=radiance_VIS, &
           ! RadIrrPrecision_k=radPrecision_VIS, PixelQualityFlags_k=radQAflags_VIS, &
         SpectralQuality_k=l1b_spec_qual_VIS, &
           QualityLevel_k=l1b_qual_level_VIS, &
           Wavelength_k=radWavelength_VIS)
      IF (status /=  OMI_S_SUCCESS) THEN
         WRITE( msg,'(A)') "L1Br_getSIGlineWL on VIS failed. exiting."
         ierr = OMI_SMF_setmsg( OMI_E_FAILURE, msg, "main", zero)
         CALL EXIT(1)
      ENDIF
    !TODO-RAM  Set proper flags when we get col4 definitions
    !nc4 Set radQAflags and radQAflags_VIS to 0
    radQAflags = 0
    radQAflags_VIS = 0
    irrQAflags = 0
       ! PRINT *,  iLine+1,shape(radiance_VIS(:,iX))
      !
      ! remove this - todo
      !
      ! get radiance QA flags and precision at the fixed output wavelength grid.
      status = L1Bri_interpWL(radWavelength, radQAflags, wl_com, &
           radQAflags_com, radPrecision, radPrecision_com)
   
      if (.not.allocated(radiance2)) then
         allocate( &
              radiance2(nWavel_rad, nXtrack_rad), &
              irradiance2(nWavel_irr, nXtrack_irr), &
              radiance2_VIS(nWavel_rad_VIS, nXtrack_rad_VIS), &
              irradiance2_VIS(nWavel_irr_VIS, nXtrack_irr_VIS), &
              stat = status)
      endif

      ! check if line is descending using latitude at nadir postion
      IF(iLine > 0) THEN
         IF(nXevenQ) THEN
            latNow = (latitude(nXtrack_rad/2-1) + latitude(nXtrack_rad/2))*0.5
         ELSE
            latNow = latitude((nXtrack_rad-1)/2)
         ENDIF
         descendQ = O3T_descendQ(latPre, latNow)
         latPre = latNow
      ENDIF
    
      ! begin processing one line
      DO iX = 1, nXtrack_rad
        iwl_oz             = iwl_oz1
        skipit             = .FALSE.
        sza_p              = szenith(iX)
        vza_p              = vzenith(iX)
        sza_p_VIS          = szenith_VIS(iX)
        vza_p_VIS          = vzenith_VIS(iX)
        phi                = phiArray(iX)
        lat                = latitude(iX)
        lon                = longitude(iX)
        phi_VIS            = phiArray_VIS(iX)
        lat_VIS            = latitude_VIS(iX)
        lon_VIS            = longitude_VIS(iX)
        hgt                = height(iX)
! @ HACK SCENE PRESSURE INTO PTARRAY(IX); PC REMAINS UNCHANGED
        do_pscene=.TRUE.
        if (do_pscene) then
           ptArray(iX) = pcArray(iX)*rcfArrayCloud(iX) + &
                ptArrayCloud(iX)*(1.0-rcfArrayCloud(iX))
           ! force scene pressure <= terrain pressure.
           if (ptArray(iX) > ptArrayCloud(iX)) ptArray(iX) = ptArrayCloud(iX)
        else
           ptArray(iX) = ptArrayCloud(iX)
        endif
        pt                 = ptArray(iX)/Pc_max
        pc                 = pcArray(iX)/Pc_max


! @ MOVED OUTPUT OF PT AND PC TO HERE SO PSCENE STORED IN PT IS OUTPUT
        ! copy all geolocation data from input file to output file PUSH TO END
        CALL L2setGeoLine( iLine+1)

! NOTE: pcArray and ptArray are stored in L2 output, not pt and pc
! handle cases when RCF is bad
! RCF < 0.05  -- screen this using the OMCLDRR qual flags?
! decide what to do about forcing the range within bounds
! remove ozone from beneath terrain

        ! initialize algorithm flag to skipped
        algflg = 0

        ! this should never happen - does geolocation error catch this?
	if (lat < -180) then
            skipit = .true.
            IF (skipit) THEN
               qaflags = errflg+qaflags
               CALL L2fillDataPix(iLine+1, iX, algflg, QAflags, phi)
!               CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
               ierr = OMI_SMF_setmsg( OZT_E_FAILURE, "skipped sample.", "main", zero)
               CYCLE
            ENDIF
        else 
           ilat = (lat+90)/5+1 ! change to lower bounds search?
           if (ilat > 36) then
              ilat=36
           endif
        endif

        ! set snow/ice based on threshold on climatology
        if (snowicearray(ix) >= 95.0) then
           pixsurf%isnow = 10 ! TODO remove, redundant
           isnow = 10
           algflg = 10  ! reinitiatize. value >= 10 indicates snow/ice
        else
           pixsurf%isnow = 0 ! TODO remove, redundant
           isnow = 0
        endif
        
        IF(TRIM(ADJUSTL(temperature_source)) == '"Climatology"') THEN
           status = climtm_prf(lat, jday, aprftm ) ! add flag for temperature profile source
           IF( status .NE. OZT_S_SUCCESS ) THEN
              ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
                   "O3T_climtm_prf:determine climtm temperature profile failed", &
                   "main", zero )
              RETURN
           ENDIF
        ELSE
! nc4 - Using 72 layer netCDF-4 OMUFPMET from AS 19994.
           ! call relayer_temp_g47_u11(geosmet_delp(:,iX,iLine), geosmet_temp(:,iX,iLine), aprftm, status)
           call relayer_temp_g72_u11(geosmet_delp(:,iX,iLine), geosmet_temp(:,iX,iLine), aprftm, status)
           IF (any(aprftm < 100. .OR. aprftm > 400.)) THEN ! basic qc for temp profile
              status = climtm_prf(lat, jday, aprftm ) ! add flag for temperature profile source
              IF( status .NE. OZT_S_SUCCESS ) THEN
                 ierr = OMI_SMF_setmsg( OZT_E_FAILURE, &
                      "O3T_climtm_prf:determine climtm temperature profile failed", &
                      "main", zero )
                 skipit=.TRUE.
              ENDIF
           ENDIF
        ENDIF
        
        ! qaflags are used first to store qa bits; then add errflg
        ! for final quality flag data field.

        ! init error and quality flags to 0
        qaflags = 0
        errflg = 0
              
        ! descending mode
        if (descendQ) then
           qaflags=ibset(qaflags,4)
        endif

        ! row anomaly flag
!nc4        if (anomflg_3(ix,ilat) == 1) then
!nc4           qaflags=ibset(qaflags,6)
!nc4        endif
        ! Use row anomally from OMUANC to set flag
        if (row_anomaly(iX,iLine+1,1) == 1) then
           qaflags=ibset(qaflags,6)
        endif
        
        ! flag cloud climatology used

        ! flag temperature climatology or assim temp used
        
         psi =acos(cos(sza_p*DEGtoRAD)*cos(vza_p*DEGtoRAD) + &
                   sin(sza_p*DEGtoRAD)*sin(vza_p*DEGtoRAD)*cos(phi*DEGtoRAD))
         psi =psi*RADtoDEG

        ! sunglint possible
         ! if (iand( ishft(geoflg(ix), -4), 1) == 1) then ! switch to use ibits
         !  errflg = 1
         ! endif

        if (psi <= 20) then 
           errflg=1
        endif
         
        ! solar zenith angle > 88
        if (sza_p > 88.) then
           qaflags=ibset(qaflags,8)
           skipit=.true.
        endif
        
        IF (skipit) THEN
           qaflags = errflg+qaflags
           ! RAM - ignoring xnvalm as it gives junk values.
           CALL L2fillDataPix(iLine+1, iX, algflg, QAflags, phi)
!           CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
           ierr = OMI_SMF_setmsg( OZT_E_FAILURE, "skipped sample.", "main", zero)
           CYCLE
        ENDIF

        ! get measured n-values for UV-2 and VIS
        ! move all of this to top of measurement section - repetitive here

        ! get instrument configuration ID (ICID)
        ! status = L1Bri_getInstConfigId(L1B_blk, iLine, instId_rad)
        status = L1Br_getDATA(L1B_blk, iLine+1, &
                  InstrumentConfigurationId_k=instId_rad, &
                  MeasurementQualityFlags_k = l1b_meas_qual)

        ! skip unwanted ICID settings
        ! TODO simplify this as an or-ed list ( put into a configuration file )
        !nc4 -TODO-RAM Put back instreument id check
        ! skipit= .FALSE.
        ! skipit = (instid_rad /= 0 .AND. instid_rad /= 1 .AND. &
        !      instid_rad /= 2 .AND. instid_rad /= 7) ! .OR. &

        IF (skipit) THEN
           qaflags = errflg+qaflags

           ! RAM - ignoring xnvalm as it gives junk values.
           CALL L2fillDataPix(iLine+1, iX, algflg, QAflags, phi)
!           CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
           CYCLE
           IF (status /= OZT_S_SUCCESS) THEN
              WRITE( msg,'(A,2(A,I4),A)') "Instrument configuration ID  rejected at Line=",iLine," iX=", iX
              ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
           ENDIF
        ENDIF

        ! compute n-values for uv2 and vis

        ALLOCATE(channel(nwl_com))
        channel = (/(i,i=1,nwl_com)/)        

        IF (COUNT(sensor_id == 2) > 0) THEN 
           ALLOCATE(sensor_2_mask(nwl_com))
           sensor_2_mask = sensor_id == 2
           ilo_uv2 = MINVAL(PACK(channel, sensor_2_mask))
           ihi_uv2 = MAXVAL(PACK(channel, sensor_2_mask))
           wl_uv2 => wl_com(ilo_uv2:ihi_uv2)
           xnvalm_uv2 => xnvalm(ilo_uv2:ihi_uv2)
!           status = O3T_nvalm(irrWavelength(:,iX), irradiance(:,iX), &
!                radWavelength(:,iX), radiance(:,iX), wl_uv2, xnvalm_uv2)
           do i=1,size(wl_uv2)

              call interp_spec(radWavelength(:,iX), radiance(:,iX), radQAflags(:,iX), radiance2(:,iX))

              call smooth_spec(radWavelength(:,iX), radiance2(:,iX), radQAflags(:,iX), wl_uv2(i), band_uv2_rad, &
                   rad_diag1, rad_diag2, rad_diag3)

              call interp_spec(irrWavelength(:,iX), irradiance(:,iX), irrQAflags(:,iX), irradiance2(:,iX))

              call smooth_spec(irrWavelength(:,iX), irradiance2(:,iX), irrQAflags(:,iX), wl_uv2(i), band_uv2_irr, &
                   irr_diag1, irr_diag2, irr_diag3)

              ! this is confusing -- explain what is going on
              band_diag(i) =  nint(rad_diag2/rad_diag1*100, 4) +      256 * nint(rad_diag3/rad_diag1*100, 4) + &
                      65536 * nint(irr_diag2/irr_diag1*100, 4) + 16777216 * nint(irr_diag3/irr_diag1*100, 4)

              ! write(32,*) 'RU', iLine, iX, wl_uv2, rad_diag1, rad_diag2, rad_diag3
              ! write(33,*) 'IU', iLine, iX, wl_uv2, irr_diag1, irr_diag2, irr_diag3
              ! write(34,*) 'DU', iLine, iX, band_diag(i)

              ! check div by zero, n-value negative, etc. [ see O3T_nvalm ]
              xnvalm_uv2(i) = -alog10(band_uv2_rad/band_uv2_irr)
              ! PRINT *,iLine,iX,i,band_uv2_rad,band_uv2_irr, &
              !   sum(radiance(:,iX)), sum(radQAflags(:,iX)), sum(radiance2(:,iX))
           enddo
           NULLIFY(wl_uv2, xnvalm_uv2)
           DEALLOCATE(sensor_2_mask)
        ENDIF

        IF (COUNT(sensor_id == 3) > 0) THEN
           ALLOCATE(sensor_3_mask(nwl_com))
           sensor_3_mask = sensor_id == 3
           ilo_vis = MINVAL(PACK(channel, sensor_3_mask))
           ihi_vis = MAXVAL(PACK(channel, sensor_3_mask))
           wl_vis => wl_com(ilo_vis:ihi_vis)
           xnvalm_vis => xnvalm(ilo_vis:ihi_vis)
           do i=1,size(wl_vis)

              call interp_spec(radWavelength_VIS(:,iX), radiance_VIS(:,iX), radQAflags_VIS(:,iX), radiance2_VIS(:,iX))

              call smooth_spec(radWavelength_VIS(:,iX),  radiance_VIS(:,iX), radQAflags_VIS(:,iX), wl_vis(i), band_vis_rad, &
                   rad_diag1_VIS, rad_diag2_VIS, rad_diag3_VIS)

              call interp_spec(irrWavelength_VIS(:,iX), irradiance_VIS(:,iX), irrQAflags_VIS(:,iX), irradiance2_VIS(:,iX))

              call smooth_spec(irrWavelength_VIS(:,iX), irradiance_VIS(:,iX), irrQAflags_VIS(:,iX), wl_vis(i), band_vis_irr, &
                   irr_diag1_VIS, irr_diag2_VIS, irr_diag3_VIS)

              band_diag(10+i) =  int(rad_diag2_VIS/rad_diag1_VIS*100, 4) +      256 * int(rad_diag3_VIS/rad_diag1_VIS*100, 4) + &
                         65536 * int(irr_diag2_VIS/irr_diag1_VIS*100, 4) + 16777216 * int(irr_diag3_VIS/irr_diag1_VIS*100, 4)

              ! write(42,*) 'RV', iLine, iX, wl_vis, rad_diag1_VIS, rad_diag2_VIS, rad_diag3_VIS
              ! write(43,*) 'IV', iLine, iX, wl_vis, irr_diag1_VIS, irr_diag2_VIS, irr_diag3_VIS
              ! write(44,*) 'DV', iLine, iX, band_diag(10+i)

              ! check div by zero, n-value negative, etc. [ see O3T_nvalm ]
              xnvalm_vis(i) = -alog10(band_vis_rad/band_vis_irr)
           enddo
           NULLIFY(wl_vis, xnvalm_vis)
           DEALLOCATE(sensor_3_mask)
        ENDIF
        DEALLOCATE(channel)

        ! todo: check for bad radiances
        ! if (bad_radiance) then                                                           
        !    errflg = 6
        !    skipit = .true.
        ! endif

        IF (skipit .OR. status /= OMI_S_SUCCESS ) THEN
           ! skipit to cycle to next pixel if calculation of N-value fails and values not stored
           ! thought: report sample vs. algorithm issues separately
           qaflags = errflg+qaflags
           CALL L2fillDataPix(iLine+1, iX, algflg, QAflags, phi, xnvalm)
!           CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
           CYCLE
           IF (status /= OZT_S_SUCCESS) THEN
              WRITE( msg,'(A,2(A,I4),A)') "Calculate xnalm failed at Line=",iLine," iX=", iX
              ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
           ENDIF
        ENDIF

        ! apply soft-calibration to measurements
        xnvalm = xnvalm + swpcr(:, iX)/100.

        pixID  = iX + nXtrack_rad*iLine
        pixGEO = geolocation_set( sza_p, vza_p, phi, pt, pc, lat, lon, pixID)
        pixGEO_VIS = geolocation_set( sza_p_VIS, vza_p_VIS, phi_VIS, pt, pc, lat_VIS, lon_VIS, pixID)

        ! compute denominators of largrangian interpolation coefficients
        ! todo: set quality bit for this
        status = O3T_lpoly_coef( coefs, LUT_cden_blk, pixGEO)

        IF (status /= OZT_S_SUCCESS) THEN
           WRITE( msg,'(A)') "Calculation of polynomial interpolation coefficients failed, " // &
                "Skip to next pixel"
           ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)

           qaflags = errflg+qaflags

           CALL L2fillDataPix(iLine+1, iX, algflg, QAflags, phi, xnvalm)
!           CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
           CYCLE
        ENDIF

        ! set surface category: water or land
        pixSURF%landsea_mask = IBITS(geoflg(iX), 0, 4)
        IF(pixSURF%landsea_mask /= 1 .AND. pixSURF%landsea_mask /= 3) THEN
           pixSURF%surface_category = 0 ! TODO REMOVE
           surface_category = 0
        ELSE
           pixSURF%surface_category = 1 ! TODO REMOVE
           surface_category = 1
        ENDIF

        ! solar eclipse possibility flag - set bit 5
        if (IBITS(geoflg(iX),5,1) == 1) then
           qaflags=ibset(qaflags,5)
        endif

        ! geolocation error flag - set bit 7 (in L1B this is bit 6)
        if (IBITS(geoflg(iX),6,1) == 1) then
           qaflags=ibset(qaflags,7)
        endif

        ! set initial guess
        IF(iX == 1) THEN
           IF(ABS(latitude(iX)) <= 45.0) THEN
              guesoz = 260
           ELSE IF(ABS(latitude(iX)) <= 75.0) THEN
              guesoz = 340
           ELSE
              guesoz = 360
           ENDIF
        ELSE
           guesoz = stp1oz
        ENDIF

         ! compute step 1 ozone

         ! no raman correction for OMI
         ramcor(:) = 0.0

         ! add extrapol error to step 1

         ! set 1 call
         status = O3T_step1(iwl_oz1, iwl_oz2, iwl_ref1, iwl_ref2, iwl_ref3, iwl_res, xnvalm, ramcor, aercor_fac, pixGEO, psi, & 
              isnow, jday, coefs, iplow, ref, guesoz, stp1oz, &   
              fgprf, aprftm, res_stp1, dndomega_t, dndr, dndt, domega_temp, error%maxitr, &
              algflg, stp1oz_valid, doz_limit, res340_limit, error%res340, &
              res331_limit, error%res331, skipit)

         ! set flags after step 1

         ! res340 error
         if (error%res340) then
            errflg = 2 
         endif

         ! res331 error
         if (error%res331) then                                                           
            errflg = 3
         endif

         ! maxitr error
         if (error%maxitr) then                                                           
            errflg = 4
            skipit = .true.
         endif
         
         ! fatal residual error
         if (any(res_stp1 > 16.)) then                                                           
            errflg = 6
            skipit = .true.
         endif
         
         ! error handling for step 1
         ! why are we checking both skipit and status?
        IF (skipit .OR. status /= OZT_S_SUCCESS) THEN
           WRITE( msg,'(A,I3,I5, 2(A9,F9.3), 3(A9,L1))') &
                "O3T_step1 skipped at (iX, iL)=", iX, iLine, &
                ",stp1oz=", stp1oz, ", ref1=", ref(iwl_ref1), ",maxitr=", maxitr, &   
                ",stp1oz=", stp1oz_valid, ",skipit=", skipit
           ierr = OMI_SMF_setmsg( OZT_W_GENERAL, msg, "main", one)
           ! set flags, metadata, and fill before cycling to next sample
!           IF (.NOT. stp1oz_valid) THEN
!              L2_parameters(1)%NumberOfStep1InvalidSamples = &
!              L2_parameters(1)%NumberOfStep1InvalidSamples + 1
!           ENDIF
           qaflags = errflg+qaflags
           CALL L2fillDataPix( iLine+1, iX, algflg, QAflags, phi, xnvalm)
!           CALL O3T_QAcounter( algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
           CYCLE
        ENDIF

        ! step 2 call
         status = O3T_step2( iwl_oe, jday, pixGEO, ref, coefs, iplow, &
              stp1oz, fgprf, res_stp1, noise, xa, sx, dndx, x2, xtoz, xerr, dfs, &
              wc2, Gc, Sret, res_stp2, opt_est_valid, skipit) 

         ! matrix inversion error
         ! if (inverr) then
         !   errflg = 5
         ! endif

         if (.not.opt_est_valid) then ! move to step2 status
            errflg = 6
            skipit = .true.
         endif
         
        IF (skipit .OR. status /= OZT_S_SUCCESS) THEN
           WRITE( msg,'(A,I3,I5,2(A9,G9.3),A8,L1)') &
                     "O3T_step2 skipped at (iX, iL)=(", iX, iLine, &
                     "),stp1oz=", stp1oz, ",stp2oz=", stp2oz,",skipit=", skipit
           ierr = OMI_SMF_setmsg( OZT_W_GENERAL, msg, "main", one)

           IF (.NOT. stp2oz_valid) THEN
              L2_parameters(1)%NumberOfStep2InvalidSamples = &
              L2_parameters(1)%NumberOfStep2InvalidSamples + 1
           ENDIF

           qaflags = errflg+qaflags

           ! TODO add nvalues to output in fill
            CALL L2fillDataPix( iLine+1, iX, algflg, QAflags, phi, xnvalm)
!            CALL O3T_QAcounter( algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
            CYCLE
         ENDIF

! @ TURNING OFF STEP 3
!         ! compute step 3 ozone
!         status = O3T_step3( xtoz, x2, wc2, xa, sx, ref(iwl_ref1), pixGEO, isnow, &
!              stp3oz, x3, wc3, fc3, pse, cldoz ) 

!         ! TODO complete step 3 flagging and checking -- Tibet error
!         ! what to do with status here? nothing set as of yet! review step 3 for issues

         IF (status /= OZT_S_SUCCESS) THEN

            errflg = 6 ! fatal -- catch via exit status from O3T_step3
            skipit = .true. ! for good measure
            qaflags = errflg+qaflags

            CALL L2fillDataPix( iLine+1, iX, algflg, QAflags, phi, xnvalm)
!            CALL O3T_QAcounter( algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
            CYCLE
         ENDIF

!         CALL O3T_QAcounter(algflg, QAflags, L2_parameters(1)%QualityFlagsCounters)
!         IF (IBITS(QAflags, 0, 4) == 0) THEN
!            L2_parameters(1)%NumberOfGoodOutputSamples = &
!            L2_parameters(1)%NumberOfGoodOutputSamples + 1
!         ELSE IF(IBITS(QAflags, 0, 4) == 1) THEN
!            L2_parameters(1)%NumberOfGlintCorrectedSamples = &
!            L2_parameters(1)%NumberOfGlintCorrectedSamples + 1
!         ENDIF
        
         ! store data in output block
         
         qaflags = errflg+qaflags
         ! PRINT *,iLine,iX, iT,ref(iwl_ref1), ref(iwl_ref2),stp1oz,stp3oz
      
          ! Store in Level-2 arrays
          CALL L2setDataPix( iLine+1, iX, nwl_com, NLYR, nWavelRet, algflg, QAflags, &
            ref(iwl_ref1), ref(iwl_ref2), ref(iwl_ref3), fc3, stp1oz, domega_temp, xtoz, pse, &
            stp3oz, dfs, aprftm, dndx, x2, wc2, xa, sx, x3, wc3, &
            xerr, cldoz, Gc, Sret, noise, phi, xnvalm)
         ! no QA counter?
   
      ENDDO ! end sample loop
      
      !%%% end of line checks %%%

      ! get radiance measurement quality flags
      ! status = L1Bri_getMeasurementQF( L1B_blk, iLine, mqa_rad   )

! ** RESTORE THIS **
! will need to check how iwlArray was defined
!
!      ! check for missing radiance values
!      IF(nXtrack_rad == O3T_nMissing(iwlArray, radQAflags)) THEN
!         radLmissing = .TRUE.
!      ELSE
!         radLmissing = .FALSE.
!      ENDIF
      radLmissing = .FALSE.
! ** RESTORE THIS **
     
      ! check for OMI ID mismatch
      instIDmismatch = O3T_instIDsCheck(TRIM(ADJUSTL(L1BUV2coreArch%ShortName)), &
           mqa_rad, instID_rad, instID_irr) 

      ! set measurement quality flags for level 2 output
      mqaL2 = O3T_setMqaL2(mqa_rad, radLMissing, instIDmismatch, bit7Q, L2_parameters(1)) 
!nc4      CALL O3T_L2setMqaLine(iT, mqaL2, datablk)

      ! write output block
    ENDDO  ! end along track loop

    ! write the last blocks of data (if any)

    ! temp_filename = Replace_Text(OMTO3_fn,'.he5','.nc')
      ! nc4 -RAM writing temporary netCDF4 file.
        call OMTO3_file_init(TRIM(OMTO3_fn_nc), &
          UV_filename, nTimes_rad, nXTrack_rad, &
          nWavel=12, nLayers=11, wl_com=wl_com)
        call OMTO3_create_variables(nTimes_rad, nXTrack_rad, nLayers=11)
        call write_values(wl_com, iwl_oe)

    ! writing finished, close output file

    ! free allocated memory
    CALL O3T_freeIRR
    CALL O3T_freeIRR_VIS
    CALL O3T_freeRAD
    CALL O3T_freeRAD_VIS
    CALL O3T_freeCLD
    CALL O3T_freeL2out

    ! free coefficient denominator data block
    status = O3T_lpolycden_dispose( LUT_cden_blk)

    ! free dndx tables
    status = O3T_dndx_dispose()
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "O3T_dndx_dispose failed. exiting."
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF

    ! clean up memory allocated for radiance tables
    status = O3T_nval_dispose()
    IF (status /= OZT_S_SUCCESS) THEN
       WRITE( msg,'(A)') "O3T_nval_dispose failed. exiting."
       ierr = OMI_SMF_setmsg( OZT_E_FAILURE, msg, "main", zero)
       CALL EXIT(1)
    ENDIF
    
    ! free memory used for anomaly flag
    if (allocated(anomflg_3)) DEALLOCATE(anomflg_3)

    ! free memory for geos5 met data
    ! call close_merra
    call close_geosmet

    ! set output metadata
    L2_parameters(1)%Name = "Total Column Ozone"
    L2_parameters(1)%SolarProductMissing = 0
    L2_parameters(1)%BackupSolarProductUsed = 0 
    mcfLUN = OMTO3_MCF_LUN

!! TODO RESTORE METADATA
!!
    ! !! Read the input file names from the PCF file and                                              
    ! !! use these files as the input pointer for the L2 output                                       
    ! IF( TRIM(cloud_pressure_source) == '"Climatology"' ) THEN
    !    LUNinputPointer(1:9)= (/ L1B_UV_FILE_LUN, &
    !                              L1B_IRR_FILE_LUN, &
    !                              GEOSMET_LUN,     &
    !                              OMTO3_NVAL_LUN,  &
    !                              OMTO3_DNDX_LUN,  &
    !                              TM_CLIM_LUN,     &
    !                              TERRAINPRES_LUN, &
    !                              CLOUDPRES_LUN,   &
    !                              NVCORR_LUN /)

    !   status = L2_setCoreArchMetaData( OMTO3_L2_LUN , L1BUV2coreArch, &
    !                              L1BIRRcoreArch, LUNinputPointer(1:9), &
    !                              mcfLUN, L2_parameters )
    ! ELSE
    !    LUNinputPointer(1:10)= (/ L1B_UV_FILE_LUN, &
    !                              L1B_IRR_FILE_LUN, &
    !                              OMCLDRR_L2_LUN,  &
    !                              GEOSMET_LUN,     &
    !                              OMTO3_NVAL_LUN,  &
    !                              OMTO3_DNDX_LUN,  &
    !                              TM_CLIM_LUN,     &
    !                              TERRAINPRES_LUN, &
    !                              CLOUDPRES_LUN,   &
    !                              NVCORR_LUN /)

    !    status = L2_setCoreArchMetaData( OMTO3_L2_LUN , L1BUV2coreArch, &
    !                             L1BIRRcoreArch, LUNinputPointer(1:10), &
    !                             mcfLUN, L2_parameters )
    ! ENDIF
!! END TODO RESTORE
    
    ! write global attributes - MB 12/07/23
    call write_global_attributes(OMTO3_fn_nc, UV_filename, VIS_filename, IRR_filename, &
            southernmost, northernmost, westernmost, easternmost, seconds, nTimes_rad, &
            temperature_source, snowice_source, aerosol_limit, residual_limit, status)

    write(6, *) 'wrote attributes: ', status

    ! write swath attribute

    ! copy some L1B data fileds to L2 output for OMI only
  
    !! copy some HE4 swath attributes and write to HE5 global attribute for TOMS





    !! write the final message and exit
    ierr = OMI_SMF_setmsg(OZT_S_SUCCESS, "program finished normally.", "main", zero)
    CALL EXIT(0)

END PROGRAM main
