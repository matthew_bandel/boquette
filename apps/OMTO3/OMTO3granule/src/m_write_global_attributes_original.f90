module m_write_global_attributes

    ! declare public
    public

    ! declare contains
    contains

    ! write the OMTO3 global attributes
    subroutine write_global_attributes(destination, ultraviolet, irradiance, &
            southernmost, northernmost, westernmost, easternmost, track, &
            snow, cloud, status)

        ! declare modules
        use netcdf
        use PGS_PC_class
        use HDF5
        use H5A
        use m_write_string_attribute
        use m_write_double_attribute
        use m_write_integer_attribute
        use PGS_TD_class

        ! declare implicits
        implicit none

        ! declare arguments
        character (len=200), intent(in) :: destination, ultraviolet, irradiance
        character (len=*), intent(in) :: snow, cloud
        real(kind=8), intent(in) :: southernmost, northernmost, westernmost, easternmost
        integer(kind=4), intent(in) :: track
        integer(kind=4), intent(out) :: status

        ! declare variables
        integer (KIND=4), external :: day_of_year
        integer(kind=4) :: file, fileii, metadata, metadataii, inventory
        ! adding groups - MB 04/01/2024
        integer(kind=4) :: root, additional, group
        integer(kind=4) :: year, month, day, julian, orbit
        real(kind=8) :: equator
        character (len=2048) :: information, reference
        character(100) :: production, history, version

        ! current datetime parameters
        character(8) :: date
        character(10) :: time
        character(19) :: zone
        integer(kind=4) :: member
        integer(kind=4) :: days
        real(kind=8) :: seconds

        ! open library and file
        call h5open_f(status)
        call h5fopen_f(trim(destination), H5F_ACC_RDWR_F, file, status)

        ! get the additional attributes group
        call h5gopen_f(file, "HDFEOS", root, status)
        call h5gopen_f(root, "ADDITIONAL", additional, status)
        call h5gopen_f(additional, "FILE_ATTRIBUTES", group, status)

        ! Get the current date and time
        call date_and_time(date, time, zone)

        ! add production time
        production = date(1:4)//"-"//date(5:6)//"-"//date(7:8)//"T"//time(1:2)//":"//time(3:4)//":"//time(5:6)
        call write_string_attribute(group, "ProductionDateTime", trim(production), status)


        ! copy toolkit version from control
        status = PGS_PC_GetConfigData(10220, information)
!        status = GetConfigString("E", "Runtime Parameters Toolkit version string", information)
        call write_string_attribute(group, "Toolkit version string", trim(information), status)


       ! copy verbosity threshold
        status = PGS_PC_GetConfigData(200100, information)
!        status = GetConfigString("E", "Runtime Parameters SMF Verbosity Threshold", information)
        call write_string_attribute(group, "SMF Verbosity Threshold", trim(information), status)


        ! copy PGE version
        status = PGS_PC_GetConfigData(200105, version)
!        status = GetConfigString("E", "Runtime Parameters PGEVERSION", information)
        call write_string_attribute(group, "PGEVersion", trim(version), status)


        ! copy processing center
        status = PGS_PC_GetConfigData(200110, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessingCenter", information)
        call write_string_attribute(group, "ProcessingCenter", trim(information), status)


        ! copy processing host
        status = PGS_PC_GetConfigData(200115, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessingHost", information)
        call write_string_attribute(group, "ProcessingHost", trim(information), status)


        ! copy reprocessing actual
        status = PGS_PC_GetConfigData(200135, information)
!        status = GetConfigString("E", "Runtime Parameters REPROCESSINGACTUAL", information)
        call write_string_attribute(group, "ReprocessingActual", trim(information), status)


        ! copy processing level
        status = PGS_PC_GetConfigData(200170, information)
!        status = GetConfigString("E", "Runtime Parameters ProcessLevel", information)
        call write_string_attribute(group, "ProcessingLevel", trim(information), status)


        ! copy instrument name into instrument name and short name
        status = PGS_PC_GetConfigData(200175, information)
!        status = GetConfigString("E", "Runtime Parameters InstrumentName", information)
        call write_string_attribute(group, "InstrumentName", trim(information), status)
        call write_string_attribute(group, "InstrumentShortName", trim(information), status)


        ! copy operation mode
        status = PGS_PC_GetConfigData(200180, information)
!        status = GetConfigString("E", "Runtime Parameters OPERATIONMODE", information)
        call write_string_attribute(group, "OperationMode", trim(information), status)


        ! copy author affiliation
        status = PGS_PC_GetConfigData(200185, information)
!        status = GetConfigString("E", "Runtime Parameters AuthorAffiliation", information)
        call write_string_attribute(group, "AuthorAffiliation", trim(information), status)


        ! copy author name
        status = PGS_PC_GetConfigData(200190, information)
!        status = GetConfigString("E", "Runtime Parameters AuthorName", information)
        call write_string_attribute(group, "AuthorName", trim(information), status)


        ! copy local version id
        status = PGS_PC_GetConfigData(200195, information)
!        status = GetConfigString("E", "Runtime Parameters LOCALVERSIONID", information)
        call write_string_attribute(group, "LocalVersionID", trim(information), status)


        ! copy orbit number
        status = PGS_PC_GetConfigData(200200, information)
!        status = GetConfigString("E", "Runtime Parameters OrbitNumber", information)
        read(information, *) orbit
        call write_integer_attribute(group, "OrbitNumber", orbit, status)
        call write_integer_attribute(group, "orbit", orbit, status)


        ! copy swath name
        status = PGS_PC_GetConfigData(200210, information)
!        status = GetConfigString("E", "Runtime Parameters SwathName", information)
        call write_string_attribute(group, "SwathName", trim(information), status)


        ! copy cloud pressure source, passed in from main
        call write_string_attribute(group, "CloudPressureSource", trim(cloud), status)


        ! copy terrain pressure source
        status = PGS_PC_GetConfigData(400091, information)
!        status = GetConfigString("E", "Runtime Parameters TERRAINPRESSURESOURCE", information)
        call write_string_attribute(group, "TerrainPressureSource", trim(information), status)


        ! copy temperature source
        status = PGS_PC_GetConfigData(400092, information)
!        status = GetConfigString("E", "Runtime Parameters TEMPERATURESOURCE", information)
        call write_string_attribute(group, "TemperatureSource", trim(information), status)


        ! copy snow ice source passed in from main
        call write_string_attribute(group, "SnowIceSource", trim(snow), status)


        ! copy apriori source
        status = PGS_PC_GetConfigData(400094, information)
!        status = GetConfigString("E", "Runtime Parameters APRIORIOZONEPROFILESOURCE", information)
        call write_string_attribute(group, "AprioriOzoneProfileSource", trim(information), status)


        ! copy version id
        status = PGS_PC_GetConfigData(123456, information)
!        status = GetConfigString("E", "Runtime Parameters VERSIONID", information)
        call write_string_attribute(group, "VersionID", trim(information), status)

        ! add history
        history = "OZONE PEATE "//trim(production)//", APPVersion="//trim(version)
        call write_string_attribute(group, "history", trim(history), status)

        ! open ultraviolet file
        status = nf90_open(trim(ultraviolet), nf90_nowrite, fileii)

        ! copy attriubtes from L1B
        status = nf90_get_att(fileii, NF90_GLOBAL, "Conventions", information)
        call write_string_attribute(group, "Conventions", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "institution", information)
        call write_string_attribute(group, "Institutioin", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "processor_version", information)
        call write_string_attribute(group, "processor_version", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "summary", information)
        call write_string_attribute(group, "summary", trim(information), status)
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_reference", reference)
        call write_string_attribute(group, "time_reference", trim(reference), status)

        ! get day of year, using time_reference field
        read(reference(1:4), *) year
        read(reference(6:7), *) month
        read(reference(9:10), *) day
        julian = day_of_year(year, month, day)

!        ! calculate number of days since 1993 for TAI1993 time
!        days = 0
!        do member = 1993, year - 1
!            days = days + 365
!
!            ! account for leap year every 4th year
!            if (mod(member, 4).eq.0) then
!                days = days + 1
!            endif
!        enddo
!
!        ! add up to ( but not including ) the julian day
!        days = days + (julian - 1)
!
!        ! compute seconds, using 86400 seconds / day
!        seconds = days * 86400
!
!        ! add 5 seconds ( of unsure origin ) to keep in sync with Col3
!        seconds = seconds + 5
!
!        ! add TAI 1993 time
!        call write_double_attribute(group, "TAI93At0zOfGranule", seconds, status)

        ! use PGS function instead
        status = PGS_TD_UTCtoTAI(reference, seconds)

        ! set granule attributes
        call write_integer_attribute(group, "GranuleYear", year, status)
        call write_integer_attribute(group, "GranuleMonth", month, status)
        call write_integer_attribute(group, "GranuleDay", day, status)
        call write_integer_attribute(group, "GranuleDayOfYear", julian, status)

        ! add time coverage beginning parameters
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_start", information)
        call write_string_attribute(group, "time_coverage_start", trim(information), status)

        ! add time coverage end based parameeters
        status = nf90_get_att(fileii, NF90_GLOBAL, "time_coverage_end", information)
        call write_string_attribute(group, "time_coverage_end", trim(information), status)

        ! get equator crossing time
        status = nf90_inq_grp_ncid(fileii, "METADATA", metadata)
        status = nf90_inq_grp_ncid(metadata, "ECS_METADATA", metadataii)
        status = nf90_inq_grp_ncid(metadataii, "Inventory_Metadata", inventory)
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingDateTime", information)
        call write_string_attribute(group, "EquatorCrossingDateTime", trim(information), status)
        call write_string_attribute(group, "EquatorCrossingDate", trim(information(1:10)), status)
        call write_string_attribute(group, "EquatorCrossingTime", trim(information(12:20)), status)

        ! add time coverage beginning parameters, adding precision to time for orbital period calculations
        status = nf90_get_att(inventory, NF90_GLOBAL, "RangeBeginningDateTime", information)
        call write_string_attribute(group, "RangeBeginningDate", trim(information(1:10)), status)
        call write_string_attribute(group, "RangeBeginningTime", trim(information(12:19))//".000000", status)

        ! add time coverage end based parameeters, adding precision to time for orbital period calculations
        status = nf90_get_att(inventory, NF90_GLOBAL, "RangeEndingDateTime", information)
        call write_string_attribute(group, "RangeEndingDate", trim(information(1:10)), status)
        call write_string_attribute(group, "RangeEndingTime", trim(information(12:19))//".000000", status)

        ! get equator crossing longitude
        status = nf90_get_att(inventory, NF90_GLOBAL, "EquatorCrossingLongitude", information)
        read(information, *) equator
        call write_double_attribute(group, "EquatorCrossingLongitude", equator, status)

        ! add input pointer
        information = trim(ultraviolet)//","//trim(irradiance)
        call write_string_attribute(group, "InputPointer", trim(information), status)

        ! add coordinates
        call write_double_attribute(group, "NorthBoundingCoordinate", northernmost, status)
        call write_double_attribute(group, "SouthBoundingCoordinate", southernmost, status)
        call write_double_attribute(group, "WestBoundingCoordinate", westernmost, status)
        call write_double_attribute(group, "EastBoundingCoordinate", easternmost, status)

        ! add num times
        call write_integer_attribute(group, "NumTimes", track, status)

        ! add destination name to granule ids
        call write_string_attribute(group, "GranuleID", trim(destination), status)
        call write_string_attribute(group, "LocalGranuleID", trim(destination), status)

        ! add other hard coded attributes
        call write_string_attribute(group, "comment", " ", status)
        call write_string_attribute(group, "DataSetQuality", "Excellent data quality", status)
        call write_string_attribute(group, "DayNightFlag", "Day", status)
        call write_string_attribute(group, "Format", "HDF5", status)
        call write_string_attribute(group, "FOVResolution", "13kmx24km", status)
        call write_string_attribute(group, "IdentifierProductDOI", " ", status)
        call write_string_attribute(group, "IdentifierProductDOIAuthority", "https://doi.org/", status)
        call write_string_attribute(group, "institution", "NASA/GSFC", status)
        call write_string_attribute(group, "LocalityValue", "Global", status)
        call write_string_attribute(group, "LongName", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km", status)
        call write_string_attribute(group, "ParameterName", "OMI Ground Pixels UV1, UV2 and VIS", status)
        call write_string_attribute(group, "PGEName", "OMTO3", status)
        call write_string_attribute(group, "PlatformShortName", "Aura", status)
        call write_string_attribute(group, "ProductType", "L2 Swath", status)
        call write_string_attribute(group, "references", " ", status)
        call write_string_attribute(group, "source", "Aura OMI", status)
        call write_string_attribute(group, "title", "OMI/Aura Ozone (O3) Total Column 1-Orbit L2 Swath 13x24km", status)
        call write_string_attribute(group, "SensorShortName", "CCD Ultraviolet, CCD Visible", status)
        call write_string_attribute(group, "ShortName", "OMTO3", status)

        ! close file and library
        call h5gclose_f(group, status)
        call h5gclose_f(additional, status)
        call h5gclose_f(root, status)
        call h5fclose_f(file, status)
        call h5close_f(status)

        ! close L1B file
        status = nf90_close(fileii)

        return

    end subroutine write_global_attributes
end module m_write_global_attributes
