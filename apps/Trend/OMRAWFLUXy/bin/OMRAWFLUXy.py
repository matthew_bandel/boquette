#!/usr/bin/env python3

# OMRAWFLUXy to stack together single orbit OMRAWFLUX files into yearly accumulation

# import system
import sys
import subprocess

# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # pad arguments with blanks as defaults
    arguments += [''] * 6
    arguments = arguments[:6]
    arguments[1] = 'y'

    # construct call to juggernaut
    call = ['python3', 'centipedes.py'] + arguments

    # make call with subprocess
    subprocess.call(call)

