#!/usr/bin/env python3

# centipedes.py for the Millipede class to reduce OMI trending data

# import local classes
from hydras import Hydra
from features import Feature

# import system
import sys

# import regex
import re

# import numpy functions
import numpy


# class Centipede to do aggregation of OMI RAWFLUX files
class Centipede(Hydra):
    """Centipede class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, controller='', code='', sink='', source='', start='', finish='', clock=True):
        """Initialize a Centipede instance.

        Arguments:
            controller: str, control file name
            code: str, letter name of concatenation ('y', 'x')
            sink: str, self.sink
            singlet: str, esdt name of single orbit
            plural: str, esdt name of plural
            start: str, begining date in 'yyyy-mm-dd'
            finish: str, ending date in 'yyyy-mm-dd'
            clock: boolean, allow timing output?

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, '')

        # codify app name
        self.code = code
        self.singlet = ''
        self.plural = ''
        self._codify()

        # set control file name
        self.sink = sink
        self.source = source
        self.controller = controller
        self._assume(sink)

        # get start and end dates
        self.start = int(start or 0)
        self.finish = int(finish or 0)

        # set timing switch
        self.clock = clock

        # set diffuser info
        self.diffusers = {8: 'vol', 18: 'reg', 30: 'bck'}

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Centipede instance at: {} >'.format(self.sink)

        return representation

    def _assume(self, sink):
        """Assume control from a default control file

        Arguments:
            sink: str, sink directory

        Returns:
            None

        Populates:
            self.controller
        """

        # if there is no control file given
        if not self.controller:

            # create control file name from sink
            self.controller = '{}/control-file.txt'.format(sink)

        # print controller
        self._print('assuming controller: {}'.format(self.controller))

        return None

    def _codify(self):
        """Create esdt names based on code.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.singlet
            self.plural
        """

        # update code, defaulting to year
        self.code = ('y' + self.code)[-1] or 'y'

        # create codes dictionary mapping plural code to singlet code
        codes = {'y': '', 'x': 'y'}

        # create esdt names
        self.singlet = 'OMRAWFLUX{}'.format(codes[self.code])
        self.plural = 'OMRAWFLUX{}'.format(self.code)

        return None

    def _generate(self):
        """Generate input paths.

        Arguments:
            None

        Returns:
            list of str
        """

        # if there is a source given
        paths = []
        if self.source:

            # if months also given
            if self.start and self.finish:

                # go through start and stop
                for month in range(self.start,  self.finish + 1):

                    # create hydra for all days
                    hydra = Hydra('{}/{}'.format(self.source, str(month).zfill(2)), 1, 31)

                    # add to paths
                    paths += hydra.paths

            # otherwise
            else:

                # get just the source
                hydra = Hydra(self.source)
                paths += hydra.paths

        # # otherwise
        # else:
        #
        #     # get datetimes from start and end times
        #     start = datetime.datetime.strptime(self.start, '%Ym%m%dt%H%M%S')
        #     finish = datetime.datetime.strptime(self.finish, '%Ym%m%dt%H%M%S')
        #
        #     # collect dates
        #     while start <= finish:
        #
        #         # create folder for date
        #         formats = [self.archive, singlet, start.year, str(start.month).zfill(2), str(start.day).zfill(2)]
        #         folder = '/tis/OMI/{}/{}/{}/{}/{}'.format(*formats)
        #
        #         # add to paths
        #         paths += [path for path in self._see(folder) if '.h5' in path]
        #
        #         # advance date
        #         start = start + datetime.timedelta(days=1)

        # sort paths
        paths.sort()

        return paths

    def _plant(self, path):
        """Build the data tree from file paths.

        Arguments:
            path: str, orbital file path

        Returns:
            dict
        """

        # begin tree
        tree = {}

        # get the data at the path
        data = self._fetch(path)

        # split into orbit, filename
        pieces = path.split('/')[-1].split('.')[0].split('_')
        product = pieces[1]
        orbit = pieces[2]

        # check for orbit in tree
        if orbit not in tree.keys():

            # add orbit
            tree[orbit] = {}

        # insert data into tree
        tree[orbit][product] = data

        return tree

    def _process(self, job):
        """Extract the processing version information from a job description string.

        Arguments:
            job: str, job description

        Returns:
            dict of strings
        """

        # begin processor
        versions = {'processor': '', 'core': ''}

        # search for processor and core
        processor = re.search('OML1BPDS-[0-9].[0-9].[0-9].[0-9]', job.decode('utf8'))
        core = re.search('core_processor_version = [0-9].[0-9].[0-9].[0-9]{5}', job.decode('utf8'))

        # if processor was found
        if processor:

            # add to versions
            versions['processor'] = processor.group()

        # if core was found
        if core:

            # add to versions
            versions['core'] = core.group()

        return versions

    def control(self, trim=False, now=''):
        """Generate a control file.

        Arguments:
            trim: boolean, trim source and sink folders form inputs and outputs?
            now: overrides current now for given now, for keeping production time constant for updates

        Returns:
            None
        """

        # read in from description
        now = now or self._note()
        description = self._acquire('../doc/Description.txt')

        # copy into control
        exclusions = ['Dynamic Input Files', 'Output Files']
        control = {key: value for key, value in description.items() if key not in exclusions}

        # get app name and singlet name
        # name = description['APP Name']
        # singlet = description['Dynamic Input Files'][0]['ESDT']
        name = self.plural
        singlet = self.singlet

        # generate all input paths
        self._print('generating paths...')
        paths = self._generate()
        self._print('generated.')

        # get version of first path for use in output version
        version = self._stage(paths[-1])['version']

        # find appropriate output template for use as destination
        destination = description['Output Files'][0]['Filename']

        # create destination name and add to outputs
        destination = destination.replace('<EndTime!%Ym%m%dt%H%M%S>', self._stage(paths[-1])['date'])
        destination = destination.replace('<EndOrbit>', self._stage(paths[-1])['orbit'])
        destination = destination.replace('<ProductionTime>', now)
        destination = destination.replace('<ECSCollection>', version)

        # if not trimming
        if not trim:

            # add sink
            destination = '{}/{}'.format(self.sink, destination)

        # if trim option selected
        if trim:

            # remove source folder information
            paths = [path.split('/')[-1] for path in paths]

        # begin specifics
        specifics = {'Input Files': {singlet: paths}, 'Output Files': {name: [destination]}}

        # update with specifics
        control.update(specifics)

        # dump into control
        self._dispense(control, self.controller)

        # improve spacing
        self._disperse(self.controller)

        return None

    def crawl(self, temporary='../tmp'):
        """Feed all files through stack.

        Arguments:
            temporary: str, folder name for temporary files

        Returns:
            None
        """

        # open up the control file and retrieve input and output files
        control = self._acquire(self.controller)

        # get input paths and output paths
        paths = [path for members in control['Input Files'].values() for path in members]
        paths.sort(key=lambda path: self._stage(path)['date'])
        destination = [path for members in control['Output Files'].values() for path in members][0]

        # # set start and finish based on paths
        # self.start = self._stage(paths[0])['date']
        # self.finish = self._stage(paths[-1])['date']

        # stack singlets together
        self.devour(paths, destination, temporary)

        return None

    def define(self):
        """Define the earth science data type.

        Arguments:
            None

        Returns:
            None
        """

        # begin definition
        definition = {}

        # set time periods
        periods = {'y': 'Years', 'x': 'Years'}
        times = {'y': 1, 'x': 25}

        # # get singlet esdt name
        # singlets = [singlet.strip() for singlet in input('single orbit ESDTs? ').split(',')]
        # plurals = [plural.strip() for plural in input('plural ESDTs for series? ').split(',')]

        # get esdts
        singlets = [self.singlet]
        plurals = [self.plural]

        # for each singlet, plural pair
        for singlet, plural in zip(singlets, plurals):

            # set sizes
            size = 'Orbit'
            sizes = {'y': 'Year'}
            for letter, chunk in sizes.items():

                # if singlet ends in letter
                if singlet.endswith(letter):

                    # set size
                    size = chunk

            # add esdt name and processing level
            definition['ESDT Name'] = plural
            definition['Processing Level'] ='L3'

            # add Long Name
            name = 'OMI/Aura Timewise Concatenation of Single {} {} Files'.format(size, singlet)
            definition['Long Name'] = name

            # add description
            formats = (size.lower(), singlet)
            text = 'Timewise concatenations of single {} {} files derived from OMI L1B Data '.format(*formats)
            text += 'into time series.'
            definition['Description'] = text

            # add conntacts
            definition['Science Team Contacts'] = ['Matthew Bandel (matthew.bandel@ssaihq.com)']
            definition['Science Team Contacts'] += ['David Haffner (david.haffner@ssaihq.com)']
            definition['Support Contact'] = 'Phillip Durbin (pdurbin@sesda.com)'

            # set storage dictionary for aggregate based on singlet size
            storage = {'Orbit': ('10 MB', '4 GB'), 'Year': ('4 GB', '1 TB')}

            # define storage size
            definition['Minimum Size'] = storage[size][0]
            definition['Maximum Size'] = storage[size][1]

            # define file name pattern
            pattern = '<Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>'
            pattern += '_v<Collection>-<ProductionTime>.<File Format>'
            definition['Filename Pattern'] = pattern

            # add filename elements
            definition['Platform'] = 'Aura'
            definition['Instrument'] = 'OMI'
            definition['File Format'] = 'h5'

            # add other processing elements
            definition['Period'] = '{}={}'.format(periods[plural[-1]], times[plural[-1]])
            definition['Archive Method'] = 'Compress'
            definition['File SubTable Type'] = 'L3'
            definition['Extractor Type'] = 'Filename_L3'
            definition['Metadata Type'] = 'timerange'
            definition['Parser'] = 'TimeAveragedFile'
            definition['keypattern'] = '<StartTime>_<EndTime>'

            # dump into yaml
            destination = '../doc/{}.txt'.format(plural)
            self._dispense(definition, destination)

            # clean up spacing
            self._disperse(destination)

        return None

    def describe(self, version=None):
        """Generate the descriptions.txt file.

        Arguments:
            version: str, version number

        Returns:
            None
        """

        # get singlet esdt name and version numbers
        # singlets = [singlet.strip() for singlet in input('single orbit ESDTs? ').split(',')]
        # plurals = [plural.strip() for plural in input('plural ESDTs for series? ').split(',')]
        version = version or input('version? ')

        # set singlet and plural esdt names
        singlet = self.singlet
        plurals = [self.plural]

        # load in the esdt info
        definitions = [self._acquire('../doc/{}.txt'.format(plural)) for plural in plurals]
        definition = definitions[0]

        # begin description
        description = {}

        # add APP Name
        name = definition['ESDT Name']
        description['APP Name'] = name

        # add APP Type
        description['APP Type'] = 'OMI'

        # add APP Version, from input if not specified
        description['APP Version'] = version

        # add Long Name
        description['Long Name'] = definition['Long Name']

        # add description, apply block form
        description['Description'] = '>\n' + definition['Description']

        # add algorithm lead
        description['Lead Algorithm Scientist'] = definition['Science Team Contacts'][0]

        # add other scientists
        description['Other Algorithm Scientists'] = definition['Science Team Contacts'][1]

        # add software developer
        description['Software Developer'] = definition['Science Team Contacts'][0]

        # add software developer
        description['Support Contact'] = definition['Support Contact']

        # add structure field
        structure = '>\nRun {}.py as a python program, using the control.txt file path as its argument.'.format(name)
        description['Structure'] = structure

        # add operational scenario
        scenario = '>\nRun over all orbits within given start and end times.'
        description['Operational Scenario'] = scenario

        # add the period
        description['Period'] = definition['Period']

        # add the execution
        description['EXE'] = {'Name': '{}.py'.format(name), 'Program': '{}.py'.format(name)}

        # begin description of dynamic input files
        dynamic = 'Dynamic Input Files'

        # set sizes
        size = 'Orbit'
        sizes = {'y': 'Year'}
        for letter, chunk in sizes.items():

            # if singlet ends in letter
            if singlet.endswith(letter):

                # set size
                size = chunk

        # make entry for singlets
        contents = 'OMI Level 1B Single {} {} File'.format(size, singlet)
        description[dynamic] = [{'ESDT': singlet, 'Rule': 'Required', 'Desc': contents}]

        # # make entry for singlets
        # contents = 'OMI Level 1B Single Orbit {} File'.format(singlet)
        # description[dynamic] = [{'ESDT': singlet, 'Rule': 'Required', 'Desc': contents}]

        # start outputs
        output = 'Output Files'

        # begin entry for Filename
        pattern = definition['Filename Pattern']

        # for each filename parameterr
        for parameter in ('Instrument', 'Platform', 'ESDT Name', 'Processing Level', 'File Format'):

            # replace with esdt definition
            pattern = pattern.replace('<{}>'.format(parameter), definition[parameter])

        # add start time format and collection for now
        pattern = pattern.replace('<DataDate>', '<EndTime!%Ym%m%dt%H%M%S>')
        pattern = pattern.replace('<OrbitNumber>', '<EndOrbit>')
        pattern = pattern.replace('<Collection>', '<ECSCollection>')

        # add output description
        contents = 'Concatenation of single {} files'.format(size.lower())
        description[output] = [{'Filename': pattern, 'ESDT': name, 'Rule': 'Required', 'Desc': contents}]

        # make entry for Runtime Parameters
        runtime = 'Runtime Parameters'
        description[runtime] = [{'Param': 'AppShortName', 'Value': name}]
        description[runtime] += [{'Param': 'Instrument', 'Value': definition['Instrument']}]
        description[runtime] += [{'Param': 'Source', 'Value': definition['Instrument']}]
        description[runtime] += [{'Param': 'Platform', 'Value': definition['Platform']}]
        description[runtime] += [{'Param': 'OrbitNumber', 'Value': '"<OrbitNumber>"'}]
        description[runtime] += [{'Param': 'StartTime', 'Value': '"<StartTime>"'}]
        description[runtime] += [{'Param': 'EndTime', 'Value': '"<EndTime>"'}]
        description[runtime] += [{'Param': 'StartOrbit', 'Value': '"<StartOrbit>"'}]
        description[runtime] += [{'Param': 'EndOrbit', 'Value': '"<EndOrbit>"'}]
        description[runtime] += [{'Param': 'ProductionTime', 'Value': '"<ProductionTime>"'}]
        description[runtime] += [{'Param': 'ECSCollection', 'Value': '"<ECSCollection>"'}]
        description[runtime] += [{'Param': 'Maximum Level of Detail', 'Value': '2'}]

        # set production rule variants based on singlet esdts
        keys = {'OMRAWFLUX': 'PGEKeyYearlyParams', 'OMRAWFLUXy': 'PGEKeyYearlyParams'}
        times = {'OMRAWFLUX': 'OrbitTimeRange', 'OMRAWFLUXy': 'L3MatchTimeRange'}

        # make entry for Production Rules
        rules = 'Production Rules'
        description[rules] = [{'Rule': keys[singlet]}]
        description[rules] += [{'Rule': times[singlet], 'ESDT': singlet, 'Min_Files': 2}]

        # add compiler
        description['Compilers'] = [{'CI': 'python', 'Version': '3.6.8'}]

        # add environment
        description['Environment'] = [{'CI': 'env_setup', 'Version': '0.0.9'}]
        description['Environment'] += [{'CI': 'python', 'Version': '3.6.8'}]

        # add operating system
        description['Operating System'] = [{'CI': 'Linux', 'Version': '2.6.9'}]

        # dump into yaml
        destination = '../doc/Description.txt'.format(self.sink)
        self._dispense(description, destination)

        # improve spacing
        self._disperse(destination)

        return None

    def devour(self, paths, destination, temporary='../tmp', size=200000):
        """Fuse all individual trending files into larger aggregates.

        Arguments:
            paths: list of single orbit paths to stack
            destination: str, file path of stacked file
            temporary: str, temporary directory
            size: minimum file size, in bytes

        Returns:
            None
        """

        # check file size for minimum size requirment ( weed out empty files )
        paths = [path for path in paths if self._ask(path)['st_size'] > size]
        paths.sort()

        # if making OMRAWFLUXy
        if 'OMRAWFLUXy' in destination:

            # create reservoirs for each diffuser, as well as its attributes
            reservoir = {diffuser: {} for diffuser in self.diffusers.values()}
            attributes = {diffuser: {} for diffuser in self.diffusers.values()}

            # creae hydra
            hydra = Hydra()

            # print status
            self._print('combining {} orbits...'.format(len(paths)))

            # go through each path
            for path in paths:

                # ingest the path
                hydra.ingest(path)

                # for each diffuser
                for diffuser in self.diffusers.values():

                    # check orbit number for other than 0
                    orbit = hydra.dig('{}_diffuser/orbit_number'.format(diffuser))[0].distil()
                    if orbit > 0:

                        # got through all features
                        features = hydra.dig('{}_diffuser'.format(diffuser))
                        for feature in features:

                            # create entry in reservoir if empty
                            arrays = reservoir[diffuser].setdefault(feature.slash, [])
                            array = feature.distil()
                            arrays.append(array)

                            # add attributes
                            attributes[diffuser][feature.slash] = feature.attributes

            # create new arrays
            features = []
            for diffuser in self.diffusers.values():

                # for each array
                for slash in reservoir[diffuser].keys():

                    # vstack array
                    array = numpy.vstack(reservoir[diffuser][slash]).squeeze()
                    attribute = attributes[diffuser][slash]

                    # add to features
                    feature = Feature(slash.split('/'), array, attributes=attribute)
                    features.append(feature)

            # stash file
            self.stash(features, destination)

        # otherwise
        else:

            # set pruning fields to eliminate duplicate orbit numbers
            prunes = ['vol_diffuser/orbit_number']
            prunes += ['reg_diffuser/orbit_number']
            prunes += ['bck_diffuser/orbit_number']

            # merge files
            self.merge(paths, destination, prunes=prunes)

        return None

    def imitate(self):
        """Imitate the /run folder setup of the APP, assuming currently in /tbl"

        Arguments:
            None

        Returns:
            None
        """

        # create run directory
        self._make('../run')

        # copy all /bin files into /run
        paths = self._see('../bin')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /tbl files into /run
        paths = self._see('../tbl')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        # copy all /utd files into /run
        paths = self._see('../utd')
        destinations = ['../run/{}'.format(path.split('/')[-1]) for path in paths]
        [self._copy(path, destination) for path, destination in zip(paths, destinations)]

        return None

    def mock(self, orbit):
        """Prepare a mock configure file to test in RunApp.

        Arguments:
            date: str, datestring
            destination: file name

        Returns:
            None
        """

        # open up the description file
        description = self._acquire('../doc/Description.txt')
        version = description['APP Version']

        # prepare mock attributes
        mock = {'archiveset': 70004, 'ECSCollection': 4, 'OrbitNumber': orbit}
        mock.update({'PGE': 'OMSTACK', 'PGEVersion': version})
        mock.update({'InstrumentConfigAS': 70004, 'source': 'OMI'})

        # set inside runtimte
        mock = {'Runtime Parameters': mock}

        # dump into file
        destination = '../bin/OMSTACK.input'
        self._dispense(mock, destination)

        return None


# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 6
    arguments = arguments[:6]

    # check for clock option to time processes
    clock = any([option in options for option in ['-clock', '--clock', '-c', '--c']])

    # check for renew option to renew the control file
    refresh = any([option in options for option in ['-refresh', '--refresh', '-r', '--r']])

    # unpack control file name, output directory, input directory, and subdirectory range
    controller, code, sink, source, start, finish = arguments

    # create centipede instance
    centipede = Centipede(controller, code, sink, source, start, finish, clock)

    # if control file is to be refreshed
    if refresh:

        # create Description.txt and control.txt file from given directories
        centipede.control()

    # reduce each orbit to orbital mean, min, max, std, and median
    centipede.crawl()
