
Archive Method: Compress

Description: Single orbit extractions of averaged irradiance data, both corrected
      and uncorrected, from OML1BCAL files, for purposes of tracking diffuser degradation
      over time.

ESDT Name: OMRAWFLUXqvd

Extractor Type: Filename_L1L2

File Format: h5

File SubTable Type: L1L2

Filename Pattern: <Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>_v<Collection>-<ProductionTime>.<File
      Format>

Instrument: OMI

Long Name: OMI/Aura Irradiance for Quartz Volume Diffuser from OML1BCAL files, 1-Orbit

Maximum Size: 1 MB

Metadata Type: orbital

Minimum Size: 1 KB

Parser: filename

Period: Orbits=1

Platform: Aura

Processing Level: L1B

Science Team Contacts: [Matthew Bandel (matthew.bandel@ssaihq.com), David Haffner
        (david.haffner@ssaihq.com)]

Support Contact: Phillip Durbin (pdurbin@sesda.com)

keypattern: <OrbitNumber>
