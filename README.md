# Boquette

Boquette is a Python based web application using the Bokeh plotting module to plot reduced OMI trending data across the mission, and for comparing parameters with each other.

#### Basic Usage

Boquette is meant to view data from hdf5 files, with a datasets in a group called "Categories" and abscissa information in a group called "IndependentVariables."  The bokeh server may be started by pointing at a directory of such files:

```buildoutcfg
$ bokeh serve boquettes.py --args directory/of/data
```